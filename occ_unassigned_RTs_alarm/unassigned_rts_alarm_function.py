import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from datetime import datetime, timezone, date
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack, file_upload_channel
from sfx import send_events_turnStoreOn
from os.path import expanduser
import time
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)

def process(country, canal):

    if country=='CO':
        country = 'CO'
        timezone = 'Bogota'
        BD = 1904
        BD1 = 13

    elif country=='AR':
        country = 'AR'
        timezone = 'Buenos_Aires'
        BD = 1337
        BD1 = 232

    elif country=='BR':
        country = 'BR'
        timezone = 'Sao_Paulo'
        BD = 1338
        BD1 = 231

    elif country=='MX':
        country = 'MX'
        timezone = 'Mexico_City'
        BD = 1371
        BD1 = 205

    elif country=='CL':
        country = 'CL'
        timezone = 'Santiago'
        BD = 1155
        BD1 = 309

    elif country=='PE':
        country = 'PE'
        timezone = 'Lima'
        BD = 1157
        BD1 = 684

    elif country=='UY':
        country = 'UY'
        timezone = 'Montevideo'
        BD = 1156
        BD1 = 306

    elif country=='EC':
        country = 'EC'
        timezone = 'Guayaquil'
        BD = 1922
        BD1 = 2177

    elif country=='CR':
        country = 'CR'
        timezone = 'Costa_Rica'
        BD = 1921
        BD1 = 2179 


    ### Iterations

    # Get all iterations
    it = run_query_r(BD1, """
    WITH a AS
    (
    SELECT 
    cast((payload->'store'->>'id') AS int) AS store_id,
    cast((payload->'store'->>'name') AS text) as name,
    order_iterations.order_id,
    id,
    m.order_id as rt_allocated
    FROM courier_dispatcher.order_iterations
    left join 
        (
        select distinct order_id 
        from courier_dispatcher.courier_order_matches 
        where taken_at is not null
        and created_at >= now() at time zone 'America/{timezone}' - interval '60 minutes'
        ) m 
    on m.order_id = order_iterations.order_id
    WHERE created_at >= now() at time zone 'America/{timezone}' - interval '60 minutes'
    )
    SELECT store_id,
    name,
    order_id, 
    rt_allocated,
    count(DISTINCT id) AS order_iterations
    FROM a
    WHERE store_id !=0
    GROUP BY 1,2,3,4
    """.format(timezone=timezone))

    print(it.shape)
    it.head()


    ### No Cancels

    # verticales
    verticals = run_query_s("""
    select 
    store_type,
    vertical_group
    from VERTICALS_LATAM.{country}_VERTICALS_V2 
    where vertical_group = 'ECOMMERCE'
    """.format(country = country))

    ## Ordenes no canceladas, state in_progress, no cancelados o ya finalizados
    no_canc = run_query_r(BD, """
    SELECT distinct 
    om.order_id,
    os.type
    FROM order_modifications om
    join orders o on o.id=om.order_id 
    left join order_stores os on os.order_id=o.id
    WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}') - interval '10 minutes'
    AND (om.TYPE NOT ILIKE '%cancel%') 
    and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
    and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
    and (os.type not in ('rappi_pay','queue_order_flow','soat_webview') or os.type is null)
    and o.payment_method not ilike '%synthe%'
    """.format(timezone=timezone))

    print(no_canc.shape)
    no_canc.head()

    # join verticals and cancels para tomar solo ecommerce
    no_cancels = pd.merge(no_canc, verticals[['store_type']],
                            left_on=["type"], right_on=["store_type"], how='inner').drop(columns=['store_type', 'type'])

    print(no_cancels.shape)
    no_cancels.head(100)


    ### Join iterations and no cancels

    it_c = pd.merge(it, no_cancels, left_on=['order_id'], right_on=['order_id'], how='inner')

    print(it_c.shape)
    it_c.head()


    ### time_iterations

    time_i = run_query_r(BD1, """
    select 
    order_id,
    max(created_at) as max_date, 
    min(created_at) as min_date
    from courier_dispatcher.order_iterations 
    WHERE created_at >= now() at time zone 'America/{timezone}' - interval '60 minutes'
    GROUP BY 1
    """.format(timezone=timezone))

    print(time_i.shape)
    time_i.head()

    time_i['max_date'] = pd.to_datetime(time_i['max_date'], format='%Y-%m-%d %H:%M:%S')
    time_i['min_date'] = pd.to_datetime(time_i['min_date'], format='%Y-%m-%d %H:%M:%S')
    time_i['iteration time'] = (time_i['max_date'] - time_i['min_date']) / pd.Timedelta(minutes=1)

    print(time_i.shape)
    time_i.head()


    ### Join iterations and time

    it_time = pd.merge(it_c, time_i[['order_id', 'iteration time']], how='left', left_on=['order_id'], right_on=['order_id'])

    print(it_time.shape)
    it_time.head()


    ### Cities

    cities = run_query_s("""
    select 
    store_id, 
    c.city as city_name
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.CITY_ADDRESSES c on c.id=s.city_address_id
    """.format(country=country))

    print(cities.shape)
    cities.head()


    ### Join iterations and cities

    it_cities = pd.merge(it_time, cities, left_on=["store_id"], right_on=["store_id"], how='left')
    print(it_cities.shape)
    it_cities.head()


    ### Thresholds

    # etiquetar las ordenes a alarmar 
    if country=='CO':
        
        it_cities.loc[(it_cities['city_name'].isin(['Bogotá', 'Cali', 'Medellín', 'Barranquilla'])) &
                    ((it_cities['order_iterations']>=60) | (it_cities['iteration time']>=90)) & (it_cities['rt_allocated'].isnull()), 'flag'] = True
        
        it_cities.loc[~(it_cities['city_name'].isin(['Bogotá', 'Cali', 'Medellín', 'Barranquilla'])) &
                    ((it_cities['order_iterations']>=100) | (it_cities['iteration time']>=120)) & (it_cities['rt_allocated'].isnull()), 'flag'] = True
        
    else:
        
        it_cities.loc[((it_cities['order_iterations']>=100) | (it_cities['iteration time']>=120)) & (it_cities['rt_allocated'].isnull()), 'flag'] = True
        
    print(it_cities.shape)
    it_cities.head()

    # filtro las iterations que superan el threshold
    df_tmp  = it_cities.loc[it_cities['flag']==True].copy()
    df_tmp = df_tmp[['store_id', 'name', 'city_name', 'order_id', 'order_iterations', 'iteration time']].copy()
    df_tmp['country'] = country
    df_tmp = df_tmp.rename(columns={'order_iterations': 'iterations'})
    df_tmp['iterations'] = df_tmp['iterations'].astype(int)
    print(df_tmp.shape)
    df_tmp.head()

    # Agrupo por city_name 
    df_tmp_group = df_tmp[['store_id', 'city_name', 'country']].copy()
    df_tmp_group['store_id'] = df_tmp_group['store_id'].astype(str)
    df_tmp_group.drop_duplicates(subset ="store_id", keep='first', inplace = True)
    df_tmp_group['store_ids'] = df_tmp_group.groupby(['city_name', 'country'])['store_id'].transform(lambda x: ','.join(x))
    df_tmp_group.drop_duplicates(subset ="store_ids", keep='first', inplace = True)
    df_tmp_group

    # Genero una lista de order_ids
    # it_cities['order_id'] = it_cities['order_id'].astype(str)
    # it_cities['Total_orders'] = it_cities.groupby(['store_id', 'name', 'city_name'])['order_id'].transform(lambda x: ','.join(x))
    # it_cities

    # Total orders
    # total_orders = it_cities.groupby(['store_id', 'name', 'city_name'])['order_id'].agg(['nunique']).reset_index()
    # total_orders.rename(columns={'nunique': 'total_orders'}, inplace=True)
    # print(total_orders.shape)
    # total_orders.head()

    # Iterations with threshold
    # iterations = it_cities.loc[it_cities['flag']==True].copy()
    # iterations['order_ids'] = iterations.groupby(['store_id', 'name', 'city_name'])['order_id'].transform(lambda x: ','.join(x))

    # iterations = iterations.groupby(['store_id', 'name', 'city_name','order_ids'])['order_id'].agg(['nunique']).reset_index()
    # iterations.rename(columns={'nunique': 'sup_iterations'}, inplace=True)
    # print(iterations.shape)
    # iterations.head()

    # no_iterations = it_cities.loc[pd.isnull(it_cities['flag'])].copy()
    # no_iterations = no_iterations.groupby(['store_id', 'name', 'city_name'])['order_id'].agg(['nunique']).reset_index()
    # no_iterations.rename(columns={'nunique': 'sup_no_iterations'}, inplace=True)
    # print(no_iterations.shape)
    # no_iterations.head()

    # print(total_orders.shape)
    # print(iterations.shape)
    # df_tmp = pd.merge(total_orders, iterations, on=['store_id', 'name', 'city_name'], how='left').fillna(0)

    # df_tmp = pd.merge(df_tmp, no_iterations, on=['store_id', 'name', 'city_name'], how='left').fillna(0)
    # df_tmp.sort_values('sup_iterations', ascending=False, inplace=True)
    # print(df_tmp.shape)
    # df_tmp = df_tmp[df_tmp['sup_iterations']>0]
    # print(df_tmp.shape)
    # df_tmp.head()


    ### Mensaje

    canal_slack = canal

    for index, row in df_tmp_group.iterrows():
        
        rows = dict(row) 
        city_name = row['city_name']
        country = row['country']
        
        if country == 'CO':
            
            if row['city_name'] in ['Bogotá', 'Cali', 'Medellín', 'Barranquilla']:
                        
                text = '''
                *Alarma Falta Asignación RTs* :alert:
                Country :flag-{country}:
                País: {country}
                Ciudad: {city_name}
                Las tiendas {store_ids} alcanzaron ordenes con más de 60 iteraciones o que llevan más de 90 minutos iterando en los últimos 10 minutos.
                <@angie.paez>, <@bpo-s.paniagua>, <@daniel.mosquera>

                Lista de tiendas con ordenes sin asignar: 
                '''.format(
                    country = country,
                    city_name = row['city_name'],
                    store_ids = row['store_ids'],
                    )

            else:
                
                text = '''
                *Alarma Falta Asignación RTs* :alert:
                Country :flag-{country}:
                País: {country}
                Ciudad: {city_name}
                Las tiendas {store_ids} alcanzaron ordenes con más de 100 iteraciones o que llevan más de 120 minutos iterando en los últimos 10 minutos.
                <@angie.paez>, <@bpo-s.paniagua>, <@daniel.mosquera>

                Lista de tiendas con ordenes sin asignar: 
                '''.format(
                    country = country,
                    city_name = row['city_name'],
                    store_ids = row['store_ids'],
                    )
        else:
            
            text = '''
            *Alarma Falta Asignación RTs* :alert:
            Country :flag-{country}:
            País: {country}
            Ciudad: {city_name}
            Las tiendas {store_ids} alcanzaron ordenes con más de 100 iteraciones o que llevan más de 120 minutos iterando en los últimos 10 minutos.
            <@angie.paez>, <@bpo-s.paniagua>, <@daniel.mosquera>
            
            Lista de tiendas con ordenes sin asignar: 
            '''.format(
                country = country,
                city_name = row['city_name'],
                store_ids = row['store_ids'],
                )
                
        print(text)      
        home = expanduser("~")
        path = '{}/stores_{}.xlsx'.format(home, city_name)
        df_tmp[df_tmp['city_name']==city_name].to_excel(path, index=False)
        file_upload_channel(canal_slack, text, path, "xlsx")

