#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#get_ipython().system('jupyter nbconvert --to script okr_availability.ipynb')


# In[1]:


from unassigned_rts_alarm_function import process

# In[5]:

# channel = ['U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV']
channel = ['C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07', 'C02LF1U8W07']
for index, country in enumerate(['BR', 'CO', 'MX', 'AR', 'CL', 'PE', 'UY', 'EC', 'CR']):

    try:
        process(country=country,  #True para encender false para no
                canal=channel[index])  #canal-mio = 'U026TUGHJTV'

    except Exception as e:
        print("Error in ", country)
        print(e.args)
