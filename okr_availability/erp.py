import os, sys, json, requests
import numpy as np
import pandas as pd
from datetime import datetime
from airflow.models import Variable

base_url = {
	'ar': 'https://services.rappi.com.ar',
	'br': 'https://services.rappi.com.br',
	'cl': 'https://services.rappi.cl',
	'co': 'https://services.rappi.com',
	'cr': 'https://services.rappi.co.cr',
	'ec': 'https://services.rappi.com.ec',
	'mx': 'https://services.mxgrability.rappi.com',
	'pe': 'https://services.rappi.pe',
	'uy': 'https://services.rappi.com.uy',
	'test': 'https://services.rappi.com.br'
}

def change_slot_status(slot_id, status, country, iteration=0):

	"""Change the slots status."""
	headers = {
		'Authorization': 'postman',
		'API_KEY': Variable.get('CPGOPS_API_KEY'),
		'Content-Type': 'application/json',
	}
	data = {
		"is_closed": not status
	}
	
	response = requests.put(
        ''.join([
            base_url[country],
            '/api/cpgops-gateway-ms/stores/slots/',
            str(slot_id)]),
        headers=headers,
        data=json.dumps(data)
	)

	print(response)
	
	if response.status_code == 200:
		return 'Sim'
	else:
		return 'Nao'