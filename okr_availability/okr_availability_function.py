#!/usr/bin/env python
# coding: utf-8

# In[1]:


# !jupyter nbconvert --to script okr_availability.ipynb


# # Import libraries

# In[2]:


import pandas as pd
import datetime as dt
import numpy as np
from datetime import datetime, timezone, date
import pytz
from snowflak import upload_df_occ, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from sfx import send_events_turnStoreOn
import time

pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", 100)
print(np.__version__)


# # Top stores

# In[19]:


def process(country, activar, canal):

    if country == "BR":
        country = "BR"
        country_min = "br"
        country_com = "Brasil"
        country_com_q = "Brasil"
        KAM = True
        KAM_stores = True
        onlist = False
        # brands = (276,729,686,357,365,437,748,283,284,187,271,340,782,1007,1062,958,2046,2214,6164,861,680,794,277,846,798)
        brands = ""
        complete_stores = True
        retail = True
        retail_dt = False
        blacklist = True
        BD = 6310
        tz = pytz.timezone("America/Sao_Paulo")
        flag = "\U0001F1E7\U0001F1F7"

    elif country == "CO":
        country = "CO"
        country_min = "co"
        country_com = "Colombia"
        country_com_q = "Colombia"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        #brands = (576, 2422, 149, 1960, 305, 664, 6972, 438, 7146, 1774, 50, 6616, 704, 770, 8049, 86, 1005, 1801, 1648, 759,527, 4238, 8242, 5971, 227, 4989, 2118, 42, 2098, 817, 1838,10,1663, 477, 2753, 1867,7167, 823,3870, 1919, 1275, 1627, 1706, 966, 290, 7439,2057, 8293, 183,5856,313,670,261,688,5461,2562,1778,7353,1834,435,446,1274,1560,8250,8022,896,5210,7407,346,7581,6047,634,3057,3064,6222,7554,7454,7561,5484,6915,8060,7881,)
        complete_stores = True
        retail = False
        retail_dt = False
        blacklist = False
        BD = 6312
        tz = pytz.timezone("America/Bogota")
        flag = "\U0001F1E8\U0001F1F4"

    #############################################################

    elif country == "LT_BR":
        country = "BR"
        country_min = "br"
        country_com = "Brasil"
        country_com_q = "Brasil"
        KAM = True
        KAM_stores = True
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = True
        BD = 6310
        tz = pytz.timezone("America/Sao_Paulo")
        flag = "\U0001F1E7\U0001F1F7"

    elif country == "LT_MX":
        country = "MX"
        country_min = "mx"
        country_com = "Mexico"
        country_com_q = "Mexico"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6324
        tz = pytz.timezone("America/Mexico_City")
        flag = "\U0001F1F2\U0001F1FD"

    elif country == "LT_CO":
        country = "CO"
        country_min = "co"
        country_com = "Colombia"
        country_com_q = "Colombia"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = False
        retail_dt = False
        blacklist = False
        BD = 6312
        tz = pytz.timezone("America/Bogota")
        flag = "\U0001F1E8\U0001F1F4"

    elif country == "LT_AR":
        country = "AR"
        country_min = "ar"
        country_com = "Argentina"
        country_com_q = "Argentina"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = True
        blacklist = False
        BD = 6309
        tz = pytz.timezone("America/Buenos_Aires")
        flag = "\U0001F1E6\U0001F1F7"

    elif country == "LT_CL":
        country = "CL"
        country_min = "cl"
        country_com = "Chile"
        country_com_q = "Chile"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6311
        tz = pytz.timezone("America/Santiago")
        flag = "\U0001F1E8\U0001F1F1"

    elif country == "LT_PE":
        country = "PE"
        country_min = "pe"
        country_com = "Peru"
        country_com_q = "Peru"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6323
        tz = pytz.timezone("America/Lima")
        flag = "\U0001F1F5\U0001F1EA"

    elif country == "LT_UY":
        country = "UY"
        country_min = "uy"
        country_com = "Uruguay"
        country_com_q = "Uruguai"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6322
        tz = pytz.timezone("America/Montevideo")
        flag = "\U0001F1FA\U0001F1FE"

    elif country == "LT_EC":
        country = "EC"
        country_min = "ec"
        country_com = "Ecuador"
        country_com_q = "Equador"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6447
        tz = pytz.timezone("America/Guayaquil")
        flag = "\U0001F1EA\U0001F1E8"

    elif country == "LT_CR":
        country = "CR"
        country_min = "cr"
        country_com = "Costa Rica"
        country_com_q = "Costa Rica"
        KAM = False
        KAM_stores = False
        onlist = False
        brands = ""
        complete_stores = False
        retail = True
        retail_dt = False
        blacklist = False
        BD = 6313
        tz = pytz.timezone("America/Costa_Rica")
        flag = "\U0001F1E8\U0001F1F7"

    # # KAM

    if KAM:

        KAM = run_query_s(
            """
        SELECT store_type, kam_email, kam_ops_email
        FROM CPGS_PLANNING_{country}.CPGS_PORTFOLIO_STORE_TYPES
        where REF_MONTH = '2021-10-01'""".format(
                country=country
            )
        )

        KAM["kam"] = KAM["kam_email"].replace({"@rappi.com": ""}, regex=True)
        KAM["kam_ops"] = KAM["kam_ops_email"].replace({"@rappi.com": ""}, regex=True)

        KAM["@name"] = "<@" + KAM["kam"].map(str) + ">"
        KAM["@name_ops"] = "<@" + KAM["kam_ops"].map(str) + ">"
        KAM[['@name_ops']] = KAM[['@name_ops']].replace(['<@>'], np.nan)

    else:

        KAM = run_query_s(
            """
        select s.store_id, kam, replace(kam,'@rappi.com', '') as name
        from {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s
        LEFT JOIN (SELECT DISTINCT STORE_ID,
                LAST_VALUE (USER_ID) OVER (PARTITION BY STORE_ID ORDER BY ID, UPDATED_AT) AS user_id
                FROM {country}_PGLR_MS_STORES_PUBLIC.STORE_COMMISSIONS_VW) SC
        ON SC.STORE_ID = S.STORE_ID
        LEFT JOIN (SELECT DISTINCT ID AS user_id,
                LAST_VALUE (EMAIL) OVER (PARTITION BY ID ORDER BY CREATED_AT, UPDATED_AT ) AS kam
                FROM {country}_GRABILITY_PUBLIC.USERS_VW) PU
        ON PU.USER_ID = SC.USER_ID
        where kam not ilike '%churn%'
        and s.deleted_at is null""".format(
                country=country
            )
        )

        KAM["@name"] = "<@" + KAM["name"].map(str) + ">"
        KAM["@name_ops"] = "--"

    print(KAM.shape)
    KAM.head()

    # # Top stores

    if onlist:
        top_stores = run_query_s(
            """
        select s.store_id from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
        inner join (
        select brand_group_id, brand_group_name, store_brand_id
        from
            (select store_brand_id, 
            last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
            last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
            from {min_country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
            join {min_country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
        where brand_group_id in {brands}
        group by 1,2,3) bg 
        on bg.store_brand_id = s.store_brand_id""".format(
                country=country, min_country=country_min, brands=brands
            )
        )

    else:
        top_stores = run_query_s(
            """
        select s.store_id from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
        inner join (
        select brand_group_id, brand_group_name, store_brand_id
        from
            (select store_brand_id, 
            last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
            last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
            from {min_country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
            join {min_country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
        group by 1,2,3) bg 
        on bg.store_brand_id = s.store_brand_id""".format(
                country=country, min_country=country_min
            )
        )

    print(top_stores.shape)
    top_stores.head()

    # # Verticals

    # In[14]:

    verticals = run_query_s(
        """
    select 
    id,
    store_type,
    vertical_sub_group,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Super'
    when upper (store_type) in ('TURBO') then 'Turbo'
    when upper (store_type) in ('TURBO_BEBIDAS') then 'Turbo'
    when upper (store_type) in ('TURBO_MARKET') then 'Turbo'
    when upper (store_type) in ('TURBO_EXPRESS') then 'Turbo'
    when upper (store_type) in ('LOJA_JA') then 'Turbo'
    when upper (store_type) in ('LOJA_JA_MARKET') then 'Turbo'
    when upper (store_type) in ('BEBIDAS_JA') then 'Turbo'
    when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('PHARMACY') then 'CPGs Now'
    when upper (vertical_sub_group) in ('LIQUOR') then 'CPGs Now'
    when upper (vertical_sub_group) in ('EXPRESS') then 'CPGs Now'
    when upper (vertical_sub_group) in ('SPECIALIZED') then 'Ecommerce'
    when upper(vertical_group) in ('CPGS','CPGs') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2 
    where vertical in ('CPGs Now','Super')""".format(
            country=country
        )
    )

    verticals.head()

    # # Orders

    # In[18]:

    # true para formales e informales
    if complete_stores:

        # others (formal stores)
        orders = run_query_s(
            """select store_id, count(distinct order_id) as orders
        from GLOBAL_FINANCES.global_orders
        where country = '{country}'
        and created_at::date >= dateadd(day,-15,current_date())::date
        and order_state in ('finished','pending_review')
        group by 1
        having count(distinct order_id) >= 2""".format(
                country=country
            )
        )

        print(orders.shape)

    # by only long tail the orders query is in long_tail query

    # Long-tail

    long_tail = run_query_s(
        """
    with orders as (select store_id, count(distinct order_id) as orders_last_15d
        from GLOBAL_FINANCES.global_orders
        where created_at::date >= dateadd(day,-15,current_date())::date
        and order_state in ('finished','pending_review')
        and country = '{country}'
        group by 1 )
    select a.country_code, a.store_id, point_of_contact as kam, is_long_tail, is_informal,
        a.tier, case when upper (a.tier) in ('TIER 1', 'TIER 2') then 2
                    when upper (a.tier) in ('TIER 3', 'TIER 3 CRITICAL') then 1
                    when is_informal='Yes' then 1
               end as min_orders_15D_required
    from FIVETRAN.CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK a
    join orders o on o.store_id=a.store_id
    where (a.is_long_tail ='Yes')
    and o.orders_last_15d>=min_orders_15D_required
    and country_code='{country}' 
    """.format(
            country=country
        )
    )

    long_tail["@name"] = long_tail["kam"].replace({"@rappi.com": ""}, regex=True)
    long_tail["@name"] = "<@" + long_tail["@name"].map(str) + ">"
    long_tail[["@name"]] = long_tail[["@name"]].replace(
        ["<@Undefined>", "<@Unknown>"], np.nan
    )
    long_tail.rename(columns={"min_orders_15d_required": "orders"}, inplace=True)

    long_tail["@name_ops"] = "--"

    print(long_tail.shape)
    long_tail.head()

    # # Stores

    # In[26]:

    core_stores = run_query_r(
        BD,
        """select distinct store_id, name, type, is_enabled, suggested_state, created_at, updated_at 
        from stores where deleted_at is null""",
    )

    print(core_stores.shape)

    # In[ ]:

    # si es true, generar todo de las no informales
    if complete_stores:

        # join with orders table
        order_stores = pd.merge(
            orders,
            core_stores,
            left_on=["store_id"],
            right_on=["store_id"],
            how="inner",
        )

        print(order_stores.shape)

        # join with top_store query
        top_stores = pd.merge(
            top_stores,
            order_stores,
            left_on=["store_id"],
            right_on=["store_id"],
            how="inner",
        )

        print(top_stores.shape)

        # join with email KAM query
        if KAM_stores:

            KAM_stores = pd.merge(
                top_stores,
                KAM[["store_type", "@name", "@name_ops"]],
                left_on=["type"],
                right_on=["store_type"],
                how="inner",
            ).drop(columns=["store_type"])

        else:

            KAM_stores = pd.merge(
                top_stores,
                KAM[["store_id", "@name", "@name_ops"]],
                left_on=["store_id"],
                right_on=["store_id"],
                how="left",
            )

        print(KAM_stores.shape)

    # In[ ]:

    # join with long-tail
    tail_stores = pd.merge(
        long_tail[["store_id", "orders", "@name", "@name_ops"]],
        core_stores,
        left_on=["store_id"],
        right_on=["store_id"],
        how="inner",
    )

    print(tail_stores.shape)

    if complete_stores:

        # only other rules
        print(KAM_stores.shape)
        stores_com = KAM_stores.copy()
        print(stores_com.shape)
        print("complete stores")

    else:

        # only long_tail
        print(tail_stores.shape)
        stores_com = tail_stores.copy()
        print(stores_com.shape)
        print("long tail")

    print(stores_com.shape)

    # In[ ]:

    # join with verticals query
    stores_verticals = pd.merge(
        stores_com,
        verticals[["vertical", "store_type", "vertical_sub_group"]],
        left_on=["type"],
        right_on=["store_type"],
        how="inner",
    ).drop(columns=["store_type"])

    stores_verticals[["@name"]] = stores_verticals[["@name"]].fillna("--")
    stores_verticals[["@name_ops"]] = stores_verticals[["@name_ops"]].fillna("--")

    print(stores_verticals.shape)
    stores_verticals.head()

    # In[ ]:

    count_stores = stores_verticals.loc[
        (stores_verticals["is_enabled"] == False)
        & (stores_verticals["suggested_state"] == True)
    ]
    print(count_stores.shape)
    count_stores.head()


    # # Store_atributes

    # In[ ]:

    store_atributes = run_query_r(
        BD, """select * from store_attribute where suspended=true"""
    )
    print(store_atributes.shape)
    store_atributes.head()

   
    # # Turn_on - store_atributes

    # In[ ]:

    turn_on = stores_verticals
    print(turn_on.shape)
    turn_on = pd.merge(
        turn_on,
        store_atributes,
        left_on=["store_id"],
        right_on=["store_id"],
        how="left",
        indicator=True,
    )
    print(turn_on.shape)
    print(turn_on["_merge"].value_counts())

    if len(turn_on) > 0:

        mask_store_atributes1 = (
            (turn_on["is_enabled"] == True)
            & (turn_on["suggested_state"] == True)
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes2 = (
            (turn_on["is_enabled"] == True)
            & (turn_on["suggested_state"] == False)
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes3 = (
            (turn_on["is_enabled"] == True)
            & (turn_on["suggested_state"] == True)
            & (turn_on["_merge"] == "left_only")
        )
        mask_store_atributes4 = (
            (turn_on["is_enabled"] == True)
            & (turn_on["suggested_state"] == False)
            & (turn_on["_merge"] == "left_only")
        )
        mask_store_atributes5 = (
            (turn_on["is_enabled"] == False)
            & (turn_on["suggested_state"] == True)
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes6 = (
            (turn_on["is_enabled"] == False)
            & (turn_on["suggested_state"] == False)
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes7 = (
            (turn_on["is_enabled"] == False)
            & (turn_on["suggested_state"] == True)
            & (turn_on["_merge"] == "left_only")
        )
        mask_store_atributes8 = (
            (turn_on["is_enabled"] == False)
            & (turn_on["suggested_state"] == False)
            & (turn_on["_merge"] == "left_only")
        )

        mask_store_atributes9 = (
            (turn_on["is_enabled"] == True)
            & (pd.isnull(turn_on["suggested_state"]))
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes10 = (
            (turn_on["is_enabled"] == True)
            & (pd.isnull(turn_on["suggested_state"]))
            & (turn_on["_merge"] == "left_only")
        )

        mask_store_atributes11 = (
            (turn_on["is_enabled"] == False)
            & (pd.isnull(turn_on["suggested_state"]))
            & (turn_on["_merge"] == "both")
        )
        mask_store_atributes12 = (
            (turn_on["is_enabled"] == False)
            & (pd.isnull(turn_on["suggested_state"]))
            & (turn_on["_merge"] == "left_only")
        )

        filters = [
            mask_store_atributes1,
            mask_store_atributes2,
            mask_store_atributes3,
            mask_store_atributes4,
            mask_store_atributes5,
            mask_store_atributes6,
            mask_store_atributes7,
            mask_store_atributes8,
            mask_store_atributes9,
            mask_store_atributes10,
            mask_store_atributes11,
            mask_store_atributes12,
        ]

        for index, filt in enumerate(filters):
            turn_on.loc[filt, "store_atributes_status"] = index + 1
        print(
            "{} Tiendas habilitadas en stores_verticals, con suggested True y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes1])
            ),
            "{} Tiendas habilitadas en stores_verticals, con suggested False y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes2])
            ),
            "{} Tiendas habilitadas en stores_verticals, con suggested True y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes3])
            ),
            "{} Tiendas habilitadas en stores_verticals, con suggested False y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes4])
            ),
            ############################################
            "{} Tiendas inhabilitadas en stores_verticals, con suggested True y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes5])
            ),
            "{} Tiendas inhabilitadas en stores_verticals, con suggested False y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes6])
            ),
            "**** {} Tiendas inhabilitadas en stores_verticals, con suggested True y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes7])
            ),
            "{} Tiendas inhabilitadas en stores_verticals, con suggested False y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes8])
            ),
            ############################################
            "{} Tiendas habilitadas en stores_verticals, con suggested Null y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes9])
            ),
            "{} Tiendas habilitadas en stores_verticals, con suggested Null y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes10])
            ),
            "{} Tiendas inhabilitadas en stores_verticals, con suggested Null y aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes11])
            ),
            "{} Tiendas inhabilitadas en stores_verticals, con suggested Null y NO aparecen en store_atributes".format(
                len(turn_on[mask_store_atributes12])
            ),
            len(turn_on) - len(turn_on["store_atributes_status"] >= 1),
        ),
        turn_on.rename(columns={"_merge": "store_atributes_merge"}, inplace=True)

        turn_on["store_atributes_status"].value_counts().sort_index()

    else:

        turn_on.rename(columns={"_merge": "store_atributes_merge"}, inplace=True)
        turn_on["store_atributes_status"] = ""


    # # Final

    # In[ ]:

    reactivate_stores = turn_on.loc[
        (turn_on["store_atributes_status"] == 7)
    ]
    reactivate_stores = reactivate_stores[
        [
            "store_id",
            "name",
            "type",
            "is_enabled",
            "suggested_state",
            "created_at_x",
            "orders",
            "updated_at_x",
            "vertical",
            "vertical_sub_group",
            "@name",
            "@name_ops",
        ]
    ]
    reactivate_stores.rename(
        columns={
            "created_at_x": "created_at",
            "updated_at_x": "updated_at",
            "@name_ops": "name_ops",
        },
        inplace=True,
    )
    reactivate_stores["updated_at"] = pd.to_datetime(
        reactivate_stores["updated_at"], format="%Y-%m-%d"
    )
    reactivate_stores["country"] = country_com

    import datetime

    reactivate_stores["Fecha de ejecución"] = str(
        datetime.datetime.now() - datetime.timedelta(hours=5)
    )
    upload_df_occ(reactivate_stores, "turn_store_on")

    print(count_stores.shape)
    print(reactivate_stores.shape)
    reactivate_stores.head(3)



    # In[ ]:

    canal_slack = canal
    encender = activar

    if len(reactivate_stores) > 0:

        for index, row in reactivate_stores.iterrows():
            time.sleep(10)
            rows = dict(row)
            store_id = str(row["store_id"])
            reason = "availability"
            turned_on_at = str(row['Fecha de ejecución'])
            send_events_turnStoreOn(store_id, country, reason, turned_on_at)

            if country == "BR":

                main_text = """
                Pais: {flag}
                \U0001F6A7 La Tienda {store_id} con vertical {vertical} es remitida a reconexión 
                \U0001F3EA Store Name: {store_name}
                \U0001F6F5 Ordenes ultimos 15 dias: {orders} 
                \U0001F514 Enable status: {is_enabled}
                \U0001F4A1 Sugested status: {suggest_state}
                \U0001F913 KAM: {KAM}
                \U0001F913 KAM_OPS: {KAM_ops}""".format(
                    store_id=row["store_id"],
                    store_name=row["name"],
                    orders=row["orders"],
                    is_enabled=row["is_enabled"],
                    suggest_state=row["suggested_state"],
                    vertical=rows["vertical"],
                    flag=flag,
                    KAM=row["@name"],
                    KAM_ops=row["name_ops"],
                )

            else:

                main_text = """
                Pais: {flag}
                \U0001F6A7 La Tienda {store_id} con vertical {vertical} es remitida a reconexión 
                \U0001F3EA Store Name: {store_name}
                \U0001F6F5 Ordenes ultimos 15 dias: {orders} 
                \U0001F514 Enable status: {is_enabled}
                \U0001F4A1 Sugested status: {suggest_state}
                \U0001F913 KAM: {KAM}""".format(
                    store_id=row["store_id"],
                    store_name=row["name"],
                    orders=row["orders"],
                    is_enabled=row["is_enabled"],
                    suggest_state=row["suggested_state"],
                    vertical=rows["vertical"],
                    flag=flag,
                    KAM=row["@name"],
                )

            if encender:
                print("Modo encender")
                # Enciende la tienda, si la respuesta es =200 la tienda se activo correctamente
                if turn_store_on(country_min, store_id) == 200:
                    main_text = main_text + "\n \t \t \U0001F7E2 Tienda activa \n \n"
                    bot_slack(main_text, canal_slack)

                else:
                    main_text = (main_text + "\n \t \t \U0001F534 Error al activar tienda \n \n")
                    bot_slack(main_text, canal_slack)

            else:
                main_text = main_text + "\n \t \t \U0001F7E2 Tienda activa \n \n"
                bot_slack(main_text, canal_slack)

    else:
        text = """Pais: {flag}
        \n \U00002705 No hay tiendas para activar.""".format(
            flag=flag
        )
        print(text)
        bot_slack(text, canal_slack)


