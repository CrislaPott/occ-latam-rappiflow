#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#get_ipython().system('jupyter nbconvert --to script okr_availability.ipynb')


# In[1]:


from okr_availability_function import process


# In[5]:

encender = [True, True, True, True, True, True, True, True, True, True, True]
channel = ['C029QH5V0HE', 'C02AF2K2D7E', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T', 'C02DJJNG24T']
for index, country in enumerate(['BR', 'CO', 'LT_BR', 'LT_CO', 'LT_MX', 'LT_AR', 'LT_CL', 'LT_PE', 'LT_UY', 'LT_EC', 'LT_CR']):
    try:
        process(country=country, 
                activar=encender[index],  #True para encender false para no
                canal=channel[index])  #canal-mio = 'U026TUGHJTV'
    except Exception as e:
        print("Error in ", country)
        print(e.args)




