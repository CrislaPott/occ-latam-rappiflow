import os, re, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

countries = ['ar', 'br', 'co', 'cl', 'ec', 'mx', 'cr', 'pe', 'uy']
final = pd.DataFrame()

for country in countries:
    query = """
    --no_cache
    select
        timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date as day,
        pais,
        count(distinct o.order_id) as ordens_impactadas
    from br_writable.occ_cpgs_latam_impacto_sat_slots o
    join {country}_core_orders_public.order_modifications om
        on to_varchar(o.order_id) = to_varchar(om.order_id)
        and o.pais = '{country}'
        and om.type = 'change_to_scheduled'
    where timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= dateadd(day, -30, current_date())::date
    group by 1, 2
    """.format(country=country)

    df = snow.run_query(query)
    final = pd.concat([final, df], ignore_index=True)

final['pais'].value_counts()

try:
    snow.truncate_table('occ_cpgs_impacto_final_slot_sat')
except:
    pass

snow.upload_df(final, 'occ_cpgs_impacto_final_slot_sat')

