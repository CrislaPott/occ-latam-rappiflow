import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token_shoppers_erp')
token = token.loc[1, 'Token'].strip()

datetime_now = str(arrow.utcnow().to('America/Recife'))
datetime_now_hour = arrow.utcnow().to('America/Recife').hour

shoppers = pd.DataFrame()
current_page = 1
last_page = 2

url = "https://services.rappi.cl/shopper-erp/api/shoppers?date={datetime_now}&page={current_page}&size=40".format(
    datetime_now=datetime_now,
    current_page=current_page
)

headers = {
'Authorization': token,
'Content-Type': 'application/json'
}

response = requests.request("GET", url, headers=headers)
results = response.json()

print(results.keys())