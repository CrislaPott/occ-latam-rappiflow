import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token_shoppers_erp')
token = token.loc[1, 'Token'].strip()

datetime_now = str(arrow.utcnow().to('America/Recife'))
datetime_now_hour = arrow.utcnow().to('America/Recife').hour

shoppers = pd.DataFrame()
current_page = 1
last_page = 2

def convert_shift(source_date):
    try:
        result = arrow.get(source_date).to('America/Recife').hour
        return result
    except:
        return np.nan 

while current_page <= last_page:
    url = "https://services.rappi.cl/shopper-erp/api/shoppers?date={datetime_now}&page={current_page}&size=40".format(
        datetime_now=datetime_now,
        current_page=current_page
    )

    headers = {
    'Authorization': token,
    'Content-Type': 'application/json'
    }

    response = requests.request("GET", url, headers=headers)
    results = response.json()

    print(results.keys())

    df = pd.DataFrame(results['data'])
    df = df[[
        'id', 'physical_store_id',
        'shift_starts_at', 'shift_ends_at',
        'supervisor_id', 'active'
    ]]

    df['shift_starts_hour'] = df['shift_starts_at'].apply(lambda x: convert_shift(x))
    df['shift_ends_hour'] = df['shift_ends_at'].apply(lambda x: convert_shift(x))

    shoppers = pd.concat([shoppers, df], ignore_index=True)

    current_page += 1
    last_page = results['last_page']

alarmed_shoppers = shoppers[
    (shoppers['shift_starts_hour'] == datetime_now_hour) &
    (shoppers['active'] == False)
]

print(alarmed_shoppers.shape[0])

if alarmed_shoppers.shape[0] > 0:

    file = 'occ_cpgs_inactive_shoppers_cl.csv'
    alarmed_shoppers.to_csv(file, index=False)

    trigger_text = """
    :alert: *Shoppers Inactivos en Inicio de Jornada* - :flag-cl: :alert:

    <!subteam^S01D2QTSE8H>

    Abajo presentase la lista de shoppers inactivos en lo inicio de su turno.
    """

    slack.file_upload_channel(channel='C01EEPH2ECU', text=trigger_text, filename=file, filetype='csv')

    if os.path.exists(file):
        os.remove(file)

