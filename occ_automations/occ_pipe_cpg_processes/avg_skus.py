import os, re, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

# pd.options.display.max_rows = 999

countries = ['co', 'br', 'ar', 'cl', 'uy', 'pe', 'mx', 'ec', 'cr']

final = pd.DataFrame()

for country in countries:

    if country == 'co':
        suffix = '_vw'
    else:
        suffix = ''

    query = """
    --no_cache
    with  verticals as (
        select distinct
            store_type,
            last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
        from {country}_PGLR_MS_STORES_PUBLIC.verticals
    )

    , base as (
    select 
        o.closed_at,
        o.id as order_id, 
        skus, 
        units
    from
        {country}_core_orders_public.orders{suffix} o
    left join
        {country}_core_orders_calculated_information.orders{suffix} o2 on o2.order_id = o.id
    left join (select order_id, count(distinct product_id) as skus, sum(units) units from {country}_core_orders_public.order_product{suffix} where _fivetran_deleted=false group by 1) op on op.order_id=o.id
    left join
        {country}_PGLR_MS_STORES_PUBLIC.stores{suffix} s on s.store_id = o2.store_id
    left join
        verticals b on b.store_type = s.type
    where
    o.closed_at::date >= dateadd(day,-60,current_date())::date
    and sub_group in ('Super','Hiper')
    group by
        1,2,3,4
    )
    select closed_at::date AS day, avg(skus) as avg_distinct_skus from base group by 1
    """.format(country=country, suffix=suffix)

    results = snow.run_query(query)
    results['country'] = country
    
    final = pd.concat([final, results], ignore_index=True)

final['country'].value_counts()

try:
    snow.truncate_table('occ_cpgs_latam_impacto_avg_skus')
except:
    pass

snow.upload_df(final, 'occ_cpgs_latam_impacto_avg_skus')