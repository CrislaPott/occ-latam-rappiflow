import os, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow
import queries

os.chdir('../')

# Empty Results Dataframe
processes_df = pd.DataFrame()

# Date and Time
current_date = date.today()
current_hour = datetime.now().hour 
current_minute = datetime.now().minute 

if 0 <= current_minute < 30:
    window_minute = 0
elif 30 <= current_minute <= 59:
    window_minute = 30

# Shopper Sobrecarregado
ps = snow.run_query(queries.ps_co)
ps = ps[ps['status'] == 'Saturado(a)']
if ps.shape[0] > 0:
    inserts = [
        {
            'country': 'CO',
            'process': 'Shopper Sobrecarregado',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': ps.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

if processes_df.shape[0] > 0:
    snow.upload_df(processes_df, 'occ_cpgs_processes_latam')