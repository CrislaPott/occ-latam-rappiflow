import os, re, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow
import queries

os.chdir('../')

# Empty Results Dataframe
processes_df = pd.DataFrame()

# Date and Time
current_date = date.today()
current_hour = datetime.now().hour 
current_minute = datetime.now().minute 

if 0 <= current_minute < 30:
    window_minute = 0
elif 30 <= current_minute <= 59:
    window_minute = 30

def get_slot_hour(value):
    try:
        value = str(value)
        match = re.findall(r"(?<=T)(.*)(?=-)", value)
        match2 = match[0].split(':')[0]
        return match2.strip()
    except:
        return np.nan

def get_slot_hour_ar(value):
    try:
        value = str(value)
        match = re.findall(r"(?<=T)(.*)", value)
        match2 = match[0].split(':')[0]
        return match2.strip()
    except:
        return np.nan

# Saturacao de Slots - BR
# cpgops-stores-ms
ss_br = redash.run_query_id(17750)

cleaned_ss_br = ss_br.copy()

if cleaned_ss_br.shape[0] > 0:
    cleaned_ss_br.rename(columns={'id': 'slot_id'}, inplace=True)
    cleaned_ss_br['slot_hour'] = cleaned_ss_br['slot'].apply(lambda x: get_slot_hour_ar(x))
    cleaned_ss_br = cleaned_ss_br[['physical_store_id', 'slot_id', 'slot_hour']]

    cleaned_ss_br['country'] = 'BR'
    cleaned_ss_br['data_utc'] = current_date
    cleaned_ss_br['hora_utc'] = current_hour 
    cleaned_ss_br['minute_utc'] = window_minute

    print(cleaned_ss_br.head())

# Saturacao de Slots - AR
# cpgops-stores-ms
ss_ar = redash.run_query_id(17879)

cleaned_ss_ar = ss_ar.copy()

if cleaned_ss_ar.shape[0] > 0:
    cleaned_ss_ar.rename(columns={'id': 'slot_id'}, inplace=True)
    cleaned_ss_ar['slot_hour'] = cleaned_ss_ar['slot'].apply(lambda x: get_slot_hour_ar(x))
    cleaned_ss_ar = cleaned_ss_ar[['physical_store_id', 'slot_id', 'slot_hour']]

    cleaned_ss_ar['country'] = 'AR'
    cleaned_ss_ar['data_utc'] = current_date
    cleaned_ss_ar['hora_utc'] = current_hour 
    cleaned_ss_ar['minute_utc'] = window_minute

    print(cleaned_ss_ar.head())

# Saturacao de Slots - CL
# cpgops-stores-ms
ss_cl = redash.run_query_id(19196)

cleaned_ss_cl = ss_cl.copy()

if cleaned_ss_cl.shape[0] > 0:
    cleaned_ss_cl.rename(columns={'id': 'slot_id'}, inplace=True)
    cleaned_ss_cl['slot_hour'] = cleaned_ss_cl['slot'].apply(lambda x: get_slot_hour_ar(x))
    cleaned_ss_cl = cleaned_ss_cl[['physical_store_id', 'slot_id', 'slot_hour']]

    cleaned_ss_cl['country'] = 'CL'
    cleaned_ss_cl['data_utc'] = current_date
    cleaned_ss_cl['hora_utc'] = current_hour 
    cleaned_ss_cl['minute_utc'] = window_minute

    print(cleaned_ss_cl.head())

# Saturacao de Slots - MX
# cpgops-stores-ms
ss_mx = redash.run_query_id(20438)

cleaned_ss_mx = ss_mx.copy()

if cleaned_ss_mx.shape[0] > 0:
    cleaned_ss_mx.rename(columns={'id': 'slot_id'}, inplace=True)
    cleaned_ss_mx['slot_hour'] = cleaned_ss_mx['slot'].apply(lambda x: get_slot_hour_ar(x))
    cleaned_ss_mx = cleaned_ss_mx[['physical_store_id', 'slot_id', 'slot_hour']]

    cleaned_ss_mx['country'] = 'MX'
    cleaned_ss_mx['data_utc'] = current_date
    cleaned_ss_mx['hora_utc'] = current_hour 
    cleaned_ss_mx['minute_utc'] = window_minute

    print(cleaned_ss_mx.head())

ss_final = pd.concat([cleaned_ss_br, cleaned_ss_ar, cleaned_ss_cl, cleaned_ss_mx], ignore_index=True)



snow.upload_df(ss_final, 'occ_cpgs_slot_sat_latam')