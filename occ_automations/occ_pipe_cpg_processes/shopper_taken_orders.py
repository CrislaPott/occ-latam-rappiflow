import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

destination_table = 'occ_cpgs_shopper_taken_orders_co'

# last_datetime = str(arrow.utcnow().shift(minutes=-10))
last_update = snow.run_query('select max(finished_at) as last_update from br_writable.{}'.format(destination_table))
last_datetime = last_update.loc[0, 'last_update']
last_datetime = str(arrow.get(last_datetime).to('UTC'))

print(last_datetime)

query = """
select
    os.physical_store_id,
    os.shopper_id,
    os.order_id,
    os.place_at,
    osc.finished_by_shopper_at as finished_at
from order_summary os 
join order_summary_calculated osc on os.order_id = osc.order_id 
where 
    osc.finished_by_shopper_at > '{last_datetime}'
""".format(last_datetime=last_datetime)

results = redash.run_query(2453, query)

snow.upload_df(results, destination_table)