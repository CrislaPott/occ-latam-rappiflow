import os, re, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

mapping_lojas = {}

lojas = gsheets.read_sheet('1Ao5QFx7xyE8uAxmwR_fno5B0zlfl7SrFrMHbckaCs70', 'Tiendas!B2:L999')
lojas = lojas[
    (lojas['PAÍS'] == 'BR') &
    (lojas['PICKING POR'].notna())
]

for index, row in lojas.iterrows():
    loja = row['ID FÍSICO'].strip()
    picking = row['PICKING POR'].strip().lower()
    mapping_lojas[loja] = picking

logs = snow.run_query("""
select * 
from BR_WRITABLE.occ_monit_latam_cpgs
where 
    pais ilike '%br%'
    and tienda is not null
    and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date) 
""")

logs['thread_id'] = logs['thread_ts'] + '#' + logs['ts']

logs = logs[['thread_id', 'thread_ts', 'ts', 'thread_name', 'analyst_name', 'tienda', 'analyst_name']]

logs['tienda'] = logs['tienda'].astype(int)
logs['tienda'] = logs['tienda'].astype(str)
logs['picking_type'] = logs['tienda'].map(mapping_lojas)

try:
    snow.truncate_table('occ_monit_latam_cpgs_picking_br')
except:
    pass

snow.upload_df(logs, 'occ_monit_latam_cpgs_picking_br')