import os, re, sys, json, requests
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

# pd.options.display.max_rows = 999

def get_skus_distribuidos(text):
    try:
        result = str(re.search("([^\n]{0,1})skus distribuidos([^\n]{0,1}):(.*)", text).group(3))
        result = result.replace('*', '')
        result = result.replace('_', '')
        result = result.strip()
        return int(result)
    except:
        return np.nan

def get_skus_ventana(text):
    try:
        result = str(re.search("([^\n]{0,1})skus de ventana([^\n]{0,1}):(.*)", text).group(3))
        result = result.replace('*', '')
        result = result.replace('_', '')
        result = result.strip()
        return int(result)
    except:
        return np.nan

def extract_order_ids_limbo(text):
    if 'order_id' in text:
        try:
            result = re.search("([^\n]{0,1})order_id([^\n]{0,1}):(.*)", text).group(3)
            return str(result)
        except:
            return np.nan 
    elif 'orden id' in text:
        try:
            result = re.search("([^\n]{0,1})orden id([^\n]{0,1}):(.*)", text).group(3)
            return str(result)
        except:
            return np.nan 
    elif 'orden_id' in text:
        try:
            result = re.search("([^\n]{0,1})orden_id([^\n]{0,1}):(.*)", text).group(3)
            return str(result)
        except:
            return np.nan 

def extract_order_ids_slot_sat(text):
    text = text.lower()
    if 'reprogramación' in text:
        try:
            result = re.search("([^\n]{0,1})order(.*)id(.*)reprogramación([^\n]{0,1}):(.*)", text).group(5)
            return str(result)
        except:
            return np.nan
    elif 'reprogramacion' in text:
        try:
            result = re.search("([^\n]{0,1})order(.*)id(.*)reprogramacion([^\n]{0,1}):(.*)", text).group(5)
            return str(result)
        except:
            return np.nan
    else:
        return np.nan  

# Ordenes en Limbo

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Órdenes en Limbo%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df['order_id'] = df['text'].apply(lambda x: extract_order_ids_limbo(x))
df['order_id'] = df['order_id'].str.replace(" e ", ",")
df['order_id'] = df['order_id'].str.replace(" y ", ",")
df['order_id'] = df['order_id'].str.split(',')
df = df.explode('order_id').reset_index()

df['order_id'] = df['order_id'].str.replace('*', '')
df['order_id'] = df['order_id'].str.strip()

df = df[
    df['order_id'].str.contains("[0-9]{1,15}", regex=True, na=False)
]

del df['index']
del df['orden_id']

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'analyst_name', 'text', 'order_id']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_limbo')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_limbo')

df.head()

# Saturacion de Slots

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Saturación de Slots%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df['order_id'] = df['text'].apply(lambda x: extract_order_ids_slot_sat(x))
df['order_id'] = df['order_id'].astype(str)
df['order_id'] = df['order_id'].str.replace(" e ", ",")
df['order_id'] = df['order_id'].str.replace(" y ", ",")
df['order_id'] = df['order_id'].str.split(',')
df = df.explode('order_id').reset_index()

df['order_id'] = df['order_id'].str.replace('*', '')
df['order_id'] = df['order_id'].str.strip()

df = df[
    df['order_id'].str.contains("[0-9]{1,15}", regex=True, na=False)
]

del df['index']
del df['orden_id']

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'analyst_name', 'text', 'order_id']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_sat_slots')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_sat_slots')

df.head()

# 40 SKUs

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Órdenes con más de 40 SKUs%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df['order_id'] = df['text'].apply(lambda x: extract_order_ids_limbo(x))
df['order_id'] = df['order_id'].str.replace(" e ", ",")
df['order_id'] = df['order_id'].str.replace(" y ", ",")
df['order_id'] = df['order_id'].str.split(',')
df = df.explode('order_id').reset_index()

df['order_id'] = df['order_id'].str.replace('*', '')
df['order_id'] = df['order_id'].str.strip()

df = df[
    df['order_id'].str.contains("[0-9]{1,15}", regex=True, na=False)
]

del df['index']
del df['orden_id']

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'analyst_name', 'text', 'order_id']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_40skus')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_40skus')

df.head()

# Fin de Jornada

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Fin de Jornada%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df['order_id'] = df['text'].apply(lambda x: extract_order_ids_limbo(x))
df['order_id'] = df['order_id'].str.replace(" e ", ",")
df['order_id'] = df['order_id'].str.replace(" y ", ",")
df['order_id'] = df['order_id'].str.split(',')
df = df.explode('order_id').reset_index()

df['order_id'] = df['order_id'].str.replace('*', '')
df['order_id'] = df['order_id'].str.strip()

df = df[
    df['order_id'].str.contains("[0-9]{1,15}", regex=True, na=False)
]

del df['index']
del df['orden_id']

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'analyst_name', 'text', 'order_id']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_fin_jornada')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_fin_jornada')

df.head()

# Shopper = 0

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Shopper igual a 0%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.replace('_', '')
df['pais'] = df['pais'].str.strip()

df['order_id'] = df['text'].apply(lambda x: extract_order_ids_limbo(x))
df['order_id'] = df['order_id'].str.replace(" e ", ",")
df['order_id'] = df['order_id'].str.replace(" y ", ",")
df['order_id'] = df['order_id'].str.split(',')
df = df.explode('order_id').reset_index()

df['order_id'] = df['order_id'].str.replace('*', '')
df['order_id'] = df['order_id'].str.strip()

df = df[
    df['order_id'].str.contains("[0-9]{1,15}", regex=True, na=False)
]

del df['index']
del df['orden_id']

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'analyst_name', 'text', 'order_id']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_shopper0')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_shopper0')

df.head()

# Shopper Sobrecargado

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Shoppers sobrecargados%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
order by ts desc
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df['skus_distribuidos'] = df['text'].apply(lambda x: get_skus_distribuidos(x))
df['skus_ventana'] = df['text'].apply(lambda x: get_skus_ventana(x))

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'tienda', 'analyst_name', 'text', 'skus_distribuidos', 'skus_ventana']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_shopper_sobrecargado')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_shopper_sobrecargado')

df.head()

# Retrasos

base_query = """
select *
from BR_WRITABLE.occ_monit_latam_cpgs
where thread_name ilike '%CPGs - Retrasos%'
and timeadd(hour, -3, dateadd('seconds', ts, '1970-01-01'))::date >= timeadd(hour, -3, dateadd(day, -60, current_date())::date)
order by ts desc
"""

df = snow.run_query(base_query)

df['pais'] = df['pais'].str.replace('*', '')
df['pais'] = df['pais'].str.strip()

df = df[['thread_ts', 'ts', 'thread_name', 'pais', 'tienda', 'analyst_name', 'text', 'assigned', 'in_picking']]

try:
    snow.truncate_table('occ_cpgs_latam_impacto_retrasos')
except:
    pass

snow.upload_df(df, 'occ_cpgs_latam_impacto_shopper_retrasos')