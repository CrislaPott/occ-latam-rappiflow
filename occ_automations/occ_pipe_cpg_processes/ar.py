import os, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow
import queries

os.chdir('../')

# Empty Results Dataframe
processes_df = pd.DataFrame()

# Date and Time
current_date = date.today()
current_hour = datetime.now().hour 
current_minute = datetime.now().minute 

if 0 <= current_minute < 30:
    window_minute = 0
elif 30 <= current_minute <= 59:
    window_minute = 30

# Saturacao de Slots
# cpgops-stores-ms
ss = redash.run_query_id(17879)
if ss.shape[0] > 0:
    inserts = [
        {
            'country': 'AR',
            'process': 'Saturacao de Slots',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': ss.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Atrasos
# cpgops-orders-ms
atrasos = redash.run_query_id(17875)
atrasos = atrasos[atrasos['Assign 10m + Picking 30m'] > 0]
if atrasos.shape[0] > 0:
    inserts = [
        {
            'country': 'AR',
            'process': 'Atrasos',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': atrasos.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Limbo
# cpgops-orders-ms
limbo = redash.run_query_id(17884)
if limbo.shape[0] > 0:
    inserts = [
        {
            'country': 'AR',
            'process': 'Limbo',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': limbo.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Embudo
# cpgops-orders-ms
embudo = redash.run_query_id(17886)
if embudo.shape[0] == 0:
    embudo_occurencies = 0
elif embudo.shape[0] > 0:
    embudo_occurencies = 1
inserts = [
    {
        'country': 'AR',
        'process': 'Embudo',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': embudo_occurencies
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# 40 SKUs
# cpgops_events_ms
df_40_skus = redash.run_query_id(17890)
if df_40_skus.shape[0] > 0:
    inserts = [
        {
            'country': 'AR',
            'process': '40 SKUs',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': df_40_skus.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Shopper Sobrecarregado
ps = snow.run_query(queries.ps_ar)
ps = ps[ps['status'] == 'Saturado(a)']
if ps.shape[0] > 0:
    inserts = [
        {
            'country': 'AR',
            'process': 'Shopper Sobrecarregado',
            'data_utc': current_date,
            'hora_utc': current_hour,
            'minute_utc': window_minute,
            'occurencies': ps.shape[0]
        } 
    ]
    inserts = pd.DataFrame(inserts)
    processes_df = pd.concat([processes_df, inserts], ignore_index=True)

snow.upload_df(processes_df, 'occ_cpgs_processes_latam')