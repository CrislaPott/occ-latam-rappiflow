import os, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow
import queries

os.chdir('../')

# Empty Results Dataframe
processes_df = pd.DataFrame()

# Date and Time
current_date = date.today()
current_hour = datetime.now().hour 
current_minute = datetime.now().minute 

if 0 <= current_minute < 30:
    window_minute = 0
elif 30 <= current_minute <= 59:
    window_minute = 30

# Saturacao de Slots
# cpgops-stores-ms
ss_br = redash.run_query(1969, queries.ss_br)
inserts = [
    {
        'country': 'BR',
        'process': 'Saturacao de Slots',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': ss_br.shape[0]
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Atrasos
# cpgops-orders-ms
br_atrasos = redash.run_query(2451, queries.br_atrasos)
br_atrasos = br_atrasos[br_atrasos['Assign 10m + Picking 30m'] > 0]

inserts = [
    {
        'country': 'BR',
        'process': 'Atrasos',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': br_atrasos.shape[0]
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Limbo
# cpgops-orders-ms
br_limbo = redash.run_query(2451, queries.br_limbo)
inserts = [
    {
        'country': 'BR',
        'process': 'Limbo',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': br_limbo.shape[0]
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Embudo
# cpgops-orders-ms
br_embudo = redash.run_query(2451, queries.br_embudo)
if br_embudo.shape[0] == 0:
    embudo_occurencies = 0
elif br_embudo.shape[0] > 0:
    embudo_occurencies = 1
inserts = [
    {
        'country': 'BR',
        'process': 'Embudo',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': embudo_occurencies
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# 40 SKUs
# cpgops_events_ms
br_40_skus = redash.run_query(2447, queries.br_40_skus)
inserts = [
    {
        'country': 'BR',
        'process': '40 SKUs',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': br_40_skus.shape[0]
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

# Shopper Sobrecarregado - BR
ps_br = snow.run_query(queries.ps_br)
ps_br = ps_br[ps_br['status'] == 'Saturado(a)']
inserts = [
    {
        'country': 'BR',
        'process': 'Shopper Sobrecarregado',
        'data_utc': current_date,
        'hora_utc': current_hour,
        'minute_utc': window_minute,
        'occurencies': ps_br.shape[0]
    } 
]
inserts = pd.DataFrame(inserts)
processes_df = pd.concat([processes_df, inserts], ignore_index=True)

snow.upload_df(processes_df, 'occ_cpgs_processes_latam')