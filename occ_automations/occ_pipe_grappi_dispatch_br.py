import os, sys, json, pytz, requests 
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import gsheets
from lib import snowflake as snow

cities = snow.run_query('select distinct code from BR_PG_MS_OPERATION_MANAGEMENT_PUBLIC.cities')
cities = cities['code'].unique().tolist()

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

def get_value(token, operation_type, city):
    tz = pytz.timezone('America/Sao_Paulo')
    current_date = str(datetime.now(tz=tz).date())
    current_hour = str(datetime.now(tz=tz).hour).zfill(2)
    current_minute = str(datetime.now(tz=tz).minute).zfill(2)

    five_minutes_earlier = str((datetime.now(tz=tz) - timedelta(minutes=5)).date())
    five_minutes_earlier_hour = str((datetime.now(tz=tz) - timedelta(minutes=5)).hour).zfill(2)
    five_minutes_earlier_minute = str((datetime.now(tz=tz) - timedelta(minutes=5)).minute).zfill(2)

    begin = '{}T{}:{}:00-03:00'.format(five_minutes_earlier, five_minutes_earlier_hour, five_minutes_earlier_minute)
    end = '{}T{}:{}:00-03:00'.format(current_date, current_hour, current_minute)

    url = "https://services.rappi.com.br/api/operation-management/metrics"

    payload="{\"metric_query\":[{\"names\":[\"orders_in_dispatch\",\"filtered_orders\"],\"params\":{\"country\":\"BR\",\"city\":\"%s\",\"store_type\":\"%s\"},\"time_range\":{\"begin\":\"%s\",\"end\":\"%s\"},\"grouping\":{\"by_store_type\":true}}]}" % (city, operation_type, begin, end)

    headers = {
    'Authorization': token,
    'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    results = response.json()

    values = results['metrics'][0]['graphs'][0]['data']['iterating_orders']
    values = [value for value in values if value != '0']

    return values[-1]

final = pd.DataFrame()
datetime_utc = str(datetime.now())
for city in cities:
    try:
        total = get_value(token, '', city)
    except:
        total = np.nan 
    
    try:
        restaurant = get_value(token, 'restaurant', city)
    except:
        restaurant = np.nan

    try:
        market = get_value(token, 'market', city)
    except:
        market = np.nan

    try:
        whim = get_value(token, 'whim', city)
    except:
        whim = np.nan
    
    try:
        mobility = get_value(token, 'mobility', city)
    except:
        mobility = np.nan

    try:
        express_picker = get_value(token, 'express_picker', city)
    except:
        express_picker = np.nan
    
    try:
        ultraservicio = get_value(token, 'ultraservicio', city)
    except:
        ultraservicio = np.nan

    try:
        courier = get_value(token, 'courier', city)
    except:
        courier = np.nan

    try:
        express = get_value(token, 'express', city)
    except:
        express = np.nan

    inserts = [{
        'datetime_utc': datetime_utc,
        'city': city,
        'total': total,
        'market': market,
        'restaurant': restaurant,
        'whim': whim,
        'mobility': mobility,
        'express_picker': express_picker,
        'ultraservicio': ultraservicio,
        'courier': courier,
        'express': express 
    }]
    
    inserts = pd.DataFrame(inserts)
    final = pd.concat([final, inserts], ignore_index=True)

snow.upload_df(final, 'occ_grappi_dispatch_br')