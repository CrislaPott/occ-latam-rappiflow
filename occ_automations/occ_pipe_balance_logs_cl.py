import sys
import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now()
oldest = latest - timedelta(minutes=30)

channel_history = tl.get_channel_history(
    channel = "C01DFAHJ05S",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

if channel_history.shape[0] == 0:
    print('Sem mensagens para processar')
    sys.exit(0)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].astype(str)
    
print(channel_history['username'].value_counts())

slack_users = pd.read_csv('input/slack_users.csv')

def transform_saturation(sat):
    sat = sat.replace(',', '.')
    try:
        sat = float(sat)
        return sat
    except:
        return None
    
def transform_unassigned(un):
    un = un.replace('%', '')
    un = un.replace(',', '.')
    try:
        un = float(un)
        if un <= 100:
            return un
    except:
        return None
    
def calculate_start_hour(begin):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        return int(begin_list[0])
    except:
        return None

def calculate_duration(begin, end):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        begin = begin_list[0] + ':' + begin_list[1]

        end_list = end.split(':')
        end_list = [x.zfill(2) for x in end_list]
        end = end_list[0] + ':' + end_list[1]

        FMT = '%H:%M'

        duration = (datetime.strptime(end, FMT) - datetime.strptime(begin, FMT)).seconds / 60
        return duration
    except:
        return None

# Eventualities
try:
    cl_eventualities = channel_history[
        (channel_history['username'].str.contains('Aplicación de Eventualidades')) &
        (channel_history['username'].str.contains('CL'))
    ].reset_index()

    cl_eventualities = cl_eventualities[['ts', 'username', 'text']]

    cl_eventualities['slack_user_id'] = cl_eventualities['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    cl_eventualities['city'] = cl_eventualities['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    cl_eventualities['zone'] = cl_eventualities['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    cl_eventualities['polygon'] = cl_eventualities['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    cl_eventualities['events'] = cl_eventualities['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Satur)")
    cl_eventualities['saturation'] = cl_eventualities['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    cl_eventualities['unassigned'] = cl_eventualities['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora de inicio)")
    cl_eventualities['start_time'] = cl_eventualities['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    cl_eventualities['end_time'] = cl_eventualities['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    cl_eventualities['observations'] = cl_eventualities['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    cl_eventualities_obj = cl_eventualities.select_dtypes(['object'])
    cl_eventualities[cl_eventualities_obj.columns] = cl_eventualities_obj.apply(lambda x: x.str.strip())

    cl_eventualities['city'] = cl_eventualities['city'].str.lower()
    cl_eventualities['city'] = cl_eventualities['city'].apply(lambda x: unidecode(x))

    cl_eventualities['saturation'] = cl_eventualities['saturation'].apply(lambda x: transform_saturation(x))
    cl_eventualities['unassigned'] = cl_eventualities['unassigned'].apply(lambda x: transform_unassigned(x))
    cl_eventualities['start_hour'] = cl_eventualities['start_time'].apply(lambda x: calculate_start_hour(x))
    cl_eventualities['event_duration_minutes'] = cl_eventualities.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    cl_eventualities = cl_eventualities.merge(slack_users, on='slack_user_id', how='left')

    print(cl_eventualities.isna().sum())

    print('Fazendo upload dos logs de eventualidades')
    snow.upload_df(cl_eventualities, 'occ_log_balance_cl_eventualities')
except:
    pass

# Fechamento de Verticais e Pgto
try:
    cl_verticals_payments = channel_history[
        (channel_history['username'].str.contains('verticales y pagos')) &
        (channel_history['username'].str.contains('CL'))
    ].reset_index()

    cl_verticals_payments = cl_verticals_payments[['ts', 'username', 'text']]

    cl_verticals_payments['slack_user_id'] = cl_verticals_payments['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    cl_verticals_payments['city'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    cl_verticals_payments['zone'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Vert*)")
    cl_verticals_payments['closed_vertical'] = cl_verticals_payments['text'].str.extract(r"(?<=Vertical cerrada\*)(.*)(?=\*Pago cerrado)") 
    cl_verticals_payments['closed_payments'] = cl_verticals_payments['text'].str.extract(r"(?<=Pago cerrado\*)(.*)(?=\*Sat)")
    cl_verticals_payments['saturation'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    cl_verticals_payments['unassigned'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora inicio)")
    cl_verticals_payments['start_time'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Hora inicio\*)(.*)(?=\*Hora de finalización)")
    cl_verticals_payments['end_time'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    cl_verticals_payments['observations'] = cl_verticals_payments['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    cl_verticals_payments_obj = cl_verticals_payments.select_dtypes(['object'])
    cl_verticals_payments[cl_verticals_payments_obj.columns] = cl_verticals_payments_obj.apply(lambda x: x.str.strip())

    cl_verticals_payments['city'] = cl_verticals_payments['city'].str.lower()
    cl_verticals_payments['city'] = cl_verticals_payments['city'].apply(lambda x: unidecode(x))

    cl_verticals_payments['saturation'] = cl_verticals_payments['saturation'].apply(lambda x: transform_saturation(x))
    cl_verticals_payments['unassigned'] = cl_verticals_payments['unassigned'].apply(lambda x: transform_unassigned(x))
    cl_verticals_payments['start_hour'] = cl_verticals_payments['start_time'].apply(lambda x: calculate_start_hour(x))
    cl_verticals_payments['event_duration_minutes'] = cl_verticals_payments.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    cl_verticals_payments = cl_verticals_payments.merge(slack_users, on='slack_user_id', how='left')

    print(cl_verticals_payments.isna().sum())

    print('Fazendo upload dos logs de vertcais e pgto')
    snow.upload_df(cl_verticals_payments, 'occ_log_balance_cl_verticals_payments')
except:
    pass

# Fechamento de Slots
try:
    cl_closed_slots = channel_history[
        (channel_history['username'].str.contains('Cierre de tiendas y Slots')) &
        (channel_history['username'].str.contains('CL'))
    ].reset_index()

    cl_closed_slots = cl_closed_slots[['ts', 'username', 'text']]

    cl_closed_slots['slack_user_id'] = cl_closed_slots['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    cl_closed_slots['city'] = cl_closed_slots['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    cl_closed_slots['zone'] = cl_closed_slots['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Slot cerrado y Stores*)")
    cl_closed_slots['closed_vertical_slot'] = cl_closed_slots['text'].str.extract(r"(?<=Slot cerrado y Stores\*)(.*)(?=\*Sat)")
    cl_closed_slots['saturation'] = cl_closed_slots['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    cl_closed_slots['unassigned'] = cl_closed_slots['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Store ID)")
    cl_closed_slots['store_id'] = cl_closed_slots['text'].str.extract(r"(?<=ID\*)(.*)(?=\*Hora de inicio)")
    cl_closed_slots['start_time'] = cl_closed_slots['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    cl_closed_slots['end_time'] = cl_closed_slots['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    cl_closed_slots['observations'] = cl_closed_slots['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    cl_closed_slots_obj = cl_closed_slots.select_dtypes(['object'])
    cl_closed_slots[cl_closed_slots_obj.columns] = cl_closed_slots_obj.apply(lambda x: x.str.strip())

    cl_closed_slots['city'] = cl_closed_slots['city'].str.lower()
    cl_closed_slots['city'] = cl_closed_slots['city'].apply(lambda x: unidecode(x))

    cl_closed_slots['saturation'] = cl_closed_slots['saturation'].apply(lambda x: transform_saturation(x))
    cl_closed_slots['unassigned'] = cl_closed_slots['unassigned'].apply(lambda x: transform_unassigned(x))
    cl_closed_slots['start_hour'] = cl_closed_slots['start_time'].apply(lambda x: calculate_start_hour(x))
    cl_closed_slots['event_duration_minutes'] = cl_closed_slots.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    cl_closed_slots = cl_closed_slots.merge(slack_users, on='slack_user_id', how='left')

    print(cl_closed_slots.isna().sum())

    print('Fazendo upload dos logs de fechamento de slots')
    snow.upload_df(cl_closed_slots, 'occ_log_balance_cl_closed_slots')
except:
    pass

# Retirada de Acoes
try:
    cl_retirada_acoes = channel_history[
        (channel_history['username'].str.contains('Acciones eliminadas')) &
        (channel_history['username'].str.contains('CL'))
    ].reset_index()

    cl_retirada_acoes = cl_retirada_acoes[['ts', 'username', 'text']]

    cl_retirada_acoes['slack_user_id'] = cl_retirada_acoes['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    cl_retirada_acoes['city'] = cl_retirada_acoes['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    cl_retirada_acoes['zone'] = cl_retirada_acoes['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    cl_retirada_acoes['polygon'] = cl_retirada_acoes['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    cl_retirada_acoes['events'] = cl_retirada_acoes['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Vert)")
    cl_retirada_acoes['vertical'] = cl_retirada_acoes['text'].str.extract(r"(?<=Vertical\*)(.*)(?=\*Método)")
    cl_retirada_acoes['payment_method'] = cl_retirada_acoes['text'].str.extract(r"(?<=Pago\*)(.*)(?=\* Hora)")
    cl_retirada_acoes['start_time'] = cl_retirada_acoes['text'].str.extract(r"(?<=\* Hora de inicio\*)(.*)(?=\*Fecha)")
    cl_retirada_acoes['observations'] = cl_retirada_acoes['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    cl_retirada_acoes_obj = cl_retirada_acoes.select_dtypes(['object'])
    cl_retirada_acoes[cl_retirada_acoes_obj.columns] = cl_retirada_acoes_obj.apply(lambda x: x.str.strip())

    cl_retirada_acoes['city'] = cl_retirada_acoes['city'].str.lower()
    cl_retirada_acoes['city'] = cl_retirada_acoes['city'].apply(lambda x: unidecode(x))

    cl_retirada_acoes['start_hour'] = cl_retirada_acoes['start_time'].apply(lambda x: calculate_start_hour(x))

    cl_retirada_acoes = cl_retirada_acoes.merge(slack_users, on='slack_user_id', how='left')

    print(cl_retirada_acoes.isna().sum())

    print('Fazendo upload dos logs de retirada de acoes')
    snow.upload_df(cl_retirada_acoes, 'occ_log_balance_cl_retirada_acoes')
except:
    pass

# Push / SMS
try:
    cl_push_sms = channel_history[
        (channel_history['username'].str.contains('Push')) &
        (channel_history['username'].str.contains('CL'))
    ].reset_index()

    cl_push_sms = cl_push_sms[['ts', 'username', 'text']]

    cl_push_sms['slack_user_id'] = cl_push_sms['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    cl_push_sms['city'] = cl_push_sms['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    cl_push_sms['zone'] = cl_push_sms['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*PUSH*)")
    cl_push_sms['push_sms'] = cl_push_sms['text'].str.extract(r"(?<=\*PUSH \/ SMS\*)(.*)(?=\*Hora de inicio)")
    cl_push_sms['start_time'] = cl_push_sms['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Fecha)")
    cl_push_sms['observations'] = cl_push_sms['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    cl_push_sms_obj = cl_push_sms.select_dtypes(['object'])
    cl_push_sms[cl_push_sms_obj.columns] = cl_push_sms_obj.apply(lambda x: x.str.strip())

    cl_push_sms['city'] = cl_push_sms['city'].str.lower()
    cl_push_sms['city'] = cl_push_sms['city'].astype(str)
    cl_push_sms['city'] = cl_push_sms['city'].apply(unidecode)

    cl_push_sms['start_hour'] = cl_push_sms['start_time'].apply(lambda x: calculate_start_hour(x))

    cl_push_sms = cl_push_sms.merge(slack_users, on='slack_user_id', how='left')

    print(cl_push_sms.isna().sum())

    print('Fazendo upload dos logs de push')
    snow.upload_df(cl_push_sms, 'occ_log_balance_cl_push_sms')
except:
    pass



