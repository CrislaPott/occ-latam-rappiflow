import os, json
import numpy as np 
import pandas as pd
import slack_sdk as slack
from time import sleep
from dotenv import load_dotenv
from datetime import datetime, timedelta

from lib import snowflake as snow 

load_dotenv()
client = slack.WebClient(token=os.getenv('SLACK_TOKEN'))
members_per_page = 1000
max_members = 50000

page = 1
print("Retrieving page {}".format(page))
response = client.users_list(limit=members_per_page)
assert response["ok"]
members_all = response['members']

while len(members_all) + members_per_page <= max_members:
    page += 1
    print("Retrieving page {}".format(page))
    sleep(1)   # need to wait 1 sec before next call due to rate limits
    response = client.users_list(
        limit=members_per_page,
        cursor=response['response_metadata']['next_cursor']
    )
    assert response["ok"]
    members = response['members']
    members_all = members_all + members

print(
    "Fetched a total of {} members".format(
        len(members_all)
))

slack_users = pd.DataFrame(members_all)
slack_users = slack_users.drop_duplicates(subset='id')

slack_users = slack_users[['id', 'name']]
slack_users.rename(columns={
    'id': 'slack_user_id',
    'name': 'slack_user'
}, inplace=True)

print('Usuarios registrados: {}'.format(slack_users.shape[0]))

slack_users.to_csv('input/slack_users.csv', index=False)

try:
    snow.truncate_table('occ_slack_users_db')
except:
    pass

snow.upload_df(slack_users, 'occ_slack_users_db')