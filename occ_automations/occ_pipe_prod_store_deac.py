import os, re, sys, json, requests
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

sheet = gsheets.read_sheet('1ZcPe-jZ9Fa7Ra0aikt4GyEhvouPrCy9KE2m_Ltj5_kg', 'Erro de catálogo')
sheet = sheet[['Deactivation/Activation date', 'País/Country', 'Vertical', 'Store_id', 'Product_id', 'Thread link']]

sheet.rename(columns={
    'Deactivation/Activation date': 'deactivation_date',
    'País/Country': 'country',
    'Vertical': 'vertical',
    'Store_id': 'store_id',
    'Product_id': 'product_id',
    'Thread link': 'thread_link'
}, inplace=True)

print(sheet.shape[0])

sheet['store_id'] = sheet['store_id'].str.split('\n')
sheet = sheet.explode('store_id').reset_index()
del sheet['index']

sheet['product_id'] = sheet['product_id'].str.split('\n')
sheet = sheet.explode('product_id').reset_index()
del sheet['index']

for column in sheet.columns.tolist():
    sheet[column] = sheet[column].str.strip()

sheet['deactivation_date'] = pd.to_datetime(sheet['deactivation_date'], dayfirst=True, errors='coerce').dt.date 

print(sheet.shape[0])

try:
    snow.truncate_table('occ_action_logs_product_store_deactivations')
except:
    pass

snow.upload_df(sheet, 'occ_action_logs_product_store_deactivations')