import os, sys, json, requests
import numpy as np
import pandas as pd
from datetime import datetime
from lib import gsheets, slack
from lib import snowflake as snow
from os.path import expanduser
from airflow.models import Variable

stores = gsheets.read_sheet('1PD3g0_9thCAWQxHfpKRZlpJimudyL1FGw8lPYAGArgU', 'BR!A:B')
physical_store_ids = stores['physical_store_id'].unique().tolist()

def counting_slots(physical_store_id):
    query = """
    with slots_table as (
        select
            id as slot_id,
            physical_store_id,
            dateadd('hour', -3, datetime_utc_iso)::date as day,
            dateadd('hour', -3, datetime_utc_iso) as open_time,
            extract(hour from dateadd('hour', -3, datetime_utc_iso)) as open_hour
        from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
        where
            type = 'SHOPPER'
            and (datetime_utc_iso - interval '3 hours')::date >= (convert_timezone('UTC',current_timestamp) - interval '3 hour')::date
            and (datetime_utc_iso - interval '3 hours')::date <= dateadd('day', 21, (convert_timezone('UTC',current_timestamp) - interval '3 hour'))::date
            and physical_store_id in ({})
            and deleted_at is null
    ),

    ditches as (
        select distinct
            slot_id,
            last_value(status) over (partition by slot_id order by created_at) as status,
            last_value(action_status) over (partition by slot_id order by created_at) as action_status
        from BR_WRITABLE.occ_valas_br
    )


    select
        st.day,
        st.physical_store_id,
        count(distinct st.slot_id) as slots_qty
    from slots_table st
    left join ditches d on st.slot_id = d.slot_id
    where d.slot_id is null
    group by 1, 2
    order by 1, 2 desc
    """.format(physical_store_id)

    results = snow.run_query(query)

    d0_date = str(results.loc[0, 'day'])
    d3_date = str(results.loc[3, 'day'])
    d14_date = str(results.loc[14, 'day'])

    return d0_date, d3_date, d14_date

def get_slots(df, physical_store_id, target_date):
    query = """
    with slots_table as (
        select
            id as slot_id,
            physical_store_id,
            dayname(dateadd('hour', -3, datetime_utc_iso)::date) as dayname,
            dateadd('hour', -3, datetime_utc_iso) as open_time,
            extract(hour from dateadd('hour', -3, datetime_utc_iso)) as open_hour
        from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
        where
            type = 'SHOPPER'
            and (datetime_utc_iso - interval '3 hours')::date = '{target_date}'
            and physical_store_id in ({physical_store_id})
    ),

    ditches as (
        select distinct
            slot_id,
            last_value(status) over (partition by slot_id order by created_at) as status,
            last_value(action_status) over (partition by slot_id order by created_at) as action_status
        from BR_WRITABLE.occ_valas_br
    )


    select st.* from slots_table st
    left join ditches d on st.slot_id = d.slot_id
    where d.slot_id is null
    order by open_time asc
    """.format(physical_store_id=physical_store_id, target_date=target_date)

    slots = snow.run_query(query)

    df = pd.concat([df, slots], ignore_index=True)

    return df

def change_slot_status(slot_id, status, iteration=0):
    slot_id = int(slot_id)

    """Change the slots status."""
    headers = {
        'Authorization': 'postman',
        'API_KEY': Variable.get('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
    }
    data = {
        "is_closed": not status
    }
    response = requests.put(
        'http://services.rappi.com.br/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
        headers=headers,
        data=json.dumps(data)
    )
    
    if response.status_code == 200:
        return 'Sim' 
    else:
        return 'Nao'

d3_slots = pd.DataFrame()
d14_slots = pd.DataFrame()

for physical_store_id in physical_store_ids:
    try:
        d0_date, d3_date, d14_date = counting_slots(physical_store_id)
        
        current_date = datetime.today().strftime('%Y-%m-%d')

        if d0_date == current_date:
            d3_slots = get_slots(d3_slots, physical_store_id, d3_date)
            d14_slots = get_slots(d14_slots, physical_store_id, d14_date)
    except:
        print(f'Erro na loja {physical_store_id}')
        continue

d3_slots['type'] = 'D3'
d14_slots['type'] = 'D14'

d3_slots['bot_taken_action'] = d3_slots['slot_id'].apply(lambda x: change_slot_status(x, True, iteration=0))
d14_slots['bot_taken_action'] = d14_slots['slot_id'].apply(lambda x: change_slot_status(x, False, iteration=0))

d3_final = pd.concat([d3_slots, d14_slots], ignore_index=True)

snow.upload_df(d3_final, 'occ_logs_processo_d3')

home = expanduser("~")
d3_file = '{}/occ_processo_d3_br_{}.csv'.format(home, datetime.now().strftime("%d_%m_%Y"))
# d3_file = 'occ_processo_d3_br_{}.csv'.format(datetime.now().strftime("%d_%m_%Y"))
d3_final.to_csv(d3_file, index=False)

texto = 'Realizado o processo D3 diário em BR. Segue anexo listagem de slots impactados pelo processo: '
slack.file_upload_channel(channel='C01JFP7LQ8N', text=texto, filename=d3_file, filetype='csv')

if os.path.exists(d3_file):
    os.remove(d3_file)