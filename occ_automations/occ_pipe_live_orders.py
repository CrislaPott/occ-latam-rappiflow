import os, re, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode
from lib import redash 
from lib import snowflake as snow
from lib import thread_listener as tl

query = f'''--no_cache
select 
	o.id as order_id,
	storekeeper_id,
	deliveryboy_id,
	cooking_time,
	cooking_time_started_at,
	place_at,
	o2.store_id,
	o.state,
	o.created_at
from
	orders o
left join
	calculated_information.orders o2 on o2.order_id = o.id
	join order_stores os on os.order_id=o.id and os.type not ilike '%flow%' and os.store_type_group not ilike '%cargo%'
where
	o.state in ('created','not_taked','in_store','in_progress','on_the_route','arrive','expediter_taken','pending_to_be_assigned' ,'pending_storekeeper_confirmation')
	and date(o.created_at) = date(now() at time zone 'America/Sao_Paulo')'''

df = redash.run_query(1338, query)


try:
	snow.truncate_table('occ_live_orders')
except:
	pass

snow.upload_df(df, 'occ_live_orders')

