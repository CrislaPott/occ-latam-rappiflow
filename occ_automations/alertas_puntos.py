import os, sys, json
import numpy as np
import pandas as pd
from lib import gsheets, redash, slack
from lib import snowflake as snow
from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps

owm = OWM(os.getenv('OWM_API_KEY'))
mgr = owm.weather_manager()

def get_conditionals(country):
    try:
        return gsheets.read_sheet('1v4vnJpGu3dE6aseGdgPrf2C7hpsCV30Dc_gwao0mGBY', '{}!A1:G'.format(country))
    except:
        print('Erro na leitura da planilha com condicionais')
        
def get_co_current_points():
    query = """
    WITH adjustments_ranked AS (
        /*  Create a table ranked for most recent modifications to zones and vehicles */
        SELECT
            'CO' AS country_code
            ,zone_id
            ,split_part(zone_id, '|', '3') AS zone
            ,city
            ,restriction_id
            ,vehicle
            ,created_at
            ,DAYOFWEEK(created_at::DATE) AS dow
            ,adjust_points
            ,required_points
            ,event
            ,event_timer
            ,weather
            ,weather_timer
            ,CASE
                WHEN restriction_id IN (30,32,40,38) THEN 'Morning'
                WHEN restriction_id IN (55,54,52,53) THEN 'Lunch'
                WHEN restriction_id IN (26,28,41,39) THEN 'Afternoon'
                WHEN restriction_id IN (49,50,48,51) THEN 'Dinner'
                ELSE 'Error'
            END AS time_of_day
            ,RANK() OVER (PARTITION BY zone_id, vehicle ORDER BY created_at DESC) AS mod_rank

        FROM OPS_GLOBAL.CO_zone_points_adjusted_ops
        WHERE created_at::DATE >= DATEADD(DAY, -2, CURRENT_DATE())
            AND zone IN ('suba_y_colina', 'la_candelaria', 'usaquen_y_cedritos', 'fontibon', 'salitre', 'antonio_narino_y_sur', 'engativa', 'chapinero', 'kennedy', 'norte', 'sur', 'ingenio_limonar', 'lido_plametto', 'oeste', 'jamudi', 'nogales_ciudad_jardin_baq', 'villa_carolina_y_paraiso_baq1', 'villa_santos_y_miramar_baq', 'riomar_y_altoprado_baq1', 'prado_baq2', 'poblado_centro', 'itagui', 'sabaneta/la_estrella', 'la_estrella', 'belen_laureles', 'envigado', 'alto_de_las_palmas')
    )

    /* Get the current configuration of each zone and vehicle, with it's latest modification timestamp */
    SELECT
        po.country_code
        ,po.city
        ,po.zone
        ,po.time_of_day
        ,CASE
            WHEN po.vehicle = 0 THEN 'Caminante'
            WHEN po.vehicle = 1 THEN 'Bici'
            WHEN po.vehicle = 2 THEN 'Moto'
            WHEN po.vehicle = 3 THEN 'Carro'
            ELSE 'Error'
        END AS vehicle

        ,po.created_at
        ,po.restriction_id
         ,po.adjust_points AS current_zone_points
        ,po.adjust_points / th.max_score AS normalized_points
        ,th.max_score AS zone_max_points

    FROM adjustments_ranked po
        LEFT JOIN co_writable.global_points_thresholds th ON po.country_code = th.country
            AND th.zone_id = po.zone_id AND po.restriction_id = th.restriction_id
            AND po.vehicle = th.vehicle
            AND po.dow = th.day_of_week
            AND DATE_TRUNC('WEEK', po.created_at::TIMESTAMP) = th.week_valid

    WHERE mod_rank = 1       -- Restrict to get just latest modification which means current state of things
        AND created_at::DATE = CURRENT_DATE
        AND city IN ('bogota','medellin','cali','barranquilla')

    ORDER BY country, city, zone, vehicle, created_at DESC
    """
    
    try:
        current_points = snow.run_query(query)
        return current_points
    except:
        print('Erro ao rodar query do Snowflake')
        
def get_br_current_points():
    query = """
    WITH adjustments_ranked AS (
        /*  Create a table ranked for most recent modifications to zones and vehicles */
        SELECT
            'BR' AS country_code
            ,zone_id
            ,split_part(zone_id, '|', '3') AS zone
            ,city
            ,restriction_id
            ,vehicle
            ,created_at
            ,DAYOFWEEK(created_at::DATE) AS dow
            ,adjust_points
            ,required_points
            ,event
            ,event_timer
            ,weather
            ,weather_timer
            ,CASE
                WHEN restriction_id IN (46) THEN 'Morning'
                WHEN restriction_id IN (62) THEN 'Lunch'
                WHEN restriction_id IN (47) THEN 'Afternoon'
                WHEN restriction_id IN (57) THEN 'Dinner'
                ELSE 'Error'
            END AS time_of_day
            ,RANK() OVER (PARTITION BY zone_id, vehicle ORDER BY created_at DESC) AS mod_rank
        FROM OPS_GLOBAL.BR_zone_points_adjusted_ops
        WHERE created_at::DATE >= DATEADD(DAY, -2, CURRENT_DATE())
            AND zone IN ('rec_zona_norte/nordeste','rec_zona_sul')
    )
    /* Get the current configuration of each zone and vehicle, with it's latest modification timestamp */
    SELECT
        po.country_code
        ,po.city
        ,po.zone
        ,po.time_of_day
        ,CASE
            WHEN po.vehicle = 0 THEN 'Caminante'
            WHEN po.vehicle = 1 THEN 'Bici'
            WHEN po.vehicle = 2 THEN 'Moto'
            WHEN po.vehicle = 3 THEN 'Carro'
            ELSE 'Error'
        END AS vehicle
        ,po.adjust_points AS current_zone_points
        ,po.created_at
        ,po.restriction_id
        ,th.max_score AS zone_max_points
        ,po.adjust_points / th.max_score AS normalized_points
    FROM adjustments_ranked po
        LEFT JOIN co_writable.global_points_thresholds th ON po.country_code = th.country
            AND th.zone_id = po.zone_id AND po.restriction_id = th.restriction_id
            AND po.vehicle = th.vehicle
            AND po.dow = th.day_of_week
            AND DATE_TRUNC('WEEK', po.created_at::TIMESTAMP) = th.week_valid
    WHERE mod_rank = 1       -- Restrict to get just latest modification which means current state of things
        AND created_at::DATE = CURRENT_DATE
        AND city IN ('recife')
    ORDER BY country, city, zone, vehicle, created_at DESC
    """
    
    try:
        current_points = snow.run_query(query)
        return current_points
    except:
        print('Erro ao rodar query do Snowflake')
        
def run_metrics(country, zones):
    zones = ["{}".format(zone) for zone in zones]
    zones = ','.join(zones)
    
    query = """
    with data as (
        select 
            zone_id, 
            max(created_at) as last 
        from zone_historic 
        where (created_at - interval '3 hours')::date = now()::date 
        and zone_id in ({zones})
        group by 1
    )

    select 
        zh.zone_id, 
        couriers, 
        orders, 
        saturation, 
        free_couriers,
        unassigned_orders,(unassigned_orders::float/NULLIF(orders,0)::float)*100.0 as unn_perc,
        (unassigned_orders::float/NULLIF(free_couriers,0)::float) as unn_free_relation,
        (created_at - interval '3 hours') as created_at, 
        grability_city_id
    from zone_historic zh
    inner join data d on d.last=zh.created_at and d.zone_id=zh.zone_id
    where (created_at - interval '3 hours')::date = (now() at time zone 'America/Sao_Paulo')::date
    """.format(zones=zones)
    
    try:
        if country == 'Brasil':
            metrics = redash.run_query(2646, query)
            return metrics 
        elif country == 'Colombia':
            metrics = redash.run_query(2645, query)
            return metrics
    except:
        print('Erro ao rodar query de metricas no Redash')
        
def run_alarm(conditionals, points, metrics, country):
    total = points.merge(metrics, on='zone_id')
    total = total.merge(conditionals, on=['zone_id', 'zone'])
    total = total[['zone', 'zone_id', 'free_couriers', 'unassigned_orders', 'unn_perc', 'unn_free_relation', 'time_of_day', 'vehicle', 'current_zone_points', 'zone_max_points', 'normalized_points', 'reduction_points', 'unn_threshold', 'lat', 'lon']]
    
    for index, row in total.iterrows():
        low_unn, mid_unn, high_unn = row['unn_threshold'].split('-')
        low_norm, mid_norm, high_norm = row['reduction_points'].split('-')
        
        unn = round(row['unn_perc'], 3)
        unn_free_relation = round(row['unn_free_relation'], 3)
        norm = row['normalized_points']
        
        print('Zona: {zone}, Unassigned: {unn}, Unassigned/Free Couriers Relation: {unn_free_relation} Norm: {norm}'.format(zone=row['zone'], unn=unn, unn_free_relation=unn_free_relation, norm=norm))
        
        low_unn = int(low_unn)
        mid_unn = int(mid_unn)
        high_unn = int(high_unn)
        
        try:
            lat = float(row['lat'].replace(',', '.'))
            lon = float(row['lon'].replace(',', '.'))
            weather_conditions = mgr.weather_at_coords(lat=lat, lon=lon).weather
            weather_status = weather_conditions.detailed_status
            
            if norm > 0 and 'rain' in weather_status and (row['free_couriers'] <= row['unassigned_orders'] * 0.3):
                rain_1h = weather_conditions.rain['1h']
                
                trigger_text = """
                *Alerta del programa de puntos por Lluvia*
                *Pais*: {country}
                *Zona*: {zone}
                *ID Zona*: {zone_id}
                *Operación*: {vehicle}
                *Normalización actual*: {norm}
                *No Asignados/RTs Libres*: {unn_free_relation}
                *Condición meteorológica actual*: {weather_status}
                *Lluvia en mm en la última hora*: {rain_1h}
                """.format(country=country, zone=row['zone'], zone_id=row['zone_id'], vehicle=row['vehicle'], norm=norm, unn_free_relation=unn_free_relation, weather_status=weather_status, rain_1h=rain_1h)
                
                slack.send_message_channel('C01G255R4SY', trigger_text)
                continue
        except:
            pass
        
        if (row['free_couriers'] <= row['unassigned_orders']* 0.3) and norm > 0:
            if low_unn <= unn < mid_unn:
                if norm > (1 - (float(low_norm) / 100)):
                    trigger_alarm(row['zone'], row['zone_id'], row['unn_perc'], unn_free_relation, row['vehicle'], low_norm, norm, country)
            elif mid_unn <= unn < high_unn:
                if norm > (1 - (float(mid_norm) / 100)):
                    trigger_alarm(row['zone'], row['zone_id'], row['unn_perc'], unn_free_relation, row['vehicle'], mid_norm, norm, country)
            elif unn >= high_unn:
                if norm > 0:
                    trigger_alarm(row['zone'], row['zone_id'], row['unn_perc'], unn_free_relation, row['vehicle'], high_norm, norm, country)
                
def trigger_alarm(zone, zone_id, unn, unn_free_relation, vehicle, reduction, norm, country):
    trigger_text = """
    *Alerta del programa de puntos por No Asignados*
    
    *Pais*: {country}
    *Zona*: {zone}
    *ID Zona*: {zone_id}
    *Operación*: {vehicle}
    *No Asignados - %*: {unn}
    *No Asignados/RTs Libres*: {unn_free_relation}
    *Normalización actual*: {norm}
    *Reducción esperada - %*: {reduction}
    """.format(zone=zone, zone_id=zone_id, unn=unn, unn_free_relation=unn_free_relation, vehicle=vehicle, reduction=reduction, norm=norm, country=country)
    
    slack.send_message_channel('C01G255R4SY', trigger_text)
        
def run_co():
    conditionals = get_conditionals('Colombia')
    points = get_co_current_points()
    
    mappings = {
        'chapinero': 4,
        'usaquen_y_cedritos': 7,
        'la_candelaria': 11,
        'suba_y_colina': 6,
        'fontibon': 10,
        'salitre': 5,
        'antonio_narino_y_sur': 8,
        'engativa': 9,
        'kennedy': 3,
        'poblado_centro': 17,
        'envigado': 22,
        'belen_laureles': 16,
        'itagui': 21,
        'sabaneta/la_estrella': 18,
        'sur': 30,
        'ingenio_limonar': 25,
        'lido_plametto': 29,
        'oeste': 27,
        'norte': 28,
        'riomar_y_altoprado_baq1': 55,
        'nogales_ciudad_jardin_baq': 64,
        'villa_carolina_y_paraiso_baq1': 57,
        'prado_baq2': 58,
        'villa_santos_y_miramar_baq': 63,
        'via_40_baq': 56,
        'barrio_abajo_baq2': 59,
        'los_pinos_baq': 65,
        'soledad_centro': 66,
        'villa_campestre_baq': 62
    }
    
    if points.shape[0] == 0:
        print('Sem pontos rodando - CO')
        return 0
    
    points['zone_id'] = points['zone'].map(mappings)
    conditionals['zone_id'] = conditionals['zone'].map(mappings)
    zones = points['zone_id'].unique().tolist()
    
    metrics = run_metrics('Colombia', list(mappings.values()))
    
    run_alarm(conditionals, points, metrics, 'Colombia')
    
def run_br():
    conditionals = get_conditionals('Brasil')
    points = get_br_current_points()
    
    mappings = {
        'rec_zona_norte/nordeste': 29,
        'rec_zona_sul': 35
    }
    
    if points.shape[0] == 0:
        print('Sem pontos rodando - BR')
        return 0
    
    points['zone_id'] = points['zone'].map(mappings)
    conditionals['zone_id'] = conditionals['zone'].map(mappings)
    zones = points['zone_id'].unique().tolist()
    
    metrics = run_metrics('Brasil', zones)
    
    run_alarm(conditionals, points, metrics, 'Brasil')
    
run_br()
run_co()