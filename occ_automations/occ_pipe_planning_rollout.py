import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

today = arrow.utcnow().to('America/Recife')
seven_days_ago = today.shift(days=-7)

today = today.date()
seven_days_ago = seven_days_ago.date()

demandas = gsheets.read_sheet('1i-s4S-PE5v1Q5ZuF-mVaIvVw_JhhDFYeX0Li5zaXw3U', 'Requests')
demandas = demandas[['Descrição', 'Area', 'Owner', 'Responsável BI', 'DATA DE INICIO', 'Comment', 'Last Status']]
demandas.rename(columns={
    'DATA DE INICIO': 'Data de Inicio'
}, inplace=True)

demandas['Data de Inicio'] = pd.to_datetime(demandas['Data de Inicio'], dayfirst=True, errors='coerce').dt.date 
demandas = demandas[
    (demandas['Data de Inicio'].notna()) &
    (demandas['Data de Inicio'] >= seven_days_ago) &
    (demandas['Data de Inicio'] <= today)
]

demandas.sort_values(by='Data de Inicio', ascending=True, inplace=True)

try:
    snow.truncate_table('occ_planning_rollout')
except:
    pass

snow.upload_df(demandas, 'occ_planning_rollout')