import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash 
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

# (AR) - [PayPlatform] NovoPayment Dispersion Error
query = """
--no_cache
with a as (select created_at, meta, status_id from ar_pglr_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Buenos_Aires', current_timestamp()))::timestamp_ntz and _fivetran_deleted=false)

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'novopayment'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table('occ_disp_ar_payplatform_novopayment_error')
except:
    pass

snow.upload_df(results, 'occ_disp_ar_payplatform_novopayment_error')

# (AR) - [PayPlatform] Edenred Dispersion Error
query = """
--no_cache
with a as (select created_at, meta, status_id from ar_pglr_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Buenos_Aires', current_timestamp()))::timestamp_ntz and _fivetran_deleted=false)

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'edenredrest'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table('occ_disp_ar_payplatform_edenred_error')
except:
    pass

snow.upload_df(results, 'occ_disp_ar_payplatform_edenred_error')

# (PE)) - [PayPlatform] NovoPayment Dispersion Error

query = """
--no_cache
with a as (select created_at, meta, status_id from pe_pglr_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -3, convert_timezone('America/Bogota', current_timestamp()))::timestamp_ntz and _fivetran_deleted=false)

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'novopayment'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table('occ_disp_pe_payplatform_novopayment_error')
except:
    pass

snow.upload_df(results, 'occ_disp_pe_payplatform_novopayment_error')

texto = "(PE) - [PayPlatform] Edenred Dispersion Error"

textos_parseados = re.findall("[\w-]*\p{L}[\w-]*", texto)
textos_parseados = [texto.lower().strip() for texto in textos_parseados]

texto_final = '_'.join(textos_parseados)
texto_final = 'occ_disp_' + texto_final

print(texto_final)


query = """
--no_cache
with a as (select created_at, meta, status_id from pe_pglr_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -3, convert_timezone('America/Bogota', current_timestamp()))::timestamp_ntz and _fivetran_deleted=false)

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'edenredrest'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table(texto_final)
except:
    pass

snow.upload_df(results, texto_final)

texto = "(EC) - [PayPlatform] NovoPayment Dispersion Error"

textos_parseados = re.findall("[\w-]*\p{L}[\w-]*", texto)
textos_parseados = [texto.lower().strip() for texto in textos_parseados]

texto_final = '_'.join(textos_parseados)
texto_final = 'occ_disp_' + texto_final

print(texto_final)


query = """
--no_cache
with a as (select created_at, meta, status_id from ec_pg_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Bogota', current_timestamp()))::timestamp_ntz )

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'novopayment'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table(texto_final)
except:
    pass

snow.upload_df(results, texto_final)

texto = "(EC) - [PayPlatform] Edenred Dispersion Error"

textos_parseados = re.findall("[\w-]*\p{L}[\w-]*", texto)
textos_parseados = [texto.lower().strip() for texto in textos_parseados]

texto_final = '_'.join(textos_parseados)
texto_final = 'occ_disp_' + texto_final

print(texto_final)


query = """
--no_cache
with a as (select created_at, meta, status_id from ec_pg_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Bogota', current_timestamp()))::timestamp_ntz )

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'edenred'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table(texto_final)
except:
    pass

snow.upload_df(results, texto_final)

texto = "(CR) - [PayPlatform] NovoPayment Dispersion Error"

textos_parseados = re.findall("[\w-]*\p{L}[\w-]*", texto)
textos_parseados = [texto.lower().strip() for texto in textos_parseados]

texto_final = '_'.join(textos_parseados)
texto_final = 'occ_disp_' + texto_final

print(texto_final)


query = """
--no_cache
with a as (select created_at, meta, status_id from cr_pg_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Costa_Rica', current_timestamp()))::timestamp_ntz )

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'novopayment'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table(texto_final)
except:
    pass

snow.upload_df(results, texto_final)

texto = "(CR) - [PayPlatform] Edenred Dispersion Error"

textos_parseados = re.findall("[\w-]*\p{L}[\w-]*", texto)
textos_parseados = [texto.lower().strip() for texto in textos_parseados]

texto_final = '_'.join(textos_parseados)
texto_final = 'occ_disp_' + texto_final

print(texto_final)


query = """
--no_cache
with a as (select created_at, meta, status_id from cr_pg_ms_dispersion_public.transactions where created_at::timestamp_ntz >= dateadd(hour, -6, convert_timezone('America/Costa_Rica', current_timestamp()))::timestamp_ntz )

SELECT
    status_id,
    case when extract(minute from created_at) between 0 and 9 then dateadd(minute, 0, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 10 and 19 then dateadd(minute, 10, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 20 and 29 then dateadd(minute, 20, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 30 and 39 then dateadd(minute, 30, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 40 and 49 then dateadd(minute, 40, date_trunc(hour, created_at)) 
    	 when extract(minute from created_at) between 50 and 59 then dateadd(minute, 50, date_trunc(hour, created_at)) 
    end as time,
    count(*)
FROM a
where
    get_path(meta, 'gateway') = 'edenred'
    and get_path(meta, 'action') = 'disperse'
group by
    1,2
order by
  2 asc
"""

results = snow.run_query(query)

try:
    snow.truncate_table(texto_final)
except:
    pass

snow.upload_df(results, texto_final)