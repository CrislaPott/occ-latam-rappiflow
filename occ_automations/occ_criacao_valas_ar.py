import os, sys, json, requests
import numpy as np
import pandas as pd
import pytz
from datetime import datetime
from lib import gsheets, slack, sfx
from lib import snowflake as snow
from os.path import expanduser
from airflow.models import Variable

query_close = """
    with

        slots_table as (
            select
                physical_store_id,
                id as slot_id,
                dateadd('hour', -3, datetime_utc_iso) as open_time
            from
                AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
            where
                dateadd('hour', -3, datetime_utc_iso)::date > (convert_timezone('America/Buenos_Aires',current_timestamp))::date
                and type = 'SHOPPER'
                and deleted_at is null
                and dateadd('hour', -3, datetime_utc_iso)::date <= dateadd(day, +7,convert_timezone('America/Buenos_Aires',current_timestamp))::date
        )

    select
        to_char(current_date::date, 'YYYY-mm-DD') as today,
        physical_store_id,
        st.slot_id,
        to_char(open_time, 'HH:00:00') as open_time,
        dayname(open_time) as open_day
    from slots_table st
"""

cols = [
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
]

"""Read a spreadsheet and create the ditches in the near future. Log them in DB and google sheets."""
print('initializing...')
closed_slots = []

df = gsheets.read_sheet('1Q3Jo25Y7R-GyijT1t5C3WdEv7oCsX8SN6yZhV41lZyE', 'Lojas!A:I')
# df = pd.DataFrame.from_records(slots[1:], columns=slots[0])
df = df.fillna('')
df = df.melt(
    id_vars=['Physical Store Id', 'Store Name'],
    var_name='open_day',
    value_name='open_time'
)

df['open_time'] = df.apply(lambda x: x['open_time'].strip(), axis=1)
df['Physical Store Id'] = df.apply(lambda x: int(x['Physical Store Id']), axis=1)

# FUTURE SLOTS
available_slots = snow.run_query(query_close)

# EXCEPTION SLOTS
exceptions = gsheets.read_sheet('1HVBrwmU6AzEvGhs7txtIGewCgTgwksbJF6oECtuX4-s', 'Exceções!A:B')
exceptions['Slot id'] = exceptions.apply(lambda x: int(x['Slot id']), axis=1)

print('calculating slots to close...')

slots_to_close = available_slots.merge(
    df,
    how='left',
    left_on=['physical_store_id', 'open_day', 'open_time'],
    right_on=['Physical Store Id', 'open_day', 'open_time']
)
slots_to_close = slots_to_close.merge(
    exceptions,
    how='left',
    left_on=['slot_id'],
    right_on=['Slot id']
)
slots_to_close = slots_to_close[
    ((slots_to_close['Physical Store Id'].notna()) & (slots_to_close['Ação'] != 'Abrir')) |
    (slots_to_close['Ação'] == 'Fechar')
]

print('Slots a ser impactados: {}'.format(slots_to_close.shape[0]))

def change_slot_status(slot_id, status, iteration=0):
    """Change the slots status."""
    headers = {
        'Authorization': 'postman',
        'API_KEY': Variable.get('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
        'x-rappi-team': 'OCC',
        'x-application-id': 'rappiflow-latam'
    }
    data = {
        "is_closed": not status
    }
    response = requests.put(
        'http://services.rappi.com.ar/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
        headers=headers,
        data=json.dumps(data)
    )
    return response.status_code

print('closing slots...')

current_time = datetime.now(pytz.timezone("America/Bogota"))

for index, row in slots_to_close.iterrows():
    # print(row['slot_id'])
    closed_slots.append(
        [
            row['today'],  # today date
            row['physical_store_id'],  # physical store id
            row['open_day'],  # day of the week of slot
            row['open_time'],  # slot opening hour
            row['slot_id'],  # slot_id
            'closed',  # bot_action
            change_slot_status(int(row['slot_id']), False) == 200  # action_status
        ]
    )

    physical_store_id = str(row["physical_store_id"])
    slot_id = str(row["slot_id"])
    country = "AR"
    reason = "slot ditches close"
    closed_at = current_time.strftime("%Y-%m-%d %H:%M:%S")

    sfx.send_events_slotDitchesClose(
        slot_id, physical_store_id, country, reason, closed_at
    )


print('logging...')
# google_sheets.writeSpreadsheet('1HVBrwmU6AzEvGhs7txtIGewCgTgwksbJF6oECtuX4-s', 'Logs!A:G', closed_slots)
df = pd.DataFrame.from_records(closed_slots, columns=['today', 'physical_store_id', 'open_day', 'open_time', 'slot_id', 'bot_action', 'action_status'])
df['slot_id'] = df.apply(lambda x: int(x['slot_id']), axis=1)
df['status'] = df.apply(lambda x: 'closed', axis=1)
df['created_at'] = df.apply(lambda x: datetime.now().strftime('%Y-%m-%d %H:%M:%S'), axis=1)
del df['today']
del df['physical_store_id']
del df['open_day']
del df['open_time']
del df['bot_action']
df = df[['slot_id', 'status', 'created_at', 'action_status']]

print(df.head())
print(df['action_status'].value_counts())

snow.upload_df_occ(df, 'occ_valas_ar')

home = expanduser("~")
results_file = '{}/occ_criacao_valas_ar_{}.xlsx'.format(home, datetime.now().strftime("%d_%m_%Y"))
# results_file = 'occ_criacao_valas_ar_{}.xlsx'.format(datetime.now().strftime("%d_%m_%Y"))

df.to_excel(results_file, index=False)

texto = 'Se crearon valas semanales. A continuación se muestra una lista de valas creadas: '
slack.file_upload_channel(channel='C01HVJF1TRS', text=texto, filename=results_file, filetype='xlsx')

if os.path.exists(results_file):
    os.remove(results_file)


