import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

pd.set_option("display.max_columns", None)

sys.path.insert(0, "../")

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

canal_disparo = 'C01672B528Y'
trigger_name = ':alert: *Alarma Playbook Balance - CL* :alert:'
playbook_sheet = '1qpyPluu90mszDWz2P39S49kFd7-wGJ7Kl6Szh8BFRmI'

def get_url_metrics(country):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/zones/live-ops',
        'co': 'http://services.rappi.com/api/operation-management/zones/live-ops',
        'cl': 'http://services.rappi.cl/api/operation-management/zones/live-ops',
        'pe': 'https://services.rappi.pe/api/operation-management/zones/live-ops',
        'uy': 'https://services.rappi.com.uy/api/operation-management/zones/live-ops',
        'ar': 'http://services.rappi.com.ar/api/operation-management/zones/live-ops',
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/zones/live-ops',
        'ec': 'https://services.rappi.com.ec/api/operation-management/zones/live-ops'
    }

    return mappings[country]

def get_url_events(country, city):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/locations/cities/{}/zones'.format(city),
        'co': 'http://services.rappi.com/api/operation-management/locations/cities/{}/zones'.format(city),
        'cl': 'http://services.rappi.cl/api/operation-management/locations/cities/{}/zones'.format(city),
        'pe': 'https://services.rappi.pe/api/operation-management/locations/cities/{}/zones'.format(city),
        'uy': 'https://services.rappi.com.uy/api/operation-management/locations/cities/{}/zones'.format(city),
        'ar': 'http://services.rappi.com.ar/api/operation-management/locations/cities/{}/zones'.format(city),
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/locations/cities/{}/zones'.format(city),
        'ec': 'https://services.rappi.com.ec/api/operation-management/locations/{}/zones'.format(city)
    }

    return mappings[country]

def transform_nulls(value):
    if value in ['none', 'None', '', None]:
        return np.nan 
    else:
        return value

def get_events(country, city, token=token):
    url = get_url_events(country, city)
    payload = {}
    headers = {
        'Authorization': token
    }

    events = pd.DataFrame()

    response = requests.request("GET", url, headers=headers, data=payload)
    results = response.json()
    
    for result in results:
        zone_name = result['name'].split('|')[2]
        event = result['event']
        raining = result['raining']
        payment_methods = ','.join(list(result['payment_methods'].keys()))

        inserts = [{
            'zone_name': zone_name,
            'event': event,
            'raining': raining,
            'payment_methods': payment_methods
        }]

        inserts = pd.DataFrame(inserts)

        events = pd.concat([events, inserts], ignore_index=True)

    return events

def get_zones_class():
    doc = gsheets.read_sheet(playbook_sheet, 'Classificacion-Zonas')
    doc.rename(columns={
        'Código zona': 'zone_id',
        'Grupo': 'zone_class'
    }, inplace=True)
    doc['zone_id'] = doc['zone_id'].astype(int)

    return doc[['zone_id', 'zone_class']]

def get_metrics(country, token=token):
    url = get_url_metrics(country)
    payload = {}
    headers = {
        'Authorization': token
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    results = response.json()

    metrics = pd.DataFrame()
    
    for result in results:
        city = result['city']
        zone_id = result['id']
        zone_name = result['name']
        orders = result['total_orders']
        rts = result['total_couriers']
        rts_livres = result['health_check']['free_couriers']
        saturation = result['saturation']
        unn_orders = result['orders_unassigned']

        inserts = [{
            'city': city,
            'zone_id': zone_id,
            'zone_name': zone_name,
            'orders': orders,
            'rts': rts,
            'rts_livres': rts_livres,
            'saturation': saturation,
            'unn_orders': unn_orders
        }]
        inserts = pd.DataFrame(inserts)
        metrics = pd.concat([metrics, inserts], ignore_index=True)

    metrics['country'] = country
    metrics = metrics[metrics['orders'] > 0]
    metrics['saturation'] = metrics['orders'] / metrics['rts']
    metrics['unn_perc'] = (metrics['unn_orders'] / metrics['orders'])*100

    cities = metrics['city'].unique().tolist()
    events = pd.DataFrame()

    for city in cities:
        city_events = get_events(country, city)
        events = pd.concat([events, city_events], ignore_index=True)
    
    metrics = metrics.merge(events, on='zone_name', how='left')

    zone_classes = get_zones_class()

    metrics = metrics.merge(zone_classes, on='zone_id', how='left')

    for column in ['event', 'raining', 'payment_methods']:
        metrics[column] = metrics[column].apply(lambda x: transform_nulls(x))

    return metrics 

def get_conditionals(zone_class):
    doc = gsheets.read_sheet(playbook_sheet, zone_class)

    for column in ['unassigned_from', 'unassigned_to']:
        doc[column] = doc[column].str.replace('%', '')
        doc[column] = doc[column].astype(float)

    for column in ['saturation_from', 'saturation_to']:
        doc[column] = doc[column].str.replace(',', '.')
        doc[column] = doc[column].astype(float)

    for column in ['desired_polygon', 'desired_payment_method', 'desired_events']:
        doc[column] = doc[column].apply(lambda x: transform_nulls(x))
        doc[column] = doc[column].str.lower()

    doc['desired_events'] = doc['desired_events'].str.replace('#', '')
    doc['desired_events'] = doc['desired_events'].str.split(',')

    return doc

def match_results(metrics, conditionals):
    metrics['key'] = 0
    conditionals['key'] = 0

    matches = metrics.merge(conditionals, on='key')
    del matches['key']

    matches = matches[
        (matches['unassigned_from'] <= matches['unn_perc']) &
        (matches['unn_perc'] < matches['unassigned_to']) &
        (matches['saturation_from'] <= matches['saturation']) &
        (matches['saturation'] < matches['saturation_to'])
    ]

    matches = matches[
        (matches['unn_orders'] > 5)
    ]

    return matches

def run_alarm(row, canal_disparo=canal_disparo):
    if (
        (str(row['desired_polygon']) not in str(row['raining']))
        or (str(row['desired_payment_method']) not in str(row['payment_methods']))
        or (str(row['event']) not in row['desired_events'])
    ):
        country = row['country']
        city = row['city']
        zone = row['zone_name']

        orders = row['orders']
        rts = row['rts']

        unn_orders = row['unn_orders']
        unn_perc = round(row['unn_perc'], 2)
        saturation = round(row['saturation'], 2)

        event_actions = row['event']
        rain_actions = row['raining']
        payment_method_actions = row['payment_methods']

        desired_rain_actions = row['desired_polygon']
        desired_payment_methods = row['desired_payment_method']
        desired_events = row['desired_events']

        alt_text = row['alt_text']

        trigger_text = """
        {trigger_name}

        *FYI*: @occ_monit-balance

        *Pais*: {country} - :flag-{country}:
        *Ciudad*: {city}
        *Zona*: {zone}

        *Órdenes*: {orders}
        *RTs*: {rts}
        *Órdenes Unassigned*: {unn_orders}

        *Unassigned Percentual*: {unn_perc}
        *Saturación*: {saturation}

        *Eventos de Flete Aplicados Ahora*: 
        {event_actions}

        *Eventos de Poligono Ahora*:
        {rain_actions}

        *Metodos de Pagamento Cerrados Ahora*:
        {payment_method_actions} 

        *Eventos de Poligono Deseados por Playbook*:
        {desired_rain_actions}

        *Eventos de Flete Deseados por Playbook*:
        {desired_events}

        *Metodos de Pagamento Deseados por Playbook*:
        {desired_payment_methods}

        *Debería ter*:
        {alt_text}
        """.format(
        trigger_name=trigger_name,
        country=country,
        city=city,
        zone=zone,
        orders=orders,
        rts=rts,
        unn_orders=unn_orders,
        unn_perc=unn_perc,
        saturation=saturation,
        event_actions=event_actions,
        rain_actions=rain_actions,
        payment_method_actions=payment_method_actions,
        desired_rain_actions=desired_rain_actions,
        desired_payment_methods=desired_payment_methods,
        desired_events=desired_events,
        alt_text=alt_text
        )

        slack.send_message_names_channel(canal_disparo, trigger_text)

# Main Code
metrics = get_metrics('cl')
metrics.drop_duplicates(subset='zone_id', inplace=True)

classification_names = get_zones_class()['zone_class'].unique().tolist()

for classification_name in classification_names:
    print(classification_name)

    metrics_to_evaluate = metrics[
        metrics['zone_class'].isin([classification_name])
    ]
    conditionals = get_conditionals(classification_name)

    print(metrics_to_evaluate.shape[0])

    matches = match_results(metrics_to_evaluate, conditionals)
    if matches.shape[0] > 0:
        matches.loc[:, 'created_at'] = str(arrow.utcnow())
        snow.upload_df(matches, 'occ_balance_playbook_cl')
        
        for index, row in matches.iterrows():
            run_alarm(row)