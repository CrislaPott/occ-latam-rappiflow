import os, sys, json, requests
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta

sys.path.insert(0, "../")

from lib import redash, gsheets, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

canal_disparo = 'C01672B528Y'
trigger_name = ':alert: Alarma Alto por Unassigned - Balance :alert:'

def get_last_zones(trigger_name=trigger_name, canal_disparo=canal_disparo):
    try:
        latest = datetime.now()
        oldest = latest - timedelta(minutes=30)
        channel_name = canal_disparo

        channel_history = tl.get_channel_history(
            channel = channel_name,
            messages_per_page = 500,
            max_messages = 20000,
            oldest = oldest,
            latest = latest
        )

        last_alarms = channel_history[
            channel_history['text'].str.contains(trigger_name)
        ]

        last_alarms = last_alarms[['ts', 'text']]
        last_alarms['zone'] = last_alarms['text'].str.extract(r"(?<=\*Zona\*:)(.*)")
        last_alarms['zone'] = last_alarms['zone'].str.strip() 

        return last_alarms['zone'].unique().tolist()
    except:
        return []

def get_url_metrics(country):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/zones/live-ops',
        'co': 'http://services.rappi.com/api/operation-management/zones/live-ops',
        'cl': 'http://services.rappi.cl/api/operation-management/zones/live-ops',
        'pe': 'https://services.rappi.pe/api/operation-management/zones/live-ops',
        'uy': 'https://services.rappi.com.uy/api/operation-management/zones/live-ops',
        'ar': 'http://services.rappi.com.ar/api/operation-management/zones/live-ops',
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/zones/live-ops',
        'ec': 'https://services.rappi.com.ec/api/operation-management/zones/live-ops'
    }

    return mappings[country]

def get_url_events(country, city):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/locations/cities/{}/zones'.format(city),
        'co': 'http://services.rappi.com/api/operation-management/locations/cities/{}/zones'.format(city),
        'cl': 'http://services.rappi.cl/api/operation-management/locations/cities/{}/zones'.format(city),
        'pe': 'https://services.rappi.pe/api/operation-management/locations/cities/{}/zones'.format(city),
        'uy': 'https://services.rappi.com.uy/api/operation-management/locations/cities/{}/zones'.format(city),
        'ar': 'http://services.rappi.com.ar/api/operation-management/locations/cities/{}/zones'.format(city),
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/locations/{}/zones'.format(city),
        'ec': 'https://services.rappi.com.ec/api/operation-management/locations/{}/zones'.format(city)
    }

    return mappings[country]

def get_metrics(country, token=token):
    url = get_url_metrics(country)
    payload = {}
    headers = {
        'Authorization': token
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    results = response.json()

    metrics = pd.DataFrame()
    
    for result in results:
        city = result['city']
        zone_id = result['id']
        zone_name = result['name']
        orders = result['total_orders']
        rts = result['total_couriers']
        rts_livres = result['health_check']['free_couriers']
        saturation = result['saturation']
        unn_orders = result['orders_unassigned']

        inserts = [{
            'city': city,
            'zone_id': zone_id,
            'zone_name': zone_name,
            'orders': orders,
            'rts': rts,
            'rts_livres': rts_livres,
            'saturation': saturation,
            'unn_orders': unn_orders
        }]
        inserts = pd.DataFrame(inserts)
        metrics = pd.concat([metrics, inserts], ignore_index=True)

    metrics['country'] = country
    metrics = metrics[metrics['orders'] > 0]
    metrics['saturation'] = metrics['orders'] / metrics['rts']
    metrics['unn_perc'] = (metrics['unn_orders'] / metrics['orders'])*100

    return metrics 

def generate_metrics(token=token):
    generated_metrics = pd.DataFrame()
    # countries = ['br', 'ar', 'co', 'cl', 'pe', 'uy', 'mx', 'ec']
    countries = ['br', 'co', 'uy', 'ec']

    for country in countries:
        metrics = get_metrics(country)
        generated_metrics = pd.concat([generated_metrics, metrics], ignore_index=True)
    
    return generated_metrics

def filter_metrics(metrics):
    cities = [
        'saopaulo', 'riodejaneiro', 'belohorizonte',
        'portoalegre', 'curitiba', 'fortaleza',
        'brasilia', 'salvador', 'recife',
        'campinas', 'goiania', 'florianopolis',
        'aracaju', 'maceio', 'ribeiraopreto', 'sanjosedoriopreto', 'sanjosedoscampos',
        'bogota', 'medellin', 'buenosaires',
        'rosario', 'cordoba', 'pinamar',
        'tucuman', 'mardelplata', 'laplata',
        'mendoza', 'santiagodechile', 'vinadelmar',
        'laserena', 'iquique', 'concepcion',
        'antofagasta', 'guayaquil', 'quito',
        'salinas', 'playas',
        'montevideo', 'puntadeleste'
    ]
    
    metrics = metrics[metrics['city'].isin(cities)]
    return metrics

def get_events(country, city, zone, token=token):
    try:
        url = get_url_events(country, city)
        payload = {}
        headers = {
            'Authorization': token
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        results = response.json()

        for result in results:
            if zone in result['name']:
                return result['event']
    except:
        return 'none'

def get_raining(country, city, zone, token=token):
    try:
        url = get_url_events(country, city)
        payload = {}
        headers = {
            'Authorization': token
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        results = response.json()

        for result in results:
            if zone in result['name']:
                return result['raining']
    except:
        return 'none'

def get_payment_methods(country, city, zone, token=token):
    try:
        url = get_url_events(country, city)
        payload = {}
        headers = {
            'Authorization': token
        }
        response = requests.request("GET", url, headers=headers, data=payload)
        results = response.json()

        for result in results:
            if zone in result['name']:
                return list(result['payment_methods'].keys())
    except:
        return 'none'

metrics = generate_metrics()
metrics = filter_metrics(metrics)

countries = metrics['country'].unique().tolist()
for country in countries:
    country_metrics = metrics[metrics['country'] == country]

    # Condicionais
    country_metrics = country_metrics[
        (metrics['unn_perc'] >= 15) &
        (metrics['unn_orders'] >= 5)
    ]

    last_zones = get_last_zones(trigger_name=trigger_name, canal_disparo=canal_disparo)

    print(last_zones)

    if country_metrics.shape[0] > 0:
        for index, row in country_metrics.iterrows():
            country = row['country']
            city = row['city']
            zone = row['zone_name']

            orders = row['orders']
            rts = row['rts']

            unn_orders = row['unn_orders']
            unn_perc = round(row['unn_perc'], 2)
            saturation = round(row['saturation'], 2)

            event_actions = get_events(country, city, zone, token=token)
            rain_actions = get_raining(country, city, zone, token=token)
            payment_actions = get_payment_methods(country, city, zone, token=token)

            trigger_text = """
            {trigger_name}

            *FYI*: @occ_monit-balance

            *Pais*: {country} - :flag-{country}:
            *Ciudad*: {city}
            *Zona*: {zone}

            *Órdenes*: {orders}
            *RTs*: {rts}
            *Órdenes Unassigned*: {unn_orders}

            *Unassigned Percentual*: {unn_perc}
            *Saturación*: {saturation}

            *Eventos Aplicados Ahora*: 
            {event_actions}

            *Eventos de Lluvia Ahora*:
            {rain_actions}

            *Metodos de Pagamento Cerrados Ahora*:
            {payment_actions} 
            """.format(
            trigger_name=trigger_name,
            country=country,
            city=city,
            zone=zone,
            orders=orders,
            rts=rts,
            unn_orders=unn_orders,
            unn_perc=unn_perc,
            saturation=saturation,
            event_actions=event_actions,
            rain_actions=rain_actions,
            payment_actions=payment_actions
            )

            if zone not in last_zones:
                slack.send_message_names_channel(canal_disparo, trigger_text)

