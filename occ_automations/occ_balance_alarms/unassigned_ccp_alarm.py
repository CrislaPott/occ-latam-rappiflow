import os, sys, json, requests, time
import pandas as pd
import numpy as np
import pytz
from datetime import datetime, timedelta

sys.path.insert(0, "../")

from lib import redash, gsheets, slack
from lib import snowflake as snow
from lib import thread_listener as tl

import datetime as dt
from os.path import expanduser

os.chdir("../")

pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", 100)

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

print(token)

canal_disparo = "C01RF4LERC4"
trigger_name = ':alert: Cierre de Tiendas - Ordenes Sin Asignar :alert:'
trigger_name1 = ':alert: Apertura de Tiendas - Ordenes Sin Asignar :alert:'


def get_last_cities(trigger_name=trigger_name, canal_disparo=canal_disparo):
	try:
		latest = datetime.now()
		oldest = latest - timedelta(minutes=90)
		channel_name = canal_disparo

		channel_history = tl.get_channel_history(
			channel=channel_name,
			messages_per_page=500,
			max_messages=20000,
			oldest=oldest,
			latest=latest
		)

		last_alarms = channel_history[
			channel_history['text'].str.contains(trigger_name)
		]

		last_alarms = last_alarms[['ts', 'text']]
		last_alarms['city'] = last_alarms['text'].str.extract(r"(?<=\*Ciudad\*:)(.*)")
		last_alarms['city'] = last_alarms['city'].str.strip()

		return last_alarms['city'].unique().tolist()
	except:
		return []


def get_url_metrics(country):
	mappings = {
		'br': 'https://services.rappi.com.br/api/operation-management/city-control/indexes',
		'co': 'http://services.rappi.com/api/operation-management/city-control/indexes',
		#         'cl': 'http://services.rappi.cl/api/operation-management/city-control/indexes',
		'pe': 'https://services.rappi.pe/api/operation-management/city-control/indexes',
		#         'uy': 'https://services.rappi.com.uy/api/operation-management/city-control/indexes',
		#         'ar': 'http://services.rappi.com.ar/api/operation-management/city-control/indexes',
		'mx': 'https://services.mxgrability.rappi.com/api/operation-management/city-control/indexes',
		'ec': 'https://services.rappi.com.ec/api/operation-management/city-control/indexes'
		#         'cr': 'https://services.rappi.co.cr/api/operation-management/city-control/indexes'
	}

	return mappings[country]


def get_metrics(country, token=token):
	url = get_url_metrics(country)
	payload = {}
	headers = {
		'Authorization': token
	}

	response = requests.request("POST", url, headers=headers, data=payload)
	results = response.json()

	metrics = pd.DataFrame()

	cities = list(results['cityControl'].keys())
	for city in cities:
		try:
			# city_id = results['cityControl'][city]['id']
			orders = results['cityControl'][city]['o']
			rts = results['cityControl'][city]['c']
			unn_orders = results['cityControl'][city]['u']

			inserts = [{
				'city': city,
				'orders': orders,
				'rts': rts,
				'unn_orders': unn_orders
			}]
			inserts = pd.DataFrame(inserts)
			metrics = pd.concat([metrics, inserts], ignore_index=True)
		except:
			pass

	metrics['country'] = country
	metrics = metrics[metrics['orders'] > 0]
	metrics['unn_perc'] = (metrics['unn_orders'] / metrics['orders']) * 100
	metrics[['country-', 'city']] = metrics['city'].str.split('|', expand=True)
	metrics['city'] = metrics['city'].str.upper()

	#     Condicionales
	if country == 'br':

		metrics = metrics[
			metrics['city'].isin(['SAOPAULO', 'BELOHORIZONTE', 'BRASILIA', 'CAMPINAS', 'CURITIBA', 'FORTALEZA',
								  'PORTOALEGRE', 'RECIFE', 'RIODEJANEIRO'])]

		if metrics['city'].isin(['SAOPAULO']).any():
			metrics_apertura = metrics[((metrics['unn_perc'] < 10.) & metrics['city'].isin(['SAOPAULO'])).any()]
			metrics = metrics.loc[((metrics['unn_orders'] >= 10) & metrics['city'].isin(['SAOPAULO'])).any()]
			metrics = metrics[((metrics['unn_perc'] > 10.) & metrics['city'].isin(['SAOPAULO'])).any()]
		if not metrics['city'].isin(['SAOPAULO']).any():
			metrics_apertura = metrics[((metrics['unn_perc'] < 15.) & ~metrics['city'].isin(['SAOPAULO'])).any()]
			metrics = metrics.loc[(metrics['unn_orders'] >= 10 & ~metrics['city'].isin(['SAOPAULO'])).any()]
			metrics = metrics[((metrics['unn_perc'] > 15.) & ~metrics['city'].isin(['SAOPAULO'])).any()]

		tz = pytz.timezone('America/Sao_Paulo')

	elif country == 'mx':
		metrics = metrics[metrics['city'].isin(
			['CIUDADDEMEXICO', 'GUADALAJARA', 'MONTERREY', 'QUERETARO', 'MERIDA', 'PUEBLA', 'SANLUISPOTOSI',
			 'AGUASCALIENTES', 'CANCUN', 'CULIACAN', 'MORELIA', 'PLAYADELCARMEN', 'PUERTOVALLARTA',
			 'HERMOSILLO', 'CUERNAVACA', 'LEON', 'SANJUANDELRIO', 'GARCIA', 'TIJUANA', 'SALTILLO',
			 'CHIHUAHUA', 'TORREON', 'MEXICALI', 'CIUDADJUAREZ'])]
		metrics_apertura = metrics[(metrics['unn_perc'] < 12.)]
		metrics = metrics.loc[metrics['unn_orders'] >= 10]
		metrics = metrics[(metrics['unn_perc'] > 12.)]

		tz = pytz.timezone('America/Mexico_City')


	elif country == 'co':
		metrics = metrics[metrics['city'].isin(
			['BOGOTA', 'MEDELLIN', 'BARRANQUILLA', 'POPAYAN', 'CUCUTA','PEREIRA','CARTAGENA','CALI','BUCARAMANGA','MANIZALEZ'])]
		metrics_apertura = metrics[(metrics['unn_perc'] < 12.)]
		metrics = metrics.loc[metrics['unn_orders'] >= 10]
		metrics = metrics[(metrics['unn_perc'] > 12.)]

		tz = pytz.timezone('America/Bogota')

	elif country == 'pe':
		metrics = metrics[metrics['city'].isin(
			['LIMA', 'TRUJILLO', 'AREQUIPA', 'CHICLAYO'])]
		metrics_apertura = metrics[(metrics['unn_perc'] < 12.)]
		metrics = metrics.loc[metrics['unn_orders'] >= 10]
		metrics = metrics[(metrics['unn_perc'] > 12.)]

		tz = pytz.timezone('America/Lima')

	elif country == 'ec':
		metrics = metrics[metrics['city'].isin(['GUAYAQUIL', 'QUITO'])]
		metrics_apertura = metrics[(metrics['unn_perc'] < 12.)]
		metrics = metrics.loc[metrics['unn_orders'] >= 10]
		metrics = metrics[(metrics['unn_perc'] > 12.)]

		tz = pytz.timezone('America/Guayaquil')

	la = datetime.now(tz)
	suspended_at = la.strftime('%Y-%m-%d %H:%M:%S')
	metrics['suspended_at'] = suspended_at
	metrics_apertura['suspended_at'] = suspended_at

	return metrics, metrics_apertura


def ccp(country):
	query = f'''
    SELECT DISTINCT 
    '{country}' as country
    ,ps.store_id
    ,ps.partner_id
    ,p.type
    ,UPPER(ca.city) AS city
    ,COUNT(partner_id) OVER (PARTITION BY ps.store_id) partners_count
    ,COALESCE(sa.suspended, false) AS suspended
    FROM {country}_pglr_status_partner_groot_public.partner_store ps
    LEFT JOIN {country}_pglr_status_partner_groot_public.partners p ON ps.partner_id = p.id AND COALESCE(p._fivetran_deleted, false) = false
    LEFT JOIN {country}_pglr_ms_stores_public.stores_vw s ON ps.store_id = s.store_id AND COALESCE(s._fivetran_deleted, false) = false
    INNER JOIN {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id::int = s.city_address_id::int AND COALESCE(ca._fivetran_deleted, false) = false
    LEFT JOIN {country}_pglr_ms_stores_public.store_attribute sa ON ps.store_id = sa.store_id AND COALESCE(sa._fivetran_deleted, false) = false
    WHERE 1=1
    AND COALESCE(ps._fivetran_deleted, false) = false
    AND COALESCE(p._fivetran_deleted, false) = false
    AND COALESCE(s._fivetran_deleted, false) = false
    AND p.type = 'callcenter_partner'
    AND s.is_enabled = true
    AND s.is_published = true
    AND s.deleted_at IS NULL
    AND p.deleted_at IS NULL  
    '''

	df = snow.run_query(query)
	return df


def exclude_city(country):
	query = f'''
    select city as exclude_city,
    suspended_at
    from ops_occ.waiting_time_ccp
    where country='{country}'
    '''

	df = snow.run_query(query)

	if country == 'br':
		tz = pytz.timezone('America/Sao_Paulo')
	elif country == 'mx':
		tz = pytz.timezone('America/Mexico_City')
	elif country == 'co':
		tz = pytz.timezone('America/Bogota')
	elif country == 'pe':
		tz = pytz.timezone('America/Lima')
	elif country == 'ec':
		tz = pytz.timezone('America/Guayaquil')
	la = datetime.now(tz)
	last_2hours = (la - timedelta(hours=2)).strftime('%Y-%m-%d %H:%M:%S')
	df['last_2hours'] = last_2hours
	df = df.loc[df['suspended_at'] >= df['last_2hours']]
	df = df[['exclude_city']]

	return df


def review_city(country):
	query = f'''
    select city as review_city,
    unn_perc as last_unn_perc,
    suspended_at as last_suspended_at
    from ops_occ.waiting_time_ccp
    where country='{country}'
    '''

	df = snow.run_query(query)

	if country == 'br':
		tz = pytz.timezone('America/Sao_Paulo')
	elif country == 'mx':
		tz = pytz.timezone('America/Mexico_City')
	elif country == 'co':
		tz = pytz.timezone('America/Bogota')
	elif country == 'pe':
		tz = pytz.timezone('America/Lima')
	elif country == 'ec':
		tz = pytz.timezone('America/Guayaquil')
	la = datetime.now(tz)
	last_2hours = (la - timedelta(minutes=120)).strftime('%Y-%m-%d %H:%M:%S')
	last_2hoursdelta = (la - timedelta(minutes=115)).strftime('%Y-%m-%d %H:%M:%S')
	df['last_2hours'] = last_2hours
	df['last_2hoursdelta'] = last_2hoursdelta
	df = df.loc[(df['last_suspended_at'] >= df['last_2hours']) & (df['last_suspended_at'] <= df['last_2hoursdelta'])]
	df = df[['review_city', 'last_unn_perc']]

	return df


def generate_metrics(token=token):
	generated_metrics = pd.DataFrame()
	generated_metrics_apertura = pd.DataFrame()
	ccp_stored = pd.DataFrame()
	excluding_cities = pd.DataFrame()
	reviewing_cities = pd.DataFrame()

	countries = ['br', 'mx','co','pe','ec']
	for country in countries:
		print(country)

		try:
			time.sleep(10)
			metrics, metrics_apertura = get_metrics(country)
			generated_metrics = pd.concat([generated_metrics, metrics], ignore_index=True)
			generated_metrics_apertura = pd.concat([generated_metrics_apertura, metrics_apertura], ignore_index=True)
			print(generated_metrics)
			print(generated_metrics_apertura)
			ccp_stores = ccp(country)
			ccp_stored = pd.concat([ccp_stored, ccp_stores], ignore_index=True)
			print(ccp_stored)
			exclude_cities = exclude_city(country)
			excluding_cities = pd.concat([excluding_cities, exclude_cities], ignore_index=True)
			print(excluding_cities)
			review_cities = review_city(country)
			reviewing_cities = pd.concat([reviewing_cities, review_cities], ignore_index=True)
			print(reviewing_cities)
		except Exception as E:
			print('Error token en el país, ', country)
			print(E)

	return generated_metrics, ccp_stored, generated_metrics_apertura, excluding_cities, reviewing_cities


metrics, ccp_stores, metrics_apertura, exclude_cities, review_cities = generate_metrics()
print(ccp_stores.shape)

if len(ccp_stores) > 0:

	ccp_stores['city'] = ccp_stores['city'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode(
		'utf-8')
	ccp_stores['city'] = ccp_stores['city'].str.replace(" ", "")

	### CIERRE DE TIENDAS
	print('CIERRE DE TIENDAS')
	# Excluyo las tiendas que se notificaron hace menos de 2 horas
	metrics = pd.merge(metrics, exclude_cities, how='left', left_on=['city'], right_on=['exclude_city'])
	metrics = metrics[metrics['exclude_city'].isnull()]
	metrics = metrics.drop(['exclude_city'], axis=1)
	print(metrics)
	# Solo tomar CCP
	metrics_ccp = pd.merge(ccp_stores[['store_id', 'city']], metrics[['country', 'city', 'orders', 'rts', 'unn_orders',
																	  'unn_perc', 'suspended_at']],
						   left_on=["city"], right_on=["city"], how='inner')
	print(metrics_ccp)
	# Agrupo por ciudad
	metrics_ccp_group = metrics_ccp.groupby(['city', 'country', 'orders', 'rts', 'unn_orders',
											 'unn_perc', 'suspended_at'])['store_id'].agg(['count']).reset_index()
	print(metrics_ccp_group)
	### APERTURA DE TIENDAS
	print('APERTURA DE TIENDAS')
	# Filtro las ciudades que se alarmaron hace 2 horas y que su % ya es menor que el threshold
	metrics_apertura = pd.merge(review_cities, metrics_apertura, how='left', left_on=['review_city'], right_on=['city'])
	metrics_apertura = metrics_apertura[metrics_apertura['city'].notnull()]
	metrics_apertura = metrics_apertura.drop(['city'], axis=1)
	metrics_apertura = metrics_apertura.rename(columns={'review_city': 'city'})
	print(metrics_apertura)
	# Solo tomar CCP
	metrics_ccp_apertura = pd.merge(ccp_stores[['store_id', 'city']], metrics_apertura[['country', 'city', 'orders',
																						'rts', 'unn_orders', 'unn_perc',
																						'last_unn_perc',
																						'suspended_at']],
									left_on=["city"], right_on=["city"], how='inner')
	print(metrics_ccp_apertura)
	# Agrupo por ciudad
	metrics_ccp_apertura_group = metrics_ccp_apertura.groupby(['city', 'country', 'orders', 'rts', 'unn_orders',
															   'unn_perc', 'last_unn_perc', 'suspended_at'])[
		'store_id'].agg(['count']).reset_index()
	print(metrics_ccp_apertura_group)
	last_cities = get_last_cities()
	print(last_cities)

	## Mensaje para ciudad que superan el threshold
	if metrics_ccp_group.shape[0] > 0:
		for index, row in metrics_ccp_group.iterrows():
			country = row['country']
			city = row['city']
			orders = row['orders']
			unn_orders = row['unn_orders']
			unn_perc = round(row['unn_perc'], 2)
			suspended_at = row['suspended_at']

			text = '''
                {trigger_name}
                *Pais*: {country} - :flag-{country}:
                *Ciudad*: {city}
                *Ordenes*: {orders}
                *Ordenes Unassigned*: {unn_orders}
                *Unassigned Percentual*: {unn_perc}%
                *Hora*: {suspended_at}
                *Acción*: Cierre

                *Lista de tiendas*:
                '''.format(
				trigger_name=trigger_name,
				country=country,
				city=city,
				orders=orders,
				unn_orders=unn_orders,
				suspended_at=suspended_at,
				unn_perc=unn_perc)

			print(text)
			snow.upload_df_occ(metrics_ccp_group, 'waiting_time_ccp')

			metrics_ccp_copy = metrics_ccp.copy()
			print(city)
			home = expanduser("~")
			path = '{}/stores_{}.xlsx'.format(home, city)
			metrics_ccp_copy['suspended_at'] = metrics_ccp_copy['suspended_at'].astype(str)
			metrics_ccp_copy[metrics_ccp_copy['city'] == city].to_excel(path, index=False)
			slack.file_upload_channel(canal_disparo, text, path, "xlsx")

	# Mensaje para apertura de tiendas que ya se notificaron hace 2 horas
	if metrics_ccp_apertura_group.shape[0] > 0:
		for index, row in metrics_ccp_apertura_group.iterrows():
			country = row['country']
			city = row['city']
			orders = row['orders']
			unn_orders = row['unn_orders']
			unn_perc = round(row['unn_perc'], 2)
			last_unn_perc = row['last_unn_perc']
			suspended_at = row['suspended_at']

			text = '''
                {trigger_name1}
                *Pais*: {country} - :flag-{country}:
                *Ciudad*: {city}
                *Ordenes*: {orders}
                *Ordenes Unassigned*: {unn_orders}
                *Unassigned Percentual*: {unn_perc}%
                *Hora*: {suspended_at}
                *Acción*: Apertura

                *Lista de tiendas*:
                '''.format(
				trigger_name1=trigger_name1,
				country=country,
				city=city,
				orders=orders,
				unn_orders=unn_orders,
				suspended_at=suspended_at,
				unn_perc=unn_perc)

			print(text)
			metrics_ccp_apertura_copy = metrics_ccp_apertura.copy()
			print(city)
			home = expanduser("~")
			path = '{}/stores_{}.xlsx'.format(home, city)
			metrics_ccp_apertura_copy['suspended_at'] = metrics_ccp_apertura_copy['suspended_at'].astype(str)
			metrics_ccp_apertura_copy[metrics_ccp_apertura_copy['city'] == city].to_excel(path, index=False)
			slack.file_upload_channel(canal_disparo, text, path, "xlsx")

else:
	print('Dataframe vacío')