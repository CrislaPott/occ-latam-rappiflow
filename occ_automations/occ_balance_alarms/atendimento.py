import os, sys, json, requests, time
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta

sys.path.insert(0, "../")

from lib import redash, gsheets, slack
from lib import snowflake as snow
from lib import thread_listener as tl
from functions import timezones

os.chdir('../')

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

canal_disparo = 'CS7UQAMLG'
trigger_name = ':alert: Alarma Alto por Unassigned - Balance :alert:'

def get_last_cities(trigger_name=trigger_name, canal_disparo=canal_disparo):
	try:
		latest = datetime.now()
		oldest = latest - timedelta(minutes=300)
		channel_name = canal_disparo

		channel_history = tl.get_channel_history(
			channel = channel_name,
			messages_per_page = 500,
			max_messages = 20000,
			oldest = oldest,
			latest = latest
		)

		last_alarms = channel_history[
			channel_history['text'].str.contains(trigger_name)
		]

		last_alarms = last_alarms[['ts', 'text']]
		last_alarms['city'] = last_alarms['text'].str.extract(r"(?<=\*Ciudad\*:)(.*)")
		last_alarms['city'] = last_alarms['city'].str.strip() 

		return last_alarms['city'].unique().tolist()
	except:
		return []

def get_url_metrics(country):
	mappings = {
		'br': 'https://services.rappi.com.br/api/operation-management/city-control/indexes',
		'co': 'http://services.rappi.com/api/operation-management/city-control/indexes',
		'cl': 'http://services.rappi.cl/api/operation-management/city-control/indexes',
		'pe': 'https://services.rappi.pe/api/operation-management/city-control/indexes',
		'uy': 'https://services.rappi.com.uy/api/operation-management/city-control/indexes',
		'ar': 'http://services.rappi.com.ar/api/operation-management/city-control/indexes',
		'mx': 'https://services.mxgrability.rappi.com/api/operation-management/city-control/indexes',
		'ec': 'https://services.rappi.com.ec/api/operation-management/city-control/indexes',
		'cr': 'https://services.rappi.co.cr/api/operation-management/city-control/indexes'
	}

	return mappings[country]

def get_metrics(country, token=token):
	url = get_url_metrics(country)
	payload = {}
	headers = {
		'Authorization': token
	}

	response = requests.request("POST", url, headers=headers, data=payload)
	results = response.json()

	metrics = pd.DataFrame()
	
	cities = list(results['cityControl'].keys())
	for city in cities:
		try:
			# city_id = results['cityControl'][city]['id']
			orders = results['cityControl'][city]['o']
			rts = results['cityControl'][city]['c']
			unn_orders = results['cityControl'][city]['u']
			polygonos = results['cityControl'][city]['po']
			earnings = results['cityControl'][city]['er']

			inserts = [{
				'city': city,
				'orders': orders,
				'rts': rts,
				'unn_orders': unn_orders,
				'polygonos': polygonos,
				'earnings': earnings
			}]
			inserts = pd.DataFrame(inserts)
			metrics = pd.concat([metrics, inserts], ignore_index=True)
		except:
			pass

	metrics['country'] = country
	metrics = metrics[metrics['orders'] > 0]
	metrics['saturation'] = metrics['orders'] / metrics['rts']
	metrics['unn_perc'] = (metrics['unn_orders'] / metrics['orders'])*100

	return metrics 

def generate_metrics(token=token):
	generated_metrics = pd.DataFrame()

	countries = ['br', 'ar', 'co', 'cl', 'pe', 'uy', 'mx', 'ec', 'cr']
	for country in countries:
		print(country)
		metrics = get_metrics(country)
		time.sleep(10)
		generated_metrics = pd.concat([generated_metrics, metrics], ignore_index=True)
		current_time = timezones.country_current_time(country)
		generated_metrics['current_time'] = current_time

	return generated_metrics

def filter_metrics(metrics):
	mappings = {
	'BR|brasilia': '@gustavo.lemos @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|saopaulo': '@kevin.souza @felipe.kim @vitor.xavier, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|riodejaneiro': '@paulo.pinto, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|goiania': '@gustavo.lemos @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|florianopolis': '@raquel.candido @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|portoalegre': '@raquel.candido @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|belohorizonte': '@artur.prates @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|recife': '@andre.souto @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|fortaleza': '@andre.souto @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|salvador': '@andre.souto @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'BR|curitiba': '@fernando.oliveira @paulo.ferrato, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'AR|buenosaires': '@vanessa.traid @luciano.rosso @matias.peluffo @gimena.berman, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'AR|corodba':  '@vanessa.traid @luciano.rosso @matias.peluffo @gimena.berman, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CL|santiagodechile': '@felipe.catalan @pablo.delgado, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CL|vinadelmar': '@felipe.catalan @pablo.delgado, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CO|bogota':  '@daniela.marquez @karen.marin @harold.perez, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CO|medellin': '@daniela.marquez @karen.marin @harold.perez, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CO|cali':'@diana.hernandez @daniela.marquez @karen.marin @harold.perez, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CO|barranquilla':'@daniela.marquez @karen.marin @harold.perez, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'CR|sanjose': '@roger.keesling @robert.solano @cristina.melo, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'MX|ciudaddemexico':  '@fmedina  @pedro.luna @maria.leal @gloeza, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'MX|guadalajara': '@fmedina  @pedro.luna @maria.leal @gloeza, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'MX|monterrey':  '@fmedina  @pedro.luna @maria.leal @gloeza, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'PE|lima': '@camila.reyes @diego.alvarez @felipe.arango, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'UY|montevideo': '@vanessa.traid @luciano.rosso @matias.peluffo @gimena.berman @andreina.lobos, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda',
	'EC|quito': '@paola.revelo @camila.reyes @felipe.arango, @maria.jimeno, @andres.diez, @camilo.prieto, @diego.sepulveda'
	}

	metrics['point_of_contact'] = metrics['city'].map(mappings)
	metrics = metrics[metrics['point_of_contact'].notna()]
	print(metrics)

	return metrics


metrics = generate_metrics()
metrics = filter_metrics(metrics)

# Condicionais
metrics = metrics[
	(metrics['unn_perc'] > 30) &
	(metrics['unn_orders'] > 20) &
	(metrics['saturation'] > 1.2)
]

last_cities = get_last_cities()
print(metrics)
print(last_cities)
hour1 = (datetime(2021, 4, 24, 9, 0, 0, 0)).time()
hour2 = (datetime(2021, 4, 24, 23, 0, 0, 0)).time()

if metrics.shape[0] > 0:
	for index, row in metrics.iterrows():
		print(row['current_time'].time())
		if row['current_time'].time() > hour1 and row['current_time'].time() < hour2:
			country = row['country']
			city = row['city']
			point_of_contact = row['point_of_contact']

			orders = row['orders']
			rts = row['rts']

			unn_orders = row['unn_orders']
			unn_perc = round(row['unn_perc'], 2)
			saturation = round(row['saturation'], 2)
			earnings = round(row['earnings'], 2)
			polygonos = round(row['polygonos'], 2)

			trigger_text = """
			{trigger_name}
	
			*FYI*: {point_of_contact} 
			<!subteam^SJWTS2PSB>
			
			*Pais*: {country} - :flag-{country}:
			*Ciudad*: {city}
	
			*Órdenes*: {orders}
			*RTs*: {rts}
			*Órdenes Unassigned*: {unn_orders}
	
			*Unassigned Percentual*: {unn_perc}
			*Saturación*: {saturation}
			*Auto Balanceo Actuación Ciudad en Polygons*: {polygonos}%
			*Auto Balanceo Actuación Ciudad en Earnings*: {earnings}%
			""".format(
			   trigger_name=trigger_name,
			   point_of_contact=point_of_contact,
			   country=country,
			   city=city,
			   orders=orders,
			   rts=rts,
			   unn_orders=unn_orders,
			   unn_perc=unn_perc,
			   saturation=saturation,
				earnings=earnings,
				polygonos=polygonos
			)
			print(trigger_text)
			if city not in last_cities:
				if (country == 'co') and (saturation < 1):
					print('Caso de CO sem Saturacao')
				else:
					slack.send_message_names_channel(canal_disparo, trigger_text)

		else:
			print('null')