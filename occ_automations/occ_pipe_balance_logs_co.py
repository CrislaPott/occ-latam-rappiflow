import sys
import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now()
oldest = latest - timedelta(minutes=30)

channel_history = tl.get_channel_history(
    channel = "C01DFAHJ05S",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

if channel_history.shape[0] == 0:
    print('Sem mensagens para processar')
    sys.exit(0)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].astype(str)
    
print(channel_history['username'].value_counts())

slack_users = pd.read_csv('input/slack_users.csv')

def transform_saturation(sat):
    sat = sat.replace(',', '.')
    try:
        sat = float(sat)
        return sat
    except:
        return None
    
def transform_unassigned(un):
    un = un.replace('%', '')
    un = un.replace(',', '.')
    try:
        un = float(un)
        if un <= 100:
            return un
    except:
        return None
    
def calculate_start_hour(begin):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        return int(begin_list[0])
    except:
        return None

def calculate_duration(begin, end):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        begin = begin_list[0] + ':' + begin_list[1]

        end_list = end.split(':')
        end_list = [x.zfill(2) for x in end_list]
        end = end_list[0] + ':' + end_list[1]

        FMT = '%H:%M'

        duration = (datetime.strptime(end, FMT) - datetime.strptime(begin, FMT)).seconds / 60
        return duration
    except:
        return None

# Eventualities
try:
    co_eventualities = channel_history[
        (channel_history['username'].str.contains('Aplicación de Eventualidades')) &
        (channel_history['username'].str.contains('CO'))
    ].reset_index()

    co_eventualities = co_eventualities[['ts', 'username', 'text']]

    co_eventualities['slack_user_id'] = co_eventualities['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    co_eventualities['city'] = co_eventualities['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    co_eventualities['zone'] = co_eventualities['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    co_eventualities['polygon'] = co_eventualities['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    co_eventualities['events'] = co_eventualities['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Satur)")
    co_eventualities['saturation'] = co_eventualities['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    co_eventualities['unassigned'] = co_eventualities['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora de inicio)")
    co_eventualities['start_time'] = co_eventualities['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    co_eventualities['end_time'] = co_eventualities['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    co_eventualities['observations'] = co_eventualities['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    co_eventualities = co_eventualities.astype(str)

    co_eventualities_obj = co_eventualities.select_dtypes(['object'])
    co_eventualities[co_eventualities_obj.columns] = co_eventualities_obj.apply(lambda x: x.str.strip())

    co_eventualities['city'] = co_eventualities['city'].str.lower()
    co_eventualities['city'] = co_eventualities['city'].apply(lambda x: unidecode(x))

    co_eventualities['saturation'] = co_eventualities['saturation'].apply(lambda x: transform_saturation(x))
    co_eventualities['unassigned'] = co_eventualities['unassigned'].apply(lambda x: transform_unassigned(x))
    co_eventualities['start_hour'] = co_eventualities['start_time'].apply(lambda x: calculate_start_hour(x))
    co_eventualities['event_duration_minutes'] = co_eventualities.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    co_eventualities = co_eventualities.merge(slack_users, on='slack_user_id', how='left')

    print(co_eventualities.isna().sum())

    print('Fazendo upload dos logs de eventualidades')
    snow.upload_df(co_eventualities, 'occ_log_balance_co_eventualities')
except:
    pass

# Fechamento de Verticais e Pgto
try:
    co_verticals_payments = channel_history[
        (channel_history['username'].str.contains('verticales y pagos')) &
        (channel_history['username'].str.contains('CO'))
    ].reset_index()

    co_verticals_payments = co_verticals_payments[['ts', 'username', 'text']]

    co_verticals_payments['slack_user_id'] = co_verticals_payments['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    co_verticals_payments['city'] = co_verticals_payments['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    co_verticals_payments['zone'] = co_verticals_payments['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Vert*)")
    co_verticals_payments['closed_vertical'] = co_verticals_payments['text'].str.extract(r"(?<=Vertical cerrada\*)(.*)(?=\*Pago cerrado)") 
    co_verticals_payments['closed_payments'] = co_verticals_payments['text'].str.extract(r"(?<=Pago cerrado\*)(.*)(?=\*Sat)")
    co_verticals_payments['saturation'] = co_verticals_payments['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    co_verticals_payments['unassigned'] = co_verticals_payments['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora inicio)")
    co_verticals_payments['start_time'] = co_verticals_payments['text'].str.extract(r"(?<=\*Hora inicio\*)(.*)(?=\*Hora de finalización)")
    co_verticals_payments['end_time'] = co_verticals_payments['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    co_verticals_payments['observations'] = co_verticals_payments['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    co_verticals_payments = co_verticals_payments.astype(str)

    co_verticals_payments_obj = co_verticals_payments.select_dtypes(['object'])
    co_verticals_payments[co_verticals_payments_obj.columns] = co_verticals_payments_obj.apply(lambda x: x.str.strip())

    co_verticals_payments['city'] = co_verticals_payments['city'].str.lower()
    co_verticals_payments['city'] = co_verticals_payments['city'].apply(lambda x: unidecode(x))

    co_verticals_payments['saturation'] = co_verticals_payments['saturation'].apply(lambda x: transform_saturation(x))
    co_verticals_payments['unassigned'] = co_verticals_payments['unassigned'].apply(lambda x: transform_unassigned(x))
    co_verticals_payments['start_hour'] = co_verticals_payments['start_time'].apply(lambda x: calculate_start_hour(x))
    co_verticals_payments['event_duration_minutes'] = co_verticals_payments.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    co_verticals_payments = co_verticals_payments.merge(slack_users, on='slack_user_id', how='left')

    print(co_verticals_payments.isna().sum())

    print('Fazendo upload dos logs de vertcais e pgto')
    snow.upload_df(co_verticals_payments, 'occ_log_balance_co_verticals_payments')
except:
    pass

# Fechamento de Slots
try:
    co_closed_slots = channel_history[
        (channel_history['username'].str.contains('tiendas y Slots')) &
        (channel_history['username'].str.contains('CO'))
    ].reset_index()

    co_closed_slots = co_closed_slots[['ts', 'username', 'text']]

    co_closed_slots['slack_user_id'] = co_closed_slots['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    co_closed_slots['city'] = co_closed_slots['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    co_closed_slots['zone'] = co_closed_slots['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Slot cerrado y Stores*)")
    co_closed_slots['closed_vertical_slot'] = co_closed_slots['text'].str.extract(r"(?<=Slot cerrado y Stores\*)(.*)(?=\*Sat)")
    co_closed_slots['saturation'] = co_closed_slots['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    co_closed_slots['unassigned'] = co_closed_slots['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Store ID)")
    co_closed_slots['store_id'] = co_closed_slots['text'].str.extract(r"(?<=ID\*)(.*)(?=\*Hora de inicio)")
    co_closed_slots['start_time'] = co_closed_slots['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    co_closed_slots['end_time'] = co_closed_slots['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    co_closed_slots['observations'] = co_closed_slots['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    co_closed_slots = co_closed_slots.astype(str)

    co_closed_slots_obj = co_closed_slots.select_dtypes(['object'])
    co_closed_slots[co_closed_slots_obj.columns] = co_closed_slots_obj.apply(lambda x: x.str.strip())

    co_closed_slots['city'] = co_closed_slots['city'].str.lower()
    co_closed_slots['city'] = co_closed_slots['city'].apply(lambda x: unidecode(x))

    co_closed_slots['saturation'] = co_closed_slots['saturation'].apply(lambda x: transform_saturation(x))
    co_closed_slots['unassigned'] = co_closed_slots['unassigned'].apply(lambda x: transform_unassigned(x))
    co_closed_slots['start_hour'] = co_closed_slots['start_time'].apply(lambda x: calculate_start_hour(x))
    co_closed_slots['event_duration_minutes'] = co_closed_slots.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    co_closed_slots = co_closed_slots.merge(slack_users, on='slack_user_id', how='left')

    print(co_closed_slots.isna().sum())

    print('Fazendo upload dos logs de fechamento de slots')
    snow.upload_df(co_closed_slots, 'occ_log_balance_co_closed_slots')
except:
    pass

# Retirada de Acoes
try:
    co_retirada_acoes = channel_history[
        (channel_history['username'].str.contains('Acciones eliminadas')) &
        (channel_history['username'].str.contains('CO'))
    ].reset_index()

    co_retirada_acoes = co_retirada_acoes[['ts', 'username', 'text']]

    co_retirada_acoes['slack_user_id'] = co_retirada_acoes['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    co_retirada_acoes['city'] = co_retirada_acoes['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    co_retirada_acoes['zone'] = co_retirada_acoes['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    co_retirada_acoes['polygon'] = co_retirada_acoes['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    co_retirada_acoes['events'] = co_retirada_acoes['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Vert)")
    co_retirada_acoes['vertical'] = co_retirada_acoes['text'].str.extract(r"(?<=Vertical\*)(.*)(?=\*Método)")
    co_retirada_acoes['payment_method'] = co_retirada_acoes['text'].str.extract(r"(?<=Pago\*)(.*)(?=\* Hora)")
    co_retirada_acoes['start_time'] = co_retirada_acoes['text'].str.extract(r"(?<=\* Hora de inicio\*)(.*)(?=\*Fecha)")
    co_retirada_acoes['observations'] = co_retirada_acoes['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    co_retirada_acoes = co_retirada_acoes.astype(str)

    co_retirada_acoes_obj = co_retirada_acoes.select_dtypes(['object'])
    co_retirada_acoes[co_retirada_acoes_obj.columns] = co_retirada_acoes_obj.apply(lambda x: x.str.strip())

    co_retirada_acoes['city'] = co_retirada_acoes['city'].str.lower()
    co_retirada_acoes['city'] = co_retirada_acoes['city'].apply(lambda x: unidecode(x))

    co_retirada_acoes['start_hour'] = co_retirada_acoes['start_time'].apply(lambda x: calculate_start_hour(x))

    co_retirada_acoes = co_retirada_acoes.merge(slack_users, on='slack_user_id', how='left')

    print(co_retirada_acoes.isna().sum())

    print('Fazendo upload dos logs de retirada de acoes')
    snow.upload_df(co_retirada_acoes, 'occ_log_balance_co_retirada_acoes')
except:
    pass

# Push / SMS
try:
    co_push_sms = channel_history[
        (channel_history['username'].str.contains('Push')) &
        (channel_history['username'].str.contains('co'))
    ].reset_index()

    co_push_sms = co_push_sms[['ts', 'username', 'text']]

    co_push_sms['slack_user_id'] = co_push_sms['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    co_push_sms['city'] = co_push_sms['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    co_push_sms['zone'] = co_push_sms['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*PUSH*)")
    co_push_sms['push_sms'] = co_push_sms['text'].str.extract(r"(?<=\*PUSH \/ SMS\*)(.*)(?=\*Hora de inicio)")
    co_push_sms['start_time'] = co_push_sms['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Fecha)")
    co_push_sms['observations'] = co_push_sms['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    co_push_sms = co_push_sms.astype(str)

    co_push_sms_obj = co_push_sms.select_dtypes(['object'])
    co_push_sms[co_push_sms_obj.columns] = co_push_sms_obj.apply(lambda x: x.str.strip())

    co_push_sms['city'] = co_push_sms['city'].str.lower()
    co_push_sms['city'] = co_push_sms['city'].astype(str)
    co_push_sms['city'] = co_push_sms['city'].apply(unidecode)

    co_push_sms['start_hour'] = co_push_sms['start_time'].apply(lambda x: calculate_start_hour(x))

    co_push_sms = co_push_sms.merge(slack_users, on='slack_user_id', how='left')

    print(co_push_sms.isna().sum())

    print('Fazendo upload dos logs de push')
    snow.upload_df(co_push_sms, 'occ_log_balance_co_push_sms')
except:
    pass



