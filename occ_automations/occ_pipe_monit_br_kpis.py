import os, re, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import thread_listener as tl
from lib import snowflake as snow

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)

channel_history = tl.get_channel_history(
    channel = "C012VS4D5PY",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

kpi_threads = channel_history[
    (channel_history['username'].str.contains('KPIs')) &
    (channel_history['thread_ts'].notna()) &
    (channel_history['subtype'] == 'bot_message')
].drop_duplicates(subset='thread_ts', keep='first')

kpi_threads = kpi_threads[['thread_ts', 'username']]
kpi_threads.rename(columns={'username': 'thread_name'}, inplace=True)

kpi_threads_messages = pd.DataFrame()
for index, row in kpi_threads.iterrows():
    thread_messages = tl.get_thread_replies('C012VS4D5PY', row['thread_ts'])
    kpi_threads_messages = pd.concat([kpi_threads_messages, thread_messages], ignore_index=True)
    
kpi_threads_messages = kpi_threads_messages[['user', 'thread_ts', 'ts', 'text']]
kpi_threads_messages['text'] = kpi_threads_messages['text'].str.lower()
kpi_threads_messages['text'] = kpi_threads_messages['text'].apply(lambda x: unidecode(x))

kpi_threads_messages = kpi_threads_messages[
    kpi_threads_messages['text'].str.contains('pais')
]

kpi_threads_messages = kpi_threads_messages.merge(kpi_threads, on='thread_ts')

print('Quantidade de mensagens nas threads: {}'.format(kpi_threads_messages.shape[0]))

def get_country(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'pais' in value:
            return value
    return None 

def get_threshold(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'threshold' in value:
            return value 
    return None

def get_diagnosis(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'diag' in value:
            return value
    return None 

def get_action_taken(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'acao' in value:
            return value
        if 'accion' in value:
            return value 
    return None 

def transform_country(text_to_parse):
    try:
        if 'brasil' in text_to_parse:
            return 'br'
        elif 'chile' in text_to_parse:
            return 'cl'
        elif 'peru' in text_to_parse:
            return 'pe'
        elif 'argentina' in text_to_parse:
            return 'ar'
        elif 'colombia' in text_to_parse:
            return 'co'
        elif 'mexico' in text_to_parse:
            return 'mx'
        elif 'ecuador' in text_to_parse:
            return 'ec'
        elif 'equador' in text_to_parse:
            return 'ec'
        elif 'uruguay' in text_to_parse:
            return 'uy'
        elif 'uruguai' in text_to_parse:
            return 'uy'
        else:
            return text_to_parse
    except:
        return None

def get_store_id(text):
    text = text.lower()
    if 'store_id' in text:
        try:
            match = re.findall("(?<=store_id:)(.*)", text)[0]
            result = str(match)
            return result
        except:
            pass
    elif 'store id' in text:
        try:
            match = re.findall("(?<=store id:)(.*)", text)[0]
            result = str(match)
            return result
        except:
            pass
    return np.nan

def get_report_link(text):
    text = text.lower()
    try:
        match = re.findall("(?<=https:\/\/)(.*)(?=\>)", text)[0]
        result = str(match)
        return result 
    except:
        return np.nan 

def transform_threshold(text_to_parse):
    try:
        if 'si' in text_to_parse:
            return 'yes'
        elif 'nao' in text_to_parse:
            return 'no'
        elif 'no' in text_to_parse:
            return 'no'
    except:
        return None 

def transform_action(text_to_parse):
    try:
        if 'ninguna' in text_to_parse:
            return 'nenhuma'
        elif 'nenhuma' in text_to_parse:
            return 'nenhuma'
        elif 'ninguno' in text_to_parse:
            return 'nenhuma'
        elif 'sem acoes' in text_to_parse:
            return 'nenhuma'
        elif 'sem acao' in text_to_parse:
            return 'nenhuma'
        elif 'sim accion' in text_to_parse:
            return 'nenhuma'
        elif 'ok' in text_to_parse:
            return 'nenhuma'
        else:
            return text_to_parse
    except:
        return None

kpi_threads_messages['country'] = kpi_threads_messages['text'].str.extract(r"(?<=pais:)(.*)")
kpi_threads_messages['country'] = kpi_threads_messages['country'].str.replace(" e ", ",")
kpi_threads_messages['country'] = kpi_threads_messages['country'].str.replace(" y ", ",")
kpi_threads_messages['country'] = kpi_threads_messages['country'].str.split(',')
kpi_threads_messages = kpi_threads_messages.explode('country').reset_index()
kpi_threads_messages['country'] = kpi_threads_messages['country'].apply(lambda x: transform_country(x))
kpi_threads_messages['country'] = kpi_threads_messages['country'].str.strip()

kpi_threads_messages['store_id'] = kpi_threads_messages['text'].apply(lambda x: get_store_id(x))
kpi_threads_messages['store_id'] = kpi_threads_messages['store_id'].str.replace(" e ", ",")
kpi_threads_messages['store_id'] = kpi_threads_messages['store_id'].str.replace(" y ", ",")
kpi_threads_messages['store_id'] = kpi_threads_messages['store_id'].str.split(',')
kpi_threads_messages = kpi_threads_messages.explode('store_id').reset_index()
kpi_threads_messages['store_id'] = kpi_threads_messages['store_id'].str.strip()

kpi_threads_messages['report_link'] = kpi_threads_messages['text'].apply(lambda x: get_report_link(x))

del kpi_threads_messages['level_0']
del kpi_threads_messages['index']
    

kpi_threads_messages['threshold_reached'] = kpi_threads_messages['text'].apply(lambda x: get_threshold(x))
kpi_threads_messages['diagnosis'] = kpi_threads_messages['text'].apply(lambda x: get_diagnosis(x))
kpi_threads_messages['action_taken'] = kpi_threads_messages['text'].apply(lambda x: get_action_taken(x))

kpi_threads_messages['threshold_reached'] = kpi_threads_messages['threshold_reached'].apply(lambda x: transform_threshold(x))
kpi_threads_messages['action_taken'] = kpi_threads_messages['action_taken'].apply(lambda x: transform_action(x))

users_query = """
select
    slack_user_id as user,
    slack_user as analyst_name
from br_writable.occ_slack_users_db
"""

users = snow.run_query(users_query)
users.columns = ['user', 'analyst_name']

kpi_threads_messages = kpi_threads_messages.merge(users, on='user', how='left')

print(kpi_threads_messages.isna().sum())

print(kpi_threads_messages['country'].value_counts().head(20))

snow.upload_df_occ(kpi_threads_messages, 'occ_monit_indicadores_kpi_logs')