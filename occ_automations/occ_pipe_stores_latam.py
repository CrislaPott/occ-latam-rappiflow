import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)

slack_users = pd.read_csv('input/slack_users.csv')

channel_mapping = {
    "C01DLLADJD7": "tiendas_cl_jumbo",
    "G01D8767HCK": "tiendas_cl_lider",
    "C01CTAFCNUX": "tiendas_cl_tottus_unimarc_isabel",
    "G01EEFDM8KG": "tiendas_co_exito",
    "G01ETCCR0SG": "tiendas_co_pricesmart_carulla_gastronomy",
    "G01A4RGEVTL": "tiendas_cr_total",
    "G01A52342QK": "tiendas_ec_total",
    "C019XJK4S4W": "tiendas_pe_metro_tottus_makro",
    "G01AAG3C1T6": "tiendas_pe_wong_florafauna",
    "G01DJJ285N1": "tiendas_uy_total",
    "G01G26SNKCG": "tiendas_mx_gp1",
    "G01FR1FE001": "tiendas_mx_gp2",
    "G01EXMABEJK": "tiendas_mx_gp3",
    "G01FCL4T199": "tiendas_mx_gp4",
    "C019G4FQWGP": "tiendas_ar_coto",
    "C01A01H6Q11": "tiendas_ar_interior",
    "C019TQX1NJZ": "tiendas_ar_wacosud",
    "C019ZQQJ9FC": "tiendas_ar_carrefour",
}


def get_users(user_ids):
    try:
        user_ids = list(user_ids)
        users = slack_users[slack_users['slack_user_id'].isin(user_ids)]
        return users['slack_user'].unique().tolist()
    except:
        return None
    
def get_attendance_time(latest_reply, thread_ts):
    try:
        interval = datetime.fromtimestamp(float(latest_reply)) - datetime.fromtimestamp(float(thread_ts))
        minutes = interval.total_seconds() / 60
        return minutes
    except:
        return None
    
total_history = pd.DataFrame()
for channel in list(channel_mapping.keys()):
    try:
        channel_history = tl.get_channel_history(
            channel = channel,
            messages_per_page = 500,
            max_messages = 20000,
            oldest = oldest,
            latest = latest
        )
    except:
        pass
    try:
        channel_history.loc[:, 'channel_id'] = channel
        channel_history['thread_id'] = channel + '#' + channel_history['thread_ts']
        channel_history['analyst_names'] = channel_history['reply_users'].apply(lambda x: get_users(x))
        total_history = pd.concat([total_history, channel_history], ignore_index=True)
    except:
        pass
    
total_history = total_history[
    (~total_history['username'].isna()) &
    (~total_history['thread_ts'].isna())
]

total_history['channel_name'] = total_history['channel_id'].map(channel_mapping)
total_history['attendance_time_minutes'] = total_history.apply(lambda x: get_attendance_time(x.latest_reply, x.thread_ts), axis=1)
total_history['analyst_names'] = total_history['analyst_names'].astype(str)

total_history = total_history[[
    'thread_id',
    'channel_name',
    'username',
    'thread_ts',
    'latest_reply',
    'analyst_names',
    'attendance_time_minutes'
]]

total_history.rename(columns={'username': 'thread_name'}, inplace=True)

print(total_history.head())

print('Fazendo upload das threads')
snow.upload_df(total_history, 'occ_store_threads_latam')

