import os, re, sys, json, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow

os.chdir('../')

def store_data(query_id, target_db):
    df = redash.run_query_id(query_id)
    df['datetime_utc'] = str(arrow.utcnow())
    if df.shape[0] > 0:
        snow.upload_df(df, target_db)
    return 0

mappings = {
    'br': 17746,
    'ar': 17875,
    'cl': 19192,
    'mx': 20434,
    'pe': 17562,
    'ec': 17670,
    'uy': 19178,
    'cr': 17621,
    'co': 19206
}

countries = list(mappings.keys())
for country in countries:
    target_db = f'occ_foto_cpgs_atrasos_{country}'
    query_id = mappings[country]
    print(target_db)
    store_data(query_id, target_db)

