import os, re, sys, json, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow

os.chdir('../')

def store_data(query_id, target_db):
    df = redash.run_query_id(query_id)
    df['datetime_utc'] = str(arrow.utcnow())
    if df.shape[0] > 0:
        snow.upload_df(df, target_db)
    return 0

mappings = {
    'br': 17750,
    'ar': 17879,
    'cl': 19196,
    'mx': 20438,
    'pe': 17567,
    'ec': 17674,
    'uy': 19182,
    'cr': 17645,
    'co': 19210
}

countries = list(mappings.keys())
for country in countries:
    target_db = f'occ_foto_cpgs_slotsat_{country}'
    query_id = mappings[country]
    print(target_db)
    store_data(query_id, target_db)