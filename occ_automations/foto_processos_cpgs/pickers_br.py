import os, re, sys, json, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash 
from lib import snowflake as snow

os.chdir('../')

def store_data(query_id, target_db):
    df = redash.run_query_id(query_id)
    df['datetime_utc'] = str(arrow.utcnow())
    if df.shape[0] > 0:
        snow.upload_df(df, target_db)
    return 0

atrasos_db = 'occ_teste_picker_br_atrasos'
slotsat_db = 'occ_teste_picker_br_slotsat'
limbo_db = 'occ_teste_picker_br_limbo'

atrasos = store_data(24435, atrasos_db)
slotsat = store_data(24439, slotsat_db)
limbo = store_data(24440, limbo_db)