# Shopper Sobrecarregado - BR
ps_br = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
  init_break as inicio_pausa,
  shopper_active as PSA,
	case when physical_store_id in (468,50,281, 278, 7610) then (capacidade*0.5)
       when physical_store_id in (286, 7949, 468, 8679, 705, 11374, 8162, 13720, 13721, 13723, 13722, 13724, 38, 40, 46, 50, 264, 265, 278, 281, 282, 284, 286, 514, 524, 532, 576, 7604, 7610, 7687, 7780, 7956, 9205, 11190, 13199, 12107, 10852) then (capacidade*0.35) 
       else capacidade 
  end as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as skus_com_ct, --items_faltando_com_ct
    missing_items_wo_ct as skus_sem_ct, --items_faltando_sem_ct
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Sao_Paulo',current_timestamp))::float/60),0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Sao_Paulo',current_timestamp))::float/60),0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Sao_Paulo',current_timestamp))::float/60),0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
     horario_entrada as Hora_Entrada
   

from
	br_writable.occ_shoppers_capacity
order by
	coalesce(perc_cpct_preenc,0) desc
"""

# Shopper Sobrecarregado - AR
ps_ar = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float as perc_cpct_preenc,
	case when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar

from
	ar_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

# Shopper Sobrecarregado - CL
ps_cl = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float as perc_cpct_preenc,
	case when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Buenos_Aires',current_timestamp))::float/60),0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar

from
	cl_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

# Shopper Sobrecarregado - MX
ps_mx = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Mexico_City',current_timestamp))::float/60),0)::float as perc_cpct_preenc,
	case when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Mexico_City',current_timestamp))::float/60),0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade*(1-extract(minute from convert_timezone('America/Mexico_City',current_timestamp))::float/60),0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar


from
	mx_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

# Shopper Sobrecarregado - CO
ps_co = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade,0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade,0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade,0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar

from
	co_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

ps_pe = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade,0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade,0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade,0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar


from
	pe_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

ps_ec = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade,0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade,0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade,0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
   horario_entrada as horario_comenzar

from
	ec_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

ps_uy = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade,0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade,0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade,0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar

from
	uy_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""

ps_cr = """
--no_cache
select
	physical_store_id as PID,
	shopper_id as PSID,
 init_break as inicio_descanso,
  shopper_active as PSA,
	capacidade as cpct,
-- 	capacidade*(1-extract(minute from current_timestamp)::float/60) as capacidade,
    --items_faltando as skus_alocados,
    missing_items_ct as  skus_com_ct,
    missing_items_wo_ct as skus_sin_ct,
    orders_wo_ct as pedidos_sem_ct,
	items_faltando::float/nullif(capacidade,0)::float as perc_cpct_preenc, 
--  perc_capacidade_preenchida
	case when items_faltando::float/nullif(capacidade,0)::float < 0.6 then 'Ocioso(a)' when items_faltando::float/nullif(capacidade,0)::float > 1 then 'Saturado(a)' else 'Normal' end as status,
    s0,
    s1,
    s2,
    s3,
    splus as "S+",
    horario_entrada as horario_comenzar

from
	cr_writable.occ_shoppers_capacity_tmp
order by
	coalesce(perc_cpct_preenc,0) desc
"""