import os, re, sys, json, arrow, time
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash, gsheets
from lib import snowflake as snow
import queries

os.chdir('../')

def store_data(country):
    target_db = f'occ_foto_cpgs_shoppersat_{country}'
    df = gsheets.read_sheet('1YkJVct9hUhWHGg6UIt_sy5KZMIR4E4jQzLZZ0E1Die8', country.upper())
    if df.shape[0] > 0:
        df['datetime_utc'] = str(arrow.utcnow())
        snow.upload_df(df, target_db)
    return 0

mappings = {
    'br': queries.ps_br,
    'ar': queries.ps_ar,
    'cl': queries.ps_cl,
    'mx': queries.ps_mx,
    'pe': queries.ps_pe,
    'ec': queries.ps_ec,
    'uy': queries.ps_uy,
    'cr': queries.ps_cr,
    'co': queries.ps_co
}

countries = list(mappings.keys())
for country in countries:
    store_data(country)
    time.sleep(3)