import os, sys, json, requests
import numpy as np
import pandas as pd
import pytz
from datetime import datetime, timedelta
from lib import gsheets, slack, redash, sfx
from lib import snowflake as snow
from airflow.models import Variable
import time
import gspread
from df2gspread import df2gspread as d2g


def get_saturation(slot_id):
    try:
        query = """
        with base as (
            select
                physical_store_id,
                id as slot_id,
                datetime_utc_iso,
                items_capacity,
                reserved_items_capacity
            from slots
            where
                physical_store_id = (select physical_store_id from slots where id = {slot_id})
                and datetime_utc_iso <= (select datetime_utc_iso from slots where id = {slot_id})
                and datetime_utc_iso >= (select datetime_utc_iso from slots where id = {slot_id}) - interval '2 hour'
                and deleted_at is null
                and type = 'SHOPPER'
            order by datetime_utc_iso asc
        )

        select
            physical_store_id,
            sum(items_capacity) as items_capacity,
            sum(reserved_items_capacity) as reserved_items_capacity,
            sum(reserved_items_capacity)::float / nullif(sum(items_capacity)::float, 0) as slots_saturation
        from base
        group by 1
        """.format(
            slot_id=slot_id
        )

        saturation = redash.run_query(1968, query)

        print(saturation)

        return round(saturation["slots_saturation"].unique().tolist()[0], 2)
    except:
        return np.nan


def change_slot_status(slot_id, status, iteration=0):
    """Change the slots status."""
    headers = {
        "Authorization": "postman",
        "API_KEY": Variable.get("CPGOPS_API_KEY"),
        "Content-Type": "application/json",
        'x-rappi-team': 'OCC',
        'x-application-id': 'rappiflow-latam'
    }
    data = {"is_closed": not status}

    response = requests.put(
        "http://services.rappi.com.ar/api/cpgops-gateway-ms/stores/slots/{}".format(
            str(slot_id)
        ),
        headers=headers,
        data=json.dumps(data),
    )

    if response.status_code == 200:
        return "Sim"
    else:
        return "Nao"


ditches_query = """
with

ditches as (
    select distinct
        slot_id,
        last_value(status) over (partition by slot_id order by created_at) as status,
        last_value(action_status) over (partition by slot_id order by created_at) as action_status
    from ops_occ.occ_valas_ar
),

slots_table as (
    select
        id as slot_id,
        physical_store_id,
        -- extract(hour from datetime_utc_iso - interval '3 hours') as hour
        dateadd('hour', -3, datetime_utc_iso) as open_time
    from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
    where
        type = 'SHOPPER'
        and (datetime_utc_iso - interval '3 hours')::date = (convert_timezone('America/Buenos_Aires',current_timestamp))::date
        and extract(hour from datetime_utc_iso - interval '3 hours') = extract(hour from (convert_timezone('America/Buenos_Aires',current_timestamp)) + interval '2h')
)

select
    st.slot_id,
    status,
    physical_store_id,
    -- hour
    to_char(open_time, 'HH:00:00') as open_time,
    dayname(open_time) as open_day
from slots_table st
join ditches d on d.slot_id = st.slot_id
--where status = 'closed'
--and action_status = True
"""

ditches = snow.run_query(ditches_query)

if ditches.shape[0] == 0:
    slack.send_message_channel("C01J24CUYM7", "Rodada sin valas para reapertura")
    print("Rodada sin valas para reapertura")
    sys.exit(0)

ditches2 = ditches.copy()
ditches2["saturation"] = ditches2["slot_id"].apply(lambda x: get_saturation(x))
current_time = datetime.now(pytz.timezone("America/Bogota"))

for index, row in ditches2.iterrows():
    physical_store_id = str(row["physical_store_id"])
    slot_id = str(row["slot_id"])
    open_time = row["open_time"]
    saturation = row["saturation"]
    country = "AR"
    reason = "slot ditches open"
    opened_at = current_time.strftime("%Y-%m-%d %H:%M:%S")

    try:
        if saturation < 1.15:
            change_status = change_slot_status(slot_id, True, iteration=0)
            sfx.send_events_slotDitchesOpen(
                slot_id, physical_store_id, country, reason, opened_at
            )

            text = """
            *Aplicación de apertura de vala - AR* :flag-ar:

            *Physical Store ID*: {physical_store_id}
            *Slot ID*: {slot_id}
            *Horario*: {open_time}
            *Saturación de Slots*: {saturation}
            *Status Modificado*: {change_status}
            """.format(
                physical_store_id=physical_store_id,
                slot_id=slot_id,
                open_time=open_time,
                saturation=saturation,
                change_status=change_status,
            )
            print(text)
            slack.send_message_channel("C01J24CUYM7", text)
            time.sleep(5)
    except:
        pass
