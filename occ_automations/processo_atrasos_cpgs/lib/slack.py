import os
from dotenv import load_dotenv
from slack import WebClient
from slack.errors import SlackApiError
from airflow.models import Variable

load_dotenv()

def bot_slack(text,channel):
    client = WebClient(token=Variable.get("SLACK_TOKEN"))
    client.chat_postMessage(text=text, channel=channel)

def file_upload_channel(channel, text, filename, filetype):
    client = WebClient(token=Variable.get("SLACK_TOKEN"))
    client.files_upload(file=filename,filename=filename,channels=channel,title=filename,initial_comment=text,filetype=filetype)
