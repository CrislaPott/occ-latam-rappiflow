import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
import pytz

def country_timezones(country):
    if country == 'co':
        timezone = 'Bogota'
        interval = '5h'
    elif country == 'cr':
        timezone = 'Costa_Rica'
        interval = '6h'
    elif country == 'mx':
        timezone = 'Mexico_City'
        interval = '5h'
    elif country == 'cl':
        timezone = 'Santiago'
        interval = '4h'
    elif country == 'pe':
        timezone = 'Lima'
        interval = '5h'
    elif country == 'ec':
        timezone = 'Guayaquil'
        interval = '5h'
    elif country == 'br':
        timezone = 'Buenos_Aires'
        interval = '3h'
    elif country == 'uy':
        timezone = 'Montevideo'
        interval = '3h'
    elif country == 'ar':
        timezone = 'Buenos_Aires'
        interval = '3h'
        
    return timezone, interval

def country_current_time(country):
    if country == 'ar':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'cl':
        tz = pytz.timezone('America/Santiago')
        current_time = datetime.now(tz)
    elif country == 'co':
        tz = pytz.timezone('America/Bogota')
        current_time = datetime.now(tz)
    elif country == 'cr':
        tz = pytz.timezone('America/Costa_Rica')
        current_time = datetime.now(tz)
    elif country == 'ec':
        tz = pytz.timezone('America/Guayaquil')
        current_time = datetime.now(tz)
    elif country == 'br':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'mx':
        tz = pytz.timezone('America/Mexico_City')
        current_time = datetime.now(tz)
    elif country == 'pe':
        tz = pytz.timezone('America/Lima')
        current_time = datetime.now(tz)
    elif country == 'uy':
        tz = pytz.timezone('America/Montevideo')
        current_time = datetime.now(tz)

    return current_time