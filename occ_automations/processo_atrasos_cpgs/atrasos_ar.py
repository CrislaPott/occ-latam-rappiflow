import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta

sys.path.insert(0, "../")

from lib import redash, gsheets, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

# Lojas em Atraso

atrasos = redash.run_query_id(17875)

if atrasos.shape[0] > 0:
    atrasos.rename(columns={'Assign 10m + Picking 30m': 'skus_atrasados'}, inplace=True)
    atrasos = atrasos[['physical_store_id', 'skus_atrasados']]
    atrasos.sort_values(by='skus_atrasados', ascending=False, inplace=True)
    atrasos = atrasos[atrasos['skus_atrasados'] > 0]

if atrasos.shape[0] > 0:
    store_ids_atrasados = atrasos['physical_store_id'].unique().tolist()
    store_ids_atrasados = [f'{store_id}' for store_id in store_ids_atrasados]
    store_ids_atrasados = ','.join(store_ids_atrasados)
else:
    print('Sem lojas em atraso')
    sys.exit(0)

print(store_ids_atrasados)

# Capacidade Extra por Slot - MS cpgops-stores-ms

if atrasos.shape[0] > 0:
    query = f"""
    --no_cache
    select
        physical_store_id::text as "physical_store_id::filter",
        (datetime_utc_iso - interval '3 hour') as slot,
        id,
        is_closed,
        (sum(case when (datetime_utc_iso ) < (now()) then items_capacity*(60-extract(minute from now()))/60 else items_capacity end) -
        sum(case when (datetime_utc_iso ) < (now()) then reserved_items_capacity*(60-extract(minute from now()))/60 else reserved_items_capacity end))::int::text as capacidade_extra_restante,
        sum(items_capacity) as capacidad_total_slot
    from
        slots
    where
        (datetime_utc_iso) >= (now() - interval '1 hour')
        and (datetime_utc_iso- interval '3 hour')::date = date(now() - interval '3 hour')
        and type = 'SHOPPER'
    and physical_store_id in ({store_ids_atrasados})
    and deleted_at is null
    group by 
        1
        , 2
        , 3
        , 4
    """

    capacidade_slots = redash.run_query(1968, query)

# Ordens Programadas - MS cpgops_events_ms

if atrasos.shape[0] > 0:
    query = f"""
    --no_cache
    with orders as (
    select
        os.physical_store_id::text as id_fisico,
        (os.place_at at time zone 'America/Buenos_Aires') :: date as data_agendamento,
        place_at as place_at,
        case when os.state ilike '%canceled%' then 'canceled'
            when os.state ilike '%scheduled%' then 'scheduled'
            when os.state in ('finished', 'pending_review') then 'finished'
            else 'live' end as state_agg,
        case when os.picked_by = 'shopper_or_rt' then 'yes'
            else 'no' end as picked_by_rt,
        os.state,
        os.order_id as order_id,
        os.products_qty as skus, was_taken
    from 
        order_summary os
    where
        (os.place_at at time zone 'America/Buenos_Aires') :: date >= (now() at time zone 'America/Buenos_Aires') :: date
        and (os.place_at at time zone 'America/Buenos_Aires') :: date <= (now() at time zone 'America/Buenos_Aires') :: date + interval '14 days'
        and os.state not ilike '%canceled%'
        and os.state not in ('finished', 'pending_review')
        and (os.was_taken=true)
        and physical_store_id in ({store_ids_atrasados})
    )
    , user_seg as (
    select distinct
        oe.order_id,
        first_value(oe.params->'user_segmentation'->'finance_user_type') over (partition by oe.order_id ) as user_segm,
        first_value(oe.params->'user_segmentation'->'new_user_cpg') over (partition by oe.order_id ) as new_user_cpg,
        first_value(oe.params->'user_segmentation'->'segment_rfm') over (partition by oe.order_id ) as segment_rfm,
        first_value(oe.params->'user_segmentation'->>'hvu_prob') over (partition by oe.order_id ) as hvu_prob
    from
        orders o
    inner join
        order_events oe on oe.order_id = o.order_id and oe.name = 'create_order'
    )

    , resched as (
    select
        oe.order_id as order_id,
        count(oe.id) as rescheds
    from 
        orders o
    inner join
        order_events oe on oe.order_id = o.order_id and oe.name = 'change_to_scheduled'
    group by
        1
    )

    select 
        o.id_fisico::text as "ID_Fisico::multi-filter",
        o.data_agendamento as "Data_Agendamento::multi-filter",
        (o.place_at - interval '3 hours') as place_at,
        extract(hour from o.place_at - interval '7 hours') as "Inicio_Shopper_Slot::multi-filter",
        o.state_agg,
        o.state,
        case when (lower(us.user_segm::text) like '%hq%') then 'HQ' 
            when (lower(segment_rfm::text) like '%diamond%' and lower(segment_rfm::text) not like '%potential%diamond%') then 'Diamond'
            when hvu_prob::float >= 0.85 then 'High hvu_prob'
            else null end as user_hq,
            hvu_prob,
        o.order_id::text as order_id,
        was_taken,
        picked_by_rt,
        o.skus,
        coalesce(rescheds,0) as qtd_reagend
    from
        orders o
    left join
        user_seg us on us.order_id = o.order_id
    left join
        resched rc on rc.order_id = o.order_id
    """

    ordens_programadas = redash.run_query(2430, query)

# Captura do Próximo Slot

datetime_local = arrow.utcnow().to('America/Buenos_Aires')
data_local = str(datetime_local.date())
hora_local = datetime_local.hour

def get_data_local(datetime_local):
    data_local = arrow.get(datetime_local).shift(hours=-3).date()
    return str(data_local)

def get_hora_local(datetime_local):
    hora_local = arrow.get(datetime_local).shift(hours=-3).hour
    return hora_local

capacidade_slots['data_slot'] = capacidade_slots['slot'].apply(lambda x: get_data_local(x)) 
capacidade_slots['hora_slot'] = capacidade_slots['slot'].apply(lambda x: get_hora_local(x)) 

final_ordens_rts = []
final_ordens_nao_rts = []

for index, row in atrasos.iterrows():
    try:
        physical_store_id = row['physical_store_id']
        skus_atrasados = int(row['skus_atrasados'])

        print(f'Rodando loja {physical_store_id}')

        slot_atual = capacidade_slots[
            (capacidade_slots['physical_store_id::filter'] == physical_store_id) &
            (capacidade_slots['data_slot'] == data_local) &
            (capacidade_slots['hora_slot'] <= hora_local)
        ]

        slot_atual = slot_atual[
            slot_atual['hora_slot'] == slot_atual['hora_slot'].max()
        ].reset_index()

        proximo_slot = capacidade_slots[
            (capacidade_slots['physical_store_id::filter'] == physical_store_id) &
            (capacidade_slots['data_slot'] == data_local) &
            (capacidade_slots['hora_slot'] > hora_local)
        ]

        proximo_slot = proximo_slot[
            proximo_slot['hora_slot'] == proximo_slot['hora_slot'].min()
        ].reset_index()

        id_slot_atual = slot_atual.loc[0, 'id']
        id_proximo_slot = proximo_slot.loc[0, 'id']

        data_proximo_slot = proximo_slot.loc[0, 'data_slot']
        hora_proximo_slot = proximo_slot.loc[0, 'hora_slot']

        status_proximo_slot = proximo_slot.loc[0, 'is_closed']
        if status_proximo_slot == True:
            status_abertura_slot = 'Cerrado'
        else:
            status_abertura_slot = 'Aberto'
            
        capacidade_extra_restante = int(proximo_slot.loc[0, 'capacidade_extra_restante'])

        ordens_proximo_slot = ordens_programadas[
            (ordens_programadas['ID_Fisico::multi-filter'] == physical_store_id) &
            (ordens_programadas['Data_Agendamento::multi-filter'] == data_local) &
            (ordens_programadas['Inicio_Shopper_Slot::multi-filter'] > hora_local)
        ]

        ordens_proximo_slot = ordens_proximo_slot[
            ordens_proximo_slot['Inicio_Shopper_Slot::multi-filter'] == ordens_proximo_slot['Inicio_Shopper_Slot::multi-filter'].min()
        ]

        ordens_por_rts = ordens_proximo_slot[
            (ordens_proximo_slot['user_hq'] != 'High hvu_prob') &
            (ordens_proximo_slot['qtd_reagend'] == 0) &
            (ordens_proximo_slot['picked_by_rt'] != 'no')
        ]

        ordens_nao_por_rts = ordens_proximo_slot[
            (ordens_proximo_slot['user_hq'] != 'High hvu_prob') &
            (ordens_proximo_slot['qtd_reagend'] == 0) &
            (ordens_proximo_slot['picked_by_rt'] == 'no')
        ]

        qty_ordens_por_rts = ordens_por_rts.shape[0]
        qty_ordens_nao_por_rts = ordens_nao_por_rts.shape[0]

        if qty_ordens_por_rts > 0:
            ordens_reagendamento_rts = ordens_por_rts['order_id'].unique().tolist()
            final_ordens_rts.extend(ordens_reagendamento_rts)
        else:
            ordens_reagendamento_rts = []

        if qty_ordens_nao_por_rts > 0:
            ordens_nao_por_rts.sort_values(by='skus', ascending=True)
            ordens_nao_por_rts['cumsum_skus'] = ordens_nao_por_rts['skus'].cumsum()

            corte_skus = min(ordens_nao_por_rts['cumsum_skus'], key=lambda x: abs(x - skus_atrasados))
            ordens_nao_por_rts = ordens_nao_por_rts[ordens_nao_por_rts['cumsum_skus'] <= corte_skus]

            qty_ordens_nao_por_rts = ordens_nao_por_rts.shape[0]

        if qty_ordens_nao_por_rts > 0:
            ordens_reagendamento_nao_rts = ordens_nao_por_rts['order_id'].unique().tolist()
            final_ordens_nao_rts.extend(ordens_reagendamento_nao_rts)
        else:
            ordens_reagendamento_nao_rts = []

        texto_slack = f"""
        :flag-ar: *Proceso de Retrasos en CPGs - AR* :flag-ar:

        *Physical Store ID*: {physical_store_id}
        *SKUs Retrasados*: {skus_atrasados}
        *ID Slot Actual*: {id_slot_atual}
        *ID Slot Siguiente*: {id_proximo_slot}
        *Hora Slot Siguiente*: {hora_proximo_slot}
        *Status Apertura Slot Siguiente*: {status_abertura_slot}
        *Capacidade Extra Restante Slot Siguiente*: {capacidade_extra_restante}

        *Ordenes que van a RT*: {ordens_reagendamento_rts}
        *Ordenes para Reprogramación*: {ordens_reagendamento_nao_rts}
        """

        if skus_atrasados > capacidade_extra_restante:
            slack.send_message_names_channel('C01H2ED81TQ', texto_slack)
    except Exception as e:
        print(e)
        continue
