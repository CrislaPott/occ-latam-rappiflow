import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from functions import timezones
from lib import redash, slack
from lib import snowflake as snow
import metrics

def run_bot(country):
	text = f"""
	Pais: {country} 
	:flag-{country}:
	*No hay tiendas con retrasos por el momento*
	"""
	atrasos = metrics.stores_with_delayed(country)
	print(atrasos)

	if atrasos.shape[0] > 0:
		atrasos.rename(columns={'Assign 10m + Picking 30m': 'skus_atrasados'}, inplace=True)
		atrasos = atrasos[['physical_store_id', 'skus_atrasados']]
		atrasos.sort_values(by='skus_atrasados', ascending=False, inplace=True)
		atrasos = atrasos[atrasos['skus_atrasados'] > 0]

	if atrasos.shape[0] > 0:
		store_ids_atrasados = atrasos['physical_store_id'].unique().tolist()
		store_ids_atrasados = [f'{store_id}' for store_id in store_ids_atrasados]
		store_ids_atrasados = ','.join(store_ids_atrasados)

	else:
		print('Sem lojas em atraso')
		slack.bot_slack(text, 'C01H2ED81TQ')
		return None

	if atrasos.shape[0] > 0:
		capacidade_slots = metrics.slots_capacity(country,store_ids_atrasados)
		print(capacidade_slots)

	if atrasos.shape[0] > 0:
	   ordens_programadas = metrics.scheduled_orders(country,store_ids_atrasados)
	   print(ordens_programadas)

	datetime_local = timezones.country_current_time(country)
	timezone, interval = timezones.country_timezones(country)
	data_local = str(datetime_local.date())
	hora_local = datetime_local.hour

	def get_data_local(datetime_local):
		if country in ['br','uy']:
			data_local = arrow.get(datetime_local).shift(hours=+0).date()
		if country in ['ar']:
			data_local = arrow.get(datetime_local).shift(hours=-0).date()
		if country in ['cl']:
			data_local = arrow.get(datetime_local).shift(hours=+0).date()
		if country in ['co','pe','ec']:
			data_local = arrow.get(datetime_local).shift(hours=+0).date()
		if country in ['mx']:
			data_local = arrow.get(datetime_local).shift(hours=+0).date()
		if country in ['cr']:
			data_local = arrow.get(datetime_local).shift(hours=+0).date()
		return str(data_local)

	def get_hora_local(datetime_local):
		if country in ['br','uy']:
			hora_local = arrow.get(datetime_local).shift(hours=+0).hour
		if country in ['ar']:
			hora_local = arrow.get(datetime_local).shift(hours=-0).hour
		if country in ['cl']:
			hora_local = arrow.get(datetime_local).shift(hours=+0).hour
		if country in ['co','ec','pe']:
			hora_local = arrow.get(datetime_local).shift(hours=+0).hour
		if country in ['mx']:
			hora_local = arrow.get(datetime_local).shift(hours=+0).hour
		if country in ['cr']:
			hora_local = arrow.get(datetime_local).shift(hours=+0).hour
		return hora_local

	print(country)
	print(get_data_local(datetime_local))
	print(get_hora_local(datetime_local))

	print("capacidade_slots")
	print(capacidade_slots)

	capacidade_slots['data_slot'] = capacidade_slots['slot'].apply(lambda x: get_data_local(x)) 
	capacidade_slots['hora_slot'] = capacidade_slots['slot'].apply(lambda x: get_hora_local(x)) 

	final_ordens_rts = []
	final_ordens_nao_rts = []

	contador_1 = 0
	contador_2 = 0
	tiendas_retrasos = []

	for index, row in atrasos.iterrows():
		try:
			physical_store_id = row['physical_store_id']
			skus_atrasados = int(row['skus_atrasados'])


			print(f'Rodando loja {physical_store_id}')

			print("slot_atual")
			slot_atual = capacidade_slots[
			(capacidade_slots['physical_store_id::filter'] == physical_store_id) &
			(capacidade_slots['data_slot'] == data_local) &
			(capacidade_slots['hora_slot'] <= hora_local)
			]

			slot_atual = slot_atual[
			slot_atual['hora_slot'] == slot_atual['hora_slot'].max()
			].reset_index()

			print("proximo_slot")
			proximo_slot = capacidade_slots[
			(capacidade_slots['physical_store_id::filter'] == physical_store_id) &
			(capacidade_slots['data_slot'] == data_local) &
			(capacidade_slots['hora_slot'] > hora_local)
			]

			proximo_slot = proximo_slot[
			proximo_slot['hora_slot'] == proximo_slot['hora_slot'].min()
			].reset_index()

			id_slot_atual = slot_atual.loc[0, 'id']
			id_proximo_slot = proximo_slot.loc[0, 'id']

			data_proximo_slot = proximo_slot.loc[0, 'data_slot']
			hora_proximo_slot = proximo_slot.loc[0, 'hora_slot']

			status_proximo_slot = proximo_slot.loc[0, 'is_closed']

			if status_proximo_slot == True:
				status_abertura_slot = 'Cerrado'
			else:
				status_abertura_slot = 'Aberto'
			
			print("status_abertura_slot")
			capacidade_extra_restante = int(proximo_slot.loc[0, 'capacidade_extra_restante'])

			ordens_proximo_slot = ordens_programadas[
			(ordens_programadas['ID_Fisico::multi-filter'] == physical_store_id) &
			(ordens_programadas['Data_Agendamento::multi-filter'] == data_local) &
			(ordens_programadas['Inicio_Shopper_Slot::multi-filter'] > hora_local)
			]


			ordens_proximo_slot = ordens_proximo_slot[
			ordens_proximo_slot['Inicio_Shopper_Slot::multi-filter'] == ordens_proximo_slot['Inicio_Shopper_Slot::multi-filter'].min()
			]


			ordens_por_rts = ordens_proximo_slot[
			(ordens_proximo_slot['user_hq'] != 'High hvu_prob') &
			(ordens_proximo_slot['qtd_reagend'] == 0) &
			(ordens_proximo_slot['picked_by_rt'] != 'no')
			]


			ordens_nao_por_rts = ordens_proximo_slot[
			(ordens_proximo_slot['user_hq'] != 'High hvu_prob') &
			(ordens_proximo_slot['qtd_reagend'] == 0) &
			(ordens_proximo_slot['picked_by_rt'] == 'no')
			]

			qty_ordens_por_rts = ordens_por_rts.shape[0]
			qty_ordens_nao_por_rts = ordens_nao_por_rts.shape[0]

			if qty_ordens_por_rts > 0:
				ordens_por_rts.sort_values(by='skus', ascending=True)
				ordens_por_rts['cumsum_skus'] = ordens_por_rts['skus'].cumsum()

				corte_skus = min(ordens_por_rts['cumsum_skus'], key=lambda x: abs(x - skus_atrasados))
				ordens_por_rts = ordens_por_rts[ordens_por_rts['cumsum_skus'] <= corte_skus]

				qty_ordens_por_rts = ordens_por_rts.shape[0]

			if qty_ordens_por_rts >0:
				ordens_reagendamento_rts = ordens_por_rts['order_id'].unique().tolist()
				final_ordens_rts.extend(ordens_reagendamento_rts)
			
			else:
				ordens_reagendamento_rts = []


			if qty_ordens_nao_por_rts > 0:
				ordens_nao_por_rts.sort_values(by='skus', ascending=True)
				ordens_nao_por_rts['cumsum_skus'] = ordens_nao_por_rts['skus'].cumsum()

				corte_skus = min(ordens_nao_por_rts['cumsum_skus'], key=lambda x: abs(x - skus_atrasados))
				ordens_nao_por_rts = ordens_nao_por_rts[ordens_nao_por_rts['cumsum_skus'] <= corte_skus]

				qty_ordens_nao_por_rts = ordens_nao_por_rts.shape[0]

			if qty_ordens_nao_por_rts > 0:
				ordens_reagendamento_nao_rts = ordens_nao_por_rts['order_id'].unique().tolist()
				final_ordens_nao_rts.extend(ordens_reagendamento_nao_rts)

			else:
				ordens_reagendamento_nao_rts = []


			texto_slack = f"""
			:flag-{country}: *Proceso de Retrasos en CPGs - {country.upper()}* :flag-{country}:
			*Physical Store ID*: {physical_store_id}
			*SKUs Retrasados*: {skus_atrasados}
			*ID Slot Actual*: {id_slot_atual}
			*ID Slot Siguiente*: {id_proximo_slot}
			*Hora Slot Siguiente*: {hora_proximo_slot}
			*Status Apertura Slot Siguiente*: {status_abertura_slot}
			*Capacidade Extra Restante Slot Siguiente*: {capacidade_extra_restante}
			*Ordenes que van a RT*: {ordens_reagendamento_rts}
			*Ordenes para Reprogramación*: {ordens_reagendamento_nao_rts}
			""".format(country=country)

			print(texto_slack)

			if skus_atrasados > capacidade_extra_restante:
				slack.bot_slack(texto_slack, 'C01H2ED81TQ')
				contador_1 = contador_1 + 1
			else:
				print("skus_atrasados <= capacidade_extra_restante")
				contador_2 = contador_2 + 1
				tiendas_retrasos.extend(physical_store_id)
		except Exception as e:
			print(e)
			contador_2 = contador_2 + 1
			tiendas_retrasos = list(dict.fromkeys(tiendas_retrasos))

	if contador_1 >= 1:
		print(contador_1)
	else:
		tiendas_retrasos = list(dict.fromkeys(tiendas_retrasos))
		print(tiendas_retrasos)
		text_slack = f'''
		Pais: {country} 
		:flag-{country}:
		Hay {contador_2} tienda(s) con retrasos, pero la cantidad de SKU es menor que la capacidad de sus respectivos slots siguientes
		Tiendas con retrasos: {tiendas_retrasos}
		'''
		print(text_slack)
		slack.bot_slack(text_slack, 'C01H2ED81TQ')


countries = ['br','ar','cl','pe','ec','cr','uy','mx']
for country in countries:
	print(country)
	try:
		run_bot(country)
	except Exception as e:
		print(e)