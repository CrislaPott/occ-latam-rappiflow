import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from lib import redash
from functions import timezones

def stores_with_delayed(country):
    if country == 'br':
        store_ids = '''19,40,265,276,278,284,302,303,304,310,311,313,335,340,345,349,360,369,384,392,407,467,503,508,520,537,582,642,649,700,726,7571,7577,7578,7582,7583,7593,7600,7602,7605,7612,7842,7876,7923,7947,7948,7952,8001,8157,8162,8188,8396,8397,8706,8717,8718,8723,8724,8725,8726,8727,8730,8762,8764,8778,8780,8781,8784,8785,8786,8787,8789,8791,8793,8813,8816,8819,8820,8821,8822,8823,8833,8874,8880,9130,9131,9132,9133,9134,9135,9137,9138,9139,9140,9177,9178,9180,9181,9215,9218,9221,9222,9553,9223,9224,9225,9300,9301,9417,9420,9426,9454,9504,9514,9536,9552,9594,9597,9641,9649,9658,9710,10076,10542,11260,11374,11679,11889,12106,12147,12246,12823,12824,13179,13240,13313,13317,13789,13820,13981,14393,14394,14395,14409,14498,14750,14849,14909,15205,15243,15794,31957,32043,32139,32140,32141,32252,32898,35083,35084,38618,43440,44005,44006,45288,45460,46969,46892,46924,47377,47379,45458,45459,45466,8817,48285,13180,9136,45385,7601,43441,701,8675,13788,32221,32251,32472,32473,45461,45465,45467,45468,45469,45470,46730'''
    elif country == 'cl':
        store_ids = '12,14,15,18,28,38,39,45,56,81,115,207,241,265,290,292,312,317,346,353,354,355,361,3674,3709,3873,3884,3904,3999,4003,4017,4031,4046,4053,4298,4345,4473,4771,5027,5028,5128,5129,5130,5131,5132,5133,5134,5135,5136,5469,5470,359,5874,5886,6845,5920,3888,3887,3885,6092,6109,6857,6844'
    elif country == 'ar':
        store_ids = '2,3,4,7,8,9,10,35,36,37,38,39,40,41,42,43,44,45,46,48,50,51,52,53,55,62,63,64,641,642,644,743,753,756,784,790,878,920,921,953,954,955,957,960,961,981,982,983,986,987,1002,1003,1004,1005,1044,1057,1117,1350,1351,1526,1527,1531,1872,1998,2060,2061,2091,2098,2153,2154,2233,2613,2614,2616,2617,2618,2644,2645,2657,2658,2659,2660,2940,2944,2962,2963,3258,3261,3278,3314,3365'
    elif country == 'pe':
        store_ids = '9,44,61,66,68,138,180,183,193,205,216,222,224,239,242,244,263,265,2736,2804,3014,3016,3405,3409,3410,3419,3462,3463,3524,3525'
    elif country == 'ec':
        store_ids = '22,23,24,25,26,28,29,30,31,32,34,62,76,101,119,127,152,153,166,167,230,231'
    elif country == 'cr':
        store_ids = '1,12,14,28,65,71,74,82,144,145,146,147,148,149'
    elif country == 'mx':
        store_ids = '2,3,4,6,7,10,11,12,13,17,18,19,22,23,25,26,27,28,33,48,49,62,77,78,79,158,159,167,168,169,170,172,173,174,175,176,200,202,236,442,443,444,445,446,447,448,449,450,456,457,500,502,608,627,630,1102,1107,1112,1123,1135,1140,1478,1544,1545,1546,1547,1563,1568,1570,1572,1573,1712,17191,17438,17445,17447,17449,17474,17494,17658,17668,17737,18682,20180,20382,21334,23898'
    elif country == 'co':
        store_ids = '19,6,7,3,51,1,337,8,2,4,7125,40,610,25,13,238,275,11,33,7634,153,7414,152,243,21,26,12,7957,20,311,109,110,7213,200,106,199,254,22,72,55,15,68,52,17,38,43,62,16,5,10,24,9,23,7877,392,7876,445'
    elif country == 'uy':
       store_ids = '6857,6844,1128675'
    
    timezone, interval = timezones.country_timezones(country)
    query = '''--no_cache
        with last as (select max(order_id) as maximo
    from order_summary os
    where
    now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}'
    and (os.place_at at time zone 'America/{timezone}')::date = (now() at time zone 'America/{timezone}')::date
    )
, os as (
 select physical_store_id, products_qty, state, was_taken, order_id, place_at
    from order_summary os 
    join last on os.order_id >= last.maximo - 1000000
    )
, base as (
select 
    physical_store_id::text,
    least(sum(case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then os.order_id else null end)*25) as "assigned (10m)",

least(sum(case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then os.order_id else null end)*25) as "in picking (30m)",

least(sum(case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then os.order_id else null end)*25) as "in payment (35m)",

least(sum(case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then os.order_id else null end)*25) as "payment requested (40m)",

least(sum(case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then os.order_id else null end)*25) as "bill reported (55m)",

least(sum(case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then os.order_id else null end)*25) as total_delayed
from 
    os
where 
    now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}'
    and (os.place_at at time zone 'America/{timezone}')::date = (now() at time zone 'America/{timezone}')::date
    and os.state not in ('finished','canceled','delivered_to_rt')
    and was_taken = true
    and physical_store_id in ({store_ids})
group by 
    1    
having
least(sum(case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'assigned' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '50 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_picking' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '30 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'in_payment' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '25 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'payment_requested' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '20 minutes' then os.order_id else null end)*25)
+
least(sum(case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then products_qty else 0 end),
count(distinct case when os.state = 'bill_reported' and now() at time zone 'America/{timezone}' > os.place_at at time zone 'America/{timezone}' - interval '5 minutes' then os.order_id else null end)*25) > 5
order by
    7 desc
)

select row_number() over (order by total_delayed desc) as numero, *, "assigned (10m)"+"in picking (30m)" as "Assign 10m + Picking 30m"
from base
order by 9 desc'''.format(timezone=timezone,store_ids=store_ids)
    
    if country == 'co':
        atrasos = redash.run_query(2453, query)
    elif country == 'ar':
        atrasos = redash.run_query(2427, query)
    elif country == 'cl':
        atrasos = redash.run_query(2428, query)
    elif country == 'mx':
        atrasos = redash.run_query(2454, query)
    elif country == 'uy':
        atrasos = redash.run_query(2429, query)
    elif country == 'ec':
        atrasos = redash.run_query(2555, query)
    elif country == 'cr':
        atrasos = redash.run_query(2551, query)
    elif country == 'pe':
        atrasos = redash.run_query(2452, query)
    elif country == 'br':
        atrasos = redash.run_query(2451, query)

    return atrasos

def slots_capacity(country,store_ids_atrasados):
    timezone, interval = timezones.country_timezones(country)
    query = f"""
        --no_cache
        set timezone= 'America/{timezone}';
        select
        physical_store_id::text as "physical_store_id::filter",
        (datetime_utc_iso::timestamp) as slot,
        id,
        is_closed,
        (sum(case when (datetime_utc_iso ) < (now()) then items_capacity*(60-extract(minute from now()))/60 else items_capacity end) -
        sum(case when (datetime_utc_iso ) < (now()) then reserved_items_capacity*(60-extract(minute from now()))/60 else reserved_items_capacity end))::int::text as capacidade_extra_restante,
        sum(items_capacity) as capacidad_total_slot
        from
        slots
        where
        datetime_utc_iso::timestamp >= now()::timestamp - interval '3 hour'
        and datetime_utc_iso::timestamp <= now()::timestamp + interval '3 hour'
        and datetime_utc_iso::date = now()::date
        and type = 'SHOPPER'
        and physical_store_id in ({store_ids_atrasados})
        and deleted_at is null
        group by 
        1
        , 2
        , 3
        , 4
        """
    if country == 'co':
        capacidade_slots = redash.run_query(1971, query)
    elif country == 'ar':
        capacidade_slots = redash.run_query(1968, query)
    elif country == 'cl':
        capacidade_slots = redash.run_query(1970, query)
    elif country == 'mx':
        capacidade_slots = redash.run_query(1972, query)
    elif country == 'uy':
        capacidade_slots = redash.run_query(1974, query)
    elif country == 'ec':
        capacidade_slots = redash.run_query(2556, query)
    elif country == 'cr':
        capacidade_slots = redash.run_query(2558, query)
    elif country == 'pe':
        capacidade_slots = redash.run_query(1973, query)
    elif country == 'br':
        capacidade_slots = redash.run_query(1969, query)

    return capacidade_slots

def scheduled_orders(country,store_ids_atrasados):
    timezone, interval = timezones.country_timezones(country)
    query = f"""
    --no_cache
    set timezone= 'America/{timezone}';

    with o as (
    select physical_store_id, place_at, state, picked_by,order_id,products_qty, was_taken
    from order_summary os 
    where
    os.place_at::timestamp >= now()::timestamp - interval '3h'
    and os.place_at::timestamp <= now()::timestamp + interval '3h'
    )

    ,orders as (
    select
    os.physical_store_id::text as id_fisico,
    os.place_at::date as data_agendamento,
    place_at as place_at,
    case when os.state ilike '%canceled%' then 'canceled'
         when os.state ilike '%scheduled%' then 'scheduled'
         when os.state in ('finished', 'pending_review') then 'finished'
         else 'live' end as state_agg,
    case when os.picked_by = 'shopper_or_rt' then 'yes'
         else 'no' end as picked_by_rt,
    os.state,
    os.order_id as order_id,
    os.products_qty as skus, was_taken
    from 
    o os
    where
    os.state not ilike '%canceled%'
    and os.state not in ('finished', 'pending_review')
    and (os.was_taken=true or os.picked_by in ('only_shopper'))
    and physical_store_id in ({store_ids_atrasados})
    )
    , user_seg as (
    select distinct
    oe.order_id,
    first_value(oe.params->'user_segmentation'->'finance_user_type') over (partition by oe.order_id ) as user_segm,
    first_value(oe.params->'user_segmentation'->'new_user_cpg') over (partition by oe.order_id ) as new_user_cpg,
    first_value(oe.params->'user_segmentation'->'segment_rfm') over (partition by oe.order_id ) as segment_rfm,
    first_value(oe.params->'user_segmentation'->>'hvu_prob') over (partition by oe.order_id ) as hvu_prob
    from
    orders o
    inner join
    order_events oe on oe.order_id = o.order_id and oe.name = 'create_order'
    )

    , resched as (
    select
    oe.order_id as order_id,
    count(oe.id) as rescheds
    from 
    orders o
    inner join
    order_events oe on oe.order_id = o.order_id and oe.name = 'change_to_scheduled'
    group by
    1
    )

    select 
    o.id_fisico::text as "ID_Fisico::multi-filter",
    o.data_agendamento as "Data_Agendamento::multi-filter",
    (o.place_at::timestamp - interval '1 hours') as place_at,
    case when 
o.id_fisico::int in (467,474,475,520,596,626,7586,7842,8874,9142,9741,10464,11277,11278,11889,12106,13193,14224,14244,14246,14257,14258,14259,14260,14263,14264,14265,14266,14267,14269,14271,14394,14395,14515,14518,14519,14520,14768,14769,14770,14849,15152,15154,15205,15214,15243,15270,15272,15274,32469,32470,32471,32664,33189,33192) 
and '{country}' = 'br' then extract(hour from o.place_at::timestamp) -2 
else extract(hour from o.place_at::timestamp) -1 end as "Inicio_Shopper_Slot::multi-filter",
    o.state_agg,
    o.state,
    case when (lower(us.user_segm::text) like '%hq%') then 'HQ' 
         when (lower(segment_rfm::text) like '%diamond%' and lower(segment_rfm::text) not like '%potential%diamond%') then 'Diamond'
         when hvu_prob::float >= 0.85 then 'High hvu_prob'
    else null end as user_hq,
    hvu_prob,
    o.order_id::text as order_id,
    was_taken,
    picked_by_rt,
    o.skus,
    coalesce(rescheds,0) as qtd_reagend


    from
    orders o
    left join
    user_seg us on us.order_id = o.order_id
    left join
    resched rc on rc.order_id = o.order_id
    """

    if country == 'co':
        ordens_programadas = redash.run_query(2449, query)
    elif country == 'ar':
        ordens_programadas = redash.run_query(2430, query)
    elif country == 'cl':
        ordens_programadas = redash.run_query(2431, query)
    elif country == 'mx':
        ordens_programadas = redash.run_query(2450, query)
    elif country == 'uy':
        ordens_programadas = redash.run_query(2426, query)
    elif country == 'ec':
        ordens_programadas = redash.run_query(2559, query)
    elif country == 'cr':
        ordens_programadas = redash.run_query(2552, query)
    elif country == 'pe':
        ordens_programadas = redash.run_query(2448, query)
    elif country == 'br':
        ordens_programadas = redash.run_query(2447, query)

    return ordens_programadas