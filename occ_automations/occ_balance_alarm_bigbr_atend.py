import os, sys, json, pytz, requests
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import thread_listener as tl
from lib import snowflake as snow
from datetime import datetime, timedelta

def get_last_alarms():
    try:
        latest = datetime.now()
        oldest = latest - timedelta(minutes=30)
        channel_name = 'CS7UQAMLG'

        channel_history = tl.get_channel_history(
            channel = channel_name,
            messages_per_page = 500,
            max_messages = 20000,
            oldest = oldest,
            latest = latest
        )

        last_alarms = channel_history[
            channel_history['text'].str.contains('Alerta de Balance - Big Cities BR')
        ]

        last_alarms = last_alarms[['ts', 'text']]
        last_alarms['city'] = last_alarms['text'].str.extract(r"(?<=\*Cidade\*:)(.*)")
        last_alarms['city'] = last_alarms['city'].str.strip() 

        return last_alarms['city'].unique().tolist()
    except:
        return []

def get_stakeholders(city):
    mappings = {
        'Grande São Paulo': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @murilo.diegues, @osmar.queiroz, @felipe.kim, @felipe.cerqueira @tijana.jankovic',
        'Rio de Janeiro': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @luiz.correa, @felipe.werner, @paulo.pinto @guilherme.mynssen',
        'Belo Horizonte': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias, @maria.maciel @wilson.cabral',
        'Porto Alegre': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @pedro.telles, @fernando.loureiro @eric.dhaese',
        'Curitiba': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @moacyr.baptista, @fernando.oliveira, @fernando.loureiro @eric.dhaese',
        'Fortaleza': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias, @maria.maciel @wilson.cabral',
        'Brasilia': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias, @maria.maciel @wilson.cabral',
        'Salvador': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias @wilson.cabral',
        'Recife': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias @wilson.cabral',
        'Campinas': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @francielle.henzler, @felipe.oliveira @augusto.quiros',
        'Goiânia': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @hugo.rios, @bruno.souza, @julliany.elias, @maria.maciel @wilson.cabral',
        'Florianópolis': '@simmon.nam, @anderson.taniguchi, @occ_monit-balance, @fernando.loureiro, @moacyr.baptista, @eric.dhaese'
    }

    try:
        return mappings[city]
    except:
        return ''

metrics = requests.get("http://services.rappi.com.br/api/al-api/zones/saturation")
metrics = metrics.json()
metrics = pd.DataFrame(metrics['zones'])

query_zones = """
select
    z.city_id as city_id,
    c.city as city_name,
    z.id as zone_id,
    z.name as zone_name
from br_pglr_ms_ops_zones_public.zones z 
join br_grability_public.city_addresses c on z.city_id = c.id
group by 1, 2, 3, 4
order by 1 asc, 3 asc
"""

zones = snow.run_query(query_zones)
zones = zones[zones['city_name'].isin([
    'Grande São Paulo','Rio de Janeiro','Belo Horizonte',
    'Porto Alegre','Curitiba','Fortaleza',
    'Brasilia','Salvador','Recife',
    'Campinas','Goiânia', 'Florianópolis'
])]

del zones['zone_name']

cities = zones['city_id'].unique().tolist()
cities = ["{}".format(city) for city in cities]
cities = ','.join(cities)

query_metrics = """
--no_cache
with base as (
    select
        zone_id,
        unassigned_orders,
        grability_city_id as city_id,
        rank() over (partition by zone_id order by created_at desc) as date_rank
    from zone_historic
    where
        grability_city_id in ({})
        and created_at >= now() - interval '10 minute'
)

select
    zone_id,
    city_id,
    unassigned_orders
from base
where date_rank = 1
""".format(cities)

unn = redash.run_query(2646, query_metrics)

try:
    metrics = metrics.merge(unn, on=['city_id', 'zone_id'])
    metrics = metrics.merge(zones, on=['city_id', 'zone_id'])
    metrics = metrics.drop_duplicates(subset=['zone_id', 'zone_name'])

    # metrics['unn_perc'] = (metrics['unassigned_orders'] / metrics['total_orders']) * 100.0
    # metrics['unn_perc'] = metrics['unn_perc'].fillna(0)
except:
    print('Sem resultados de UNN vindos da ba-saturations, possivelmente delay')
    sys.exit(0)

def summarize_cities(metrics):
    cities = metrics.groupby(
        ['city_id', 'city_name']
    ).agg(
        orders=('total_orders', 'sum'),
        unassigned_orders=('unassigned_orders', 'sum'),
        couriers=('total_couriers', 'sum')
    ).reset_index()
    
    cities['unn_perc'] = round((cities['unassigned_orders'] / cities['orders']) * 100, 1)
    cities['saturation'] = round(cities['orders'] / cities['couriers'], 1)
    
    return cities

cities = summarize_cities(metrics)

for index, row in cities.iterrows():
    city = row['city_name']
    city_id = row['city_id']
    
    orders = row['orders']
    unn = row['unassigned_orders']
    rts = row['couriers']

    unn_perc = row['unn_perc']
    saturation = row['saturation']

    last_cities = get_last_alarms()
    stakeholders = get_stakeholders(city)

    if (unn_perc > 20) and (unn > 5) and (city not in last_cities):
        trigger_text = """
        :alert: Alerta de Balance - Big Cities BR :alert:
        
        *FYI*: {stakeholders}
        
        *Cidade*: {city}
        *Unassigned - %*: {unn_perc}
        *Saturação*: {saturation}
        
        *Ordens*: {orders}
        *Ordens Unassgined*: {unn}
        *RTs*: {rts}
        """.format(
            stakeholders=stakeholders,
            city=city, 
            unn_perc=unn_perc, 
            saturation=saturation,
            orders=orders,
            unn=unn,
            rts=rts
        )

        slack.send_message_names_channel('CS7UQAMLG', trigger_text)