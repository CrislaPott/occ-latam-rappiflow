import os, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import gsheets, slack
from lib import thread_listener as tl
from lib import snowflake as snow

slack_users = pd.read_csv('input/slack_users.csv')
occ_users = gsheets.read_sheet('1aN4HB97rnldRgJDfFSA5tPDIoEnaZmlpNXLaRZBbXr4', 'users')

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
channel_name = 'CS7UQAMLG'

channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

workflows = channel_history[
    (channel_history['username'].notna()) &
    (channel_history['subtype'] == 'bot_message')
]

def get_occ_users(thread_users):
    try:
        usernames = slack_users[
            slack_users['slack_user_id'].isin(thread_users)
        ]['slack_user'].unique().tolist()

        occ_thread_users = occ_users[
            occ_users['analyst_name'].isin(usernames)
        ]['analyst_name'].unique().tolist()

        if len(occ_thread_users) == 0:
            return np.nan
        
        return occ_thread_users 
    except:
        return np.nan

def get_non_occ_users(thread_users):
    try:
        usernames = slack_users[
            slack_users['slack_user_id'].isin(thread_users)
        ]

        non_occ_thread_users = usernames[
            ~(usernames['slack_user'].isin(occ_users['analyst_name'].unique().tolist()))
        ]['slack_user'].unique().tolist()

        if len(non_occ_thread_users) == 0:
            return np.nan 
        
        return non_occ_thread_users
    except:
        return np.nan 

workflows['thread_ts'] = workflows['ts']
workflows['occ_users'] = workflows['reply_users'].apply(lambda x: get_occ_users(x))
workflows['non_occ_users'] = workflows['reply_users'].apply(lambda x: get_non_occ_users(x))

workflows.rename(columns={
    'username': 'workflow'
}, inplace=True)

workflows = workflows[['workflow', 'thread_ts', 'text', 'reply_users', 'occ_users', 'non_occ_users']]

def get_slack_user(user_id):
    try:
        username = slack_users[
            slack_users['slack_user_id'] == user_id
        ]['slack_user'].unique().tolist()[0]
        return username
    except:
        return np.nan

occ_replies = pd.DataFrame()
for index, row in workflows.iterrows():
    try: 
        messages = tl.get_thread_replies(channel_name, row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: get_slack_user(x))
        messages = messages[['thread_ts', 'ts', 'slack_user', 'text']]

        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['analyst_name'].unique().tolist())) &
            (messages['slack_user'].notna())
        ]

        occ_replies_thread = occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_occ_reply=('ts', 'min'),
            last_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        occ_replies = pd.concat([occ_replies, occ_replies_thread])
    except:
        pass

workflows = workflows.merge(occ_replies, on='thread_ts', how='left')

workflows['reply_users'] = workflows['reply_users'].astype(str)
workflows['occ_users'] = workflows['occ_users'].astype(str)
workflows['non_occ_users'] = workflows['non_occ_users'].astype(str)

print(workflows.head())

print('Fazendo upload das threads')
snow.upload_df(workflows, 'occ_atendimento_workflows')