import os, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from os.path import expanduser

from lib import redash, gsheets, slack
from lib import snowflake as snow
from lib import thread_listener as tl

countries = ['br', 'ar', 'co', 'cl', 'mx', 'pe', 'ec', 'uy', 'cr']
final = pd.DataFrame()

for country in countries:
    query = f"""
    with order_dates as (
        select
            max(created_at) as last_order,
            dateadd(day, -1, max(created_at)) as last_order_d_1
        from ops_global.global_orders
        where country = '{country.upper()}'
    )

    select
        o.application_user_id,
        o.country,
        count(distinct o.order_id) as num_orders,
        sum(o.gmv_usd) as sum_gmv_usd,
        listagg(o.bin, ',') as distinct_bins,
        listagg(o.order_id, ',') as distinct_orders
    from ops_global.global_orders o
    join global_support_ds.{country}_user_segmentation u
        on o.application_user_id = u.application_user_id and o.created_at::date = u.created_at::date
    join order_dates od on o.created_at >= od.last_order_d_1 and o.created_at <= od.last_order
    where
        u.segment_rfm in ('Regular','Zero','Low_Value')
        and o.state_type = 'canceled_by_user'
        and o.device_score in (1, 2, 3, 4)
        -- and o.gmv_usd >= 10
        and o.country = '{country.upper()}'
    group by 1, 2
    having count(distinct o.order_id) >= 3 and sum(o.gmv_usd) > 10
    order by 2 desc
    """

    results = snow.run_query(query)

    if results.shape[0] > 0:
        final = pd.concat([final, results], ignore_index=True)

home = expanduser("~")
final_file = f'{home}/fraud_latam.csv'

if final.shape[0] > 0:
    final.to_csv(final_file, index=False)
    text = """
    *Alarma de sospecha de fraude en LATAM* :male-detective::skin-tone-4: :alert:

    <!subteam^S018ASNLN5T>

    Usuarios con más de una orden cancelada en las últimas 24 horas en los siguientes aspectos:

    1. Segment RFM de lo usuario está entre ('Regular','Zero','Low_Value')
    2. Device Score de lo usuario está entre (1, 2, 3, 4)
    3. Soma de GMV en dólares de las órdenes canceladas por el usuario es mayor que 10 USD
    """
    slack.file_upload_channel(channel='C01672B528Y', text=text, filename=final_file, filetype='csv')

    if os.path.exists(final_file):
        os.remove(final_file)