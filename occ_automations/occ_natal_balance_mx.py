import os, sys, json
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import snowflake as snow

def get_db_interval(country):
    query = """
    select
        max(created_at) as last_refresh 
    from zone_historic
    """

    if country == 'co':
        last_refresh = redash.run_query(2645, query)
    elif country == 'ar':
        last_refresh = redash.run_query(2633, query)
    elif country == 'cl':
        last_refresh = redash.run_query(2627, query)
    elif country == 'mx':
        last_refresh = redash.run_query(2658, query)
    elif country == 'pe':
        last_refresh = redash.run_query(2626, query)
    elif country == 'uy':
        last_refresh = redash.run_query(2628, query)

    for column in last_refresh.columns:
        last_refresh[column] = pd.to_datetime(last_refresh['last_refresh'])

    time_diff = datetime.now() - last_refresh.loc[0, 'last_refresh']

    return time_diff.total_seconds() / 60

def get_zones(country):
    if country == 'co':
        query_zones = """
        select
            z.city_id as city_id,
            c.city as city_name,
            z.id as zone_id,
            z.name as zone_name
        from {country}_pglr_ms_ops_zones_public.zones z 
        join {country}_grability_public.city_addresses_vw c on z.city_id = c.id
        group by 1, 2, 3, 4
        order by 1 asc, 3 asc
        """.format(country=country)
    else:
        query_zones = """
        select
            z.city_id as city_id,
            c.city as city_name,
            z.id as zone_id,
            z.name as zone_name
        from {country}_pglr_ms_ops_zones_public.zones z 
        join {country}_grability_public.city_addresses c on z.city_id = c.id
        group by 1, 2, 3, 4
        order by 1 asc, 3 asc
        """.format(country=country)
        
    zones = snow.run_query(query_zones)
    zones = filter_zones(country, zones)
    metrics = get_metrics(country, zones)
    
    zones = zones.merge(metrics, on='zone_id')
    
    return zones

def filter_zones(country, zones):
    if country == 'co':
        mask = zones['city_name'].isin(['Bogotá', 'Barranquilla', 'Cali', 'Medellín', 'Cartagena'])
    elif country == 'ar':
        mask = zones['city_name'].isin(['Buenos Aires'])
    elif country == 'cl':
        mask = zones['city_name'].isin(['Santiago de Chile', 'Viña del Mar', 'Antofagasta', 'Concepcion'])
    elif country == 'mx':
        mask = zones['city_name'].isin(['Ciudad de México', 'Monterrey', 'Cancún', 'Guadalajara'])
    elif country == 'pe':
        mask = zones['city_name'].isin(['Lima'])
    
    return zones[mask]

def get_metrics(country, zones):
    cities = zones['city_id'].unique().tolist()
    cities = ["{}".format(city) for city in cities]
    cities = ','.join(cities)
    
    query_metrics = """
    --no_cache
    with base as (
        select
            zone_id,
            couriers,
            free_couriers,
            orders,
            unassigned_orders,
            saturation,
            grability_city_id,
            created_at,
            rank() over (partition by zone_id order by created_at desc) as date_rank
        from zone_historic
        where
            grability_city_id in ({})
    )

    select
        zone_id,
        grability_city_id,
        orders,
        unassigned_orders,
        couriers,
        free_couriers
    from base
    where date_rank = 1
    """.format(cities)
    
    if country == 'co':
        metrics = redash.run_query(2645, query_metrics)
    elif country == 'ar':
        metrics = redash.run_query(2633, query_metrics)
    elif country == 'cl':
        metrics = redash.run_query(2627, query_metrics)
    elif country == 'mx':
        metrics = redash.run_slow_query(2658, query_metrics)
    elif country == 'pe':
        metrics = redash.run_query(2626, query_metrics)
        
    return metrics

def summarize_cities(zones):
    cities = zones.groupby(
        ['city_id', 'city_name']
    ).agg(
        orders=('orders', 'sum'),
        unassigned_orders=('unassigned_orders', 'sum'),
        couriers=('couriers', 'sum'),
        free_couriers=('free_couriers', 'sum')
    ).reset_index()
    
    cities['unn_perc'] = round((cities['unassigned_orders'] / cities['orders']) * 100, 1)
    cities['saturation'] = round(cities['orders'] / cities['couriers'], 1)
    
    return cities

def run_alarm(country):
    zones = get_zones(country)
    cities = summarize_cities(zones)
    
    for index, row in cities.iterrows():
        city = row['city_name']
        city_id = row['city_id']
        
        orders = row['orders']
        unn = row['unassigned_orders']
        rts = row['couriers']
        rts_livres = row['free_couriers']
        unn_perc = row['unn_perc']
        saturation = row['saturation']
        
        if (unn_perc > 12 and unn_perc <= 20) or (saturation > 1):
            trigger_text = """
            *Alerta de Balance Cidades LATAM*

            *FYI*: @gloeza @aracely.yanez @alejandro.castaneda @agutierrezr @maria.leal
            
            *País*: {country} - :flag-{country_icon}:
            
            *Cidade*: {city}
            *Unassigned - %*: {unn_perc}
            *Saturação*: {saturation}
            
            *Ordens*: {orders}
            *Ordens Unassgined*: {unn}
            *RTs*: {rts}
            *RTs Livres*: {rts_livres}
            
            """.format(
                country=country.upper(), 
                country_icon=country, 
                city=city, 
                unn_perc=unn_perc, 
                saturation=saturation,
                orders=orders,
                unn=unn,
                rts=rts,
                rts_livres=rts_livres
            )
            
            slack.send_message_names_channel('CL1HEBCMD', trigger_text)

        if (unn_perc > 20) and (unn > 15):
            trigger_text = """
            :alert: Alarma Alto Unassigned! :alert:

            *FYI*: @gloeza @aracely.yanez @alejandro.castaneda @agutierrezr @maria.leal @occ_monit-balance @yasmin.pedro @pedro.ayres @paula @jorge.donado @ilan.schuster
            
            *País*: {country} - :flag-{country_icon}:
            
            *Cidade*: {city}
            *Unassigned - %*: {unn_perc}
            *Saturação*: {saturation}
            
            *Ordens*: {orders}
            *Ordens Unassgined*: {unn}
            *RTs*: {rts}
            *RTs Livres*: {rts_livres}
            
            """.format(
                country=country.upper(), 
                country_icon=country, 
                city=city, 
                unn_perc=unn_perc, 
                saturation=saturation,
                orders=orders,
                unn=unn,
                rts=rts,
                rts_livres=rts_livres
            )
            
            slack.send_message_names_channel('CS7UQAMLG', trigger_text)
            
countries = ['mx']
for country in countries:
    db_delay = get_db_interval(country)
    if db_delay <= 11:
        run_alarm(country)
        print('Rodou: {}'.format(country))
    else:
        print('Delay da base do país {}: {}'.format(country, db_delay))