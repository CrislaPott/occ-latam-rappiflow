import os, sys, json, requests 
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta

from lib import thread_listener as tl
from lib import snowflake as snow

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
users = pd.read_csv('input/slack_users.csv')

channel_history = tl.get_channel_history(
    channel = "C01GW80MFKJ",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].astype(str)

msgs = channel_history[
    (channel_history['username'].str.contains('Massive Slots Management'))
].reset_index()

df = msgs[['ts', 'text']]

df['slack_user_id'] = df['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
df['reason'] = df['text'].str.extract(r"(?<=\*Razón\*)(.*)(?=\*País\*)")
df['country'] = df['text'].str.extract(r"(?<=flag-[a-z]{2}:)(.*)(?=\*ID de tienda)")
df['physical_store_id'] = df['text'].str.extract(r"(?<=ID de tienda física\*)(.*)(?=\*Tipo)")
df['physical_store_id'] = df['physical_store_id'].str.split(',')
df['type'] = df['text'].str.extract(r"(?<=Tipo\*)(.*)(?=\*Fecha)")
df['date'] = df['text'].str.extract(r"(?<=slots\*)(.*)(?=\*Hora de inicio)")
df['date'] = pd.to_datetime(df['date'], dayfirst=True, errors='coerce').dt.date 
df['first_hour'] = df['text'].str.extract(r"(?<=Hora de inicio\*)(.*)(?=\*Hora fin)")
df['last_hour'] = df['text'].str.extract(r"(?<=Hora fin\*)(.*)(?=\*Opción)")
df['option'] = df['text'].str.extract(r"(?<=de cierre\*)(.*)(?=\*Acción)")
df['action'] = df['text'].str.extract(r"(?<=circle:)(.*)")

for column in channel_history.columns:
    channel_history[column] = channel_history[column].str.strip()

normalized_df = pd.DataFrame()
for index, row in df.iterrows():
    stores = row['physical_store_id']
    for store in stores:
        try:
            store = int(store)
            temp = df[df.index == index]
            temp['physical_store_id'] = store
            normalized_df = pd.concat([normalized_df, temp], ignore_index=True)
        except:
            pass

normalized_df = normalized_df.merge(users, on='slack_user_id', how='left')

del normalized_df['slack_user_id']

print(normalized_df.head())

snow.upload_df(normalized_df, 'occ_logs_slots_mgmt')