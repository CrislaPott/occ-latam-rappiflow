import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)

slack_users = pd.read_csv('input/slack_users.csv')

channel_mapping = {
    'C015SRHGHLY': 'lojas_rio_2',
    'C016SA516GG': 'lojas_rio_1',
    'C0172H3TMHP': 'lojas_nordeste',
    'C016Q12NVPV': 'lojas_sul',
    'C01CKH3F7UN': 'lojas_sp_1',
    'C01DGCHAEDN': 'lojas_sp_2',
    'C01CYQQG352': 'lojas_sp_3',
    'C01DGJR614G': 'lojas_sp_4',
    'C016V5K9DTK': 'lojas_bh',
    'C016REGSD44': 'lojas_centro_oeste'
}

def get_users(user_ids):
    try:
        user_ids = list(user_ids)
        users = slack_users[slack_users['slack_user_id'].isin(user_ids)]
        return users['slack_user'].unique().tolist()
    except:
        return None
    
def get_attendance_time(latest_reply, thread_ts):
    try:
        interval = datetime.fromtimestamp(float(latest_reply)) - datetime.fromtimestamp(float(thread_ts))
        minutes = interval.total_seconds() / 60
        return minutes
    except:
        return None
    
total_history = pd.DataFrame()
for channel in list(channel_mapping.keys()):
    channel_history = tl.get_channel_history(
        channel = channel,
        messages_per_page = 500,
        max_messages = 20000,
        oldest = oldest,
        latest = latest
    )
    channel_history.loc[:, 'channel_id'] = channel
    channel_history['thread_id'] = channel + '#' + channel_history['thread_ts']
    channel_history['analyst_names'] = channel_history['reply_users'].apply(lambda x: get_users(x))
    total_history = pd.concat([total_history, channel_history], ignore_index=True)
    
total_history = total_history[
    (~total_history['username'].isna()) &
    (~total_history['thread_ts'].isna())
]

total_history['channel_name'] = total_history['channel_id'].map(channel_mapping)
total_history['attendance_time_minutes'] = total_history.apply(lambda x: get_attendance_time(x.latest_reply, x.thread_ts), axis=1)
total_history['analyst_names'] = total_history['analyst_names'].astype(str)

total_history = total_history[[
    'thread_id',
    'channel_name',
    'username',
    'thread_ts',
    'latest_reply',
    'analyst_names',
    'attendance_time_minutes'
]]

total_history.rename(columns={'username': 'thread_name'}, inplace=True)

print(total_history.head())

print('Fazendo upload das threads')
snow.upload_df(total_history, 'occ_store_threads_br')

