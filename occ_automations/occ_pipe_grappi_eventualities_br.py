import os, sys, json
import numpy as np 
import pandas as pd 
from lib import redash 
from lib import snowflake as snow 

# Eventualities - BR
br_eventualities = pd.DataFrame()

query = """
select *
from audit 
where 
    endpoint = '/api/operation-management/zones/eventualities'
    and created_at >= now() - interval '10 minute'
order by created_at desc
"""

results = redash.run_query(823, query)

if results.shape[0] == 0:
    print('Sem novos registros')
    sys.exit(0)

def get_event(zone):
    try:
        return zone['eventualities']['level']
    except:
        return np.nan

def get_event_duration(zone):
    try:
        return zone['eventualities']['level_duration']
    except:
        return np.nan

def get_raining(zone):
    try:
        return zone['eventualities']['raining']
    except:
        return np.nan

def get_raining_duration(zone):
    try:
        return zone['eventualities']['raining_duration']
    except:
        return np.nan

for index, row in results.iterrows():
    created_at_utc = pd.to_datetime(row['created_at'])
    city = row['city']
    user = row['user']
    changes = json.loads(row['changes'])
    zones = changes['zones']
    for zone in zones:
        try:
            line = {
                'city': city,
                'created_at_utc': created_at_utc,
                'user': user,
                'zone': zone['name'],
                'event': get_event(zone),
                'event_duration': get_event_duration(zone),
                'raining': get_raining(zone),
                'raining_duration': get_raining_duration(zone)
            }
            temp = pd.DataFrame([line])
            br_eventualities = pd.concat([br_eventualities, temp], ignore_index=True)
        except:
            pass

print(br_eventualities.head())

snow.upload_df(br_eventualities, 'occ_grappi_logs_eventualities_br')