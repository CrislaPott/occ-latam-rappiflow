import os, re, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

def get_db_ids(country):
    # Orders Core, dispatch, cpgops_events_ms
    mappings = {
        'co': [1904, 13, 2449],
        'br': [1338, 231, 2447],
        'ar': [1337, 232, 2430],
        'cl': [1155, 309, 2431],
        'mx': [1371, 205, 2450],
        'cr': [1921, 2179, 2552],
        'uy': [1156, 306, 2426],
        'ec': [1922, 2177, 2559],
        'pe': [1157, 684, 2448]
    }
    return mappings[country]

def get_dispatcher_delay(orders_db, dispatcher_db):
    query1 = """
    select
        max(created_at) as last_update
    from courier_dispatcher.order_iterations
    """

    query2 = """
    select
        max(created_at) as last_update
    from orders
    """

    last_update_dispatcher = arrow.get(redash.run_query(dispatcher_db, query1).loc[0, 'last_update'])
    last_update_orders = arrow.get(redash.run_query(orders_db, query2).loc[0, 'last_update'])

    if last_update_dispatcher < last_update_orders:    
        delta = last_update_orders - last_update_dispatcher
        delta = round(delta.seconds / 60, 0)
    else:
        delta = 0

    return delta

def get_orders(db_id):
    query = """
    select
        distinct order_id
    from order_events
    where
        created_at >= (select max(created_at) from order_events) - interval '15 minute'
        and created_at <= (select max(created_at) from order_events)
        and name = 'order_to_rt'
    """

    results = redash.run_query(db_id, query)
    results = results['order_id'].unique().tolist()
    return results 

def filter_orders(db_id, order_list):

    order_list = ["{}".format(order_id) for order_id in order_list]
    order_list = ','.join(order_list)

    query = """
    with base as ( 
        select
            order_id,
            min(created_at) as first_date,
            max(created_at) as last_date
        from order_modifications
        where order_id in ({order_list})
        group by 1
    ),

    base2 as (
        select
            order_id,
            extract(minute from last_date - first_date) as time_elapsed
        from base
    )

    select 
        o.id as order_id,
        os.store_id as store_id,
        os.store_type_group as store_type_group
    from 
    orders o join order_stores os on o.id = os.order_id
    join base2 b on o.id = b.order_id
    join delivery_order dor on o.id = dor.order_id
    where o.id in ({order_list})
        and o.state not in ('finished', 'pending_review')
        and o.place_at is null
        and os.is_marketplace is False
        and b.time_elapsed >= 10
        and dor.delivery_method <> 'national_delivery'
        and os.type <> 'viajes'
    """.format(order_list=order_list)

    results = redash.run_query(db_id, query)
    return results 

def get_iteration_info(db_id, df):
    order_ids = df['order_id'].unique().tolist()
    order_ids = ["{}".format(order_id) for order_id in order_ids]
    order_ids = ','.join(order_ids)

    query = """
    WITH

    iterations AS
    (
        SELECT created_at, iteration_id, oi.order_id, payload, meta_data ->> 'constraint_stats' AS constraint_stats
        FROM courier_dispatcher.order_iterations oi
        WHERE order_id in ({})
    ),

    prospects AS
    (
        SELECT i.order_id, i.iteration_id, cop.courier_id, CASE WHEN com.order_id IS NULL THEN TRUE ELSE FALSE END AS is_free
        FROM courier_dispatcher.courier_order_prospects cop
            JOIN iterations i USING(iteration_id, order_id)
            LEFT JOIN courier_dispatcher.courier_order_matches com ON cop.courier_id = com.courier_id AND cop.iteration_id = com.iteration_id AND cop.order_id != com.order_id
    ),

    prospects_info AS
    (
        SELECT
            p.iteration_id,
            p.order_id,
            COUNT(*) AS total_prospects,
            COUNT(*) FILTER(WHERE p.is_free) AS available_prospects
        FROM prospects p
        GROUP BY 1,2
    ),

    iteration_info AS
    (
        SELECT
            i.created_at, i.iteration_id, i.order_id,
            CASE WHEN pi.order_id IS NOT NULL THEN total_prospects ELSE 0 END AS total_prospects,
            CASE WHEN pi.order_id IS NOT NULL THEN available_prospects ELSE 0 END AS available_prospects,
            com.courier_id AS notified_courier,
            com.taken_at,
            i.payload,
            i.constraint_stats
        FROM iterations i
            LEFT JOIN prospects_info pi ON i.iteration_id = pi.iteration_id AND i.order_id = pi.order_id
            LEFT JOIN courier_dispatcher.courier_order_matches com ON i.iteration_id = com.iteration_id AND i.order_id = com.order_id
        ORDER BY i.order_id, i.created_at DESC
    )

    SELECT * FROM iteration_info
    """.format(order_ids)

    results = redash.run_query(db_id, query)
    return results 

# countries = ['ar', 'cl', 'mx', 'cr', 'uy', 'ec', 'pe', 'co', 'br']
countries = ['ar', 'cr', 'uy', 'co', 'br']

for country in countries:
    try:
        db_ids = get_db_ids(country)

        dispatcher_delay = get_dispatcher_delay(db_ids[0], db_ids[1])

        if dispatcher_delay >= 5:
            trigger_text = """
            :alert: *Dispatcher MS - Delay - {country_upper}* :flag-{country}: :alert:

            Se registra delay en la tabla de Dispatcher para lo pais informado. Lo delay es de *{dispatcher_delay}* minutos actualmente.
            """.format(country=country, country_upper=country.upper(), dispatcher_delay=dispatcher_delay)

            slack.send_message_names_channel('C01H2ED81TQ', trigger_text)
            print('Rodou {}'.format(country))
            continue

        order_list = get_orders(db_ids[2])
        if len(order_list) == 0:
            print('Rodou {}'.format(country))
            continue

        orders = filter_orders(db_ids[0], order_list)
        if orders.shape[0] == 0:
            print('Rodou {}'.format(country))
            continue

        iterations = get_iteration_info(db_ids[1], orders)

        if iterations.shape[0] > 0:
            iterated_orders = iterations['order_id'].unique().tolist()
            orders = orders[~orders['order_id'].isin(iterated_orders)]

        if orders.shape[0] == 0:
            print('Rodou {}'.format(country))
            continue

        suspect_orders_file = 'orders_sin_interaccion_order_to_rt{}.csv'.format(country)
        orders.to_csv(suspect_orders_file, index=False)

        trigger_text = """
        :alert: *Alarma de Ordenes sin Interaccion - Order to RT* - {country_upper} :flag-{country}: :alert:

        <!subteam^S018ASNLN5T>

        La seguinte relacción de las *{num_orders}* órdenes sob lo evento *order_to_rt* en los ultimos 15 minutos presentan-se sin interaccion del algoritmo.
        """.format(country_upper=country.upper(), country=country, num_orders=orders.shape[0])

        if country == 'co':
            slack.file_upload_channel(channel='C01C1LSQREW', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'br':
            slack.file_upload_channel(channel='C01672B528Y', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'ar':
            slack.file_upload_channel(channel='C01EEPH2ECU', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'cl':
            slack.file_upload_channel(channel='C01D5GZ9A4X', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'mx':
            slack.file_upload_channel(channel='C01GMHJRDM2', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'cr':
            slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'uy':
            slack.file_upload_channel(channel='C01EEPH2ECU', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'ec':
            slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')
        elif country == 'pe':
            slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')

        if os.path.exists(suspect_orders_file):
            os.remove(suspect_orders_file)

        print('Rodou {}, Casos: {}'.format(country, orders.shape[0]))
    except:
        print('Erro ao rodar {}'.format(country))