import os, sys, json, requests, time
import numpy as np
import pandas as pd
from lib import gsheets, redash, slack, sfx
from lib import thread_listener as tl
from lib import snowflake as snow
from datetime import datetime, timedelta,date
from os.path import expanduser
from airflow.models import Variable
import pytz

tz = pytz.timezone('America/Bogota')
co_time = datetime.now(tz)
co_time = co_time.strftime("%Y-%m-%d %H:%M:%S")

latest = datetime.now()
oldest = latest - timedelta(minutes=20)
channel_name = 'C01GW80MFKJ'

channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest)
if channel_history.shape[0] == 0:
    sys.exit(0)

channel_history = channel_history[channel_history['username'] == 'OCC - Massive Slots Management']
channel_history.sort_values(by='ts', ascending=True, inplace=True)

if channel_history.shape[0] == 0:
    sys.exit(0)

users = pd.read_csv('input/slack_users.csv')
# authorized_users = pd.read_excel('input/authorized_users_massivo.xlsx')
authorized_users = gsheets.read_sheet('1ckKP6Cd7ZLwBcUxMg5g8dLqkvYfojteqQsbhGuXkcuQ', 'users')

authorized_users['slack_user'] = authorized_users['slack_user'].str.strip().str.lower()
users['slack_user'] = users['slack_user'].str.strip().str.lower()

authorized_users = users.merge(authorized_users, on='slack_user')

print("Authorized Users: {}".format(authorized_users.shape[0]))

channel_history['slack_user_id'] = channel_history['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
channel_history['reason'] = channel_history['text'].str.extract(r"(?<=Razón\*)(.*)(?=\*País)")
if channel_history['reason'].isna().any():
    channel_history['reason'] = channel_history['text'].str.extract(r"(?<=\*Razón \*)(.*)(?=\*País)")
channel_history['country'] = channel_history['text'].str.extract(r"(?<=País\*)(.*)(?=\*ID de tienda física)")
if channel_history['country'].isna().any():
    channel_history['country'] = channel_history['text'].str.extract(r"(?<=País \*)(.*)(?=\*ID de tienda física)")
channel_history['country'] = channel_history['country'].str.extract(r"(?<=:\ )(.*)")
channel_history['store_id'] = channel_history['text'].str.extract(r"(?<=ID de tienda física\*)(.*)(?=\*Tipo)")
if channel_history['store_id'].isna().any():
    channel_history['store_id'] = channel_history['text'].str.extract(r"(?<=ID de tienda física \*)(.*)(?=\*Tipo)")
channel_history['type'] = channel_history['text'].str.extract(r"(?<=Tipo\*)(.*)(?=\*Fecha de los slots)")
if channel_history['type'].isna().any():
    channel_history['type'] = channel_history['text'].str.extract(r"(?<=Tipo \*)(.*)(?=\*Fecha de los slots)")
channel_history['date'] = channel_history['text'].str.extract(r"(?<=Fecha de los slots\*)(.*)(?=\*Hora de inicio)")
if channel_history['date'].isna().any():
    channel_history['date'] = channel_history['text'].str.extract(r"(?<=Fecha de los slots \*)(.*)(?=\*Hora de inicio)")
channel_history['begin_hour'] = channel_history['text'].str.extract(r"(?<=Hora de inicio\*)(.*)(?=\*Hora fin)")
if channel_history['begin_hour'].isna().any():
    channel_history['begin_hour'] = channel_history['text'].str.extract(r"(?<=Hora de inicio \*)(.*)(?=\*Hora fin)")
channel_history['end_hour'] = channel_history['text'].str.extract(r"(?<=Hora fin\*)(.*)(?=\*Opción de cierre\*)")
if channel_history['end_hour'].isna().any():
    channel_history['end_hour'] = channel_history['text'].str.extract(r"(?<=Hora fin \*)(.*)(?=\*Opción de cierre \*)")
channel_history['action_method'] = channel_history['text'].str.extract(r"(?<=Opción de cierre\*)(.*)(?=\*Acción\*)")
if channel_history['action_method'].isna().any():
    channel_history['action_method'] = channel_history['text'].str.extract(r"(?<=Opción de cierre \*)(.*)(?=\*Acción \*)")
channel_history['action'] = channel_history['text'].str.extract(r"(?<=Acción\*)(.*)")
if channel_history['action'].isna().any():
    channel_history['action'] = channel_history['text'].str.extract(r"(?<=Acción \*)(.*)")
channel_history['action'] = channel_history['action'].str.extract(r"(?<=:\ )(.*)")

channel_history = channel_history[['ts', 'slack_user_id', 'country', 'reason', 'store_id', 'type', 'date', 'begin_hour', 'end_hour', 'action_method', 'action']]
print(channel_history)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].str.strip()

channel_history['date'] = pd.to_datetime(channel_history['date'], dayfirst=True, errors='coerce').dt.date

channel_history = channel_history.merge(authorized_users, on='slack_user_id')

if channel_history.shape[0] == 0:
    sys.exit(0)

print(channel_history.loc[0, :])

def get_slot_ids(country, store_id, slot_type, date, selected_hours):
    if slot_type == 'Shopper':
        slot_type = 'SHOPPER'
    elif slot_type == 'RT':
        slot_type = 'RT'
        
    query = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::text as "hora_agendamento"
    from br_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (datetime_utc_iso - interval '3 hour')::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)

    query_ar = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::text as "hora_agendamento"
    from ar_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (datetime_utc_iso - interval '3 hour')::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)

    query_cl = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::text as "hora_agendamento"
    from cl_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Recife', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)
    
    query_co = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::text as "hora_agendamento"
    from co_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)
    
    query_pe = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::text as "hora_agendamento"
    from pe_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)

    query_ec = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::text as "hora_agendamento"
    from ec_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Bogota', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)

    query_cr = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE( 'America/Costa_Rica', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE( 'America/Costa_Rica', datetime_utc_iso))::text as "hora_agendamento"
    from cr_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE( 'America/Costa_Rica', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE( 'America/Costa_Rica', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)

    query_mx = """
    select
        id as slot_id,
        physical_store_id,
        (CONVERT_TIMEZONE('America/Mexico_City', datetime_utc_iso))::date as "dia_agendamento",
        extract(hour from CONVERT_TIMEZONE('America/Mexico_City', datetime_utc_iso))::text as "hora_agendamento"
    from mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (CONVERT_TIMEZONE('America/Mexico_City', datetime_utc_iso))::date = '{date}'
        and extract(hour from CONVERT_TIMEZONE('America/Mexico_City', datetime_utc_iso)) in ({selected_hours})
    """.format(store_id=store_id, slot_type=slot_type, date=date, selected_hours=selected_hours)
    
    if country == 'BR':
        query_results = snow.run_query(query)
    elif country == 'AR':
        query_results = snow.run_query(query_ar)
    elif country == 'CO':
        query_results = snow.run_query(query_co)
    elif country == 'CL':
        query_results = snow.run_query(query_cl)
    elif country == 'PE':
        query_results = snow.run_query(query_pe)
    elif country == 'EC':
        query_results = snow.run_query(query_ec)
    elif country == 'CR':
        query_results = snow.run_query(query_cr)
    elif country == 'MX':
        query_results = snow.run_query(query_mx)
        
    query_results['country'] = country

    print(query_results)
    
    return query_results

def change_slot_status(country, slot_id, status, iteration=0):
    """Change the slots status."""
   
    headers = {
        'Authorization': 'postman',
        'API_KEY': Variable.get('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
        'x-rappi-team': 'OCC',
        'x-application-id': 'rappiflow-latam'
    }
    
    data = {
        "is_closed": not status
    }
    
    if country == 'BR':
        response = requests.put(
            'http://services.rappi.com.br/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'AR':
        response = requests.put(
            'http://services.rappi.com.ar/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CL':
        response = requests.put(
            'http://services.rappi.cl/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CO':
        response = requests.put(
            'http://services.rappi.com/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'PE':
        response = requests.put(
            'https://services.rappi.pe/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'EC':
        response = requests.put(
            'https://services.rappi.com.ec/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CR':
        response = requests.put(
            'https://services.rappi.co.cr/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'MX':
        response = requests.put(
            'https://services.mxgrability.rappi.com/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
    )
    
    if response.status_code == 200:
        return 'Sim'
    else:
        return 'Nao'
       
for index, row in channel_history.iterrows():
    try:
        if ':' in row['begin_hour']:
            row['begin_hour'] = row['begin_hour'].split(':')[0]
        if ':' in row['end_hour']:
            row['end_hour'] = row['end_hour'].split(':')[0]
        
        if row['action_method'] == 'Todos los slots':
            selected_hours = [hour for hour in range(int(row['begin_hour']), int(row['end_hour'])+1)]
            selected_hours = ','.join([str(hour) for hour in selected_hours])
        if row['action_method'] == 'Alternar cierres':
            selected_hours = [hour for hour in range(int(row['begin_hour']), int(row['end_hour'])+1)]
            selected_hours = [hour for hour in selected_hours if selected_hours.index(hour) % 2 == 0]
            selected_hours = ','.join([str(hour) for hour in selected_hours])

        print(row['country'])
        print(row['store_id'])
        print(row['type'])
        print(row['date'])
        print(selected_hours)

        slot_ids = get_slot_ids(row['country'], row['store_id'], row['type'], row['date'], selected_hours)

        print(slot_ids)

        if slot_ids.shape[0] == 0:
            texto = 'Sin slots a impactar en lo range de horarios informado.'
            slack.send_message_thread('C01GW80MFKJ', row['ts'], text=texto)
            continue

        slot_ids['motivo'] = row['reason']
        slot_ids['usuario_slack'] = row['slack_user']
        slot_ids['slot_type'] = row['type']

        print("aqui1")

        if row['action'] == 'Abrir':
            slot_ids['acao_tomada'] = slot_ids.apply(lambda x: change_slot_status(x.country, x.slot_id, True, iteration=0), axis=1)
            try:
                sfx.send_events_slotDitchesOpen(row['store_id'], row['store_id'], row['country'], row['reason'], co_time)
            except Exception as e:
                print(e)
        elif row['action'] == 'Cerrar':
            slot_ids['acao_tomada'] = slot_ids.apply(lambda x: change_slot_status(x.country, x.slot_id, False, iteration=0), axis=1)
            try:
                sfx.send_events_slotDitchesClose(row['store_id'], row['store_id'], row['country'], row['reason'], co_time)
            except Exception as e:
                print(e)
    except:
        texto = 'Tuve algunas dificultades para procesar tu solicitud. Verifique que todos los campos estén ingresados ​​correctamente e intente nuevamente.'
        slack.send_message_thread('C01GW80MFKJ', row['ts'], text=texto)
        continue

    slot_ids['thread_ts'] = row['ts']
    home = expanduser("~")
    slot_ids.to_csv('{}/occ_massivo_slot_ids.csv'.format(home), index=False)
    snow.upload_df_occ(slot_ids, 'occ_slots_massivo_logs')

    texto = 'A continuación, se muestra una lista de los slots afectados por la solicitud:'
    slack.file_upload_thread(channel='C01GW80MFKJ', thread_ts=row['ts'], text=texto, filename='{}/occ_massivo_slot_ids.csv'.format(home), filetype='csv')

