import os, sys, json, requests, time
import numpy as np
import pandas as pd
from lib import gsheets, redash, slack
from lib import thread_listener as tl
from lib import snowflake as snow
from datetime import datetime, timedelta
from airflow.models import Variable

latest = datetime.now()
oldest = latest - timedelta(minutes=15)

channel_history = tl.get_channel_history(
    channel = "C01GW80MFKJ",
    messages_per_page = 100,
    max_messages = 2000,
    oldest = oldest,
    latest = latest
)

if channel_history.shape[0] == 0:
    sys.exit(0)

channel_history = channel_history[channel_history['username'] == 'OCC - Massive Slots Management']
channel_history.sort_values(by='ts', ascending=True, inplace=True)

if channel_history.shape[0] == 0:
    sys.exit(0)
    
users = pd.read_csv('input/slack_users.csv')
# authorized_users = pd.read_excel('input/authorized_users_massivo.xlsx')
authorized_users = gsheets.read_sheet('1ckKP6Cd7ZLwBcUxMg5g8dLqkvYfojteqQsbhGuXkcuQ', 'users')

authorized_users['slack_user'] = authorized_users['slack_user'].str.strip().str.lower()
users['slack_user'] = users['slack_user'].str.strip().str.lower()

authorized_users = users.merge(authorized_users, on='slack_user')

print("Authorized Users: {}".format(authorized_users.shape[0]))

channel_history['slack_user_id'] = channel_history['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
channel_history['country'] = channel_history['text'].str.extract(r"(?<=País\*)(.*)(?=\*Physical)")
channel_history['country'] = channel_history['country'].str.extract(r"(?<=:\ )(.*)")
channel_history['store_id'] = channel_history['text'].str.extract(r"(?<=Store ID\*)(.*)(?=\*Type)")
channel_history['type'] = channel_history['text'].str.extract(r"(?<=Type\*)(.*)(?=\*Data)")
channel_history['date'] = channel_history['text'].str.extract(r"(?<=Data dos Slots\*)(.*)(?=\*Hora Inicial)")
channel_history['begin_hour'] = channel_history['text'].str.extract(r"(?<=Hora Inicial\*)(.*)(?=\*Hora Final)")
channel_history['end_hour'] = channel_history['text'].str.extract(r"(?<=Hora Final\*)(.*)(?=\*Ação\*)")
channel_history['action'] = channel_history['text'].str.extract(r"(?<=Ação\*)(.*)")
channel_history['action'] = channel_history['action'].str.extract(r"(?<=:\ )(.*)")

channel_history = channel_history[['ts', 'slack_user_id', 'country', 'store_id', 'type', 'date', 'begin_hour', 'end_hour', 'action']]

for column in channel_history.columns:
    channel_history[column] = channel_history[column].str.strip()
    
channel_history['date'] = pd.to_datetime(channel_history['date'], dayfirst=True, errors='coerce').dt.date 

channel_history = channel_history.merge(authorized_users, on='slack_user_id')

if channel_history.shape[0] == 0:
    sys.exit(0)

print(channel_history.head())

def get_slot_ids(country, store_id, slot_type, date, begin_hour, end_hour):
    if slot_type == 'Shopper':
        slot_type = 'SHOPPER'
    elif slot_type == 'RT':
        slot_type = 'RT'
        
    query = """
    select
        id as slot_id,
        physical_store_id,
        (datetime_utc_iso at time zone 'America/Recife')::date as "dia_agendamento",
        extract(hour from datetime_utc_iso at time zone 'America/Recife')::text as "hora_agendamento"
    from slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (datetime_utc_iso - interval '3 hour')::date = '{date}'
        and extract(hour from datetime_utc_iso at time zone 'America/Recife') >= {begin_hour}
        and extract(hour from datetime_utc_iso at time zone 'America/Recife') <= {end_hour}
    """.format(store_id=store_id, slot_type=slot_type, date=date, begin_hour=begin_hour, end_hour=end_hour)
    
    query_co = """
    select
        id as slot_id,
        physical_store_id,
        (datetime_utc_iso at time zone 'America/Bogota')::date as "dia_agendamento",
        extract(hour from datetime_utc_iso at time zone 'America/Bogota')::text as "hora_agendamento"
    from slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (datetime_utc_iso at time zone 'America/Bogota')::date = '{date}'
        and extract(hour from datetime_utc_iso at time zone 'America/Bogota') >= {begin_hour}
        and extract(hour from datetime_utc_iso at time zone 'America/Bogota') <= {end_hour}
    """.format(store_id=store_id, slot_type=slot_type, date=date, begin_hour=begin_hour, end_hour=end_hour)
    
    query_cr = """
    select
        id as slot_id,
        physical_store_id,
        (datetime_utc_iso at time zone 'America/Bogota')::date as "dia_agendamento",
        extract(hour from datetime_utc_iso at time zone 'America/Costa_Rica')::text as "hora_agendamento"
    from slots 
    where
        type = '{slot_type}'
        and physical_store_id in ({store_id})
        and (datetime_utc_iso at time zone 'America/Bogota')::date = '{date}'
        and extract(hour from datetime_utc_iso at time zone 'America/Costa_Rica') >= {begin_hour}
        and extract(hour from datetime_utc_iso at time zone 'America/Costa_Rica') <= {end_hour}
    """.format(store_id=store_id, slot_type=slot_type, date=date, begin_hour=begin_hour, end_hour=end_hour)
    
    if country == 'BR':
        query_results = redash.run_query(1969, query)
    elif country == 'AR':
        query_results = redash.run_query(1968, query)
    elif country == 'CO':
        query_results = redash.run_query(1971, query_co)
    elif country == 'CL':
        query_results = redash.run_query(1970, query)
    elif country == 'PE':
        query_results = redash.run_query(1973, query_co)
    elif country == 'EC':
        query_results = redash.run_query(2556, query_co)
    elif country == 'CR':
        query_results = redash.run_query(2558, query_cr)
        
    query_results['country'] = country
    
    return query_results

def change_slot_status(country, slot_id, status, iteration=0):
    """Change the slots status."""
    
    headers = {
        'Authorization': 'postman',
        'API_KEY': Variable.get('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
    }
    
    data = {
        "is_closed": not status
    }
    
    if country == 'BR':
        response = requests.put(
            'http://services.rappi.com.br/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'AR':
        response = requests.put(
            'http://services.rappi.com.ar/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CL':
        response = requests.put(
            'http://services.rappi.cl/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CO':
        response = requests.put(
            'http://services.rappi.com/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'PE':
        response = requests.put(
            'https://services.rappi.pe/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'EC':
        response = requests.put(
            'https://services.rappi.com.ec/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    elif country == 'CR':
        response = requests.put(
            'https://services.rappi.co.cr/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
            headers=headers,
            data=json.dumps(data)
        )
    
    if response.status_code == 200:
        return 'Sim'
    else:
        return 'Nao'

try:
    start_time = time.time()
    for index, row in channel_history.iterrows():
        try:
            if ':' in row['begin_hour']:
                row['begin_hour'] = row['begin_hour'].split(':')[0]
            if ':' in row['end_hour']:
                row['end_hour'] = row['end_hour'].split(':')[0]
                
            slot_ids = get_slot_ids(row['country'], row['store_id'], row['type'], row['date'], row['begin_hour'], row['end_hour'])

            if row['action'] == 'Abrir':
                slot_ids['acao_tomada'] = slot_ids.apply(lambda x: change_slot_status(x.country, x.slot_id, True, iteration=0), axis=1)        
            elif row['action'] == 'Fechar':
                slot_ids['acao_tomada'] = slot_ids.apply(lambda x: change_slot_status(x.country, x.slot_id, False, iteration=0), axis=1)
        except:
            texto = 'Tive alguma dificuldade ao processar a sua solicitação. Verifique se todos os campos estão informados da forma correta, e tente novamente.'
            slack.send_message_thread('C01GW80MFKJ', row['ts'], text=texto)
            continue
            
        slot_ids.to_excel('occ_massivo_slot_ids.xlsx', index=False)
        snow.upload_df(slot_ids, 'occ_slots_massivo_logs')

        texto = 'Segue lista de slots impactados pela solicitação:'
        slack.file_upload_thread(channel='C01GW80MFKJ', thread_ts=row['ts'], text=texto, filename='occ_massivo_slot_ids.xlsx', filetype='xlsx')
    elapsed_time = time.time() - start_time
    if elapsed_time >= 900:
        trigger_text = """
        @crisla.pott @yasmin.pedro @nathalia.rocha @fernando.araujo @leandro.benasse @pedro.ayres

        :alert: Problemas de delay na execução da janela do Bot de Slots Massivo :alert:
        """
        slack.send_message_names_channel('C01GW80MFKJ', trigger_text)
except:
    trigger_text = """
    @crisla.pott @yasmin.pedro @nathalia.rocha @fernando.araujo @leandro.benasse @pedro.ayres

    :alert: Problemas na execução da janela do Bot de Slots Massivo :alert:
    """
    slack.send_message_names_channel('C01GW80MFKJ', trigger_text)