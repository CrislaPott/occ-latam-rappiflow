import os, sys, json
import numpy as np 
import pandas as pd 
from lib import redash 
from lib import snowflake as snow 

# Payment Methods - BR
br_payment_methods = pd.DataFrame()

query = """
select *
from audit 
where 
    endpoint = '/api/operation-management/zones/payment-methods'
    and created_at >= now() - interval '10 minute'
order by created_at desc
"""

results = redash.run_query(823, query)

if results.shape[0] == 0:
    print('Sem novos registros')
    sys.exit(0)

for index, row in results.iterrows():
    created_at_utc = pd.to_datetime(row['created_at'])
    city = row['city']
    user = row['user']
    changes = json.loads(row['changes'])
    zones = changes['zones']
    payment_methods = changes['payment_methods']
    event_duration = changes['duration']
    for zone in zones:
        for payment_method in payment_methods:
            line = {
                'city': city,
                'created_at_utc': created_at_utc,
                'user': user,
                'zone': zone,
                'payment_method': payment_method,
                'event_duration': event_duration
            }
            temp = pd.DataFrame([line])
            br_payment_methods = pd.concat([br_payment_methods, temp], ignore_index=True)

print(br_payment_methods.head())

snow.upload_df(br_payment_methods, 'occ_grappi_logs_payment_methods_br')