import os, re, json, requests
import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now()
oldest = latest - timedelta(minutes=30)

channel_history = tl.get_channel_history(
    channel = "C01DFAHJ05S",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].astype(str)
    
print(channel_history['username'].value_counts())

slack_users = pd.read_csv('input/slack_users.csv')

def transform_saturation(sat):
    sat = str(sat)
    sat = sat.replace(',', '.')
    try:
        sat = float(sat)
        return sat
    except:
        return None
    
def transform_unassigned(un):
    un = str(un)
    un = un.replace('%', '')
    un = un.replace(',', '.')
    try:
        un = float(un)
        if un <= 100:
            return un
    except:
        return None
    
def calculate_start_hour(begin):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        return int(begin_list[0])
    except:
        return None

def calculate_duration(begin, end):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        begin = begin_list[0] + ':' + begin_list[1]

        end_list = end.split(':')
        end_list = [x.zfill(2) for x in end_list]
        end = end_list[0] + ':' + end_list[1]

        FMT = '%H:%M'

        duration = (datetime.strptime(end, FMT) - datetime.strptime(begin, FMT)).seconds / 60
        return duration
    except:
        return None

def get_city(text):
    try:
        grande = re.findall('(?<=\*Cidades grandes\*)(.*)(?=\*Cidades pequenas)', text)[0].strip()
        pequena = re.findall('(?<=\*Cidades pequenas\*)(.*)(?=\*Zona\*)', text)[0].strip()
        if 'left blank' in pequena:
            return grande
        elif 'left blank' in grande:
            return pequena
        else:
            return grande 
    except:
        return np.nan 

try:
# Eventualities
    br_eventualities = channel_history[
        (channel_history['username'].str.contains('Aplicação de Eventualidades')) &
        (channel_history['username'].str.contains('BR'))
    ].reset_index()

    br_eventualities = br_eventualities[['ts', 'username', 'text']]

    br_eventualities['slack_user_id'] = br_eventualities['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    br_eventualities['city'] = br_eventualities['text'].apply(lambda x: get_city(x))
    br_eventualities['zone'] = br_eventualities['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Zona com)")
    br_eventualities['zone_more_70_percent_store'] = br_eventualities['text'].str.extract(r"(?<=em mercado\?\*)(.*)(?=\*Tipo de veículo\*)")
    br_eventualities['vehicle_type'] = br_eventualities['text'].str.extract(r"(?<=\*Tipo de veículo\*)(.*)(?=\*Tipo de vertical\*)")
    br_eventualities['vertical_type'] = br_eventualities['text'].str.extract(r"(?<=\*Tipo de vertical\*)(.*)(?=\*Polígono\*)")
    br_eventualities['polygon'] = br_eventualities['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Frete)")
    br_eventualities['events'] = br_eventualities['text'].str.extract(r"(?<=\*Frete\*)(.*)(?=\*Satur)")
    br_eventualities['saturation'] = br_eventualities['text'].str.extract(r"(?<=\*Saturação\*)(.*)(?=\*Unassigned)")
    br_eventualities['unassigned'] = br_eventualities['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Horário inicial)")
    br_eventualities['start_time'] = br_eventualities['text'].str.extract(r"(?<=\*Horário inicial\*)(.*)(?=\*Horário final)")
    br_eventualities['end_time'] = br_eventualities['text'].str.extract(r"(?<=\*Horário final\*)(.*)(?=\*Data)")
    br_eventualities['observations'] = br_eventualities['text'].str.extract(r"(?<=\*Observações\*)(.*)")

    br_eventualities_obj = br_eventualities.select_dtypes(['object'])
    br_eventualities[br_eventualities_obj.columns] = br_eventualities_obj.apply(lambda x: x.str.strip())

    br_eventualities['saturation'] = br_eventualities['saturation'].apply(lambda x: transform_saturation(x))
    br_eventualities['unassigned'] = br_eventualities['unassigned'].apply(lambda x: transform_unassigned(x))
    br_eventualities['start_hour'] = br_eventualities['start_time'].apply(lambda x: calculate_start_hour(x))
    br_eventualities['event_duration_minutes'] = br_eventualities.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    br_eventualities = br_eventualities.merge(slack_users, on='slack_user_id', how='left')

    print(br_eventualities.isna().sum())

    print('Fazendo upload dos logs de eventualidades')
    snow.upload_df(br_eventualities, 'occ_log_balance_br_eventualities_copy')
except:
    pass

try:
    # Fechamento de Verticais e Pgto
    br_verticals_payments = channel_history[
        (channel_history['username'].str.contains('Fechamento de verticais e pgto')) &
        (channel_history['username'].str.contains('BR'))
    ].reset_index()

    br_verticals_payments = br_verticals_payments[['ts', 'username', 'text']]

    br_verticals_payments['slack_user_id'] = br_verticals_payments['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    br_verticals_payments['city'] = br_verticals_payments['text'].apply(lambda x: get_city(x))
    br_verticals_payments['zone'] = br_verticals_payments['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Zona*)")
    br_verticals_payments['zone_more_70_percent_store'] = br_verticals_payments['text'].str.extract(r"(?<=em mercado\?\*)(.*)(?=\*Vert)")
    br_verticals_payments['closed_vertical'] = br_verticals_payments['text'].str.extract(r"(?<=fechada\*)(.*)(?=\*Pagamento)")
    br_verticals_payments['closed_payments'] = br_verticals_payments['text'].str.extract(r"(?<=fechado\*)(.*)(?=\*Sat)")
    br_verticals_payments['saturation'] = br_verticals_payments['text'].str.extract(r"(?<=\*Saturação\*)(.*)(?=\*Unassigned)")
    br_verticals_payments['unassigned'] = br_verticals_payments['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Horário inicial)")
    br_verticals_payments['start_time'] = br_verticals_payments['text'].str.extract(r"(?<=\*Horário inicial\*)(.*)(?=\*Horário final)")
    br_verticals_payments['end_time'] = br_verticals_payments['text'].str.extract(r"(?<=\*Horário final\*)(.*)(?=\*Data)")
    br_verticals_payments['observations'] = br_verticals_payments['text'].str.extract(r"(?<=\*Observações\*)(.*)")

    br_verticals_payments_obj = br_verticals_payments.select_dtypes(['object'])
    br_verticals_payments[br_verticals_payments_obj.columns] = br_verticals_payments_obj.apply(lambda x: x.str.strip())

    br_verticals_payments['saturation'] = br_verticals_payments['saturation'].apply(lambda x: transform_saturation(x))
    br_verticals_payments['unassigned'] = br_verticals_payments['unassigned'].apply(lambda x: transform_unassigned(x))
    br_verticals_payments['start_hour'] = br_verticals_payments['start_time'].apply(lambda x: calculate_start_hour(x))
    br_verticals_payments['event_duration_minutes'] = br_verticals_payments.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    br_verticals_payments = br_verticals_payments.merge(slack_users, on='slack_user_id', how='left')

    print(br_verticals_payments.isna().sum())

    print('Fazendo upload dos logs de vertcais e pgto')
    snow.upload_df(br_verticals_payments, 'occ_log_balance_br_verticals_payments')
except:
    pass

try:
    # Fechamento de Slots
    br_closed_slots = channel_history[
        (channel_history['username'].str.contains('Fechamento de lojas e Slots')) &
        (channel_history['username'].str.contains('BR'))
    ].reset_index()

    br_closed_slots = br_closed_slots[['ts', 'username', 'text']]

    br_closed_slots['slack_user_id'] = br_closed_slots['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    br_closed_slots['city'] = br_closed_slots['text'].apply(lambda x: get_city(x))
    br_closed_slots['zone'] = br_closed_slots['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Zona*)")
    br_closed_slots['store_id'] = br_closed_slots['text'].str.extract(r"(?<=ID\*)(.*)(?=\*Horário inicial)")
    br_closed_slots['zone_more_70_percent_store'] = br_closed_slots['text'].str.extract(r"(?<=em mercado\?\*)(.*)(?=\*Slot fechado)")
    br_closed_slots['closed_vertical_slot'] = br_closed_slots['text'].str.extract(r"(?<=Stores\*)(.*)(?=\*Saturação)")
    br_closed_slots['saturation'] = br_closed_slots['text'].str.extract(r"(?<=\*Saturação\*)(.*)(?=\*Unassigned)")
    br_closed_slots['unassigned'] = br_closed_slots['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Store ID)")
    br_closed_slots['start_time'] = br_closed_slots['text'].str.extract(r"(?<=\*Horário inicial\*)(.*)(?=\*Horário final)")
    br_closed_slots['end_time'] = br_closed_slots['text'].str.extract(r"(?<=\*Horário final\*)(.*)(?=\*Data)")
    br_closed_slots['observations'] = br_closed_slots['text'].str.extract(r"(?<=\*Observações\*)(.*)")

    br_closed_slots_obj = br_closed_slots.select_dtypes(['object'])
    br_closed_slots[br_closed_slots_obj.columns] = br_closed_slots_obj.apply(lambda x: x.str.strip())

    br_closed_slots['saturation'] = br_closed_slots['saturation'].apply(lambda x: transform_saturation(x))
    br_closed_slots['unassigned'] = br_closed_slots['unassigned'].apply(lambda x: transform_unassigned(x))
    br_closed_slots['start_hour'] = br_closed_slots['start_time'].apply(lambda x: calculate_start_hour(x))
    br_closed_slots['event_duration_minutes'] = br_closed_slots.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    br_closed_slots = br_closed_slots.merge(slack_users, on='slack_user_id', how='left')

    print(br_closed_slots.isna().sum())

    print('Fazendo upload dos logs de fechamento de slots')
    snow.upload_df(br_closed_slots, 'occ_log_balance_br_closed_slots')
except:
    pass

try:
    # Retirada de Acoes
    br_retirada_acoes = channel_history[
        (channel_history['username'].str.contains('Ações retiradas')) &
        (channel_history['username'].str.contains('BR'))
    ].reset_index()

    br_retirada_acoes = br_retirada_acoes[['ts', 'username', 'text']]

    br_retirada_acoes['slack_user_id'] = br_retirada_acoes['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    br_retirada_acoes['city'] = br_retirada_acoes['text'].apply(lambda x: get_city(x))
    br_retirada_acoes['zone'] = br_retirada_acoes['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    br_retirada_acoes['polygon'] = br_retirada_acoes['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Frete)")
    br_retirada_acoes['events'] = br_retirada_acoes['text'].str.extract(r"(?<=\*Frete\*)(.*)(?=\*Vert)")
    br_retirada_acoes['vertical'] = br_retirada_acoes['text'].str.extract(r"(?<=Vertical\*)(.*)(?=\*Método)")
    br_retirada_acoes['payment_method'] = br_retirada_acoes['text'].str.extract(r"(?<=pagamento\*)(.*)(?=\*Horário)")
    br_retirada_acoes['start_time'] = br_retirada_acoes['text'].str.extract(r"(?<=\*Horário inicial\*)(.*)(?=\*Data)")
    br_retirada_acoes['observations'] = br_retirada_acoes['text'].str.extract(r"(?<=\*Observações\*)(.*)")

    br_retirada_acoes_obj = br_retirada_acoes.select_dtypes(['object'])
    br_retirada_acoes[br_retirada_acoes_obj.columns] = br_retirada_acoes_obj.apply(lambda x: x.str.strip())

    br_retirada_acoes['start_hour'] = br_retirada_acoes['start_time'].apply(lambda x: calculate_start_hour(x))

    br_retirada_acoes = br_retirada_acoes.merge(slack_users, on='slack_user_id', how='left')

    print(br_retirada_acoes.isna().sum())

    print('Fazendo upload dos logs de retirada de acoes')
    snow.upload_df(br_retirada_acoes, 'occ_log_balance_br_retirada_acoes')
except:
    pass

try:
    # Push / SMS
    br_push_sms = channel_history[
        (channel_history['username'].str.contains('Push')) &
        (channel_history['username'].str.contains('BR'))
    ].reset_index()

    br_push_sms = br_push_sms[['ts', 'username', 'text']]

    br_push_sms['slack_user_id'] = br_push_sms['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    br_push_sms['city'] = br_push_sms['text'].apply(lambda x: get_city(x))
    br_push_sms['zone'] = br_push_sms['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*PUSH*)")
    br_push_sms['push_sms'] = br_push_sms['text'].str.extract(r"(?<=\*PUSH \/ SMS\*)(.*)(?=\*Horário)")
    br_push_sms['start_time'] = br_push_sms['text'].str.extract(r"(?<=\*Horário inicial\*)(.*)(?=\*Data)")
    br_push_sms['observations'] = br_push_sms['text'].str.extract(r"(?<=\*Observações\*)(.*)")

    br_push_sms_obj = br_push_sms.select_dtypes(['object'])
    br_push_sms[br_push_sms_obj.columns] = br_push_sms_obj.apply(lambda x: x.str.strip())

    br_push_sms['start_hour'] = br_push_sms['start_time'].apply(lambda x: calculate_start_hour(x))

    br_push_sms = br_push_sms.merge(slack_users, on='slack_user_id', how='left')

    print(br_push_sms.isna().sum())

    print('Fazendo upload dos logs de push')
    snow.upload_df(br_push_sms, 'occ_log_balance_br_push_sms')
except:
    pass

