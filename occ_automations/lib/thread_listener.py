import os, json
import numpy as np 
import pandas as pd
import slack_sdk as slack
from time import sleep
from dotenv import load_dotenv
from datetime import datetime, timedelta
from airflow.models import Variable

load_dotenv()

client = slack.WebClient(token=Variable.get('SLACK_TOKEN'))

def get_channel_history(channel, messages_per_page, max_messages, oldest, latest):
    # get first page
    page = 1
    print("Retrieving page {}".format(page))
    response = client.conversations_history(
        channel=channel,
        limit=messages_per_page,
        oldest=str(oldest.timestamp()),
        latest=str(latest.timestamp())
        
    )
    assert response["ok"]
    messages_all = response['messages']

    # get additional pages if below max message and if they are any
    while len(messages_all) + messages_per_page <= max_messages and response['has_more']:
        page += 1
        print("Retrieving page {}".format(page))
        sleep(1)   # need to wait 1 sec before next call due to rate limits
        response = client.conversations_history(
            channel=channel,
            limit=messages_per_page,
            oldest=str(oldest.timestamp()),
            latest=str(latest.timestamp()),
            cursor=response['response_metadata']['next_cursor']
        )
        assert response["ok"]
        messages = response['messages']
        messages_all = messages_all + messages

    print(
        "Fetched a total of {} messages from channel {}".format(
            len(messages_all),
            channel
    ))

    return pd.DataFrame(messages_all)

def get_thread_replies(channel, thread_ts):
    thread_info = client.conversations_replies(
        channel = channel,
        ts = thread_ts
    )
    
    thread_messages = pd.DataFrame(thread_info['messages'])
    thread_messages =  thread_messages[[
        'type',
        'text',
        'user',
        'ts',
        'thread_ts',
        'latest_reply',
        'subtype',
        'username',
        'bot_id'
    ]]
    
    return thread_messages