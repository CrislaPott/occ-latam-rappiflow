import os
from dotenv import load_dotenv
from slack import WebClient
from slack.errors import SlackApiError
from airflow.models import Variable

load_dotenv()

try:
	client = WebClient(token=Variable.get("SLACK_TOKEN"))
except:
	print("Fail Slack")
	
def bot_slack(text,channel):
    try:
    	client.chat_postMessage(text=text, channel=channel)
    except:
    	print(text)
def send_message_channel(channel, text):
    try:
    	client.chat_postMessage(channel=channel, text=text)
    except:
    	print(text)
def send_message_names_channel(channel, text):
    try:
    	client.chat_postMessage(channel=channel, text=text, link_names=1)
    except:
    	print(text)
def send_message_thread(channel, thread_ts, text):
    try:
    	client.chat_postMessage(channel=channel, text=text, thread_ts=thread_ts)
    except:
    	print(text)
def file_upload_channel(channel, text, filename, filetype):
	try:
		client.files_upload(file=filename,filename=filename,channels=channel,title=filename,initial_comment=text,filetpye=filetype)
	except:
		print(initial_comment)
def file_upload_thread(channel, thread_ts, text, filename, filetype):
	try:
		client.files_upload(file=filename,filename=filename,channels=channel,title=filename,initial_comment=text,filetpye=filetype,thread_ts=thread_ts)
	except:
		print(initial_comment)