import os, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash, gsheets
from lib import snowflake as snow

os.chdir('../')

def get_data(country):
    payment_methods = pd.DataFrame()

    query = """
    select *
    from audit 
    where 
        endpoint = '/api/operation-management/zones/payment-methods'
        and created_at >= now() - interval '10 minute'
    order by created_at desc
    """

    query_ec = """
    select *
    from audit 
    where 
        endpoint = '/api/operation-management/zones/payment-methods'
        and created_at >= (select max(created_at) from audit) - interval '10 minute'
    order by created_at desc
    """

    if country == 'br':
        results = redash.run_query(823, query)
    elif country == 'cl':
        results = redash.run_query(825, query)
    elif country == 'co':
        results = redash.run_query(801, query)
    elif country == 'ar':
        results = redash.run_query(824, query)
    elif country == 'ec':
        results = redash.run_query(2272, query_ec)
    elif country == 'mx':
        results = redash.run_query(827, query)
    elif country == 'uy':
        results = redash.run_query(826, query)
    elif country == 'pe':
        results = redash.run_query(874, query)
    elif country == 'cr':
        results = redash.run_query(2271, query)

    if results.shape[0] == 0:
        return 0

    for index, row in results.iterrows():
        created_at_utc = pd.to_datetime(row['created_at'])
        city = row['city']
        user = row['user']
        changes = json.loads(row['changes'])
        zones = changes['zones']
        payment_methods_list = changes['payment_methods']
        event_duration = changes['duration']
        
        for zone in zones:
            for payment_method in payment_methods_list:
                line = {
                    'city': city,
                    'created_at_utc': created_at_utc,
                    'user': user,
                    'zone': zone,
                    'payment_method': payment_method,
                    'event_duration': event_duration
                }
                temp = pd.DataFrame([line])
                payment_methods = pd.concat([payment_methods, temp], ignore_index=True)
    
    print('Salvando registros para o pais {}'.format(country))

    snow.upload_df(payment_methods, 'occ_grappi_logs_payment_methods_{}'.format(country))


countries = ['br', 'cl', 'co', 'ar', 'ec', 'mx', 'uy', 'pe', 'cr']
for country in countries:
    get_data(country)