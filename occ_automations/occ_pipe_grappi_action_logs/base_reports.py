import os, sys, json, requests, arrow
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode

sys.path.insert(0, "../")

from lib import gsheets, redash, slack
from lib import snowflake as snow
from lib import thread_listener as tl

os.chdir('../')

def update_balance_reports(country):
    target_db = "occ_balance_grappi_base_reports_{}".format(country)

    base_query = """
    --no_cache
    with one as (
        select
            dateadd('hour', -3, created_at_utc)::date as day,
            zone,
            concat(raining, ' rain') as event,
            round(sum(raining_duration) / 60.0, 2) as total_duration
        from (
            select
                distinct concat(created_at_utc, user, zone, raining, raining_duration),
                *
            from br_writable.occ_grappi_logs_eventualities_{country}
        )
        where
            event_duration <> -1
            and raining_duration <> -1
            and raining is not null
            and raining in ('moderate', 'heavy', 'violent')
            and dateadd('hour', -3, created_at_utc)::date >= dateadd(day, -7, (convert_timezone('UTC',current_timestamp) - interval '3 hour'))::date
        group by 1, 2, 3
    ),

    two as (
        select
            dateadd('hour', -3, created_at_utc)::date as day,
            zone,
            concat('Event ', event) as event,
            round(sum(event_duration) / 60.0, 2) as total_duration
        from (
            select
                distinct concat(created_at_utc, user, zone, event, event_duration),
                *
            from br_writable.occ_grappi_logs_eventualities_{country}
        )
        where
            event_duration <> -1
            and event is not null
            and dateadd('hour', -3, created_at_utc)::date >= dateadd(day, -7, (convert_timezone('UTC',current_timestamp) - interval '3 hour'))::date
        group by 1, 2, 3
    ),

    three as (
        select
            dateadd('hour', -3, created_at_utc)::date as day,
            zone,
            case
                when events ilike '%cc%' and events not ilike '%cash%' then 'Closed CC'
                when events not ilike '%cc%' and events ilike '%cash%' then 'Closed Cash'
                when events ilike '%cc%' and events ilike '%cash%' then 'Closed CC and Cash'
            else null end as event,
            round(sum(event_duration) / 60.0, 2) as total_duration
        from (
            select
                created_at_utc,
                user,
                zone,
                event_duration,
                listagg(payment_method, ', ') as events
            from
            (select
                distinct concat(created_at_utc, user, zone, payment_method, event_duration) as uuid,
                *
            from br_writable.occ_grappi_logs_payment_methods_{country})
            group by 1, 2, 3, 4
        )
        where
            event_duration <> -1
            and event is not null
            and dateadd('hour', -3, created_at_utc)::date >= dateadd(day, -7, (convert_timezone('UTC',current_timestamp) - interval '3 hour'))::date
        group by 1, 2, 3
    ),

    final as (
        select * from one
        union all
        select * from two
        union all
        select * from three
    )

    select * from final
    where event is not null
    order by day desc
    """.format(country=country)

    results = snow.run_query(base_query)

    try:
        snow.truncate_table(target_db)
    except:
        pass
    
    try:
        snow.upload_df(results, target_db)
    except:
        pass

    

countries = ['br', 'cl', 'co', 'ar', 'mx', 'uy']
for country in countries:
    update_balance_reports(country)

    
        
    