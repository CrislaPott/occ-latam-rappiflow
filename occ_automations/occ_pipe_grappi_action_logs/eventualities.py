import os, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime

sys.path.insert(0, "../")

from lib import redash, gsheets
from lib import snowflake as snow

os.chdir('../')

def get_event(zone):
    try:
        return zone['eventualities']['level']
    except:
        return np.nan

def get_event_duration(zone):
    try:
        return zone['eventualities']['level_duration']
    except:
        return np.nan

def get_raining(zone):
    try:
        return zone['eventualities']['raining']
    except:
        return np.nan

def get_raining_duration(zone):
    try:
        return zone['eventualities']['raining_duration']
    except:
        return np.nan

def get_data(country):
    eventualities = pd.DataFrame()

    query = """
    select *
    from audit 
    where 
        endpoint = '/api/operation-management/zones/eventualities'
        and created_at >= now() - interval '10 minute'
    order by created_at desc
    """

    query_ec = """
    select *
    from audit 
    where 
        endpoint = '/api/operation-management/zones/eventualities'
        and created_at >= (select max(created_at) from audit) - interval '10 minute'
    order by created_at desc
    """

    if country == 'br':
        results = redash.run_query(823, query)
    elif country == 'cl':
        results = redash.run_query(825, query)
    elif country == 'co':
        results = redash.run_query(801, query)
    elif country == 'ar':
        results = redash.run_query(824, query)
    elif country == 'ec':
        results = redash.run_query(2272, query_ec)
    elif country == 'mx':
        results = redash.run_query(827, query)
    elif country == 'uy':
        results = redash.run_query(826, query)
    elif country == 'pe':
        results = redash.run_query(874, query)
    elif country == 'cr':
        results = redash.run_query(2271, query)

    if results.shape[0] == 0:
        return 0

    for index, row in results.iterrows():
        created_at_utc = pd.to_datetime(row['created_at'])
        city = row['city']
        user = row['user']
        changes = json.loads(row['changes'])
        zones = changes['zones']
        for zone in zones:
            try:
                line = {
                    'city': city,
                    'created_at_utc': created_at_utc,
                    'user': user,
                    'zone': zone['name'],
                    'event': get_event(zone),
                    'event_duration': get_event_duration(zone),
                    'raining': get_raining(zone),
                    'raining_duration': get_raining_duration(zone)
                }
                temp = pd.DataFrame([line])
                eventualities = pd.concat([eventualities, temp], ignore_index=True)
            except:
                pass
            
    print('Salvando registros para o pais {}'.format(country))

    snow.upload_df(eventualities, 'occ_grappi_logs_eventualities_{}'.format(country))


countries = ['br', 'cl', 'co', 'ar', 'ec', 'mx', 'uy', 'pe', 'cr']
for country in countries:
    get_data(country)

