import sys
import numpy as np
import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow
from unidecode import unidecode
from datetime import datetime, timedelta

latest = datetime.now()
oldest = latest - timedelta(minutes=30)

channel_history = tl.get_channel_history(
    channel = "C01DFAHJ05S",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

if channel_history.shape[0] == 0:
    print('Sem mensagens para processar')
    sys.exit(0)

for column in channel_history.columns:
    channel_history[column] = channel_history[column].astype(str)
    
print(channel_history['username'].value_counts())

slack_users = pd.read_csv('input/slack_users.csv')

def transform_saturation(sat):
    sat = sat.replace(',', '.')
    try:
        sat = float(sat)
        return sat
    except:
        return None
    
def transform_unassigned(un):
    un = un.replace('%', '')
    un = un.replace(',', '.')
    try:
        un = float(un)
        if un <= 100:
            return un
    except:
        return None
    
def calculate_start_hour(begin):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        return int(begin_list[0])
    except:
        return None

def calculate_duration(begin, end):
    try:
        begin_list = begin.split(':')
        begin_list = [x.zfill(2) for x in begin_list]
        begin = begin_list[0] + ':' + begin_list[1]

        end_list = end.split(':')
        end_list = [x.zfill(2) for x in end_list]
        end = end_list[0] + ':' + end_list[1]

        FMT = '%H:%M'

        duration = (datetime.strptime(end, FMT) - datetime.strptime(begin, FMT)).seconds / 60
        return duration
    except:
        return None

# Eventualities
try:
    ar_eventualities = channel_history[
        (channel_history['username'].str.contains('Aplicación de Eventualidades')) &
        (channel_history['username'].str.contains('AR'))
    ].reset_index()

    ar_eventualities = ar_eventualities[['ts', 'username', 'text']]

    ar_eventualities['slack_user_id'] = ar_eventualities['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    ar_eventualities['city'] = ar_eventualities['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    ar_eventualities['zone'] = ar_eventualities['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    ar_eventualities['polygon'] = ar_eventualities['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    ar_eventualities['events'] = ar_eventualities['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Satur)")
    ar_eventualities['saturation'] = ar_eventualities['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    ar_eventualities['unassigned'] = ar_eventualities['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora de inicio)")
    ar_eventualities['start_time'] = ar_eventualities['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    ar_eventualities['end_time'] = ar_eventualities['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    ar_eventualities['observations'] = ar_eventualities['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    ar_eventualities = ar_eventualities.astype(str)

    ar_eventualities_obj = ar_eventualities.select_dtypes(['object'])
    ar_eventualities[ar_eventualities_obj.columns] = ar_eventualities_obj.apply(lambda x: x.str.strip())

    ar_eventualities['city'] = ar_eventualities['city'].str.lower()
    ar_eventualities['city'] = ar_eventualities['city'].apply(lambda x: unidecode(x))

    ar_eventualities['saturation'] = ar_eventualities['saturation'].apply(lambda x: transform_saturation(x))
    ar_eventualities['unassigned'] = ar_eventualities['unassigned'].apply(lambda x: transform_unassigned(x))
    ar_eventualities['start_hour'] = ar_eventualities['start_time'].apply(lambda x: calculate_start_hour(x))
    ar_eventualities['event_duration_minutes'] = ar_eventualities.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    ar_eventualities = ar_eventualities.merge(slack_users, on='slack_user_id', how='left')

    print(ar_eventualities.isna().sum())

    print('Fazendo upload dos logs de eventualidades')
    snow.upload_df(ar_eventualities, 'occ_log_balance_ar_eventualities')
except:
    pass

# Fechamento de Verticais e Pgto
try:
    ar_verticals_payments = channel_history[
        (channel_history['username'].str.contains('verticales y pagos')) &
        (channel_history['username'].str.contains('AR'))
    ].reset_index()

    ar_verticals_payments = ar_verticals_payments[['ts', 'username', 'text']]

    ar_verticals_payments['slack_user_id'] = ar_verticals_payments['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    ar_verticals_payments['city'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    ar_verticals_payments['zone'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Vert*)")
    ar_verticals_payments['closed_vertical'] = ar_verticals_payments['text'].str.extract(r"(?<=Vertical cerrada\*)(.*)(?=\*Pago cerrado)") 
    ar_verticals_payments['closed_payments'] = ar_verticals_payments['text'].str.extract(r"(?<=Pago cerrado\*)(.*)(?=\*Sat)")
    ar_verticals_payments['saturation'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    ar_verticals_payments['unassigned'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Hora inicio)")
    ar_verticals_payments['start_time'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Hora inicio\*)(.*)(?=\*Hora de finalización)")
    ar_verticals_payments['end_time'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    ar_verticals_payments['observations'] = ar_verticals_payments['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    ar_verticals_payments = ar_verticals_payments.astype(str)

    ar_verticals_payments_obj = ar_verticals_payments.select_dtypes(['object'])
    ar_verticals_payments[ar_verticals_payments_obj.columns] = ar_verticals_payments_obj.apply(lambda x: x.str.strip())

    ar_verticals_payments['city'] = ar_verticals_payments['city'].str.lower()
    ar_verticals_payments['city'] = ar_verticals_payments['city'].apply(lambda x: unidecode(x))

    ar_verticals_payments['saturation'] = ar_verticals_payments['saturation'].apply(lambda x: transform_saturation(x))
    ar_verticals_payments['unassigned'] = ar_verticals_payments['unassigned'].apply(lambda x: transform_unassigned(x))
    ar_verticals_payments['start_hour'] = ar_verticals_payments['start_time'].apply(lambda x: calculate_start_hour(x))
    ar_verticals_payments['event_duration_minutes'] = ar_verticals_payments.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    ar_verticals_payments = ar_verticals_payments.merge(slack_users, on='slack_user_id', how='left')

    print(ar_verticals_payments.isna().sum())

    print('Fazendo upload dos logs de vertcais e pgto')
    snow.upload_df(ar_verticals_payments, 'occ_log_balance_ar_verticals_payments')
except:
    pass

# Fechamento de Slots
try:
    ar_closed_slots = channel_history[
        (channel_history['username'].str.contains('tiendas y Slots')) &
        (channel_history['username'].str.contains('AR'))
    ].reset_index()

    ar_closed_slots = ar_closed_slots[['ts', 'username', 'text']]

    ar_closed_slots['slack_user_id'] = ar_closed_slots['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    ar_closed_slots['city'] = ar_closed_slots['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    ar_closed_slots['zone'] = ar_closed_slots['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Slot cerrado y Stores*)")
    ar_closed_slots['closed_vertical_slot'] = ar_closed_slots['text'].str.extract(r"(?<=Slot cerrado y Stores\*)(.*)(?=\*Sat)")
    ar_closed_slots['saturation'] = ar_closed_slots['text'].str.extract(r"(?<=\*Saturación\*)(.*)(?=\*Unassigned)")
    ar_closed_slots['unassigned'] = ar_closed_slots['text'].str.extract(r"(?<=\*Unassigned\*)(.*)(?=\*Store ID)")
    ar_closed_slots['store_id'] = ar_closed_slots['text'].str.extract(r"(?<=ID\*)(.*)(?=\*Hora de inicio)")
    ar_closed_slots['start_time'] = ar_closed_slots['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Hora de finalización)")
    ar_closed_slots['end_time'] = ar_closed_slots['text'].str.extract(r"(?<=\*Hora de finalización\*)(.*)(?=\*Fecha)")
    ar_closed_slots['observations'] = ar_closed_slots['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    ar_closed_slots = ar_closed_slots.astype(str)

    ar_closed_slots_obj = ar_closed_slots.select_dtypes(['object'])
    ar_closed_slots[ar_closed_slots_obj.columns] = ar_closed_slots_obj.apply(lambda x: x.str.strip())

    ar_closed_slots['city'] = ar_closed_slots['city'].str.lower()
    ar_closed_slots['city'] = ar_closed_slots['city'].apply(lambda x: unidecode(x))

    ar_closed_slots['saturation'] = ar_closed_slots['saturation'].apply(lambda x: transform_saturation(x))
    ar_closed_slots['unassigned'] = ar_closed_slots['unassigned'].apply(lambda x: transform_unassigned(x))
    ar_closed_slots['start_hour'] = ar_closed_slots['start_time'].apply(lambda x: calculate_start_hour(x))
    ar_closed_slots['event_duration_minutes'] = ar_closed_slots.apply(lambda x: calculate_duration(x.start_time, x.end_time), axis=1)

    ar_closed_slots = ar_closed_slots.merge(slack_users, on='slack_user_id', how='left')

    print(ar_closed_slots.isna().sum())

    print('Fazendo upload dos logs de fechamento de slots')
    snow.upload_df(ar_closed_slots, 'occ_log_balance_ar_closed_slots')
except:
    pass

# Retirada de Acoes
try:
    ar_retirada_acoes = channel_history[
        (channel_history['username'].str.contains('Acciones eliminadas')) &
        (channel_history['username'].str.contains('AR'))
    ].reset_index()

    ar_retirada_acoes = ar_retirada_acoes[['ts', 'username', 'text']]

    ar_retirada_acoes['slack_user_id'] = ar_retirada_acoes['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    ar_retirada_acoes['city'] = ar_retirada_acoes['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    ar_retirada_acoes['zone'] = ar_retirada_acoes['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*Pol*)")
    ar_retirada_acoes['polygon'] = ar_retirada_acoes['text'].str.extract(r"(?<=\*Polígono\*)(.*)(?=\*Flete)")
    ar_retirada_acoes['events'] = ar_retirada_acoes['text'].str.extract(r"(?<=\*Flete\*)(.*)(?=\*Vert)")
    ar_retirada_acoes['vertical'] = ar_retirada_acoes['text'].str.extract(r"(?<=Vertical\*)(.*)(?=\*Método)")
    ar_retirada_acoes['payment_method'] = ar_retirada_acoes['text'].str.extract(r"(?<=Pago\*)(.*)(?=\* Hora)")
    ar_retirada_acoes['start_time'] = ar_retirada_acoes['text'].str.extract(r"(?<=\* Hora de inicio\*)(.*)(?=\*Fecha)")
    ar_retirada_acoes['observations'] = ar_retirada_acoes['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    ar_retirada_acoes = ar_retirada_acoes.astype(str)

    ar_retirada_acoes_obj = ar_retirada_acoes.select_dtypes(['object'])
    ar_retirada_acoes[ar_retirada_acoes_obj.columns] = ar_retirada_acoes_obj.apply(lambda x: x.str.strip())

    ar_retirada_acoes['city'] = ar_retirada_acoes['city'].str.lower()
    ar_retirada_acoes['city'] = ar_retirada_acoes['city'].apply(lambda x: unidecode(x))

    ar_retirada_acoes['start_hour'] = ar_retirada_acoes['start_time'].apply(lambda x: calculate_start_hour(x))

    ar_retirada_acoes = ar_retirada_acoes.merge(slack_users, on='slack_user_id', how='left')

    print(ar_retirada_acoes.isna().sum())

    print('Fazendo upload dos logs de retirada de acoes')
    snow.upload_df(ar_retirada_acoes, 'occ_log_balance_ar_retirada_acoes')
except:
    pass

# Push / SMS
try:
    ar_push_sms = channel_history[
        (channel_history['username'].str.contains('Push')) &
        (channel_history['username'].str.contains('co'))
    ].reset_index()

    ar_push_sms = ar_push_sms[['ts', 'username', 'text']]

    ar_push_sms['slack_user_id'] = ar_push_sms['text'].str.extract(r"(?<=\<@)(.*)(?=\>)")
    ar_push_sms['city'] = ar_push_sms['text'].str.extract(r"(?<=\*Ciudad\*)(.*)(?=\*Zona\*)")
    ar_push_sms['zone'] = ar_push_sms['text'].str.extract(r"(?<=\*Zona\*)(.*)(?=\*PUSH*)")
    ar_push_sms['push_sms'] = ar_push_sms['text'].str.extract(r"(?<=\*PUSH \/ SMS\*)(.*)(?=\*Hora de inicio)")
    ar_push_sms['start_time'] = ar_push_sms['text'].str.extract(r"(?<=\*Hora de inicio\*)(.*)(?=\*Fecha)")
    ar_push_sms['observations'] = ar_push_sms['text'].str.extract(r"(?<=\*Observaciones\*)(.*)")

    ar_push_sms = ar_push_sms.astype(str)

    ar_push_sms_obj = ar_push_sms.select_dtypes(['object'])
    ar_push_sms[ar_push_sms_obj.columns] = ar_push_sms_obj.apply(lambda x: x.str.strip())

    ar_push_sms['city'] = ar_push_sms['city'].str.lower()
    ar_push_sms['city'] = ar_push_sms['city'].astype(str)
    ar_push_sms['city'] = ar_push_sms['city'].apply(unidecode)

    ar_push_sms['start_hour'] = ar_push_sms['start_time'].apply(lambda x: calculate_start_hour(x))

    ar_push_sms = ar_push_sms.merge(slack_users, on='slack_user_id', how='left')

    print(ar_push_sms.isna().sum())

    print('Fazendo upload dos logs de push')
    snow.upload_df(ar_push_sms, 'occ_log_balance_ar_push_sms')
except:
    pass



