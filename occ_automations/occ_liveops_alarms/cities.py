import numpy as np
import pandas as pd
from lib import gsheets, redash
from lib import snowflake as snow

class Cities():
    def __init__(self, name, grability_city_list):
        self.name = name
        self.grability_city_list = grability_city_list
        self.conditionals = Cities.get_conditionals(self)
        self.metrics = Cities.get_metrics(self)

    def get_conditionals(self):
        return gsheets.read_sheet('1qnZ7RABMX5OxEU8q0_KpcKPhRRVLHyHt3p5l2-UN820', '{}!A1:F'.format(self.name))
    
    def get_metrics(self):
        cities = self.grability_city_list
        cities = ["{}".format(city) for city in cities]
        cities = ','.join(cities)
        
        zones_descriptions_query = """
        select
            z.id as zone_id,
            concat(c.city, '|', z.name) as zone
        from br_PG_MS_OPS_ZONES_PUBLIC.zones z
        join br_grability_public.city_addresses c on z.city_id = c.id
        where z.city_id in ({cities})
        """.format(cities=cities)
        
        metrics_query = """
        with base as (
            select
                zone_id,
                couriers,
                free_couriers,
                orders,
                unassigned_orders,
                saturation,
                grability_city_id,
                created_at,
                rank() over (partition by zone_id order by created_at desc) as date_rank
            from zone_historic
            where
                grability_city_id in ({cities})
        )

        select
            a.zone_id,
            a.couriers,
            a.orders,
            a.saturation,
            a.free_couriers,
            a.unassigned_orders,
            coalesce((a.unassigned_orders::float/NULLIF(a.orders,0)::float)*100.0, 0) as unn_perc,
            coalesce(((a.couriers / nullif(b.couriers, 0)::float) - 1.0)*100.0, 0) as curva_rts,
            (a.created_at - interval '3 hours') as created_at,
            a.grability_city_id
        from base a
        join base b on a.zone_id = b.zone_id 
            and a.date_rank = (b.date_rank - 1)
        where a.date_rank = 1
        """.format(cities=cities)
        
        zones_descriptions = snow.run_query(zones_descriptions_query)
        
        # Database BR ba-saturations
        metrics = redash.run_query(2646, metrics_query)
        
        metrics = metrics.merge(zones_descriptions, on='zone_id')
        return metrics
            