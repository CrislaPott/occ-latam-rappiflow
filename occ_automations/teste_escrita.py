import os, re, sys, json
import numpy as np
import pandas as pd
from datetime import date, datetime
from unidecode import unidecode
from os.path import expanduser

from lib import redash, gsheets
from lib import snowflake as snow
from lib import thread_listener as tl

ar = gsheets.read_sheet('1qpyPluu90mszDWz2P39S49kFd7-wGJ7Kl6Szh8BFRmI', 'SantiagoChile')

home = expanduser("~")
ar.to_csv('{}/ar.csv'.format(home), index=False)