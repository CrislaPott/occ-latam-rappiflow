import os, sys, json, requests, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta

from lib import redash, gsheets
from lib import snowflake as snow

# Mapeamento de Bases no Redash para o MS MicroServiceCore - stores

mappings = {
    'br': 6310,
    'ar': 6309,
    'cl': 6311,
    'co': 6312,
    'mx': 6324,
    'pe': 6323,
    'ec': 6447,
    'cr': 6313,
    'uy': 6322
}

mappings_retail = {
    'br': 200,
    'ar': 198,
    'cl': 393,
    'co': 197,
    'mx': 199,
    'pe': 1213,
    'ec': 2052,
    'cr': 2053,
    'uy': 394
}

# Helper functions

def get_store_ids(df, column_name):
    store_ids = df[column_name].unique().tolist()
    store_ids = ["{}".format(store_id) for store_id in store_ids]
    store_ids = ','.join(store_ids)
    return store_ids

def get_store_status(country, store_ids):
    query_redash = """
    select store_id, is_enabled
    from stores 
    where store_id in ({})
    """.format(store_ids)

    query_excecoes = """
    with maxid as (select max(id) as m from log.retail_logs),

    b as (
        select
            distinct(replace(rl.uri,'/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/','')) as store_id
        from log.retail_logs rl
        join maxid on rl.id >= (maxid.m - 500000)
        where uri ilike '%/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/%'
        and content ->> 'is_enable' = 'true'
        and rl.user ilike '%@rappi.com%'
        and rl.created_at::date = now()::date
    )

    select * from b 
    """

    excecoes = redash.run_query(mappings_retail[country], query_excecoes)
    results = redash.run_query(mappings[country], query_redash)
    
    if excecoes.shape[0] > 0:
        excecoes_stores = excecoes['store_id'].unique().tolist()
        results = results[~results['store_id'].isin(excecoes_stores)]

    return results

# Processo 1: Suspendidas por cancellacion por store closed ( soporte, RT y chat con key words )

final = pd.DataFrame()
target_db = 'occ_bot_storeclose_checkup'

query_snowflake = """
select
    country, 
    store_id, 
    store_name, 
    vertical, 
    category, 
    order_ids,
    suspended_at::timestamp_ntz as suspended_at_local_country,
    case when vertical in ('Express','Licores','Farmacia') then filhas else null end as daughter_stores
from br_writable.occ_bot_store_closed
where 
    suspended_at::date = (
        case when country in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date                                    
        when country in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date
        when country in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date 
        end
    )
"""

suspended_stores = snow.run_query(query_snowflake)
if suspended_stores.shape[0] > 0:
    countries_list = suspended_stores['country'].unique().tolist()

    for country in countries_list:
        temp = suspended_stores[suspended_stores['country'] == country]
        store_ids = get_store_ids(temp, 'store_id')
        store_status = get_store_status(country, store_ids)
        temp = temp.merge(store_status)
        final = pd.concat([final, temp], ignore_index=True)

    try:
        snow.truncate_table(target_db)
    except:
        pass
    snow.upload_df(final, target_db)
    
elif suspended_stores.shape[0] == 0:
    try:
        snow.truncate_table(target_db)
    except:
        pass

# Processo 2: Suspendidas por Fallback

final = pd.DataFrame()
target_db = 'occ_bot_storeclose_fallback_checkup'

query_snowflake = """
--no_cache
select 
    country, 
    store_id, 
    store_name, 
    vertical, 
    category, 
    order_ids,
    suspended_at::timestamp_ntz as suspended_at_local_countr
from br_writable.occ_fallback
where 
    suspended_at::date = (
        case when country in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date                                    
        when country in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date 
        when country in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date 
        end
    )
"""

suspended_stores = snow.run_query(query_snowflake)
if suspended_stores.shape[0] > 0:
    countries_list = suspended_stores['country'].unique().tolist()

    for country in countries_list:
        temp = suspended_stores[suspended_stores['country'] == country]
        store_ids = get_store_ids(temp, 'store_id')
        store_status = get_store_status(country, store_ids)
        temp = temp.merge(store_status)
        final = pd.concat([final, temp], ignore_index=True)

    try:
        snow.truncate_table(target_db)
    except:
        pass
    snow.upload_df(final, target_db)
    
elif suspended_stores.shape[0] == 0:
    try:
        snow.truncate_table(target_db)
    except:
        pass

# Processo 3: Ecommerce - Email

final = pd.DataFrame()
target_db = 'occ_bot_storeclose_ecommerce_checkup'

query_snowflake = """
--no_cache
select
  store_closed_logs.country as country
  , store_closed_logs.store_id
  , si.name as storename,
 
--   , store_closed_logs.vertical 
  si.city
  , type
  --, is_published
--   , to_char((case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD HH24:MI') as suspended_at 
-- , to_char((case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD') as suspended_date
  , order_ids
from
  br_writable.occ_suspended_stores store_closed_logs

left join (select 'br' as country, store_id, name, type, is_enabled, is_published , ca.city
           from br_PGLR_MS_STORES_PUBLIC.stores_vw
           left join br_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          union all 
           select 'co' as country, store_id, name, type, is_enabled, is_published , ca.city
           from co_PGLR_MS_STORES_PUBLIC.stores_vw
           left join co_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'ar' as country, store_id, name, type, is_enabled, is_published , ca.city
           from ar_PGLR_MS_STORES_PUBLIC.stores_vw
           left join ar_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'cl' as country, store_id, name, type, is_enabled, is_published , ca.city
           from cl_PGLR_MS_STORES_PUBLIC.stores_vw
           left join cl_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'pe' as country, store_id, name, type, is_enabled, is_published , ca.city
           from pe_PGLR_MS_STORES_PUBLIC.stores_vw
           left join pe_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'mx' as country, store_id, name, type, is_enabled, is_published , ca.city
           from mx_PGLR_MS_STORES_PUBLIC.stores_vw
           left join mx_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'uy' as country, store_id, name, type, is_enabled, is_published , ca.city
           from uy_PGLR_MS_STORES_PUBLIC.stores_vw
           left join uy_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          ) si on si.country=store_closed_logs.country and si.store_id=store_closed_logs.store_id

where (case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::date = case when store_closed_logs.country in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date when store_closed_logs.country in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date when store_closed_logs.country in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date end

and reason in ('ecommerce_process')
"""

suspended_stores = snow.run_query(query_snowflake)
if suspended_stores.shape[0] > 0:
    countries_list = suspended_stores['country'].unique().tolist()

    for country in countries_list:
        temp = suspended_stores[suspended_stores['country'] == country]
        store_ids = get_store_ids(temp, 'store_id')
        store_status = get_store_status(country, store_ids)
        temp = temp.merge(store_status)
        final = pd.concat([final, temp], ignore_index=True)

    try:
        snow.truncate_table(target_db)
    except:
        pass
    snow.upload_df(final, target_db)
    
elif suspended_stores.shape[0] == 0:
    try:
        snow.truncate_table(target_db)
    except:
        pass

# Processo 4: Small Worst Offenders - Email

final = pd.DataFrame()
target_db = 'occ_bot_storeclose_smallwo_checkup'

query_snowflake = """
--no_cache
select
  store_closed_logs.country as country
  , store_closed_logs.store_id
  , si.name as storename
--   , store_closed_logs.vertical 
  , si.city
  --, is_enabled
  --, is_published
--   , to_char((case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD HH24:MI') as suspended_at 
-- , to_char((case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD') as suspended_date
  , order_ids
from
  br_writable.occ_suspended_stores store_closed_logs

left join (select 'br' as country, store_id, name, is_enabled, is_published , ca.city
           from br_PGLR_MS_STORES_PUBLIC.stores_vw
           left join br_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          union all 
           select 'co' as country, store_id, name, is_enabled, is_published , ca.city
           from co_PGLR_MS_STORES_PUBLIC.stores_vw
           left join co_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'ar' as country, store_id, name, is_enabled, is_published , ca.city
           from ar_PGLR_MS_STORES_PUBLIC.stores_vw
           left join ar_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'cl' as country, store_id, name, is_enabled, is_published , ca.city
           from cl_PGLR_MS_STORES_PUBLIC.stores_vw
           left join cl_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'pe' as country, store_id, name, is_enabled, is_published , ca.city
           from pe_PGLR_MS_STORES_PUBLIC.stores_vw
           left join pe_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'mx' as country, store_id, name, is_enabled, is_published , ca.city
           from mx_PGLR_MS_STORES_PUBLIC.stores_vw
           left join mx_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all 
           select 'uy' as country, store_id, name, is_enabled, is_published , ca.city
           from uy_PGLR_MS_STORES_PUBLIC.stores_vw
           left join uy_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          ) si on si.country=store_closed_logs.country and si.store_id=store_closed_logs.store_id

where (case when store_closed_logs.country in ('co','pe','ec') then dateadd(hour,-2,suspended_at) when store_closed_logs.country in ('cr','mx') then dateadd(hour,-3,suspended_at) else suspended_at end)::date = case when store_closed_logs.country in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date when store_closed_logs.country in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date when store_closed_logs.country in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date end

and reason in ('now_process')
"""

suspended_stores = snow.run_query(query_snowflake)
if suspended_stores.shape[0] > 0:
    countries_list = suspended_stores['country'].unique().tolist()

    for country in countries_list:
        temp = suspended_stores[suspended_stores['country'] == country]
        store_ids = get_store_ids(temp, 'store_id')
        store_status = get_store_status(country, store_ids)
        temp = temp.merge(store_status)
        final = pd.concat([final, temp], ignore_index=True)

    try:
        snow.truncate_table(target_db)
    except:
        pass
    snow.upload_df(final, target_db)
    
elif suspended_stores.shape[0] == 0:
    try:
        snow.truncate_table(target_db)
    except:
        pass

# Processo 5: Store Closed Recurrent

final = pd.DataFrame()
target_db = 'occ_bot_storeclose_recurrent_checkup'

mapping_country = {
    'BRAZIL': 'br',
    'COLOMBIA': 'co',
    'ARGENTINA': 'ar',
    'CHILE': 'cl',
    'PERU': 'pe',
    'MEXICO': 'mx',
    'URUGUAI': 'uy',
    'EQUADOR': 'ec',
    'COSTARICA': 'cr'
}

query_snowflake = """
--no_cache
with
	occ_slack_bot_shutdowns as (
		select
			occ.store_id,
      st.name,
      case
        when type = 'restaurant' then 'Restaurant'
        when "GROUP" = 'ecommerce' and store_type not in ('petz_loja', 'petzdark_loja') then 'Ecommerce'
        else 'CPGs'
			end as vertical,
			'OCC' as responsible,
      initcap(suspended_reason) as reason,
      'Suspension' as action,
    country,
			dateadd (s, shutdown_moment - 3600*3, '1970-01-01'::timestamp) as shutdown_moment,
			case
				when timeframe::varchar = 'indefinido' then timeframe::varchar
				else to_timestamp_ntz(timeframe, 'dd/mm/yyyy')::varchar
			end as scheduled_revival
		from br_writable.occ_suspensions occ
    left join br_grability_public.stores st on (st.store_id = occ.store_id)
    left join
      (select distinct
          "GROUP",
          store_type
      from br_grability_public.verticals
      ) vert on (vert.store_type = st.type)
		where dateadd (s, shutdown_moment - 3600*3, '1970-01-01'::timestamp)::date between dateadd ('days', -7, convert_timezone ('America/Recife', current_timestamp))::date and convert_timezone ('America/Recife', current_timestamp)::date
	),

	stores_shutdowns as (
		select * from occ_slack_bot_shutdowns
		order by shutdown_moment desc
	),

	suspended_stores as (
    select distinct
        store_id
    from ops_global.suspended_stores
    where
        country = 'BR'
	)

, a as (
select
	shut.store_id,
  shut.name as store_name,
	shut.vertical,
	shut.responsible,
  --shut.reason,
  --shut.action,
  --susp.store_id is null as is_active,
  country,
	split_part (date_trunc ('seconds', shut.shutdown_moment) ,'.', 1) as inicio_suspensao,
  case
    when shut.scheduled_revival::varchar = 'indefinido' then shut.scheduled_revival::varchar
    else split_part (date_trunc ('seconds', shut.scheduled_revival::timestamp_ntz) ,'.', 1)::varchar
  end as fim_suspensao
from stores_shutdowns shut
left join suspended_stores susp on (susp.store_id = shut.store_id)
order by shutdown_moment desc
  )


,final as (
select a.* from a
  where inicio_suspensao::date=dateadd(day, -1, case when lower(country) in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date when lower(country) in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date when lower(country) in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date end)::date
order by inicio_suspensao desc
)

select
  upper(store_closed_logs.country) as country_
  , store_closed_logs.store_id
  , si.name as storename

  , si.city
  , case when f.store_id is not null then true else false end as suspended
  , f.responsible
  , to_char((case when country_ in ('COLOMBIA','PERU','EQUADOR') then dateadd(hour,-2,suspended_at) when country_ in ('COSTARICA','MEXICO') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD HH24:MI') as suspended_at
, to_char((case when country_ in ('COLOMBIA','PERU','EQUADOR') then dateadd(hour,-2,suspended_at) when country_ in ('COSTARICA','MEXICO') then dateadd(hour,-3,suspended_at) else suspended_at end)::timestamp_ntz,'YYYY-MM-DD') as suspended_date
  , order_ids
from
  br_writable.occ_suspended_stores store_closed_logs

left join (select store_id, country, listagg(responsible, ',') as responsible from final group by 1,2) f on f.store_id=store_closed_logs.store_id and f.country=upper(store_closed_logs.country)

left join (select 'BRAZIL' as country, store_id, name, is_enabled, is_published , ca.city
           from br_grability_public.stores_vw
           left join br_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          union all
           select 'COLOMBIA' as country, store_id, name, is_enabled, is_published , ca.city
           from co_grability_public.stores_vw
           left join co_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all
           select 'ARGENTINA' as country, store_id, name, is_enabled, is_published , ca.city
           from ar_grability_public.stores_vw
           left join ar_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all
           select 'CHILE' as country, store_id, name, is_enabled, is_published , ca.city
           from cl_grability_public.stores_vw
           left join cl_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all
           select 'PERU' as country, store_id, name, is_enabled, is_published , ca.city
           from pe_grability_public.stores_vw
           left join pe_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all
           select 'MEXICO' as country, store_id, name, is_enabled, is_published , ca.city
           from mx_grability_public.stores_vw
           left join mx_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
           union all
           select 'URUGUAI' as country, store_id, name, is_enabled, is_published , ca.city
           from uy_grability_public.stores_vw
           left join uy_grability_public.city_addresses_vw ca on ca.id=stores_vw.city_address_id
           where coalesce(stores_vw._fivetran_deleted,'false')=false and deleted_at is null
          ) si on si.country=upper(store_closed_logs.country) and si.store_id=store_closed_logs.store_id

where (case when country_ in ('COLOMBIA','PERU','EQUADOR') then dateadd(hour,-2,suspended_at) when country_ in ('COSTARICA','MEXICO') then dateadd(hour,-3,suspended_at) else suspended_at end)::date = (case when country_ in ('COLOMBIA','PERU','EQUADOR') then convert_timezone('America/Bogota',current_timestamp)::date when country_ in ('BRAZIL','CHILE','ARGENTINA','URUGUAI') then convert_timezone('America/Buenos_Aires',current_timestamp)::date when country_ in ('COSTARICA','MEXICO') then convert_timezone('America/Costa_Rica',current_timestamp)::date end)::date

and reason in ('storeclosed_recorrente')
"""

suspended_stores = snow.run_query(query_snowflake)
suspended_stores['country'] = suspended_stores['country_'].map(mapping_country)
if suspended_stores.shape[0] > 0:
    countries_list = suspended_stores['country'].unique().tolist()

    for country in countries_list:
        temp = suspended_stores[suspended_stores['country'] == country]
        store_ids = get_store_ids(temp, 'store_id')
        store_status = get_store_status(country, store_ids)
        temp = temp.merge(store_status)
        final = pd.concat([final, temp], ignore_index=True)
        
    try:
        snow.truncate_table(target_db)
    except:
        pass
    snow.upload_df(final, target_db)
    
elif suspended_stores.shape[0] == 0:
    try:
        snow.truncate_table(target_db)
    except:
        pass

