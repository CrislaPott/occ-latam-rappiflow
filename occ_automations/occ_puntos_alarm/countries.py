import numpy as np
import pandas as pd
from lib import gsheets, redash
from lib import snowflake as snow

class Countries():
    def __init__(self, country_name, zone_mappings, puntos_query):
        self.country_name = country_name
        self.zone_mappings = zone_mappings
        self.puntos_query = puntos_query
        self.conditionals = Countries.get_conditionals(self)
        self.puntos = Countries.get_puntos(self)
        self.metrics = Countries.get_metrics(self)
        self.country_df = Countries.get_country_df(self)
        
    def get_conditionals(self):
        return gsheets.read_sheet('1v4vnJpGu3dE6aseGdgPrf2C7hpsCV30Dc_gwao0mGBY', '{}!A1:G'.format(self.country_name))
    
    def get_puntos(self):
        return snow.run_query(self.puntos_query)
    
    def get_country_df(self):
        conditionals = self.conditionals.copy()
        metrics = self.metrics.copy()
        puntos = self.puntos.copy()

        if puntos.shape[0] == 0:
            return 0

        conditionals['zone_id'] = conditionals['zone'].map(self.zone_mappings)
        puntos['zone_id'] = puntos['zone'].map(self.zone_mappings)

        total = puntos.merge(metrics, on='zone_id')
        total = total.merge(conditionals, on=['zone_id', 'zone'])
        total = total[['zone', 'zone_id', 'couriers', 'orders', 'free_couriers', 'unassigned_orders', 'saturation', 'unn_perc', 'curva_rts', 'time_of_day', 'vehicle', 'current_zone_points', 'zone_max_points', 'normalized_points', 'reduction_points', 'unn_threshold', 'lat', 'lon']]

        return total
    
    def get_metrics(self):
        zones = list(self.zone_mappings.values())
        zones = ["{}".format(zone) for zone in zones]
        zones = ','.join(zones)
        metrics_query = """
        with base as (
            select
                zone_id,
                couriers,
                free_couriers,
                orders,
                unassigned_orders,
                saturation,
                grability_city_id,
                created_at,
                rank() over (partition by zone_id order by created_at desc) as date_rank
            from zone_historic
            where
                zone_id in ({zones})
        )

        select
            a.zone_id,
            a.couriers,
            a.orders,
            a.saturation,
            a.free_couriers,
            a.unassigned_orders,
            coalesce((a.unassigned_orders::float/NULLIF(a.orders,0)::float)*100.0, 0) as unn_perc,
            coalesce(((a.couriers / nullif(b.couriers, 0)::float) - 1.0)*100.0, 0) as curva_rts,
            (a.created_at - interval '3 hours') as created_at,
            a.grability_city_id
        from base a
        join base b on a.zone_id = b.zone_id and a.date_rank = (b.date_rank - 1)
        where a.date_rank = 1
        """.format(zones=zones)
        
        # Database ba-saturations
        if self.country_name == 'BR':
            return redash.run_query(2646, metrics_query)
        elif self.country_name == 'CO':
            return redash.run_query(2645, metrics_query)
        