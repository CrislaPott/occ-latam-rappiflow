mappings_co = {
    'chapinero': 4,
    'usaquen_y_cedritos': 7,
    'la_candelaria': 11,
    'suba_y_colina': 6,
    'fontibon': 10,
    'salitre': 5,
    'antonio_narino_y_sur': 8,
    'engativa': 9,
    'kennedy': 3,
    'poblado_centro': 17,
    'envigado': 22,
    'belen_laureles': 16,
    'itagui': 21,
    'sabaneta/la_estrella': 18,
    'sur': 30,
    'ingenio_limonar': 25,
    'lido_plametto': 29,
    'oeste': 27,
    'norte': 28,
    'riomar_y_altoprado_baq1': 55,
    'nogales_ciudad_jardin_baq': 64,
    'villa_carolina_y_paraiso_baq1': 57,
    'prado_baq2': 58,
    'villa_santos_y_miramar_baq': 63,
    'via_40_baq': 56,
    'barrio_abajo_baq2': 59,
    'los_pinos_baq': 65,
    'soledad_centro': 66,
    'villa_campestre_baq': 62
}

mappings_br = {
    'rec_zona_norte/nordeste': 29,
    'rec_zona_sul': 35
}