import os, sys, json
import numpy as np
import pandas as pd
from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps

sys.path.insert(0, "../")

from lib import gsheets, redash, slack
from lib import snowflake as snow
import countries, puntos_queries, puntos_zone_mappings

os.chdir('../')

owm = OWM(os.getenv('OWM_API_KEY'))
mgr = owm.weather_manager()

co = countries.Countries('CO', puntos_zone_mappings.mappings_co, puntos_queries.puntos_co)
br = countries.Countries('BR', puntos_zone_mappings.mappings_br, puntos_queries.puntos_br)

def run_rain_alarm(country):
    df = country.country_df
    for index, row in df.iterrows():
        country_name = country.country_name
        zone = row['zone']
        zone_id = row['zone_id']
        lat = row['lat']
        lon = row['lon']
        vehicle = row['vehicle']
        saturation = row['saturation']
        curva_rts = row['curva_rts']
        norm = row['normalized_points']
        
        try:
            lat = float(row['lat'].replace(',', '.'))
            lon = float(row['lon'].replace(',', '.'))
            weather_conditions = mgr.weather_at_coords(lat=lat, lon=lon).weather
            weather_status = weather_conditions.detailed_status
            
            if (norm > 0) and (saturation >= 1) and (curva_rts <= -20) and ('rain' in weather_status):
                trigger_text = """
                *Alerta del programa de puntos por Lluvia*
                *Pais*: {country_name}
                *Zona*: {zone}
                *ID Zona*: {zone_id}
                *Operación*: {vehicle}
                *Normalización actual*: {norm}
                *Condición meteorológica actual*: {weather_status}
                *Lluvia en mm en la última hora*: {rain_1h}
                """.format(
                   country_name=country_name,
                   zone=zone,
                   zone_id=zone_id,
                   vehicle=vehicle,
                   norm=norm,
                   weather_status=weather_status,
                   rain_1h=weather_conditions.rain['1h']
                )
                slack.send_message_channel('C01DJK11W66', trigger_text)
        except:
            pass        
        
def run_unassigned_alarm(country):
    df = country.country_df
    for index, row in df.iterrows():
        country_name = country.country_name
        zone = row['zone']
        zone_id = row['zone_id']
        vehicle = row['vehicle']
        unn = round(row['unn_perc'], 3)
        norm = row['normalized_points']
        low_unn, mid_unn, high_unn = row['unn_threshold'].split('-')
        low_norm, mid_norm, high_norm = row['reduction_points'].split('-')
        
        low_unn = int(low_unn)
        mid_unn = int(mid_unn)
        high_unn = int(high_unn)
        
        print('Country: {}, Zone: {}, Zone_ID: {}, Unn: {}, Norm: {}'.format(country_name, zone, zone_id, unn, norm))
        
        trigger_text = """
        *Alerta del programa de puntos por No Asignados*
    
        *Pais*: {country_name}
        *Zona*: {zone}
        *ID Zona*: {zone_id}
        *Operación*: {vehicle}
        *No Asignados - %*: {unn}
        *Normalización actual*: {norm}
        """.format(
            country_name=country_name,
            zone=zone,
            zone_id=zone_id,
            vehicle=vehicle,
            unn=unn,
            norm=norm
        )
        
        if (norm > 0):
            if (low_unn <= unn < mid_unn):
                slack.send_message_channel('C01DJK11W66', trigger_text)
            elif (mid_unn <= unn < high_unn):
                slack.send_message_channel('C01DJK11W66', trigger_text)
            elif unn >= high_unn:
                slack.send_message_channel('C01DJK11W66', trigger_text)
                
for country in [co, br]:
    if country.puntos.shape[0] == 0:
        print('Programa de pontos não está rodando em {}'.format(country.country_name))
        continue
    
    run_unassigned_alarm(country)
    run_rain_alarm(country)