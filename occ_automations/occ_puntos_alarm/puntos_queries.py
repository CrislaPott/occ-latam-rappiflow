puntos_co = """
WITH adjustments_ranked AS (
    /*  Create a table ranked for most recent modifications to zones and vehicles */
    SELECT
        'CO' AS country_code
        ,zone_id
        ,split_part(zone_id, '|', '3') AS zone
        ,city
        ,restriction_id
        ,vehicle
        ,created_at
        ,DAYOFWEEK(created_at::DATE) AS dow
        ,adjust_points
        ,required_points
        ,event
        ,event_timer
        ,weather
        ,weather_timer
        ,CASE
            WHEN restriction_id IN (30,32,40,38) THEN 'Morning'
            WHEN restriction_id IN (55,54,52,53) THEN 'Lunch'
            WHEN restriction_id IN (26,28,41,39) THEN 'Afternoon'
            WHEN restriction_id IN (49,50,48,51) THEN 'Dinner'
            ELSE 'Error'
        END AS time_of_day
        ,RANK() OVER (PARTITION BY zone_id, vehicle ORDER BY created_at DESC) AS mod_rank

    FROM OPS_GLOBAL.CO_zone_points_adjusted_ops
    WHERE created_at::DATE >= DATEADD(DAY, -2, CURRENT_DATE())
        AND zone IN ('suba_y_colina', 'la_candelaria', 'usaquen_y_cedritos', 'fontibon', 'salitre', 'antonio_narino_y_sur', 'engativa', 'chapinero', 'kennedy', 'norte', 'sur', 'ingenio_limonar', 'lido_plametto', 'oeste', 'jamudi', 'nogales_ciudad_jardin_baq', 'villa_carolina_y_paraiso_baq1', 'villa_santos_y_miramar_baq', 'riomar_y_altoprado_baq1', 'prado_baq2', 'poblado_centro', 'itagui', 'sabaneta/la_estrella', 'la_estrella', 'belen_laureles', 'envigado', 'alto_de_las_palmas')
)

/* Get the current configuration of each zone and vehicle, with it's latest modification timestamp */
SELECT
    po.country_code
    ,po.city
    ,po.zone
    ,po.time_of_day
    ,CASE
        WHEN po.vehicle = 0 THEN 'Caminante'
        WHEN po.vehicle = 1 THEN 'Bici'
        WHEN po.vehicle = 2 THEN 'Moto'
        WHEN po.vehicle = 3 THEN 'Carro'
        ELSE 'Error'
    END AS vehicle

    ,po.created_at
    ,po.restriction_id
     ,po.adjust_points AS current_zone_points
    ,po.adjust_points / th.max_score AS normalized_points
    ,th.max_score AS zone_max_points

FROM adjustments_ranked po
    LEFT JOIN co_writable.global_points_thresholds th ON po.country_code = th.country
        AND th.zone_id = po.zone_id AND po.restriction_id = th.restriction_id
        AND po.vehicle = th.vehicle
        AND po.dow = th.day_of_week
        AND DATE_TRUNC('WEEK', po.created_at::TIMESTAMP) = th.week_valid

WHERE mod_rank = 1       -- Restrict to get just latest modification which means current state of things
    AND created_at::DATE = CURRENT_DATE
    AND city IN ('bogota','medellin','cali','barranquilla')

ORDER BY country, city, zone, vehicle, created_at DESC
"""

puntos_br = """
WITH adjustments_ranked AS (
    /*  Create a table ranked for most recent modifications to zones and vehicles */
    SELECT
        'BR' AS country_code
        ,zone_id
        ,split_part(zone_id, '|', '3') AS zone
        ,city
        ,restriction_id
        ,vehicle
        ,created_at
        ,DAYOFWEEK(created_at::DATE) AS dow
        ,adjust_points
        ,required_points
        ,event
        ,event_timer
        ,weather
        ,weather_timer
        ,CASE
            WHEN restriction_id IN (46) THEN 'Morning'
            WHEN restriction_id IN (62) THEN 'Lunch'
            WHEN restriction_id IN (47) THEN 'Afternoon'
            WHEN restriction_id IN (57) THEN 'Dinner'
            ELSE 'Error'
        END AS time_of_day
        ,RANK() OVER (PARTITION BY zone_id, vehicle ORDER BY created_at DESC) AS mod_rank
    FROM OPS_GLOBAL.BR_zone_points_adjusted_ops
    WHERE created_at::DATE >= DATEADD(DAY, -2, CURRENT_DATE())
        AND zone IN ('rec_zona_norte/nordeste','rec_zona_sul')
)
/* Get the current configuration of each zone and vehicle, with it's latest modification timestamp */
SELECT
    po.country_code
    ,po.city
    ,po.zone
    ,po.time_of_day
    ,CASE
        WHEN po.vehicle = 0 THEN 'Caminante'
        WHEN po.vehicle = 1 THEN 'Bici'
        WHEN po.vehicle = 2 THEN 'Moto'
        WHEN po.vehicle = 3 THEN 'Carro'
        ELSE 'Error'
    END AS vehicle
    ,po.adjust_points AS current_zone_points
    ,po.created_at
    ,po.restriction_id
    ,th.max_score AS zone_max_points
    ,po.adjust_points / th.max_score AS normalized_points
FROM adjustments_ranked po
    LEFT JOIN co_writable.global_points_thresholds th ON po.country_code = th.country
        AND th.zone_id = po.zone_id AND po.restriction_id = th.restriction_id
        AND po.vehicle = th.vehicle
        AND po.dow = th.day_of_week
        AND DATE_TRUNC('WEEK', po.created_at::TIMESTAMP) = th.week_valid
WHERE mod_rank = 1       -- Restrict to get just latest modification which means current state of things
    AND created_at::DATE = CURRENT_DATE
    AND city IN ('recife')
ORDER BY country, city, zone, vehicle, created_at DESC
"""

