import os, sys, json, pytz
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import snowflake as snow
from datetime import datetime, timedelta

def get_db_interval():
    query = """
    select
        max(created_at) as last_refresh 
    from zone_historic
    """

    last_refresh = redash.run_query(2646, query)

    for column in last_refresh.columns:
        last_refresh[column] = pd.to_datetime(last_refresh['last_refresh'])

    time_diff = datetime.now() - last_refresh.loc[0, 'last_refresh']

    return time_diff.total_seconds() / 60

def get_zones():
    query_zones = """
    select
        z.city_id as city_id,
        c.city as city_name,
        z.id as zone_id,
        z.name as zone_name
    from br_pglr_ms_ops_zones_public.zones z 
    join br_grability_public.city_addresses_vw c on z.city_id = c.id
    group by 1, 2, 3, 4
    order by 1 asc, 3 asc
    """

    zones = snow.run_query(query_zones)
    zones = filter_zones(zones)
    metrics = get_metrics(zones)

    zones = zones.merge(metrics, on='zone_id')
    return zones

def filter_zones(zones):
    mask = zones['city_name'].isin([
        'Aracaju', 'Balneario Camboriu', 'Campo Grande', 'João Pessoa', 'Juiz de Fora', 'Jundiaí', 'Londrina',
        'Maceió', 'Manaus', 'Santos', 'Sorocaba', 'São José do Rio Preto', 'São José dos Campos', 'Teresina',
        'Uberlandia', 'Vila Velha', 'Vitória', 'Belém', 'Caxias do Sul'
    ])

    return zones[mask]

def get_metrics(zones):
    cities = zones['city_id'].unique().tolist()
    cities = ["{}".format(city) for city in cities]
    cities = ','.join(cities)

    query_metrics = """
    --no_cache
    with base as (
        select
            zone_id,
            couriers,
            free_couriers,
            orders,
            unassigned_orders,
            saturation,
            grability_city_id,
            created_at,
            rank() over (partition by zone_id order by created_at desc) as date_rank
        from zone_historic
        where
            grability_city_id in ({})
    )

    select
        zone_id,
        grability_city_id,
        orders,
        unassigned_orders,
        couriers,
        free_couriers
    from base
    where date_rank = 1
    """.format(cities)

    metrics = redash.run_query(2646, query_metrics)
    return metrics

def summarize_cities(zones):
    cities = zones.groupby(
        ['city_id', 'city_name']
    ).agg(
        orders=('orders', 'sum'),
        unassigned_orders=('unassigned_orders', 'sum'),
        couriers=('couriers', 'sum'),
        free_couriers=('free_couriers', 'sum')
    ).reset_index()
    
    cities['unn_perc'] = round((cities['unassigned_orders'] / cities['orders']) * 100, 1)
    cities['saturation'] = round(cities['orders'] / cities['couriers'], 1)

    cities.fillna(0, inplace=True)
    
    return cities

zones = get_zones()
cities = summarize_cities(zones)

for index, row in cities.iterrows():
    city = row['city_name']
    city_id = row['city_id']
    
    orders = row['orders']
    unn = row['unassigned_orders']
    rts = row['couriers']
    rts_livres = row['free_couriers']

    unn_perc = row['unn_perc']
    saturation = row['saturation']

    if (unn_perc >= 30 and unn >= 5):
        trigger_text = """
        :alert: *Alerta de Balance - Cidades T3 Brasil* :alert: 
        
        *País*: BR - :flag-br:

        *FYI: @occ_monit-balance
        
        *Cidade*: {city}
        *Unassigned - %*: {unn_perc}
        *Saturação*: {saturation}
        
        *Ordens*: {orders}
        *Ordens Unassgined*: {unn}
        *RTs*: {rts}
        *RTs Livres*: {rts_livres}
        
        """.format(
            city=city, 
            unn_perc=unn_perc, 
            saturation=saturation,
            orders=orders,
            unn=unn,
            rts=rts,
            rts_livres=rts_livres
        )

        slack.send_message_names_channel('C01672B528Y', trigger_text)
    
print('Rodou')

    