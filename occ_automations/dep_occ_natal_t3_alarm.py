import os, sys, json, pytz
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import snowflake as snow
from datetime import datetime, timedelta

begin = str(datetime.now(pytz.timezone('America/Recife')) - timedelta(minutes=20))

print(begin)

query_orders = """
--no_cache
select
    o.id as order_id,
    o.created_at as created_at,
    os.store_id as store_id,
    os.name as store_name,
    os.type as store_type
from orders o 
left join order_stores os on o.id = os.order_id
where o.created_at >= '{}'
and os.is_marketplace = false 
""".format(begin)

query_cities = """
select
    s.store_id,
    c.city
from br_grability_public.city_addresses c
join br_grability_public.stores s on c.id = s.city_address_id
where c.city in (
    'Aracaju', 'Balneario Camboriu', 'Campo Grande', 'João Pessoa', 'Juiz de Fora', 'Jundiaí', 'Londrina',
    'Maceió', 'Manaus', 'Santos', 'Sorocaba', 'São José do Rio Preto', 'São José dos Campos', 'Teresina',
    'Uberlandia', 'Vila Velha', 'Vitória', 'Belém', 'Caxias do Sul'
)
and c.country_id = 5
"""

query_verticals = """
select
    distinct store_type,
    'group' as vertical,
    sub_group
from br_grability_public.verticals
where sub_group not in ('Farmacia','super','Boletas','Rappi','Rappifavor','Informativo','Super','Hiper')
    and vertical not in ('ecommerce')
"""

orders = redash.run_query(1338, query_orders)
cities = snow.run_query(query_cities)
verticals = snow.run_query(query_verticals)

orders = orders.merge(cities, on='store_id', how='inner')
orders = orders.merge(verticals, on='store_type', how='inner')

print('Ordens: {}'.format(orders.shape[0]))

if orders.shape[0] > 0:
    results = orders.groupby([
        'city',
        'store_id',
        'store_name'
    ]).agg(
        orders=('order_id', 'nunique'),
        orders_list=('order_id', 'unique')
    ).reset_index().sort_values(by='orders', ascending=False)
    
    results_file = 'occ_t3_alarm_natal.xlsx'

    results.to_excel(results_file, index=False)

    texto = """
    *Alarme Natal - Pedidos em Lojas de Cidades Pequenas (T3)*

    Segue listagem de lojas com pedidos em cidades pequenas nos últimos 20 minutos.
    """

    slack.file_upload_channel(channel='C01672B528Y', text=texto, filename=results_file, filetype='xlsx')

    if os.path.exists(results_file):
        os.remove(results_file)
    