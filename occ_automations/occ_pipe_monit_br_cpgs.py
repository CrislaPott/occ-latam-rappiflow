import os, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import thread_listener as tl
from lib import snowflake as snow

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)

channel_history = tl.get_channel_history(
    channel = "C012VS4D5PY",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

cpgs_threads = channel_history[
    (channel_history['username'].str.contains('CPGs')) &
    (channel_history['thread_ts'].notna()) &
    (channel_history['subtype'] == 'bot_message')
].drop_duplicates(subset='thread_ts', keep='first')

cpgs_threads = cpgs_threads[['thread_ts', 'username']]
cpgs_threads.rename(columns={'username': 'thread_name'}, inplace=True)

cpgs_threads_messages = pd.DataFrame()
for index, row in cpgs_threads.iterrows():
    thread_messages = tl.get_thread_replies('C012VS4D5PY', row['thread_ts'])
    cpgs_threads_messages = pd.concat([cpgs_threads_messages, thread_messages], ignore_index=True)
    
cpgs_threads_messages = cpgs_threads_messages[['user', 'thread_ts', 'ts', 'text']]
cpgs_threads_messages['text'] = cpgs_threads_messages['text'].str.lower()
cpgs_threads_messages['text'] = cpgs_threads_messages['text'].apply(lambda x: unidecode(x))

cpgs_threads_messages = cpgs_threads_messages.merge(cpgs_threads, on='thread_ts')

print('Quantidade de mensagens nas threads: {}'.format(cpgs_threads_messages.shape[0]))

users_query = """
select * from br_writable.slack_users
"""

users = snow.run_query(users_query)
users.columns = ['user', 'analyst_name']

cpgs_threads_messages = cpgs_threads_messages.merge(users, on='user', how='left')
cpgs_threads_messages = cpgs_threads_messages[
    (~cpgs_threads_messages['analyst_name'].isna()) &
    (~cpgs_threads_messages['thread_name'].str.contains('KPI'))
]

print(cpgs_threads_messages.head())

snow.upload_df(cpgs_threads_messages, 'occ_monit_indicadores_cpgs_logs')