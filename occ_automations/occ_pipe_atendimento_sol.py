import os, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import gsheets, slack
from lib import thread_listener as tl
from lib import snowflake as snow

slack_users = pd.read_csv('input/slack_users.csv')
occ_users = gsheets.read_sheet('1aN4HB97rnldRgJDfFSA5tPDIoEnaZmlpNXLaRZBbXr4', 'users')

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
channel_name = 'CS7UQAMLG'

channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

solicitations = channel_history[
    (channel_history['subtype'].isna()) &
    (channel_history['user'].notna())
]

def get_occ_users(thread_users):
    try:
        usernames = slack_users[
            slack_users['slack_user_id'].isin(thread_users)
        ]['slack_user'].unique().tolist()

        occ_thread_users = occ_users[
            occ_users['analyst_name'].isin(usernames)
        ]['analyst_name'].unique().tolist()

        if len(occ_thread_users) == 0:
            return np.nan
        
        return occ_thread_users 
    except:
        return np.nan

def get_non_occ_users(thread_users):
    try:
        usernames = slack_users[
            slack_users['slack_user_id'].isin(thread_users)
        ]

        non_occ_thread_users = usernames[
            ~(usernames['slack_user'].isin(occ_users['analyst_name'].unique().tolist()))
        ]['slack_user'].unique().tolist()

        if len(non_occ_thread_users) == 0:
            return np.nan 
        
        return non_occ_thread_users
    except:
        return np.nan 

def get_slack_user(user_id):
    try:
        username = slack_users[
            slack_users['slack_user_id'] == user_id
        ]['slack_user'].unique().tolist()[0]
        return username
    except:
        return np.nan

solicitations['thread_ts'] = solicitations['ts']
solicitations['solicitante'] = solicitations['user'].apply(lambda x: get_slack_user(x))
solicitations['occ_users'] = solicitations['reply_users'].apply(lambda x: get_occ_users(x))
solicitations['non_occ_users'] = solicitations['reply_users'].apply(lambda x: get_non_occ_users(x))

solicitations = solicitations[['solicitante', 'thread_ts', 'text', 'reply_users', 'occ_users', 'non_occ_users']]

occ_replies = pd.DataFrame()
for index, row in solicitations.iterrows():
    try: 
        messages = tl.get_thread_replies(channel_name, row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: get_slack_user(x))
        messages = messages[['thread_ts', 'ts', 'slack_user', 'text']]

        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['analyst_name'].unique().tolist())) &
            (messages['slack_user'].notna())
        ]

        occ_replies_thread = occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_occ_reply=('ts', 'min'),
            last_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        occ_replies = pd.concat([occ_replies, occ_replies_thread])
    except:
        pass

solicitations = solicitations.merge(occ_replies, on='thread_ts', how='left')

solicitations['reply_users'] = solicitations['reply_users'].astype(str)
solicitations['occ_users'] = solicitations['occ_users'].astype(str)
solicitations['non_occ_users'] = solicitations['non_occ_users'].astype(str)

print(solicitations.head())

print('Fazendo upload das threads')
snow.upload_df(solicitations, 'occ_atendimento_solicitations')