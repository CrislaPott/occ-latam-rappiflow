import os, sys, json, requests 
import numpy as np
import pandas as pd
from datetime import date, datetime

from lib import redash, slack
from lib import snowflake as snow

def get_db_ids(country):
    mappings = {
        'co': [1904, 13],
        'br': [1338, 231],
        'ar': [1337, 232],
        'cl': [1155, 309],
        'mx': [1371, 205],
        'cr': [1921, 2179],
        'uy': [1156, 306],
        'ec': [1922, 2177],
        'pe': [1157, 684]
    }
    return mappings[country]

def get_cancel_orders(db_id):
    query = """
    select distinct order_id
    from order_modifications
    where type = 'cancel_by_user'
    and created_at >= (select max(created_at) from order_modifications) - interval '30 minute'
    and created_at <= (select max(created_at) from order_modifications)
    """

    results = redash.run_query(db_id, query)
    results = results['order_id'].unique().tolist()
    return results

def get_order_modifications(db_id, order_ids):
    order_ids = ["{}".format(order_id) for order_id in order_ids]
    order_ids = ','.join(order_ids)

    query = """
    select
        order_id,
        type
    from order_modifications
    where order_id in ({})
    """.format(order_ids)

    results = redash.run_query(db_id, query)
    results = results.groupby(['order_id']).agg(
        modification_types=('type', 'unique')
    ).reset_index()

    results['modification_types'] = results['modification_types'].apply(lambda x: sorted(x))
    results['modification_types'] = results['modification_types'].astype(str)

    return results 

def filter_orders(df):
    df = df[df['modification_types'] == "['cancel_by_user', 'created_queue']"]
    return df

def get_iteration_info(db_id, df):
    order_ids = df['order_id'].unique().tolist()
    order_ids = ["{}".format(order_id) for order_id in order_ids]
    order_ids = ','.join(order_ids)

    query = """
    WITH

    iterations AS
    (
        SELECT created_at, iteration_id, oi.order_id, payload, meta_data ->> 'constraint_stats' AS constraint_stats
        FROM courier_dispatcher.order_iterations oi
        WHERE order_id in ({})
    ),

    prospects AS
    (
        SELECT i.order_id, i.iteration_id, cop.courier_id, CASE WHEN com.order_id IS NULL THEN TRUE ELSE FALSE END AS is_free
        FROM courier_dispatcher.courier_order_prospects cop
            JOIN iterations i USING(iteration_id, order_id)
            LEFT JOIN courier_dispatcher.courier_order_matches com ON cop.courier_id = com.courier_id AND cop.iteration_id = com.iteration_id AND cop.order_id != com.order_id
    ),

    prospects_info AS
    (
        SELECT
            p.iteration_id,
            p.order_id,
            COUNT(*) AS total_prospects,
            COUNT(*) FILTER(WHERE p.is_free) AS available_prospects
        FROM prospects p
        GROUP BY 1,2
    ),

    iteration_info AS
    (
        SELECT
            i.created_at, i.iteration_id, i.order_id,
            CASE WHEN pi.order_id IS NOT NULL THEN total_prospects ELSE 0 END AS total_prospects,
            CASE WHEN pi.order_id IS NOT NULL THEN available_prospects ELSE 0 END AS available_prospects,
            com.courier_id AS notified_courier,
            com.taken_at,
            i.payload,
            i.constraint_stats
        FROM iterations i
            LEFT JOIN prospects_info pi ON i.iteration_id = pi.iteration_id AND i.order_id = pi.order_id
            LEFT JOIN courier_dispatcher.courier_order_matches com ON i.iteration_id = com.iteration_id AND i.order_id = com.order_id
        ORDER BY i.order_id, i.created_at DESC
    )

    SELECT * FROM iteration_info
    """.format(order_ids)

    results = redash.run_query(db_id, query)
    return results 

def get_suspect_orders(db_id, orders, iteration_info):
    order_ids = orders['order_id'].unique().tolist()
    if iteration_info.shape[0] > 0:
        iterated_ids = iteration_info['order_id'].unique().tolist()
        suspect_orders = [order_id for order_id in order_ids if order_id not in iterated_ids]
    elif iteration_info.shape[0] == 0:
        suspect_orders = order_ids

    suspect_orders = ["{}".format(order_id) for order_id in suspect_orders]
    suspect_orders = ','.join(suspect_orders)

    query = """
    with base as ( 
        select
            order_id,
            min(created_at) as first_date,
            max(created_at) as last_date
        from order_modifications
        where order_id in ({suspect_orders})
        group by 1
    ),

    base2 as (
        select
            order_id,
            extract(minute from last_date - first_date) as time_elapsed
        from base
    )

    select 
        o.id as order_id,
        os.store_id as store_id,
        os.store_type_group as store_type_group
    from 
    orders o join order_stores os on o.id = os.order_id
    join base2 b on o.id = b.order_id
    join delivery_order dor on o.id = dor.order_id
    where o.id in ({suspect_orders})
        and o.state not in ('finished', 'pending_review')
        and o.place_at is null
        and os.is_marketplace is False
        and b.time_elapsed >= 10
        and dor.delivery_method <> 'national_delivery'
        and os.type <> 'viajes'
    """.format(suspect_orders=suspect_orders)

    results = redash.run_query(db_id, query)
    return results

# countries = ['ar', 'cl', 'mx', 'cr', 'uy', 'ec', 'pe', 'co', 'br']
countries = ['ar', 'cr', 'uy', 'co', 'br']

for country in countries:
    try:
        db_ids = get_db_ids(country)

        orders = get_cancel_orders(db_ids[0])
        if len(orders) == 0:
            print('Rodou: {}'.format(country))
            continue
        orders = get_order_modifications(db_ids[0], orders)
        if orders.shape[0] == 0:
            print('Rodou: {}'.format(country))
            continue
        orders = filter_orders(orders)
        if orders.shape[0] == 0:
            print('Rodou: {}'.format(country))
            continue
        iteration_info = get_iteration_info(db_ids[1], orders)

        suspect_orders = get_suspect_orders(db_ids[0], orders, iteration_info)
    except:
        print('Erro na hora de rodar as queries de {}, possivelmente delay na base'.format(country))
        continue
    
    try:
        suspect_orders_file = 'orders_sin_interaccion_{}.csv'.format(country)
        suspect_orders.to_csv(suspect_orders_file, index=False)

        trigger_text = """
        :alert: *Alarma de Ordenes sin Interaccion* - {country_upper} :flag-{country}: :alert:

        <!subteam^S018ASNLN5T>

        La seguinte relacción de las *{num_orders}* órdenes canceladas por usuario en los ultimos 30 minutos presentan-se sin interaccion del algoritmo.
        """.format(country_upper=country.upper(), country=country, num_orders=suspect_orders.shape[0])

        if suspect_orders.shape[0] > 0:
            if country == 'co':
                slack.file_upload_channel(channel='C01C1LSQREW', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'br':
                slack.file_upload_channel(channel='C01672B528Y', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'ar':
                slack.file_upload_channel(channel='C01EEPH2ECU', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'cl':
                slack.file_upload_channel(channel='C01D5GZ9A4X', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'mx':
                slack.file_upload_channel(channel='C01GMHJRDM2', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'cr':
                slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'uy':
                slack.file_upload_channel(channel='C01EEPH2ECU', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'ec':
                slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')
            elif country == 'pe':
                slack.file_upload_channel(channel='C01E5G49L1K', text=trigger_text, filename=suspect_orders_file, filetype='csv')

        if os.path.exists(suspect_orders_file):
            os.remove(suspect_orders_file)

        print('Rodou: {}'.format(country))
    except:
        pass