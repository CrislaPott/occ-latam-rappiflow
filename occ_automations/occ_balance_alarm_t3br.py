import os, sys, json, pytz, requests
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import thread_listener as tl
from lib import snowflake as snow
from datetime import datetime, timedelta

def get_last_alarms():
    try:
        latest = datetime.now()
        oldest = latest - timedelta(minutes=30)
        channel_name = 'C01672B528Y'

        channel_history = tl.get_channel_history(
            channel = channel_name,
            messages_per_page = 500,
            max_messages = 20000,
            oldest = oldest,
            latest = latest
        )

        last_alarms = channel_history[
            channel_history['text'].str.contains('Alerta de Balance - Cidades T3 - BR')
        ]

        last_alarms = last_alarms[['ts', 'text']]
        last_alarms['zone'] = last_alarms['text'].str.extract(r"(?<=\*Zona\*:)(.*)")
        last_alarms['zone'] = last_alarms['zone'].str.strip() 

        return last_alarms['zone'].unique().tolist()
    except:
        return []

metrics = requests.get("http://services.rappi.com.br/api/al-api/zones/saturation")
metrics = metrics.json()
metrics = pd.DataFrame(metrics['zones'])

query_zones = """
select
    z.city_id as city_id,
    c.city as city_name,
    z.id as zone_id,
    z.name as zone_name
from br_pglr_ms_ops_zones_public.zones z 
join br_grability_public.city_addresses c on z.city_id = c.id
group by 1, 2, 3, 4
order by 1 asc, 3 asc
"""

zones = snow.run_query(query_zones)
zones = zones[zones['city_name'].isin([
    'Aracaju', 'Balneario Camboriu', 'Campo Grande', 'João Pessoa', 'Juiz de Fora', 'Jundiaí', 'Londrina',
    'Maceió', 'Manaus', 'Santos', 'Sorocaba', 'São José do Rio Preto', 'São José dos Campos', 'Teresina',
    'Uberlandia', 'Vila Velha', 'Vitória', 'Belém', 'Caxias do Sul'
])]

cities = zones['city_id'].unique().tolist()
cities = ["{}".format(city) for city in cities]
cities = ','.join(cities)

query_metrics = """
--no_cache
with base as (
    select
        zone_id,
        unassigned_orders,
        grability_city_id as city_id,
        rank() over (partition by zone_id order by created_at desc) as date_rank
    from zone_historic
    where
        grability_city_id in ({})
        and created_at >= now() - interval '10 minute'
)

select
    zone_id,
    city_id,
    unassigned_orders
from base
where date_rank = 1
""".format(cities)

unn = redash.run_query(2646, query_metrics)

try:
    metrics = metrics.merge(unn, on=['city_id', 'zone_id'])
    metrics = metrics.drop_duplicates(subset=['zone_id', 'zone_name'])

    metrics['unn_perc'] = (metrics['unassigned_orders'] / metrics['total_orders']) * 100.0
    metrics['unn_perc'] = metrics['unn_perc'].fillna(0)
except:
    print('Sem resultados de UNN vindos da ba-saturations, possivelmente delay')
    sys.exit(0)

query_ev_logs = """
select *
from br_writable.occ_grappi_logs_eventualities_br
where
    created_at_utc >= dateadd(minute, -30, current_timestamp())
    and raining_duration <> -1
order by created_at_utc desc
"""
ev_logs = snow.run_query(query_ev_logs)
ev_logs.rename(columns={'zone': 'zone_name'}, inplace=True)

query_pay_logs = """
select *
from br_writable.occ_grappi_logs_payment_methods_br
where
    created_at_utc >= dateadd(minute, -30, current_timestamp())
order by created_at_utc desc
"""
pay_logs = snow.run_query(query_pay_logs)
pay_logs.rename(columns={'zone': 'zone_name'}, inplace=True)

def get_events(df, event, zone_name):
    try:
        df = df[
            (df['zone_name'] == zone_name) &
            (df[event].notna()) &
            (df[event] != 'none')
        ]
        events = df[event].unique().tolist()
        events = sorted(events)
        if len(events) == 0:
            return 'nenhum'
        return events
    except:
        return 'nenhum'

metrics['event_actions'] = metrics['zone_name'].apply(lambda x: get_events(ev_logs, 'event', x)) 
metrics['rain_actions'] = metrics['zone_name'].apply(lambda x: get_events(ev_logs, 'raining', x)) 
metrics['payment_actions'] = metrics['zone_name'].apply(lambda x: get_events(pay_logs, 'payment_method', x)) 

metrics.sort_values(by='unn_perc', ascending=False, inplace=True)

last_zones = get_last_alarms()

for index, row in metrics.iterrows():
    zone_name = row['zone_name']

    orders = row['total_orders']
    rts = row['total_couriers']
    unn_orders = row['unassigned_orders']

    saturation = row['coefficient']
    unn = round(row['unn_perc'], 2)
    
    event_actions = row['event_actions']
    rain_actions = row['rain_actions']
    payment_actions = row['payment_actions']

    if ((saturation >= 1) and (unn >= 30)) and (unn_orders > 2) and ('default' not in zone_name) and (zone_name not in last_zones):
        trigger_text = """
        :alert: *Alerta de Balance - Cidades T3 - BR* :flag-br: :alert:

        *FYI*: @occ_monit-balance

        *Zona*: {zone_name}

        *Ordens*: {orders}
        *RTs*: {rts}
        *Ordens Unassigned*: {unn_orders}

        *Saturação*: {saturation}
        *Unassigned Percentual*: {unn}

        *Eventos Aplicados - Últimos 30 minutos*:
        {event_actions}

        *Eventos de Chuva Aplicados - Últimos 30 minutos*:
        {rain_actions}

        *Métodos de Pagamento Fechados - Últimos 30 minutos*:
        {payment_actions}
        """.format(
            zone_name=zone_name,
            orders=orders,
            rts=rts,
            unn_orders=unn_orders,
            saturation=saturation,
            unn=unn,
            event_actions=event_actions,
            rain_actions=rain_actions,
            payment_actions=payment_actions
        )

        slack.send_message_names_channel('C01672B528Y', trigger_text)