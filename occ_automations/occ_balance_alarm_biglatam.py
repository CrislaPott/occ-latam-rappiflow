import os, sys, json, pytz, requests
import numpy as np
import pandas as pd
from lib import redash, slack
from lib import thread_listener as tl
from lib import snowflake as snow
from datetime import datetime, timedelta

def get_db_interval(country):
    query = """
    select
        max(created_at) as last_refresh 
    from zone_historic
    """

    if country == 'co':
        last_refresh = redash.run_query(2645, query)
    elif country == 'ar':
        last_refresh = redash.run_query(2633, query)
    elif country == 'cl':
        last_refresh = redash.run_query(2627, query)
    elif country == 'mx':
        last_refresh = redash.run_query(2658, query)
    elif country == 'pe':
        last_refresh = redash.run_query(2626, query)
    elif country == 'uy':
        last_refresh = redash.run_query(2628, query)

    for column in last_refresh.columns:
        last_refresh[column] = pd.to_datetime(last_refresh['last_refresh'])

    time_diff = datetime.now() - last_refresh.loc[0, 'last_refresh']

    return time_diff.total_seconds() / 60

def get_last_alarms():
    try:
        latest = datetime.now()
        oldest = latest - timedelta(minutes=30)
        channel_name = 'C01672B528Y'

        channel_history = tl.get_channel_history(
            channel = channel_name,
            messages_per_page = 500,
            max_messages = 20000,
            oldest = oldest,
            latest = latest
        )

        last_alarms = channel_history[
            channel_history['text'].str.contains('Alerta de Balance Cidades LATAM')
        ]

        last_alarms = last_alarms[['ts', 'text']]
        last_alarms['city'] = last_alarms['text'].str.extract(r"(?<=\*Cidade\*:)(.*)")
        last_alarms['city'] = last_alarms['city'].str.strip() 

        return last_alarms['city'].unique().tolist()
    except:
        return []

def get_zones(country):
    if country == 'co':
        query_zones = """
        select
            z.city_id as city_id,
            c.city as city_name,
            z.id as zone_id,
            z.name as zone_name
        from {country}_pglr_ms_ops_zones_public.zones z 
        join {country}_grability_public.city_addresses_vw c on z.city_id = c.id
        group by 1, 2, 3, 4
        order by 1 asc, 3 asc
        """.format(country=country)
    else:
        query_zones = """
        select
            z.city_id as city_id,
            c.city as city_name,
            z.id as zone_id,
            z.name as zone_name
        from {country}_pglr_ms_ops_zones_public.zones z 
        join {country}_grability_public.city_addresses c on z.city_id = c.id
        group by 1, 2, 3, 4
        order by 1 asc, 3 asc
        """.format(country=country)
        
    zones = snow.run_query(query_zones)
    zones = filter_zones(country, zones)
    metrics = get_metrics(country, zones)

    del zones['zone_name']
    
    zones = zones.merge(metrics, on=['city_id', 'zone_id'])
    
    return zones

def filter_zones(country, zones):
    if country == 'co':
        mask = zones['city_name'].isin(['Bogotá', 'Barranquilla', 'Cali', 'Medellín', 'Cartagena'])
    elif country == 'ar':
        mask = zones['city_name'].isin(['Buenos Aires'])
    elif country == 'cl':
        mask = zones['city_name'].isin(['Santiago de Chile', 'Viña del Mar'])
    elif country == 'mx':
        mask = zones['city_name'].isin(['Ciudad de México', 'Monterrey', 'Guadalajara'])
    elif country == 'pe':
        mask = zones['city_name'].isin(['Lima'])
    elif country == 'uy':
        mask = zones['city_name'].isin(['Montevideo'])
    
    return zones[mask]

def get_metrics(country, zones):
    cities = zones['city_id'].unique().tolist()
    cities = ["{}".format(city) for city in cities]
    cities = ','.join(cities)
    
    query_metrics = """
    --no_cache
    with base as (
        select
            zone_id,
            unassigned_orders,
            grability_city_id as city_id,
            rank() over (partition by zone_id order by created_at desc) as date_rank
        from zone_historic
        where
            grability_city_id in ({})
            and created_at >= now() - interval '10 minute'
    )

    select
        zone_id,
        city_id,
        unassigned_orders
    from base
    where date_rank = 1
    """.format(cities)
    
    if country == 'co':
        metrics = requests.get("http://services.rappi.com/api/al-api/zones/saturation")
        metrics = metrics.json()
        metrics = pd.DataFrame(metrics['zones'])

        unn = redash.run_query(2645, query_metrics)
    elif country == 'ar':
        metrics = requests.get("http://services.rappi.com.ar/api/al-api/zones/saturation")
        metrics = metrics.json()
        metrics = pd.DataFrame(metrics['zones'])

        unn = redash.run_query(2633, query_metrics)
    elif country == 'cl':
        metrics = requests.get("http://services.rappi.cl/api/al-api/zones/saturation")
        metrics = metrics.json()
        metrics = pd.DataFrame(metrics['zones'])

        unn = redash.run_query(2627, query_metrics)
    elif country == 'pe':
        metrics = requests.get("https://services.rappi.pe/api/al-api/zones/saturation")
        metrics = metrics.json()
        metrics = pd.DataFrame(metrics['zones'])

        unn = redash.run_query(2626, query_metrics)
    elif country == 'uy':
        metrics = requests.get("https://services.rappi.com.uy/api/al-api/zones/saturation")
        metrics = metrics.json()
        metrics = pd.DataFrame(metrics['zones'])

        unn = redash.run_query(2628, query_metrics)
    
    unn = unn.drop_duplicates(subset='zone_id')

    metrics = metrics.merge(unn, on=['city_id', 'zone_id'])
    metrics = metrics.drop_duplicates(subset=['zone_id', 'zone_name'])

    metrics['unn_perc'] = (metrics['unassigned_orders'] / metrics['total_orders']) * 100.0
    metrics['unn_perc'] = metrics['unn_perc'].fillna(0)

    return metrics

def summarize_cities(zones):
    cities = zones.groupby(
        ['city_id', 'city_name']
    ).agg(
        orders=('total_orders', 'sum'),
        unassigned_orders=('unassigned_orders', 'sum'),
        couriers=('total_couriers', 'sum')
    ).reset_index()
    
    cities['unn_perc'] = round((cities['unassigned_orders'] / cities['orders']) * 100, 1)
    cities['saturation'] = round(cities['orders'] / cities['couriers'], 1)
    
    return cities

def run_alarm(country):
    try:
        zones = get_zones(country)
    except:
        print('Erro ao buscar infos de UNN da ba-saturations em {}, possivelmente delay da base'.format(country))
        return 0
    cities = summarize_cities(zones)

    for index, row in cities.iterrows():
        city = row['city_name']
        city_id = row['city_id']
        
        orders = row['orders']
        unn = row['unassigned_orders']
        rts = row['couriers']
        unn_perc = row['unn_perc']
        saturation = row['saturation']

        last_cities = get_last_alarms()

        if (unn_perc > 15 and unn > 5 and city not in last_cities):
            trigger_text = """
            *Alerta de Balance Cidades LATAM*
            
            *FYI*: @occ_monit-balance
            
            *País*: {country} - :flag-{country_icon}:
            
            *Cidade*: {city}
            *Unassigned - %*: {unn_perc}
            *Saturação*: {saturation}
            
            *Ordens*: {orders}
            *Ordens Unassgined*: {unn}
            *RTs*: {rts}
            """.format(
                country=country.upper(), 
                country_icon=country, 
                city=city, 
                unn_perc=unn_perc, 
                saturation=saturation,
                orders=orders,
                unn=unn,
                rts=rts
            )
        
            slack.send_message_names_channel('C01672B528Y', trigger_text)
            
countries = ['ar', 'co', 'cl', 'pe', 'uy']
for country in countries:
    db_delay = get_db_interval(country)
    if db_delay <= 11:
        run_alarm(country)
        print('Rodou: {}'.format(country))
    else:
        print('Delay da base do país {}: {}'.format(country, db_delay))