import os, re, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import thread_listener as tl
from lib import snowflake as snow

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=7)

channel_history = tl.get_channel_history(
    channel = "C01BJUSMSG7",
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

cpg_threads = channel_history[
    (channel_history['username'].str.contains('CPGs')) &
    (channel_history['thread_ts'].notna()) &
    (channel_history['subtype'] == 'bot_message')
].drop_duplicates(subset='thread_ts', keep='first')

cpg_threads = cpg_threads[['thread_ts', 'username']]
cpg_threads.rename(columns={'username': 'thread_name'}, inplace=True)

cpg_threads_messages = pd.DataFrame()
for index, row in cpg_threads.iterrows():
    try:
        thread_messages = tl.get_thread_replies('C01BJUSMSG7', row['thread_ts'])
        cpg_threads_messages = pd.concat([cpg_threads_messages, thread_messages], ignore_index=True)
    except:
        pass 
    
cpg_threads_messages = cpg_threads_messages[['user', 'thread_ts', 'ts', 'text']]
cpg_threads_messages['text'] = cpg_threads_messages['text'].str.lower()
cpg_threads_messages['text'] = cpg_threads_messages['text'].apply(lambda x: unidecode(x))

cpg_threads_messages = cpg_threads_messages[
    cpg_threads_messages['text'].str.contains('pais')
]

cpg_threads_messages = cpg_threads_messages.merge(cpg_threads, on='thread_ts')

print('Quantidade de mensagens nas threads: {}'.format(cpg_threads_messages.shape[0]))

def get_pais(text):
    values = text.split('\n')
    for value in values:
        if 'pais' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None
        
def get_tienda(text):
    values = text.split('\n')
    for value in values:
        if 'tienda' in value:
            match = re.findall(r"(?<=:)(.*)", value)
            try:
                match = int(match[0].strip())
                return match
            except:
                return None
    return None

def get_assigned(text):
    values = text.split('\n')
    for value in values:
        if 'assigned' in value:
            match = re.findall(r"(?<=:)(.*)", value)
            try:
                match = int(match[0].strip())
                return match
            except:
                return None
    return None
            
def get_picking(text):
    values = text.split('\n')
    for value in values:
        if 'picking' in value:
            match = re.findall(r"(?<=:)(.*)", value)
            try:
                match = int(match[0].strip())
                return match
            except:
                return None
    return None

def get_accion(text):
    values = text.split('\n')
    for value in values:
        if 'accion' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None

def get_detalle(text):
    values = text.split('\n')
    for value in values:
        if 'detalle' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None

def get_orden_id(text):
    values = text.split('\n')
    for value in values:
        if 'orden' in value:
            match = re.findall(r"(?<=:)(.*)", value)
            try:
                match = int(match[0].strip())
                return match
            except:
                return None
    return None

def get_hora_local(text):
    values = text.split('\n')
    for value in values:
        if 'hora' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                match2 = re.findall(r"[0-9]{1,2}h", match[0])
                return match2[0].strip()
            except:
                return None
    return None

def get_slot(text):
    values = text.split('\n')
    for value in values:
        if 'slot' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                match2 = re.findall(r"[0-9]{1,2}h", match[0])
                return match2[0].strip()
            except:
                return None
    return None

def get_error(text):
    values = text.split('\n')
    for value in values:
        if 'error' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None

def get_estado(text):
    values = text.split('\n')
    for value in values:
        if 'estado' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None

def get_motivo(text):
    values = text.split('\n')
    for value in values:
        if 'motivo' in value:
            try:
                match = re.findall(r"(?<=:)(.*)", value)
                return match[0].strip()
            except:
                return None
    return None

cpg_threads_messages['pais'] = cpg_threads_messages['text'].apply(lambda x: get_pais(x))
cpg_threads_messages['tienda'] = cpg_threads_messages['text'].apply(lambda x: get_tienda(x))
cpg_threads_messages['estado'] = cpg_threads_messages['text'].apply(lambda x: get_estado(x))
cpg_threads_messages['error'] = cpg_threads_messages['text'].apply(lambda x: get_error(x))
cpg_threads_messages['slot'] = cpg_threads_messages['text'].apply(lambda x: get_slot(x))
cpg_threads_messages['hora_local'] = cpg_threads_messages['text'].apply(lambda x: get_hora_local(x))
cpg_threads_messages['orden_id'] = cpg_threads_messages['text'].apply(lambda x: get_orden_id(x))
cpg_threads_messages['assigned'] = cpg_threads_messages['text'].apply(lambda x: get_assigned(x))
cpg_threads_messages['in_picking'] = cpg_threads_messages['text'].apply(lambda x: get_picking(x))
cpg_threads_messages['detalle'] = cpg_threads_messages['text'].apply(lambda x: get_detalle(x))
cpg_threads_messages['accion'] = cpg_threads_messages['text'].apply(lambda x: get_accion(x))
cpg_threads_messages['motivo'] = cpg_threads_messages['text'].apply(lambda x: get_motivo(x))

users = pd.read_csv('input/slack_users.csv')
users.columns = ['user', 'analyst_name']

cpg_threads_messages = cpg_threads_messages.merge(users, on='user')

print(cpg_threads_messages.isna().sum())

print(cpg_threads_messages.head())

snow.upload_df(cpg_threads_messages, 'occ_monit_latam_cpgs')