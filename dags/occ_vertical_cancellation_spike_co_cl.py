from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta
from airflow.models import Variable,TaskInstance
from lib import slack

def failure_message(context):
    dag = context['dag']
    task_instance = context['task_instance']
    exception = context['exception']
    log_url=context.get('task_instance').log_url

    message = '''
    *FAIL!!!* :rotating_light::rotating_light::rotating_light: 
    <@rafaela.lorenzini>
    *DAG Name*: {dag}
    *Task Name*: {task_instance}
    *Exception*: {exception}
    *Log url*: {log_url}'''.format(dag = str(dag),  task_instance = str(task_instance), exception = str(exception), log_url=log_url)

    slack.bot_slack(message,'C02LC89SNRX')

default_args = {
    'owner': 'alejandra.gomez',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'on_failure_callback': failure_message,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('occ_vertical_cancellation_spike_co_cl',
    start_date=datetime(2021, 10, 15),
    concurrency=30,
    max_active_runs=1,
    schedule_interval=timedelta(minutes=30),
    default_args=default_args,
    catchup=False
) as dag:    

    t0 = BashOperator(
        task_id='occ_vertical_cancellation_spike_co_cl',
        execution_timeout=timedelta(minutes=12), 
        bash_command='cd /usr/local/airflow/occ2 && python vertical_cancellation_spike_co_cl.py'
    )

    [t0]