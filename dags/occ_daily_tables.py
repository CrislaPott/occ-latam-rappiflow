from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta

default_args = {
    'owner': 'leandro.benasse',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('daily_occ_tables',
    start_date=datetime(2021, 2, 23),
    concurrency=30,
    max_active_runs=1,
    schedule_interval='0 8,21 * * *',
    default_args=default_args,
    catchup=False
) as dag:    


    t0 = BashOperator(
        task_id='kpi_logs',
        execution_timeout=timedelta(minutes=40),
        bash_command='cd /usr/local/airflow/datamart && python kpi_logs.py'
    )

    t1 = BashOperator(
        task_id='risk_management',
        execution_timeout=timedelta(minutes=40),
        bash_command='cd /usr/local/airflow/datamart && python risk_management.py'
    )

    [t0,t1]