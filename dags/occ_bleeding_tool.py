from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta
from airflow.models import Variable,TaskInstance
from lib import slack

def failure_message(context):
    dag = context['dag']
    task_instance = context['task_instance']
    exception = context['exception']
    log_url=context.get('task_instance').log_url

    message = '''
    *FAIL!!!* :rotating_light::rotating_light::rotating_light: 
    <@rafaela.lorenzini>
    *DAG Name*: {dag}
    *Task Name*: {task_instance}
    *Exception*: {exception}
    *Log url*: {log_url}'''.format(dag = str(dag),  task_instance = str(task_instance), exception = str(exception), log_url=log_url)

    slack.bot_slack(message,'C02LC89SNRX')

default_args = {
    'owner': 'leandro.benasse',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'on_failure_callback': failure_message,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('bleeding_tool',
    start_date=datetime(2021, 2, 23),
    concurrency=30,
    max_active_runs=1,
    schedule_interval='3,13,23,33,45 * * * *',
    default_args=default_args,
    catchup=False
) as dag:

    bleeding = BashOperator(
        task_id='bleeding_tool_offenders.py',
        execution_timeout=timedelta(minutes=20),
        bash_command='cd /usr/local/airflow/bleeding_tool && python bleeding_tool.py'
    )

    bleeding_pr = BashOperator(
        task_id='bleeding_tool_offenders_pr.py',
        execution_timeout=timedelta(minutes=20),
        bash_command='cd /usr/local/airflow/bleeding_tool && python bleeding_tool_pr.py'
    )

    turbo = BashOperator(
        task_id='turbo_cancellation.py',
        execution_timeout=timedelta(minutes=15), 
        bash_command='cd /usr/local/airflow/bleeding_tool && python cancellation_turbo.py'
    )

    pivot = BashOperator(
        task_id='waiting_time_cpgsnow.py',
        execution_timeout=timedelta(minutes=15),
        bash_command='cd /usr/local/airflow/bleeding_tool && python waiting_time_cpgsnow.py'
    )

    #lg_ecommerce = BashOperator(
    #    task_id='ecommerce_stores_offenders.py',
    #    execution_timeout=timedelta(minutes=20),
    #    bash_command='cd /usr/local/airflow/bleeding_tool && python ecommerce_stores_offenders.py'
    #)

    sc_co = BashOperator(
        task_id='storeclosed_co',
        execution_timeout=timedelta(minutes=15), 
        bash_command='cd /usr/local/airflow/occ_storeclosed_bot && python storeclosed_bot_co.py'
    )

    sc_br = BashOperator(
        task_id='storeclosed_br',
        execution_timeout=timedelta(minutes=15), 
        bash_command='cd /usr/local/airflow/occ_storeclosed_bot && python storeclosed_bot_br.py'
    )

    sc_mx = BashOperator(
        task_id='storeclosed_mx',
        execution_timeout=timedelta(minutes=15), 
        bash_command='cd /usr/local/airflow/occ_storeclosed_bot && python storeclosed_bot_mx.py'
    )

    sc_small = BashOperator(
        task_id='storeclosed_small',
        execution_timeout=timedelta(minutes=15), 
        bash_command='cd /usr/local/airflow/occ_storeclosed_bot && python storeclosed_bot_small.py'
    )

    [sc_mx,sc_co,sc_br,sc_small,pivot,bleeding,bleeding_pr]

    ## Dag Dependencies
    sc_co >> bleeding
    sc_br >> bleeding
    sc_mx >> bleeding
    sc_small >> bleeding
    pivot >> bleeding

    sc_co >> bleeding_pr
    sc_br >> bleeding_pr
    sc_mx >> bleeding_pr
    sc_small >> bleeding_pr
    pivot >> bleeding_pr