import json
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta
from airflow.models import Variable,TaskInstance
from lib import slack

def failure_message(context):
    dag = context['dag']
    task_instance = context['task_instance']
    exception = context['exception']
    log_url=context.get('task_instance').log_url

    message = '''
    *FAIL!!!* :rotating_light::rotating_light::rotating_light: 
    <@rafaela.lorenzini>
    *DAG Name*: {dag}
    *Task Name*: {task_instance}
    *Exception*: {exception}
    *Log url*: {log_url}'''.format(dag = str(dag),  task_instance = str(task_instance), exception = str(exception), log_url=log_url)

    slack.bot_slack(message,'C02LC89SNRX')

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(seconds=2),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

with open('/usr/local/airflow/occ_balance_bot/dag_arguments_latam.json') as f:
    arguments = json.load(f)

arg_indexes = list(arguments.keys())

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('occ_balance_bot_latam',
    start_date=datetime(2021, 4, 12),
    concurrency=3,
    max_active_runs=1,
    schedule_interval='7,17,27,37,47,57 * * * *',
    default_args=default_args,
    catchup=False
) as dag:

    for index in arg_indexes:
        task = BashOperator(
            task_id='occ_balance_bot_latam_{}'.format(index),
            execution_timeout=timedelta(minutes=3),
            bash_command='cd /usr/local/airflow/occ_balance_bot && python main.py {country} {playbook_sheet} {slack_channel} {action_mode}'.format(
                country=arguments["{}".format(index)]['country'].lower(),
                playbook_sheet=arguments["{}".format(index)]['playbook_sheet'],
                slack_channel=arguments["{}".format(index)]['slack_channel'],
                action_mode=arguments["{}".format(index)]['action_mode']
            )
        )

        task