from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('occ_alarm_fraud_latam2',
    start_date=datetime(2021, 4, 7),
    concurrency=30,
    max_active_runs=1,
    schedule_interval='0 19 * * *',
    default_args=default_args,
    catchup=False
) as dag:    

    t0 = BashOperator(
        task_id='fraud_latam2',
        execution_timeout=timedelta(minutes=30), 
        bash_command='cd /usr/local/airflow/occ_automations && python occ_alarm_fraud_latam2.py'
    )

    [t0]