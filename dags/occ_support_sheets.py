from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from airflow.version import version
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from datetime import datetime, timedelta

default_args = {
    'owner': 'leandro.benasse',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'retry_delay': timedelta(minutes=5),
    'executor_config': get_k8s_executor_resource_config(resource_request='S', resource_limits='M')
}

# Using a DAG context manager, you don't have to specify the dag property of each task
with DAG('occ_support_sheets',
    start_date=datetime(2021, 2, 23),
    concurrency=30,
    max_active_runs=1,
    schedule_interval=timedelta(minutes=10),
    default_args=default_args,
    catchup=False
) as dag:    

    t0 = BashOperator(
        task_id='occ_support_sheets',
        execution_timeout=timedelta(minutes=9), 
        bash_command='cd /usr/local/airflow/occ2 && python support_gsheets.py'
    )

    t0