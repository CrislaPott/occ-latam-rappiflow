"""Global Project Settings."""
import os

from copy import deepcopy
from datetime import timedelta
from typing import Optional

from dags.utils.db_mappings import get_tables
from rappiflow.utils.k8s_settings import get_k8s_executor_resource_config
from rappiflow.utils.opsgenie_alerts import get_ops_genie_failure_hook

# Airflow Connections
AMPLITUDE_API_CONN_ID = 'amplitude_export_api'
OPEN_EXCHANGE_RATES_CONN_ID = 'openexchangerates_dotcom_api'
FIVETRAN_CONECTORS_API_CONN_ID = 'fivetran_connectors_api'
OPSGENIE_CONN_ID = 'opsgenie_alerting'

# General Settings
RAPPI_DOCKER_REGISTRY = os.getenv('RAPPI_DOCKER_REGISTRY', '')
RAPPI_DOCKER_REGISTRY_SECRET = os.getenv('RAPPI_DOCKER_REGISTRY_SECRET_NAME')
AIRFLOW_DNS = os.getenv('AIRFLOW_BASE_DOMAIN', '')
AIRFLOW_INCLUDE_DIR = '/usr/local/airflow/include'
OPSGENIE_TEAM_ID = os.getenv('OPSGENIE_TEAM_ID', '')

# Environment aware Settings
S3_BUCKET = 'bucket-dev'

IS_PRODUCTION = os.getenv('airflow_environment') or os.getenv('AIRFLOW_ENVIRONMENT') == 'production'

if IS_PRODUCTION:
    S3_BUCKET = 'bucket-live'

# DAG default args
default_args = {
    'owner': '',
    'email': [''],
    'email_on_failure': False,  # Alerting handled with OpsGenie
    'email_on_retry': False,  # Alerting handled with OpsGenie
    'on_retry_callback': False,
    'on_failure_callback': get_ops_genie_failure_hook(
        conn_id=OPSGENIE_CONN_ID,
        team_name=OPSGENIE_TEAM_ID,
        tags=['P1-tag'],
        airflow_dns=AIRFLOW_DNS,
        priority='P1',
        dry_run=not IS_PRODUCTION
    ),
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
    'executor_config': get_k8s_executor_resource_config()
}


def get_default_args(owner: Optional[str] = None, priority_fail: str = "P3"):
    """Configure custom default args.

    :param owner The email of the person that requested the dag, and will recieve the alerts.
    :param priority_fail P1, P2, P3, P4 the priority of the alarm when a task fails. P1 is the highest.
    :param priority_retry P1, P2, P3, P4 the priority of the alarm when a task is retried. P1 is the highest.
    """
    if owner is not None:
        default_args['email'] = owner

    default_args['on_failure_callback'] = get_ops_genie_failure_hook(
        conn_id=OPSGENIE_CONN_ID,
        team_name=OPSGENIE_TEAM_ID,
        tags=[priority_fail],
        airflow_dns=AIRFLOW_DNS,
        priority=priority_fail,
        dry_run=not IS_PRODUCTION,
        owner=owner
    )

    return deepcopy(default_args)


def get_default_db_aliases() -> dict:
    """Return default DB Aliases."""
    aliases = {
        'sf_tables': get_tables()
    }
    return deepcopy(aliases)
