"""Custom User Defined Macros."""
from airflow import AirflowException
from typing import Optional
from dags.utils.db_mappings import get_tables


def resolve_table_name(table_name: str, country_code: Optional[str] = None) -> str:
    """Resolve the table name  by country."""
    country_code = country_code.lower() if country_code else 'all'
    tables = get_tables()

    try:
        return tables.get(country_code, {})[table_name]
    except KeyError as ex:
        raise AirflowException(
            'Failed to fetch the table name {} in country {}'.format(table_name, country_code)
        ) from ex
