"""Example Failing Passing Operator."""
import airflow
from airflow import DAG
from datetime import timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator

from rappiflow.utils.k8s_settings import get_k8s_namespace

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': airflow.utils.dates.days_ago(2),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(
    'zzz_example_failing_passing_k8s_executor', default_args=default_args, schedule_interval=None)

passing = KubernetesPodOperator(namespace=get_k8s_namespace(),
                                image="python:3.6",
                                cmds=["python", "-c"],
                                arguments=["print('hello world')"],
                                labels={"foo": "bar"},
                                name="passing-test",
                                task_id="passing-task",
                                get_logs=True,
                                in_cluster=True,
                                dag=dag
                                )

failing = KubernetesPodOperator(namespace=get_k8s_namespace(),
                                image="ubuntu:19.04",
                                cmds=["python", "-c"],
                                arguments=["print('hello world')"],
                                labels={"foo": "bar"},
                                name="fail",
                                task_id="failing-task",
                                get_logs=True,
                                in_cluster=True,
                                dag=dag
                                )

failing.set_upstream(passing)
