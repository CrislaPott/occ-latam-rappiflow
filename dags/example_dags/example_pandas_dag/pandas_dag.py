"""DAG to process with pandas.

Connections:
* Connection Type S3 with the name `s3_cpgs_datascience`.
"""
import os

from datetime import datetime, timedelta

from airflow.models import DAG
from airflow.utils.trigger_rule import TriggerRule
from rappiflow.operators import CreateTempFolderOperator, RemoveLocalFolder, UploadFileToS3

from dags.example_dags.example_pandas_dag.operators.pandas_operator import PandasOperator


default_args = {
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

s3_settings = {
    's3_destination_bucket': 'cpgs-datascience-dev',
    'aws_conn_id': 's3_cpgs_datascience'
}

with DAG(
        'zzz_example_pandas_dag',
        default_args=default_args,
        schedule_interval=None,
        start_date=datetime(2019, 1, 1),
        catchup=False
) as dag:

    # Folders
    create_temporary_folder = CreateTempFolderOperator(
        task_id='create_temporary_folder',
        prefix='zzz_example_pandas_dag_'
    )

    # Files and templates
    output_csv_s3 = 'example/{{ params.country_code }}/pandas_example/' \
        + '{{ ds }}/result_{{ params.country_code }}_{{ ds_nodash }}.csv'

    temp_folder = "{{ task_instance.xcom_pull('create_temporary_folder', key='return_value') }}"
    file_name = "pandas_result_{{ params.country_code }}_{{ ds_nodash }}.csv"
    output_csv_local = os.path.join(temp_folder, file_name)

    delete_temporary_folder = RemoveLocalFolder(
        task_id='remove_temporary_folder',
        folder_path=temp_folder,
        trigger_rule=TriggerRule.NONE_FAILED
    )

    # Reducers Definition

    process_with_pandas = PandasOperator(
        task_id='br_cpgs_process_with_pandas',
        output_file_path=output_csv_local
    )

    load_status_to_s3 = UploadFileToS3(
        task_id='br_cpgs_load_status_to_s3',
        local_file=output_csv_local,
        s3_destination_key=output_csv_s3,
        s3_destination_bucket=s3_settings['s3_destination_bucket'],
        aws_conn_id=s3_settings['aws_conn_id']
    )

    dag \
        >> create_temporary_folder \
        >> process_with_pandas \
        >> load_status_to_s3 \
        >> delete_temporary_folder
