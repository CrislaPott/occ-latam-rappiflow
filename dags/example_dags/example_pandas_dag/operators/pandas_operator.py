"""Operators to process with pandas."""

import pandas as pd
from random import choice, randint

from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults

from rappiflow.utils.decorators import profile


class PandasOperator(BaseOperator):
    """Operator to process with pandas."""

    template_fields = ('output_file_path',)
    template_ext = ()
    ui_color = '#0fafff'

    @apply_defaults
    def __init__(self, output_file_path, *args, **kwargs):
        """Class initializer."""
        super().__init__(*args, **kwargs)
        self.output_file_path = output_file_path

    @profile
    def execute(self, context):
        """Process data with pandas and write result in local csv."""
        df = self.process_with_pandas()
        self.write_dataframe_to_local_csv(df)

    def process_with_pandas(self):
        """Apply pandas operations."""
        # Let's create a df example
        count = [randint(20, 100) for _ in range(16)]
        targets = ['A', 'B', 'C', 'D', 'E', 'F']
        labels = [choice(targets) for _ in range(16)]
        df = pd.DataFrame({'targets': labels, 'data': count})

        res = df.groupby(['targets'])['data'].describe()
        res = res.reset_index()

        return res

    def write_dataframe_to_local_csv(self, df):
        """Write a dataframe to a CSV on local folder."""
        self.log.info("Writing {} records to {}".format(len(df),
                                                        self.output_file_path))
        df.to_csv(self.output_file_path, index=False)
