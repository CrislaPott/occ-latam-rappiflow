"""
# Snowflake Example DAG.

SUMMARY:
--------

* DAG Name:
    zzz_example_snowflake_dag.
* Owner:
    Rappiflow Admin Team
* Description:
    This is an example DAG intended to serve as an example of how to use:
        - MultiStatementSnowflakeOperator
        - SQLPartitionSensor


CONNECTIONS:
------------

* Connection Type Snowflake with the name `cpgs_datascience_write`.


DISCLAIMER:
-----------

This is just an **EXAMPLE**, you may not have the correct credentials to run this DAG succesfully,
nevertheless you could use part of this for the development of your own DAGs

"""
from datetime import datetime, timedelta

from airflow import DAG
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.bash_operator import BashOperator

from rappiflow.sensors import SQLPartitionSensor
from rappiflow.operators import MultiStatementSnowflakeOperator


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=10),
}

settings = {
    'snowflake_conn_id': 'cpgs_datascience_write',  # Modify this connection id for a Snowflake connection of your own
}

dag = DAG('zzz_example_snowflake_dag',
          max_active_runs=1,
          schedule_interval=None,
          start_date=datetime(2019, 9, 18),
          default_args=default_args)

test_table_name = 'cpgs_datascience_dev.sql_partition_sensor_test_table'  # Change this schema/table for one of your own

create_test_data_sql = ("""
   BEGIN;

   CREATE OR REPLACE TABLE %s (
        dt DATE
        ,country_code VARCHAR(10)
        ,product_id NUMBER(38,0) NOT NULL UNIQUE PRIMARY KEY
    );

   INSERT INTO cpgs_datascience_dev.sql_partition_sensor_test_table (dt,country_code,product_id)
   VALUES  ('2019-09-17', 'AR', 1234),
           ('2019-09-17', 'AR', 1215),
           ('2019-09-17', 'AR', 1456),
           ('2019-09-17', 'CO', 1234),
           ('2019-09-17', 'CO', 1215),
           ('2019-09-17', 'BR', 1234),
           ('2019-09-18', 'UY', 1215),
           ('2019-09-18', 'UY', 1456),
           ('2019-09-18', 'CO', 1215),
           ('2019-09-18', 'PE', 1234);

    COMMIT;
""" % test_table_name)

create_test_data = MultiStatementSnowflakeOperator(
    task_id='create_test_data',
    sql=create_test_data_sql,
    snowflake_conn_id=settings['snowflake_conn_id'],
)

passing_sensor = SQLPartitionSensor(
    task_id='passing_sensor',
    table_name=test_table_name,
    filters={'dt': datetime(2019, 9, 17), 'country_code': 'AR'},
    snowflake_conn_id=settings['snowflake_conn_id'],
    mode='reschedule',
    poke_interval=10,
    timeout=30,
)

failing_sensor = SQLPartitionSensor(
    task_id='failing_sensor',
    table_name=test_table_name,
    filters={'dt': datetime(2019, 9, 18), 'country_code': 'AR'},
    snowflake_conn_id=settings['snowflake_conn_id'],
    mode='reschedule',
    poke_interval=10,
    timeout=30,
)

t1 = BashOperator(
    task_id='print_date1',
    bash_command='sleep $[ ( $RANDOM % 3 )  + 1 ]s',
    dag=dag)

t2 = BashOperator(
    task_id='print_date2',
    bash_command='sleep $[ ( $RANDOM % 3 )  + 1 ]s',
    dag=dag)

t3 = BashOperator(
    task_id='print_date3',
    bash_command='sleep $[ ( $RANDOM % 3 )  + 1 ]s',
    dag=dag)

clean_test_data_sql = "DROP TABLE cpgs_datascience_dev.sql_partition_sensor_test_table;"

clean_test_data = MultiStatementSnowflakeOperator(
    task_id='clean_test_data',
    sql=clean_test_data_sql,
    snowflake_conn_id=settings['snowflake_conn_id'],
    trigger_rule=TriggerRule.ALL_DONE,
)

dag >> create_test_data >> passing_sensor >> t1 >> t2 >> failing_sensor >> t3 >> clean_test_data
