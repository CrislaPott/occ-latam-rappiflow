BEGIN;

-- Common pattern to clean old data before inserting new rows.
DELETE FROM {{ get_table_name('target_table') }}
WHERE 1=1
    AND dt = '{{ ds }}';

-- DT and COUNTRY_CODE are standard columns.
INSERT INTO {{ get_table_name('target_table') }}
SELECT 
    '{{ ds }}' dt,
    country country_code,
    some_field,
    TRY_PARSE_JSON(variant_field) AS parsed_variant_field,
    parsed_variant_field:field_name::string cast_field
FROM {{ get_table_name('source_table') }}
WHERE 1=1 
    -- AND _DATE::date = '{{ ds }}'
    AND country IN ('{{ params.country_codes | join("', '") }}')
    AND some_field IS NOT NULL AND some_field != 'null';

COMMIT;