"""
# Example DAG with SQL query.

SUMMARY:
--------

* DAG Name:
    zzz_example_sql_dag
* Owner:
    ignacio.gonzalez@rappi.com
* Tech owner:
    ignacio.gonzalez@rappi.com
* Description:
    Select columns from `source_table` and insert the results in `target_table`.
* Output:
    Snowflake table updated (target_table).

CONNECTIONS:
------------

* Connection Type Snowflake with the name `cpgs_datascience_write`.
"""
import os
from datetime import datetime, timedelta
from airflow.models import DAG
from rappiflow.operators import MultiStatementSnowflakeOperator
from dags import settings


dev_schema = 'cpgs_datascience_dev'
source_table_name = '%s.example_sql_dag_source_table' % dev_schema
target_table_name = '%s.example_sql_dag_target_table' % dev_schema


def resolve_table_name(table_name):
    """In a real DAG you must use the function imported from dags.utils.user_defined_macros."""
    return {
        'source_table': source_table_name,
        'target_table': target_table_name
    }[table_name]


create_example_tables_sql = """
    BEGIN;

    create or replace table %s (
        COUNTRY VARCHAR(16777216),
        _DATE TIMESTAMP_NTZ(9),
        VARIANT_FIELD VARIANT,
        SOME_FIELD VARCHAR(16777216)
    );

    create or replace table %s (
        DT DATE,
        COUNTRY_CODE VARCHAR(16777216),
        SOME_FIELD VARCHAR(16777216),
        PARSED_VARIANT_FIELD VARIANT,
        CAST_FIELD VARCHAR(16777216)
    );

    COMMIT;
""" % (source_table_name, target_table_name)

insert_example_data_sql = """
    BEGIN;

    INSERT INTO {source_table} (COUNTRY, _DATE, VARIANT_FIELD, SOME_FIELD)
        SELECT 'AR', '2019-01-01', NULL, 123;
    INSERT INTO {source_table} (COUNTRY, _DATE, VARIANT_FIELD, SOME_FIELD)
        SELECT 'BR', '2019-01-01', parse_json('{{"field_name": "value"}}'), 123;
    INSERT INTO {source_table} (COUNTRY, _DATE, VARIANT_FIELD, SOME_FIELD)
        SELECT 'CO', '2019-01-01', parse_json('{{"unmatched_field": "value"}}'), 123;

    COMMIT;
""".format(source_table=source_table_name)

drop_example_tables_sql = """
    drop table %s;
    drop table %s;
""" % (source_table_name, target_table_name)

# In a real use case default_operators_args must be initialized with settings.get_default_args()
default_operators_args = {
    'owner': 'airflow',
    'email_on_failure': False,
    'email_on_retry': False,
    'depends_on_past': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

# In general we initialize the DAG with params = settings.get_default_db_aliases() to access the settings in all tasks.
# Then we can use for example `params.sf_staging_areas` in a templated query. In this example we mock this setting.
default_operators_params = {}

queries_base_path = os.path.join(os.path.dirname(__file__), 'queries')

with DAG(
    'zzz_example_sql_dag',
    default_args=default_operators_args,
    params=default_operators_params,
    start_date=datetime(2019, 1, 1),
    schedule_interval=None,
    max_active_runs=1,
    catchup=True,
    template_searchpath=queries_base_path,
    user_defined_macros={'get_table_name': resolve_table_name}
) as dag:

    # In the (templated) example.sql query there's a common use case with this param.
    params = {
        'country_codes': ['AR', 'BR', 'CL', 'CO', 'MX', 'PE', 'UY'],
    }

    # In a real DAG the tables must be created separately, not in a task, and documented in the Data Catalog.
    create_example_tables = MultiStatementSnowflakeOperator(
        task_id='create_example_tables',
        sql=create_example_tables_sql,
        snowflake_conn_id=settings.SNOWFLAKE_CPGS_DS_WRITE_CONN_ID,
    )

    insert_example_data = MultiStatementSnowflakeOperator(
        task_id='insert_example_data',
        sql=insert_example_data_sql,
        snowflake_conn_id=settings.SNOWFLAKE_CPGS_DS_WRITE_CONN_ID,
    )

    sql_query = 'example.sql'
    run_example_query = MultiStatementSnowflakeOperator(
        task_id='run_example_query',
        sql=sql_query,
        snowflake_conn_id=settings.SNOWFLAKE_CPGS_DS_WRITE_CONN_ID,
        params=params
    )

    drop_example_tables = MultiStatementSnowflakeOperator(
        task_id='drop_example_tables',
        sql=drop_example_tables_sql,
        snowflake_conn_id=settings.SNOWFLAKE_CPGS_DS_WRITE_CONN_ID,
    )

    dag \
        >> create_example_tables \
        >> insert_example_data \
        >> run_example_query \
        >> drop_example_tables
