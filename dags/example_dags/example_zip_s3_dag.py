"""
# Snowflake Example DAG.

SUMMARY:
--------

* DAG Name:
    zzz_example_zip_s3_dag.
* Owner:
    Rappiflow Admin Team
* Description:
    This is an example DAG intended to serve as an example of how to use:
        - ZipOperator
        - UploadFileToS3
        - PythonOperator


CONNECTIONS:
------------

* Connection Type S3 with the name `s3_cpgs_datascience`.


DISCLAIMER:
-----------

This is just an **EXAMPLE**, you may not have the correct credentials to run this DAG succesfully,
nevertheless you could use part of this for the development of your own DAGs

"""
import os
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from rappiflow.operators import ZipOperator, UploadFileToS3, CreateTempFolderOperator, RemoveLocalFolder

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=10),
}

settings = {
    'snowflake_conn_id': 'cpgs_datascience_write',  # Modify this connection id for a Snowflake connection of your own
    's3_conn_id': 's3_cpgs_datascience',  # Modify this with an S3 connection id you have access to
    's3_bucket': 'cpgs-datascience-dev',  # Modify this to a S3 bucket you have access to
}


def generate_csv(**kwargs):
    """Generate a test csv file."""
    csv_file = kwargs['output_path']

    with open(csv_file, 'w') as fp:
        for x in range(100):
            for y in range(10):
                fp.write(str(x+y) + ',')
            fp.write('\n')


with DAG('zzz_example_zip_s3_dag',
         max_active_runs=1,
         schedule_interval=None,
         start_date=datetime(2019, 9, 18),
         default_args=default_args
         ) as dag:

    create_temp_folder_task_id = 'create_temp_folder_task'

    create_temporary_folder = CreateTempFolderOperator(
        task_id=create_temp_folder_task_id,
        prefix='example_zip_s3_dag_',
    )

    temp_folder = "{{ task_instance.xcom_pull('%s', key='return_value') }}" % create_temp_folder_task_id

    csv_output_path = os.path.join(temp_folder, 'test_file.csv')

    create_csv_task = PythonOperator(
        task_id='generate_test_csv_task',
        provide_context=True,
        python_callable=generate_csv,
        op_kwargs={'output_path': csv_output_path},
        dag=dag,
    )

    zip_output_path = csv_output_path[:-3] + '.zip'

    zip_csv_file_task = ZipOperator(
        task_id='zip_csv_file_task',
        path_to_file_to_zip=csv_output_path,
        path_to_save_zip=zip_output_path,
    )

    output_zip_s3 = 'rappiflow/example_dags/test.zip'

    upload_zip_to_s3_task = UploadFileToS3(
        task_id='upload_zip_to_s3_task',
        local_file=zip_output_path,
        s3_destination_key=output_zip_s3,
        s3_destination_bucket=settings['s3_bucket'],
        aws_conn_id=settings['s3_conn_id'],
    )

    delete_temporary_folder = RemoveLocalFolder(
        task_id='remove_temporary_folder',
        folder_path=temp_folder,
    )

    dag \
        >> create_temporary_folder \
        >> create_csv_task \
        >> zip_csv_file_task \
        >> upload_zip_to_s3_task \
        >> delete_temporary_folder
