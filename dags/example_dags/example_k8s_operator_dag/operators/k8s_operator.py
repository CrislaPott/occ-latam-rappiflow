"""Operator to run a docker image into a k8s pod."""

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.utils.decorators import apply_defaults
from airflow.contrib.kubernetes.pod import Resources

from dags import settings
from rappiflow.utils.k8s_settings import (get_k8s_resources_mapping, get_k8s_node_group_type, get_k8s_node_affinity,
                                          get_k8s_node_tolerations)


class ExampleK8sOperator(KubernetesPodOperator):
    """Operator to run Docker images inside k8s pod."""

    ui_color = '#0fafff'

    @apply_defaults
    def __init__(self, resource_request: str, image_name: str, image_tag_name: str,
                 k8s_namespace: str, *args, **kwargs):
        """Class Initializer."""
        namespace = k8s_namespace
        image = f"{image_name}:{image_tag_name}"
        name = 'k8s-pod-operator-example'

        super().__init__(namespace=image_name, image=image, name=name, *args, **kwargs)

        self.image = image
        self.name = name
        self.namespace = namespace
        self.get_logs = True
        self.in_cluster = True
        self.is_delete_operator_pod = True

        if settings.IS_LOCAL:
            self.in_cluster = False
            self.cluster_context = 'docker-desktop'
            self.config_file = '/usr/local/airflow/include/k8s/config'
            self.image_pull_policy = 'Never'
        else:
            self.resources = Resources(
                request_memory=get_k8s_resources_mapping(resource_request)['memory'],
                limit_memory=get_k8s_resources_mapping(resource_request)['memory'],
                request_cpu=get_k8s_resources_mapping(resource_request)['cpu'],
                limit_cpu=get_k8s_resources_mapping(resource_request)['cpu']
            )

            node_group_assigned = get_k8s_node_group_type(get_k8s_resources_mapping(resource_request)['memory'])

            affinity = get_k8s_node_affinity(node_group=node_group_assigned)
            self.affinity = affinity

            toleration = get_k8s_node_tolerations(node_group=node_group_assigned)

            if toleration:
                self.tolerations = [toleration]
