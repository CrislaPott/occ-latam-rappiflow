"""DAG to test kubernetes pod operator."""

from datetime import datetime, timedelta

from airflow.models import DAG
from dags.example_dags.example_k8s_operator_dag.operators.k8s_operator import ExampleK8sOperator
from rappiflow.utils.k8s_settings import get_k8s_namespace

default_args = {
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

with DAG(
        'zzz_example_k8s_operator_dag',
        default_args=default_args,
        schedule_interval=None,
        start_date=datetime(2019, 1, 1),
        catchup=False
) as dag:

    k8s_example_operator_dag = ExampleK8sOperator(
        task_id="zzz_example_k8s_operator_dag_task",
        image_name="ubuntu",
        image_tag_name="latest",
        k8s_namespace=get_k8s_namespace(),
        startup_timeout_seconds=600,
        resource_request="M",
        cmds=["/bin/sleep", "900"]
    )

    dag \
        >> k8s_example_operator_dag
