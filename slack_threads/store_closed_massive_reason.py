import pandas as pd
import re
from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow
from datetime import datetime, timedelta
import datetime as dt

filter_stores = snow.run_query("""select store_id, reason, country, thread_ts, date from ops_occ.slack_pipe_stores_suspended""")

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=14)
channel_name = 'CS7UQAMLG'

all_chats =  pd.DataFrame()
channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)
all_chats = all_chats.append(channel_history)

all_chats['reactions'] = all_chats['reactions'].astype(str)
all_chats = all_chats[all_chats['reactions'].str.contains('compliance_bot')]
all_chats['thread_ts'] = all_chats['ts']



stores_suspended = pd.DataFrame()
for index, row in all_chats.iterrows():
    try:
        messages = tl.get_thread_replies(channel_name, row['ts'])[1:]
        messages['reactions'] = messages['reactions'].astype(str)
        messages = messages[(messages['reactions'].str.contains('compliance_bot'))]
    except:
        pass

    try:
        for index, row in messages.iterrows():
            thread_ts = row['thread_ts']
            country = cb.get_country(row['text'])
            date = row['ts']
            reason = re.search('(?<=reason:)(.*)', row['text'].lower()).group(1)
            stores =  re.search('```(.+?)```', row['text']).group(1)
            temp = {'store_id':[stores],'reason':[reason],'country':[country],'thread_ts':[thread_ts],'date':[date]}
            stores_suspended = stores_suspended.append(pd.DataFrame(temp))
            sleep(1)
    except:
        pass

try:
    final = stores_suspended.merge(all_chats[['thread_ts']],on='thread_ts')
except:
    print('error 0')
    pass

try:
    final['store_id'] = final['store_id'].str.split(',')
except:
    print('error 1')
    pass
    
try:
    final = final.explode('store_id').reset_index(drop=True)
except:
    print('error 2')
    pass


redoing = final[final['store_id'].str.contains(',')]

if redoing.empty:
    pass
else:
    final = final[~(final['store_id'].str.contains(','))]
    redoing['store_id'] = redoing['store_id'].str.split(',')
    redoing = redoing.explode('store_id').reset_index(drop=True)
    final = final.append(redoing)


try:
    final = final.merge(filter_stores, on=['store_id', 'reason', 'country', 'thread_ts', 'date'], how = 'left',indicator = True)
    final = final[final['_merge'] == 'left_only']
    final.drop(columns='_merge', inplace = True)
except:
    pass
    

final = final.drop_duplicates()


if len(final) > 0:
    snow.upload_df_occ(final,'slack_pipe_stores_suspended')
else:
    pass