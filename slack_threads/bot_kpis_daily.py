import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from time import sleep
from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

slack_users = snow.run_query("""--no cache
                SELECT * FROM ops_occ.slack_users""")


slack_users['title'] = slack_users['title'].fillna('No_Title')
occ_users = slack_users[(slack_users['title'].str.contains("OCC"))]


latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
channel_name = ['C029MJCAMS6','C02L5JX5CT0', 'C02L9B59BQV', 'C02L9BG8Z9T', 'C02LCB2N2AE'] 

all_chats =  pd.DataFrame()
for i in channel_name:
    channel_history = tl.get_channel_history(
        channel = i,
        messages_per_page = 500,
        max_messages = 20000,
        oldest = oldest,
        latest = latest
    )
    channel_history['channel_name'] = i
    all_chats = all_chats.append(channel_history)



base_messages = all_chats[(all_chats['user'] == 'U01FDE3RD7E')]
base_messages['country'] = base_messages['text'].apply(lambda x: cb.get_country(x))
try:
    base_messages['alarm_type'] = base_messages.apply(lambda x: cb.get_alert_type(x['text']), axis = 1)
except:
    pass
base_messages = base_messages[~base_messages['alarm_type'].isna()]
base_messages['thread_ts'] = base_messages['ts']
base_messages = base_messages[[ 'thread_ts', 'text', 'country','alarm_type','reply_count', 'channel_name']]


occ_replies = pd.DataFrame() 
for index, row in base_messages[base_messages['reply_count'] > 0].iterrows():
    thread_ts =  row['thread_ts']
    replies_details = pd.DataFrame(columns=['thread_ts','analysis_start_time','analysis_end_time','analyst','is_real','action','diagnosis','store_id','report_link' ]) 
    try: 
        messages = tl.get_thread_replies(row['channel_name'], row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: cb.get_slack_user(x, slack_users))
        
        messages = messages[['thread_ts', 'ts', 'slack_user', 'text']]
        
        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]
        analysis_start_time =  np.nan
        analysis_end_time = np.nan
        analyst  =  np.nan
        action = np.nan
        diagnosis =  np.nan
        store_id = np.nan
        report_link = np.nan
        validation = False
        try:
            for index, row in occ_replies_thread.iterrows():
                if cb.inicio_analise(row['text']) > 0.5:
                    analysis_start_time =  row['ts']
                    analyst =  row['slack_user']
                elif 'diagnóstico' in row['text'].lower() or 'diagnostico' in row['text'].lower():
                    analysis_end_time = row['ts']
                    action = cb.get_action_taken(row['text'])
                    diagnosis =  cb.get_diagnosis(row['text'])
                    store_id = cb.get_store_id(row['text'])
                    report_link = cb.get_report_link(row['text'])
                    validation = cb.effectivity(row['text'])

        except:
            pass
        replies_details ={'thread_ts': [thread_ts], 'analysis_start_time':[analysis_start_time] , 'analysis_end_time': [analysis_end_time], 'analyst':[analyst], 'is_real':[validation], 'action':[action],'diagnosis':[diagnosis],'store_id':[store_id],'report_link':[report_link]}
        occ_replies = pd.concat([occ_replies, pd.DataFrame(replies_details)])
    except Exception as e:
        print(e)
        pass
    sleep(1)

base_messages = base_messages.merge(occ_replies, on='thread_ts', how='left')
base_messages['action'] = base_messages.apply(lambda x: cb.transform_action(x['action']), axis = 1)
base_messages['thread_link'] =  base_messages.apply(lambda x: tl.get_thread_link(x['channel_name'],x['thread_ts'])['permalink'], axis = 1)
snow.upload_df_occ(base_messages,'bot_kpis_daily')