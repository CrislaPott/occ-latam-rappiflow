import pandas as pd
import os
from lib import slack
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones


def get_rate():
    rate = snow.run_query("""select * from global_finances.trm_fixed""")
    return rate

def products_with_high_discounts(country, rate):
    timezone, interval = timezones.country_timezones(country)
    base_high_discounted_products = snow.run_query("""
with VERTICAL as (select store_type as store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as sub_group
from VERTICALS_LATAM.{country}_VERTICALS_V2
),

base_orders as (select A.ID AS ORDER_ID,
       COUNT(CASE WHEN OM.TYPE = 'add_products_with_payment' THEN OM.ORDER_ID ELSE NULL END) AS ADDED_BY_USERS
       
from  {country}_core_orders_public.orders_vw a
            JOIN {country}_core_orders_public.order_modifications OM
                ON A.ID = OM.ORDER_ID
            LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
                ON         OS.ORDER_ID = A.ID
            LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON         S.STORE_ID = OS.STORE_ID
            JOIN VERTICAL
                ON vertical.STORE_TYPE = S.type
        where COALESCE(a._fivetran_deleted,false)=false
        and (( a.closed_at::date = convert_timezone('UTC','America/{timezone}',current_timestamp())::date
        and a.closed_at::timestamp_ntz >= convert_timezone('UTC','America/{timezone}',current_timestamp())::timestamp_ntz - interval '2h')
        or (a.created_at::date = convert_timezone('UTC','America/{timezone}',current_timestamp())::date
        and a.created_at::timestamp_ntz >= convert_timezone('UTC','America/{timezone}',current_timestamp())::timestamp_ntz - interval '2h'))
        and vertical.sub_group not in ( 'Antojos' , 'RappiCash' ,'RappiFavor')
        AND a.state NOT IN ('canceled_by_fraud',
                            'canceled_for_payment_error',
                            'canceled_by_split_error',
                            'canceled_by_early_regret')
                   
GROUP BY 1
HAVING ADDED_BY_USERS < 1)

,ids as (
        select a.id, 
               max(total_value) as total_value, 
               sum(total_price) as total_price,           
               s.store_id, 
               s.name, 
               sub_group as vertical, 
               a.created_at  
        from {country}_core_orders_public.orders_vw a
            LEFT join {country}_core_orders_public.order_product_vw B
                on a.id = b.order_id
            LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
                ON         OS.ORDER_ID = A.ID
            LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON         S.STORE_ID = OS.STORE_ID
            JOIN VERTICAL
                ON vertical.STORE_TYPE = S.type
        where COALESCE(a._fivetran_deleted,false)=false
        and COALESCE(b._fivetran_deleted,false)=false
        AND A.ID IN (SELECT DISTINCT ORDER_ID FROM BASE_ORDERS)
        group by 1,4,5,6,7 )

,INITIAL_VALUE AS (SELECT ORDER_ID,
                   payload:total_payment::numeric as order_initiaL_value,
                   a.id,
                   RANK () OVER (partition by a.order_id order by a.id desc) as rank 
                   FROM {country}_pg_ms_dispatch_courier_dispatcher.order_iterations a
                  join ids
                  on a.order_id = ids.id
                 group by 1,2,3 
                  )


select distinct 
       --IDS.CREATED_AT, 
       IDS.ID AS ORDER_ID,
       ROUND(INITIAL.order_initiaL_value / {rate},2) as INITIAL_VALUE,
       ROUND(TOTAL_VALUE / {rate},2) as FINAL_TOTAL_VALUE,
       ROUND(DIV0(FINAL_TOTAL_VALUE,INITIAL_VALUE), 2) AS GROWTH,
       STORE_ID, 
       NAME, 
       VERTICAL 
       from ids
             LEFT JOIN (SELECT ORDER_ID, SUM(PRICE) AS ADDED_PRICE from {country}_CORE_ORDERS_PUBLIC.ORDER_WHIMS 
                   WHERE COALESCE(_FIVETRAN_DELETED, false) = false
                   AND STATE = 'confirmed'
                   GROUP BY 1) ADDED
                ON ids.ID = ADDED.ORDER_ID 
            LEFT JOIN  INITIAL_VALUE INITIAL
                ON ids.ID = INITIAL.ORDER_ID
                AND INITIAL.RANK = 1
             having (initial_value >= 20
             and growth >= 2)
             order by growth desc
""".format(country = country, rate= rate, timezone=timezone))
    return base_high_discounted_products



countries = ['br','mx','ar','co','cr','cl','ec','pe','uy']

value = get_rate()

alarms = pd.DataFrame()
for i in countries:
    which_rate = value[value['country_code'] == i.upper()]['trm'].iloc[0]
    results = products_with_high_discounts(i, which_rate)
    results['country'] = i
    alarms = alarms.append(results)
    print(i+" done.")
    
home = expanduser("~")
results_file = '{}/fraude_rt.xlsx'.format(home)

if len(alarms) > 0:
    snow.upload_df_occ(alarms,'occ_rt_fraud')
    alarms.to_excel(results_file, sheet_name = "fraud_rt", index=False)
    book = load_workbook(results_file)
    writer = pd.ExcelWriter(results_file, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    writer.save()
    writer.close()



    slack.file_upload_channel('C02L9B59BQV',':alert: *Fraude RTs - Alta variacion de valores de orders* :alert:\n\n :warning: As seguintes orders estão com suspeita de fraude.',results_file,'xlsx')
else:
    # slack.bot_slack(':alert: *FRAUDE RT* :alert:\n\n :warning: Nenhuma order com suspeita de fraude.','C02L9B59BQV')
    print("nothing")
    pass
