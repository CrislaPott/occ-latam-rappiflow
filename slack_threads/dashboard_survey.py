import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from lib import snowflake as snow



# autenticando conexão com GAPI
scope = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file'
    ]
file_name = 'client_key.json'
credentials = ServiceAccountCredentials.from_json_keyfile_name(file_name,scope)
client = gspread.authorize(credentials)


# leitura dos casos gerais
dashboard = snow.run_query("""--no  cache 
select 
coalesce(o.deliveryboy_id,o.storekeeper_id) rt_id,
ca.city,
so.transport_media_type as vehicle, 
last_online_moment, case when last_online_moment::date=current_date::date then 'online' else 'offline' end as online_today,
case when extract(hour from coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)) in (0,1,2,3,4,5) then '0h-5h|Madrugada'
     when extract(hour from coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)) in (6,7,8,9,10,11) then '6h-12h|Manha'
     when extract(hour from coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)) in (12,13,14,15,16,17) then '12h-17h|Tarde'
     when extract(hour from coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)) in (18,19,20,21,22,23) then '18h-23h|Janta'
     else 'check_me'                                  
end as Hour,
max(coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)) as last_order_at,

count(distinct(case when coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)::date = current_date()::date then o.id else null end)) as Today,
count(distinct(case when coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)::date = dateadd(day,-7,convert_timezone('America/Sao_Paulo',current_timestamp))::date then o.id else null end)) as L7D

from br_core_orders_public.orders o
left join br_core_orders_calculated_information.orders o2 on o.id = o2.order_id and coalesce(o2._fivetran_deleted,false)=false
left join br_grability_public.city_addresses ca on ca.id=o2.city_address_id and country_id=5 and coalesce(ca._fivetran_deleted,false)=false
join BR_PG_MS_STOREKEEPERS_PUBLIC.storekeepers_ofuscated so on so.id = coalesce(o.deliveryboy_id,o.storekeeper_id) and so.type = 'rappitendero'

left join (
  SELECT courier_id as storekeeper_id, max(created_at) as last_online_moment
FROM informatica.BR_PG_MS_DISPATCH_COURIER_DISPATCHER.COURIER_ITERATIONS
  where created_at::date >= dateadd(day,-8,convert_timezone('America/Sao_Paulo',current_timestamp))::date
group by 1) logs on logs.storekeeper_id = coalesce(o.deliveryboy_id,o.storekeeper_id)

left join (select distinct storekeeper_id from br_pglr_ms_cns_discipline_ms_public.storekeeper_emergencies
where end_time_disabled::DATE >= current_date and deleted_at is null) inci on inci.storekeeper_id = so.id

--left join (select distinct storekeeper_id from br_pglr_ms_cns_discipline_ms_public.incidents
--where applied=true and declined=false) inci on inci.storekeeper_id = so.id

where 
coalesce(o.closed_at, o.taked_at,o.updated_at,o.created_at)::date >= dateadd(day,-8,convert_timezone('America/Sao_Paulo',current_timestamp))::date
and o.state in ('finished','pending_review')
and inci.storekeeper_id is null --and inci.storekeeper_id is null
and so.is_active='t'
group by 1,2,3,4,5,6""")



# Abrindo a SpreadSheet
sheet = client.open('Dashboard - Survey')

# Acessando a primeira aba da spreadsheet
worksheet = sheet.get_worksheet(0)
worksheet.clear()


# Convertendo o campo para texto por compatibilidade
try:
    dashboard['last_order_at'] = dashboard['last_order_at'].dt.strftime('%Y-%m-%d %H:%M:%S')
except:
    pass
try:
    dashboard['last_online_moment'] = dashboard['last_online_moment'].dt.strftime('%Y-%m-%d %H:%M:%S')
except:
    pass

# Inserindo dados da consulta na aba da spreadsheet
worksheet.update([dashboard.columns.values.tolist()] + dashboard.values.tolist())