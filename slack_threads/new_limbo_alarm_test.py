import pandas as pd 
from os.path import expanduser
from openpyxl import load_workbook
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones




def remove_verticals(orders, db_id,country):
       timezone,timeframe = timezones.country_timezones(country)
       if not orders.empty:
              order_list = ", ".join(map(str, orders['order_id'].to_list()))
              get_vertical_query = """ SET TIMEZONE TO 'America/{timezone}';
              select  a.order_id, b.group as vertical, a.type as order_type,c.state as is_closed_order, 
              case when place_at is not null and place_at <= now() then 'yes'
                   when place_at is null then 'yes'
                   else 'no' END AS is_past_placed_at from order_stores as a
                     join verticals as b
                     on a.store_type_group = b.store_type
                     join orders as c
                     on a.order_id = c.id
              where order_id in ({order_list})
              and state not ilike '%cancel%'
              and state not in ('finished','pending_review')
              """.format(order_list=order_list,timezone=timezone)
              verticals = redash.run_query(db_id,get_vertical_query)
              orders = orders.merge(verticals, on='order_id')
              orders = orders[(orders['vertical'] != "Rappifavor") & (orders['vertical'] != "Restaurantes") & (orders['order_type'] != "queue_order_flow") & (orders['is_past_placed_at'] != 'no')]
              orders.drop(columns=['vertical','order_type','is_closed_order','is_past_placed_at'], inplace = True)
       return orders





limbo = """WITH BASE_ORDERS AS (   SELECT   distinct on (b.order_id) b.order_id,
                         b.id AS latest_event
                FROM     order_events b
                WHERE b.created_at >= now() - interval '80 minutes'
                ORDER BY 1,2 DESC
                )
                ,

LIMBO_CHECKER AS (SELECT a.order_id,
       d.store_id,
       a.type,
       a.NAME,
       a.params->>'event' "event",
       a.created_at at time zone 'America/Buenos_Aires' ,
       extract(epoch FROM now() - a.created_at )/60 AS minutes_difference,
       CASE WHEN d.is_placed_now = 'true' then 'NOW' else 'NOT_NOW' end as is_now,
       CASE WHEN extract(epoch FROM now() - a.created_at )/60 >= 40 then 'YES'
       ELSE 'NO' END as TRIGGERED
FROM   order_events a
JOIN BASE_ORDERS c
ON     a.order_id = c.order_id
AND    a.id = c.latest_event
JOIN order_summary d
on a.order_id = d.order_id
WHERE  type = 'event'
AND      d.state NOT ilike '%cancel%'
AND      d.state NOT IN ('finished',
                         'pending_review')
AND    NAME NOT IN ('order_canceled',
                    'order_finished',
                    'order_scheduled',
                    'scheduled_tagged_and_calculated',
                    'multiple_products_added',
                    'product_preference_updated',
                    'product_weight_quantity_updated',
                    'picking_finished',
                    'picking_started',
                    'product_removed',
                    'product_checked',
                    'order_in_payment',
                    'rt_took_order',
                    'order_in_picking',
                    'whim_added',
                    'released_to_rt',
                    'order_received_by_sk',
                    'order_to_sh',
                     'released_to_integration',
                     'released_to_picker',
                     'assign_to_shopper',
                     'order_to_shopper',
                     'handshake_code_request',
                     'integration_order_creation_notified',
                     'price_difference_updated',
                     'elimination_approved',
                     'product_preference_updated',
                     'product_quantity_updated',
                     'released_to_shopper',
                     'order_assigned'
                    )
ORDER BY 1 ASC)

SELECT * from LIMBO_CHECKER
WHERE TRIGGERED = 'YES'
"""

final = pd.DataFrame()

try:
       br = redash.run_query(2447,limbo)
       if not br.empty:
              br['country'] = 'br'
              br = remove_verticals(br,1338,'br')
       final = final.append(br)
       print('BR done') 
except:
       pass   
try: 
       ar = redash.run_query(2430,limbo)
       if not ar.empty:
              ar['country'] = 'ar'
              ar = remove_verticals(ar,1337,'ar')
       final = final.append(ar)
       print('AR done')
except Exception as e:
       print(e)
       pass
try:
       cr = redash.run_query(2552,limbo)
       if not cr.empty:
              cr['country'] = 'cr'
              cr = remove_verticals(cr,1921,'cr')
       final = final.append(cr)
       print('CR done')
except:
       pass
try:
       co = redash.run_query(2449,limbo)
       if not co.empty:
              co['country'] = 'co'
              co = remove_verticals(co,1904,'co')
       final = final.append(co)
       print('CO done')
except Exception as e:
       print(e)
       pass
try:
       cl = redash.run_query(2431,limbo)
       if not cl.empty:
              cl['country'] = 'cl'
              cl = remove_verticals(cl,1155,'cl')
       final = final.append(cl)
       print('CL done')
except:
       pass
try:
       ec = redash.run_query(2559,limbo)
       if not ec.empty:
              ec['country'] = 'ec'
              ec = remove_verticals(ec,1922,'ec')
       final = final.append(ec)
       print('EC done')
except:
       pass
try:
       mx = redash.run_query(2450,limbo)
       if not mx.empty:
              mx['country'] = 'mx'
              mx = remove_verticals(mx,1371,'mx')
       final = final.append(mx)
       print('MX done')
except Exception as e:
       print(e)
       pass
try:
       pe = redash.run_query(2448,limbo)
       if not pe.empty:
              pe['country'] = 'pe'
              pe = remove_verticals(pe,1157,'pe')
       final = final.append(pe)
       print('PE done')
except Exception as e:
       print(e)
       pass
try:
       uy = redash.run_query(2426,limbo)
       if not uy.empty:
              uy['country'] = 'uy'
              uy = remove_verticals(uy,1156,'uy')
       final = final.append(uy)
       print('UY done')
except:
       pass


print(final)


home = expanduser("~")
results_file = '{}/limbo_teste.xlsx'.format(home)
try:
       final['order_id'] = final['order_id'].astype(int)
       final['order_id'] = final['order_id'].astype(str)
       final['store_id'].fillna('9999999999999', inplace = True)
       events = final.groupby(['country','store_id','is_now'])['name'].agg(','.join).reset_index(name='error_list')
       final = final.groupby(['country','store_id','is_now'])['order_id'].agg(','.join).reset_index(name='order_list').merge(\
              final.groupby(['country','store_id','is_now'])['order_id'].count().reset_index(name='orders'), \
              on=['country','store_id','is_now'])
       final = final.merge(events, on = ['country','store_id','is_now'])
       now = final[final['is_now'] == 'NOW']
       not_now = final[final['is_now'] == 'NOT_NOW']
except Exception as e:
       print(e)
       pass

current_time = timezones.country_current_time('co')
try:
       to_upload = final.copy()
       to_upload['alarm_at'] = current_time
       print(to_upload)
       snow.upload_df_occ(to_upload,'limbo_teste')
except:
       pass

try:
       not_now.to_excel(results_file, sheet_name = "NOT NOW", index=False)
       book = load_workbook(results_file)
       writer = pd.ExcelWriter(results_file, engine='openpyxl')
       writer.book = book
       writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
       now.to_excel(writer, sheet_name = "NOW", index=False)
       writer.save()
       writer.close()

       slack.file_upload_channel('C036PQNDQ1E',':alert: *TESTE DE LIMBO* :alert:\n\n :warning: As seguintes orders não tem eventos novos há pelo menos 25 minutos.',results_file,'xlsx')
except:
       pass