import os, json
import numpy as np 
import pandas as pd
#import slack_sdk as slack > nao usar
import logging as logger
from time import sleep
from dotenv import load_dotenv
from datetime import datetime, timedelta
from airflow.models import Variable
import slack
from slack.errors import SlackApiError

load_dotenv()

client = slack.WebClient(token=(Variable.get("SLACK_TOKEN")))

def get_channel_history(channel, messages_per_page, max_messages, oldest, latest):
    # get first page
    page = 1
    print("Retrieving page {}".format(page))
    response = client.conversations_history(
        channel=channel,
        limit=messages_per_page,
        oldest=str(oldest.timestamp()),
        latest=str(latest.timestamp())
        
    )
    assert response["ok"]
    messages_all = response['messages']

    # get additional pages if below max message and if they are any
    while len(messages_all) + messages_per_page <= max_messages and response['has_more']:
        page += 1
        print("Retrieving page {}".format(page))
        sleep(1)   # need to wait 1 sec before next call due to rate limits
        response = client.conversations_history(
            channel=channel,
            limit=messages_per_page,
            oldest=str(oldest.timestamp()),
            latest=str(latest.timestamp()),
            cursor=response['response_metadata']['next_cursor']
        )
        assert response["ok"]
        messages = response['messages']
        messages_all = messages_all + messages

    print(
        "Fetched a total of {} messages from channel {}".format(
            len(messages_all),
            channel
    ))

    return pd.DataFrame(messages_all)

def get_thread_replies(channel, thread_ts):
    thread_info = client.conversations_replies(
        channel = channel,
        ts = thread_ts
    )
    
    thread_messages = pd.DataFrame(thread_info['messages'])
    if 'reactions' not in thread_messages:
        thread_messages['reactions'] = np.nan
        thread_messages['reactions'] = thread_messages['reactions'].astype(str)
    if 'subtype' not in thread_messages:
        thread_messages['subtype'] = np.nan
        thread_messages['subtype'] = thread_messages['subtype'].astype(str)
    if 'username' not in thread_messages:
        thread_messages['username'] = np.nan
        thread_messages['username'] = thread_messages['username'].astype(str)
    if 'bot_id' not in thread_messages:
        thread_messages['bot_id'] = np.nan
        thread_messages['bot_id'] = thread_messages['bot_id'].astype(str)
    thread_messages =  thread_messages[[
        'type',
        'reactions',
        'text',
        'user',
        'ts',
        'thread_ts',
        'latest_reply',
        'subtype',
        'username',
        'bot_id'
    ]]
    
    return thread_messages

def get_user_list():
        get_all_users = []
        for iterations in client.users_list(limit=1000):
            get_all_users += iterations["members"]
        return get_all_users

def send_message(channel, message):
    try:
        result = client.chat_postMessage(
            channel=channel, 
            text=message
        )
        return result

    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")

def send_reply(channel, message, thread_ts):
    try:
        result = client.chat_postMessage(
            channel=channel, 
            text=message,
            thread_ts=thread_ts
        )

    except SlackApiError as e:
        logger.error(f"Error posting message: {e}")


def get_thread_link(channel, thread_timestamp):
    try:
        return client.chat_getPermalink(
            channel=channel,
            message_ts=thread_timestamp
        )
    except SlackApiError as e:
        logger.error(f"Error getting link: {e}")