import numpy as np
import re
from difflib import SequenceMatcher
from time import sleep


def is_occ_kpis(user, kpis_user_list):
    if len(kpis_user_list[(kpis_user_list['slack_user_id'] == user) ]) > 0:
        return True
    else:
        return False

def is_closed(reactions):
    try: 
        reactions =  str(reactions)
        return 'close' in reactions
    except:
        return False

def get_users_reacted(reactions):
    try:
        return reactions[0]['users']
    except:
        return np.nan

def get_occ_users(thread_users, user_list, occ_user_list):
    try:
        usernames = user_list[
            user_list['slack_user_id'].isin(thread_users)
        ]['slack_user'].unique().tolist()

        occ_thread_users = occ_user_list[
            occ_user_list['slack_user'].isin(usernames)
        ]['slack_user'].unique().tolist()

        if len(occ_thread_users) == 0:
            return np.nan
        
        return occ_thread_users 
    except:
        return np.nan

def get_non_occ_users(thread_users, user_list, occ_user_list):
    try:
        usernames = user_list[
            user_list['slack_user_id'].isin(thread_users)
        ]

        non_occ_thread_users = usernames[
            ~(usernames['slack_user'].isin(occ_user_list['slack_user'].unique().tolist()))
        ]['slack_user'].unique().tolist()

        if len(non_occ_thread_users) == 0:
            return np.nan 
        
        return non_occ_thread_users
    except:
        return np.nan 


def get_country(thread):
    thread = thread.lower()
    try:
        if 'argentina' in thread or ':flag-ar:' in thread:
            country = 'AR'
        elif 'brasil' in thread or 'brazil' in thread  or ':flag-br:' in thread:
            country  =  'BR'
        elif 'costa rica' in thread  or ':flag-cr:' in thread:
            country  =  'CR'
        elif 'chile' in thread  or ':flag-cl:' in thread:
            country  =  'CL'
        elif 'colombia' in thread or 'colômbia' in thread  or ':flag-co:' in thread:
            country  =  'CO'
        elif 'ecuador' in thread or 'equador' in thread  or ':flag-ec:' in thread  or ':ec_flag:' in thread:
            country  =  'EC'
        elif 'mexico' in thread or 'méxico' in thread  or ':flag-mx:' in thread:
            country  =  'MX'
        elif 'peru' in thread or 'perú' in thread  or ':flag-pe:' in thread:
            country  =  'PE'
        elif 'uruguai' in thread or 'uruguay' in thread  or ':flag-uy:' in thread:
            country  =  'UY'
        return country
    except:
        return  None

def get_vertical(thread):
    thread = thread.lower()
    if 'restaurante' in thread:
        vertical = 'Restaurantes'
    elif 'ecommerce' in thread or 'commerce' in thread:
        vertical = 'Ecommerce'
    elif 'antojos' in thread:
        vertical = 'Antojos'
    elif 'rappicash' in thread:
        vertical = 'RappiCash'
    elif 'rappifavor' in thread:
        vertical = 'RappiFavor'
    elif 'turbo' in thread:
        vertical = 'Turbo'
    elif 'mercados' in thread or 'super' in thread or 'hiper' in thread or 'super/hiper' in thread:
        vertical = 'Mercados'
    elif 'farmacia' in thread or 'farmácia' in thread or 'pharma' in thread:
        vertical = 'Farmacia'
    elif 'licores' in thread:
        vertical = 'Licores'
    elif 'express' in thread:
        vertical = 'Express'
    elif 'babies_kids' in thread:
        vertical = 'Babies_Kids'
    elif 'tech' in thread:
        vertical = 'Tech'
    elif 'cpgs' in thread or 'cpg' in thread:
        vertical = 'CPGs'
    elif 'others' in thread or 'other' in thread:
        vertical = 'Others'
    else:
        vertical = None
    return vertical

def get_alert_type(thread):  
    try:
        if 'alarma de dispersion' in thread.lower():
            alert = 'Alarma de Dispersion'
        elif 'alert type' in thread.lower():
            alert = re.search('alert type:\* (.+?)\n', thread.lower()).group(1).title()
        elif 'tipo de alarma' in thread.lower():
            alert = re.search('tipo de alarma:\* (.+?)\n', thread.lower()).group(1).title()
        else:
            alert = None
    except:
        alert = None
    return alert

def get_slack_user(user_id, user_list):
    try:
        username = user_list[
            user_list['slack_user_id'] == user_id
        ]['slack_user'].unique().tolist()[0]
        return username
    except:
        return np.nan

def get_suspended(thread, user, kpis_users):
    try:
        if (('susp' in thread.lower()) and ('indefinidamente' in thread.lower() or 'indefinido' in thread.lower() or 'retorno' in thread.lower() or 'respuesta' in thread.lower() or 'resposta' in thread.lower()) and ((is_occ_kpis(user,kpis_users) == True))):
            return 1
        else:
            return 0
    except:
        return 0

def inicio_analise(thread):
    return SequenceMatcher(None, 'inicio analisis', thread.lower()).ratio()

def effectivity(thread):
    effect = re.search('(?<=Efectividad:)(.*)', thread).group(1)
    return not (SequenceMatcher(None, 'falso positivo', effect.lower()).ratio() > 0.4)

def get_diagnosis(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'diag' in value.lower():
            return value
    return None 

def get_action_taken(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'acao:' in value.lower():
            return value
        if 'accion:' in value.lower():
            return value 
        if 'ação:' in value.lower():
            return value 
        if 'açao:' in value.lower():
            return value 
        if 'acão:' in value.lower():
            return value 
        if 'acción:' in value.lower():
            return value 
        if 'action:' in value.lower():
            return value 
    return None 

def transform_action(text_to_parse):
    try:
        if 'ninguna' in text_to_parse.lower():
            return 'nenhuma'
        elif 'nenhuma' in text_to_parse.lower():
            return 'nenhuma'
        elif 'ninguno' in text_to_parse.lower():
            return 'nenhuma'
        elif 'sem acoes' in text_to_parse.lower():
            return 'nenhuma'
        elif 'sem acao' in text_to_parse.lower():
            return 'nenhuma'
        elif 'sim accion' in text_to_parse.lower():
            return 'nenhuma'
        elif 'ok' in text_to_parse.lower():
            return 'nenhuma'
        else:
            return text_to_parse
    except:
        return None

def get_report_link(text):
    text = text.lower()
    try:
        match = re.findall("(?<=https:\/\/)(.*)(?=\>)", text)[0]
        result = str(match)
        return result 
    except:
        return np.nan

def get_store_id(text):
    text = text.lower()
    if 'store_id' in text:
        try:
            match = re.findall("(?<=store_id:)(.*)", text)[0]
            result = str(match)
            return result
        except:
            pass
    elif 'store id' in text:
        try:
            match = re.findall("(?<=store id:)(.*)", text)[0]
            result = str(match)
            return result
        except:
            pass
    return np.nan

def get_alert_type(thread):  
    try:
        if 'pico de ordenes' in thread.lower():
            alert = 'Pico de Ordenes Creadas'
        elif 'payless alert' in thread.lower() or  'wrong payment' in thread.lower():
            alert = 'Payless Alert'
        elif 'cbpr' in thread.lower():
            alert = 'Cancelaciones por CBPR'
        elif 'cbpi' in thread.lower():
            alert = 'Cancelaciones por CBPI'
        elif 'turbo store delays' in thread.lower():
            alert = 'Turbo Store Delays'
        elif 'worst offender' in thread.lower() :
            alert = 'Worst Offenders Stores'
        elif 'rappi connect' in thread.lower() or 'rappiconnect' in thread.lower():
            alert = 'Rappi Connect - Cancel by Picker'
        elif 'con precio' in thread.lower():
            alert = 'Producto con precio $0 o $1'
        elif 'limbo' in thread.lower():
            alert = 'Limbo (Picker Stores)'
        elif 'dispersion' in thread.lower():
            alert = 'Alarma de Dispersion'
        elif 'high discounts' in thread.lower():
            alert = 'High Discounts'
        elif 'global offers' in thread.lower():
            alert = 'Global Offers'
        elif 'pico de cancelac' in thread.lower():
            alert = 'Pico de Cancelaciones en Vertical'
        elif 'alarma riesgos' in thread.lower():
            alert = 'Riesgos'
        elif 'diferencia de precio producto maestro incorrecto' in thread.lower():
            alert = 'Diferencia de precio producto maestro incorrecto'
        elif 'turbo cancellation vs d-1' in thread.lower():
            alert = 'Turbo Cancellation vs D-1'
        elif 'productos con alta variacion de stock' in thread.lower():
            alert = 'Productos con alta variacion de stock'
        elif 'tiendas on/off' in thread.lower():
            alert = 'Tiendas on/off - CMS Logger'
        elif 'turbo fake orders' in thread.lower():
            alert = 'Turbo Fake Orders'
        elif 'electrónico precio bajo' in thread.lower():
            alert = 'Electrónico Precio Bajo'
        elif 'stockout' in thread.lower() or 'stock out' in thread.lower():
            alert = 'Stockout'
        elif 'rt in marketplace' in thread.lower():
            alert = 'RT in marketplace'
        elif 'productos sospechosos en ecommerce com precio muy bajo' in thread.lower():
            alert = 'Productos sospechosos precio bajo - Ecommerce'
        elif 'store closed' in thread.lower():
            alert = 'Store Closed'
        elif 'down selling' in thread.lower():
            alert = 'Down Selling'
        elif 'rt fraudulento' in thread.lower():
            alert = 'RT fraudulento'
        elif 'indicación de posibles errores' in thread.lower():
            alert = 'Posibles Errores'
        elif 'alarma de falta de rts' in thread.lower():
            alert = 'Alarma de Falta de RTs'
        elif 'stock bajo' in thread.lower():
            alert = 'Stock bajo'
        elif 'limitador de sku' in thread.lower():
            alert = 'Limitador de SKUs'
        elif 'ley seca' in thread.lower():
            alert = 'Ley Seca'
        else:
            alert = None
    except:
        alert = None
    return alert


def fixing_messages_breaks(message,max_len = 4000):
    for i in range(3999,len(message), max_len):
        if message[i] == '<':
            message = message[:i]+ "\n" + message[i:]
        else:
            for j in range(0, 100):
                if message[i-j] == '<':
                    message = message[:i-j]+ "\n" + message[i-j:]
                    break;
                else:
                    pass
    return message
    
    
def non_live_get_process(thread):
    if 'poligonos incorrectos' in thread.lower():
        process = 'POLIGONOS'
    elif 'fuera de cobertura' in thread.lower():
        process = 'COBERTURA'
    elif 'sin slots' in thread.lower():
        process = 'SLOTS'
    elif 'autocall' in thread.lower():
        process = 'AUTOCALL'
    elif 'worst offenders' in thread.lower() and 'long tail' in thread.lower():
        process = 'WORST OFFENDERS LT'
    elif 'worst offenders' in thread.lower():
        process = 'WORST OFFENDERS'
    elif 'fallback rt' in thread.lower():
        process = 'FALLBACK'
    elif 'sin id fisico' in thread.lower():
        process = 'SIN ID'
    elif 'payless in erp' in thread.lower():
        process = 'PAYLESS'
    elif 'programadas con horario' in thread.lower():
        process = 'PROGRAMADAS'
    elif 'action cpgs' in thread.lower():
        process = 'INTEGRACAO CPGS'
    else:
        process = np.nan
    return process
    
    
def get_cpgs_country(channel):
    if channel in ['C016V5K9DTK', 'C016REGSD44', 'C0172H3TMHP', 'C016SA516GG', 'C01CKH3F7UN', 'C01DGCHAEDN','C016Q12NVPV'] :
        country = 'BR'
    elif channel in ['C019ZQQJ9FC','C019G4FQWGP','C01A01H6Q11','C019TQX1NJZ']:
        country = 'AR'
    elif channel in ['C01DLLADJD7','C01D8767HCK']:
        country = 'CL'
    elif channel in ['G01EEFDM8KG','G01ETCCR0SG']:
        country = 'CO'
    elif channel in ['G01A4RGEVTL']:
        country = 'CR'
    elif channel in ['G01A52342QK']:
        country = 'EC'
    elif channel in ['G01G26SNKCG','G01FR1FE001','G01EXMABEJK','G01FCL4T199']:
        country = 'MX'
    elif channel in ['C019XJK4S4W','G01AAG3C1T6']:
        country = 'PE'
    elif channel in ['G01DJJ285N1']:
        country = 'UY'
    else:
        country = np.nan
    return country


def get_solicit_cpgs(solicitation):
    value = solicitation.lower()
    if 'distribuição' in value or 'distribución' in value:
        solicit = 'Distribuição'
    elif 'bug' in value and 'machetazo' not in value:
        solicit = 'Bug'
    elif 'antecipação' in value or 'anticipar' in value:
        solicit = 'Antecipação'
    elif 'dispersão' in value or 'dispersao' in value:
        solicit = 'Dispersão'
    elif 'cancelamento' in value or 'cancelación' in value:
        solicit = 'Cancelamento'
    elif 'capacidade' in value:
        solicit = 'Capacidade'
    elif 'machetazo' in value:
        solicit = 'Machetazo'
    elif 'chuva_proativo' in value or 'chuva proativo' in value:
        solicit = 'Chuva Proativo'
    elif 'chuva_reativo' in value or 'lluvia reactivo' in value or 'chuva reativo' in value:
        solicit = 'Chuva Reativo'
    elif 'comunicado' in value:
        solicit = 'Comunicado (reativo)'
    elif 'slots' in value:
        solicit = 'Abertura Slots'
    elif 'problemas' in value or 'poblemas' in value:
        solicit = 'Problemas Loja'
    elif 'expediente' in value or 'jornada' in value or 'fim_exp' in value:
        solicit = 'Fim de expediente'
    elif 'fraude' in value:
        solicit = 'Fraude'
    else:
        solicit = value
    return solicit
