import pandas as pd
import numpy as np
import re
from datetime import datetime, timedelta
from time import sleep

from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

slack_users = snow.run_query("""--no cache
                SELECT * FROM ops_occ.slack_users""")

slack_users['title'] = slack_users['title'].fillna('No_Title')

filter_stores = snow.run_query("""select username, store_id, action, country, thread_ts, date from ops_occ.non_live_threads""")

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=3)
channel_name = 'C02MJQ1Q5H6'

all_chats =  pd.DataFrame()
channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest)
all_chats = all_chats.append(channel_history)


alarms = ['NONLIVE - Alarma PIN/POLIGONOS INCORRECTOS',\
          'NONLIVE - Alarma PIN INCORRECTO O FUERA DE COBERTURA',\
          'NONLIVE - TIENDAS PROGRAMADAS SIN SLOTS',\
          'NONLIVE - CAMBIO DEL TIEMPO DE AUTOCALL',\
          'NONLIVE - ALARMA WORST OFFENDERS',\
          'NONLIVE - Alarma Integrations - ACTION CPGS',\
          'NONLIVE - CONFIGURACIÓN DE FALLBACK RT',\
          'NONLIVE - Alarma Tiendas SIN ID FISICO',\
          'Stores Payless in ERP and not in backend',\
          'NONLIVE - TIENDAS NO PROGRAMADAS CON HORARIO 24 HORAS'
              ]

base_chats = all_chats[all_chats['text'].str.contains('|'.join(alarms))]
base_chats['process'] =  base_chats['text'].apply(lambda x: cb.non_live_get_process(x) )
base_chats['thread_ts'] = base_chats['ts']

stores_suspended = pd.DataFrame()
for index, row in base_chats.iterrows():
    try:
        messages = tl.get_thread_replies(channel_name, row['ts'])[1:]
        messages['reactions'] = messages['reactions'].astype(str)
        messages = messages[~(messages['reactions'].str.contains('red_circle'))]
    except:
        pass

    try:
        for index, row in messages.iterrows():
            country  = ''
            date = ''
            action = ''
            stores = ''
            user = ''
            thread_ts = ''
            if 'stores:' in row['text'].lower().replace('*','') or 'stores :' in row['text'].lower().replace('*',''):
                thread_ts = row['thread_ts']
                country = cb.get_country(row['text'])
                date = row['ts']
                action = cb.get_action_taken(row['text'])
                stores =  re.search('```(.+?)```', row['text']).group(1)
                user = cb.get_slack_user(row['user'], slack_users)
                temp = {'username':[user],'store_id':[stores],'action':[action],'country':[country],'thread_ts':[thread_ts],'date':[date]}
                stores_suspended = stores_suspended.append(pd.DataFrame(temp))
            else:
                pass
            sleep(1)
    except:
        pass


final = stores_suspended.merge(base_chats[['thread_ts','process']],on='thread_ts')

try:
    final['store_id'] = final['store_id'].str.split(',')
except:
    pass

try:
    final = final.explode('store_id').reset_index(drop=True)
except:
    pass


try:
    final = final.merge(filter_stores, on=['store_id', 'action', 'country', 'thread_ts', 'username', 'date'], how = 'left',indicator = True)
    final = final[final['_merge'] == 'left_only']
    final.drop(columns='_merge', inplace = True)
except:
    pass

final = final.drop_duplicates()

if len(final) > 0:
    snow.upload_df_occ(final,'non_live_threads')
else:
    pass