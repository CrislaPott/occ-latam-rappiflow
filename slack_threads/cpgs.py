import pandas as pd
import numpy as np
from pandas.io.formats.format import TableFormatter
import re
from datetime import datetime, timedelta
from time import sleep

from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

################################## STEP 1 ##########################################

new = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
old = new - timedelta(days=1)
management_channel = 'C01BJUSMSG7'

management =  pd.DataFrame()
channel_history = tl.get_channel_history(
    channel = management_channel,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = old,
    latest = new)
management = management.append(channel_history)


management = management[~(management['username'].isna()) & (management['subtype'] == 'bot_message')].drop_duplicates(subset='thread_ts', keep='first')

slack_users = snow.run_query("""select *  from ops_occ.slack_users""")
slack_users['title'] = slack_users['title'].fillna('No_Title')

occ_users = slack_users[(slack_users['title'].str.contains("OCC"))]

thread_replies = pd.DataFrame()

for index, row in management.iterrows():
    try:
        messages = tl.get_thread_replies(management_channel, row['ts'])[1:]  
    except:
        pass
    for index, row in messages.iterrows():
        ts = ''
        comment = ''
        pais = ''
        action = ''
        stores = ''
        if 'acción' in row['text'].lower() or 'acao:' in row['text'].lower() or 'accion:' in row['text'].lower() or 'ação:' in row['text'].lower() or 'acão:' in row['text'].lower() or 'action:' in row['text'].lower():
            ts = row['thread_ts']
            comment = row['ts']
            pais = cb.get_country(row['text'])
            stores = cb.get_store_id(row['text'])
            action = cb.get_action_taken(row['text'])
            temp = {'thread_ts':[ts],'store_id':[stores],'action':[action],'country':[pais],'comment':[comment]}
            thread_replies = thread_replies.append(pd.DataFrame(temp))
        sleep(0.5)

thread_replies = thread_replies.drop_duplicates()
try:
    thread_replies =  thread_replies[['thread_ts','store_id','action','country']]
except:
    pass

final_management = management.merge(thread_replies, left_on='ts', right_on='thread_ts')

final_management = final_management[['username','text','ts','action','country']]

final_management['action'] = final_management['action'].apply(lambda x: cb.transform_action(x))


snow.upload_df_occ(final_management,'final_management_cpgs')


#####################################STEP 2######################################


latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
channel_name = ['C016V5K9DTK', 'C0172H3TMHP', 'C016SA516GG', 'C01CKH3F7UN', 'C01DGCHAEDN','C016Q12NVPV','C019ZQQJ9FC','C019G4FQWGP','C01A01H6Q11','C019TQX1NJZ','C01DLLADJD7','C01D8767HCK','G01EEFDM8KG','G01ETCCR0SG','G01A4RGEVTL','G01A52342QK','G01G26SNKCG','G01FR1FE001','G01EXMABEJK','G01FCL4T199','C019XJK4S4W','G01AAG3C1T6','G01DJJ285N1'] 

all_chats =  pd.DataFrame()
for i in channel_name:
    channel_history = tl.get_channel_history(
        channel = i,
        messages_per_page = 500,
        max_messages = 20000,
        oldest = oldest,
        latest = latest
    )
    channel_history['channel_name'] = i
    all_chats = all_chats.append(channel_history)

workflows = all_chats[(all_chats['subtype'] == 'bot_message') & (all_chats['username'].notna())]

if 'reactions' not in workflows:
    workflows['reactions'] = None
    workflows['reactions'] = workflows['reactions'].astype(str)

workflows['users_reacted'] = workflows.apply(lambda x: cb.get_users_reacted(x['reactions']), axis =1 )
workflows['is_closed'] = workflows.apply(lambda x: cb.is_closed(x['reactions']), axis = 1)


workflows['thread_ts'] = workflows['ts']
workflows['occ_users'] = workflows['reply_users'].apply(lambda x: cb.get_occ_users(x, slack_users, occ_users))
workflows['non_occ_users'] = workflows['reply_users'].apply(lambda x: cb.get_non_occ_users(x, slack_users, occ_users))
workflows['country'] = workflows['channel_name'].apply(lambda x: cb.get_cpgs_country(x))
workflows['solicitation'] = workflows['username'].apply(lambda x: cb.get_solicit_cpgs(x))
workflows['occ_reactions'] =  workflows['users_reacted'].apply(lambda x: cb.get_occ_users(x, slack_users, occ_users))
workflows['non_occ_reactions'] =  workflows['users_reacted'].apply(lambda x: cb.get_non_occ_users(x, slack_users, occ_users))

workflows.rename(columns={
    'solicitation': 'workflow'
}, inplace=True)

workflows = workflows[['workflow', 'thread_ts', 'text', 'reply_users', 'occ_users', 'non_occ_users', 'country', 'occ_reactions', 'non_occ_reactions','channel_name']]


occ_replies = pd.DataFrame() 
non_occ_replies =  pd.DataFrame()
for index, row in workflows.iterrows():
    try: 
        messages = tl.get_thread_replies(row['channel_name'], row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: cb.get_slack_user(x, slack_users))

        messages = messages[['thread_ts', 'ts', 'slack_user', 'text']]

        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]
    
        non_occ_replies_thread = messages[
            ~(messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]

        occ_replies_thread = occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_occ_reply=('ts', 'min'),
            last_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        non_occ_replies_thread = non_occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_non_occ_reply=('ts', 'min'),
            last_non_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        occ_replies = pd.concat([occ_replies, occ_replies_thread])
        non_occ_replies = pd.concat([non_occ_replies, non_occ_replies_thread])
    except:
        pass
    sleep(0.5)

workflows = workflows.merge(occ_replies, on='thread_ts', how='left')
workflows = workflows.merge(non_occ_replies, on='thread_ts', how='left')

workflows['reply_users'] = workflows['reply_users'].astype(str)
workflows['occ_users'] = workflows['occ_users'].astype(str)
workflows['non_occ_users'] = workflows['non_occ_users'].astype(str)
workflows['occ_reactions'] = workflows['occ_reactions'].astype(str)
workflows['non_occ_reactions'] = workflows['non_occ_reactions'].astype(str)

workflows = workflows.drop_duplicates()

snow.upload_df_occ(workflows,'cpgs_workflows')