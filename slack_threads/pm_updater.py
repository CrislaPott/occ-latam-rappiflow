import pandas as pd
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from lib import snowflake as snow


# autenticando conexão com GAPI
scope = [
    'https://www.googleapis.com/auth/drive',
    'https://www.googleapis.com/auth/drive.file'
    ]
file_name = 'client_key.json'
credentials = ServiceAccountCredentials.from_json_keyfile_name(file_name,scope)
client = gspread.authorize(credentials)


# leitura dos casos gerais
base_alarmes_general = snow.run_query("""with base as (SELECT country, alarm_type,
CASE
    WHEN COUNTRY = 'cl' then CONVERT_TIMEZONE( 'America/Santiago','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'cr' then CONVERT_TIMEZONE( 'America/Costa_Rica','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'co' then CONVERT_TIMEZONE( 'America/Bogota','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'ec' then CONVERT_TIMEZONE( 'America/Guayaquil','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'uy' then CONVERT_TIMEZONE( 'America/Montevideo','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'mx' then CONVERT_TIMEZONE( 'America/Mexico_City','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'pe' then CONVERT_TIMEZONE( 'America/Lima','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    ELSE alarm_at END as suspension_date
FROM OPS_OCC.ALARM_OVERVIEW
where suspension_date::date BETWEEN current_date()  - 15 AND current_date() - 1) 

select suspension_date::date as DATE, upper(country) as country, alarm_type, count(*) as alarms from base
group by 1,2,3
order by 1,2,3 asc""")



# leitura dos casos de high discounts
base_high_discounts = snow.run_query("""with base_brands as (SELECT country,
CASE
    WHEN COUNTRY = 'cl' then CONVERT_TIMEZONE( 'America/Santiago','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'cr' then CONVERT_TIMEZONE( 'America/Costa_Rica','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'co' then CONVERT_TIMEZONE( 'America/Bogota','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'ec' then CONVERT_TIMEZONE( 'America/Guayaquil','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'uy' then CONVERT_TIMEZONE( 'America/Montevideo','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'mx' then CONVERT_TIMEZONE( 'America/Mexico_City','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'pe' then CONVERT_TIMEZONE( 'America/Lima','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    ELSE alarm_at END as suspension_date
FROM OPS_OCC.high_discounts_brand
where suspension_date::date BETWEEN current_date()  - 15 AND current_date() - 1)


,base_stores as (SELECT country,
CASE
    WHEN COUNTRY = 'cl' then CONVERT_TIMEZONE( 'America/Santiago','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'cr' then CONVERT_TIMEZONE( 'America/Costa_Rica','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'co' then CONVERT_TIMEZONE( 'America/Bogota','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'ec' then CONVERT_TIMEZONE( 'America/Guayaquil','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'uy' then CONVERT_TIMEZONE( 'America/Montevideo','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'mx' then CONVERT_TIMEZONE( 'America/Mexico_City','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    WHEN COUNTRY = 'pe' then CONVERT_TIMEZONE( 'America/Lima','America/Buenos_Aires',  to_timestamp_ntz(alarm_at))
    ELSE alarm_at END as suspension_date
FROM OPS_OCC.high_discounts_physical
where suspension_date::date BETWEEN current_date()  - 15 AND current_date() - 1)



select suspension_date::date as DATE, upper(country) as country, count(*) as alarms from (select * from base_brands union all select * from base_stores)
group by 1,2
order by 1,2 asc""")


# Abrindo a SpreadSheet
sheet = client.open('Compilado - Alarmes')

# Acessando a primeira aba da spreadsheet
worksheet = sheet.get_worksheet(0)
worksheet.clear()


# Convertendo o campo para texto por compatibilidade
base_alarmes_general['date']= base_alarmes_general.apply(lambda x: x['date'].strftime("%m/%d/%Y"), axis =1)

# Inserindo dados da consulta na aba da spreadsheet
worksheet.update([base_alarmes_general.columns.values.tolist()] + base_alarmes_general.values.tolist())

# Acessando a segunda aba da spreadsheet
worksheet_2 = sheet.get_worksheet(1)
worksheet_2.clear()

# Convertendo o campo para texto por compatibilidade
base_high_discounts['date']= base_high_discounts.apply(lambda x: x['date'].strftime("%m/%d/%Y"), axis =1)

# Inserindo dados da consulta na aba da spreadsheet
worksheet_2.update([base_high_discounts.columns.values.tolist()] + base_high_discounts.values.tolist())