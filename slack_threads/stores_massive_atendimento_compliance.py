import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import sys
from lib import thread_listener as tl
from lib import chat_bots as cb
from time import sleep



latest = datetime.now()
oldest = latest - timedelta(hours=6)
channel_name = 'CS7UQAMLG'

all_chats =  pd.DataFrame()
channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)
all_chats = all_chats.append(channel_history)

try:
    all_chats['reactions'] = all_chats['reactions'].astype(str)
    all_chats = all_chats[(all_chats['reactions'].str.contains('store-closed')) & ~(all_chats['reactions'].str.contains('compliance_bot'))]
    all_chats['thread_link'] = all_chats.apply(lambda x: tl.get_thread_link(channel_name,x['ts'])['permalink'], axis = 1)
except:
    pass

if len(all_chats) > 0:
    alert = "Los siguientes hilos han sido marcados con tiendas apagadas: \n"
    count = 1
    for index, row in all_chats.iterrows():
        if count < len(all_chats):
            separator = ','
        else:
            separator = ''
        alert += "<{thread_link}|{count}>{sep}".format(thread_link = row['thread_link'], count = count, sep = separator)
        count += 1
    alert += '\n <@UNBG4SZU4> <@UNN5MKSS1> <@UQTV4BZ33> <@U013FBMCD8C>'

    opening_thread = tl.send_message('C01RHLNV336','Tiendas apagadas - Compliance')
    tl.send_reply('C01RHLNV336',alert,opening_thread['ts'])
else:
    alert = "No hay hilos que han sido marcados con tiendas apagadas"
    opening_thread = tl.send_message('C01RHLNV336','Tiendas apagadas - Compliance')
    tl.send_reply('C01RHLNV336',alert,opening_thread['ts'])