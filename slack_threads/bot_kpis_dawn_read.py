import pandas as pd
import numpy as np
from datetime import datetime, timedelta
from time import sleep
from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

slack_users = snow.run_query("""--no cache
                SELECT * FROM ops_occ.slack_users""")
slack_users['title'] = slack_users['title'].fillna('No_Title')

occ_users = slack_users[(slack_users['title'].str.contains("OCC"))]
# occ_users = occ_users[occ_users['slack_user'].isin(['pamela.rodrigues','leticia.silva','bpo-s.charryperez','bpo-f.gomez'])]

latest = datetime.now().replace(minute=0, hour=9, second=0, microsecond=0)
oldest = latest - timedelta(hours=12)
channel_name = ['C029MJCAMS6','C02L5JX5CT0', 'C02L9B59BQV', 'C02L9BG8Z9T', 'C02LCB2N2AE'] 

all_chats =  pd.DataFrame()
for i in channel_name:
    channel_history = tl.get_channel_history(
        channel = i,
        messages_per_page = 500,
        max_messages = 20000,
        oldest = oldest,
        latest = latest
    )
    try:
        channel_history['thread_link'] = channel_history.apply(lambda x: tl.get_thread_link(i,x['ts'])['permalink'], axis = 1)
    except:
        pass
    channel_history['channel_name'] = i
    all_chats = all_chats.append(channel_history)


messages = all_chats[(all_chats['user'] == 'U01FDE3RD7E')]

try:
    messages['occ_users'] = messages['reply_users'].apply(lambda x: cb.get_occ_users(x,slack_users,occ_users))
    messages['non_occ_users'] = messages['reply_users'].apply(lambda x: cb.get_non_occ_users(x,slack_users,occ_users))
except:
    pass

messages['country'] = messages['text'].apply(lambda x: cb.get_country(x))

try:
    messages['alarm_type'] = messages.apply(lambda x: cb.get_alert_type(x['text']), axis = 1)
except:
    pass

messages = messages[~messages['country'].isna()]

occ_replies = pd.DataFrame() 
for index, row in messages[messages['reply_count'] > 0].iterrows():
    thread_ts =  row['thread_ts']
    replies_details = pd.DataFrame(columns=['thread_ts','is_checked']) 
    try: 
        messages_actions = tl.get_thread_replies(row['channel_name'], row['thread_ts'])
        messages_actions['slack_user'] = messages_actions['user'].apply(lambda x: cb.get_slack_user(x, slack_users))
        
        messages_actions = messages_actions[['thread_ts', 'ts', 'slack_user', 'text']]
        
        occ_replies_thread = messages_actions[
            (messages_actions['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages_actions['slack_user'].notna()) & ~(messages_actions['slack_user'] == 'bot_occ')
        ]
        checked = 'no'
        try:
            for index, row in occ_replies_thread.iterrows():
                if 'diagnóstico' in row['text'].lower() or 'diagnostico' in row['text'].lower():
                    checked = 'yes'
        except:
            pass
        replies_details ={'thread_ts': [thread_ts],'is_checked':[checked]}
        occ_replies = pd.concat([occ_replies, pd.DataFrame(replies_details)])
    except Exception as e:
        print(e)
        pass
    sleep(0.2)

messages = messages.merge(occ_replies, on='thread_ts', how='left')
messages['is_checked'] = messages.apply(lambda x: x['is_checked'] if x['reply_count'] > 0 else 'no', axis = 1)
messages_actions = messages[messages['is_checked'] == 'no']


alert = ""
for countries in messages_actions['country'].unique():
    alert += "\n:flag-{country}:\n ".format(country = countries.lower())
    for types in messages_actions[messages_actions['country'] == countries]['alarm_type'].unique():
        count = 1
        alert += "{alarm_type}: ".format(alarm_type = types)
        for threads in messages_actions[(messages_actions['country'] == countries) & (messages_actions['alarm_type'] == types)]['thread_link']:
            if count < len(messages_actions[(messages_actions['country'] == countries) & (messages_actions['alarm_type'] == types)]['thread_link']):
                separator = ','
            else:
                separator = '\n'
            alert += "<{thread_link}|{count}>{sep}".format(thread_link = threads, count = count, sep = separator)
            count += 1
alert += '\n <@UQTV4BZ33> <@U013FBMCD8C> <@UNBG4SZU4> <@UNN5MKSS1>'

alert =  cb.fixing_messages_breaks(alert)

opening_thread = tl.send_message('C01RHLNV336','Compliance Alarmas')
tl.send_reply('C01RHLNV336',alert,opening_thread['ts'])