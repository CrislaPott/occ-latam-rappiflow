import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

slack_users = snow.run_query("""--no cache
                SELECT * FROM ops_occ.slack_users""")

base_messages = snow.run_query("""--no cache
                SELECT *
                FROM ops_occ.slack_workflows
                WHERE IS_CLOSED = False
                AND WORKFLOW not ilike '%operations%' 
                AND WORKFLOW not ilike '%comunicado%'
                AND to_timestamp_ntz(TO_NUMBER(thread_ts))::DATE >= CURRENT_DATE()-14""")

slack_users['title'] = slack_users['title'].fillna('No_Title')

occ_users = slack_users[(slack_users['title'].str.contains("OCC"))]
kpis_users = slack_users[(slack_users['title'] == 'Monitoring Analyst | OCC') | (slack_users['slack_user'].isin(['sefhora.maciel','andrea.gonzalez','higor.silva']))]

base_messages = base_messages.sort_values('update_at').groupby('thread_ts').tail(1)
base_messages = base_messages[base_messages['is_closed'] == False].reset_index(drop = True)
base_messages = base_messages.drop(columns=['is_closed','last_non_occ_reply','last_occ_reply', 'occ_reactions','non_occ_reactions','suspension'])


channel_name = 'CS7UQAMLG'
occ_replies = pd.DataFrame() 
non_occ_replies =  pd.DataFrame()
new_status = pd.DataFrame()
users_non_occ_reacted =  pd.DataFrame()
users_occ_reacted = pd.DataFrame()
suspended =  pd.DataFrame()
for index, row in base_messages.iterrows():
    try: 
        messages = tl.get_thread_replies(channel_name, row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: cb.get_slack_user(x, slack_users))
        messages['is_closed'] = messages['reactions'].apply(lambda x: cb.is_closed(x))
        messages['users_reacted'] = messages.apply(lambda x: cb.get_users_reacted(x['reactions']), axis =1 )
        messages['occ_reactions'] = messages['users_reacted'].apply(lambda x: cb.get_occ_users(x, slack_users, occ_users))
        messages['non_occ_reactions'] = messages['users_reacted'].apply(lambda x: cb.get_non_occ_users(x, slack_users, occ_users))
        messages['suspension'] = messages.apply(lambda x: cb.get_suspended(x['text'], x['user'], kpis_users), axis = 1)
        messages = messages[['thread_ts', 'ts', 'is_closed', 'slack_user', 'text', 'non_occ_reactions', 'occ_reactions','suspension']]
        current_status = messages[['thread_ts', 'is_closed']].iloc[[0]]
        occ_reacted =  messages[['thread_ts', 'occ_reactions']].iloc[[0]]
        non_occ_reacted =  messages[['thread_ts', 'non_occ_reactions']].iloc[[0]]
        suspensions = messages.groupby('thread_ts').agg({'suspension':'sum'})
        suspended = pd.concat([suspended, suspensions])

        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]
    
        non_occ_replies_thread = messages[
            ~(messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]

        occ_replies_thread = occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            last_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        non_occ_replies_thread = non_occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            last_non_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        occ_replies = pd.concat([occ_replies, occ_replies_thread])
        non_occ_replies = pd.concat([non_occ_replies, non_occ_replies_thread])
        new_status = pd.concat([new_status,current_status])
        users_non_occ_reacted =  pd.concat([users_non_occ_reacted, non_occ_reacted])
        users_occ_reacted = pd.concat([users_occ_reacted, occ_reacted])
    except:
        pass


base_messages = base_messages.merge(occ_replies, on='thread_ts', how='left')
base_messages = base_messages.merge(non_occ_replies, on='thread_ts', how='left')
base_messages = base_messages.merge(new_status, on='thread_ts', how='left')
base_messages = base_messages.merge(users_non_occ_reacted, on='thread_ts', how='left')
base_messages = base_messages.merge(users_occ_reacted, on='thread_ts', how='left')
base_messages = base_messages.merge(suspended, on='thread_ts', how='left')

base_messages['reply_users'] = base_messages['reply_users'].astype(str)
base_messages['occ_users'] = base_messages['occ_users'].astype(str)
base_messages['non_occ_users'] = base_messages['non_occ_users'].astype(str)
base_messages['occ_reactions'] = base_messages['occ_reactions'].astype(str)
base_messages['non_occ_reactions'] = base_messages['non_occ_reactions'].astype(str)

base_messages['is_closed'] = base_messages['is_closed'].fillna(False)

base_messages['update_at'] = str((datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)).timestamp())

base_messages = base_messages[['workflow', 'thread_ts', 'text', 'reply_users', 'occ_users', 'non_occ_users', 'country', 'vertical', 'alarm_type', 'is_closed', 'first_occ_reply', 'last_occ_reply', 'suspension', 'first_non_occ_reply', 'last_non_occ_reply', 'thread_link', 'update_at', 'occ_reactions', 'non_occ_reactions']]

snow.upload_df_occ(base_messages, 'slack_workflows')

latest_update =  base_messages[(base_messages['is_closed'] == False) & ~(base_messages['workflow'].isin(['occ_workflow_warning','OCC - Operations Control Center_pm', 'OCC - Operations Control Center', 'OCC Report - Daily Summary', 'OCC Comunicado - Daily CPGS']))].sort_values(by='thread_ts', ascending = True).reset_index(drop=True)
latest_update['thread_ts'] = latest_update['thread_ts'].apply(lambda x: datetime.fromtimestamp(float(x)).date())
latest_update = latest_update[(latest_update['thread_ts'] == datetime.now().date() - timedelta(days=11))]


alert = "Los siguientes hilos han sido creados hace 11 días y siguen abiertos: "
count = 1 
for index, row in latest_update.iterrows():
    if count % 30 == 0:
        separator = '\n'
    elif count < len(latest_update): 
        separator = ', '
    else:
        separator = '\n <@U013FBMCD8C> <@UNBG4SZU4> <@UNN5MKSS1>'
    alert += "<{thread_link}|{count}>{sep}".format(thread_link = row['thread_link'], count = count, sep = separator)
    count += 1
        
opening_thread = tl.send_message('C01RHLNV336','OCC Atendimento - Summary')
tl.send_reply('C01RHLNV336',alert,opening_thread['ts'])