import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import thread_listener as tl
from lib import chat_bots as cb
from lib import snowflake as snow

slack_users = snow.run_query("""--no cache
                SELECT * FROM ops_occ.slack_users""")

slack_users['title'] = slack_users['title'].fillna('No_Title')

occ_users = slack_users[(slack_users['title'].str.contains("OCC"))]
kpis_users = slack_users[(slack_users['title'] == 'Monitoring Analyst | OCC') | (slack_users['slack_user'].isin(['sefhora.maciel','andrea.gonzalez','higor.silva']))]

latest = datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
oldest = latest - timedelta(days=1)
channel_name = 'CS7UQAMLG'

channel_history = tl.get_channel_history(
    channel = channel_name,
    messages_per_page = 500,
    max_messages = 20000,
    oldest = oldest,
    latest = latest
)

channel_history['username'] = channel_history.apply(lambda x: "Alarma de Dispersion" if ('Alarma de Dispersion' in x['text'] and x['username'] != x['username'])  else x['username'], axis = 1 )
channel_history['subtype'] = channel_history.apply(lambda x: 'bot_message' if x['username'] == 'Alarma de Dispersion' and x['subtype'] != x['subtype'] else x['subtype'], axis = 1)

workflows = channel_history[
    (channel_history['username'].notna()) &
    (channel_history['subtype'] == 'bot_message')
]


if 'reactions' not in workflows:
    workflows['reactions'] = None
    workflows['reactions'] = workflows['reactions'].astype(str)

workflows['users_reacted'] = workflows.apply(lambda x: cb.get_users_reacted(x['reactions']), axis =1 )
workflows['is_closed'] = workflows.apply(lambda x: cb.is_closed(x['reactions']), axis = 1)


workflows['thread_ts'] = workflows['ts']
workflows['occ_users'] = workflows['reply_users'].apply(lambda x: cb.get_occ_users(x, slack_users, occ_users))
workflows['non_occ_users'] = workflows['reply_users'].apply(lambda x: cb.get_non_occ_users(x, slack_users, occ_users))
workflows['country'] = workflows['text'].apply(lambda x: cb.get_country(x))
workflows['vertical'] = workflows['text'].apply(lambda x: cb.get_vertical(x))
workflows['alarm_type'] = workflows['text'].apply(lambda x: cb.get_alert_type(x))
workflows['occ_reactions'] =  workflows['users_reacted'].apply(lambda x: cb.get_occ_users(x, slack_users, occ_users))
workflows['non_occ_reactions'] =  workflows['users_reacted'].apply(lambda x: cb.get_non_occ_users(x, slack_users, occ_users))



workflows.rename(columns={
    'username': 'workflow'
}, inplace=True)



workflows = workflows[['workflow', 'thread_ts', 'text', 'reply_users', 'occ_users', 'non_occ_users', 'country', 'vertical','alarm_type', 'occ_reactions', 'non_occ_reactions', 'is_closed']]



occ_replies = pd.DataFrame() 
non_occ_replies =  pd.DataFrame()
suspended =  pd.DataFrame()
for index, row in workflows.iterrows():
    try: 
        messages = tl.get_thread_replies(channel_name, row['thread_ts'])
        messages['slack_user'] = messages['user'].apply(lambda x: cb.get_slack_user(x, slack_users))
        messages['suspension'] = messages.apply(lambda x: cb.get_suspended(x['text'], x['user'], kpis_users), axis = 1)
        suspensions = messages.groupby('thread_ts').agg({'suspension':'sum'})
        suspended = pd.concat([suspended, suspensions])

        messages = messages[['thread_ts', 'ts', 'slack_user', 'text']]

        occ_replies_thread = messages[
            (messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]
    
        non_occ_replies_thread = messages[
            ~(messages['slack_user'].isin(occ_users['slack_user'].unique().tolist())) &
            (messages['slack_user'].notna()) & ~(messages['slack_user'] == 'bot_occ')
        ]

        occ_replies_thread = occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_occ_reply=('ts', 'min'),
            last_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        non_occ_replies_thread = non_occ_replies_thread.groupby([
            'thread_ts'
        ]).agg(
            first_non_occ_reply=('ts', 'min'),
            last_non_occ_reply=('ts', 'max')
        ).reset_index().sort_values(by='thread_ts', ascending=False)

        occ_replies = pd.concat([occ_replies, occ_replies_thread])
        non_occ_replies = pd.concat([non_occ_replies, non_occ_replies_thread])
    except:
        pass


workflows = workflows.merge(occ_replies, on='thread_ts', how='left')
workflows = workflows.merge(suspended, on='thread_ts', how='left')
workflows = workflows.merge(non_occ_replies, on='thread_ts', how='left')


workflows['reply_users'] = workflows['reply_users'].astype(str)
workflows['occ_users'] = workflows['occ_users'].astype(str)
workflows['non_occ_users'] = workflows['non_occ_users'].astype(str)
workflows['occ_reactions'] = workflows['occ_reactions'].astype(str)
workflows['non_occ_reactions'] = workflows['non_occ_reactions'].astype(str)


workflows['thread_link'] =  workflows.apply(lambda x: tl.get_thread_link(channel_name,x['thread_ts'])['permalink'], axis = 1)

workflows['update_at'] = str((datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)).timestamp())

snow.upload_df_occ(workflows, 'slack_workflows')