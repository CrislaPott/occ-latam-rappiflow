import pandas as pd 
from os.path import expanduser
from openpyxl import load_workbook
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones




def remove_verticals(orders, db_id,country):
       timezone, timeframe = timezones.country_timezones(country)
       if not orders.empty:
              order_list = ", ".join(map(str, orders['order_id'].to_list()))
              get_vertical_query = """ SET TIMEZONE TO 'America/{timezone}';
              select  a.order_id, b.group as vertical, a.type as order_type,c.state as is_closed_order, 
              case when place_at is not null and place_at <= now() then 'yes'
                     when place_at is null then 'yes'
                     else 'no' END AS is_past_placed_at from order_stores as a
                     join verticals as b
                     on a.store_type_group = b.store_type
                     join orders as c
                     on a.order_id = c.id
              where order_id in ({order_list})
              and state not ilike '%cancel%'
              and state not in ('finished','pending_review')
              """.format(order_list=order_list,timezone=timezone)
              verticals = redash.run_query(db_id,get_vertical_query)
              orders = orders.merge(verticals, on='order_id')
              orders = orders[(orders['vertical'] != "Restaurantes") & (orders['vertical'] != "Rappifavor") & (orders['order_type'] != "queue_order_flow") & (orders['is_past_placed_at'] != 'no')]
              orders.drop(columns=['vertical','order_type','is_closed_order','is_past_placed_at'], inplace = True)
       return orders


limbo = """--no_cache
    BEGIN;
    SET LOCAL timezone = 'UTC';
    
    with potential_limbo_orders as (
    SELECT   distinct on (a.order_id) a.order_id,
                         a.id AS latest_event,
                         a.name as last_event_name
                FROM     order_events a
                WHERE  created_at >= now() - interval  '4 hours'
                ORDER BY 1,2 DESC
    )
    
    ,checking_orders as (select *  from potential_limbo_orders
    where last_event_name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') )
    
    ,release_to_shopper as (
    select A.order_id,
       min(case when name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') then (created_at) else null end) as first_error,
       max(case when name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') then (created_at) else null end) as latest_error,
       count(distinct case when name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') then created_at else null end) as errors
       from order_events a 
       LEFT JOIN checking_orders plo 
        on a.order_id = plo.order_id
    where created_at >= now() - interval  '6 hours'
    and name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper')
    group by 1
    )


    , base as (select rts.order_id, 
                      physical_store_id, 
                      first_error, latest_error, 
                      state,errors,
                      extract(epoch from now() - first_error)/60 as time_since_first_error,
                      extract(epoch from now() - latest_error)/60 as time_since_last_error,
                      extract(epoch from latest_error - first_error)/60 as time_between_first_last
               from release_to_shopper rts
               inner join order_summary os on os.order_id=rts.order_id
               where os.state not ilike '%cancel%'
               and os.state not in ('finished','pending_review')
               and errors is not null
               and os.store_type_store not in ('rappi_pay','soat_webview')
               )

                    
 ,alarm as (SELECT a.*, plo.latest_event, plo.last_event_name, 
 CASE WHEN LAST_EVENT_NAME IN ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') AND (TIME_BETWEEN_FIRST_LAST >= 25 OR TIME_SINCE_LAST_ERROR >= 25 OR ERRORS > 10) THEN 'YES'
      ELSE 'NO'
      END AS TRIGGERED
 FROM BASE A 
 JOIN checking_orders plo 
 on a.order_id = plo.order_id)
 

select *  from alarm
where TRIGGERED = 'YES'
"""
final = pd.DataFrame()

try:
       br = redash.run_query(2447,limbo)
       if not br.empty:
              br['country'] = 'br'
              br = remove_verticals(br,1338,'br')
       final = final.append(br)
       print('BR done') 
except:
       pass   
try: 
       ar = redash.run_query(2430,limbo)
       if not ar.empty:
              ar['country'] = 'ar'
              ar = remove_verticals(ar,1337,'ar')
       final = final.append(ar)
       print('AR done')
except Exception as e:
       print(e)
       pass
try:
       cr = redash.run_query(2552,limbo)
       if not cr.empty:
              cr['country'] = 'cr'
              cr = remove_verticals(cr,1921,'cr')
       final = final.append(cr)
       print('CR done')
except:
       pass
try:
       co = redash.run_query(2449,limbo)
       if not co.empty:
              co['country'] = 'co'
              co = remove_verticals(co,1904,'co')
       final = final.append(co)
       print('CO done')
except Exception as e:
       print(e)
       pass
try:
       cl = redash.run_query(2431,limbo)
       if not cl.empty:
              cl['country'] = 'cl'
              cl = remove_verticals(cl,1155,'cl')
       final = final.append(cl)
       print('CL done')
except:
       pass
try:
       ec = redash.run_query(2559,limbo)
       if not ec.empty:
              ec['country'] = 'ec'
              ec = remove_verticals(ec,1922,'ec')
       final = final.append(ec)
       print('EC done')
except:
       pass
try:
       mx = redash.run_query(2450,limbo)
       if not mx.empty:
              mx['country'] = 'mx'
              mx = remove_verticals(mx,1371,'mx')
       final = final.append(mx)
       print('MX done')
except Exception as e:
       print(e)
       pass
try:
       pe = redash.run_query(2448,limbo)
       if not pe.empty:
              pe['country'] = 'pe'
              pe = remove_verticals(pe,1157,'pe')
       final = final.append(pe)
       print('PE done')
except Exception as e:
       print(e)
       pass
try:
       uy = redash.run_query(2426,limbo)
       if not uy.empty:
              uy['country'] = 'uy'
              uy = remove_verticals(uy,1156,'uy')
       final = final.append(uy)
       print('UY done')
except:
       pass


if len(final) > 0:
       home = expanduser("~")
       results_file = '{}/limbo_teste.xlsx'.format(home)

       final = final[['country','order_id','physical_store_id','first_error','latest_error','state','errors','time_since_first_error','time_since_last_error','time_between_first_last','last_event_name','triggered']]
       current_time = timezones.country_current_time('co')
       to_upload = final.copy()
       to_upload['alarm_at'] = current_time
       print(to_upload)
       snow.upload_df_occ(to_upload,'limbo_assign_to_shopper_teste')


       final.to_excel(results_file, sheet_name = "LIMBO ORDERS", index=False)
       book = load_workbook(results_file)
       writer = pd.ExcelWriter(results_file, engine='openpyxl')
       writer.book = book
       writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
       # now.to_excel(writer, sheet_name = "NOW", index=False)
       writer.save()
       writer.close()

       slack.file_upload_channel('C036PQNDQ1E',':alert: *TESTE DE LIMBO - SHOPPER* :alert:\n\n :warning: As seguintes orders estão com problema de assignação de shopper.',results_file,'xlsx')
else:
       print("no data")