import pandas as pd
from lib import thread_listener as tl
from lib import snowflake as snow

user_list =  pd.DataFrame(columns=['slack_user_id','slack_user','deactivated','title'])
all_users  = tl.get_user_list()

for i in range(0, len(all_users)-4):
    temp = {'slack_user_id':[all_users[i]['id']],'slack_user':[all_users[i]['name']], 'deactivated':[all_users[i]['deleted']], 'title':[all_users[i]['profile']['title']]}
    user_list = user_list.append(pd.DataFrame(temp))


snow.truncate_table_occ('slack_users')
snow.upload_df_occ(user_list, 'slack_users') 