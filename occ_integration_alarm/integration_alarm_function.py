import pandas as pd
import datetime as dt
import numpy as np
import pytz
from lib import snowflake as snow
from lib import slack, redash
from datetime import datetime, timedelta
from functions import timezones

def process(country):

    if country=='BR':
        country = 'BR'
        country_min = 'br'
        BD = 6310
        timezone = 'Sao_Paulo'
        flag = '\U0001F1E7\U0001F1F7'

    elif country=='MX':
        country = 'MX'
        country_min = 'mx'
        BD = 6324
        timezone = 'Mexico_City'
        flag = '\U0001F1F2\U0001F1FD'

    elif country=='CO':
        country = 'CO'
        country_min = 'co'
        BD = 6312
        timezone = 'Bogota'
        flag = '\U0001F1E8\U0001F1F4'

    elif country=='AR':
        country = 'AR'
        country_min = 'ar'
        BD = 6309
        timezone = 'Buenos_Aires'
        flag = '\U0001F1E6\U0001F1F7'


    elif country=='CL':
        country = 'CL'
        country_min = 'cl'
        BD = 6311
        timezone = 'Santiago'
        flag = '\U0001F1E8\U0001F1F1'


    elif country=='PE':
        country = 'PE'
        country_min = 'pe'
        BD = 6323
        timezone = 'Lima'
        flag = '\U0001F1F5\U0001F1EA'

    elif country=='UY':
        country = 'UY'
        country_min = 'uy'
        BD = 6322
        timezone = 'Montevideo'
        flag = '\U0001F1FA\U0001F1FE'


    elif country=='EC':
        country = 'EC'
        country_min = 'ec'
        BD = 6447 
        timezone = 'Guayaquil'
        flag = '\U0001F1EA\U0001F1E8'

    elif country=='CR':
        country = 'CR'
        country_min = 'cr'
        BD = 6313
        timezone = 'Costa_Rica' 
        flag = '\U0001F1E8\U0001F1F7'

    
    ### Total stores exclude -> lojas que estão suspendidas, desligasdas para depois retirá-las da análise

    # Stores que han sido borradas y apagadas
    core_stores = redash.run_query(BD,
    """select distinct store_id, name, type, is_enabled, suggested_state, created_at, updated_at 
    from stores 
    where deleted_at is not null
    and is_enabled = False
    """)
    print(core_stores)

    # Tiendas que estan suspendidas
    store_atributes = redash.run_query(BD,
    """select store_id as storeid
    from store_attribute
    where suspended=true
    group by 1
    """)

    # Me quedo con las tiendas borradas, apagadas y suspendidas
    if (len(store_atributes)>0):

        exclude_stores = pd.merge(core_stores, store_atributes, how='left', left_on=core_stores['store_id'], right_on=store_atributes['storeid'])
        exclude_stores = exclude_stores[~exclude_stores['storeid'].isnull()]  # junto os dois dataframes core_stores e store_atributes e fico só com o que está suspenso - excluo as lojas suspensas. ~ significa negação (seria um notnull)
        exclude_stores = exclude_stores.drop(['key_0', 'storeid'], axis=1)
        
    else: 
        
        exclude_stores = core_stores[['store_id']]

    exclude_stores = exclude_stores.rename(columns={'store_id':'storeid'})    # só mudança do nome da coluna store_id para storeid
    print(exclude_stores)

    ### Total Integrations

    # dataframe brands tem store_id pai e filha

    brands =  snow.run_query("""select distinct s.store_id, 
    brand_group_id as brandgroupid, 
    super_store_id, 
    case when super_store_id is null then 'pai' else 'filha' end as pai_filha 
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    inner join (
    select brand_group_id, brand_group_name, store_brand_id
    from
        (select store_brand_id, 
        last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
        last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
        from {country_min}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
        join {country_min}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
    group by 1,2,3) bg 
    on bg.store_brand_id = s.store_brand_id
    """.format(country=country, country_min=country_min))

    #
    map_integration =  snow.run_query("""select distinct
    country, 
    integration_id,
    brand_group_id, 
    integration_name
    from GPS_ANALYTICS.VW_INTEGRATION_MAPPING 
    where country='{country_min}'
    """.format(country_min=country_min))

    # Join map_integration y brands para traerme los store_ids relacioandos a cada integration_id -> aqui traz lojas pai e lojas filhas
    all_integrations = pd.merge(map_integration, brands,left_on=map_integration["brand_group_id"], right_on=brands["brandgroupid"], how='left',indicator=True)
    all_integrations = all_integrations.drop(['key_0', 'brandgroupid','_merge'], axis=1)
    print(all_integrations)

    ### Active integrations

    active_integrations =  snow.run_query("""
    select 
    distinct(i.id),
    i.country, 
    i.integration_name, 
    i.integration_type,
    im.tam_assigned_email
    from CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations i
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations_metadata im on im.id_integration = i.id
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
    where i.run_mode = 'worker'
    and i.integration_type != 'bundle'
    and i.country = '{country_min}'
    and i.id not in 
        (
        select i.id from CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations i
        left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
        left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
        where t.name = 'Deactivated'
        and i.country= '{country_min}'
        )
    union
    select 
    distinct(i.id),
    i.country, 
    i.integration_name, 
    i.integration_type,
    im.tam_assigned_email
    from CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations i
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_versions iv on iv.id_integration = i.id and iv.current_version = true
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations_metadata im on im.id_integration = i.id
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
    left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
    where iv.is_enabled = TRUE 
    and i.run_mode = 'astronomer'
    and i.integration_type != 'bundle'
    and i.country = '{country_min}'
    and i.id not in 
        (
        select i.id from CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integrations i
        left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
        left join CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
        where t.name = 'Deactivated'
        and i.country = '{country_min}'
        )
    """.format(country_min=country_min))

    ## Join all integration with active integrations
    act_integrations = pd.merge(active_integrations[['id']], all_integrations,
                        left_on=["id"], right_on=["integration_id"], how='inner').drop(columns=['id'])
    print(act_integrations)

    ### Integration Type

    type_i =  snow.run_query("""select distinct
    id,
    integration_name,
    integration_type
    from CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS
    where country='{country_min}'
    """.format(country_min=country_min))

    print('integrations')
    ## Join all active integrations with type integrations
    integrations = pd.merge(act_integrations, type_i[['id','integration_type']],
                        left_on=["integration_id"], right_on=["id"], how='left').drop(columns=['id'])
    print(integrations)

    ## Only Full integrations
    # integrations = integrations.loc[integrations['integration_type'] == 'full']

    ## Exclude stores 
    total_integrations = pd.merge(integrations, exclude_stores[['storeid']],left_on=["store_id"], right_on=["storeid"], how='left')
    total_integrations = total_integrations[total_integrations['storeid'].isnull()]

    total_integrations = total_integrations[['country', 'store_id', 'integration_id', 'integration_name',  'brand_group_id', 'integration_type']]

    # Count store_ids- Hago un group by para sumarizar los store_ids por integration_id
    total_integrations['count_total'] = total_integrations.groupby(['integration_id'])['store_id'].transform('count')

    print(total_integrations)

    ### Last integration query
    # last_integration pega a última integração
    last_integration = snow.run_query("""
    SELECT
        RAPPI_STORE_ID, b.store_id as store_id_filha, case when b.store_id is null then RAPPI_STORE_ID::int else b.store_id::int end as pai_filha,
        case when b.store_id is null then 'pai' else 'filha' end as pai_filha_text,
        MAX(LAST_INTEGRATION) AS LAST_INTEGRATION
        FROM
            (
            SELECT DISTINCT RAPPI_STORE_ID,
            MAX(CONVERT_TIMEZONE('UTC','America/Bogota',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
            FROM CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
            WHERE STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
            GROUP BY 1
            UNION ALL
            SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID,
            MAX(CONVERT_TIMEZONE('UTC','America/Bogota',EXECUTION_DATE)) AS LAST_INTEGRATION
            FROM CPGS_DATASCIENCE.{country}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
            WHERE try_to_date(execution_date)::DATE >= CURRENT_DATE - 7
            GROUP BY 1
            ) ile
        left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw b on b.super_store_id=ile.RAPPI_STORE_ID and b.deleted_at is null and not coalesce (b._fivetran_deleted, false)
        GROUP BY 1,2
        """.format(country=country, timezone=timezone))

    # Current date by timezone
    import datetime
    la = pytz.timezone("America/Bogota")
    now_time = datetime.datetime.now(la)

    # Create column with current date
    last_integration['now'] = now_time.strftime('%Y-%m-%d %H:%M:%S')
    last_integration['now'] = pd.to_datetime(last_integration['now'])

    # Ignore errors last integration column
    last_integration['last_integration'] = pd.to_datetime(last_integration['last_integration'], errors='coerce')

    #last_integration.loc[(last_integration['last_integration'] > last_integration['now']), 'last_integration'] = (
    #            last_integration['now'] - timedelta(minutes=5))

    print('last_integration')
    print(last_integration)

    ### Stores have Integrations

    non_i =  snow.run_query("""
    SELECT
    distinct i.country,
    s.store_id,
    i.id as integration_id,
    i.integration_name
    FROM
        (
        SELECT * FROM 
            (
            SELECT DISTINCT RAPPI_STORE_ID, 
            DELETED_AT,
            _FIVETRAN_DELETED, 
            DATASOURCE_ID,
            DENSE_RANK() OVER (PARTITION BY RAPPI_STORE_ID ORDER BY ID DESC) AS RANK
            FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE
            )
        WHERE RANK = 1
        ) nds
    JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE d ON nds.datasource_id = d.id
    JOIN (
    SELECT *, DENSE_RANK() OVER (PARTITION BY COUNTRY, DATASOURCE ORDER BY ID DESC) AS RANK
    FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS
    WHERE INTEGRATION_NAME not ilike '%bundle%'
    AND RUN_MODE = 'worker'
    --and has_spin_off = false
    ) i 
    ON i.datasource = d.name and i.country = d.country and i.rank = 1
    LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS_METADATA im ON i.id = im.id_integration 
    LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATION_VERSIONS iv ON iv.id_integration = i.id
    JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s ON s.store_id = nds.rappi_store_id
    LEFT JOIN 
        (
        SELECT ID, 
        ID_INTEGRATION, 
        ID_TAG, 
        DENSE_RANK() OVER (PARTITION BY ID_INTEGRATION ORDER BY ID DESC) AS RANK 
        FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags
        where _fivetran_deleted = false
        ) it 
    on it.id_integration = i.id and it.rank = 1
    LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
    WHERE
    coalesce(nds._fivetran_deleted,false) = false
    AND coalesce(d._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false 
    AND nds.deleted_at is null AND d.deleted_at is null AND s.deleted_at is null
    AND d.country = '{country_min}'
    AND i.country = '{country_min}'
    AND ((i.run_mode = 'worker') or (i.run_mode = 'astronomer'and iv.is_enabled = 'true'))
    AND t.name not in ('Deactivated', 'In guarantee')
    AND s.super_store_id is null
    """.format(country=country, country_min=country_min))

    ## Join all non integrations with type integrations
    non_integrations = pd.merge(non_i, type_i[['id','integration_type']],left_on=["integration_id"], right_on=["id"], how='left').drop(columns='id')

    print('parte de integração')
    ## Exclude stores

    non_integrations = pd.merge(non_integrations[['country', 'store_id', 'integration_id', 'integration_name']],
                                exclude_stores[['storeid']],left_on=["store_id"], right_on=["storeid"], how='left')
    non_integrations = non_integrations[non_integrations['storeid'].isnull()]

    non_integrations = non_integrations[['country','store_id', 'integration_id', 'integration_name']]

    # Join entre la totalidad de integraciones y las no integraciones, juntando el count de total de integraciones 
    total_integrations = pd.merge(non_integrations[['store_id', 'integration_id']], total_integrations[['country', 'store_id', 'count_total', 'integration_id', 'integration_name']],
                        left_on=['store_id', 'integration_id'], right_on=['store_id', 'integration_id'], how='right', indicator=True)

    #non_integrations.loc[non_integrations['_merge'] != 'both', 'sin_int'] = True

    ### acho que tenho que pegar both ou right_only

    total_integrations = total_integrations.drop('_merge', 1)
    ## Calculo la cantidad de tiendas sin integrar
    #non_integrations = non_integrations.loc[non_integrations['sin_int'] == True]

    total_integrations['store_id'] = total_integrations['store_id'].astype(str)
    last_integration['pai_filha'] = last_integration['pai_filha'].astype(str)

    df = pd.merge(total_integrations, last_integration, left_on=total_integrations["store_id"],right_on=last_integration["pai_filha"], how='left')

    df = df.drop(['key_0'], axis=1)
    df['now'] = now_time.strftime('%Y-%m-%d %H:%M:%S')
    df['now'] = pd.to_datetime(df['now'])
    print(df.sort_values('last_integration'))

    ### Excel

    ## excel for count store_ids

    excel1 = snow.run_query("""
    select 
    country, 
    integration_id, 
    integration_name, 
    integration_type, 
    check_frequency,
    stores_threshold,
    checkin_end as check_end, 
    checking_start as check_start,
    weekdays_to_check,
    poc
    from google_sheets.integration_alert_settings
    where country='{country_min}'
    and integration_id in ('801', '277', '265', '338', '888', '1262', '903')
    """.format(country_min= country_min))
    import datetime
    la = pytz.timezone("America/Bogota")
    now_time = datetime.datetime.now(la)
    now_time = now_time - timedelta(hours=2)

    now_time_min = (now_time - timedelta(hours=1)).replace(tzinfo=None)
    now_time_max = (now_time + timedelta(hours=1)).replace(tzinfo=None)

    excel1['date'] = now_time.strftime('%Y-%m-%d')
    excel1['check_start'] = excel1['date'] + ' ' + excel1['check_start']
    excel1['check_end'] = excel1['date'] + ' ' + excel1['check_end']
    excel1['check_end'] = pd.to_datetime(excel1['check_end'], errors='coerce')
    excel1['check_start'] = pd.to_datetime(excel1['check_start'], errors='coerce')
    excel1['day_week'] = str(now_time.weekday())
    excel1['weekdays_to_check'] = excel1['weekdays_to_check'].str.replace('Mon', '0').str.replace('Wed','2').str.replace('Fri', '4')
    excel1['weekdays_to_check'] = excel1['weekdays_to_check'].fillna('0,1,2,3,4,5,6').str.split(',')
    excel1.loc[(excel1['check_end'] - excel1['check_start']) < pd.Timedelta(0, 'D'), 'check_end'] = excel1['check_end'] + timedelta(days=1)
    excel1['check_frequency'] = pd.to_timedelta(excel1['check_frequency'].astype(str) + ':00:00')
    excel1['check_delta1'] = (excel1['check_end'] - excel1['check_start'])
    excel1['check_delta'] = ((excel1['check_end'] - excel1['check_start']) / excel1['check_frequency'])
    excel1['check_delta2'] = excel1['check_delta'].astype(int)
    excel1.loc[(excel1['check_delta'] / excel1['check_delta2'] != 1), 'check_delta2'] = excel1['check_delta2'] + 1
    excel1['check_delta2'] = excel1['check_delta2'] + 1

    now_time = now_time.replace(tzinfo=None)
    df_final1 = []
    for index, row in excel1.iterrows():
        if row['day_week'] in row['weekdays_to_check']:
            for i in range(1, row['check_delta2']):
                print(row['integration_id'])
                horario = row['check_start'] + row['check_frequency'] * i
                horario_min = horario - timedelta(minutes=30)
                horario_max = horario + timedelta(hours=1)
                if horario_max >= now_time >= horario_min:
                    df1 = df[df['integration_id'] == row['integration_id']]
                    df1 = pd.merge(df1, excel1, on=['integration_id', 'integration_name', 'country'], how='inner')
                    df1 = df1.loc[df1['last_integration'] < (df1['now'] - timedelta(hours=1, minutes=30))]
                    if (df1['now'] > df1['check_start']).any() & (df1['now'] < df1['check_end']).any():
                        if not df1.empty:
                            if ((df1['last_integration'] < now_time_min).any() | df1['last_integration'].isna().any()):
                                df1 = df1.loc[
                                    (df1['last_integration'] < now_time_min) | (df1['last_integration'].isna())]
                                if not df1.empty:
                                    df1['count'] = df1.groupby(
                                        ['country', 'integration_id', 'integration_name', 'count_total',
                                         'stores_threshold',
                                         'check_start', 'check_end', 'poc'])['store_id'].transform('count')
                                    df1['variation'] = df1['count'] * 100 / df1['count_total']
                                    print(df1)
                                    df1 = df1.loc[df1['variation'] >= df1['stores_threshold']]
                                    df1 = df1.groupby(
                                        ['country', 'integration_id', 'integration_name', 'check_frequency',
                                         'check_start', 'check_end', 'variation', 'poc'])['rappi_store_id'].apply(
                                        list).reset_index(name='store_ids')
                                    df1['store_ids'] = df1['store_ids'].apply(
                                        lambda x: [i for i in x if str(i) != "nan"]).astype(str)

                                    df_final1.append(df1)
                                else:
                                    print('df1 empty')
                else:
                    print('fora do horário')
            try:
                df_final = pd.concat(df_final1, ignore_index=True)
            except:
                df_final = pd.DataFrame()
            df_final = df_final.drop_duplicates()
            print(df_final)

    if not df_final.empty:

        for index, row in df_final.iterrows():
            if row['store_ids'] != '[]':
                main_text = '''
                      \U000026A0 *Integration Alarm* \U0001F6A8: 
                      Pais: {flag}
                      El integration id {integration_id} alcanzo *{variation:.1f}%* de tiendas sin integrar en el intervalo de tiempo de {check_start} a {check_end}.
                      Check Frequency: {check_frequency}
                      Integration name: {integration_name}
                      store_ids: {store_ids}
                      Poc: <@{poc}>
                      '''.format(
                    flag=flag,
                    integration_id=row['integration_id'],
                    integration_name=row['integration_name'],
                    check_frequency=row['check_frequency'],
                    variation=row['variation'],
                    check_start=row['check_start'],
                    check_end=row['check_end'],
                    store_ids=row['store_ids'],
                    poc=row['poc']
                )

                print(main_text)
                slack.bot_slack(main_text, 'C02LF1U8W07')


for country in ['BR', 'CO', 'MX', 'AR', 'CL', 'PE', 'UY', 'EC', 'CR']:
    try:
        print(country)
        process(country)
    except Exception as e:
        print("Error in ", country)
        print(e.args)

        