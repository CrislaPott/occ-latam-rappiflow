import pandas as pd
import os, sys
sys.path.insert(0, "../")
from lib import snowflake as snow
os.chdir('../')

def country_list():
    countries = ['mx','br','co','cl','uy','cr','ec','pe','ar']
    return countries

def store_infos(country):
    query_store_infos = f"""
    --no_cache
    select 
           a.store_id::text         as storeid,
           a.name                   as store_name,
           a.type                   as store_type,
           c.id::text               as brand_id,
           c.name                   as brand_name,
           a.city_address_id        as city_id,
           msc.city                 as city_name,
           v.vertical,
           brand_group_id::text     as brand_group_id,
           brand_group_name,
           cpgops.physical_store_id::text as physical_store_id,
           pss.name                 as physical_store_name
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
             left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                       on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
             left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
             left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
             left join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                       on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
             inner join (select store_type                      as storetype,
                                case
                                    when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                    when vertical_group = 'WHIM' then 'Antojos'
                                    when vertical_group = 'RAPPICASH' then 'RappiCash'
                                    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                    when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                    when upper(store_type) in ('TURBO') then 'Turbo'
                                    when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                    when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                    when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                    when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                    when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                    when upper(vertical_group) in ('CPGS') then 'CPGs'
                                    else vertical_sub_group end as vertical
                         from VERTICALS_LATAM.{country}_VERTICALS_V2
    ) v on v.storetype = a.type

             left join (
        select *
        from (select store_brand_id,
                     last_value(brand_group_id)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_id,
                     last_value(bg.name)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_name
              from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                       join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
        group by 1, 2, 3
    ) bg on bg.store_brand_id = a.store_brand_id

    where not coalesce(a._fivetran_deleted, false)
    """
    df = snow.run_query(query_store_infos)
    return df