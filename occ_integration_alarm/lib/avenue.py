import requests
import json
import pandas as pd

def drop_salles(brand, orders0, orders7, orders14, avg, growth, kam, leader, global_team):
	webhook_url = 'https://app.useavenue.com/api/trigger/a6de592f-1130-4168-bc18-4b1a86fe53ec'
	data = { "brand": brand,
			 "d0": orders0,
			 "d7": orders7,
			 "d14": orders14,
			 "avg": avg,
			 "growth": growth,
			 "kam_phone_number": kam,
			 "leader_phone_number": leader,
			 "global_team_number": global_team
			}
	response = requests.post(
		webhook_url, data=json.dumps(data),
		headers={'Content-Type': 'application/json'}
		)
	print(response)