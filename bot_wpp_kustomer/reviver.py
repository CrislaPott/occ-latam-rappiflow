import os, sys, json, requests, arrow, time
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode
from lib import redash
from lib import slack
from lib import snowflake as snow
import pytz
from functools import reduce
import operator

def suspended_stores(country):
    query = '''select store_id::text as storeid, suspended_reason, created_at::date as created_at from store_attribute
    where suspended=true
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)
    return metrics

def reviver_br(df0,log_reviver,blacklist):

  df_suspended = suspended_stores('br')
  convs = df0
  reported = log_reviver

  convs['store_ids'] = convs['preview'].str.findall('[0-9]{6,}').apply(', '.join)
  # convs['suspensao_ate'] =  convs['preview'].str.findall('[0-9]{1,}\/[0-9]{1,}\/[0-9]{4,}').apply(', '.join)

  convs = convs[["kustomer_conversation_id", "kustomer_datetime", "phone", "preview", "store_ids"]]
  one = convs[(convs['preview'].str.contains("@")) & (convs['preview'].str.len() >= 4)]
  one['preview'] = one['preview'].str.lstrip()
  one['preview'] = one['preview'].str.rstrip()
  one.columns = one.columns.str.lower()
  one = one[(one['preview'].str.startswith('@', na=False))]
  one = one[(one['preview'].str.endswith('@', na=False) == False)]
  one = one[one["preview"].str.contains('@rappi', na=False) == False]

  one = one[['kustomer_conversation_id', 'preview', "kustomer_datetime"]]
  one['date'] = pd.to_datetime(one['kustomer_datetime'], format="%Y/%m/%d").dt.strftime('%d/%m/%Y')
  one = one.drop_duplicates()
  one = one.rename(columns={"preview": "user_name"})
  one['user_name'] = one['user_name'].str.lower()

  two = convs[convs['store_ids'].str.len() >= 1]
  # two = two[(two['preview'].str.contains("motivo"))]
  # two['motivo'] = two['preview'].str.split('motivo').str[-1]
  two = two[['kustomer_conversation_id', 'store_ids']]
  two = two.drop_duplicates()

  convs = pd.merge(one, two, how='left', on=['kustomer_conversation_id'])
  convs = convs[convs['store_ids'].notnull()]

  convs = pd.merge(convs, reported, how='left', on=['kustomer_conversation_id'])
  convs = convs[convs['sync_at'].isnull()]
  convs = convs[['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'store_ids', 'sync_at']]
  print(convs)
  convs = convs.drop_duplicates()

  to_upload = convs
  tz = pytz.timezone('America/Buenos_Aires')
  current_time = datetime.now(tz)
  to_upload['sync_at'] = current_time
  to_upload['country'] = 'br'

  if not to_upload.empty:
    snow.upload_df_occ(to_upload, 'occ_wpp_log_reviver')

  my_list = blacklist['storeid'].tolist()
  occ_list = ['00000000','0000000','000000']

  df_suspended = df_suspended[['storeid', 'suspended_reason']]
  df_suspended = df_suspended.drop_duplicates(subset='storeid', keep="first")
  df_suspended['suspended_reason'] = df_suspended['suspended_reason'].str.replace('Suspendida por OCC','Suspensa por OCC').str.replace(
        'suspendido por operaciones', 'Suspensa por OCC').str.replace('Solicitacao KAM OCC', 'Solicitud KAM OCC').str.replace(
        'Problems with Cancellations','Problemas con Cancelaciones').str.replace('Problemas de operação', 'Problemas de Operación').str.replace(
        'Sucursal cerrada', 'store_closed').str.replace('Temporalmente cerrado', 'store_closed').str.replace('CLOSED_STORE', 'store_closed').str.replace(
        'Problemas com cancelamentos','Problemas con Cancelaciones').str.replace('Request KAM OCC','Solicitud KAM OCC').replace(
        'Problems with high operating times','Problemas tiempos de Operación altos').str.replace('Monitor de integraciones','INTEGRATION_MONITOR').str.replace(
        'Suspended by OCC','Suspensa por OCC').str.replace('CONTROLTOWER_CANCELLATIONS','CONTROLTOWER_CANCELLATION').str.replace('wo_ecomm_monthly',
        'occ_ecomm_monthly').str.replace('weekly_ecommerce','occ_ecomm_weekly').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CBPR','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CANCEL_BY_INTEGRATION','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CBPI','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-NEGATIVE-PING-RULE','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-STOCKOUT','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-DELAY_IN_STORE','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-THIRD-PARTY-CANCELLATION-RULE','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CLOSED_STORE','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-ANDREACAROLINA.RUIZ','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-DANGHELY.CARDENAS','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-FERNANDO.ESPINOZA','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-FLORENCIA.BUDANO','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-JBUSTAMANTE','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-JENNIFER.HERNANDEZ','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-JOSE.OLAYA','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-JUAN.GRACIA','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-KAREN.CAMACHO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-MARIA.FARFAN','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-MARY.MARCANO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-MICHAEL.MARINO','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-MILEYDI.GUERRERO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-NESLY.PARRA','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-OMAR.FLORES','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-VICTOR.LIMA','INTEGRATION_MONITOR')   
  
  df_suspended.loc[df_suspended.suspended_reason.str.contains('ICC-SUSPENSION-MASSIVE-SUSPENSION', na=False),'suspended_reason'] = 'INTEGRATION_MONITOR'   
  df_suspended['storeid'] = df_suspended['storeid'].astype(str)
  df_suspended['storeid']=df_suspended['storeid'].str.strip()

  ecomm_monthly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_monthly']['storeid'])
  ecomm_weekly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_weekly']['storeid'])
  cancelaciones = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas con Cancelaciones']['storeid'])
  reclamos = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas alta tasa de reclamos']['storeid'])
  operacion = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas tiempos de Operación altos']['storeid'])
  ultrawo = list(df_suspended[df_suspended['suspended_reason'] == 'Ultra WO - Offboarding']['storeid'])
  integration = list(df_suspended[df_suspended['suspended_reason'] == 'INTEGRATION_MONITOR']['storeid'])
  controltower = list(df_suspended[df_suspended['suspended_reason'] == 'CONTROLTOWER_CANCELLATION']['storeid'])
  waiting_time = list(df_suspended[df_suspended['suspended_reason'] == 'waiting_time_saturated']['storeid'])
  solicitudkam = list(df_suspended[df_suspended['suspended_reason'] == 'Solicitud KAM OCC']['storeid'])
  suspensa_occ = list(df_suspended[df_suspended['suspended_reason'] == 'Suspensa por OCC']['storeid'])

  other = list(df_suspended[(df_suspended.suspended_reason != 'Problemas con Cancelaciones')
                            & (df_suspended.suspended_reason != 'Problemas alta tasa de reclamos')
                            & (df_suspended.suspended_reason != 'Problemas tiempos de Operación altos')
                            & (df_suspended.suspended_reason != 'Ultra WO - Offboarding')
                            & (df_suspended.suspended_reason != 'INTEGRATION_MONITOR')
                            & (df_suspended.suspended_reason != 'CONTROLTOWER_CANCELLATION')
                            & (df_suspended.suspended_reason != 'waiting_time_saturated')
                            & (df_suspended.suspended_reason != 'Solicitud KAM OCC')
                            & (df_suspended.suspended_reason != 'Suspensa por OCC')
                            & (df_suspended.suspended_reason != 'occ_ecomm_monthly')
                            & (df_suspended.suspended_reason != 'occ_ecomm_weekly')]['storeid'])

  convs = convs.drop_duplicates()
  for index, row in convs.iterrows():
      user = row['user_name']
      store_ids = row['store_ids']
      store_ids_list = store_ids.split(', ')
      print(store_ids_list)
      dia = row['date']
      black_list_stores = list(set(store_ids_list).intersection(my_list))
      print("black list stores")
      print(black_list_stores)
      occ_list_stores = list(set(store_ids_list).intersection(occ_list))
      print("occ list stores")
      print(occ_list_stores)
      print(len(black_list_stores))

      ecomm_monthly = list(set(store_ids_list).intersection(ecomm_monthly))
      ecomm_weekly = list(set(store_ids_list).intersection(ecomm_weekly))

      cancelaciones = list(set(store_ids_list).intersection(cancelaciones))
      reclamos = list(set(store_ids_list).intersection(reclamos))
      operacion = list(set(store_ids_list).intersection(operacion))
      ultrawo = list(set(store_ids_list).intersection(ultrawo))
      integration = list(set(store_ids_list).intersection(integration))
      controltower = list(set(store_ids_list).intersection(controltower))
      waiting_time = list(set(store_ids_list).intersection(waiting_time))
      solicitudkam = list(set(store_ids_list).intersection(solicitudkam))
      suspensa_occ = list(set(store_ids_list).intersection(suspensa_occ))
      other = list(set(store_ids_list).intersection(other))


      if int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 \
              and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores, )
          print(text)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 \
              and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
          
          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     cancelaciones=cancelaciones)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 \
              and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
               -
               :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

               :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores, reclamos=reclamos)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 \
              and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
               -
               :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

               :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          operacion=operacion)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
               -
               :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

               :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores, ultrawo=ultrawo)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
               -
               :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

               :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          integration=integration)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     controltower=controltower)
          print(text)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     waiting_time=waiting_time)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
           '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                      solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     cancelaciones=cancelaciones)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     cancelaciones=cancelaciones)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, reclamos=reclamos)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
               -
               :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

               :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, operacion=operacion)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
               Country: :flag-br:
               <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}*  
               -
               :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

               :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, ultrawo=ultrawo)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:
               
          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, integration=integration)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, controltower=controltower)
          print(text)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, waiting_time=waiting_time)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, other=other)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
          
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     occ_list_stores=occ_list_stores,
                     cancelaciones=cancelaciones)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          occ_list_stores=occ_list_stores,
                          reclamos=reclamos)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     occ_list_stores=occ_list_stores,
                     operacion=operacion)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     occ_list_stores=occ_list_stores,
                     ultrawo=ultrawo)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     occ_list_stores=occ_list_stores,
                     integration=integration)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                     occ_list_stores=occ_list_stores,
                     controltower=controltower)
          print(text)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          occ_list_stores=occ_list_stores,
                          waiting_time=waiting_time)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          occ_list_stores=occ_list_stores,
                          solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          occ_list_stores=occ_list_stores,
                          suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
               
          :warning: As lojas {occ_list_stores} tiveram solicitação de suspensão ao BOT. Requer análise :male-detective:

          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
               '''.format(user=user, dia=dia, store_ids=store_ids, black_list_stores=black_list_stores,
                          occ_list_stores=occ_list_stores,
                          other=other)
          print(text)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     cancelaciones=cancelaciones)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, reclamos=reclamos)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
              and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, operacion=operacion)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores, ultrawo=ultrawo)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                          integration=integration)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                          controltower=controltower)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                          waiting_time=waiting_time)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                          solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
               '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                          suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     other=other)
          print(text)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, other=other)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, solicitudkam=solicitudkam)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, waiting_time=waiting_time)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, controltower=controltower)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
          
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, other=other)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}*  
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
          
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, solicitudkam=solicitudkam)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, waiting_time=waiting_time)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, controltower=controltower)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                     occ_list_stores=occ_list_stores,
                     suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)



      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
             '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                        solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, controltower=controltower)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, waiting_time=waiting_time)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
            :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, other=other)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
             '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                        solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, controltower=controltower)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, waiting_time=waiting_time)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}*  
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
            :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, other=other)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other))== 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}*  
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
            
            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       cancelaciones=cancelaciones, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))>= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other))== 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       reclamos=reclamos, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))== 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       operacion=operacion, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))== 0 and int(len(operacion))== 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*  
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       ultrawo=ultrawo, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))== 0 and int(len(operacion))== 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       controltower=controltower, integration=integration)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))== 0 and int(len(operacion))== 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       waiting_time=waiting_time, integration=integration)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos))== 0 and int(len(operacion))== 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time))== 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -
          :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       other=other, integration=integration)

      elif int(len(black_list_stores))== 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       cancelaciones=cancelaciones, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       reclamos=reclamos, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       operacion=operacion, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*  
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       ultrawo=ultrawo, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       controltower=controltower, integration=integration)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}*  
            -

            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
            :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
            '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                       waiting_time=waiting_time, integration=integration)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
          Country: :flag-br:
          <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
          -

          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(user=user, dia=dia, store_ids=store_ids, occ_list_stores=occ_list_stores,
                     other=other, integration=integration)


      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, reclamos=reclamos, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*  
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, operacion=operacion, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, ultrawo=ultrawo, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, controltower=controltower, black_list_stores=black_list_stores)

      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, waiting_time=waiting_time, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) >= 1 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -
            :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, other=other, black_list_stores=black_list_stores)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, reclamos=reclamos, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*  
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, operacion=operacion, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, ultrawo=ultrawo, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, controltower=controltower, black_list_stores=black_list_stores)

      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, waiting_time=waiting_time, black_list_stores=black_list_stores)
      elif int(len(black_list_stores)) == 0 and int(len(occ_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(
              len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_monthly)) == 0 and int(len(ecomm_weekly)) == 0:
          print("the store is in the black list")
          text = '''
            Country: :flag-br:
            <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
            -

            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
            '''.format(user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, other=other, black_list_stores=black_list_stores)

      else:
          if int(len(black_list_stores)) >= 1 and int(
                  len(cancelaciones) + len(reclamos) + len(operacion) + len(ultrawo) + len(integration) + len(
                          controltower) + len(waiting_time) + len(solicitudkam) + len(
                          suspensa_occ) + len(other) + len(ecomm_monthly) + len(ecomm_weekly)) >= 1:
              text = '''
              Country: :flag-br:
              <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
              -
              :warning: As lojas {black_list_stores} estão na blacklist :skull_and_crossbones:
              
            :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
            :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
  			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
  			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
  			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
  			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
  			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
  			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
  			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
  			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
  			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
              '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                         occ_list_stores=occ_list_stores,
                         cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion, ultrawo=ultrawo,
                         integration=integration,
                         controltower=controltower, waiting_time=waiting_time,
                         solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, other=other,ecomm_weekly=ecomm_weekly,ecomm_monthly=ecomm_monthly)
          elif int(len(black_list_stores)) == 0 and int(
                  len(cancelaciones) + len(reclamos) + len(operacion) + len(ultrawo) + len(integration) + len(
                      controltower) + len(waiting_time) + len(solicitudkam) + len(
                      suspensa_occ) + len(other) + len(ecomm_monthly) + len(ecomm_weekly)) >= 1:
              text = '''
              Country: :flag-br:
              <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
              -

            :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
            :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
  			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
  			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
  			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
  			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
  			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
  			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
  			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
  			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
  			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
              '''.format(user=user, dia=dia, black_list_stores=black_list_stores, store_ids=store_ids,
                         occ_list_stores=occ_list_stores,
                         cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion, ultrawo=ultrawo,
                         integration=integration,
                         controltower=controltower, waiting_time=waiting_time,
                         solicitudkam=solicitudkam,suspensa_occ=suspensa_occ,
                         other=other,ecomm_weekly=ecomm_weekly,ecomm_monthly=ecomm_monthly)
          else:

              text = f'''
              Country: :flag-br:
              <{user}> :arrows_blue: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* 
              '''
              print(text)
      print(text)
      slack.bot_slack(text,'C021RGAGVV2')

def reviver_ss(df0,log_reviver):
  countries = ['ar', 'cl', 'pe', 'uy', 'mx', 'co', 'ec', 'cr']
  df_suspensa_ = []
  for country in countries:
      try:
          print(country)
          df = suspended_stores(country)
          df_suspensa_.append(df)
          df_suspended = pd.concat(df_suspensa_, ignore_index=True)
      except Exception as e:
          print(e)

  convs = df0
  reported = log_reviver

  convs['store_ids'] =  convs['preview'].str.findall('[0-9]{6,}').apply(', '.join)

  convs = convs[["kustomer_conversation_id","kustomer_datetime","phone","preview","store_ids","country"]]
  one = convs[(convs['preview'].str.contains("@")) & (convs['preview'].str.len() >= 4)]
  one['preview'] = one['preview'].str.lstrip()
  one['preview'] = one['preview'].str.rstrip()
  one.columns = one.columns.str.lower()
  one = one[(one['preview'].str.startswith('@', na=False))]
  one = one[(one['preview'].str.endswith('@', na=False) == False)]
  one = one[one["preview"].str.contains('@rappi',na=False) == False]

  one = one[['kustomer_conversation_id','preview',"kustomer_datetime"]]
  one['date'] = pd.to_datetime(one['kustomer_datetime'], format="%Y/%m/%d").dt.strftime('%d/%m/%Y')
  one = one.drop_duplicates()
  one = one.rename(columns={"preview": "user_name"})
  one['user_name'] = one['user_name'].str.lower()

  two = convs[convs['store_ids'].str.len() >= 1]
  #two = two[(two['preview'].str.contains("motivo"))]
  #two['motivo'] = two['preview'].str.split('motivo').str[-1]
  two = two[['kustomer_conversation_id','store_ids']]
  two = two.drop_duplicates()

  three = convs[(convs['country'].str.len() == 2) & ((convs['country'].str.contains("mx")) 
    | (convs['country'].str.contains("ar"))
    | (convs['country'].str.contains("cl"))
    | (convs['country'].str.contains("co"))
    | (convs['country'].str.contains("ec"))
    | (convs['country'].str.contains("cr"))
    | (convs['country'].str.contains("pe"))
    | (convs['country'].str.contains("uy"))
    | (convs['country'].str.contains("mx"))
    )]

  three = three[['kustomer_conversation_id','country']]

  convs = pd.merge(one,two, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['store_ids'].notnull()]
  convs = pd.merge(convs,three, how='left',on=['kustomer_conversation_id'])

  convs = convs[convs['store_ids'].notnull()]

  convs = pd.merge(convs,reported, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['sync_at'].isnull()]
  convs = convs[['kustomer_conversation_id','user_name','kustomer_datetime','date','store_ids','sync_at','country']]
  print(convs)

  to_upload = convs
  tz = pytz.timezone('America/Buenos_Aires')
  current_time = datetime.now(tz)
  to_upload['sync_at'] = current_time 
  to_upload = to_upload[['kustomer_conversation_id','user_name','kustomer_datetime','date','store_ids','sync_at','country']]
  print("insert into snowflake")
  if not to_upload.empty:
      snow.upload_df_occ(to_upload, 'occ_wpp_log_reviver')

  df_suspended = df_suspended[['storeid', 'suspended_reason']]
  df_suspended = df_suspended.drop_duplicates(subset='storeid', keep="first")
  df_suspended['suspended_reason'] = df_suspended['suspended_reason'].str.replace('Suspendida por OCC','Suspensa por OCC').str.replace(
      'suspendido por operaciones', 'Suspensa por OCC').str.replace('Solicitacao KAM OCC', 'Solicitud KAM OCC').str.replace(
      'Problems with Cancellations','Problemas con Cancelaciones').str.replace('Problemas de operação', 'Problemas de Operación').str.replace(
      'Sucursal cerrada', 'store_closed').str.replace('Temporalmente cerrado', 'store_closed').str.replace('CLOSED_STORE', 'store_closed').str.replace(
      'Problemas com cancelamentos','Problemas con Cancelaciones').str.replace('Request KAM OCC','Solicitud KAM OCC').replace(
      'Problems with high operating times','Problemas tiempos de Operación altos').str.replace('Monitor de integraciones','INTEGRATION_MONITOR').str.replace(
      'Suspended by OCC','Suspensa por OCC').str.replace('CONTROLTOWER_CANCELLATIONS','CONTROLTOWER_CANCELLATION').str.replace('weekly_ecommerce',
    'occ_ecomm_weekly').str.replace('wo_ecomm_monthly','occ_ecomm_monthly')

  df_suspended['storeid'] = df_suspended['storeid'].astype(str)
  df_suspended['storeid']=df_suspended['storeid'].str.strip()

  ecomm_monthly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_monthly']['storeid'])
  ecomm_weekly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_weekly']['storeid'])
  cancelaciones = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas con Cancelaciones']['storeid'])
  reclamos = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas alta tasa de reclamos']['storeid'])
  operacion = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas tiempos de Operación altos']['storeid'])
  ultrawo = list(df_suspended[df_suspended['suspended_reason'] == 'Ultra WO - Offboarding']['storeid'])
  integration = list(df_suspended[df_suspended['suspended_reason'] == 'INTEGRATION_MONITOR']['storeid'])
  controltower = list(df_suspended[df_suspended['suspended_reason'] == 'CONTROLTOWER_CANCELLATION']['storeid'])
  waiting_time = list(df_suspended[df_suspended['suspended_reason'] == 'waiting_time_saturated']['storeid'])
  solicitudkam = list(df_suspended[df_suspended['suspended_reason'] == 'Solicitud KAM OCC']['storeid'])
  suspensa_occ = list(df_suspended[df_suspended['suspended_reason'] == 'Suspensa por OCC']['storeid'])

  other = list(df_suspended[(df_suspended.suspended_reason != 'Problemas con Cancelaciones')
                            & (df_suspended.suspended_reason != 'Problemas alta tasa de reclamos')
                            & (df_suspended.suspended_reason != 'Problemas tiempos de Operación altos')
                            & (df_suspended.suspended_reason != 'Ultra WO - Offboarding')
                            & (df_suspended.suspended_reason != 'INTEGRATION_MONITOR')
                            & (df_suspended.suspended_reason != 'CONTROLTOWER_CANCELLATION')
                            & (df_suspended.suspended_reason != 'waiting_time_saturated')
                            & (df_suspended.suspended_reason != 'Solicitud KAM OCC')
                            & (df_suspended.suspended_reason != 'Suspensa por OCC')
                            & (df_suspended.suspended_reason != 'occ_ecomm_monthly')
                            & (df_suspended.suspended_reason != 'occ_ecomm_weekly')
                            ]['storeid'])


  for index, row in convs.iterrows():
      country = row['country']
      user = row['user_name']
      store_ids = row['store_ids']
      store_ids_list = store_ids.split(', ')
      print(store_ids_list)
      dia = row['date']
      #store_ids_list = []
      #store_ids_list.append(store_ids)
      #black_list_stores = list(set(store_ids_list).intersection(my_list))
      #print(len(black_list_stores))
      #if int(len(black_list_stores)) >= 1:
      #    print("the store is in the black list")
      #    text = f'''
      #    country: :flag-{country}:
      #    <{user}> :white_check_mark: solicitou a abertura da(s) loja(s) *{store_ids}* no dia *{dia}* :white_check_mark:
      #    :warning: *As lojas {black_list_stores} estão na blacklist* :skull_and_crossbones:
      #    '''
      ecomm_weekly = list(set(store_ids_list).intersection(ecomm_weekly))
      ecomm_monthly = list(set(store_ids_list).intersection(ecomm_monthly))
      cancelaciones = list(set(store_ids_list).intersection(cancelaciones))
      reclamos = list(set(store_ids_list).intersection(reclamos))
      operacion = list(set(store_ids_list).intersection(operacion))
      ultrawo = list(set(store_ids_list).intersection(ultrawo))
      integration = list(set(store_ids_list).intersection(integration))
      controltower = list(set(store_ids_list).intersection(controltower))
      waiting_time = list(set(store_ids_list).intersection(waiting_time))
      solicitudkam = list(set(store_ids_list).intersection(solicitudkam))
      suspensa_occ = list(set(store_ids_list).intersection(suspensa_occ))
      other = list(set(store_ids_list).intersection(other))
      if int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
              len(integration)) == 0 and int(len(controltower)) == 0 and int(
              len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(len(other)) == 0\
              and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 \
              and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
          '''.format(country=country, user=user, dia=dia, store_ids=store_ids, reclamos=reclamos)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 \
              and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
         '''.format(country=country,user=user, dia=dia, store_ids=store_ids, operacion=operacion)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, ultrawo=ultrawo)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:

          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, integration=integration)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:

          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,controltower=controltower)
          print(text)

      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:

          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, waiting_time=waiting_time)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:

          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,suspensa_occ=suspensa_occ)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, other=other)
          print(text)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(
              len(ultrawo)) == 0 and int(
              len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) >= 1 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, other=other,suspensa_occ=suspensa_occ)
          print(text)

      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, suspensa_occ=suspensa_occ, waiting_time=waiting_time)

      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,suspensa_occ=suspensa_occ, controltower=controltower)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 and int(len(suspensa_occ)) >= 1 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)

      elif int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}*  
          -
          :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, cancelaciones=cancelaciones)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) >= 1 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, reclamos=reclamos)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, operacion=operacion)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, ultrawo=ultrawo)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) >= 1 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, integration=integration)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) >= 1 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, controltower=controltower)

      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) == 0 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids, solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, waiting_time=waiting_time)
      elif int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
          len(integration)) == 0 and int(len(controltower)) == 0 and int(
          len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 and int(len(suspensa_occ)) == 0 and int(
          len(other)) >= 1 and int(len(ecomm_weekly)) == 0 and int(len(ecomm_monthly)) == 0:
          text = '''
          Country: :flag-{country}:
          <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
          -
          :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
          :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
          '''.format(country=country,user=user, dia=dia, store_ids=store_ids,solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, other=other)


      else:
          if int(len(cancelaciones) + len(reclamos) + len(operacion) + len(ultrawo) + len(integration) + len(
                      controltower)  + len(waiting_time) + len(solicitudkam) + len(
                      suspensa_occ) + len(other) + len(ecomm_weekly) + len(ecomm_monthly)) >= 1:
              text = '''
              Country: :flag-{country}:
              <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
              -
            :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
            :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
            :red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
            :red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
            :red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
            :red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
            :red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
            :red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
            :red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
            :large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
            :large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
            :large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
              '''.format(country=country,user=user, dia=dia, store_ids=store_ids, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                                 ultrawo=ultrawo,integration=integration,controltower=controltower, waiting_time=waiting_time,
                                 solicitudkam=solicitudkam,suspensa_occ=suspensa_occ, other=other, ecomm_weekly=ecomm_weekly, ecomm_monthly=ecomm_monthly)
          else:
              text = f'''
              Country: :flag-{country}:
              <{user}> :arrows_blue: solicitó la apertura de las tiendas *{store_ids}* en el dia *{dia}* 
              '''

      slack.bot_slack(text,'C021RGAGVV2')
