import os, sys, json, requests, arrow, time
import pandas as pd
import reviver as rev
import suspender as sus
import massive as mas
#import produtos as prod
import get_data as gd

general_df = gd.get_data()
log =  gd.logs()
gmv_store = gd.store_gmv()
blacklist = gd.blacklist()
#prod_blacklist = gd.blacklist_produtos()
#whitelist_prod = gd.whitelist()

log_reviver = log[log['type'] == 'reviver']
log_suspensao = log[log['type'] == 'suspender']
log_massivo = log[log['type'] == 'massivo']
#log_produtos = log[log['type'] == 'produtos']
#log_produtos = log_produtos.rename(columns={'kustomer_conversation_id': 'kustomer_conversationid'})

#whitelist_prod_br = whitelist_prod[whitelist_prod['country'] == 'br']

df_rev_br = general_df[general_df['request_type'] == 'BR - Reviver']
df_rev_ss = general_df[general_df['request_type'] == 'SS - Reviver']
df_sus_br = general_df[general_df['request_type'] == 'BR - Suspensao']
df_sus_ss = general_df[general_df['request_type'] == 'SS - Suspensao']
#df_prod_br = general_df[general_df['request_type'] == 'SS - Produtos']
#df_prod_ss = general_df[general_df['request_type'] == 'SS - Produtos']

print("reviver br")
try:
	rev.reviver_br(df_rev_br, log_reviver,blacklist)
except Exception as e:
	print(e)
print("reviver ss")
try:
	rev.reviver_ss(df_rev_ss, log_reviver)
except Exception as e:
	print(e)
print("suspender br")
try:
	sus.suspender_br(df_sus_br,log_suspensao, gmv_store)
except Exception as e:
	print(e)
print("suspender ss")
try:
	sus.suspender_ss(df_sus_ss,log_suspensao, gmv_store)
except Exception as e:
	print(e)
print("massivo - lojas")
try:
	mas.massive(log_massivo, gmv_store, blacklist)
except Exception as e:
	print(e)
'''print("produtos br")
try:
	prod.products_br(df_prod_br,log_produtos,prod_blacklist, whitelist_prod_br)
except Exception as e:
	print(e)
print("produtos ss")
try:
	prod.produtos_ss(df_prod_ss,log_produtos,prod_blacklist,whitelist_prod)
except Exception as e:
	print(e)
print('massivo - produtos')
try:
	prod.massive(whitelist_prod, prod_blacklist)
except Exception as e:
	print(e)
'''
print("fim")