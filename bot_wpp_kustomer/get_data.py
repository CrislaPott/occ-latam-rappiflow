from lib import snowflake as snow
import pandas as pd

def get_data():
  query = """
  --no_cache
WITH b AS
  (SELECT DISTINCT kustomer_conversation_id,
                   kustomer_created_at,
                   country,
                   tags, ended_at
   FROM CO_PG_MS_KUSTOMER_ETL_PUBLIC.conversations conv
      where kustomer_created_at::date >= current_date::date - 1
   ),
     d AS
  (SELECT kustomer_conversation_id,
          preview,
          m."FROM" AS phone,m.kustomer_message_id
   FROM CO_PG_MS_KUSTOMER_ETL_PUBLIC.messages m
   WHERE m.direction_type = 'followup-in'
      and created_at::date >= current_date::date - 1)

SELECT b.kustomer_conversation_id,
       kustomer_created_at AS kustomer_datetime,
       phone,
       preview,
       kustomer_message_id,
       case when (tags::text ILIKE '%5f5bcaa0c020c6001a053bc3%') then 'BR - Reviver'
            when (tags::text ILIKE '%5fea382ff15ec968f51ca08c%') then 'SS - Reviver'
            when (tags::text ILIKE '%5f5bbfb3ef0aa00019d3280f%') then 'BR - Suspensao'
            when (tags::text ILIKE '%5fea38440468e27aed903589%') then 'SS - Suspensao'
            when (tags::text ILIKE '%ffd134d227d2442ade5ed%') then 'BR - Produtos'
            when (tags::text ILIKE '%611febd1122dedcd1a1bc3a1%') then 'SS - Produtos'
       else 'Other' end as request_type,
       max(CASE
               WHEN (tags::text ILIKE '%5f5bcaa0c020c6001a053bc3%'
                    or tags::text ILIKE '%5f5bbfb3ef0aa00019d3280f%'
                    or tags::text ILIKE '%ffd134d227d2442ade5ed%') then 'br'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('1') THEN 'ar'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('2') THEN 'cl'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('3') THEN 'co'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('4') THEN 'cr'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('5') THEN 'ec'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('6') THEN 'mx'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('7') THEN 'pe'
               WHEN length(trim(preview)::text) = 1
                    AND preview IN ('8') THEN 'uy'
               ELSE NULL
           END) AS country

FROM b
JOIN d ON d.kustomer_conversation_id=b.kustomer_conversation_id
WHERE
      ---reviver
    ((tags::text ILIKE '%5f5bcaa0c020c6001a053bc3%') or (tags::text ILIKE '%5fea382ff15ec968f51ca08c%')
     --suspensao
        or (tags::text ILIKE '%5f5bbfb3ef0aa00019d3280f%')
        or (tags::text ILIKE '%5fea38440468e27aed903589%')
        or (tags::text ILIKE '%ffd134d227d2442ade5ed%')
        or (tags::text ILIKE '%611febd1122dedcd1a1bc3a1%')
        )
  and ended_at is not null
--and b.kustomer_conversation_id in ('616f2fbceb18fc32ae989c26')
group by 1,2,3,4,5,6
  """
  df = snow.run_query(query)
  return df

def logs():
  query = """
  select kustomer_conversation_id, sync_at, 'reviver' as type, null as ids
  from ops_occ.occ_wpp_log_reviver
  where sync_at::date >= current_date::date - 2
  group by 1,2,3,4

  union all
  select kustomer_conversation_id, sync_at, 'suspender' as type, null as ids
  from ops_occ.occ_wpp_log
  where sync_at::date >= current_date::date - 2
  group by 1,2,3,4

  union all
  select kustomer_conversation_id, requested_at as sync_at, 'massivo' as type, ids
  from ops_occ.wpp_log_massive
  group by 1,2,3,4
  """
  df_log = snow.run_query(query)
  return df_log

def blacklist():
  query = """
  select distinct store_id::text as storeid from fivetran.google_sheets.br_rc_store_blacklist
  """
  df_blacklist = snow.run_query(query)
  return df_blacklist

def whitelist():
  query = '''
  select distinct p.id::text as productid, 'br' as country
    from br_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='br'
union all
select distinct p.id::text as productid,'co' as country
    from co_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='co'
union all
select distinct p.id::text as productid,'mx' as country
    from mx_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='mx'
union all
select distinct p.id::text as productid,'cl' as country
    from cl_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='cl'
union all
select distinct p.id::text as productid,'cr' as country
    from cr_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='cr'
union all
select distinct p.id::text as productid,'ar' as country
    from ar_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='ar'
union all
select distinct p.id::text as productid,'uy' as country
    from uy_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='uy'
union all
select distinct p.id::text as productid,'pe' as country
    from pe_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='pe'
union all
select distinct p.id::text as productid,'ec' as country
    from ec_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='ec'
  '''
  df_whitelist_prod = snow.run_query(query)
  return df_whitelist_prod

def blacklist_produtos():
  query = """
  select distinct product_id::text as productid, store_id::text as store_id from ops_occ.product_blacklist limit 1
  """
  df_blacklist_prod = snow.run_query(query)
  return df_blacklist_prod

def store_gmv():
  query = """
  select store_id::text as store_id,days, hours, gmv_aov_usd, finished_orders_per_hour from ops_occ.RISK_MANAGEMENT
  """
  df_gmv = snow.run_query(query)
  return df_gmv