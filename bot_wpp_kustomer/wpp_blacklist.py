import os, sys, json
import pandas as pd
import numpy as np
import gspread
from lib import snowflake as snow
from lib import slack
import pytz
from datetime import date, datetime, timedelta

def get_logs():
	query = '''--no_cache
	select kustomer_id::text as kustomer_id, id_producto::text as id_producto, store_id::text as store_id, pais as country, sync_at
	from ops_occ.wpp_log_product_massive
	where sync_at::date >= current_date::date -1
	group by 1,2,3,4,5
	'''
	df_log = snow.run_query(query)
	return df_log

df = pd.read_csv("https://docs.google.com/spreadsheets/d/1Ehtu1laIzUjtNU1Ha-KrM3mBowrx0xOXksmybjcEd4s/export?format=csv&gid=0",error_bad_lines=False)

print("read sheets - done")

df['fecha_de_solicitude'] = pd.to_datetime(df['fecha_de_solicitude'], format="%d/%m/%Y", errors='coerce').dt.tz_localize(None)
df=df.loc[df['fecha_de_solicitude'].dt.date >= date.today()-timedelta(days=1)]
df['fecha_de_solicitude'] =df['fecha_de_solicitude'].dt.strftime("%d/%m/%Y")

print("sheets only since yesterday - done")

# evitar preenchimento em andamento
df = df[
    (df['@slack'].notnull())
    & (df['kustomer_id'].notnull())
    & (df['id_producto'].notnull())
    & (df['store_id'].notnull())
    & (df['pais'].notnull()
    & (df['blacklist'].notnull())
       )]

print(df)

## fix tag slack
df['@slack'] = df['@slack'].str.rstrip()
df['@slack'] = df['@slack'].str.lstrip()

reported = get_logs()
print(reported)
reported['kustomer_id'] = reported['kustomer_id'].astype(str)
df['id_producto'] = df['id_producto'].astype(str)
df['store_id'] = df['store_id'].astype(str)

df = pd.merge(df, reported, how='left',on=['kustomer_id','id_producto','store_id'])
df = df[df['sync_at'].isnull()]

countries = {'mexico' : 'mx',
                 'Mexico' : 'mx',
                 'México' : 'mx',
                 'méxico' : 'mx',
                 'Brasil' : 'br',
                 'brasil' : 'br',
                 'Colômbia': 'co',
                 'colômbia': 'co',
                 'colombia': 'co',
                 'Colombia': 'co',
                 'Chile' : 'cl',
                 'chile': 'cl',
                 'Uruguai': 'uy',
                 'uruguai': 'uy',
                 'Uruguay': 'uy',
                 'uruguay': 'uy',
                 'Costa Rica': 'cr',
                 'costa rica': 'cr',
                 'Equador': 'ec',
                 'equador': 'ec',
                 'Ecuador': 'ec',
                 'ecuador': 'ec',
                 'Peru': 'pe',
                 'peru': 'pe',
                 'Perú': 'pe',
                 'perú': 'pe',
                 'Argentina': 'ar',
                 'argentina': 'ar'
                 }

df['pais'] = df['pais'].str.strip()
df['pais'] = df['pais'].map(countries)

if not df.empty:

    tz = pytz.timezone('America/Bogota')
    current_time = datetime.now(tz)
    df['sync_at'] = current_time

    log = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'accion','fecha_de_reactivacion','pais','sync_at','kustomer_id']]
    df = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'pais','sync_at','kustomer_id','blacklist']]

    snow.upload_df_occ(df, 'blacklist_wpp')
    snow.upload_df_occ(log,'wpp_log_product_massive')
