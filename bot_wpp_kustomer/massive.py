import os, sys, json
import pandas as pd
import gspread
from lib import snowflake as snow
import pytz
from lib import redash
import numpy as np
from datetime import date, datetime, timedelta
from lib import slack
from ast import literal_eval

def suspended_stores(country):
    query = '''select store_id::text as storeid, suspended_reason, created_at::date as created_at from store_attribute
    where suspended=true
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)
    return metrics

sigla = {
    'Brasil': 'br',
    'Chile': 'cl',
    'Colombia': 'co',
    'Equador': 'ec',
    'Mexico': 'mx',
    'Argentina': 'ar',
    'Peru': 'pe',
    'Uruguai': 'uy',
    'Costa Rica': 'cr'}

def massive(log_massivo, gmv_lost, blacklist):
    reported = log_massivo

    my_list = blacklist['storeid'].tolist()
    occ_list = ['00000000','0000000','000000']


    countries = ['ar', 'cl', 'pe', 'br', 'uy', 'mx', 'co', 'ec', 'cr']
    df_suspensa_ = []
    for country in countries:
    	try:
    		print(country)
    		df = suspended_stores(country)
    		df_suspensa_.append(df)
    		df_suspended = pd.concat(df_suspensa_, ignore_index=True)
    	except Exception as e:
    		print(e)

    new_google_sheet_url = "https://docs.google.com/spreadsheets/d/1TQRvBqPurzyjnEuTHQ-7YurLg2pe-6cv8mxEHWEHri8/export?format=csv&gid=0"
    df = pd.read_csv(new_google_sheet_url, error_bad_lines=False)

    new_google_sheet_url = "https://docs.google.com/spreadsheets/d/1TQRvBqPurzyjnEuTHQ-7YurLg2pe-6cv8mxEHWEHri8/export?format=csv&gid=2034515473"
    whitelist = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
    my_whitelist = whitelist['Tiendas'].tolist()

    df = df[(df['@slack'].notnull()) & (df['kustomer_conversation_id'].notnull()) & (df['ids'].notnull()) & (
        df['hasta_cuiando_se_cierra'].notnull()) & (df['status'].notnull()) & (df['pais'].notnull())]
    df['@slack'] = df['@slack'].str.rstrip()
    df['@slack'] = df['@slack'].str.lstrip()
    df['kustomer_conversation_id'] = df['kustomer_conversation_id'].str.strip()

    df = df[["ids", "hasta_cuiando_se_cierra", "status", "@slack", "kustomer_conversation_id", "pais", "motivo"]]

    tz = pytz.timezone('America/Buenos_Aires')
    current_time = datetime.now(tz)

    reported['kustomer_conversation_id'] = reported['kustomer_conversation_id'].str.strip()
    df = pd.merge(df, reported, how='left', on=['kustomer_conversation_id', 'ids'])
    df = df[df['sync_at'].isnull()]
    df["motivo"] = df["motivo"].fillna("null")

    df_suspended = df_suspended[['storeid', 'suspended_reason']]
    df_suspended = df_suspended.drop_duplicates(subset='storeid', keep="first")
    df_suspended['suspended_reason'] = df_suspended['suspended_reason'].str.replace('Suspendida por OCC','Suspensa por OCC').str.replace(
        'suspendido por operaciones', 'Suspensa por OCC').str.replace('Solicitacao KAM OCC', 'Solicitud KAM OCC').str.replace(
        'Problems with Cancellations','Problemas con Cancelaciones').str.replace('Problemas de operação', 'Problemas de Operación').str.replace(
        'Sucursal cerrada', 'store_closed').str.replace('Temporalmente cerrado', 'store_closed').str.replace('CLOSED_STORE', 'store_closed').str.replace(
        'Problemas com cancelamentos','Problemas con Cancelaciones').str.replace('Request KAM OCC','Solicitud KAM OCC').replace(
        'Problems with high operating times','Problemas tiempos de Operación altos').str.replace('Monitor de integraciones','INTEGRATION_MONITOR').str.replace(
        'Suspended by OCC','Suspensa por OCC').str.replace('CONTROLTOWER_CANCELLATIONS','CONTROLTOWER_CANCELLATION').str.replace('wo_ecomm_monthly',
        'occ_ecomm_monthly').str.replace('weekly_ecommerce','occ_ecomm_weekly').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CBPR','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CANCEL_BY_INTEGRATION','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CBPI','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-NEGATIVE-PING-RULE','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-STOCKOUT','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-DELAY_IN_STORE','INTEGRATION_MONITOR').str.replace('MONITOR-SUSPENSION-THIRD-PARTY-CANCELLATION-RULE','INTEGRATION_MONITOR').str.replace(
        'MONITOR-SUSPENSION-CANCELLATION-ORDER-RULE-CLOSED_STORE','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-ANDREACAROLINA.RUIZ','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-DANGHELY.CARDENAS','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-FERNANDO.ESPINOZA','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-FLORENCIA.BUDANO','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-JBUSTAMANTE','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-JENNIFER.HERNANDEZ','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-JOSE.OLAYA','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-JUAN.GRACIA','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-KAREN.CAMACHO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-MARIA.FARFAN','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-MARY.MARCANO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-MICHAEL.MARINO','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-MILEYDI.GUERRERO','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-NESLY.PARRA','INTEGRATION_MONITOR').str.replace('ICC-SUSPENSION-MASSIVE-SUSPENSION-OMAR.FLORES','INTEGRATION_MONITOR').str.replace(
        'ICC-SUSPENSION-MASSIVE-SUSPENSION-VICTOR.LIMA','INTEGRATION_MONITOR')   
    df_suspended.loc[df_suspended.suspended_reason.str.contains('ICC-SUSPENSION-MASSIVE-SUSPENSION', na=False),'suspended_reason'] = 'INTEGRATION_MONITOR'    
        
    df_suspended['storeid'] = df_suspended['storeid'].astype(str)

    ecomm_monthly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_monthly']['storeid'])
    ecomm_weekly = list(df_suspended[df_suspended['suspended_reason'] == 'occ_ecomm_weekly']['storeid'])
    cancelaciones = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas con Cancelaciones']['storeid'])
    reclamos = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas alta tasa de reclamos']['storeid'])
    operacion = list(df_suspended[df_suspended['suspended_reason'] == 'Problemas tiempos de Operación altos']['storeid'])
    ultrawo = list(df_suspended[df_suspended['suspended_reason'] == 'Ultra WO - Offboarding']['storeid'])
    integration = list(df_suspended[df_suspended['suspended_reason'] == 'INTEGRATION_MONITOR']['storeid'])
    controltower = list(df_suspended[df_suspended['suspended_reason'] == 'CONTROLTOWER_CANCELLATION']['storeid'])
    waiting_time = list(df_suspended[df_suspended['suspended_reason'] == 'waiting_time_saturated']['storeid'])
    solicitudkam = list(df_suspended[df_suspended['suspended_reason'] == 'Solicitud KAM OCC']['storeid'])
    suspensa_occ = list(df_suspended[df_suspended['suspended_reason'] == 'Suspensa por OCC']['storeid'])

    other = list(df_suspended[(df_suspended.suspended_reason != 'Problemas con Cancelaciones')
                              & (df_suspended.suspended_reason != 'Problemas alta tasa de reclamos')
                              & (df_suspended.suspended_reason != 'Problemas tiempos de Operación altos')
                              & (df_suspended.suspended_reason != 'Ultra WO - Offboarding')
                              & (df_suspended.suspended_reason != 'INTEGRATION_MONITOR')
                              & (df_suspended.suspended_reason != 'CONTROLTOWER_CANCELLATION')
                              & (df_suspended.suspended_reason != 'waiting_time_saturated')
                              & (df_suspended.suspended_reason != 'Solicitud KAM OCC')
                              & (df_suspended.suspended_reason != 'Suspensa por OCC')
                              & (df_suspended.suspended_reason != 'occ_ecomm_monthly')
                              & (df_suspended.suspended_reason != 'occ_ecomm_weekly')
                              ]['storeid'])

    if not df.empty:
        df['datetime'] = current_time
        df0 = df.groupby(["kustomer_conversation_id", "@slack", "status", "hasta_cuiando_se_cierra", "pais", "motivo"])[
            "ids"].apply(list).reset_index(name='store_ids')
        df0['store_ids'] = [','.join(map(str, l)) for l in df0['store_ids']]
        df_reviver = df0[df0["status"] == 'reapertura']

        gmv_lost['days'] = gmv_lost['days'].astype(float)
        gmv_lost['hours'] = gmv_lost['hours'].astype(float)
        df["ids"]=df["ids"].astype(str)
        gmv_lost['store_id']=gmv_lost['store_id'].astype(str)
        gmv_lost['finished_orders_per_hour'] = gmv_lost['finished_orders_per_hour'].astype(float)
        gmv_lost['gmv_aov_usd'] = gmv_lost['gmv_aov_usd'].astype(float)
        df_ = pd.merge(df, gmv_lost, how='left', left_on=df["ids"], right_on=gmv_lost['store_id']).rename(columns={'days':'day_gmv','hours':'hour_gmv'})
        df_ = df_.drop(['key_0', 'store_id'], axis=1)
        df_ = df_.fillna(0)

        df_ = df_[df_["status"] == 'suspension']

        df_['hasta_cuiando_se_cierra'] = df_['hasta_cuiando_se_cierra'].replace(0, np.nan)
        df_['hasta_cuiando_se_cierra'] = df_['hasta_cuiando_se_cierra'].replace('25/09/0201','25/09/2021').replace(
            '31/012/2021','31/12/2021').replace('31/012/2050','31/12/2050').replace('31/12/20250','31/12/2050')
        df_['hasta_cuiando_se_cierra_datetime'] = pd.to_datetime(df_['hasta_cuiando_se_cierra'], format="%d/%m/%Y", errors='coerce')
        df_['hasta_cuiando_se_cierra_datetime'] = df_['hasta_cuiando_se_cierra_datetime'] + timedelta(hours=00, minutes=00, seconds=00,
                                                                                           milliseconds=000)
        df_['datetime']=df_['datetime'].dt.tz_localize(None)
        df_['diff_date'] = (df_['hasta_cuiando_se_cierra_datetime'] - df_['datetime'])
        df_time = df_['diff_date'].dt.components[['days', 'hours', 'minutes']]
        df_ = pd.concat([df_, df_time], axis=1)

        df_.loc[df_.days >= 60, 'days'] = 60
        df_['days_hour'] = df_['days'] * 24
        df_['time_total'] = ((df_['days_hour'] + df_['hours']) / df_['hour_gmv']) / df_['day_gmv']
        df_['time_total'] = df_['time_total'].fillna(0)

        df_['gmv_lost'] = df_['gmv_aov_usd'] * df_['finished_orders_per_hour'] * df_['time_total']
        df_.loc[df_.gmv_lost < 0, 'gmv_lost'] = 0
        df_['gmv_lost'] = df_['gmv_lost'].fillna(0)
        print(df_)
        print(df_.columns)
        df_1 = df_.groupby(['hasta_cuiando_se_cierra', 'status', '@slack', 'kustomer_conversation_id', 'pais', 'motivo', 'datetime',
             'hasta_cuiando_se_cierra_datetime', 'diff_date'])['ids'].agg([('store_ids', lambda x: ', '.join(map(str, x)))]).reset_index()

        df_2 = df_.groupby(['hasta_cuiando_se_cierra', 'status', '@slack', 'kustomer_conversation_id', 'pais', 'motivo', 'datetime',
             'hasta_cuiando_se_cierra_datetime', 'diff_date']).sum().reset_index()[['hasta_cuiando_se_cierra', 'status', '@slack', 'kustomer_conversation_id', 'pais', 'motivo', 'datetime',
             'hasta_cuiando_se_cierra_datetime', 'diff_date', 'gmv_lost']]

        if not df_.empty:
            df_suspender = pd.merge(df_1, df_2,on=['hasta_cuiando_se_cierra', 'status', '@slack', 'kustomer_conversation_id', 'pais','motivo', 'datetime','hasta_cuiando_se_cierra_datetime', 'diff_date'])
        else:
            print('df_suspender null')


        for index, row in df_reviver.iterrows():
            store_ids = row['store_ids']
            store_ids_list = store_ids.split(',')
            print(store_ids_list)
            print(len(store_ids_list))
            country = row['pais']
            country = sigla[country]
            motivo = row['motivo']
            black_list_stores = list(set(store_ids_list).intersection(my_list))
            print("black list")
            print(black_list_stores)
            occ_list_stores = list(set(store_ids_list).intersection(occ_list))
            print("occ_list_stores")
            print(occ_list_stores)

            ecomm_monthly = list(set(store_ids_list).intersection(ecomm_monthly))
            ecomm_weekly = list(set(store_ids_list).intersection(ecomm_weekly))
            cancelaciones = list(set(store_ids_list).intersection(cancelaciones))
            reclamos = list(set(store_ids_list).intersection(reclamos))
            operacion = list(set(store_ids_list).intersection(operacion))
            ultrawo = list(set(store_ids_list).intersection(ultrawo))
            integration = list(set(store_ids_list).intersection(integration))
            controltower = list(set(store_ids_list).intersection(controltower))
            waiting_time = list(set(store_ids_list).intersection(waiting_time))
            solicitudkam = list(set(store_ids_list).intersection(solicitudkam))
            suspensa_occ = list(set(store_ids_list).intersection(suspensa_occ))
            other = list(set(store_ids_list).intersection(other))

            if int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 1 and int(len(ecomm_weekly)) >= 1:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:
                
                :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
                :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    ecomm_weekly = ecomm_weekly, ecomm_monthly=ecomm_monthly,
                    black_list_stores=black_list_stores
                )
                print(text)

            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) >= 1 and int(len(controltower)) == 0  and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*  
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) >= 1 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)

            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1  and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 1 and int(len(ecomm_weekly)) >= 1:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:
                
                :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
                :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    ecomm_monthly = ecomm_monthly, ecomm_weekly=ecomm_weekly,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*  
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
                    len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1  and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)
            elif int(len(black_list_stores)) >= 1 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                    len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*  
    			Motivo: {motivos}
    			-
    			:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    black_list_stores=black_list_stores
                )
                print(text)

            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 1 and int(len(ecomm_weekly)) >= 1:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
                
                :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
                :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other,
                    ecomm_weekly=ecomm_weekly,ecomm_monthly=ecomm_monthly
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"*
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"*
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    		
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"*
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0  and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*  
    			Motivo: {motivos}
    			-

    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) == 0 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) >= 1 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*  
    			Motivo: {motivos}
    			-
    			
    			:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) == 0 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			'''.format(user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) == 0 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) == 0 and int(
                len(integration)) == 0 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) == 0 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-
    			
    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1  and int(
                len(waiting_time)) == 0 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"*
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"*
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) == 0 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"*
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"*
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) == 0 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"*
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            elif int(len(black_list_stores)) == 0 and int(len(cancelaciones)) >= 1 and int(len(reclamos)) >= 1 and int(
                    len(operacion)) >= 1 and int(len(ultrawo)) >= 1 and int(
                len(integration)) >= 1 and int(len(controltower)) >= 1 and int(
                len(waiting_time)) >= 1 and int(len(solicitudkam)) >= 1 \
                    and int(len(suspensa_occ)) >= 1 and int(len(other)) == 0 and int(len(ecomm_monthly)) >= 0 and int(len(ecomm_weekly)) >= 0:
                text = '''
    			Country: :flag-{country}:
    			*Ação Massiva - Reviver*
    			<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    			Motivo: {motivos}
    			-

    			:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    			:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"*
    			:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    			:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
    			:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    			:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    			:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    			:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
    			:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    			'''.format(
                    user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'], dia=current_time.date(),
                    country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos, operacion=operacion,
                    ultrawo=ultrawo, integration=integration, controltower=controltower,
                    waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other
                )
                print(text)
            else:
                if int(len(black_list_stores)) >= 1 and int(
                    len(cancelaciones) + len(reclamos) + len(operacion) + len(ultrawo) + len(integration) + len(
                        controltower) + len(waiting_time) + len(solicitudkam) + len(
                        suspensa_occ) + len(other) +len(ecomm_monthly) + len(ecomm_weekly)) >= 1:
                    text = '''
    				Country: :flag-{country}:
    				*Ação Massiva - Reviver*
    				<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    				Motivo: {motivos}
    				-
    				:warning: *Las tiendas {black_list_stores} están en la blacklist* :skull_and_crossbones:

                    :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
                    :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
    				:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    				:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    				:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    				:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
    				:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    				:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    				:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    				:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
    				:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    				:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    				'''.format(
                        user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'],
                        dia=current_time.date(), black_list_stores=black_list_stores,
                        country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos,
                        operacion=operacion,
                        ultrawo=ultrawo, integration=integration, controltower=controltower,
                        waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other, ecomm_weekly=ecomm_weekly,
                        ecomm_monthly=ecomm_monthly
                    )
                    print(text)
                elif int(len(black_list_stores)) == 0 and int(
                    len(cancelaciones) + len(reclamos) + len(operacion) + len(ultrawo) + len(integration) + len(
                        controltower) + len(waiting_time) + len(solicitudkam) + len(
                        suspensa_occ) + len(other)) >= 1:
                    text = '''
    				Country: :flag-{country}:
    				*Ação Massiva - Reviver*
    				<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}* 
    				Motivo: {motivos}
    				-
                    
                    :red_circle: Las tiendas {ecomm_weekly} están suspendidas con el motivo *"Ecomm Weekly"*
                    :red_circle: Las tiendas {ecomm_monthly} están suspendidas con el motivo *"Ecomm Monthly"*
    				:red_circle: Las tiendas {cancelaciones} están suspendidas con el motivo *"Problemas con Cancelaciones"* 
    				:red_circle: Las tiendas {reclamos} están suspendidas con el motivo *"Problemas Alta Tasa de Reclamos"* 
    				:red_circle: Las tiendas {operacion} están suspendidas con el motivo *"Problemas Tiempos de Operación Altos"* 
    				:red_circle: Las tiendas {ultrawo} están suspendidas con el motivo *"Ultra WO - Offboarding"* 
    				:red_circle: Las tiendas {integration} están suspendidas con el motivo *"Integration Monitor"* 
    				:red_circle: Las tiendas {controltower} están suspendidas con el motivo *"Control Tower Cancellation"* 
    				:red_circle: Las tiendas {waiting_time} están suspendidas con el motivo *"Waiting Time Saturated"* 
    				:large_yellow_circle: Las tiendas {solicitudkam} están suspendidas con el motivo *"OCC KAM Solicitud"* 
    				:large_yellow_circle: Las tiendas {suspensa_occ} están suspendidas con el motivo *"Suspendido por OCC"* 
    				:large_blue_circle: Las tiendas {other} están con suspensión vigente no relacionadas al OCC 
    				'''.format(
                        user=row['@slack'], store_ids=store_ids, data=row['hasta_cuiando_se_cierra'],
                        dia=current_time.date(),
                        country=country, motivos=motivo, cancelaciones=cancelaciones, reclamos=reclamos,
                        operacion=operacion,
                        ultrawo=ultrawo, integration=integration, controltower=controltower,
                        waiting_time=waiting_time, solicitudkam=solicitudkam, suspensa_occ=suspensa_occ, other=other, ecomm_weekly=ecomm_weekly,
                        ecomm_monthly=ecomm_monthly
                    )
                    print(text)
                else:
                    text = '''
    				Country: :flag-{country}:
    				*Ação Massiva - Reviver*
    				<{user}> :arrows_blue: reviveu a(s) loja(s) *{store_ids}* no dia *{data}*
    				Motivo: {motivos} 
    				'''.format(
                        user=row['@slack'],
                        store_ids=store_ids,
                        data=row['hasta_cuiando_se_cierra'],
                        # motivo = row['motivo'],
                        dia=current_time.date(),
                        country=country,
                        black_list_stores=black_list_stores,
                        motivos=motivo
                    )
                    print(text)

            print("aqui")
            slack.bot_slack(text, 'C021RGAGVV2')

        if not df_.empty:
            for index, row in df_suspender.iterrows():

                store_ids = row['store_ids']
                store_ids_list = store_ids.split(', ')
                print(store_ids_list)
                country = row['pais']
                motivo = row['motivo']
                country = sigla[country]
                white_list_stores = list(set(store_ids_list).intersection(my_whitelist))
                print(white_list_stores)
                gmv_lost = '{:,.2f}'.format(row['gmv_lost'])
                print(row['gmv_lost'])
                if int(len(white_list_stores)) >= 1 and row['gmv_lost']>=0:
                    text = '''
                    Country: :flag-{country}:
                    *Ação Massiva - Suspender*
                    <{user}> :arrowred: solicitó la suspensión de las tiendas *{store_ids}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* 
                    -
                    :warning: As lojas {white_list_stores} estão na whitelist :skull_and_crossbones:
                    :burning-money: *GMV Lost*: La suspensión provoca una *pérdida de US$ {gmv_lost}* :dollar:
                    '''.format(
                        user=row['@slack'],
                        store_ids=store_ids,
                        data=row['hasta_cuiando_se_cierra'],
                        dia=current_time.date(),
                        country=country,
                        white_list_stores=white_list_stores,
                        motivo=motivo,
                        gmv_lost =gmv_lost
                    )
                    print(text)
                    slack.bot_slack(text, 'C021RGAGVV2')
                elif int(len(white_list_stores)) ==0 and row['gmv_lost']>=0:
                    text = '''
                    Country: :flag-{country}:
                    *Ação Massiva - Suspender*
                    <{user}> :arrowred: solicitó la suspensión de las tiendas *{store_ids}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* 
                    -
                    :burning-money: *GMV Lost*: La suspensión provoca una *pérdida de US$ {gmv_lost}* :dollar:
                    '''.format(
                        user=row['@slack'],
                        store_ids=store_ids,
                        data=row['hasta_cuiando_se_cierra'],
                        dia=current_time.date(),
                        country=country,
                        white_list_stores=white_list_stores,
                        motivo=motivo,
                        gmv_lost=gmv_lost
                    )
                    print(text)
                    slack.bot_slack(text, 'C021RGAGVV2')
        print('df_suspender null')

        df_['requested_at'] = current_time.date()
        df_=df_[["ids", "hasta_cuiando_se_cierra", "status", "@slack", "kustomer_conversation_id", "requested_at", "pais",'gmv_lost']]

        to_upload = df
        to_upload['requested_at'] = current_time.date()
        to_upload = to_upload[to_upload["status"] == 'reapertura']
        to_upload = to_upload[["ids", "hasta_cuiando_se_cierra", "status", "@slack", "kustomer_conversation_id", "requested_at", "pais"]]

        df_final = to_upload.append(df_,sort=False)
        print(df_final)
        if not df_final.empty:
            snow.upload_df_occ(df_final, "wpp_log_massive")
