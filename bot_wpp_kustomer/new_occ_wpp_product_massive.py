import os, sys, json
import pandas as pd
import numpy as np
import gspread
from lib import snowflake as snow
from lib import slack
import pytz
from datetime import date, datetime, timedelta

def get_logs():
	query = '''--no_cache
	select kustomer_id, id_producto, store_id,sync_at
	from ops_occ.wpp_log_product_massive
	where sync_at::date >= current_date::date -1
	group by 1,2,3,4
	'''
	df_log = snow.run_query(query)
	return df_log

def blacklist():
    query = '''

       select distinct product_id::text as productid from ops_occ.nl_products_bl_vw
       '''
    df = snow.run_query(query)
    return df

def whitelist(country):
    query = ''' select distinct op.product_id::text as productid
    from {country}_core_orders_public.order_product_vw op
    join {country}_pglr_ms_grability_products_public.products_vw p ON op.product_id = p.id
    join {country}_pglr_ms_grability_products_public.product_store_vw  PS ON PS.PRODUCT_ID = op.product_id and ps.store_id = op.store_id
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='{country}'
         '''.format(country=country)
    df = snow.run_query(query)
    return df

try:
	reported = get_logs()
except:
	sys.exit()

print("logs snowflake - done")

df = pd.read_csv("https://docs.google.com/spreadsheets/d/1Ehtu1laIzUjtNU1Ha-KrM3mBowrx0xOXksmybjcEd4s/export?format=csv&gid=0",error_bad_lines=False)

print("read sheets - done")

df['fecha_de_solicitude'] = pd.to_datetime(df['fecha_de_solicitude'], format="%d/%m/%Y", errors='coerce').dt.tz_localize(None)
df=df.loc[df['fecha_de_solicitude'].dt.date >= date.today()-timedelta(days=1)]
df['fecha_de_solicitude'] =df['fecha_de_solicitude'].dt.strftime("%d/%m/%Y")

print("sheets only since yesterday - done")

# evitar preenchimento em andamento
df = df[
    (df['@slack'].notnull())
    & (df['kustomer_id'].notnull())
    & (df['id_producto'].notnull())
    & (df['store_id'].notnull())
    & (df['motivo'].notnull())
    & (df['pais'].notnull())
    & (df['blacklist'].isnull())
    ]

## fix tag slack
df['@slack'] = df['@slack'].str.rstrip()
df['@slack'] = df['@slack'].str.lstrip()

df = df[['fecha_de_solicitude','pais','@slack','store_id','accion','id_producto','motivo','fecha_de_reactivacion','kustomer_id','reactivado']]

reported['kustomer_id'] = reported['kustomer_id'].astype(str)
df['kustomer_id'] = df['kustomer_id'].astype(str)
reported['id_producto'] = reported['id_producto'].astype(str)
df['id_producto'] = df['id_producto'].astype(str)
reported['store_id'] = reported['store_id'].astype(str)
df['store_id'] = df['store_id'].astype(str)

print("merge")
df = pd.merge(df, reported, how='left', on=['kustomer_id','id_producto','store_id'])
print("sync_at is null")
df = df[df['sync_at'].isnull()]

countries = {'mexico' : 'mx',
                 'Mexico' : 'mx',
                 'México' : 'mx',
                 'méxico' : 'mx',
                 'Brasil' : 'br',
                 'brasil' : 'br',
                 'Colômbia': 'co',
                 'colômbia': 'co',
                 'colombia': 'co',
                 'Colombia': 'co',
                 'Chile' : 'cl',
                 'chile': 'cl',
                 'Uruguai': 'uy',
                 'uruguai': 'uy',
                 'Uruguay': 'uy',
                 'uruguay': 'uy',
                 'Costa Rica': 'cr',
                 'costa rica': 'cr',
                 'Equador': 'ec',
                 'equador': 'ec',
                 'Ecuador': 'ec',
                 'ecuador': 'ec',
                 'Peru': 'pe',
                 'peru': 'pe',
                 'Perú': 'pe',
                 'perú': 'pe',
                 'Argentina': 'ar',
                 'argentina': 'ar'
                 }

df['pais'] = df['pais'].str.strip()
df['pais'] = df['pais'].map(countries)

print("starts if")
if not df.empty:

    tz = pytz.timezone('America/Bogota')
    current_time = datetime.now(tz)
    df['sync_at'] = current_time

    df = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'accion', 'motivo', 'fecha_de_reactivacion','pais','sync_at','kustomer_id']]

    df1 = df.drop(['motivo'], axis=1)

    snow.upload_df_occ(df1, 'wpp_log_product_massive')

    df_blacklist = blacklist()

    df_blacklist['productid'] = df_blacklist['productid'].astype(float)
    df['id_producto'] = df['id_producto'].astype(float)
    df_bl = pd.merge(df, df_blacklist, how='left', left_on=df['id_producto'],
                     right_on=df_blacklist['productid']).rename(columns={'productid': 'blproductid'})

    df_bl['accion'] = df_bl['accion'].str.strip()
    df_bl['store_id'] = df_bl['store_id'].astype(str).str.strip()
    df_bl['id_producto'] = df_bl['id_producto'].astype(str).str.strip()
    df_bl['prod_store'] = df_bl['id_producto'] + '/' + df_bl['store_id']
    df_bl['fecha_de_reactivacion'] = df_bl['fecha_de_reactivacion'].fillna(0)
    df_bl['fecha_de_solicitude'] = df_bl['fecha_de_solicitude'].fillna(0)

    df_bl1 = df_bl[df_bl['blproductid'] > 0]  #### blacklist
    df_bl2 = df_bl[df_bl['blproductid'].isnull()]
    df_slack_bl1 = df_bl1.groupby(['@slack', 'accion', 'motivo', 'fecha_de_solicitude', 'pais', 'fecha_de_reactivacion'])['prod_store'].agg([('prod_store', ', '.join)]).reset_index()  #### blacklist

    df_whitelist_ = []
    for country in df[df['pais'].notna()]['pais'].unique().tolist():
        try:
            print(country)
            df_ = whitelist(country)
            df_whitelist_.append(df_)
            df_whitelist = pd.concat(df_whitelist_, ignore_index=True)
            df_whitelist['country'] = country
        except Exception as e:
            print(e)
            
    print(df_whitelist)


    df_whitelist['productid'] = df_whitelist['productid'].astype(float)

    df['id_producto'] = df['id_producto'].astype(float)
    df_wl = pd.merge(df, df_whitelist, how='left', left_on=df['id_producto'],right_on=df_whitelist['productid']).rename(columns={'productid': 'wlproductid'})

    df_wl['accion'] = df_wl['accion'].str.strip()
    df_wl['store_id'] = df_wl['store_id'].astype(str).str.strip()
    df_wl['id_producto'] = df_wl['id_producto'].astype(str).str.strip()
    df_wl['prod_store'] = df_wl['id_producto'] + '/' + df_wl['store_id']
    df_wl['fecha_de_reactivacion'] = df_wl['fecha_de_reactivacion'].fillna(0)
    df_wl['fecha_de_solicitude'] = df_wl['fecha_de_solicitude'].fillna(0)

    df_wl1 = df_wl[df_wl['wlproductid'] > 0] ## whitelist
    df_wl2 = df_wl[df_wl['wlproductid'].isnull()]
    df_slack_wl1 = df_wl1.groupby(['@slack', 'accion', 'motivo', 'fecha_de_solicitude', 'pais', 'fecha_de_reactivacion'])['prod_store'].agg([('prod_store', ', '.join)]).reset_index()

    ### nem blacklist nem whitelist
    df_wl2 = df_wl2.drop(['key_0'], axis=1)
    df_bl2 = df_bl2.drop(['key_0'], axis=1)
    df_union = pd.merge(df_wl2,df_bl2, how='inner', on=['fecha_de_solicitude','@slack','id_producto','store_id','accion','motivo','fecha_de_reactivacion','pais','sync_at','kustomer_id','prod_store'])

    df_slack_union = df_union.groupby(['@slack', 'accion', 'motivo', 'fecha_de_solicitude', 'pais', 'fecha_de_reactivacion'])['prod_store'].agg([('prod_store', ', '.join)]).reset_index()
    print(df_slack_union)
    print(df_slack_union.columns)

    ### blacklist
    list_blacklist_activo = []
    list_blacklist_desactivo = []
    if not df_bl1.empty:
        try:
            for index, row in df_bl1.iterrows():
                if row['accion'] == 'Activar' or row['accion'] == 'Ativar' or row['accion'] == 'ativar' or row['accion'] == 'activar':
                    list_blacklist_activo.append(str(int(row['blproductid'])))
                elif row['accion'] == 'Desactivar' or row['accion'] == 'Desativar' or row['accion'] == 'desativar' or row['accion'] == 'desactivar':
                    list_blacklist_desactivo.append(str(int(row['blproductid'])))
        except Exception as e:
            print(e)

    list_whitelist_activo = []
    list_whitelist_desactivo = []
    if not df_wl1.empty:
        try:
            for index, row in df_wl1.iterrows():
                if row['accion'] == 'Activar' or row['accion'] == 'Ativar' or row['accion'] == 'ativar' or row['accion'] == 'activar':
                    list_whitelist_activo.append(str(int(row['wlproductid'])))
                elif row['accion'] == 'Desactivar' or row['accion'] == 'Desativar' or row['accion'] == 'desativar' or row['accion'] == 'desactivar':
                    list_whitelist_desactivo.append(str(int(row['wlproductid'])))
        except Exception as e:
            print(e)

    if not df_slack_bl1.empty:
        try:
            for index, row in df_slack_bl1.iterrows():
                if row['accion'] == 'Activar' or row['accion'] == 'Ativar' or row['accion'] == 'ativar' or row['accion'] == 'activar':
                    ### presente na blacklist
                    text = '''Country: :flag-{country}:
                             *Ação Massiva - Activar producto*
                              <{slack}> solicitó el encendido del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, por motivo *{motivo}* no dia *{fecha_de_solicitude}*. :doblecheck:
                        :warning: El producto *{productid_blacklist}* está en la blacklist. '''.format(
                        country=row['pais'],
                        slack=row['@slack'],
                        prod_store=row['prod_store'],
                        fecha_de_solicitude=row['fecha_de_solicitude'],
                        productid_blacklist=list_blacklist_activo,
                        motivo=row['motivo']

                    )
                    print(text)
                    slack.bot_slack(text, 'C02C5HGFZLJ')

                elif row['accion'] == 'Desactivar' or row['accion'] == 'Desativar' or row['accion'] == 'desativar' or row['accion'] == 'desactivar':

                    text = '''Country: :flag-{country}:
                             *Ação Massiva - Desactivar producto*
                             <{slack}> solicitó el apagado del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, no dia *{fecha_de_solicitude}*, por el motivo *{motivo}* hasta el día *{fecha_de_reactivacion}*.  :no_entry:
                       '''.format(
                        country=row['pais'],
                        slack=row['@slack'],
                        prod_store=row['prod_store'],
                        fecha_de_solicitude=row['fecha_de_solicitude'],
                        fecha_de_reactivacion=row['fecha_de_reactivacion'],
                        motivo=row['motivo']

                    )
                    print(text)
                    slack.bot_slack(text, 'C02C5HGFZLJ')
        except Exception as e:
            print(e)

    ##Whitelist

    if not df_slack_wl1.empty:
        try:
            for index, row in df_slack_wl1.iterrows():
                if row['accion'] == 'Activar' or row['accion'] == 'Ativar' or row['accion'] == 'ativar' or row['accion'] == 'activar':

                    text = '''Country: :flag-{country}:
                             *Ação Massiva - Activar producto*
                              <{slack}> solicitó el encendido del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, por motivo *{motivo}* no dia *{fecha_de_solicitude}*. :doblecheck:
                        '''.format(
                        country=row['pais'],
                        slack=row['@slack'],
                        prod_store=row['prod_store'],
                        fecha_de_solicitude=row['fecha_de_solicitude'],
                        motivo=row['motivo']
                    )
                    print(text)
                    slack.bot_slack(text, 'C02C5HGFZLJ')

                elif row['accion'] == 'Desactivar' or row['accion'] == 'Desativar' or row['accion'] == 'desativar' or row['accion'] == 'desactivar':

                    text = '''Country: :flag-{country}:
                        *Ação Massiva - Desactivar producto*
                        <{slack}> solicitó el apagado del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, no dia *{fecha_de_solicitude}*, por el motivo *{motivo}* hasta el día *{fecha_de_reactivacion}*.  :no_entry:
                       :alert: El producto {productid_whitelist} no puede ser desactivado por ser un top seller :alert:
                       '''.format(
                        country=row['pais'],
                        slack=row['@slack'],
                        prod_store=row['prod_store'],
                        fecha_de_solicitude=row['fecha_de_solicitude'],
                        productid_whitelist=list_whitelist_desactivo,
                        fecha_de_reactivacion=row['fecha_de_reactivacion'],
                        motivo=row['motivo']
                    )
                    print(text)
                    slack.bot_slack(text, 'C02C5HGFZLJ')

        except Exception as e:
            print(e)

    ##nem Whitelist nem blacklist
    if not df_slack_union.empty:
        try:
            for index, row in df_slack_union.iterrows():
                if row['accion'] == 'Activar' or row['accion'] == 'Ativar' or row['accion'] == 'ativar' or row['accion'] == 'activar':

                        text = '''Country: :flag-{country}:
                                 *Ação Massiva - Activar producto*
                                  <{slack}> solicitó el encendido del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, por motivo *{motivo}* no dia *{fecha_de_solicitude}*. :doblecheck:
                            '''.format(
                            country=row['pais'],
                            slack=row['@slack'],
                            prod_store=row['prod_store'],
                            fecha_de_solicitude=row['fecha_de_solicitude'],
                            motivo=row['motivo']
                        )
                        print(text)
                        slack.bot_slack(text, 'C02C5HGFZLJ')

                elif row['accion'] == 'Desactivar' or row['accion'] == 'Desativar' or row['accion'] == 'desativar' or row['accion'] == 'desactivar':

                        text = '''Country: :flag-{country}:
                            *Ação Massiva - Desactivar producto*
                            <{slack}> solicitó el apagado del :avocado: producto (Product_ID/Store_ID :building_construction:): *{prod_store}*, no dia *{fecha_de_solicitude}*, por el motivo *{motivo}* hasta el día *{fecha_de_reactivacion}*.  :no_entry:
                           '''.format(
                            country=row['pais'],
                            slack=row['@slack'],
                            prod_store=row['prod_store'],
                            fecha_de_solicitude=row['fecha_de_solicitude'],
                            fecha_de_reactivacion=row['fecha_de_reactivacion'],
                            motivo=row['motivo']
                        )
                        print(text)
                        slack.bot_slack(text, 'C02C5HGFZLJ')

        except Exception as e:
            print(e)

else:
    print('null')
