import os, sys, json, requests, arrow, time
import regex as re
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
from unidecode import unidecode
from lib import gsheets, slack
from lib import redash
from lib import snowflake as snow
import pytz

def explode_str(df, col, sep):
    s = df[col]
    i = np.arange(len(s)).repeat(s.str.count(sep) + 1)
    return df.iloc[i].assign(**{col: sep.join(s).split(sep)})

def suspender_br(df0,log_suspender, gmv_lost):
  new_google_sheet_url = "https://docs.google.com/spreadsheets/d/1TQRvBqPurzyjnEuTHQ-7YurLg2pe-6cv8mxEHWEHri8/export?format=csv&gid=2034515473"
  whitelist = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
  print("whitelist")
  my_list = whitelist['Tiendas'].tolist()
  print(my_list)

  reported = log_suspender
  convs = df0

  convs = convs[["kustomer_conversation_id", "kustomer_datetime", "phone", "preview"]]

  convs['store_ids'] = convs['preview'].str.findall('[0-9]{6,}').apply(', '.join)
  convs['suspensao_ate'] = convs['preview'].str.findall('[0-9]{1,}\/[0-9]{1,}\/[0-9]{4,}').apply(', '.join)

  convs = convs[["kustomer_conversation_id", "kustomer_datetime", "phone", "preview", "store_ids", "suspensao_ate"]]
  one = convs[(convs['preview'].str.contains("@")) & (convs['preview'].str.len() >= 4)]
  one['preview'] = one['preview'].str.strip()
  one.columns = one.columns.str.lower()
  one = one[(one['preview'].str.startswith('@', na=False))]
  one = one[one["preview"].str.contains('@rappi') == False]

  one = one[['kustomer_conversation_id', 'preview', "kustomer_datetime"]]
  one['date'] = pd.to_datetime(one['kustomer_datetime'], format="%Y/%m/%d").dt.strftime('%d/%m/%Y')
  one = one.drop_duplicates()
  one = one.rename(columns={"preview": "user_name"})
  one['user_name'] = one['user_name'].str.lower()
  print(one)
  two = convs[convs['store_ids'].str.len() >= 1]
  two = two[(two['preview'].str.contains("motivo"))]
  two['motivo'] = two['preview'].str.split('motivo').str[-1]
  two = two[['kustomer_conversation_id', 'store_ids', 'suspensao_ate', 'motivo']]
  two = two.drop_duplicates()
  print(two)
  convs = pd.merge(one, two, how='left', on=['kustomer_conversation_id'])
  print(convs)
  convs = convs[convs['store_ids'].notnull()]

  convs = convs[convs['store_ids'].notnull()]

  convs = pd.merge(convs,reported, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['sync_at'].isnull()]
  print("ver convs")

  convs = convs[['kustomer_conversation_id','user_name','kustomer_datetime','date','store_ids','suspensao_ate','motivo','sync_at']]
  print(convs)

  ######gmv lost

  if not convs.empty:

      convs = explode_str(convs, 'store_ids', ',')
      convs = convs.drop(['sync_at'], axis=1)

      gmv_lost['days'] = gmv_lost['days'].astype(float)
      gmv_lost['hours'] = gmv_lost['hours'].astype(float)
      gmv_lost['store_id'] = gmv_lost['store_id'].astype(str)
      gmv_lost['finished_orders_per_hour'] = gmv_lost['finished_orders_per_hour'].astype(float)
      gmv_lost['gmv_aov_usd'] = gmv_lost['gmv_aov_usd'].astype(float)

      convs1 = pd.merge(convs, gmv_lost, how='left', left_on=convs["store_ids"], right_on=gmv_lost['store_id']).rename(
          columns={'days': 'day_gmv', 'hours': 'hour_gmv'})
      convs1 = convs1.drop(['key_0', 'store_id'], axis=1)
      convs1 = convs1.fillna(0)

      convs1 = convs1.replace(0, np.nan)

      convs1['suspensao_ate'] = convs1['suspensao_ate'].replace('', "01/01/2010")
      convs1['suspensao_ate_datetime'] = pd.to_datetime(convs1['suspensao_ate'], format="%d/%m/%Y", errors='coerce')
      convs1['date_datetime'] = pd.to_datetime(convs1['kustomer_datetime']).dt.tz_localize(None)
      convs1['diff_date'] = (convs1['suspensao_ate_datetime'] - convs1['date_datetime'])
      convs1['suspensao_ate'] = convs1['suspensao_ate'].replace("01/01/2010", '')
      df_time = convs1['diff_date'].dt.components[['days', 'hours', 'minutes']]
      convs = pd.concat([convs1, df_time], axis=1)
      convs['day_gmv'] = convs['day_gmv'].astype(float)
      convs['hour_gmv'] = convs['hour_gmv'].astype(float)

      convs.loc[convs.days >= 60, 'days'] = 60
      convs['days_hour'] = convs['days'] * 24
      convs['time_total'] = ((convs['days_hour'] + convs['hours']) / convs['hour_gmv']) / convs['day_gmv']
      convs['time_total'] = convs['time_total'].fillna(0)

      convs['gmv_lost'] = convs['gmv_aov_usd'] * convs['finished_orders_per_hour'] * convs['time_total']
      convs.loc[convs.gmv_lost < 0, 'gmv_lost'] = 0
      convs['gmv_lost']=convs['gmv_lost'].fillna(0)


      convs_1 = convs.groupby(['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date'])['store_ids'].agg(
          [('store_ids', lambda x: ', '.join(map(str, x)))]).reset_index()

      convs_2 = convs.groupby(
          ['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date']).sum().reset_index()[['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date', 'gmv_lost']]

      if not convs.empty:
          convs = pd.merge(convs_1, convs_2,on=['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date'])
      else:
          print('convs null')
      #####

      to_upload = convs

      tz = pytz.timezone('America/Buenos_Aires')
      current_time = datetime.now(tz)
      to_upload['sync_at'] = current_time
      to_upload['country'] = 'br'
      to_upload = to_upload[['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'store_ids', 'sync_at', 'country','gmv_lost']]
      print("insert into snowflake")
      if not to_upload.empty:
        snow.upload_df_occ(to_upload, 'occ_wpp_log')
      if not convs.empty:
          for index, row in convs.iterrows():
              store_ids = row['store_ids']
              dia = row['date']
              store_ids_list = []
              store_ids_list.append(store_ids)
              print(store_ids)
              white_list_stores = list(set(store_ids_list).intersection(my_list))
              print(len(white_list_stores))
              gmv_lost = '{:,.2f}'.format(row['gmv_lost'])
              print(row['gmv_lost'])
              print(row['user_name'])
              print(row['suspensao_ate'])

              if int(len(white_list_stores)) >= 1 and row['gmv_lost']>=0:
                  text = '''
                  Country: :flag-br:
                  <{user}> :arrowred: requisitou a suspensão da(s) loja(s) *{store_ids}*, no dia *{dia}*, pelo motivo *{motivo}* até a data *{data}* 
                  :warning: *As lojas {white_list_stores} estão na whitelist* :skull_and_crossbones:
              
                  :burning-money: *GMV Lost*: A suspensão ocasiona uma *perda de US$ {gmv_lost}* :dollar:
                  '''.format(
                      user=row['user_name'],
                      store_ids=store_ids,
                      data=row['suspensao_ate'],
                      motivo=row['motivo'],
                      dia=row['date'],
                      white_list_stores=white_list_stores,
                      gmv_lost=gmv_lost
                  )
                  print(text)
              elif int(len(white_list_stores)) == 0 and row['gmv_lost']>=0:
                  text = '''
                  Country: :flag-br:
                  <{user}> :arrowred: requisitou a suspensão da(s) loja(s) *{store_ids}*, no dia *{dia}*, pelo motivo *{motivo}* até a data *{data}* 
                 
                  :burning-money: *GMV Lost*: A suspensão ocasiona uma *perda de US$ {gmv_lost}* :dollar:
                  '''.format(
                      user=row['user_name'],
                      store_ids=store_ids,
                      data=row['suspensao_ate'],
                      motivo=row['motivo'],
                      dia=row['date'],
                      white_list_stores=white_list_stores,
                      gmv_lost=gmv_lost
                  )
                  print(text)
              elif row['user_name'] != 'nan' and row['suspensao_ate'] != 'nan' and len(store_ids) != 0:
                  text = '''
                  Country: :flag-br:
                  <{user}> :arrowred: requisitou a suspensão da(s) loja(s) *{store_ids}*, no dia *{dia}*, pelo motivo *{motivo}* até a data *{data}* 
                  '''.format(
                      user=row['user_name'],
                      store_ids=store_ids,
                      data=row['suspensao_ate'],
                      motivo=row['motivo'],
                      dia=row['date'],
                      white_list_stores=white_list_stores
                  )
                  print(text)
              else:
                  print('null')


              slack.bot_slack(text,'C021RGAGVV2')


      else:
          print('convs null')

  else:
      print('df is null')

def suspender_ss(df0, log_suspender, gmv_lost):
  reported = log_suspender
  convs = df0

  convs = convs[["kustomer_conversation_id","kustomer_datetime","phone","preview","country"]]

  convs['store_ids'] =  convs['preview'].str.findall('[0-9]{6,}').apply(', '.join)
  convs['suspensao_ate'] =  convs['preview'].str.findall('[0-9]{1,}\/[0-9]{1,}\/[0-9]{4,}').apply(', '.join)

  convs = convs[["kustomer_conversation_id","kustomer_datetime","phone","preview","store_ids","suspensao_ate","country"]]
  one = convs[(convs['preview'].str.contains("@")) & (convs['preview'].str.len() >= 4)]
  one['preview'] = one['preview'].str.lstrip()
  one.columns = one.columns.str.lower()
  one = one[(one['preview'].str.startswith('@', na=False))]
  one = one[one["preview"].str.contains('@rappi') == False]

  one = one[['kustomer_conversation_id','preview',"kustomer_datetime"]]
  one['date'] = pd.to_datetime(one['kustomer_datetime'], format="%Y/%m/%d").dt.strftime('%d/%m/%Y')
  one = one.drop_duplicates()
  one = one.rename(columns={"preview": "user_name"})
  one['user_name'] = one['user_name'].str.lower()

  two = convs[convs['store_ids'].str.len() >= 1] 
  two = two[(two['preview'].str.contains("motivo"))]
  two['motivo'] = two['preview'].str.split('motivo').str[-1]
  two = two[['kustomer_conversation_id','store_ids','suspensao_ate','motivo']]
  two = two.drop_duplicates()

  three = convs[(convs['country'].str.len() == 2) & ((convs['country'].str.contains("mx")) 
    | (convs['country'].str.contains("ar"))
    | (convs['country'].str.contains("cl"))
    | (convs['country'].str.contains("co"))
    | (convs['country'].str.contains("ec"))
    | (convs['country'].str.contains("cr"))
    | (convs['country'].str.contains("pe"))
    | (convs['country'].str.contains("uy"))
    | (convs['country'].str.contains("mx"))
    )]

  three = three[['kustomer_conversation_id','country']]

  convs = pd.merge(one,two, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['store_ids'].notnull()]
  convs = pd.merge(convs,three, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['store_ids'].notnull()]

  #########
  convs = pd.merge(convs,reported, how='left',on=['kustomer_conversation_id'])
  convs = convs[convs['sync_at'].isnull()]


  convs = convs[['kustomer_conversation_id','user_name','kustomer_datetime','date','store_ids','suspensao_ate','motivo','sync_at','country']]

  ######gmv lost

  if not convs.empty:
      convs = explode_str(convs, 'store_ids', ',')
      convs = convs.drop(['sync_at'], axis=1)

      gmv_lost['days'] = gmv_lost['days'].astype(float)
      gmv_lost['hours'] = gmv_lost['hours'].astype(float)
      gmv_lost['store_id'] = gmv_lost['store_id'].astype(str)
      gmv_lost['finished_orders_per_hour'] = gmv_lost['finished_orders_per_hour'].astype(float)
      gmv_lost['gmv_aov_usd'] = gmv_lost['gmv_aov_usd'].astype(float)

      convs1 = pd.merge(convs, gmv_lost, how='left', left_on=convs["store_ids"], right_on=gmv_lost['store_id']).rename(
          columns={'days': 'day_gmv', 'hours': 'hour_gmv'})
      convs1 = convs1.drop(['key_0', 'store_id'], axis=1)
      convs1 = convs1.fillna(0)

      convs1 = convs1.replace(0, np.nan)

      convs1['suspensao_ate'] = convs1['suspensao_ate'].replace('', "01/01/2010")
      convs1['suspensao_ate_datetime'] = pd.to_datetime(convs1['suspensao_ate'], format="%d/%m/%Y", errors='coerce')
      convs1['date_datetime'] = pd.to_datetime(convs1['kustomer_datetime']).dt.tz_localize(None)
      convs1['diff_date'] = (convs1['suspensao_ate_datetime'] - convs1['date_datetime'])
      convs1['suspensao_ate'] = convs1['suspensao_ate'].replace("01/01/2010", '')
      df_time = convs1['diff_date'].dt.components[['days', 'hours', 'minutes']]
      convs = pd.concat([convs1, df_time], axis=1)

      convs.loc[convs.days >= 60, 'days'] = 60
      convs['days_hour'] = convs['days'] * 24
      convs['time_total'] = ((convs['days_hour'] + convs['hours']) / convs['hour_gmv']) / convs['day_gmv']
      convs['time_total'] = convs['time_total'].fillna(0)

      convs['gmv_lost'] = convs['gmv_aov_usd'] * convs['finished_orders_per_hour'] * convs['time_total']
      convs.loc[convs.gmv_lost < 0, 'gmv_lost'] = 0
      convs['gmv_lost'] = convs['gmv_lost'].fillna(0)

      convs_1 = convs.groupby(
          ['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date', 'country'])[
          'store_ids'].agg(
          [('store_ids', lambda x: ', '.join(map(str, x)))]).reset_index()

      convs_2 = convs.groupby(
          ['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo',
           'diff_date', 'country']).sum().reset_index()[
          ['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate', 'motivo', 'diff_date',
           'gmv_lost', 'country']]

      if not convs.empty:
          convs = pd.merge(convs_1, convs_2,
                       on=['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'suspensao_ate',
                           'motivo', 'diff_date', 'country'])
      else:
          print('convs null')

      print(convs)
      ####

      convs = convs.drop_duplicates()

      to_upload = convs

      tz = pytz.timezone('America/Buenos_Aires')
      current_time = datetime.now(tz)
      to_upload['sync_at'] = current_time
      to_upload = to_upload[['kustomer_conversation_id', 'user_name', 'kustomer_datetime', 'date', 'store_ids', 'sync_at', 'country','gmv_lost']]
      print("insert into snowflake")
      print(to_upload)
      if not to_upload.empty:
        snow.upload_df_occ(to_upload, 'occ_wpp_log')

      if not convs.empty:
          for index, row in convs.iterrows():
              store_ids = row['store_ids']
              dia = row['date']
              country = row['country']
              print(row['gmv_lost'])
              gmv_lost = '{:,.2f}'.format(row['gmv_lost'])
              if row['gmv_lost'] >= 0 and row['user_name'] != 'nan' and row['suspensao_ate'] != 'nan' and len(store_ids) != 0:
                  text = '''
                  Country: :flag-{country}:
                  <{user}> :arrowred: solicitó la suspensión de las tiendas *{store_ids}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}*
            
                  :burning-money: *GMV Lost*: La suspensión provoca una *pérdida de US$ {gmv_lost}* :dollar:
                  '''.format(
                    user=row['user_name'],
                    country=country,
                    store_ids=store_ids,
                    data=row['suspensao_ate'],
                    motivo=row['motivo'],
                    dia=row['date'],
                    gmv_lost=gmv_lost
                  )
                  print(text)
              elif row['user_name'] != 'nan' and row['suspensao_ate'] != 'nan' and len(store_ids) != 0:
                  text = '''
                  Country: :flag-{country}:
                  <{user}> :arrowred: solicitó la suspensión de las tiendas *{store_ids}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* 
                  '''.format(
                    user=row['user_name'],
                    store_ids=store_ids,
                    data=row['suspensao_ate'],
                    motivo=row['motivo'],
                    dia=row['date'],
                    country=country)
              else:
                  print('null')

              slack.bot_slack(text,'C021RGAGVV2')

      else:
          print('convs null')
  else:
      print('df is null')