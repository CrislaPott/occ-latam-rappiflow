import itertools
import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
import numpy as np
import pytz
from datetime import date, datetime, timedelta



def get_logs():
  query = '''--no_cache
  select kustomer_id, sync_at
  from ops_occ.wpp_log_product_massive
  group by 1,2
  '''
  df_log = snow.run_query(query)
  return df_log

def massive():
  try:
    reported = get_logs()
  except:
    sys.exit()

  print("logs snowflake - done")

  df = pd.read_csv("https://docs.google.com/spreadsheets/d/12GUCEjTt44nl_J78JGkIhS0kKr7fwvUjExRFWRLtHwc/export?format=csv&gid=0",error_bad_lines=False, low_memory=False)

  print("read sheets - done")
  print(date.today())
  tz = pytz.timezone('America/Bogota')
  current_time = datetime.now(tz)
  df['fecha_de_solicitude'] = pd.to_datetime(df['fecha_de_solicitude'], format="%d/%m/%Y", errors='coerce').dt.tz_localize(None)
  #df=df.loc[df['fecha_de_solicitude'].dt.date >= current_time.date()]
  df['fecha_de_solicitude'] =df['fecha_de_solicitude'].dt.strftime("%d/%m/%Y")
  print(df)
  print("sheets only since yesterday - done")

  # evitar preenchimento em andamento
  df = df[
      (df['@slack'].notnull())
      & (df['kustomer_id'].notnull())
      & (df['id_producto'].notnull())
      & (df['store_id'].notnull())
      & (df['pais'].notnull()
         )]

  ## fix tag slack
  df['@slack'] = df['@slack'].str.rstrip()
  df['@slack'] = df['@slack'].str.lstrip()

  df = df[['fecha_de_solicitude','pais','@slack','store_id','accion','id_producto','motivo','fecha_de_reactivacion','kustomer_id','reactivado','store_product_id']]

  print("reported as str")
  reported['kustomer_id'] = reported['kustomer_id'].astype(str)
  print("df as str")
  df['kustomer_id'] = df['kustomer_id'].astype(str)

  print("merge")
  df = pd.merge(df, reported, how='left', left_on=df['kustomer_id'], right_on=reported['kustomer_id'])
  print("sync_at is null")
  df = df[df['sync_at'].isnull()]
  print("drop columns")
  df = df.drop(['key_0', 'sync_at','kustomer_id_y'], axis=1)
  print("rename")
  df = df.rename(columns={'store_id_x': 'store_id', 'kustomer_id_x': 'kustomer_id'})

  countries = {
      'Mexico': 'mx',
      'Brasil': 'br',
      'Colombia': 'co',
      'Chile': 'cl',
      'Uruguay': 'uy',
      'Costa Rica': 'cr',
      'Ecuador': 'ec',
      'Peru': 'pe',
      'Argentina': 'ar',
  }

  df['pais'] = df['pais'].str.strip()
  df['pais'] = df['pais'].map(countries)

  print("starts if")
  if not df.empty:

      tz = pytz.timezone('America/Bogota')
      current_time = datetime.now(tz)
      df['sync_at'] = current_time

      df = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'accion','fecha_de_reactivacion','pais','sync_at','kustomer_id','motivo','store_product_id']]
      df['store_product_id'] = df['store_product_id'].astype(str)
      df1 = df
      print('snowflake')
      print(df1)
      snow.upload_df_occ(df1, 'wpp_log_product_massive')



def blacklist_do_wpp():
  try:
    reported = get_logs()
  except:
    sys.exit()

  df = pd.read_csv("https://docs.google.com/spreadsheets/d/12GUCEjTt44nl_J78JGkIhS0kKr7fwvUjExRFWRLtHwc/export?format=csv&gid=0",error_bad_lines=False, low_memory=False)

  df = df[
  (df['@slack'].notnull())
  & (df['kustomer_id'].notnull())
  & (df['id_producto'].notnull())
  & (df['store_id'].notnull())
  & (df['pais'].notnull()
  & (df['blacklist'].notnull())
  )]

  print(df)

  ## fix tag slack
  df['@slack'] = df['@slack'].str.rstrip()
  df['@slack'] = df['@slack'].str.lstrip()

  reported['kustomer_id'] = reported['kustomer_id'].astype(str)
  df['id_producto'] = df['id_producto'].astype(str)
  df['store_id'] = df['store_id'].astype(str)

  df = pd.merge(df, reported, how='left',on=['kustomer_id'])
  df = df[df['sync_at'].isnull()]

  countries = {
     'Mexico': 'mx',
     'Brasil': 'br',
     'Colombia': 'co',
     'Chile': 'cl',
     'Uruguay': 'uy',
     'Costa Rica': 'cr',
     'Ecuador': 'ec',
     'Peru': 'pe',
     'Argentina': 'ar',
     }

  df['pais'] = df['pais'].str.strip()
  df['pais'] = df['pais'].map(countries)
  if not df.empty:

    tz = pytz.timezone('America/Bogota')
    current_time = datetime.now(tz)
    df['sync_at'] = current_time

    log = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'accion','fecha_de_reactivacion','pais','sync_at','kustomer_id']]
    df = df[['fecha_de_solicitude', '@slack', 'id_producto', 'store_id', 'pais','sync_at','kustomer_id','blacklist']]

    snow.upload_df_occ(df, 'blacklist_wpp')

print("wpp")
blacklist_do_wpp()
print('massive')
massive()

