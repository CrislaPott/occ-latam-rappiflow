import itertools
import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
import numpy as np
import pytz

def logs():
    query = '''

    select kustomer_conversation_id as kustomer_conversationid
    from ops_occ.occ_productos_log
    where country = 'BR'
    group by 1
    '''
    df = snow.run_query(query)
    return df

def blacklist():
    query = '''
       select distinct product_id::text as productid from ops_occ.nl_products_bl_vw
       '''
    df = snow.run_query(query)
    return df

def whitelist(country):
    query = ''' select distinct op.product_id::text as productid
    from {country}_core_orders_public.order_product_vw op
    join {country}_pglr_ms_grability_products_public.products_vw p ON op.product_id = p.id
    join {country}_pglr_ms_grability_products_public.product_store_vw  PS ON PS.PRODUCT_ID = op.product_id and ps.store_id = op.store_id
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='{country}'
         '''.format(country=country)
    df = snow.run_query(query)
    return df

def redash_query(first, second):
    query = f"""
    --no_cache
    WITH a AS
  (SELECT max(id) AS id_maximo
   FROM conversations
   ),
     b AS
  (SELECT DISTINCT kustomer_conversation_id,
                   kustomer_created_at,
                   country,
                   tags, ended_at
   FROM a
   INNER JOIN conversations conv ON conv.id >= (a.id_maximo - {first})
   ),
     c AS
  (SELECT max(id) AS id_maximo
   FROM messages),
     d AS
  (SELECT kustomer_conversation_id,
          preview,
          m.FROM AS phone, m.kustomer_message_id
   FROM c
   INNER JOIN messages m ON m.id >= (c.id_maximo - {second})
   WHERE m.direction_type = 'followup-in' )
SELECT b.kustomer_conversation_id,
       kustomer_created_at AS kustomer_datetime,
       country,
       phone,
       preview, kustomer_message_id
FROM b
LEFT JOIN d ON d.kustomer_conversation_id::text=b.kustomer_conversation_id::text
WHERE (tags::text ILIKE '%ffd134d227d2442ade5ed%')
          and ended_at is not null
          and country = 'br'
          """

    df = redash.run_query(3414,query)
    if df.shape[0] == 0:
      sys.exit(0)
    return df


query_snowflake = '''--no_cache
    select * from
    (
    WITH b AS
      (SELECT DISTINCT kustomer_conversation_id,
                       kustomer_created_at,
                       country,
                       tags, ended_at
       FROM CO_PG_MS_KUSTOMER_ETL_PUBLIC.conversations conv
       ),
         d AS
      (SELECT kustomer_conversation_id,
              preview,
              m."FROM" AS phone,m.kustomer_message_id
       FROM CO_PG_MS_KUSTOMER_ETL_PUBLIC.messages m
       WHERE m.direction_type = 'followup-in' )

    SELECT b.kustomer_conversation_id,
           kustomer_created_at AS kustomer_datetime,
           country,
           phone,
           preview,kustomer_message_id
    FROM b
    JOIN d ON d.kustomer_conversation_id=b.kustomer_conversation_id
    WHERE (tags::text ILIKE '%ffd134d227d2442ade5ed%')
              and ended_at is not null

    union all

    select * from (
    --no_cache
    WITH b AS
      (SELECT DISTINCT kustomer_conversation_id,
                       kustomer_created_at,
                       country,
                       tags, ended_at
       FROM informatica.CO_PG_MS_KUSTOMER_ETL_PUBLIC.conversations conv
       ),
         d AS
      (SELECT kustomer_conversation_id,
              preview,
              m."FROM" AS phone,m.kustomer_message_id
       FROM informatica.CO_PG_MS_KUSTOMER_ETL_PUBLIC.messages m
       WHERE m.direction_type = 'followup-in' )

    SELECT b.kustomer_conversation_id,
           kustomer_created_at AS kustomer_datetime,
           country,
           phone,
           preview,kustomer_message_id
    FROM b
    JOIN d ON d.kustomer_conversation_id=b.kustomer_conversation_id
    WHERE  (tags::text ILIKE '%ffd134d227d2442ade5ed%')
              and ended_at is not null)

    order by 2 desc
        )
    where kustomer_datetime::date >= current_date::date - 2
    
        '''


def products_br(df_whitelist):

    try:
        df = redash_query('10000', '60000')
        print('redash')

    except:
        df = snow.run_query(query_snowflake)
        print('snowflake')

    df['return_date'] = df['preview'].str.findall('[0-9]{1,}\-[0-9]{1,}\-[0-9]{4,}').apply(', '.join)
    df['date'] = pd.to_datetime(df['kustomer_datetime'], format="%Y/%m/%d").dt.strftime('%d/%m/%Y')
    df1 = df[df['preview'].str.contains('/')]
    df2 = df1['preview'].str.split('\n').apply(pd.Series)
    df1 = pd.concat([df1, df2], axis=1)

    pais = []
    store_id = []
    action = []
    product_id = []
    reason = []
    date = []
    index = []
    if not df1.empty:
        try:
            for i in range(0, (len(df1.columns) - len(df.columns))):
                df_into = df1[i].str.split(' / ').apply(pd.Series)
                pais.append(df_into[0])
                store_id.append(df_into[1])
                action.append(df_into[2])
                product_id.append(df_into[3])
                reason.append(df_into[4])
                index.append(df_into.index)

            pais = list(itertools.chain.from_iterable(pais))
            store_id = list(itertools.chain.from_iterable(store_id))
            action = list(itertools.chain.from_iterable(action))
            product_id = list(itertools.chain.from_iterable(product_id))
            reason = list(itertools.chain.from_iterable(reason))

            index = list(itertools.chain.from_iterable(index))

            df3 = pd.DataFrame([index, pais, store_id, action, product_id, reason, date]).T.rename(
                columns={0: 'Unnamed0', 1: "pais", 2: "store_id", 3: 'action', 4: "product_id", 5: 'reason'}).dropna(how='all')
            df1 = df1.reset_index()
            df4 = pd.merge(df3, df1, how='left', left_on=df3['Unnamed0'], right_on=df1['index'])
            df4 = df4[['kustomer_conversation_id', 'kustomer_datetime', 'date', 'phone', 'pais', 'store_id', 'action', 'product_id',
                       'reason', 'return_date']].rename(columns={"pais": "country"})

            df4 = df4[df4['country'].notna()]
            user = df[df['preview'].str.contains('@')][['kustomer_conversation_id', 'preview']].rename(
                columns={'preview': 'user_name', 'kustomer_conversation_id': 'kustomer_conversationid'})
            df4 = pd.merge(df4, user, how='left', left_on=df4['kustomer_conversation_id'], right_on=user['kustomer_conversationid'])
            df4 = df4.drop(['key_0', 'kustomer_conversationid'], axis=1)
            df4 = df4.drop_duplicates()

            tz = pytz.timezone('America/Bogota')
            current_time = datetime.now(tz)
            df4['sync_at'] = current_time

            print(df4)

            #### logs data

            df_logs = logs()

            df = pd.merge(df4, df_logs, how='left', left_on=df4['kustomer_conversation_id'], right_on=df_logs['kustomer_conversationid'])
            df = df[(df['kustomer_conversationid'].isnull())]
            df = df.drop(['key_0', 'kustomer_conversationid'], axis=1)

            print(df)

            snow.upload_df_occ(df, 'occ_productos_log')

            #### blacklist data

            df_blacklist = blacklist()
            df['product_id'] = df['product_id'].astype(float)
            df_blacklist['productid'] = df_blacklist['productid'].astype(float)

            df_bl = pd.merge(df4, df_blacklist, how='left', left_on=df['product_id'],
                             right_on=df_blacklist['productid']).rename(columns={'productid': 'productid_blacklist'})
            df_bl = df_bl.drop(['key_0'], axis=1)

            #### whitelist data

            print(df_whitelist)
            df_whitelist['productid'] = df_whitelist['productid'].astype(float)
            df_final = pd.merge(df_bl, df_whitelist, how='left', left_on=df_bl['product_id'],
                             right_on=df_whitelist['productid']).rename(columns={'productid': 'productid_whitelist'})

            print(df_final)

            if not df_final.empty:
                try:
                    for index, row in df_final.iterrows():
                        if row['productid_blacklist'] > 0:

                            ####product id presente na blacklist

                            if row['action'] == 'Desativar' or row['action'] == 'desativar':
                                text = '''
                                                  Country: :flag-{country}:
                                                  <{user}> solicitó el apagado del :avocado: producto *{product_id}* de la :building_construction:   tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* :no_entry:
                                                  '''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    motivo=row['reason'],
                                    dia=row['date'],

                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            elif row['action'] == 'Ativar' or row['action'] == 'ativar':
                                text = '''
                                                      Country: :flag-{country}:
                                                      <{user}> solicitó el encendido del :avocado: producto *{product_id}* de la :building_construction:  tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* :doblecheck:
                                                      :warning: El producto {productid_blacklist} está en la blacklist. 
                                                      '''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    motivo=row['reason'],
                                    dia=row['date'],
                                    productid_blacklist=int(row['productid_blacklist']),
                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            else:
                                print('action não encontrada, está escrito assim:', row['action'])

                        elif row['productid_whitelist'] > 0:

                            ####product id presente na whitelist

                            if row['action'] == 'Desativar' or row['action'] == 'desativar':
                                text = '''
                                                  Country: :flag-{country}:
                                                  <{user}> solicitó el apagado del :avocado: producto *{product_id}* de la :building_construction:   tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* :no_entry:
                                                  :alert: El producto {productid_whitelist} no puede ser desactivado por ser un top seller :alert:'''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    productid_whitelist=row['productid_whitelist'],
                                    motivo=row['reason'],
                                    dia=row['date'],
                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            elif row['action'] == 'Ativar' or row['action'] == 'ativar':
                                text = '''
                                                      Country: :flag-{country}:
                                                      <{user}> solicitó el encendido del :avocado: producto *{product_id}* de la :building_construction:  tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* :doblecheck:
                                                      '''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    motivo=row['reason'],
                                    dia=row['date'],
                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            else:
                                print('action não encontrada, está escrito assim:', row['action'])
                        else:
                            ####nem whitelist nem backlist

                            if row['action'] == 'Desativar' or row['action'] == 'desativar':
                                text = '''
                                Country: :flag-{country}:
                                <{user}> solicitó el apagado del :avocado: producto *{product_id}* de la :building_construction:   tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* hasta *{data}* :no_entry:
                                '''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    motivo=row['reason'],
                                    dia=row['date'],
                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            elif row['action'] == 'Ativar' or row['action'] == 'ativar':
                                text = '''
                                Country: :flag-{country}:
                                <{user}> solicitó el encendido del :avocado: producto *{product_id}* de la :building_construction:  tienda *{store_id}*, en el dia *{dia}*, por el motivo *{motivo}* :doblecheck:
                                '''.format(
                                    user=row['user_name'],
                                    product_id=row['product_id'],
                                    store_id=row['store_id'],
                                    data=row['return_date'],
                                    motivo=row['reason'],
                                    dia=row['date'],
                                    country=row['country'].lower()
                                )
                                print(text)
                                slack.bot_slack(text, 'C02C5HGFZLJ')

                            else:
                                print('action não encontrada, está escrito assim:', row['action'])
                except Exception as e:
                    print(e)
            else:
                print('df_final null')

        except Exception as e:
            print(e)

    else:
        print('df1 null')


countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']
df_whitelist_ = []
for country in countries:
    try:
        print(country)
        df = whitelist(country)
        df_whitelist_.append(df)
        df_whitelist = pd.concat(df_whitelist_, ignore_index=True)
    except Exception as e:
        print(e)

products_br(df_whitelist)
