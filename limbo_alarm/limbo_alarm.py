#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# get_ipython().system('jupyter nbconvert --to script limbo_alarm.ipynb')


# In[1]:


from limbo_alarm_function import process


# In[5]:

channel = [
    "C01672B528Y",
    "C01C1LSQREW",
    "C01GMHJRDM2",
    "C01EEPH2ECU",
    "C01EEPH2ECU",
    "C01E5G49L1K",
    "C01E5G49L1K",
    "C01E5G49L1K",
    "C01E5G49L1K",
]
for index, country in enumerate(["BR", "CO", "MX", "AR", "CL", "PE", "UY", "EC", "CR"]):
    try:
        process(country=country, canal=channel[index])  # canal-mio = 'U026TUGHJTV'
    except Exception as e:
        print("Error in ", country)
        print(e.args)
