#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# get_ipython().system('jupyter nbconvert --to script limbo_alarm.ipynb')


# In[1]:


import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)


# In[27]:

def process(country, canal):

    if country=='CO':
        country = 'CO'
        interval=-5
        interval1=-6
        BD = 2453
        physical_store_id = (19,6,7,3,51,1,337,8,2,4,7125,40,610,25,13,238,275,11,33,7634,153,7414,152,243,21,26,12,7957,20,
                311,109,110,7213, 200,106,199,254,22,72,55,15,68,52,17,38,43,62,16,5,10,24,9,23,7877,392,7876,445)
        flag = '\U0001F1E8\U0001F1F4'
        query = True

    elif country=='BR':
        country = 'BR'
        interval=-3
        interval1=-4
        BD = 2451
        physical_store_id = (19,38,40,50,264,265,276,278,284,286,302,303,304,310,311,313,335,340,345,349,360,369,384,392,407,433,462,
                            467,503,508, 520,537,582,626,642,649,700,726,7571,7577,7578,7582,7583,7593,7600,7602,7605,7610,7612,7842,
                            7876,7923,7924,7947,7948,7952,8001,8157,8162,8188,8396,8397,8706,8717,8718,8723,8724,8725,8726,8727,8730,
                            8762,8764,8778,8780,8781,8784,8785,8786,8787,8789,8791,8793,8813,8816,8819,8820,8821,8822,8823,8833,8868,
                            8874,8880,9130,9131,9133,9134,9135,9137,9138,9139,9140,9177,9178,9180,9181,9205,9215,9218,9221,9222,9223,
                            9224,9225,9300,9301,9417,9420,9426,9454,9504,9514,9536,9552,9594,9597,9641,9649,9658,9710,10076,10464,
                            10542,11260,11374,11679,11889,12106,12147,12246,12823,12824,13179,13240,13313,13317,13789,13820,13981,
                            14224,14393,14394,14395,14409,14498,14750,14849,14909,14991,15205,15243,15794,31957,32043,32139,32140,
                            32141,32251,32252,32803,32804,32805,32833,32898,35083,35084,38618,43440,44005,44006,45288,45460,46892,
                            46924,46969,47377,47379,45459,45385,45458,701,32221,32251,45465,8675, 13788, 32472, 32473, 45467, 45468, 
                            45470, 46730,13676,13686)
        physical_store_id1 = (467,474,475,520,596,626,7586,7842,8874,9142,9741,10464,11277,11278,11889,13193,14224,14244,14246,
                            14257,14258,14259,14260,14263,14264,14265,14266,14267,14269,14271,14394,14395,14515,14518,14519,14520,
                            14768,14769,14770,14849,15152,43994,15154,15205,15214,15243,15270,15272,15274,32469,32470,32471,32664,
                            33189,33192,43441,7601,45385,9136)
        physical_store_id2 = (467,474,475,520,596,626,7586,7842,8874,9142,9741,10464,11277,11278,11889,12106,13193,14224,14244,14246,
                            14257,14258,14259,14260,14263,14264,14265,14266,14267,14269,14271,14394,14395,14515,14518,14519,14520,
                            14768,14769,14770,14849,15152,15154,15205,15214,15243,15270,15272,15274,32469,32470,32471,32664,33189,
                            33192,43441,7601,45385,9136)
        flag = '\U0001F1E7\U0001F1F7'
        query = False

    elif country=='MX':
        country = 'MX'
        interval=-5
        interval1 =-6
        BD = 2454
        physical_store_id = (2,3,4,6,7,10,11,12,13,17,18,19,22,23,25,26,27,28,33,48,49,62,77,78,79,158,159,167,168,169,170,172,173,
                174,175,176,200,202,236,442,443,444,445,446,447,448,449,450,456,457,500,502,608,627,630,1102,1107,1112,1123,1135,
                1140,1478,1544,1545,1546,1547,1563,1568,1570,1572,1573,1712,17191,17438,17445,17447,17449,17474,17494,17658,17668,
                17737,18682,20180,20382,21334,23898)
        flag = '\U0001F1F2\U0001F1FD'
        query = True


    elif country=='AR':
        country = 'AR'
        interval=-3
        interval1=-4
        BD = 2427
        physical_store_id = (2,3,4,7,8,9,10,35,36,37,38,39,40,41,42,43,44,45,46,48,50,51,52,53,55,62,63,64,641,642,644,743,753,
                756,784,790,878,920,921,953,954,955,957,960,961,981,982,983,986,987,1002,1003,1004,1005,1044,1057,
                1117,1350,1351,1526,1527,1531,1872,1998,2060,2061,2091,2098,2153,2154,2233,2613,2614,2616,2617,
                2618,2644,2645,2657,2658,2659,2660,2940,2944,2962,2963,3258,3261,3278,3314,3365)
        flag = '\U0001F1E6\U0001F1F7'
        query = True


    elif country=='CL':
        country = 'CL'
        interval= -4
        interval1= -5
        BD = 2428
        physical_store_id = (12,14,15,18,28,38,39,45,56,81,115,207,241,265,290,292,312,317,346,353,354,355,361,3674,3709,3873,3884,
                3904,3999,4003,4017,4031,4046,4053,4298,4345,4473,4771,5027,5028,5128,5129,5130,5131,5132,5133,5134,5135,5136,
                5469,5470,359,5874,5886,6845,5920,3888,3887,3885,6092,6109,6857,6844)
        flag = '\U0001F1E8\U0001F1F1'
        query = True


    elif country=='PE':
        country = 'PE'
        interval=-5
        interval1= -6
        BD = 2452
        physical_store_id = (9,44,61,66,68,138,180,183,193,205,216,222,224,239,242,244,263,265,2736,2804,3014,3016,3405,3409,3410,
                3419,3462,3463,3524,3525)
        flag = '\U0001F1F5\U0001F1EA'
        query = True

    elif country=='UY':
        country = 'UY'
        interval=-3
        interval1= -4
        BD = 2429
        physical_store_id = (6857,6844,1128675)
        flag = '\U0001F1FA\U0001F1FE'
        query = True

    elif country=='EC':
        country = 'EC'
        interval= -5
        interval1= -6
        BD = 2555
        physical_store_id = (22,23,24,25,26,28,29,30,31,32,34,62,76,101,119,127,152,153,166,167,230,231)
        flag = '\U0001F1EA\U0001F1E8'
        query = True

    elif country=='CR':
        country = 'CR'
        interval= -6
        interval1= -7
        BD = 2551
        physical_store_id = (1,12,14,28,65,71,74,82,144,145,146,147,148,149)
        flag = '\U0001F1E8\U0001F1F7'
        query = True


    # In[28]:

    print('init_', country)

    if query==True:
    
        df = run_query_r(BD, """
        select 
        physical_store_id::text, 
        (os.place_at - interval '{interval1}') as slot,
        count(distinct os.order_id) as orders, 
        string_agg(os.order_id::text,',') as order_ids
        from order_summary os
        left join order_summary_calculated osc on osc.order_id = os.order_id
        where date_trunc('h',place_at - interval '{interval1}') = date_trunc('h', now() - interval '{interval}')
        and ((os.state = 'unassigned_order') or (os.shopper_id is null and os.state not ilike '%cancel%' 
        and os.state not ilike '%finished%'))
        --and (os.state not ilike '%cancel%' and os.state not ilike '%finished%')
        and was_taken = true
        and physical_store_id in {physical_store_id}
        group by 1, 2
        order by 2, 3 desc
        """.format(physical_store_id=physical_store_id, interval=interval, interval1=interval1))
        
    else: 
        
        df = run_query_r(BD, """
        with a as 
        (
            select 
            physical_store_id, 
            os.place_at, 
            order_id, 
            state, 
            shopper_id, 
            os.store_type as type
            from order_summary os
            where date_trunc('h',(case when physical_store_id in {physical_store_id1} then (os.place_at - interval '1h')
            else (os.place_at) end) - interval '{interval1}') = date_trunc('h',now() - interval '{interval}')
            and was_taken = true
        )

        select 
        physical_store_id::text, 
        os.type,
        case when physical_store_id in {physical_store_id2} then (os.place_at - interval '5h') else (os.place_at - interval '4h') end as slot, 
        count(distinct os.order_id) as orders, 
        string_agg(os.order_id::text,',') as order_ids, 
        (now() at time zone 'America/Sao_Paulo') as now_br 
        from a os
        where 
        ((os.state = 'unassigned_order') or (os.shopper_id is null and os.state not ilike '%cancel%' and os.state not ilike '%finished%'))
        and physical_store_id in {physical_store_id}
        group by 1,2,3,6
        order by 4 desc
        """.format(physical_store_id=physical_store_id, physical_store_id1=physical_store_id1, physical_store_id2=physical_store_id2, 
        interval=interval, interval1=interval1))

    print(df.shape)
    print('end_', country)
    df.head()


    # In[30]:


    if (len(df)>0):
        
        df_alarm= df.loc[df['orders']>2]

        print(df_alarm.shape)
        df_alarm.head()


    # In[ ]:


    canal_slack = canal

    if (len(df)>0):
        
        for index, row in df_alarm.iterrows():

            if (len(df_alarm)>5):

                main_text = '''
                \U000026A0 * Alarma Limbo shopper/super - {flag}*:
                Physical store id: {physical_store_id}
                Orders: {orders}
                Order ids: {order_ids}
                Slot: {slot}
                '''.format(
                    flag=flag,
                    physical_store_id =row['physical_store_id'],
                    orders =row['orders'],
                    order_ids = row['order_ids'],
                    slot = datetime.strptime(row['slot'], '%Y-%m-%dT%H:%M:%SZ').strftime("%m/%d/%Y %H:%M")
                    )

                print(main_text)
                bot_slack(main_text, canal_slack)