import os
import numpy as np
import pandas as pd 
from dotenv import load_dotenv
from redashAPI import RedashAPIClient
from airflow.models import Variable

load_dotenv()

Redash = RedashAPIClient(Variable.get('REDASH_API_KEY'), 'https://redash.rappi.com')

def get_databases(pattern):
    databases = pd.DataFrame(Redash.get('data_sources').json())
    filtered = databases[databases['name'].str.contains(pattern)]
    return filtered[['id', 'name']]

def run_query(database_id, query):
    results = Redash.query_and_wait_result(database_id, query, 240)
    df = df = pd.DataFrame(results.json()['query_result']['data']['rows'])
    return df


