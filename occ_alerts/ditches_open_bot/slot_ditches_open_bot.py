import os, sys, json, requests 
import pytz
from lib import redash, snowflake, sfx
from dotenv import load_dotenv
from slack import WebClient
from slack.errors import SlackApiError
from airflow.models import Variable
from datetime import datetime, timedelta

load_dotenv()

client = WebClient(token=Variable.get('SLACK_TOKEN'))

query_snowflake = """
with ditches as (
   select distinct
       slot_id,
       last_value(status) over (partition by slot_id order by created_at) as status,
        last_value(action_status) over (partition by slot_id order by created_at) as action_status
   from ops_occ.occ_valas_br
),

slots_table1 as (
   select
       id as slot_id,
       physical_store_id,
       -- extract(hour from datetime_utc_iso - interval '3 hours') as hour
        dateadd('hour', -3, datetime_utc_iso) as open_time
   from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
   where
        type = 'SHOPPER'
       and (datetime_utc_iso - interval '3 hours')::date = (convert_timezone('UTC',current_timestamp) - interval '3 hour')::date
       and extract(hour from datetime_utc_iso - interval '3 hours') = extract(hour from (convert_timezone('UTC',current_timestamp) - interval '1 hour'))
        and physical_store_id not in (467,474,475,520,596,626,7586,7842,8874,9142,9741,10464,11277,11278,11889,12106,13193,14224,14244,14246,14257,14258,14259,14260,14263,14264,14265,14266,14267,14269,14271,14394,14395,14515,14518,14519,14520,14768,14769,14770,14849,15152,15154,15205,15214,15243,15270,15272,15274,32469,32470,32471,32664,33189,33192)
),

slots_table2 as (
   select
       id as slot_id,
       physical_store_id,
       -- extract(hour from datetime_utc_iso - interval '3 hours') as hour
        dateadd('hour', -3, datetime_utc_iso) as open_time
   from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
   where
        type = 'SHOPPER'
       and (datetime_utc_iso - interval '3 hours')::date = (convert_timezone('UTC',current_timestamp) - interval '3 hour')::date
       and extract(hour from datetime_utc_iso - interval '3 hours') = extract(hour from (convert_timezone('UTC',current_timestamp)))
        and physical_store_id in (467,474,475,520,596,626,7586,7842,8874,9142,9741,10464,11277,11278,11889,12106,13193,14224,14244,14246,14257,14258,14259,14260,14263,14264,14265,14266,14267,14269,14271,14394,14395,14515,14518,14519,14520,14768,14769,14770,14849,15152,15154,15205,15214,15243,15270,15272,15274,32469,32470,32471,32664,33189,33192)
),

slots_table as (
    select * from slots_table1
    union all
    select * from slots_table2
)

select
    st.slot_id,
    status,
    physical_store_id,
    -- hour
    to_char(open_time, 'HH:00:00') as open_time,
    dayname(open_time) as open_day
from slots_table st
left join ditches d on d.slot_id = st.slot_id
where status = 'closed'
"""

slots = snowflake.run_query(query_snowflake)

if slots.shape[0] == 0:
    client.chat_postMessage(
        channel='C01GDLQB6GG',
        text='Rodada sem slots para reabertura'
    )
    sys.exit(0)

physical_store_ids = slots['physical_store_id'].unique().tolist()
physical_store_ids = ["'{}'".format(physical_store_id) for physical_store_id in physical_store_ids]
physical_store_ids = ','.join(physical_store_ids)

print(physical_store_ids)

query_redash = """
with summaries as (
    select
        physical_store_id,
        order_id,
        place_at
    from order_summary 
    where
    (place_at at time zone 'America/Sao_Paulo')::date = (now() at time zone 'America/Sao_Paulo')::date
    and state in ('finish', 'finished', 'pending_review')
    and was_taken = true
    and physical_store_id in ({})
),

order_delays as (
    select
        summaries.*,
        oe.created_at as finish_at,
        case when oe.created_at > (place_at + interval '1 hour') then 'delay' else 'no delay' end as flag
    from summaries
    left join order_events oe on oe.order_id = summaries.order_id 
    and oe.name='finish_order'
    and place_at at time zone 'America/Sao_Paulo' >= (now() at time zone 'America/Sao_Paulo' - interval '2 hour')
)

select 
    physical_store_id, 
    count(distinct case when flag = 'delay' then order_id else null end)::float / count(distinct order_id)::float as percentage_delay
from order_delays
group by physical_store_id
""".format(physical_store_ids)

store_delay = redash.run_query(2447, query_redash)

if store_delay.shape[0] == 0:
    client.chat_postMessage(
        channel='C01GDLQB6GG',
        text='Rodada sem slots para reabertura'
    )
    sys.exit(0)

try:
    # Filtra apenas as lojas com percentual de atraso inferior a 5% no dia
    store_delay = store_delay[store_delay['percentage_delay'] < 0.05]
except:
    client.chat_postMessage(
        channel='C01GDLQB6GG',
        text='Rodada sem slots para reabertura'
    )
    sys.exit(0)

if store_delay.shape[0] == 0:
    client.chat_postMessage(
        channel='C01GDLQB6GG',
        text='Rodada sem slots para reabertura'
    )
    sys.exit(0)

slots_to_open = slots.merge(store_delay, on='physical_store_id')

print(slots_to_open.head())

def change_slot_status(slot_id, status, iteration=0):
    """Change the slots status."""
    headers = {
        'Authorization': 'postman',
        'API_KEY': Variable.get('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
        'x-rappi-team': 'OCC',
        'x-application-id': 'rappiflow-latam'
    }
    data = {
        "is_closed": not status
    }
    response = requests.put(
        'http://services.rappi.com.br/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
        headers=headers,
        data=json.dumps(data)
    )
    
    if response.status_code == 200:
        return 'Sim'
    else:
        return 'Nao'

current_time = datetime.now(pytz.timezone("America/Bogota"))
    
try:
    if slots_to_open.shape[0] == 0:
        client.chat_postMessage(
            channel='C01GDLQB6GG',
            text='Rodada sem slots para reabertura'
        )
        
    if slots_to_open.shape[0] > 0:

        for index, row in slots_to_open.iterrows():
            percentage_delay = row['percentage_delay']
            physical_store_id = str(row['physical_store_id'])
            slot_id = str(row['slot_id'])
            stores_zero_delay = [520, 276, 19, 467, 14224]
            country = 'BR'
            reason = 'slot ditches open'
            opened_at = current_time.strftime("%Y-%m-%d %H:%M:%S")

            if physical_store_id not in stores_zero_delay:
                change_status = change_slot_status(slot_id, True, iteration=0)
                sfx.send_events_slotDitchesOpen(slot_id, physical_store_id, country, reason, opened_at)

            elif (physical_store_id in stores_zero_delay) and (percentage_delay == 0):
                change_status = change_slot_status(slot_id, True, iteration=0)
                sfx.send_events_slotDitchesOpen(slot_id, physical_store_id, country, reason, opened_at)

            else:
                change_status = 'Nao'
            
            texto = """
            Realizando a abertura do slot:

            *Physical Store ID*: {physical_store_id}
            *Slot ID*: {slot_id}
            *Horário de Abertura do Slot*: {open_time}
            *Status modificado*: {change_status}
            """.format(
                slot_id=row['slot_id'],
                physical_store_id=row['physical_store_id'],
                open_time=row['open_time'],
                change_status=change_status
            )
            
            if physical_store_id not in stores_zero_delay:
                client.chat_postMessage(
                    channel='C01GDLQB6GG',
                    text=texto
                )
            if (physical_store_id in stores_zero_delay) and (percentage_delay == 0):
                client.chat_postMessage(
                    channel='C01GDLQB6GG',
                    text=texto
                )
except:
    print('Deu ruim')