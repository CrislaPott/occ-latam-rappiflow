import os, sys, json, requests 
from lib import redash, snowflake
from dotenv import load_dotenv
from slack import WebClient
from slack.errors import SlackApiError

load_dotenv()

client = WebClient(token=os.getenv('SLACK_TOKEN'))

query_snowflake = """
with

ditches as (
	select distinct
	    slot_id,
	    last_value(status) over (partition by slot_id order by created_at) as status,
        last_value(action_status) over (partition by slot_id order by created_at) as action_status
	from BR_WRITABLE.occ_valas_ar
),

slots_table as (
	select
		id as slot_id,
		physical_store_id,
		-- extract(hour from datetime_utc_iso - interval '3 hours') as hour
        dateadd('hour', -3, datetime_utc_iso) as open_time
	from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots
	where
        type = 'SHOPPER'
		and (datetime_utc_iso - interval '3 hours')::date = (convert_timezone('UTC',current_timestamp) - interval '3 hour')::date
		and extract(hour from datetime_utc_iso - interval '3 hours') = extract(hour from (convert_timezone('UTC',current_timestamp) - interval '1 hour'))
)

select
    st.slot_id,
    status,
    physical_store_id,
    -- hour
    to_char(open_time, 'HH:00:00') as open_time,
    dayname(open_time) as open_day
from slots_table st
left join ditches d on d.slot_id = st.slot_id
where status = 'closed'
and action_status = True
"""

slots = snowflake.run_query(query_snowflake)

if slots.shape[0] == 0:
    client.chat_postMessage(
        channel='C01J24CUYM7',
        text='Rodada sem slots para reabertura'
    )
    sys.exit(0)

physical_store_ids = slots['physical_store_id'].unique().tolist()
physical_store_ids = ["'{}'".format(physical_store_id) for physical_store_id in physical_store_ids]
physical_store_ids = ','.join(physical_store_ids)

query_redash = """
with summaries as (
    select
        physical_store_id,
        order_id,
        place_at
    from order_summary 
    where
    (place_at at time zone 'America/Sao_Paulo')::date = (now() at time zone 'America/Sao_Paulo')::date
    and state in ('finish', 'finished', 'pending_review')
    and was_taken = true
    and physical_store_id in ({})
),

order_delays as (
    select
        summaries.*,
        oe.created_at as finish_at,
        case when oe.created_at > (place_at + interval '1 hour') then 'delay' else 'no delay' end as flag
    from summaries
    left join order_events oe on oe.order_id = summaries.order_id 
    and oe.name='finish_order'
    and place_at at time zone 'America/Sao_Paulo' >= (now() at time zone 'America/Sao_Paulo' - interval '2 hour')
)

select 
    physical_store_id, 
    count(distinct case when flag = 'delay' then order_id else null end)::float / count(distinct order_id)::float as percentage_delay
from order_delays
group by physical_store_id
""".format(physical_store_ids)

store_delay = redash.run_query(2430, query_redash)

# Filtra apenas as lojas com percentual de atraso inferior a 5% no dia
store_delay = store_delay[store_delay['percentage_delay'] < 0.05]

slots_to_open = slots.merge(store_delay, on='physical_store_id')

def change_slot_status(slot_id, status, iteration=0):
    """Change the slots status."""
    headers = {
        'Authorization': 'postman',
        'API_KEY': os.getenv('CPGOPS_API_KEY'),
        'Content-Type': 'application/json',
    }
    data = {
        "is_closed": not status
    }
    response = requests.put(
        'http://services.rappi.com.ar/api/cpgops-gateway-ms/stores/slots/{}'.format(str(slot_id)),
        headers=headers,
        data=json.dumps(data)
    )
    
    if response.status_code == 200:
        return 'Sim'
    else:
        return 'Nao'
    
try:
    if slots_to_open.shape[0] == 0:
        client.chat_postMessage(
            channel='C01J24CUYM7',
            text='Rodada sem slots para reabertura'
        )
    if slots_to_open.shape[0] > 0:
        for index, row in slots_to_open.iterrows():
            change_status = change_slot_status(int(row['slot_id']), True, iteration=0)
            
            texto = """
            Realizando a abertura do slot:

            *Physical Store ID*: {physical_store_id}
            *Slot ID*: {slot_id}
            *Horário de Abertura do Slot*: {open_time}
            *Status modificado*: {change_status}
            """.format(
                slot_id=row['slot_id'],
                physical_store_id=row['physical_store_id'],
                open_time=row['open_time'],
                change_status=change_status
            )
            
            client.chat_postMessage(
                channel='C01J24CUYM7',
                text=texto
            )
except:
    print('Deu ruim')