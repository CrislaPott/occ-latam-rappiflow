# The structure for this Dockerfile comes from the `onbuild` version of Astronomer.
# https://github.com/astronomer/astronomer/blob/master/docker/airflow/1.10.5/onbuild/Dockerfile
ARG RAPPI_DOCKER_REGISTRY=registry.rappiai.com
FROM ${RAPPI_DOCKER_REGISTRY}/rappiflow:0.4.3

COPY . .


# You can customize this Dockerfile below this line. Keep in mind that environment variables for localhost can be
# defined in the .env file in the root dir of the project, and any Alpine or Python packages can be installed via the
# packages.txt and requirements.txt files, respectively.