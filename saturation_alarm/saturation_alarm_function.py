#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# !jupyter nbconvert --to script saturation_alarm.ipynb


# In[2]:


import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)


# In[36]:

def process(country, canal):


    if country=='CO':
        country = 'CO'
        flag = '\U0001F1E8\U0001F1F4'
        BD = 1971
        time_zones = 'Bogota'


    elif country=='BR':
        country = 'BR'
        flag = '\U0001F1E7\U0001F1F7'
        BD = 1969
        time_zones = 'Sao_Paulo'


    elif country=='MX':
        country = 'MX'
        flag = '\U0001F1F2\U0001F1FD'
        BD = 1972
        time_zones = 'Mexico_City'

    elif country=='AR':
        country = 'AR'
        time_zones = 'Buenos_Aires'
        flag = '\U0001F1E6\U0001F1F7'
        BD = 1968


    elif country=='CL':
        country = 'CL'
        flag = '\U0001F1E8\U0001F1F1'
        BD = 1970
        time_zones = 'Santiago'


    elif country=='PE':
        country = 'PE'
        flag = '\U0001F1F5\U0001F1EA'
        BD = 1973
        time_zones = 'Lima'


    elif country=='UY':
        country = 'UY'
        flag = '\U0001F1FA\U0001F1FE'
        BD = 1974
        time_zones = 'Montevideo'


    elif country=='EC':
        country = 'EC'
        flag = '\U0001F1EA\U0001F1E8'
        BD = 2556
        time_zones = 'Guayaquil'


    elif country=='CR':
        country = 'CR'
        flag = '\U0001F1E8\U0001F1F7'
        BD = 2558
        time_zones = 'Costa_Rica'


    # In[39]:

    print('init_', country)

    df = run_query_r(BD, """
    --no_cache
    set timezone= 'America/{timezone}';

    select
    physical_store_id,
    id as slot_id,
    datetime_utc_iso::timestamp as slot_starts_at,
    items_capacity,
    reserved_items_capacity,
    (reserved_items_capacity - items_capacity) as skus_encima_capacidad
    from slots
    where
    datetime_utc_iso::date=now()::date
    and extract('hour' from datetime_utc_iso) = extract('hour' from now()::timestamp) +2
    and deleted_at is null
    and type = 'SHOPPER'
    """.format(timezone=time_zones))

    print(df.shape)
    print('end_', country)
    df.head()


    # In[ ]:


    df1= df.loc[df['skus_encima_capacidad']>= 10]

    print(df1.shape)
    df1.head()


    # In[47]:


    canal_slack = canal

    if len(df1)>0:
        
        for index, row in df1.iterrows():
            
            main_text = '''
                \U000026A0 *Slots Saturation Management  - {flag}*:
                <!subteam^S01D2QTSE8H>
                Physical store id: {physical_store_id}
                Slot id: {slot_id}            
                Items capacity: {items_capacity}
                Reserved items capacity: {reserved_items_capacity}
                Skus over capacity: {skus_encima_capacidad}
                Slot starts at: {slot_starts_at}
                '''.format(
                    flag=flag,
                    physical_store_id =row['physical_store_id'],
                    slot_id =row['slot_id'],
                    items_capacity = row['items_capacity'],
                    reserved_items_capacity = row['reserved_items_capacity'],
                    skus_encima_capacidad = row['skus_encima_capacidad'],
                    slot_starts_at = datetime.strptime(row['slot_starts_at'], '%Y-%m-%dT%H:%M:%S').strftime("%m/%d/%Y %H:%M:%S")
                    )
                
            print(main_text)
            bot_slack(main_text, canal_slack)

    
    else:
        
        main_text = '''Pais: {flag}
        \n \U00002705 No hay tiendas con saturación de slots.'''.format(flag=flag)    

        print(main_text)
        bot_slack(main_text, canal_slack) 
