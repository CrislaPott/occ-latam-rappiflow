import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta

def products():

    query_so = ''' 
SELECT DISTINCT O.COUNTRY,
                'CANCELAR CPGS + REST' AS ACTION,
                O.APPLICATION_USER_ID AS USER_ID,
                O.ORDER_ID,
                O.DATE_ORDER_CREATED,
                O.ORDER_STATE,
                O.STORE_ID,
                O.STORE_NAME,
                O.BRAND_GROUP AS BRAND_NAME,
                O.STORE_TYPE,
                O.VERTICAL,
                M.DELIVERY_METHOD
FROM GLOBAL_FINANCES.GLOBAL_ORDERS O
INNER JOIN GLOBAL_FINANCES.GLOBAL_ORDER_DETAILS D
ON D.COUNTRY = O.COUNTRY AND D.ORDER_ID = O.ORDER_ID
LEFT JOIN
          (
                 SELECT 'CO' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CO_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'BR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   BR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'MX' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   MX_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'AR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   AR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CL' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CL_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'PE' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   PE_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'EC' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   EC_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'UY' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   UY_CORE_ORDERS_PUBLIC.DELIVERY_ORDER) M
ON        M.COUNTRY = O.COUNTRY
AND       M.ORDER_ID = O.ORDER_ID
WHERE     O.ORDER_STATE IN ('in_store',
                          'in_progress',
                          'created',
                          'on_the_route')
AND       O.DATE_ORDER_CREATED < CURRENT_DATE -2
AND       O.DATE_ORDER_CREATED >= CURRENT_DATE -30
AND       O.VERTICAL IN ('RESTAURANTES', 'CPGS')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('ishop_accesorios',
                             'mac_center_watch')
AND NOT(O.STORE_NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))   
AND D.STORE_TYPE_STORE NOT ILIKE '%turbo%'
AND M.DELIVERY_METHOD NOT IN ('national_delivery',
                                    'marketplace',
                                    'pickup')

UNION ALL

SELECT DISTINCT O.COUNTRY,
                'TRAMITAR CPGS + REST' AS ACTION,
                O.APPLICATION_USER_ID AS USER_ID,
                O.ORDER_ID,
                O.DATE_ORDER_CREATED,
                O.ORDER_STATE,
                O.STORE_ID,
                O.STORE_NAME,
                O.BRAND_GROUP AS BRAND_NAME,
                O.STORE_TYPE,
                O.VERTICAL,
                M.DELIVERY_METHOD
FROM GLOBAL_FINANCES.GLOBAL_ORDERS O
INNER JOIN GLOBAL_FINANCES.GLOBAL_ORDER_DETAILS D
ON D.COUNTRY = O.COUNTRY AND D.ORDER_ID = O.ORDER_ID
LEFT JOIN
          (
                 SELECT 'CO' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CO_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'BR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   BR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'MX' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   MX_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'AR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   AR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CL' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CL_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'PE' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   PE_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'EC' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   EC_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'UY' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   UY_CORE_ORDERS_PUBLIC.DELIVERY_ORDER) M
ON        M.COUNTRY = O.COUNTRY
AND       M.ORDER_ID = O.ORDER_ID
WHERE     O.ORDER_STATE IN ('arrive')
AND       O.DATE_ORDER_CREATED < CURRENT_DATE -2
AND       O.DATE_ORDER_CREATED >= CURRENT_DATE -30
AND       O.VERTICAL IN ('RESTAURANTES', 'CPGS')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('ishop_accesorios',
                             'mac_center_watch')
AND NOT(O.STORE_NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))   
AND D.STORE_TYPE_STORE NOT ILIKE '%turbo%'
AND M.DELIVERY_METHOD NOT IN ('national_delivery',
                                    'marketplace',
                                    'pickup')

UNION ALL

SELECT DISTINCT O.COUNTRY,
                'CANCELAR ECOMM' AS ACTION,
                O.APPLICATION_USER_ID AS USER_ID,
                O.ORDER_ID,
                O.DATE_ORDER_CREATED,
                O.ORDER_STATE,
                O.STORE_ID,
                O.STORE_NAME,
                O.BRAND_GROUP AS BRAND_NAME,
                O.STORE_TYPE,
                O.VERTICAL,
                M.DELIVERY_METHOD
FROM GLOBAL_FINANCES.GLOBAL_ORDERS O
INNER JOIN GLOBAL_FINANCES.GLOBAL_ORDER_DETAILS D
ON D.COUNTRY = O.COUNTRY AND D.ORDER_ID = O.ORDER_ID
LEFT JOIN
          (
                 SELECT 'CO' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CO_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'BR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   BR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'MX' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   MX_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'AR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   AR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CL' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CL_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'PE' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   PE_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'EC' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   EC_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'UY' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   UY_CORE_ORDERS_PUBLIC.DELIVERY_ORDER) M
ON        M.COUNTRY = O.COUNTRY
AND       M.ORDER_ID = O.ORDER_ID
WHERE     O.ORDER_STATE IN ('in_store',
                          'in_progress',
                          'created',
                          'on_the_route')
AND       O.DATE_ORDER_CREATED < CURRENT_DATE -5
AND       O.DATE_ORDER_CREATED >= CURRENT_DATE -30
AND       O.VERTICAL IN ('E-COMMERCE')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('ishop_accesorios',
                             'mac_center_watch')
AND NOT(O.STORE_NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))   
AND D.STORE_TYPE_STORE NOT ILIKE '%turbo%'
AND M.DELIVERY_METHOD NOT IN ('national_delivery',
                                    'marketplace',
                                    'pickup')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('telmov',
                             'elmercadazo',
                             'bio_mattres',
                             'tekshop_enc',
                             'anker',
                             'startgames',
                             'contoysatecnologia_nc',
                             'supermex',
                             'kocare_moda',
                             'rca_tecnologia',
                             'secrecy',
                             'mandalibre',
                             'lush_belleza',
                             'urban_curls_enc',
                             'julio_cepeda',
                             'mi_alegria',
                             'baby_bebe',
                             'newera',
                             'sosoland',
                             'xamania_enc',
                             'namaro',
                             'cv_directo',
                             'peluchesaurora',
                             'onix_baby',
                             'tic_tac_toy_enc',
                             'limited_edition',
                             'vitazana_enc',
                             'storyland',
                             'verano_inflable_nc',
                             'juguetega',
                             'calzado_tropicana_nc',
                             'apple_cosmetics_nc',
                             'calzado_rilo_nc',
                             'monchi_accesorios_enc',
                             'ishop_accesorios',
                             'mac_center_watch',
                             'ishop_accesorios',
                             'mac_center_watch')
AND       O.STORE_ID NOT IN ('1923266352',
                           '1923258698',
                           '1923294077',
                           '1923481234',
                           '1923459460',
                           '1923246086',
                           '1923301066',
                           '1923460953',
                           '1923284914',
                           '1923472530',
                           '1923227254',
                           '1923262186',
                           '1923261424',
                           '1923465308',
                           '1923228875',
                           '1923289904',
                           '1923256859',
                           '1923244612',
                           '1923248063',
                           '1923468140',
                           '1923224210',
                           '1923294141',
                           '1923257403',
                           '1923256425',
                           '1923464353',
                           '1923240491',
                           '1923474074',
                           '1923247295',
                           '1923326800',
                           '1923248557',
                           '1923326871',
                           '1923460063',
                           '1923302486',
                           '1923468144')                             
                                    
UNION ALL

SELECT DISTINCT O.COUNTRY,
                'TRAMITAR ECOMM' AS ACTION,
                O.APPLICATION_USER_ID AS USER_ID,
                O.ORDER_ID,
                O.DATE_ORDER_CREATED,
                O.ORDER_STATE,
                O.STORE_ID,
                O.STORE_NAME,
                O.BRAND_GROUP AS BRAND_NAME,
                O.STORE_TYPE,
                O.VERTICAL,
                M.DELIVERY_METHOD
FROM GLOBAL_FINANCES.GLOBAL_ORDERS O
INNER JOIN GLOBAL_FINANCES.GLOBAL_ORDER_DETAILS D
ON D.COUNTRY = O.COUNTRY AND D.ORDER_ID = O.ORDER_ID
LEFT JOIN
          (
                 SELECT 'CO' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CO_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'BR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   BR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'MX' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   MX_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'AR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   AR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CL' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CL_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'PE' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   PE_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'CR' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   CR_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'EC' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   EC_CORE_ORDERS_PUBLIC.DELIVERY_ORDER
                 UNION ALL
                 SELECT 'UY' AS COUNTRY,
                        ORDER_ID,
                        DELIVERY_METHOD
                 FROM   UY_CORE_ORDERS_PUBLIC.DELIVERY_ORDER) M
ON        M.COUNTRY = O.COUNTRY
AND       M.ORDER_ID = O.ORDER_ID
WHERE     O.ORDER_STATE IN ('arrive')
AND       O.DATE_ORDER_CREATED < CURRENT_DATE -5
AND       O.DATE_ORDER_CREATED >= CURRENT_DATE -30
AND       O.VERTICAL IN ('E-COMMERCE')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('ishop_accesorios',
                             'mac_center_watch')
AND NOT(O.STORE_NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))   
AND D.STORE_TYPE_STORE NOT ILIKE '%turbo%'
AND M.DELIVERY_METHOD NOT IN ('national_delivery',
                                    'marketplace',
                                    'pickup')
AND       O.STORE_TYPE NOT ILIKE '%siniva%'
AND       O.STORE_TYPE NOT IN ('telmov',
                             'elmercadazo',
                             'bio_mattres',
                             'tekshop_enc',
                             'anker',
                             'startgames',
                             'contoysatecnologia_nc',
                             'supermex',
                             'kocare_moda',
                             'rca_tecnologia',
                             'secrecy',
                             'mandalibre',
                             'lush_belleza',
                             'urban_curls_enc',
                             'julio_cepeda',
                             'mi_alegria',
                             'baby_bebe',
                             'newera',
                             'sosoland',
                             'xamania_enc',
                             'namaro',
                             'cv_directo',
                             'peluchesaurora',
                             'onix_baby',
                             'tic_tac_toy_enc',
                             'limited_edition',
                             'vitazana_enc',
                             'storyland',
                             'verano_inflable_nc',
                             'juguetega',
                             'calzado_tropicana_nc',
                             'apple_cosmetics_nc',
                             'calzado_rilo_nc',
                             'monchi_accesorios_enc',
                             'ishop_accesorios',
                             'mac_center_watch',
                             'ishop_accesorios',
                             'mac_center_watch')
AND       O.STORE_ID NOT IN ('1923266352',
                           '1923258698',
                           '1923294077',
                           '1923481234',
                           '1923459460',
                           '1923246086',
                           '1923301066',
                           '1923460953',
                           '1923284914',
                           '1923472530',
                           '1923227254',
                           '1923262186',
                           '1923261424',
                           '1923465308',
                           '1923228875',
                           '1923289904',
                           '1923256859',
                           '1923244612',
                           '1923248063',
                           '1923468140',
                           '1923224210',
                           '1923294141',
                           '1923257403',
                           '1923256425',
                           '1923464353',
                           '1923240491',
                           '1923474074',
                           '1923247295',
                           '1923326800',
                           '1923248557',
                           '1923326871',
                           '1923460063',
                           '1923302486',
                           '1923468144')                             
                                    
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            to_upload = df
            to_upload = to_upload[
                ["country", "action", "user_id", "order_id", "date_order_created", "order_state", "store_id", "store_name", "brand_name", "store_type", "vertical", "delivery_method"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_limbo_ordenes")

            text = '''
            *NONLIVE - Alarma Ordenes Limbo*
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/ordenes_limbo.csv'.format(home)
            df_final = df[["country", "action", "user_id", "order_id", "date_order_created", "order_state", "store_id", "store_name", "brand_name", "store_type", "vertical", "delivery_method"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            text = '''
            *NONLIVE - Alarma Ordenes Limbo*
            Sin nuevas ordenes para actuar :verified:
            '''
            slack.bot_slack(text,'C02MJQ1Q5H6')
    else:
        print('products null')



df = products()

run_alarm(df)