import pandas as pd
import os
from lib import slack,redash
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz
from functions import timezones

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

  query_so = '''
 with elastic as (

select distinct e.country_code,
                e.product_id,
                e.store_id,
                e.price
from cpgs_datascience.cpgs_elastic_content e
left join ops_occ.store_infos_latam s on s.store_id = e.store_id and upper(s.country) = e.country_code
where (e.price is null or e.price = 0)
and is_available = 'TRUE'
and is_discontinued = 'FALSE'
and in_stock = 'TRUE'
AND NOT(E.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
AND NOT(S.STORETYPE_ ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%')))
 
SELECT DISTINCT 'BR' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            BR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      BR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'BR' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       BR_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'BR' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.BR_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  UNION ALL
  
SELECT DISTINCT 'CO' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            CO_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      CO_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'CO' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       CO_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'CO' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.CO_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  UNION ALL
  
  SELECT DISTINCT 'MX' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            MX_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      MX_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'MX' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       MX_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'MX' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.MX_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  UNION ALL
  
  SELECT DISTINCT 'CL' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            CL_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      CL_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'CL' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       CL_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'CL' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.CL_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  union all
  
  SELECT DISTINCT 'PE' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            PE_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      PE_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'PE' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       PE_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'PE' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.PE_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  union all
  
  SELECT DISTINCT 'AR' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            AR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      AR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'AR' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       AR_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'AR' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.AR_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  UNION ALL
  
  SELECT DISTINCT 'CR' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            CR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      CR_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'CR' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       CR_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'CR' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.CR_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  union all
  
  SELECT DISTINCT 'EC' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            EC_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      EC_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  LEFT JOIN       EC_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'EC' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.EC_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL
  
  union all
  
  SELECT DISTINCT 'UY' AS COUNTRY,
                  P.ID AS PRODUCT_ID,
                  P.NAME,
                  PS.STORE_ID,
                  V.VERTICAL_GROUP AS VERTICAL,
                  PS.PRICE,
                  PS.PERCENTAGE_PRICE_VARIATION AS MARKDOWN,
                  P.RETAIL_ID,
                  'AVAILABLE' AS STATUS,
                  E.PRICE AS ELASTIC_PRICE,
                  CASE WHEN MARKDOWN > 99 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND E.PRICE >= 0 THEN 'CATALOG'
                  WHEN (PS.PRICE IS NULL OR PS.PRICE = 0) AND (E.PRICE = 0 OR E.PRICE IS NULL) THEN 'CATALOG AND ELASTIC'                  
                  WHEN E.PRICE = 0 AND PS.PRICE >= 0 THEN 'ELASTIC'
                  ELSE NULL END AS SOURCE,
                  PH.PHYSICAL_STORE_ID
  FROM            UY_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCTS_VW P
  INNER JOIN      UY_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW PS
  ON              PS.PRODUCT_ID = P.ID
  INNER JOIN      CPGS_DATASCIENCE.LOOKUP_STORE LS
  ON              LS.STORE_ID = PS.STORE_ID AND LS.COUNTRY_CODE = 'UY' AND LS.IN_CONTENT = 'FALSE'
  LEFT JOIN       UY_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON              S.STORE_ID = PS.STORE_ID
  LEFT JOIN       ELASTIC E
  ON              E.COUNTRY_CODE = 'UY' AND P.ID = E.PRODUCT_ID AND PS.STORE_ID = E.STORE_ID
  LEFT JOIN       VERTICALS_LATAM.UY_VERTICALS_V2 V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
  ON              PH.STORE_ID = PS.STORE_ID  
  WHERE           P.DELETED_AT IS NULL
  AND             S.DELETED_AT IS NULL
  AND             COALESCE(P._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(PS._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
  AND             COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
  AND             PS.IS_AVAILABLE = TRUE
  AND             NOT(P.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
  AND             PS.IS_DISCONTINUED = FALSE
  AND             NOT(
                                  S.TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
  AND             V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
  AND             SOURCE IS NOT NULL                            
'''
  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def run_alarm(df):
    if not df.empty:
        df['checked_at'] = current_time
        to_upload = df
        to_upload = to_upload[
          ["country", "checked_at", "product_id", "name", "store_id", "vertical", "price", "markdown", "retail_id", "status", "elastic_price", "source"]]
        print("uploading df")
        snow.upload_df_occ(to_upload, "nl_precio_y_markdown_cms")
        text = '''
        *NONLIVE - PRODUCTOS CON PRECIO = 0 OR MARKDOWN >= 99% (CMS)*
        '''
        print(text)
        home = expanduser("~")
        results_file = '{}/details_nl_precio_y_markdown_cms.csv'.format(home)
        df_final = df[["country", "product_id", "name", "store_id", "vertical", "price", "markdown", "retail_id", "status", "elastic_price", "source"]]
        df_final.to_csv(results_file, index=False)
        slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")
    else:
        print('products null')
        text = '''
        *NONLIVE - PRODUCTOS CON PRECIO = 0 OR MARKDOWN >= 99% (CMS)*
        Sin nuevos productos para actuar :verified:
        '''
        slack.bot_slack(text,'C02MJQ1Q5H6')


df = products()

run_alarm(df)
