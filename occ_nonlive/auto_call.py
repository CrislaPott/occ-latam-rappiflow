import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = """
with BR as (
    with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'BR'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)
            ,
         PHYSICAL_STORES as (
             select distinct ps.id,
                             ps.name,
                             sps.picked_by,
                             pt.store_bucket,
                             case
                                 when coalesce(
                                              round(
                                                      avg(ideal_time)
                                                  ),
                                              3
                                          ) >= 8 then 8
                                 when coalesce(
                                              round(
                                                      avg(ideal_time)
                                                  ),
                                              3
                                          ) <= 2 then 3
                                 else coalesce(
                                         round(
                                                 avg(ideal_time)
                                             ),
                                         3
                                     ) end as ideal_config,
                             listagg(
                                     distinct v.vertical_sub_group, ','
                                 )         as vertical
             from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
                      left join sps on sps.physical_store_id = ps.id::int
                      left join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
                      LEFT JOIN BR_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
                      left join verticals_latam.BR_verticals_v2 v on v.store_type::text = st.type::text
                      left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                                on pt.key_physical_store_id::text = concat('BR', '_', ps.id)::text
                      left join ot on ot.store_id = st.store_id
             where pt.store_bucket in (
                                       'Only picker', 'Shopper + RT', 'Picker + RT',
                                       'Only shopper'
                 )
               and s.IS_ENABLED = 'TRUE'
               and coalesce(st._fivetran_deleted, false) = false
               and v.vertical_sub_group in (
                                            'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                            'PHARMACY'
                 )
               and v.vertical_group = 'CPGS'
               AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'BR')

             group by 1, 2, 3, 4)
            ,
         AUTOCALL_EVENT AS (
             with inicial as (
                 select config_value    as physical_store_id,
                        row_number() over (
                            partition by config_value,
                                rules_id
                            order by
                                created_at desc
                            )           as rank,
                        trim(
                                "VALUES" : minutes_after_assign_to_shopper,
                                '"'
                            ):: varchar as min_before_autocall
                 from BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
                 where rules_id in ('85', '6')
             )
             select *
             from inicial
             where rank = 1
         ),
         REQUEST_RT_EVENT AS (
             with inicial as (
                 select physical_store_id,
                        request_rt,
                        report_bill,
                        row_number() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            ) as rank
                 from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
             )
             select *
             from inicial
             where rank = 1
         ),
         EVENT_FOR_REQUESTING_RT_EVENT AS (
             with configs_tags as (
                 select groups.name       GROUPS_NAME,
                        rules.name        RULES_NAME,
                        "VALUES"."VALUES" values_json,
                        "VALUES".config_type,
                        "VALUES".config_value,
                        "VALUES".id AS    VALUES_ID,
                        rules.id    AS    RULES_ID,
                        "VALUES".created_at,
                        rank() over (
                            partition by rules.id,
                                "VALUES".config_value
                            order by
                                "VALUES".created_at desc
                            )             rank
                 from BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
                          left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules
                                    on rules.id = groups_rules.rules_id
                          left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups
                                    on groups_rules.groups_id = groups.id
                          left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES"
                                    on "VALUES".rules_id = rules.id
                 where groups.name in (
                                       'release-to-rt-by-tag', 'release-to-rt-by-fallback',
                                       'request-rt-by-place-at'
                     )
                   and rules.name in (
                                      'has-tag', 'picked-by', 'shopper-online',
                                      'has-not-event', 'minutes-before-delivery',
                                      'has-events'
                     )
             ),
                  default_configs_tags as (
                      select groups.name           GROUPS_NAME_D,
                             rules.name            RULES_NAME_D,
                             "VALUES"."VALUES"     values_json_d,
                             "VALUES".CONFIG_TYPE  CONFIG_TYPE_D,
                             "VALUES".config_value config_value_d,
                             "VALUES".id AS        VALUES_ID_D,
                             rules.id    AS        RULES_ID_D,
                             rank() over (
                                 partition by rules.id,
                                     "VALUES".config_value
                                 order by
                                     "VALUES".created_at desc
                                 )                 rank
                      from BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
                               left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules
                                         on rules.id = groups_rules.rules_id
                               left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups
                                         on groups_rules.groups_id = groups.id
                               left join BR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES"
                                         on "VALUES".rules_id = rules.id
                      where groups.name in (
                                            'release-to-rt-by-tag', 'release-to-rt-by-fallback',
                                            'request-rt-by-place-at'
                          )
                        and rules.name in (
                                           'has-tag', 'picked-by', 'shopper-online',
                                           'has-not-event', 'minutes-before-delivery',
                                           'has-events'
                          )
                        and config_value_d = 'default'
                  ),
                  configss_tag as (
                      select *
                      from configs_tags
                      where rank = 1
                  ),
                  final as (
                      select config_value,
                             case
                                 when GROUPS_NAME = 'request-rt-by-place-at'
                                     and CONFIG_TYPE = 'by_physical_store_id'
                                     and RULES_NAME = 'has-events' then values_json : events
                                 when GROUPS_NAME = 'request-rt-by-place-at'
                                     and CONFIG_TYPE = 'by_store_type_store'
                                     and RULES_NAME = 'has-events' then values_json : events
                                 when (
                                             GROUPS_NAME <> 'request-rt-by-place-at'
                                         and CONFIG_TYPE = 'by_physical_store_id'
                                     ) then null
                                 when (
                                             GROUPS_NAME <> 'request-rt-by-place-at'
                                         and CONFIG_TYPE = 'by_store_type_store'
                                     ) then null
                                 else (
                                     select values_json_d : minutes_before_delivery
                                     from default_configs_tags
                                     where GROUPS_NAME_D = 'request-rt-by-place-at'
                                       and RULES_NAME_D = 'has-events'
                                       and rank = 1
                                 ) end as event_for_requesting_rt
                      from configss_tag
                  )
             select config_value as              physical_store_id,
                    max(event_for_requesting_rt) event_for_requesting_rt
             from final
             group by 1
         ),
         FINAL AS (
             SELECT ps.id                                                                           as physical_store_id,
                    ps.name                                                                         as store_name,
                    ps.store_bucket,
                    vertical,
                    rre.report_bill,
                    rre.request_rt,
                    ae.min_before_autocall,
                    efrt.event_for_requesting_rt,
                    case
                        when efrt.event_for_requesting_rt is not null then null
                        when (
                                         ae.min_before_autocall > 8
                                     or ae.min_before_autocall = 0
                                     or ae.min_before_autocall is null
                                 )
                            and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar
                        when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
                    case
                        when (
                                     rre.request_rt is null
                                     or rre.request_rt = 'FALSE'
                                 )
                            or (
                                         vertical ilike '%SUPER%'
                                     and efrt.event_for_requesting_rt is null
                                 )
                            or (
                                         vertical not ilike '%SUPER%'
                                     and (
                                                     ae.min_before_autocall <= 0
                                                 or ae.min_before_autocall > 8
                                             )
                                 ) then 'true'
                        else 'false' end                                                            as alert
             FROM PHYSICAL_STORES PS
                      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
                      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
                      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT
                                ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
             WHERE ALERT = 'true'
         )
    SELECT 'BR' as country,
           physical_store_id,
           store_name,
           vertical,
           min_before_autocall,
           suggested_config
    from final
    where suggested_config is not null
      and suggested_config not in (
        '[ "start_shopper_billing_report" ]'
        )
      and (
                min_before_autocall > 0
            or min_before_autocall is null
        )
),

co as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from co_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'CO'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from co_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join co_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN co_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.co_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('CO', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
     AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CO')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        co_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYPE_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join co_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_type_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_type_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'CO' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)
,
mx as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'MX'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN mx_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.mx_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('MX', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'MX')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYPE_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join mx_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_type_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_type_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'MX' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)
, ar as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'AR'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN AR_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.AR_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('AR', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'AR')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYPE_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join AR_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_type_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_type_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'AR' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)

,
     cl as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'CL'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN CL_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.CL_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('CL', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CL')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYPE_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join CL_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_type_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_type_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'CL' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)
,
     pe as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from pe_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'PE'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from pe_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join pe_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN pe_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.pe_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('PE', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'PE')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        pe_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYPE_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join pe_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_type_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_type_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'PE' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
),
     ec as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from ec_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'EC'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from ec_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join ec_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN ec_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.ec_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('EC', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'EC')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        ec_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYec_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join ec_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_tyec_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_tyec_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'EC' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
),
     cr as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from cr_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'CR'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from cr_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join cr_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN cr_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.cr_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('CR', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CR')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        cr_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYcr_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join cr_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_tycr_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_tycr_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'CR' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)

, uy as (

with sps as (
        select *
        from (
                 select physical_store_id::int as physical_store_id,
                        picked_by,
                        rank() over (
                            partition by physical_store_id
                            order by
                                _fivetran_synced desc
                            )                  as rank
                 from uy_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
                 where coalesce(_fivetran_deleted, false) = false
             )
        where rank = 1
    ),
         ot as (
             select store_id::int                               as store_id,
                    round(
                            avg(time_shopper_wait_rt) / 60
                        )                                       as shopper_waiting_time,
                    round(
                            avg(time_with_shopper) / 60
                        )                                       as shopper_picking_time,
                    shopper_picking_time - shopper_waiting_time as ideal_time
             from OPS_GLOBAL.ORDER_TIMES
             where country = 'UY'
               and date_trunc('month', created_at):: date = date_trunc('month', current_date):: date
             group by 1)

, PHYSICAL_STORES as (
    select distinct ps.id,
                    ps.name,
                    sps.picked_by,
                    pt.store_bucket,
                    case
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) >= 8 then 8
                        when coalesce(
                                     round(
                                             avg(ideal_time)
                                         ),
                                     3
                                 ) <= 2 then 3
                        else coalesce(
                                round(
                                        avg(ideal_time)
                                    ),
                                3
                            ) end as ideal_config,
                    listagg(
                            distinct v.vertical_sub_group, ','
                        )         as vertical
    from uy_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps
             left join sps on sps.physical_store_id = ps.id::int
             left join uy_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores st on st.physical_store_id::int = ps.id::int
             LEFT JOIN uy_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID::int = ST.STORE_ID::int
             left join verticals_latam.uy_verticals_v2 v on v.store_type::text = st.type::text
             left join CPGS_OPS_SHOPPER_GLOBAL.TMP_MKAGUE_JOB20210806_1746_91 pt
                       on pt.key_physical_store_id::text = concat('UY', '_', ps.id)::text
             left join ot on ot.store_id = st.store_id
    where pt.store_bucket in (
                              'Only picker', 'Shopper + RT', 'Picker + RT',
                              'Only shopper'
        )
      and s.IS_ENABLED = 'TRUE'
      and coalesce(st._fivetran_deleted, false) = false
      and v.vertical_sub_group in (
                                   'SUPER', 'HIPER', 'EXPRESS', 'LIQUOR',
                                   'PHARMACY'
        )
      and v.vertical_group = 'CPGS'
      AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'UY')

    group by 1, 2, 3, 4)

,
  AUTOCALL_EVENT AS (
    with inicial as (
      select
        config_value as physical_store_id,
        row_number () over (
          partition by config_value,
          rules_id
          order by
            created_at desc
        ) as rank,
        trim(
          "VALUES" : minutes_after_assign_to_shopper,
          '"'
        ):: varchar as min_before_autocall
      from
        uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" V
      where
        rules_id in ('85', '6')
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  REQUEST_RT_EVENT AS (
    with inicial as (
      select
        physical_store_id,
        request_rt,
        report_bill,
        row_number () over (
          partition by physical_store_id
          order by
            _fivetran_synced desc
        ) as rank
      from
        uy_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores
    )
    select
      *
    from
      inicial
    where
      rank = 1
  ),
  EVENT_FOR_REQUESTING_RT_EVENT AS (
    with configs_tags as (
      select
        groups.name GROUPS_NAME,
        rules.name RULES_NAME,
        "VALUES"."VALUES" values_json,
        "VALUES".config_type,
        "VALUES".config_value,
        "VALUES".id AS VALUES_ID,
        rules.id AS RULES_ID,
        "VALUES".created_at,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
    ),
    default_configs_tags as (
      select
        groups.name GROUPS_NAME_D,
        rules.name RULES_NAME_D,
        "VALUES"."VALUES" values_json_d,
        "VALUES".CONFIG_TYPE CONFIG_TYuy_D,
        "VALUES".config_value config_value_d,
        "VALUES".id AS VALUES_ID_D,
        rules.id AS RULES_ID_D,
        rank() over (
          partition by rules.id,
          "VALUES".config_value
          order by
            "VALUES".created_at desc
        ) rank
      from
        uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.rules rules
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups_rules groups_rules on rules.id = groups_rules.rules_id
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC.groups groups on groups_rules.groups_id = groups.id
        left join uy_PG_MS_CPGOPS_DISPATCH_MS_PUBLIC."VALUES" as "VALUES" on "VALUES".rules_id = rules.id
      where
        groups.name in (
          'release-to-rt-by-tag', 'release-to-rt-by-fallback',
          'request-rt-by-place-at'
        )
        and rules.name in (
          'has-tag', 'picked-by', 'shopper-online',
          'has-not-event', 'minutes-before-delivery',
          'has-events'
        )
        and config_value_d = 'default'
    ),
    configss_tag as (
      select
        *
      from
        configs_tags
      where
        rank = 1
    ),
    final as (
      select
        config_value,
        case when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_physical_store_id'
        and RULES_NAME = 'has-events' then values_json : events when GROUPS_NAME = 'request-rt-by-place-at'
        and CONFIG_TYPE = 'by_store_tyuy_store'
        and RULES_NAME = 'has-events' then values_json : events when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_physical_store_id'
        ) then null when (
          GROUPS_NAME <> 'request-rt-by-place-at'
          and CONFIG_TYPE = 'by_store_tyuy_store'
        ) then null else (
          select
            values_json_d : minutes_before_delivery
          from
            default_configs_tags
          where
            GROUPS_NAME_D = 'request-rt-by-place-at'
            and RULES_NAME_D = 'has-events'
            and rank = 1
        ) end as event_for_requesting_rt
      from
        configss_tag
    )
    select
      config_value as physical_store_id,
      max(event_for_requesting_rt) event_for_requesting_rt
    from
      final
    group by
      1
  ),
  FINAL AS (
    SELECT
      ps.id as physical_store_id,
      ps.name as store_name,
      ps.store_bucket,
      vertical,
      rre.report_bill,
      rre.request_rt,
      ae.min_before_autocall,
      efrt.event_for_requesting_rt,
      case when efrt.event_for_requesting_rt is not null then null when (
        ae.min_before_autocall > 8
        or ae.min_before_autocall = 0
        or ae.min_before_autocall is null
      )
      and vertical not ilike '%SUPER%' then ps.ideal_config :: varchar when vertical ilike '%SUPER%' then '[ "start_shopper_billing_report" ]' end as SUGGESTED_CONFIG,
      case when (
        rre.request_rt is null
        or rre.request_rt = 'FALSE'
      )
      or (
        vertical ilike '%SUPER%'
        and efrt.event_for_requesting_rt is null
      )
      or (
        vertical not ilike '%SUPER%'
        and (
          ae.min_before_autocall <= 0
          or ae.min_before_autocall > 8
        )
      ) then 'true' else 'false' end as alert
    FROM
      PHYSICAL_STORES PS
      LEFT JOIN AUTOCALL_EVENT AE ON AE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN REQUEST_RT_EVENT RRE ON RRE.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
      LEFT JOIN EVENT_FOR_REQUESTING_RT_EVENT EFRT ON EFRT.PHYSICAL_STORE_ID :: VARCHAR = PS.ID :: VARCHAR
    WHERE
      ALERT = 'true'
  )
  SELECT
    'UY' as country,
    physical_store_id,
    store_name,
    vertical,
    min_before_autocall,
    suggested_config
  from
    final
  where
    suggested_config is not null
    and suggested_config not in (
      '[ "start_shopper_billing_report" ]'
    )
    and (
      min_before_autocall > 0
      or min_before_autocall is null
    )
)

SELECT
  *
FROM
  BR
UNION ALL
SELECT
  *
FROM
  CO
UNION ALL
SELECT
  *
FROM
  MX
UNION ALL
SELECT
  *
FROM
  AR
UNION ALL
SELECT
  *
FROM
  CL
UNION ALL
SELECT
  *
FROM
  PE
UNION ALL
SELECT
  *
FROM
  EC
UNION ALL
SELECT
  *
FROM
  CR
UNION ALL
SELECT
  *
FROM
  UY
"""

    print('rodando snowflake')
    df = snow.run_query(query_so)
    print(df)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "vertical", "physical_store_id", "store_name","min_before_autocall", "suggested_config"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_auto_call")

            text = '''
            *NONLIVE - CAMBIO DEL TIEMPO DE AUTOCALL*

            :warning: Este cambio se realiza sobre la base del tiempo medio entre el horario que el Shopper termina el picking time y comienza a llamar a un RT para la orden. Con este cambio, las órdenesse asignarán automáticamente a los rts de manera más rápida :warning:

            :retraso_asignación_rt: La regla para la alteración del autocall fue:
            
            La media del tiempo de la orden con Shopper - (menos) - media del tiempo del Shopper esperando RT

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_nl_auto_call.csv'.format(home)
            df_final = df[["country", "vertical", "physical_store_id","store_name", "min_before_autocall", "suggested_config"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
            
    else:
        print('products null')
        slack.bot_slack('No hay tiendas que necesitan ajuste de auto call en este momento','C02MJQ1Q5H6')



df = products()

run_alarm(df)