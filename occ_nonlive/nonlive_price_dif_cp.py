import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz
from functions import timezones

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def excluding_products():
    query = '''select country as country_, product_id as productid, store_id as storeid from ops_occ.nl_price_dif_cp
where checked_at::date >= dateadd(hours ,-36,convert_timezone('America/Buenos_Aires',current_timestamp()))::date
'''
    df = snow.run_query(query)
    return df

def products():

    query_so = ''' 
with base as ( select distinct country_code as country ,store_type , store_id,store_name, SUB_VERTICAL as VERTICAL_SUB_GROUP,
              in_content, suspended ,coalesce (brand_group_name, store_brand_name) as brand
              from  CPGS_DATASCIENCE.LOOKUP_STORE
              where
             vertical in ('CPGS') and
             store_name not ilike('%prueba%')and store_name not ilike('%test%')
             and deleted_at is null
             and (suspended is null or suspended in ('false'))
             and in_content = ('TRUE')
             )
    ,product_store as (

select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.product ps
left JOIN br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.store s on s.id = ps.store_id
left JOIN br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_store vs on vs.store_id = s.id
left join br_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join br_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'BR' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

          union all

 select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from co_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.product ps
left JOIN co_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.store s on s.id = ps.store_id
left JOIN co_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_store vs on vs.store_id = s.id
left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'CO' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

        union all

(
with base as (
select distinct
country_code as country
,store_type
,store_id
,store_name
,SUB_VERTICAL as VERTICAL_SUB_GROUP
,in_content
,suspended
from CPGS_DATASCIENCE.LOOKUP_STORE
where vertical in ('CPGS')
and store_name not ilike('%prueba%')and store_name not ilike('%test%')
and deleted_at is null
and (suspended is null or suspended in ('false'))
and in_content = ('TRUE')
and SUB_VERTICAL in ('SUPER','LIQUOR','EXPRESS','PHARMACY')
and country_code in ('MX')
),

ps as (

select
base.country
,ps.id as store_product_id
,ps.retailer_product_id as product_id
,ps.price as balance_price
,pro.sku as Retail_ID
,pro.ean
,ps.stock
,r.name as Brand_Name
,ps.store_id as store_id_cp
,vs.id as store_id
,vs.name as store_name
,vs.store_type
--,s.retailer_id
,base.in_content
,base.VERTICAL_SUB_GROUP
,pro.category_id as Taxonomy_ID
--,SALE_TYPE as sell_type_type
,pro.sell_Type:minQuantity as sell_type_min_quantity
,pro.sell_Type:maxQuantity as sell_type_max_quantity
,pro.sell_type:stepQuantity as sell_type_step_quantity
,pro.sell_type:type as sell_type_type
from mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.product ps
left JOIN mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.store s on s.id = ps.store_id
left JOIN mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_store vs on vs.store_id = s.id
left join mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER_PRODUCT pro on ps.retailer_product_id = pro.id
left join mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
join base on base.country = 'MX' and base.store_id = vs.id
where
ps._FIVETRAN_DELETED = ('FALSE')
and ps.status in ('published')
and ps.in_stock = ('TRUE')
and s._FIVETRAN_DELETED = ('FALSE')
and s.status in ('published')
and vs._FIVETRAN_DELETED = ('FALSE')
and vs.is_enabled in ('TRUE')
and vs.status in ('published')
and ps.in_stock = 'TRUE'
--and ps.stock < 1
and pro.sell_type:type in ('U')
--and pro.ean not in ('NULL')
),

stores_on as (
select
country,
product_id,
count (distinct store_id) as stores_on
from ps

group by 1,2
),

precio as (
select
country
,product_id
,percentile_cont(0.60) within group (order by balance_price asc) over (partition by country , product_id) as percentile_60

from ps
)

, final as (
select distinct
 ps.country
,store_product_id
,ps.product_id
,ps.Retail_ID
,ps.ean
,b.product_id as Master_PID
,d.name as product_name
,ps.Brand_Name
,ps.balance_price as Error_Price
,p.percentile_60 as Real_Price
,ps.stock
,VERTICAL_SUB_GROUP
,ps.store_id as store_id
,store_id_cp as store_id_cp
,ps.store_name
,ps.Taxonomy_ID
,abs(percentile_60 - balance_price) Variation_Money
,Variation_Money / percentile_60 as Variation_Porcent
,stores_on
,d.long_description as description
,balance_price as price
,st.type as store_type

from ps
join precio p on p.product_id = ps.product_id and p.country = ps.country
left join stores_on as s on s.product_id = ps.product_id and s.country = ps.country
left join MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT b on b.id = ps.product_id
left join MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.PRODUCT d on d.id = b.product_id
left join mx_pglr_ms_stores_public.stores st on st.store_id = ps.store_id
where percentile_60 > balance_price
and variation_porcent > .6
and real_price not in (10000,1000)
and real_price < 10000
and Taxonomy_ID not in (90,91,92,93,94,103,104,105,106,1028,116,117,120,121,158,1037,626,215,214,213,639,167,437,578)
order by error_price, VARIATION_PORCENT desc, STORE_NAME)


select 
country,
description,
store_product_id,
ean,
product_name,
store_id_cp,
product_id,
store_id,
price,
store_type,
store_name as name,
TRUE as in_content,
VERTICAL_SUB_GROUP,
brand_name as brand

    from final
)

        union all

  select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from cl_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN cl_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN cl_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join cl_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join cl_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'CL' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

        union all

   select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from ar_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN ar_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN ar_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join ar_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join ar_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'AR' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

  union all

       select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from pe_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN pe_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN pe_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join pe_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join pe_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'PE' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

  union all

   select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from ec_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN ec_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN ec_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join ec_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join ec_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'EC' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

  union all

    select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from cr_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN cr_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN cr_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join cr_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join cr_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'CR' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

  union all

    select
base.country,
p.long_description as description,
ps.id as store_product_id,
ean, p.name as product_name,
ps.store_id as store_id_cp,
ps.retailer_product_id as product_id,
vs.id as store_id,
ps.price,
vs.store_type,
vs.name,
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand
from uy_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
left JOIN uy_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
left JOIN uy_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
left join uy_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product pro on ps.retailer_product_id = pro.id
--left join br_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
left join uy_PGLR_MS_CPGS_CLG_PM_PUBLIC.product p on p.id=pro.product_id
join base on base.country = 'UY' and base.store_id = vs.id
  where
    ps._FIVETRAN_DELETED = ('FALSE')
  and ps.status in ('published')
  and ps.in_stock = ('TRUE')
  and s._FIVETRAN_DELETED = ('FALSE')
  and s.status in ('published')
  and vs._FIVETRAN_DELETED = ('FALSE')
  and vs.is_enabled in ('TRUE')
  and vs.status in ('published')
  and ps.in_stock = 'TRUE'

    )

, stores_on as (
  select
    country,
    product_id,
  count (distinct store_id) as stores_on
 from product_store

    group by 1,2
)

,precio as (select country , product_id  ,
percentile_cont(0.50) within group (order by price asc) over (partition by country , product_id ) as percentile_50

from product_store
           ),

final as (
select distinct ps.country ,
ps.product_id , ps.description, ps.store_id , ps.name, ps.store_type, ps.price ,  p.percentile_50 ,VERTICAL_SUB_GROUP,brand
, in_content,store_product_id, store_id_cp,
abs(percentile_50 - price) calculo , calculo / percentile_50 as calculo2 ,stores_on, product_name, ean
from  product_store ps
join  precio p on p.product_id = ps.product_id and p.country = ps.country
left join stores_on as s on  s.product_id = ps.product_id and s.country = ps.country
where
calculo2 > 5
and p.percentile_50 > 0
and calculo > 0)

SELECT F.COUNTRY,
F.PRODUCT_ID,
F.DESCRIPTION,
F.STORE_ID,
F.NAME AS STORE_NAME,
F.STORE_PRODUCT_ID,
F.price,
F.VERTICAL_SUB_GROUP AS VERTICAL,
KAM_FRONT,
KAM_OPS,
percentile_50,
product_name,
ean,
f.store_type,
ph.physical_store_id
FROM FINAL F
LEFT JOIN OPS_OCC.CPGS_KAMS_VW K ON K.COUNTRY = F.COUNTRY AND K.STORE_TYPE = F.STORE_TYPE
LEFT JOIN (
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'BR' AS COUNTRY
FROM BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'CO' AS COUNTRY
FROM CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES  
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'MX' AS COUNTRY
FROM MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'AR' AS COUNTRY
FROM AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES  
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'CL' AS COUNTRY
FROM CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES  
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'PE' AS COUNTRY
FROM PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'AR' AS COUNTRY
FROM AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'CR' AS COUNTRY
FROM CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES  
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'EC' AS COUNTRY
FROM EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES
UNION ALL
SELECT DISTINCT STORE_ID, PHYSICAL_STORE_ID, 'UY' AS COUNTRY
FROM UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES     
) PH ON PH.COUNTRY = F.COUNTRY AND PH.STORE_ID = F.STORE_ID 
where VERTICAL not in ('TURBO')
AND NOT(F.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def products_oa(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::int as storeid_, product_id::int as productid_ from override_availability
    	    where ends_at >= now() at time zone 'America/{timezone}'
    	    and (in_stock = False or status = 'unpublished')
    	    order by ends_at desc
    	    '''.format(timezone=timezone)

    if country == 'ar':
      df = redash.run_query(6519, query)
    elif country == 'br':
      df = redash.run_query(6520, query)
    elif country == 'cl':
      df = redash.run_query(6521, query)
    elif country == 'co':
      df = redash.run_query(6522, query)
    elif country == 'cr':
      df = redash.run_query(6523, query)
    elif country == 'ec':
      df = redash.run_query(6524, query)
    elif country == 'mx':
      df = redash.run_query(6526, query)
    elif country == 'pe':
      df = redash.run_query(6525, query)
    elif country == 'uy':
      df = redash.run_query(6527, query)

    return df

def run_alarm(df,df_oa,df_excluding_prod):

    if not df_oa.empty:
        print(df)

        df_oa['productid_'] = df_oa['productid_'].astype(int).astype(str)
        df_oa['storeid_'] = df_oa['storeid_'].astype(int).astype(str)
        df['product_id'] = df['product_id'].astype(str)
        df['store_id'] = df['store_id'].astype(str)
        df['physical_store_id'] = df['physical_store_id'].astype(str)
        df_excluding_prod['productid'] = df_excluding_prod['productid'].astype(str)
        df_excluding_prod['storeid'] = df_excluding_prod['storeid'].astype(str)

        df_excluding_prod = pd.merge(df_excluding_prod, df_oa, how="left", left_on=['productid', 'storeid'], right_on=['productid_', 'storeid_'])
        df_excluding_prod = df_excluding_prod[(df_excluding_prod['productid_'].isnull())]
        df_excluding_prod = df_excluding_prod.drop(['productid_', 'storeid_'], axis=1)

        df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'physical_store_id'], right_on=['productid_', 'storeid_'])
        df = df[(df['productid_'].isnull())]
        df = df.drop(['productid_', 'storeid_'], axis=1)
        print(df)

        if not df.empty:
            df = pd.merge(df, df_excluding_prod, how="left", left_on=['country', 'product_id', 'store_id'],
                          right_on=['country_', 'productid', 'storeid'])
            df = df[(df['productid'].isnull())]
            df = df.drop(['country_', 'productid', 'storeid'], axis=1)

        print(df)

        if not df.empty:
            df['price'] = round(df['price'],2)
            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "product_id", "description", "store_id", "store_product_id", "price", "vertical", "kam_front", "kam_ops", "store_name"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_price_dif_cp")

            text = '''
            *NONLIVE - ALARMA PRICE DIF TIENDAS CONTENT PORTAL*

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_price_dif_cp.xlsx'.format(home)
            df_final = df[["country", "product_id","ean", "product_name", "description", "store_id", "store_name","store_type", "vertical", "store_product_id", "price","percentile_50", "kam_front", "kam_ops"]]
            df_final.to_excel(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file,"xlsx")
            print("okkk")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack(': ok_exp_qa: NO TENEMOS PRODUCTOS CON ERRORES DE DIFERENCIA DE PRECIO EN ESTE MOMENTO (CONTENT PORTAL) :ok_exp_qa:','C02MJQ1Q5H6')


countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']
df1 = []
for country in countries:
    try:
        df2 = products_oa(country)
        df1.append(df2)
        df_oa = pd.concat(df1, ignore_index=True)
    except Exception as e:
        print(e)
try:
  df = products()
  df_excluding_prod = excluding_products()
  run_alarm(df,df_oa,df_excluding_prod)
except Exception as e:
    print(e)

