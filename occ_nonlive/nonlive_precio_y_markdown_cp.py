import pandas as pd
import os
from lib import slack, redash
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz
from functions import timezones

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

  query_so = ''' 
SELECT DISTINCT
            'MX' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product AS ps
    JOIN mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store AS s ON s.id = ps.store_id
    JOIN mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.MX_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'MX'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'MX' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR  (PS.PRICE BETWEEN 0 AND 1))
    
    UNION ALL
    
SELECT DISTINCT
            'BR' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM BR_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product AS ps
    JOIN BR_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store AS s ON s.id = ps.store_id
    JOIN BR_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.BR_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN BR_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN BR_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'BR'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'BR' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR (PS.PRICE = 0)) 
    
    UNION ALL
    
SELECT DISTINCT
            'CO' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM CO_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product AS ps
    JOIN CO_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store AS s ON s.id = ps.store_id
    JOIN CO_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.CO_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN CO_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN CO_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'CO'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'CO' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR  (PS.PRICE BETWEEN 0 AND 1))
    
    UNION ALL
    
SELECT DISTINCT
            'CL' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM CL_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN CL_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN CL_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.CL_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN CL_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN CL_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'CL'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'CL' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR  (PS.PRICE BETWEEN 0 AND 1))
    
    UNION ALL
    
SELECT DISTINCT
            'AR' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM AR_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN AR_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN AR_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.AR_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN AR_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN AR_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'AR'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'AR' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR  (PS.PRICE BETWEEN 0 AND 1))
    
    UNION ALL
    
SELECT DISTINCT
            'PE' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM PE_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN PE_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN PE_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.PE_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN PE_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN PE_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'PE'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'PE' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR (PS.PRICE = 0)) 
    
    UNION ALL
    
SELECT DISTINCT
            'CR' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM CR_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN CR_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN CR_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.CR_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN CR_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN CR_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'CR'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'CR' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR (PS.PRICE = 0)) 
    
    UNION ALL
    
SELECT DISTINCT
            'EC' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM EC_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN EC_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN EC_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.EC_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN EC_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN EC_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'EC'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'EC' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR (PS.PRICE = 0)) 
    
    UNION ALL
    
SELECT DISTINCT
            'UY' AS country,
            vs.id AS store_id,
            ps.retailer_product_id AS product_id,
            ps.id AS product_store_id,
            a.name,
            v.vertical_group as vertical,
            round(ps.price) as price,
            ps.retailer_markdown as markdown,
            pro.sku AS retail_id,
            ps.status,
            s.id as cp_store_id
    FROM UY_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN UY_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN UY_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN VERTICALS_LATAM.UY_VERTICALS_V2 AS v ON v.store_type = vs.store_type
    JOIN UY_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN UY_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = 'UY'
    JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.STORE_ID = VS.ID AND LS.COUNTRY_CODE = 'UY' AND LS.IN_CONTENT = 'TRUE'
    WHERE ps.status = 'published'
    AND ps.in_stock = 'TRUE'
    AND coalesce(ps._fivetran_deleted,false) = false
    AND coalesce(s._fivetran_deleted,false) = false
    AND coalesce(vs._fivetran_deleted,false) = false
    AND coalesce(pro._fivetran_deleted,false) = false
    AND coalesce(r._fivetran_deleted,false) = false
    AND NOT(VS.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%', '%turbo%'))
    AND V.VERTICAL_GROUP IN ('ECOMMERCE', 'CPGS')
    AND NOT(A.NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%'))
    AND ((ABS(MARKDOWN) >= 99) OR (PS.PRICE = 0)) 
                   
'''
  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def products_oa(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::int as storeid, product_id::int as productid from override_availability
            where ends_at >= now() at time zone 'America/{timezone}'
            and (in_stock = False or status = 'unpublished')
            order by ends_at desc
            '''.format(timezone=timezone)

    if country == 'ar':
      df = redash.run_query(6519, query)
    elif country == 'br':
      df = redash.run_query(6520, query)
    elif country == 'cl':
      df = redash.run_query(6521, query)
    elif country == 'co':
      df = redash.run_query(6522, query)
    elif country == 'cr':
      df = redash.run_query(6523, query)
    elif country == 'ec':
      df = redash.run_query(6524, query)
    elif country == 'mx':
      df = redash.run_query(6526, query)
    elif country == 'pe':
      df = redash.run_query(6525, query)
    elif country == 'uy':
      df = redash.run_query(6527, query)

    return df


def run_alarm(df,df_oa):
    if not df.empty:
        print(df)
        if not df_oa.empty:
            df_oa['productid'] = df_oa['productid'].astype(int).astype(str)
            df_oa['storeid'] = df_oa['storeid'].astype(int).astype(str)
            df['product_id'] = df['product_id'].astype(str)
            df['cp_store_id'] = df['cp_store_id'].astype(str)
            df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'cp_store_id'], right_on=['productid', 'storeid'])
            df = df[(df['productid'].isnull())]
            df = df.drop(['productid', 'storeid'], axis=1)

        print(df)
        if not df.empty:
          df['checked_at'] = current_time
          to_upload = df
          to_upload = to_upload[
            ["country", "checked_at", "store_id", "product_id", "product_store_id", "name", "vertical", "price", "markdown", "retail_id", "status"]]
          print("uploading df")
          snow.upload_df_occ(to_upload, "nl_precio_y_markdown_cp")
          text = '''
          *NONLIVE - PRODUCTOS CON PRECIO = 0 OR MARKDOWN >=99% (CONTENT PORTAL)*
          '''
          print(text)
          home = expanduser("~")
          results_file = '{}/details_nl_precio_y_markdown_cp.csv'.format(home)
          df_final = df[["country", "store_id", "product_id", "product_store_id", "name", "vertical", "price", "markdown", "retail_id", "status"]]
          df_final.to_csv(results_file, index=False)
          slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")
    else:
        print('products null')
        text = '''
        *NONLIVE - PRODUCTOS CON PRECIO = 0 OR MARKDOWN >=99% (CONTENT PORTAL)*
        Sin nuevos productos para actuar :verified:
        '''
        slack.bot_slack(text,'C02MJQ1Q5H6')


countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']
df1 = []
for country in countries:
    try:
        df2 = products_oa(country)
        df1.append(df2)
        df_oa = pd.concat(df1, ignore_index=True)
    except Exception as e:
        print(e)
try:
  df = products()
  run_alarm(df, df_oa)
except Exception as e:
    print(e)