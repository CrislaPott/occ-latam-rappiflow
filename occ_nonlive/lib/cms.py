import requests
import json
import os
from airflow.models import Variable

headers = {
    'accept': 'application/json, text/plain, */*',
    'Referer': 'https://retail-cms.rappi.com/stores',
    'Origin': 'https://retail-cms.rappi.com',
    'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.'
        '79 Safari/537.36',
    'Sec-Fetch-Mode': 'cors',
    'sec-fetch-site': 'cross-site',
    'Content-Type': 'application/json',
    'Authorization': Variable.get('CMS_KEY'),
    'Auth-user': 'kissflow',
    'Cache-Control': "no-cache"
}

base_url = {
    'ar': 'https://services.rappi.com.ar',
    'br': 'https://services.rappi.com.br',
    'cl': 'https://services.rappi.cl',
    'co': 'https://services.rappi.com',
    'cr': 'https://services.rappi.co.cr',
    'ec': 'https://services.rappi.com.ec',
    'mx': 'https://services.mxgrability.rappi.com',
    'pe': 'https://services.rappi.pe',
    'uy': 'https://services.rappi.com.uy',
    'test': 'https://services.rappi.com.br'
}

def turnStoreOff(store_id, time_closed, country, reason):
    data = {
        "is_enabled": False,
        "reason": reason,
        "time": time_closed,
    }
    response = requests.put(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/',
            str(store_id)]),
        headers=headers,
        data=json.dumps(data)
    )
    return(response)


def disable_store(store_id, country, reason, time_closed=None):
    data = {
        "store_attribute": {
            "store_id": int(store_id),
            "suspended_reason": reason
        },
        "time": time_closed
    }
    return requests.post(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/store-attribute',
        ]),
        headers=headers,
        data=json.dumps(data)
    ).status_code
