import os
import json
import requests
import numpy as np 
import pandas as pd
import snowflake.connector
from dotenv import load_dotenv
from datetime import datetime, timedelta
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from airflow.models import Variable

load_dotenv()

def run_query(query):
	"""Run a select query on Snowflake."""
	conn = snowflake.connector.connect(
		host=Variable.get('SNOWFLAKE_HOST'),
		user=Variable.get('SNOWFLAKE_USER'),
		password=Variable.get('SNOWFLAKE_PASSWORD'),
		account=Variable.get('SNOWFLAKE_ACCOUNT'),
		database=Variable.get('SNOWFLAKE_DATABASE'),
		warehouse=Variable.get('SNOWFLAKE_WAREHOUSE'),
		timezone='America/Recife'
	)
	cur = conn.cursor()
	cur.execute(query)
	df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
	conn.close()
	df.columns = [x.lower() for x in df.keys()]
	return df

def upload_df(df, table_name):
	engine = create_engine(URL(
		account = Variable.get('SNOWFLAKE_ACCOUNT'),
		user = Variable.get('SNOWFLAKE_USER'),
		password = Variable.get('SNOWFLAKE_PASSWORD'),
		database = Variable.get('SNOWFLAKE_DATABASE'),
		schema = 'br_writable',
		warehouse = Variable.get('SNOWFLAKE_WAREHOUSE')
	))
	connection = engine.connect()
	try:
		print('Check: ' + str(len(df)))
		data = [df.iloc[i:i + 10000] for i in range(0, len(df), 10000)]
		for dt in data:
			dt.to_sql(table_name, con=connection, if_exists='append', index=False)
			print('Bulk insert feito com sucesso')
	except Exception as e:
		print(e)
		print('Bulk deu merda')
	finally:
		connection.close()
		engine.dispose()

def upload_df_debug(df, table_name):
	engine = create_engine(URL(
		account = Variable.get('SNOWFLAKE_ACCOUNT'),
		user = Variable.get('SNOWFLAKE_USER'),
		password = Variable.get('SNOWFLAKE_PASSWORD'),
		database = Variable.get('SNOWFLAKE_DATABASE'),
		schema = 'br_writable',
		warehouse = Variable.get('SNOWFLAKE_WAREHOUSE')
	))
	connection = engine.connect()
	try:
		print('Check: ' + str(len(df)))
		data = [df.iloc[i:i + 10000] for i in range(0, len(df), 10000)]
		for dt in data:
			print(dt)
			dt.to_sql(table_name, con=connection, if_exists='append', index=False)
			print('Bulk insert feito com sucesso')
	except Exception as e:
		print(e)
		print('Bulk deu merda')
	finally:
		connection.close()
		engine.dispose()

def upload_df_occ(df, table_name):
	engine = create_engine(URL(
		account = Variable.get('SNOWFLAKE_ACCOUNT'),
		user = Variable.get('SNOWFLAKE_USER'),
		password = Variable.get('SNOWFLAKE_PASSWORD'),
		database = Variable.get('SNOWFLAKE_DATABASE'),
		schema = 'ops_occ',
		role = 'OPS_OCC_WRITE_ROLE',
		warehouse = 'cpgs_small_load',
	))
	connection = engine.connect()
	try:
		print('Check: ' + str(len(df)))
		data = [df.iloc[i:i + 10000] for i in range(0, len(df), 10000)]
		for dt in data:
			dt.to_sql(table_name, con=connection, if_exists='append', index=False)
			print('Bulk insert feito com sucesso')
	except Exception as e:
		print(e)
		print('Fail')
	finally:
		connection.close()
		engine.dispose()
		
def truncate_table(table_name):
	con = snowflake.connector.connect(
		host=Variable.get('SNOWFLAKE_HOST'),
		user=Variable.get('SNOWFLAKE_USER'),
		password=Variable.get('SNOWFLAKE_PASSWORD'),
		account=Variable.get('SNOWFLAKE_ACCOUNT'),
		database=Variable.get('SNOWFLAKE_DATABASE'),
		warehouse=Variable.get('SNOWFLAKE_WAREHOUSE')
	)
	try:
		cur = con.cursor()
		cur.execute("truncate BR_WRITABLE.{}".format(table_name))
		con.commit()
		cur.close()
	
	except Exception as e:
		con.rollback()
		raise e

	finally:
		con.close()
