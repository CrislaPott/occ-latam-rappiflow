import os, sys, json
import pandas as pd
from lib import snowflake as snow


def get_products(country):
    query = '''
with base as (select distinct g.country
                            , g.order_id
                            , case when g.state_type ilike '%cancel%' then 'canceled' else 'finished' end as state_type
                            , case when c.level_2 ilike '%stockout%' then 'stockout' else 'otros' end as level_2
               from ops_global.global_orders g
               left join ops_global.CANCELLATION_REASONS c on c.country = g.country and c.order_id = g.order_id
               where g.vertical in ('Licores','Express','Farmacia','Super/Hiper', 'Specialized')
               and g.created_at::date between dateadd(day,-4,convert_timezone('America/Buenos_Aires',current_timestamp()))::date and dateadd(day,-1,convert_timezone('America/Buenos_Aires',current_timestamp()))::date
               and g.store_type not in (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE) 
               ),

integrations as (
SELECT
    distinct i.country,
    s.store_id
    FROM
    (SELECT * FROM (
    SELECT DISTINCT RAPPI_STORE_ID, DELETED_AT, _FIVETRAN_DELETED, DATASOURCE_ID,
     DENSE_RANK() OVER (PARTITION BY RAPPI_STORE_ID ORDER BY ID DESC) AS RANK
        FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE)
      WHERE RANK = 1) nds
        JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE d ON nds.datasource_id = d.id
        JOIN (SELECT *, DENSE_RANK() OVER (PARTITION BY DATASOURCE ORDER BY ID DESC) AS RANK
        FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS WHERE INTEGRATION_NAME not ilike '%bundle%')
        i ON    i.datasource = d.name and i.rank = 1
        JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS_METADATA im ON i.id = im.id_integration 
        LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATION_VERSIONS iv ON iv.id_integration = i.id
        JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s ON s.store_id = nds.rappi_store_id
    LEFT JOIN (SELECT ID, ID_INTEGRATION, ID_TAG, DENSE_RANK() OVER (PARTITION BY ID_INTEGRATION ORDER BY ID DESC) AS RANK 
               FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags) it on it.id_integration = i.id and it.rank = 1
    LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
    WHERE
        coalesce(nds._fivetran_deleted,false) = false
        AND coalesce(d._fivetran_deleted,false) = false
        AND coalesce(s._fivetran_deleted,false) = false 
        AND nds.deleted_at is null AND d.deleted_at is null AND s.deleted_at is null
        AND upper(d.country) = '{country}' 
        AND upper(i.country) = '{country}' 
        AND ((i.run_mode = 'worker') or (i.run_mode = 'astronomer'and iv.is_enabled = 'true'))
        AND t.name not in ('Deactivated', 'In guarantee'))  

, DOLLAR_CHANGE AS (SELECT country_CODE as country, TRM  FROM GLOBAL_FINANCES.TRM_FIXED)

, WHITELIST AS (select retailer_product_id::text as product_id, country_code as country from
CPGS_SALESCAPABILITY.TBL_CORE_PRODUCTS_IDEAL_BASKET_BY_STORE_CP WHERE IS_COVERED = TRUE)

, VERTICALS AS (

                                                SELECT DISTINCT
                                                                CASE
                                                                                WHEN UPPER (VERTICAL_GROUP) IN ('ECOMMERCE',
                                                                                                                'RAPPIFAVOR',
                                                                                                                'RESTAURANTS',
                                                                                                                'WHIM') THEN UPPER (VERTICAL_GROUP)
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) IN ('BOOKSTORES_STATIONARY',
                                                                                                                    'PETS',
                                                                                                                    'FASHION',
                                                                                                                    'HOME',
                                                                                                                    'SPORTS_OUTDOORS',
                                                                                                                    'GIFTS',
                                                                                                                    'TICKETS',
                                                                                                                    'HEALTH_BEAUTY',
                                                                                                                    'RAPPISMILE',
                                                                                                                    'INFORMATIVE',
                                                                                                                    'MEGA',
                                                                                                                    'TECH',
                                                                                                                    'EVENTS',
                                                                                                                    'BABIES_KIDS',
                                                                                                                    'SERVICES',
                                                                                                                    'BETS',
                                                                                                                    'GAMING') THEN 'ECOMMERCE'
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) = 'PHARMACY' THEN 'FARMACIA'
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) = 'EXPRESS' THEN 'EXPRESS'
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) = 'LIQUOR' THEN 'LICORES'
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) = ('SUPER') THEN 'MERCADOS'
                                                                                WHEN UPPER (VERTICAL_SUB_GROUP) IN ('SPECIALIZED',
                                                                                                                    'RESTAURANTS',
                                                                                                                    'RAPPICASH',
                                                                                                                    'ANTOJOS',
                                                                                                                    'RAPPIFAVOR') THEN UPPER (VERTICAL_SUB_GROUP)
                                                                                ELSE 'OTHER'
                                                                END AS VERTICAL,
                                                                STORE_TYPE
                                                FROM            VERTICALS_LATAM.{country}_VERTICALS_V2) 

,stores_co as (SELECT s.store_id AS id,
                      v.vertical AS vertical,
                      s.type,
                      s.name AS name,
                      s.is_enabled,
                      bg.name AS brand,
                      s.super_store_id,
                      i.store_id as integration
              FROM {country}_PGLR_MS_STORES_PUBLIC.stores_vw  s
              LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS name
                                                           FROM     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) BG ON BG.STORE_TYPE = S.TYPE
              JOIN VERTICALS v ON v.store_type = s.type
              LEFT JOIN INTEGRATIONS I ON I.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
              WHERE v.vertical IN ('LICORES', 'MERCADOS', 'FARMACIA', 'EXPRESS', 'SPECIALIZED') AND s.type NOT ILIKE '%prueba%'
              AND coalesce(s._fivetran_deleted,false)=false
),
orders_co AS (select distinct * from base where country = '{country}'),

order_products_co AS (
    SELECT op.store_id,
           op.order_id,
           o.state_type,
           o.level_2,
           s.vertical,
           s.brand,
           s.type AS store_type,
           s.name AS store_name,
           case when s.integration is null then 'FALSE' else 'TRUE' end as has_integration,
           s.super_store_id,
           op.product_id,
           p.name AS product_name,
           p.ean,
           p.parent_id,
           ps.price,
           ps.balance_price ,
           ps.is_available ,
           ps.is_discontinued,
           ps.in_stock,
           p.retail_id,
           op.unit_price,
           op.total_price,
           op.units,
           p.deleted_at,
           op.id AS op_id,
           COALESCE(op._fivetran_deleted, FALSE) AS is_deleted
    FROM {country}_core_orders_public.order_product_vw op
    JOIN stores_co s ON op.store_id = s.id
    JOIN orders_co o ON op.order_id = o.order_id
    JOIN {country}_pglr_ms_grability_products_public.products_vw p ON op.product_id = p.id
    JOIN {country}_pglr_ms_grability_products_public.product_store_vw  PS ON PS.PRODUCT_ID = op.product_id and ps.store_id = op.store_id
),

order_products_removed_co AS (
    SELECT om.order_id,
           1 AS stockout,
           CASE WHEN op_1.product_id IS NOT NULL THEN 1 ELSE 0 END AS substituted,
           CASE WHEN op_1.product_id IS NOT NULL THEN op_1.total_price ELSE 0 END AS gmv_substituted,
           COALESCE(om.params['removed_product_id'], om.params['product_id']) AS product_id
    FROM {country}_core_orders_public.order_modifications_vw om
    join orders_co o on o.order_id = om.order_id
    LEFT JOIN order_products_co op_1 ON op_1.op_id = om.params['order_product_id']
    LEFT JOIN order_products_co op_2 ON op_2.op_id = om.params['removed_product_id']
where ((om.type in ('shopper_replacement_product') 
  and (coalesce(om.params:removed_product_id, om.params:product_id) <> om.params:added_product_id))
     or om.type in ('shopper_remove_product','remove_product', 'replacement_product', 'support_remove_product'))
  AND COALESCE(op_1.product_id, 0) != COALESCE(op_2.product_id, 1)
),

join_table_canceled_orders_by_so_with_orders_co AS (
    SELECT 
        '{country}' AS country,
        op.store_id,
        op.order_id,
        op.state_type,
        op.level_2,
        op.product_id,
        op.vertical,
        op.super_store_id,
        op.brand,
        op.store_type,
        op.store_name,
        op.has_integration,
        op.product_name,
        op.retail_id,
        op.ean,
        op.parent_id,
        op.units,
        op.unit_price,
        op.total_price,
        COALESCE(op.is_deleted, FALSE) is_deleted,
        op.deleted_at AS product_deleted_at,
        opr.substituted,
        opr.gmv_substituted,
        COALESCE(opr.stockout, 0) AS stockout,
        op.price,
        op.balance_price,
        op.is_available,
        op.is_discontinued,
        op.in_stock
    FROM order_products_co op
    LEFT JOIN order_products_removed_co opr ON op.order_id = opr.order_id AND (op.product_id = opr.product_id OR opr.product_id = op.op_id)
    WHERE NOT (is_deleted = TRUE AND stockout = 0)
),

final_table AS (
    SELECT
        a.country,
        order_id,
        state_type,
        level_2,
        vertical,
        brand,
        super_store_id,
        store_type,
        store_name,
        has_integration,
        store_id,
        product_id,
        product_name,
        retail_id,
        ean,
        parent_id,
        stockout,
        total_price/trm AS gmv,
        substituted,
        gmv_substituted/trm as gmv_substituted,
        price,
        balance_price ,
        is_available ,
        is_discontinued,
        in_stock
    FROM join_table_canceled_orders_by_so_with_orders_co a
    join dollar_change dc on dc.country = a.country
)
, metrics as (
SELECT
    co.country,
    vertical,
    store_type,
    brand,
    co.store_id,
    co.store_name,
    has_integration,
    co.product_id,
    product_name,
    retail_id,
    ean,
    parent_id,
    price,
    balance_price ,
    is_available ,
    is_discontinued,
    in_stock,
    super_store_id ,
    count (distinct order_id ) as total_orders,
    sum (gmv) as gmv ,
    count (distinct case when stockout = 1 then order_id else null end ) as so_orders ,
    sum ( case when stockout = 1 then gmv else null end ) as so_gmv ,
    count ( distinct case when substituted = 1 then order_id else null end ) as substituted_orders ,
    sum (  case when substituted = 1 then gmv else null end ) as substituted_gmv1 ,
    sum ( gmv_substituted) as substituted_gmv2 ,
    COUNT(distinct CASE WHEN state_type ilike '%cancel%' THEN order_id END) AS canceled_orders,
    COUNT(distinct CASE WHEN level_2 = 'stockout' THEN order_id END) AS canceled_so_orders,
    sum (CASE WHEN state_type ilike '%cancel%' THEN gmv END) AS canceled_gmv,
    case when total_orders = 0 then 0 else (total_orders - so_orders) / total_orders end as found_rate ,
    case when so_orders = 0 then null else substituted_orders /  so_orders end as subst_rate,
    case when total_orders = 0 then 0 else canceled_orders / total_orders end as cancel_rate,
    case when total_orders = 0 then 0 else canceled_so_orders / total_orders end as so_cancel_rate,
    case when cancel_rate > 0.50 then '72h'
         when cancel_rate <= 0.50 and found_rate < 0.50 then '72h'
         when cancel_rate <= 0.50 and found_rate between 0.50 and 0.79 then '24h'
         else null end as triggers,
    case when triggers in ('72h') then dateadd(day,+3, convert_timezone('America/Buenos_Aires',current_timestamp()))::date
         when triggers in ('24h') then dateadd(day,+1, convert_timezone('America/Buenos_Aires',current_timestamp()))::date
         end as ends_at
FROM final_table co
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
)
select convert_timezone('America/Buenos_Aires',current_timestamp())::date as day,
       m.country, m.vertical as sub_vertical, m.store_type, m.brand, m.store_id, m.store_name, m.super_store_id,
       m.product_id, m.product_name, m.retail_id, m.ean,
       m.total_orders, m.gmv, m.so_orders, m.substituted_orders, m.canceled_orders, m.canceled_so_orders,
       m.found_rate, m.subst_rate, m.cancel_rate,
       m.triggers, m.ends_at,m.has_integration
from metrics m
left join (select * from (select country, store_id, product_id, last_value(ends_at) over (partition by product_id, store_id, country order by day asc) as last_ends_at
         from ops_occ.nonlive_products
          ) group by 1,2,3,4)  nlp on nlp.product_id=m.product_id and nlp.store_id=m.store_id and nlp.country=m.country
left join WHITELIST on WHITELIST.product_id = m.product_id and upper(WHITELIST.country) = upper(m.country)
where triggers is not null
and (datediff(day,last_ends_at,convert_timezone('America/Buenos_Aires',current_timestamp())::date) >= (CASE WHEN m.triggers = '24h' THEN 4 WHEN m.triggers = '72h' THEN 7 END) 
    OR LAST_ENDS_AT IS NULL)
and (canceled_orders >= 2 or so_orders >= 2)
and WHITELIST.product_id is null
    '''.format(country=country)
    df = snow.run_query(query)
    snow.upload_df_occ(df, 'nonlive_products')
    return df


countries = ['uy', 'pe', 'ar', 'cl', 'ec', 'cr', 'br', 'mx', 'co']
for country in countries:
    print(country)
    try:
        df = get_products(country.upper())
        print(df)
    except Exception as e:
        print(e)