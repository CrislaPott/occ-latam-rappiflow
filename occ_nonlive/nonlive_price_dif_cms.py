import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz
from functions import timezones

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def excluding_products():
    query = '''select country as country_, product_id as productid, store_id as storeid from ops_occ.nl_price_dif_cp
where checked_at::date >= dateadd(hours ,-36,convert_timezone('America/Buenos_Aires',current_timestamp()))::date
'''
    df = snow.run_query(query)
    return df

def products():

    query_so = ''' 
with base as ( select distinct country_code as country ,store_type , store_id,store_name, SUB_VERTICAL as VERTICAL_SUB_GROUP,
              in_content, suspended ,coalesce (brand_group_name, store_brand_name) as brand
              from  CPGS_DATASCIENCE.LOOKUP_STORE  
              where 
             vertical in ('CPGS','ECOMMERCE') and 
             store_name not ilike('%prueba%')and store_name not ilike('%test%')
             and deleted_at is null
             and (suspended is null or suspended in ('false'))
             and in_content = ('FALSE')
             )

    ,product_store as (
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,
p.MIN_QUANTITY_IN_GRAMS,        
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id     
 from co_pglr_ms_grability_products_public.product_store_vw ps 
  left join co_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'CO' and base.store_id = ps.store_id
  LEFT JOIN CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%') 
        
union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description, 
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id                
 from br_pglr_ms_grability_products_public.product_store_vw ps 
  left join br_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'BR' and base.store_id = ps.store_id
  LEFT JOIN BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')
        
union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description, 
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id                
 from mx_pglr_ms_grability_products_public.product_store_vw ps 
  left join mx_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'MX' and base.store_id = ps.store_id
  LEFT JOIN MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')
        
union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id                 
 from cl_pglr_ms_grability_products_public.product_store_vw ps 
  left join cl_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'CL' and base.store_id = ps.store_id
  LEFT JOIN CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')
        
union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id                  
 from ar_pglr_ms_grability_products_public.product_store_vw ps 
  left join ar_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'AR' and base.store_id = ps.store_id
  LEFT JOIN AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')         
        
union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id              
 from pe_pglr_ms_grability_products_public.product_store_vw ps 
  left join pe_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'PE' and base.store_id = ps.store_id
  LEFT JOIN PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')         
        
        Union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description, 
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id              
 from ec_pglr_ms_grability_products_public.product_store_vw ps 
  left join ec_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'EC' and base.store_id = ps.store_id
  LEFT JOIN EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%') 
        
        union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id                
 from cr_pglr_ms_grability_products_public.product_store_vw ps 
  left join cr_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'CR' and base.store_id = ps.store_id
  LEFT JOIN CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%') 
        
        union all 
        
select  
base.country,
base.store_type, 
ean, p.name as product_name,   
ps.store_id,
base.store_name,
ps.product_id, 
ps.balance_price,        
ps.price,
p.sale_type,
p.description,  
p.MIN_QUANTITY_IN_GRAMS,         
base.in_content,
base.VERTICAL_SUB_GROUP,
base.brand,
ph.physical_store_id
 from uy_pglr_ms_grability_products_public.product_store_vw ps 
  left join uy_grability_public.products_vw p on p.id = ps.product_id 
  --left join co_PGLR_MS_CPGS_CLG_PM_PUBLIC.retailer_product as rp on ps.product_id = rp.id 
  join base on base.country = 'UY' and base.store_id = ps.store_id
  LEFT JOIN UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = PS.STORE_ID    
  where
  coalesce(ps._fivetran_deleted,false)=false
  and coalesce(p._fivetran_deleted,false)=false
  and ps.is_available = 'TRUE'
and ps.is_discontinued = 'FALSE'
AND PS.IN_STOCK = 'TRUE' 
and store_name not ilike('%prueba%')and store_name not ilike('%test%')         
        
    )
    
, stores_on as (
  select
    country,
    product_id,
  count (distinct store_id) as stores_on 
 from product_store 
 
    group by 1,2
) 
      
,precio as (select country , product_id  ,
percentile_cont(0.50) within group (order by balance_price asc) over (partition by country , product_id ) as percentile_50,
percentile_cont(0.50) within group (order by price asc) over (partition by country , product_id ) as percentile_50_price       
from product_store ps            
           ),
           
final as (    
select distinct ps.country,
ps.product_id , ps.store_id ,ps.store_name, ps.store_type, ps.balance_price ,ps.SALE_TYPE,ps.MIN_QUANTITY_IN_GRAMS ,ps.price, ps.description, p.percentile_50,percentile_50_price ,VERTICAL_SUB_GROUP,brand
, in_content, abs(percentile_50 - balance_price) calculo , div0(calculo,percentile_50) as calculo2, abs(percentile_50_price - price) calculo_price , div0(calculo_price,percentile_50_price) as calculo2_price ,stores_on,
case when ps.sale_type in ('U') then p.percentile_50 else  (div0(percentile_50,1000)*MIN_QUANTITY_IN_GRAMS) end as new_vw,
case when ps.sale_type in ('U') then p.percentile_50_price else  (div0(percentile_50_price,1000)*MIN_QUANTITY_IN_GRAMS) end as new_price,
abs(div0(balance_price,percentile_50)) as calculo3,
abs(div0(price,new_vw)) as calculo3_price,
case when ps.sale_type in ('U') then  (case when balance_price <> price then 'no_ok_u' else 'ok_u' end ) else 'vw' end as review_u,   
div0(percentile_50,trm) as trm_percentil_50,
div0(percentile_50_price,trm) as trm_percentil_50_price,
 case when ps.sale_type in ('U') and trm_percentil_50_price < (0.013) then  'APAGAR' else 'OK' end as trm_bp_u,
 case when ps.sale_type not in ('U') and percentile_50 < (0.013) then  'APAGAR' else 'OK' end as trm_p_notu , ean, product_name, ps.physical_store_id
from  product_store ps 
join  precio p on p.product_id = ps.product_id and p.country = ps.country 
left join stores_on as s on  s.product_id = ps.product_id and s.country = ps.country
LEFT JOIN GLOBAL_FINANCES.trm_fixed as trm ON ps.country=trm.country_code
where 
calculo3 > 5
or trm_bp_u in ('APAGAR') or trm_p_notu in ('APAGAR'))

SELECT F.COUNTRY,
F.PRODUCT_ID,
F.DESCRIPTION,
F.STORE_ID,
F.STORE_NAME,
F.STORE_TYPE,
F.VERTICAL_SUB_GROUP AS VERTICAL,
F.PRICE,
KAM_FRONT,
KAM_OPS,
calculo3,
ean, product_name, f.store_type, physical_store_id
FROM FINAL F
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K ON K.COUNTRY = F.COUNTRY AND K.STORE_TYPE = F.STORE_TYPE
where VERTICAL not in ('TURBO')
AND NOT(F.STORE_TYPE ILIKE ANY ('courier_hours', 'courier_sampling', '%sampling%', '%soat%', '%turbo%'))
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def products_oa(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::int as storeid_, product_id::int as productid_ from override_availability
    	    where ends_at >= now() at time zone 'America/{timezone}'
    	    and (in_stock = False or status = 'unpublished')
    	    order by ends_at desc
    	    '''.format(timezone=timezone)

    if country == 'ar':
      df = redash.run_query(6519, query)
    elif country == 'br':
      df = redash.run_query(6520, query)
    elif country == 'cl':
      df = redash.run_query(6521, query)
    elif country == 'co':
      df = redash.run_query(6522, query)
    elif country == 'cr':
      df = redash.run_query(6523, query)
    elif country == 'ec':
      df = redash.run_query(6524, query)
    elif country == 'mx':
      df = redash.run_query(6526, query)
    elif country == 'pe':
      df = redash.run_query(6525, query)
    elif country == 'uy':
      df = redash.run_query(6527, query)

    return df

def run_alarm(df,df_oa,df_excluding_prod):

    if not df.empty:
        print(df)
        if not df_oa.empty:

            df_oa['productid_'] = df_oa['productid_'].astype(int).astype(str)
            df_oa['storeid_'] = df_oa['storeid_'].astype(int).astype(str)
            df['product_id'] = df['product_id'].astype(str)
            df['store_id'] = df['store_id'].astype(str)
            df['physical_store_id'] = df['physical_store_id'].astype(str)
            df_excluding_prod['productid'] = df_excluding_prod['productid'].astype(str)
            df_excluding_prod['storeid'] = df_excluding_prod['storeid'].astype(str)
            df_excluding_prod = pd.merge(df_excluding_prod, df_oa, how="left", left_on=['productid', 'storeid'], right_on=['productid_', 'storeid_'])
            df_excluding_prod = df_excluding_prod[(df_excluding_prod['productid_'].isnull())]
            df_excluding_prod = df_excluding_prod.drop(['productid_', 'storeid_'], axis=1)

            df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'physical_store_id'], right_on=['productid_', 'storeid_'])
            df = df[(df['productid_'].isnull())]
            df = df.drop(['productid_', 'storeid_'], axis=1)

            if not df.empty:
                df = pd.merge(df, df_excluding_prod, how="left", left_on=['country', 'product_id', 'store_id'],
                              right_on=['country_', 'productid', 'storeid'])
                df = df[(df['productid'].isnull())]
                df = df.drop(['country_', 'productid', 'storeid'], axis=1)

            print(df)

            if not df.empty:

                df['checked_at'] = current_time
                to_upload = df
                to_upload = to_upload[
                    ["country", "checked_at", "product_id", "description", "store_id", "vertical", "price", "kam_front", "kam_ops", "store_name"]]

                print("uploading df")
                snow.upload_df_occ(to_upload, "nl_price_dif_cms")

                text = '''
                *NONLIVE - ALARMA PRICE DIF TIENDAS CMS*
    
                '''
                print(text)
                home = expanduser("~")
                results_file = '{}/details_price_dif_cms.csv'.format(home)
                df_final = df[["country", "product_id","ean","product_name", "description", "store_id", "store_name","store_type", "vertical", "price","calculo3", "kam_front", "kam_ops"]]
                df_final.to_csv(results_file, index=False)
                slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")


            else:
                print('products null')
                slack.bot_slack(': ok_exp_qa: NO TENEMOS PRODUCTOS CON ERRORES DE DIFERENCIA DE PRECIO EN ESTE MOMENTO (CMS) :ok_exp_qa:','C02MJQ1Q5H6')



countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']
df1 = []
for country in countries:
    try:
        df2 = products_oa(country)
        df1.append(df2)
        df_oa = pd.concat(df1, ignore_index=True)
    except Exception as e:
        print(e)
try:
  df = products()
  df_excluding_prod = excluding_products()
  run_alarm(df,df_oa,df_excluding_prod)
except Exception as e:
    print(e)
