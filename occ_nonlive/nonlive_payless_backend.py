import pandas as pd
import os
from lib import slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

  query_so = ''' 
with stores_backend as (
  
  select
  'BR' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from br_pg_ms_cpgops_integ_owners_public.stores s
  join br_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join BR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled = 'TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'CO' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from co_pg_ms_cpgops_integ_owners_public.stores s
  join co_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join co_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'MX' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from mx_pg_ms_cpgops_integ_owners_public.stores s
  join mx_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join MX_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'AR' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from AR_pg_ms_cpgops_integ_owners_public.stores s
  join AR_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join AR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'CL' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from CL_pg_ms_cpgops_integ_owners_public.stores s
  join CL_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join CL_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'PE' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from PE_pg_ms_cpgops_integ_owners_public.stores s
  join PE_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join PE_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'EC' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from EC_pg_ms_cpgops_integ_owners_public.stores s
  join EC_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join EC_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'CR' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from CR_pg_ms_cpgops_integ_owners_public.stores s
  join CR_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join CR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

    union all

  select
  'UY' as country,
  s.owner_id,
  o.owner,
  s.store_id,
  o.rappi_payless

  from UY_pg_ms_cpgops_integ_owners_public.stores s
  join UY_pg_ms_cpgops_integ_owners_public.owners o on s.owner_id = o.id
  join UY_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
  where st.is_enabled='TRUE'
    and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))

 )

, stores_payless as (
  
select 'BR' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join BR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
LEFT JOIN  VERTICALS_LATAM.BR_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'CO' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join CO_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
LEFT JOIN  VERTICALS_LATAM.CO_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'MX' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join MX_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.MX_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE  
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all

select 'AR' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join AR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.AR_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE  
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'CL' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join CL_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id 
LEFT JOIN  VERTICALS_LATAM.CR_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'PE' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join PE_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.PE_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'EC' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join EC_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.EC_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'CR' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join CR_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.CR_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE    
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1
  
  union all
  
select 'UY' as country,
        s.physical_store_id,
        p.name,
        s.store_id,
        c.payment_flow,
        V.VERTICAL_SUB_GROUP AS VERTICAL

from (select physical_store_id
             , payment_flow
             , rank() over (partition by physical_store_id order by _fivetran_synced desc) as rank 
      from UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.app_configs_physical_stores) c
join UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on c.physical_store_id = s.physical_store_id
join UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores p on s.physical_store_id = p.id
join UY_PGLR_MS_STORES_PUBLIC.STORES_VW st on st.store_id=s.store_id
LEFT JOIN  VERTICALS_LATAM.UY_VERTICALS_V2 V
ON         V.STORE_TYPE = ST.TYPE  
where st.is_enabled='TRUE'
  and coalesce (st._fivetran_deleted, 'FALSE') = 'FALSE'
    and st.deleted_at is null 
    and not(st.type ilike any ('%Dummy%', 'Dummie%', '%Sampling%', '%rappi_pay%'))
  and payment_flow = 'payless_integration'
  and rank = 1

)

select DISTINCT
sp.country,
sp.physical_store_id,
sp.name,
sp.payment_flow as config_erp,
sp.store_id AS "VIRTUAL STORE ID NOT PAYLESS IN BACKEND",
sp.vertical,
s.brand_group_name

from stores_payless sp
left join stores_backend sb 
 on sp.store_id = sb.store_id and sp.country = sb.country
left join ops_occ.store_infos_latam s on upper(s.country) = upper(sp.country) and s.store_id = sp.store_id
Where sb.owner_id is null
order by 1, 2 asc        
'''
  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def run_alarm(df):

  if not df.empty:
    print(df)

    if not df.empty:

      df['checked_at'] = current_time
      to_upload = df
      to_upload = to_upload[
        ["country", "checked_at", "physical_store_id", "name", "config_erp", "virtual store id not payless in backend", "vertical", "brand_group_name"]]

      print("uploading df")
      snow.upload_df_occ(to_upload, "nl_stores_payless_backend")
      text = '''
      *NONLIVE - Payless Stores in ERP and not in backend*
      '''
      print(text)
      home = expanduser("~")
      results_file = '{}/details_tiendas_payless_backend.csv'.format(home)
      df_final = df[["country", "physical_store_id", "name", "config_erp", "virtual store id not payless in backend", "vertical", "brand_group_name"]]
      df_final.to_csv(results_file, index=False)
      slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

    else:
      print('df null')
  else:
    print('products null')
    text = '''
    *NONLIVE - Payless Stores in ERP and not in backend*
    Sin nuevas tiendas para actuar :verified:
    '''
    slack.bot_slack(text,'C02MJQ1Q5H6')


df = products()

run_alarm(df)