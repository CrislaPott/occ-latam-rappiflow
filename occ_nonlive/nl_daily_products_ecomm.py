import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta


def products():

    query_so = ''' 
select
'DESABILITAR' as process,
ends_at as fecha_reactivacion, country, sub_vertical, brand, store_name, store_id, product_id, retail_id, has_integration, cancel_rate, found_rate, triggers
from
ops_occ.nonlive_products_ecomm
where day::date = current_date::date

union all

select
'HABILITAR' as process,
ends_at as fecha_reactivacion, country, sub_vertical, brand, store_name, store_id, product_id, retail_id, has_integration, cancel_rate, found_rate, triggers
from
ops_occ.nonlive_products_ecomm
where ends_at::date = current_date::date
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            text = '''
            *NONLIVE - ALARMA DAILY PRODUCTS ECOMMERCE*
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_daily_products_ecomm.csv'.format(home)
            df_final = df[["process", "fecha_reactivacion", "country", "sub_vertical", "brand", "store_name", "store_id", "product_id", "retail_id", "has_integration", "cancel_rate", "found_rate", "triggers"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')            
    else:
        print('products null')
        text = '''
        *ALARMA DAILY PRODUCTS ECOMMERCE*
        Sin nuevos productos para actuar :verified:
        '''
        slack.bot_slack(text,'C02MJQ1Q5H6')


df = products()

run_alarm(df)