import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 
--no_cache
WITH BASEBR AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       BR_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN BR_CORE_ORDERS_PUBLIC.ORDER_STORES OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  BR_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASEAR AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       AR_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN AR_CORE_ORDERS_PUBLIC.ORDER_STORES OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  AR_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASECL AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       CL_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN CL_CORE_ORDERS_PUBLIC.ORDER_STORES OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  CL_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASEMX AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       MX_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN MX_CORE_ORDERS_PUBLIC.ORDER_STORES OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  MX_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASECO AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       CO_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN CO_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  CO_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASEPE AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       PE_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN PE_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  PE_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASECR AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       CR_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN CR_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  CR_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASEEC AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       EC_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN EC_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  EC_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC),

BASEUY AS
(
           SELECT     Count(DISTINCT O.ID) AS TOTAL,
                      OS.STORE_ID
           FROM       UY_CORE_ORDERS_PUBLIC.ORDERS_VW O
           INNER JOIN UY_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
           ON         OS.ORDER_ID = O.ID
           LEFT JOIN  UY_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
           ON         OM.ORDER_ID = O.ID
           WHERE      O.STATE NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error')
           AND        O.CREATED_AT >= CURRENT_DATE -30
           AND        OM.TYPE IN ('taken_visible_order')
           AND        GET_PATH(OM.PARAMS,'storekeeper_id') NOT ILIKE 'null'
           GROUP BY   2
           HAVING     TOTAL >= 1
           ORDER BY   1 DESC)

SELECT     'BR'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       BR_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  BR_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.BR_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEBR B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'BR')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM BR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'AR'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       AR_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  AR_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.AR_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEAR B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'AR')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM AR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'CL'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       CL_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  CL_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.CL_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASECL B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'CL')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM CL_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL


SELECT     'CO'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       CO_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  CO_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.CO_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASECO B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'CO')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM CO_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'MX'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       MX_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  MX_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.MX_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEMX B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'MX')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM MX_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'PE'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       PE_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  PE_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.PE_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEPE B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'PE')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM PE_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'CR'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       CR_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  CR_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.CR_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASECR B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'CR')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM CR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'EC'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       EC_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  EC_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.EC_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEEC B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'EC')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM EC_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)

UNION ALL

SELECT     'UY'                 AS COUNTRY,
           V.VERTICAL_SUB_GROUP AS VERTICAL,
           S.STORE_ID,
           S.NAME AS STORE_NAME,
           BG.BRAND_GROUP_NAME as BRAND_NAME, 
           S.TYPE,
           KAM_FRONT,
           KAM_OPS

FROM       UY_PGLR_MS_STORES_PUBLIC.STORES_VW S
LEFT JOIN  UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
ON         PH.STORE_ID = S.STORE_ID
AND        COALESCE(PH._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  UY_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
ON         ST.ID = S.TYPE
AND        COALESCE(ST._FIVETRAN_DELETED,FALSE) = FALSE
LEFT JOIN  VERTICALS_LATAM.UY_VERTICALS_V2 V
ON         V.STORE_TYPE = S.TYPE
INNER JOIN BASEUY B
ON         B.STORE_ID = S.STORE_ID
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
ON         K.COUNTRY = COUNTRY
AND        K.STORE_TYPE = S.TYPE
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)

WHERE      COALESCE(S._FIVETRAN_DELETED,FALSE) = FALSE
AND        COALESCE(V._FIVETRAN_DELETED,FALSE) = FALSE
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        V.VERTICAL_GROUP = 'CPGS'
AND        S.DELETED_AT IS NULL
AND        NOT(S.NAME ILIKE ANY ('%DUMMY%', 'DUMMIE%'))
AND        S.TYPE NOT ILIKE '%prueba%'
AND        PH.PHYSICAL_STORE_ID IS NULL
AND        ST.PAYMENT_FLOW NOT IN ('IMMEDIATE')
AND        S.TYPE NOT          IN ('peri_domicilo',
                                   'cerveceria_35',
                                   'automercado',
                                   'cerveceria_35',
                                   'market',
                                   'euro_opticas_rappimall',
                                   'coronavirus',
                                   'ingressos_final',
                                   'cuidados',
                                   'recetas_home',
                                   'soat_webview',
                                   'soat_market',
                                   'soat',
                                   'loja_ja_market')
AND        S.TYPE NOT IN
                          (
                          SELECT DISTINCT STORE_TYPE
                          FROM            OPS_OCC.TURBO_STORE_TYPE
                          WHERE           COUNTRY = 'UY')
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM UY_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "vertical", "store_id", "type", "kam_front", "kam_ops", "store_name", "brand_name"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_tiendas_sin_id_fisico")

            text = '''
            *NONLIVE - Alarma Tiendas SIN ID FISICO*

            :no_entry: Es necesario que las tiendas sean reportadas vía Kissflow para que los ID físicos seas creados. Para crear un ticket haga clic en la opción "+" en la esquina inferior derecha, después en el "lupa" busque por "Maintenance - Ops (ERP and Login)"

            :warning: Esta tienda ha sido suspendida por tiempo indeterminado :warning:

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_tiendas_sin_id.csv'.format(home)
            df_final = df[["country", "vertical", "store_id", "store_name", "brand_name", "type", "kam_front", "kam_ops"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
            #slack.bot_slack('Sin Tiendas en el momento sin id physico','C02MJQ1Q5H6')
    else:
        print('products null')
        slack.bot_slack('Sin Tiendas en el momento sin id physico','C02MJQ1Q5H6')



df = products()

run_alarm(df)