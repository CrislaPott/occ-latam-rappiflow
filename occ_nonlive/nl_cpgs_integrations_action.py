import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from functions import timezones, snippets


def products(country):
  timezone, interval = timezones.country_timezones(country)
  query_so = ''' 
   --no_cache
   
   WITH REPORTED AS (
   SELECT *
   FROM ops_occ.cpgs_integrations_report
   WHERE checked_at::date = convert_timezone('America/{timezone}',CURRENT_TIMESTAMP)::date
   AND COUNTRY = '{country}'),
   
   I AS

   (

   SELECT RAPPI_STORE_ID, MAX(LAST_INTEGRATION) AS FINAL_INTEGRATION
   FROM

   (  SELECT DISTINCT RAPPI_STORE_ID,
   MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
   FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
   WHERE           STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
   GROUP BY        1
   UNION ALL
   SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID,
   MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',EXECUTION_DATE)) AS LAST_INTEGRATION
   FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
   WHERE           try_to_date(execution_date)::DATE >= CURRENT_DATE - 7
   GROUP BY        1 )
   GROUP BY 1),
   
  FINAL AS ( 
  SELECT B.*, 
  I.FINAL_INTEGRATION,
  DATEDIFF(HOUR, I.FINAL_INTEGRATION::TIMESTAMP_NTZ,    
  CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::TIMESTAMP_NTZ) AS DIF 
  FROM REPORTED B
  LEFT JOIN I ON I.RAPPI_STORE_ID = B.TIENDA_QUE_INTEGRA)
  
  SELECT 
  F.COUNTRY,
  F.STORE_ID,
  F.NAME,
  COALESCE(F.FINAL_INTEGRATION::text, 'No integration in the last 7 days') AS LAST_INTEGRATION,
  F.INTEGRATION_NAME,
  F.BRAND_GROUP_NAME,
  F.TYPE,
  F.TIENDA_QUE_INTEGRA,
  F.VERTICAL,
  F.ORDERS_TOTAL,
  F.CANCEL_RATE,
  F.PARTNER_RELATED,
  F.TIER_GMV,
  F.ACTION,
  F.KAM_FRONT,
  F.KAM_OPS,
  ST.SCHEDULED AS HAS_SLOTS,
  SP.SCHEDULED AS HAS_SLOTS_TIENDA_QUE_INTEGRA,
  CASE WHEN HAS_SLOTS = 'TRUE' AND HAS_SLOTS_TIENDA_QUE_INTEGRA = 'TRUE' THEN 'REPORTAR AMBAS'
  WHEN HAS_SLOTS = 'TRUE' AND HAS_SLOTS_TIENDA_QUE_INTEGRA = 'FALSE' THEN 'SUSPENSION PADRE'
  WHEN HAS_SLOTS = 'FALSE' AND HAS_SLOTS_TIENDA_QUE_INTEGRA = 'TRUE' THEN 'SUSPENSION HIJA'
  WHEN HAS_SLOTS = 'FALSE' AND HAS_SLOTS_TIENDA_QUE_INTEGRA = 'FALSE' THEN 'SUSPENDER AMBAS'
  END AS SUSPENSION,
  CASE WHEN COUNTRY = 'MX' AND F.BRAND_GROUP_NAME ILIKE '%7 Eleven%' AND (DIF >= 48 OR DIF IS NULL) THEN 'YES'
  ELSE 'NO' END AS FIRST,
  CASE WHEN (DIF >= 24 OR DIF IS NULL) THEN 'YES'
  ELSE NULL END AS SECOND
  FROM FINAL F
  LEFT JOIN       {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
  ON              ST.ID = F.TYPE
  LEFT JOIN (SELECT DISTINCT S.STORE_ID, S.TYPE, SS.SCHEDULED
            FROM {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
            LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
            ON SS.ID = S.TYPE
            WHERE S.SUPER_STORE_ID IS NULL) SP
  ON             SP.STORE_ID = F.TIENDA_QUE_INTEGRA
  WHERE (FIRST = 'YES' OR SECOND = 'YES')
  '''.format(timezone=timezone, country=country.upper())

  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:
            current_time = timezones.country_current_time(country)
            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "store_id", "name", "last_integration", "integration_name", "brand_group_name", "type", "tienda_que_integra", "vertical", "orders_total", "cancel_rate", "partner_related", "tier_gmv", "action", "kam_front", "kam_ops", "has_slots", "has_slots_tienda_que_integra", "suspension"]]
            print("uploading df")
            snow.upload_df_occ(to_upload, "cpgs_integrations_action")
            text = '''
            *NONLIVE - Alarma Integrations - ACTION CPGS*
            Tiendas que no han integrado desde el reporte deben ser suspendidas.
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_action_integrations_cpgs.csv'.format(home)
            df_final = df[["country", "store_id", "name", "last_integration", "integration_name", "brand_group_name", "type", "tienda_que_integra", "vertical", "orders_total", "cancel_rate", "partner_related", "tier_gmv", "action", "kam_front", "kam_ops", "has_slots", "has_slots_tienda_que_integra", "suspension"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        text = '''
        *NONLIVE - Alarma Integrations - ACTION CPGS*
        Sin nuevas tiendas para actuar :verified:
        '''
        slack.bot_slack(text,'C02MJQ1Q5H6')


data = []
countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        df = products(country)
        data.append(df)
    except Exception as e:
        print(e)

df_final = pd.concat(data, ignore_index=True)
run_alarm(df_final)