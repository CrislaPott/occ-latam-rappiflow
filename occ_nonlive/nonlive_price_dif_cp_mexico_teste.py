import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 
(
with base as (
select distinct
country_code as country
,store_type
,store_id
,store_name
,SUB_VERTICAL as VERTICAL_SUB_GROUP
,in_content
,suspended
from CPGS_DATASCIENCE.LOOKUP_STORE
where vertical in ('CPGS')
and store_name not ilike('%prueba%')and store_name not ilike('%test%')
and deleted_at is null
and (suspended is null or suspended in ('false'))
and in_content = ('TRUE')
and SUB_VERTICAL in ('SUPER','LIQUOR','EXPRESS','PHARMACY')
and country_code in ('MX')
),

ps as (

select
base.country
,ps.id as store_product_id
,ps.retailer_product_id as product_id
,ps.price as balance_price
,pro.sku as Retail_ID
,pro.ean
,ps.stock
,r.name as Brand_Name
,ps.store_id as store_id_cp
,vs.id as store_id
,vs.name as store_name
,vs.store_type
--,s.retailer_id
,base.in_content
,base.VERTICAL_SUB_GROUP
,pro.category_id as Taxonomy_ID
--,SALE_TYPE as sell_type_type
,pro.sell_Type:minQuantity as sell_type_min_quantity
,pro.sell_Type:maxQuantity as sell_type_max_quantity
,pro.sell_type:stepQuantity as sell_type_step_quantity
,pro.sell_type:type as sell_type_type
from mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.product ps
left JOIN mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.store s on s.id = ps.store_id
left JOIN mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_store vs on vs.store_id = s.id
left join mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER_PRODUCT pro on ps.retailer_product_id = pro.id
left join mx_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER r on r.id = pro.retailer_id
join base on base.country = 'MX' and base.store_id = vs.id
where
ps._FIVETRAN_DELETED = ('FALSE')
and ps.status in ('published')
and ps.in_stock = ('TRUE')
and s._FIVETRAN_DELETED = ('FALSE')
and s.status in ('published')
and vs._FIVETRAN_DELETED = ('FALSE')
and vs.is_enabled in ('TRUE')
and vs.status in ('published')
and ps.in_stock = 'TRUE'
--and ps.stock < 1
and pro.sell_type:type in ('U')
--and pro.ean not in ('NULL')
),

stores_on as (
select
country,
product_id,
count (distinct store_id) as stores_on
from ps

group by 1,2
),

precio2 as (
select
country
,product_id
,percentile_cont(0.60) within group (order by balance_price asc) over (partition by country , product_id) as percentile_60

from ps
)

, final2 as (
select distinct
 ps.country
,store_product_id
,ps.product_id
,ps.Retail_ID
,ps.ean
,b.product_id as Master_PID
,d.name as product_name
,ps.Brand_Name
,ps.balance_price as Error_Price
,p.percentile_60 as Real_Price
,ps.stock
,VERTICAL_SUB_GROUP
,ps.store_id as store_id
,store_id_cp as store_id_cp
,ps.store_name
,ps.Taxonomy_ID
,abs(percentile_60 - balance_price) Variation_Money
,Variation_Money / percentile_60 as Variation_Porcent
,stores_on
,d.long_description as description
,balance_price as price
,st.type as store_type

from ps
join precio2 p on p.product_id = ps.product_id and p.country = ps.country
left join stores_on as s on s.product_id = ps.product_id and s.country = ps.country
left join MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT b on b.id = ps.product_id
left join MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.PRODUCT d on d.id = b.product_id
left join mx_pglr_ms_stores_public.stores st on st.store_id = ps.store_id
where percentile_60 > balance_price
and variation_porcent > .6
and real_price not in (10000,1000)
and real_price < 10000
and Taxonomy_ID not in (90,91,92,93,94,103,104,105,106,1028,116,117,120,121,158,1037,626,215,214,213,639,167,437,578)
order by error_price, VARIATION_PORCENT desc, STORE_NAME)


SELECT
F.COUNTRY,
F.PRODUCT_ID,
F.DESCRIPTION,
F.STORE_ID,
F.store_name,
F.STORE_PRODUCT_ID,
F.Error_Price as price,
Real_Price,
F.VERTICAL_SUB_GROUP AS VERTICAL,
KAM_FRONT,
KAM_OPS,
product_name,
ean,
variation_Money,
Variation_Porcent,
f.store_type

from final2 f
LEFT JOIN OPS_OCC.CPGS_KAMS_VW K ON K.COUNTRY = F.COUNTRY AND K.STORE_TYPE = F.STORE_TYPE
)
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:
            df['price'] = round(df['price'],2)
            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "product_id", "description", "store_id", "store_product_id", "price", "vertical", "kam_front", "kam_ops", "store_name"]]

            print("uploading df")
            #snow.upload_df_occ(to_upload, "nl_price_dif_cp")

            text = '''
            *NONLIVE - ALARMA PRICE DIF TIENDAS CONTENT PORTAL - MEXICO (QUERY EN PRUEBA)*

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_price_dif_cp_new_query.xlsx'.format(home)
            df_final = df[["country", "product_id","ean", "product_name", "description", "store_id", "store_name","store_type", "vertical", "store_product_id", "price","real_price","variation_money","variation_porcent","kam_front", "kam_ops"]]
            df_final.to_excel(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file,"xlsx")
            print("okkk")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack(': ok_exp_qa: NO TENEMOS PRODUCTOS CON ERRORES DE DIFERENCIA DE PRECIO EN ESTE MOMENTO (CONTENT PORTAL) - Mexico (query teste) :ok_exp_qa:','C02MJQ1Q5H6')


df = products()

run_alarm(df)