from os import remove
import os
import pandas as pd
from datetime import datetime
import pytz
from datetime import timedelta
import numpy as np
import time
from datetime import date
import datetime as dt
from datetime import timedelta
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones
from lib import slack


def LSNI_(Pais,complemento):
  
    if ((Pais.upper() == 'CO') | (Pais.upper() == 'MX') | (Pais.upper() == 'BR')):
        CP = f""" WITH 

  PRODUCTOS_CP as (
    SELECT DISTINCT
            '{Pais.upper()}' AS country,
            vs.id AS store_id, 
            pro.id AS product_id,
            a.name,
            pro.sku AS retail_id,
            CAST(ps.id AS INTEGER) AS product_store_id, 
            ps.status
    FROM {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product AS ps
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store AS s ON s.id = ps.store_id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store AS vs ON vs.store_id = s.id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = '{Pais.upper()}'
    WHERE ps.status != 'deleted' AND ps.status != 'discontinued' AND ps._fivetran_deleted = 'false'
),  
   
""";
    else:
       
        CP = f""" WITH 

  PRODUCTOS_CP as (
    SELECT DISTINCT
            '{Pais.upper()}' AS country,
            vs.id AS store_id, 
            pro.id AS product_id,
            a.name,
            pro.sku AS retail_id,
            CAST(ps.id AS INTEGER) AS product_store_id, 
            ps.status
    FROM {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory.product AS ps
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory.store AS s ON s.id = ps.store_id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store AS vs ON vs.store_id = s.id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer_product AS pro ON ps.retailer_product_id = pro.id
    JOIN {Pais.lower()}_amysql_cpgs_clg_im_cpgs_clg_inventory.retailer AS r ON r.id = pro.retailer_id
    JOIN ai_automation.global_analytics_product AS a ON a.product_id = pro.id AND a.country = '{Pais.upper()}'
    WHERE ps.status != 'deleted' AND ps.status != 'discontinued' AND ps._fivetran_deleted = 'false' 

), 

 """;

    query = CP + f"""

DM_DAILY_REPLACEMENT_ORDER_PRODUCTS as(
WITH order_product as (
	SELECT		DISTINCT
				date_trunc(week, COALESCE(o.taked_at, o.created_at))::date order_week,
				COALESCE(o.taked_at, o.created_at)::date order_date,
				COALESCE(o.taked_at, o.created_at) order_date_time,
				op.store_id,
				o.id order_id,
				op.product_id,
				op.units,
				op.total_price,
				op.id order_product_id,
				op.original_order_product_id,
				iff(op.status='confirmed','pending',op.status) 		AS status,
				op.created_at,
				IFF(os.was_taken IS NULL or os.was_taken, 'SHOPPER', 'RT') picker,
				op.product_preferred:product_info:product_id::int 	AS replacement_preferred_product, 
	 			op.preferred_action 								AS replacement_preferred_action,
				o.application_user_id								AS user_id
	FROM		{Pais.lower()}_core_orders_public.orders_vw o
	JOIN		{Pais.lower()}_pg_ms_cpgops_orders_ms_public.order_products op ON o.id = op.order_id
																						   AND op.status <> 'deleted'
	LEFT JOIN FIVETRAN.{Pais.lower()}_PG_MS_CPGOPS_ORDERS_MS_PUBLIC.ORDER_SUMMARY os ON  o.id = os.order_id
																		
	WHERE		
		   order_date::date BETWEEN dateadd('week', -1, date_trunc('week', current_date)) AND current_date
	       AND			
           o.state::varchar IN ('finished', 'pending_review')
)

, whim_replacements AS(
	SELECT	DISTINCT
			
			op.order_id,
			op.order_product_id not_found_order_product_id,
			op.product_id not_found_product_id,
			om.type,
			om.params:reason_id::string							AS reason_id,
			om.params:reason::string							AS reason,
			om.params:url_photo::string							AS whim_photo,
			om.params:url_photo IS NOT NULL						AS is_replacement_whim,
			om.params:name::string								AS whim_name,
			om.params:units::number								AS whim_units,
			om.params:price::number								AS whim_price
	FROM    order_product op
	JOIN    {Pais.lower()}_core_orders_public.order_modifications_vw om	ON  om.order_id = op.order_id
																	
																    AND (
																		  om.type = 'shopper_replacement_product'
																		  AND om.params:url_photo IS NOT NULL
																		  AND om.params:url_photo <> ''
																		  AND om.params:name IS NOT NULL
																		  AND om.params:name <> ''
																		  AND op.order_product_id = om.params:removed_product_id::number
																		)
	 WHERE  status = 'replaced'
)

,added_products AS(
	SELECT	DISTINCT
				
				op.order_id,
				op.original_order_product_id,
				FIRST_VALUE(op.order_product_id)	OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS replacement_order_product_id,
				FIRST_VALUE(op.product_id)			OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS replacement_product_id,
				FIRST_VALUE(op.units)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS replacement_units,
				FIRST_VALUE(op.total_price)			OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS replacement_total_price,
				FIRST_VALUE(op.status)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS replacement_product_status,
				FIRST_VALUE(type)					OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS type,
				FIRST_VALUE(reason_id)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)   AS reason_id,
				FIRST_VALUE(reason)					OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS reason,
				FIRST_VALUE(whim_photo)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS whim_photo,
				FIRST_VALUE(is_replacement_whim)	OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)   AS is_replacement_whim,
				FIRST_VALUE(whim_name)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS whim_name,
				FIRST_VALUE(whim_units)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS whim_units,
				FIRST_VALUE(whim_price)				OVER (PARTITION BY original_order_product_id ORDER BY op.created_at DESC)	AS whim_price
	FROM		order_product op
	LEFT JOIN	whim_replacements wr	ON	op.order_id=wr.order_id
										AND op.order_product_id=wr.not_found_order_product_id
	WHERE		original_order_product_id IS NOT NULL
)

, final_replacements AS(
	SELECT	
           
			op.store_id,
                        op.order_week,			
			op.order_date,
			op.order_date_time,
			op.order_id,
			op.picker,
			op.product_id,
			op.units,
			op.total_price,
			op.order_product_id,
			op.replacement_preferred_product,
			op.replacement_preferred_action,
			op.user_id,
			ap.replacement_product_id,
			COALESCE(s.whim_price, ap.whim_price, ap.replacement_total_price)		AS replacement_total_price,
			COALESCE(s.whim_units, ap.whim_units, ap.replacement_units)      		AS replacement_units,
			COALESCE(s.is_replacement_whim, ap.is_replacement_whim, FALSE)			AS is_replacement_whim,
			COALESCE(s.whim_name, ap.whim_name)										AS whim_name,
			COALESCE(s.whim_photo, ap.whim_photo)									AS whim_photo,
			COALESCE(s.reason_id, ap.reason_id)										AS modification_reason_id,
			COALESCE(s.reason, ap.reason)  											AS modification_reason,
			COALESCE(s.TYPE, ap.TYPE)     											AS modification_event_type,
			ap.replacement_order_product_id,
			ap.replacement_product_status,
			op.status
FROM		order_product op
LEFT JOIN	whim_replacements s ON   op.order_id = s.order_id
								AND op.product_id = s.not_found_product_id
								AND op.order_product_id = s.not_found_order_product_id
LEFT JOIN	added_products ap	ON  op.order_id = ap.order_id
					 			AND op.order_product_id = ap.original_order_product_id

)

SELECT
      store_id,
			order_week,
			order_date,
			order_date_time,
			order_id,
			picker,
			product_id,
			CASE
				WHEN (  status = 'checked' OR status='pending' ) THEN 'FOUND'
				WHEN (
						(   status = 'removed'
							AND NOT is_replacement_whim
							AND replacement_order_product_id IS NULL
						)
						OR (
							( status = 'replaced' OR status = 'removed' )
							AND replacement_product_status='removed'
						)
				) THEN 'REFUNDED'
				WHEN (  (status = 'replaced' OR status = 'removed')
						AND  (  is_replacement_whim
							    OR replacement_product_status = 'checked'
							    OR replacement_product_status = 'pending'
						)
				) THEN 'REPLACED'
				ELSE 'UNKNOWN'
			END product_state,
			units,
			total_price,
			order_product_id,
			iff(product_state='REPLACED' AND NOT is_replacement_whim, replacement_product_id, NULL)		AS replacement_product_id,
			iff(product_state='REPLACED', replacement_total_price, NULL)									AS replacement_total_price,
			iff(product_state='REPLACED', replacement_units, NULL)      									AS replacement_units,
			modification_event_type,
			iff(product_state='REPLACED' AND NOT is_replacement_whim, replacement_order_product_id, NULL)	AS replacement_order_product_id,
			product_state='REPLACED' AND NOT is_replacement_whim AND replacement_product_id=product_id	  AS is_fake_replacement		

FROM   final_replacements 
WHERE	 order_product_id NOT IN (	SELECT	op2.order_product_id
										FROM	order_product op2
										WHERE	op2.original_order_product_id IS NOT NULL)
),

padre_hija AS 
       (         
           SELECT coalesce(PHYSICAL_STORE_ID,STORE_ID) AS Padre, STORE_ID
           FROM CPGS_DATASCIENCE.cpgs_physical_store
           WHERE country_code = '{Pais.upper()}'
       ),

base as 
 (
    select DISTINCT
         g.order_id,
         g.country,
         i.Padre AS super_store_id,
         g.store_id, 
         CASE WHEN g.brand_group IS NULL THEN g.brand_name ELSE g.brand_group END AS brand_group
     FROM ops_global.global_orders AS g
     LEFT JOIN padre_hija AS i ON i.store_id = g.store_id   
     WHERE date_trunc('day',coalesce(g.place_at::date,g.created_at::date)) = date_trunc('day',current_date) {complemento}  
           AND g.vertical IN ('Farmacia','Super/Hiper','Licores','Express','Specialized','Pets')
           AND g.country = '{Pais.upper()}'
  ),


productos AS (

    SELECT DISTINCT op.order_id, op.product_id        
    FROM {Pais.lower()}_PG_MS_CPGOPS_ORDERS_MS_PUBLIC.order_products AS op
    JOIN base ON base.order_id = op.order_id  
    WHERE original_order_product_id IS NULL
   
),

sku AS (

    SELECT 
           base.country,
           op.order_id, 
           COUNT(op.product_id) AS sku_count          
    FROM productos AS OP
    JOIN base ON base.order_id = op.order_id  
    GROUP BY 1,2

),

cso_one_sku AS (

SELECT DISTINCT p.order_id, p.product_id
FROM sku AS g
JOIN ops_global.CANCELLATION_REASONS c ON c.order_id = g.order_id AND c.country = g.country
JOIN productos AS p ON p.order_id = g.order_id 
WHERE g.sku_count = 1 AND c.level_2 ilike '%stockout%'

),


modifications as (
   SELECT order_id, value:order_product_id as order_product_id, value:units as units 
   FROM {Pais.lower()}_core_orders_public.order_modifications_vw, lateral flatten(input =>  params:products) 
   WHERE (type='change_quantity_products' AND units=0) 
         AND 
         order_id IN (SELECT DISTINCT order_id FROM base)
), 
  
user_removal as (
    SELECT order_id, params:removed_product.product_id as product_id, params:reason_id as reason 
    FROM {Pais.lower()}_core_orders_public.order_modifications_vw
    WHERE reason=3
          AND 
          product_id is not null
          AND 
          order_id IN (SELECT DISTINCT order_id FROM base)
),


Previo AS (
select DISTINCT
         g.country,
         g.super_store_id,
         g.brand_group,
         COALESCE(op.product_id, cso.product_id) AS product_id
     FROM base AS g
     LEFT JOIN DM_DAILY_REPLACEMENT_ORDER_PRODUCTS AS op ON op.order_id = g.order_id
     LEFT JOIN modifications m ON m.order_id=op.order_id AND m.order_product_id=op.order_product_id
     LEFT JOIN user_removal ur ON ur.order_id=op.order_id AND ur.product_id=op.product_id
     LEFT JOIN cso_one_sku AS cso ON cso.order_id = g.order_id
     WHERE 
            ((op.product_state <>'FOUND') AND (m.order_product_id IS NULL) AND (ur.reason IS NULL) AND (op.is_fake_replacement = false)) 
            OR 
            (cso.order_id IS NOT NULL)
          
),


integracion AS ( 
            (
             SELECT DISTINCT try_to_numeric(rappi_store_id) AS tienda_que_integra, 
                             date_trunc('minute',stocker_execution_date) AS hour,
                             try_to_numeric(rappi_product_id) AS rappi_product_id,
                             partner_stock
                                         
             FROM fivetran.cpg_integrations.{Pais.lower()}_INTEGRATION_METRICS_METADATA AS i
             WHERE  1=1             
                AND hour::date >= current_date - 3
                AND try_to_numeric(i.rappi_store_id) in (SELECT DISTINCT super_store_id FROM Previo)
                AND try_to_numeric(i.rappi_product_id) in (SELECT DISTINCT product_id FROM Previo)
                AND RAPPI_PARTNER_NAME NOT IN ('grupo-casino-prices','grupo-casino-discounts')
                ORDER BY 1,2,3 ASC
             )

             UNION 
 
             (
	         SELECT DISTINCT try_to_numeric(virtual_store_id) AS tienda_que_integra,
                                 date_trunc('minute',TRY_TO_TIMESTAMP_NTZ(execution_date)) AS hour,
                                 try_to_numeric(rappi_product_id) AS rappi_product_id,
                                 stock AS partner_stock
                                 
             FROM cpg_integrations.{Pais.lower()}_NEW_CATALOG_INTEGRATION_METRICS_METADATA AS i
	         WHERE  1=1             
                AND hour::date >= current_date - 3
                AND try_to_numeric(i.virtual_store_id) in (SELECT DISTINCT super_store_id FROM Previo)
                AND try_to_numeric(i.rappi_product_id) in (SELECT DISTINCT product_id FROM Previo)
                AND RAPPI_PARTNER_NAME NOT IN ('grupo-casino-prices','grupo-casino-discounts')
                ORDER BY 1,2,3 ASC       
             )
  
),

integra_pre_max AS (

  SELECT tienda_que_integra, rappi_product_id, max(hour) AS last_inte
  FROM integracion
  GROUP BY 1,2

),

integra_max AS ( 
  
SELECT pre.tienda_que_integra, pre.rappi_product_id, pre.last_inte, int.partner_stock
FROM integra_pre_max AS pre
JOIN integracion AS int ON pre.tienda_que_integra = int.tienda_que_integra AND pre.RAPPI_PRODUCT_ID = int.RAPPI_PRODUCT_ID AND pre.last_inte = int.hour
WHERE pre.last_inte IS NOT NULL  

),

tienda_integra AS ( 
  
          SELECT DISTINCT RAPPI_STORE_ID
          FROM (
  
                SELECT DISTINCT RAPPI_STORE_ID              
                FROM CPGS_DATASCIENCE.{Pais.lower()}_INTEGRATIONS_LAST_EVENT
  
                UNION   
  
                SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID                                
                FROM CPGS_DATASCIENCE.{Pais.lower()}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
            
               )    
    
),


top_cp AS (
          SELECT DISTINCT country_code AS country, store_product_id  
          FROM CPGS_SALESCAPABILITY.TBL_CORE_PRODUCTS_IDEAL_BASKET_BY_STORE_CP
), 


Salida AS (
  
  SELECT b.country,
       cp.product_store_id,
       b.super_store_id,
       b.brand_group,
       b.product_id,
       aia.name AS product_name,
       CASE WHEN top_cp.store_product_id IS NULL THEN 0 ELSE 1 END AS top_seller_cp,
       CASE WHEN im.rappi_product_id IS NULL AND ti.rappi_store_id IS NOT NULL THEN 1 ELSE 0 END AS producto_no_integro, 
       im.partner_stock,
       im.last_inte,
       COALESCE(CASE WHEN k.sale_type ILIKE '%w%' THEN 1 ELSE 0 END,0) AS pesable,
       CASE WHEN ((pesable = 0 AND im.partner_stock < 5) OR (pesable = 1 AND im.partner_stock < 2)) AND (im.partner_stock > 0) THEN 1 ELSE 0 END AS stock_bajo
   FROM Previo AS b
   LEFT JOIN productos_cp AS cp 
         ON cp.store_id = b.super_store_id 
            AND 
            cp.product_id = b.product_id 
            AND 
            cp.country = b.country
   LEFT JOIN top_cp
        ON top_cp.country = cp.country
           AND
           top_cp.store_product_id = cp.product_store_id
   LEFT JOIN tienda_integra AS ti ON ti.rappi_store_id = b.super_store_id
   LEFT JOIN integra_max AS im ON im.tienda_que_integra = b.super_store_id AND im.rappi_product_id = b.product_id  
   LEFT JOIN ai_automation.global_analytics_product AS aia ON aia.product_id = b.product_id AND aia.country = '{Pais.upper()}'
   LEFT JOIN {Pais.lower()}_grability_public.products_vw AS k ON k.id = b.product_id
)

SELECT * 
FROM salida
WHERE 1 = 1
      AND top_seller_cp = 0
      AND (producto_no_integro = 1 OR stock_bajo = 1)
      AND NOT ( brand_group = 'Éxito' AND product_id IN (1297,1253,1098,594130,1532,1189,1186,1256,1072,1279,1220,1161,1402,
                                                                 1141,1179,1259,1278,1231,1486,1055,1290) 
              )


"""
    df = snow.run_query(query)
    return df

def alarm(Entrada):

    current_time = timezones.country_current_time(country)
    if not Entrada.empty:
        CMS_data = Entrada.copy().reset_index(drop=True)
        CMS_local = CMS_data[CMS_data['country'] == country.upper()].copy().reset_index(drop=True)

        time_date = current_time
        date_start = time_date.strftime("%m/%d/%Y")
        time_start = time_date.strftime("%H:%M:%S")
        date_end = (time_date + timedelta(days=day)).strftime("%m/%d/%Y")

        print('CMS_salida')
        CMS_salida = pd.DataFrame([])
        CMS_salida['store_id'] = CMS_local['super_store_id']
        CMS_salida['product_id'] = CMS_local['product_id']
        CMS_salida['start_date'] = date_start
        CMS_salida['start_time'] = time_start
        CMS_salida['end_date'] = date_end
        CMS_salida['end_time'] = '23:59:59'
        CMS_salida['price'] = ''
        CMS_salida['balance_price'] = ''
        CMS_salida['has_price_cut'] = ''
        CMS_salida['percentage_price_variation'] = ''
        CMS_salida['discount_percentage_by_rappi'] = ''
        CMS_salida['discount_percentage_by_revenue'] = ''
        CMS_salida['index'] = ''
        CMS_salida['is_available'] = ''
        CMS_salida['in_stock'] = 'false'

        CMS_salida = CMS_salida.drop_duplicates()
        print(CMS_salida)
        if not Entrada.empty:
            try:
                print('caracteristicas')
                caracteristicas = Entrada.copy().reset_index(drop=True)
                caracteristicas = caracteristicas.drop_duplicates()
                print(caracteristicas)
                if len(caracteristicas) > 0:
                    print('Excel')
                    home = expanduser("~")
                    results_file = '{home}/details_nonlive_products_stock_integration_{country}.xlsx'.format(home=home, country=country)
                    caracteristicas.to_excel(results_file, sheet_name='2_info', index=False)
                    book = load_workbook(results_file)
                    writer = pd.ExcelWriter(results_file, engine='openpyxl')
                    writer.book = book
                    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

            except:
                print('Error info ' + country)

            try:
                CP_data = Entrada[~Entrada['product_store_id'].isna()].copy().reset_index(drop=True)
                CP_local = CP_data[CP_data['country'] == country.upper()].copy().reset_index(drop=True)
                print(CP_local)
                CP_salida = pd.DataFrame([])
                CP_salida['store_product_id'] = CP_local['product_store_id'].map(int)
                CP_salida['price'] = ''
                CP_salida['retailer_markdown'] = ''
                CP_salida['stock'] = ''
                CP_salida['security_stock'] = ''
                CP_salida['in_stock'] = 'false'
                CP_salida['status'] = ''

                CP_salida = CP_salida.drop_duplicates()
                if len(CP_salida) > 0:
                    print('CP_salida')
                    print(CP_salida)
                    CP_salida.to_excel(writer, sheet_name="2_content_portal", index=False)
            except:
                pass

            if len(CMS_salida) > 0:
                print('CMS_salida')
                print(CMS_salida)
                CMS_salida.to_excel(writer, sheet_name="2_cms", index=False)
            try:
                print('OV_salida')
                OV_salida = pd.DataFrame([])
                OV_salida['store_id'] = CMS_local['super_store_id']
                OV_salida['product_id'] = CMS_local['product_id']
                OV_salida['price'] = ''
                OV_salida['markdown'] = ''
                OV_salida['status'] = ''
                OV_salida['in_stock'] = 'false'
                OV_salida = OV_salida.drop_duplicates()
                if len(OV_salida) > 0:
                    print('OV_salida')
                    print(OV_salida)
                    OV_salida.to_excel(writer, sheet_name="2_override_v3", index=False)
            except:
                print('Error override ' + country)

            writer.save()

            if len(caracteristicas) > 0:
                text = '''
                *NONLIVE - PRODUCTOS - LOW STOCK Y NO INTEGRATION*
                :flag-{country}:
                Pais: {country}
                '''.format(country=country)
                print(text)
                slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, 'xlsx')

                if os.path.exists(results_file):
                    os.remove(results_file)

            else:
                text = '''
                *NONLIVE - PRODUCTOS - LOW STOCK Y NO INTEGRATION*
                :flag-{country}:
                Pais: {country}
                No hay productos con stock bajo o sin integración.
                '''.format(country=country)
                print(text)
                slack.bot_slack(text, 'C02MJQ1Q5H6')
    else:
        print('Entrada empty')


zone = pytz.timezone('America/Bogota')
Global = datetime.now(zone)

Hora_actual = Global.hour

if Global.weekday() == 5:
    saturday = 1
else:
    saturday = 0

if Hora_actual > 12:
    tarde = 1
else:
    tarde = 0

if (saturday == 1) | (tarde == 1):
    day = 1
else:
    day = 0

if tarde == 1:
    complemento = " "
else:
    complemento = " - interval '1 day' "

for country in ['ar','co','mx','br','cl','ec','pe','uy','cr']:
    print(country)
    try:
        Entrada = LSNI_(country, complemento)
        print(Entrada)
        alarm(Entrada)
    except Exception as e:
        print(e)
