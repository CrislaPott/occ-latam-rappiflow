import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 
select distinct
'CO' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from co_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.co_verticals_v2 v on v.store_type = s.type
left join co_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from co_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join co_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CO')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM CO_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'BR' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from BR_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.BR_verticals_v2 v on v.store_type = s.type
left join BR_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from BR_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join BR_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'BR')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM BR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'MX' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from MX_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.MX_verticals_v2 v on v.store_type = s.type
left join MX_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from MX_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join MX_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'MX')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM MX_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'CL' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from CL_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.CL_verticals_v2 v on v.store_type = s.type
left join CL_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from CL_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join CL_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CL')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM CL_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'AR' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from AR_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.AR_verticals_v2 v on v.store_type = s.type
left join AR_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from AR_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join AR_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'AR')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM AR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'PE' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from PE_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.PE_verticals_v2 v on v.store_type = s.type
left join PE_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from PE_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join PE_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'PE')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM PE_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'EC' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from EC_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.EC_verticals_v2 v on v.store_type = s.type
left join EC_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from EC_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join EC_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'EC')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM EC_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'CR' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from CR_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.CR_verticals_v2 v on v.store_type = s.type
left join CR_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from CR_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join CR_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type



where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CR')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM CR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5

union all

select distinct
'UY' as country,
s.store_id,
s.name as store_name,
s.type,
v.vertical_sub_group as vertical,
BG.BRAND_GROUP_NAME AS BRAND_NAME,
kam_front,
kam_ops


from UY_PGLR_MS_STORES_PUBLIC.STORES_VW s
left join verticals_latam.UY_verticals_v2 v on v.store_type = s.type
left join UY_grability_public.city_addresses_vw ca on ca.id = s.city_address_id
left join( select store_id,starts_time,ends_time, day_of_week,_fivetran_deleted, updated_at from UY_PGLR_MS_STORE_SCHEDULES_PUBLIC.schedules) sc on sc.store_id=s.store_id and coalesce(sc._fivetran_deleted,false)=false
left join UY_PGLR_MS_STORES_PUBLIC.store_types st on st.id = s.type
LEFT JOIN
  (
   SELECT   *
    FROM     (
    SELECT   STORE_BRAND_ID,
    STORE_TYPE,
    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
    FROM     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
    JOIN     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
    ON       BRAND_GROUP_ID = BG.ID)
    GROUP BY 1,2,3,4) BG
ON   COALESCE(BG.STORE_BRAND_ID::TEXT, BG.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K on k.country = country and k.store_type = s.type


where v.vertical_group = 'CPGS'
and v.vertical_sub_group in ('SUPER','HIPER')
and not(s.name ilike any ('%Dummy%', 'Dummie%'))
and type not in ('oxxo_express_nc')
and s.deleted_at is null
and st.scheduled = false
and type not ilike '%prueba%'
and type not ilike '%fake%'
and st.payment_flow != 'IMMEDIATE'
and coalesce(s._fivetran_deleted,false)=false
and coalesce(v._fivetran_deleted,false)=false
and coalesce(ca._fivetran_deleted,false)=false
and coalesce(st._fivetran_deleted,false)=false
and ends_time >= '23:00:00'
and starts_time < '01:00:00'
and sc.day_of_week not in ('calculated')
AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'UY')
AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM UY_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
GROUP BY 1,2,3,4,5,6,7,8
having datediff('minutes', min(starts_time)::time, max(ends_time)::time) > 5
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "store_id", "store_name", "type", "kam_front", "kam_ops", "vertical", "brand_name", "store_name"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_tiendas_no_programadas")

            text = '''
            *NONLIVE - TIENDAS NO PROGRAMADAS CON HORARIO 24 HORAS *

            Para ajustar el horario de estas tiendas, es necesario abrir una solicitud a través del BOT de Whatsapp:

            :earth_americas: Para tiendas spanish speaking: +52 1 55 8896 9764. Seleccionar opción 14

            :flag-br: Para tiendas brasileñas: +55 61 3686 0633. Seleccionar opción 6

            :slack: Una vez que hayas ajustado el horario de la tienda, soliciten la reapertura en este hilo con la evidencia

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_tiendas_no_programadas.csv'.format(home)
            df_final = df[["country", "store_id", "store_name", "vertical", "brand_name", "type", "kam_front", "kam_ops"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack(':ok_qa: No hay tiendas que no están programadas en este momento :ok_qa:','C02MJQ1Q5H6')



df = products()

run_alarm(df)