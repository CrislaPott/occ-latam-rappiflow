import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from functions import timezones, snippets


def products(country):
    timezone, interval = timezones.country_timezones(country)
    query_so = ''' 
    with base as 
      
    (
    select distinct store_id from ops_occ.NON_LIVE_THREADS
    where process = 'INTEGRACAO CPGS'
    and timeadd(hour, -3, dateadd('seconds', date, '1970-01-01'))::date >= current_date -4
    and country = '{country}'),
      
    FINAL AS (
      SELECT DISTINCT S.STORE_ID,
                      CONVERT_TIMEZONE('UTC','America/{timezone}',ST._FIVETRAN_SYNCED::timestamp) AS SUSPENDED_AT,
                      COALESCE(S.SUPER_STORE_ID, S.STORE_ID) AS SUPER_STORE_ID
               FROM   {country}_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE ST 
               JOIN   {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID = ST.STORE_ID
               JOIN   BASE B ON B.STORE_ID = S.STORE_ID
                      AND ST._FIVETRAN_DELETED = 'FALSE'
                      AND SUSPENDED_REASON ilike '%OCC%'
                      AND SUSPENDED_REASON NOT ILIKE '%KAM%'),


    I AS (
      SELECT RAPPI_STORE_ID, MAX(LAST_INTEGRATION) AS LAST_INTEGRATION
       FROM
                    (  SELECT DISTINCT RAPPI_STORE_ID,
                                      MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
                      FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
                      WHERE           STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
                      GROUP BY        1
                      UNION ALL
                      SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID,
                                      MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',EXECUTION_DATE)) AS LAST_INTEGRATION
                      FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
                      WHERE           try_to_date(execution_date)::DATE >= CURRENT_DATE - 7
                      GROUP BY        1 )
      GROUP BY 1)
      
    SELECT 
    '{country}' AS COUNTRY,
    F.STORE_ID,
    F.SUSPENDED_AT,
    I.LAST_INTEGRATION,
    CASE WHEN SUSPENDED_AT < LAST_INTEGRATION THEN 'YES'
    ELSE 'NO'
    END AS REOPEN
    FROM FINAL F
    LEFT JOIN I ON I.RAPPI_STORE_ID = F.SUPER_STORE_ID
    WHERE REOPEN = 'YES'                 
    '''.format(timezone=timezone, country=country.upper())
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df


def run_alarm(df):
    if not df.empty:
        print(df)
        if not df.empty:
            text = '''
            *NONLIVE - CHECK INTEGRATIONS - CPGS*
            Tiendas que se integraron después de ser suspendidas.
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_check_integrations.csv'.format(home)
            df_final = df[["country", "store_id", "suspended_at", "last_integration", "reopen"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        text = '''
        *NONLIVE - CHECK INTEGRATIONS - CPGS*
        Sin nuevas tiendas para actuar :verified:
        '''
        slack.bot_slack(text, 'C02MJQ1Q5H6')

data = []
countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        df = products(country)
        data.append(df)
    except Exception as e:
        print(e)

df_final = pd.concat(data, ignore_index=True)
run_alarm(df_final)