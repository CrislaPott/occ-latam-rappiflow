import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones

def logs_nonlive(country):
	timezone, interval = timezones.country_timezones(country)
	if country == 'co':
		query = f'''
		with base as (SELECT 
	   'co' as Country,
		user,
	    id as retail_log_id,
		created_at::date as date,
		value[0]::text as store_id,
		value[1]::text as product_id,
	    value[2]::text as price,
	    value[3]::text as balance_price,
		value[4]::text as is_available,
	    value[5]::text as in_stock,
		value[6]::text as is_discontinued
		from INFORMATICA.CO_PG_RETAIL_CO_LOG.RETAIL_LOGS,
		table(flatten(content,'product_stores'))
		where uri = '/api/cms/gateway/cms-products/api/cms/products/product-store/upload-product-stores'
		and user in  ('jose.goyo@rappi.com', 'johan.lopez@rappi.com','eduardo.gonzalezgarc@rappi.com')
		and created_at::date = convert_timezone('America/{timezone}',current_timestamp)::date -1)     
	 	select 
	 	base.country, base.user, base.retail_log_id, base.date, base.store_id, s.name as store_name, base.product_id, base.price, base.balance_price, base.is_available, base.in_stock, base.is_discontinued
	 	from base
	 	left join co_pglr_ms_stores_public.stores_vw s on s.store_id = base.store_id 
		'''.format(timezone=timezone)
	else:
		query = f'''
	with base as (SELECT 
   '{country}' as Country,
	user,
    id as retail_log_id,
	created_at::date as date,
	value[0]::text as store_id,
	value[1]::text as product_id,
    value[2]::text as price,
    value[3]::text as balance_price,
	value[4]::text as is_available,
    value[5]::text as in_stock,
	value[6]::text as is_discontinued
	from {country}_PG_RETAIL_LOG.retail_logs,
	table(flatten(content,'product_stores'))
	where uri = '/api/cms/gateway/cms-products/api/cms/products/product-store/upload-product-stores'
	and user in  ('jose.goyo@rappi.com', 'johan.lopez@rappi.com','eduardo.gonzalezgarc@rappi.com')
	and created_at::date = convert_timezone('America/{timezone}',current_timestamp)::date -1)     
 	select 
 	base.country, base.user, base.retail_log_id, base.date, base.store_id, s.name as store_name, base.product_id, base.price, base.balance_price, base.is_available, base.in_stock, base.is_discontinued
 	from base
 	left join {country}_pglr_ms_stores_public.stores s on s.store_id = base.store_id 
	'''.format(country=country, timezone=timezone)
	
	df = snow.run_query(query)
	snow.upload_df_occ(df,'nonlive_product_action_logs')

countries = ['br','co','mx','cr','ec','uy','pe','cl','ar']
for country in countries:
	print(country)
	try:
		logs_nonlive(country)
	except Exception as e:
		print(e)