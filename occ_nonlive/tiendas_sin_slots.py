import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 
with base as (
  select 
    distinct 'BR' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    BR_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join BR_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join BR_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.BR_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        BR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'CO' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    CO_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join CO_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join CO_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.CO_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        CO_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'MX' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    MX_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join MX_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join MX_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.MX_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        MX_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'AR' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    AR_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join AR_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join AR_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.AR_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        AR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'CL' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    CL_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join CL_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join CL_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.CL_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        CL_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'PE' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    PE_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join PE_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join PE_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.PE_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        PE_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'CR' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    CR_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join CR_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join CR_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.CR_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        CR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'EC' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    EC_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join EC_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join EC_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.EC_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        EC_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
  union all 
  select 
    distinct 'UY' as country, 
    oh.physical_store_id, 
    s.store_id, 
    s.type, 
    s.name as store_name,
    kam_front, 
    kam_ops, 
    v.vertical_sub_group, 
    oh.day_of_week, 
    datediff(
      'hour', oh.CLOSE_TIME :: time, oh.open_time :: time
    ) as dif 
  from 
    UY_pg_ms_cpgops_stores_ms_public.physical_store_open_hours oh 
    left join UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES ph on ph.physical_store_id = oh.physical_store_id 
    left join UY_PGLR_MS_STORES_PUBLIC.STORE_TYPES st on st.id = ph.type 
    left join UY_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = ph.store_id 
    left join VERTICALS_LATAM.UY_VERTICALS_V2 v on v.store_type = s.type 
    left join OPS_OCC.CPGS_KAMS_VW K on k.country = country 
    and k.store_type = s.type 
  Where 
    coalesce(oh._fivetran_deleted, false)= false 
    and coalesce(ph._fivetran_deleted, false)= false 
    and coalesce(st._fivetran_deleted, false)= false 
    and coalesce(s._fivetran_deleted, false)= false 
    and s.is_enabled = true 
    and st.scheduled = true 
    and not(s.name ilike any ('%Dummy%', 'Dummie%'))
    and S.STORE_ID NOT IN (
      SELECT 
        DISTINCT STORE_ID 
      FROM 
        UY_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE 
      WHERE 
        SUSPENDED = TRUE 
        AND _FIVETRAN_DELETED = FALSE
    ) 
  group by 
    1, 
    2, 
    3, 
    4, 
    5, 
    6, 
    7, 
    8, 
    9,
    10
) 
select 
  distinct country, 
  physical_store_id, 
  store_id,
  store_name, 
  type, 
  vertical_sub_group as vertical, 
  kam_front, 
  kam_ops, 
  sum(
    abs(dif)
  ) as slots 
from 
  base 
group by 
  1, 
  2, 
  3, 
  4, 
  5, 
  6, 
  7,
  8
having 
  slots = 0
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "vertical", "physical_store_id", "store_id", "store_name", "type", "kam_front", "kam_ops"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_tiendas_sin_slots")

            text = '''
            *NONLIVE - TIENDAS PROGRAMADAS SIN SLOTS*

            Identificamos tiendas que no tienen slots configurados en ERP. Cuando la tienda es programada y no tiene slots configurados, no permite al usuario crear pedidos, así que suspendemos las tiendas por tiempo indefinido hasta que se haga el ajuste.

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_nl_sin_slots.csv'.format(home)
            df_final = df[["country", "vertical", "physical_store_id", "store_id", "store_name", "type", "kam_front", "kam_ops"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack('No hay tiendas programadas sin slots en el momento','C02MJQ1Q5H6')


df = products()

run_alarm(df)