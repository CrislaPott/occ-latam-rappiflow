import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 

with base as (

select
o.country,
date_trunc(week,o.closed_at)::date as wk,
o.order_id,
o.store_id,
o.store_name,
o.city_address_id,
o.brand_group_id,
o.vertical,
o.store_type,
cr.level_1

from Ops_global.global_orders o
left join ops_global.cancellation_reasons cr on cr.order_id = o.order_id and cr.country = o.country
where o.vertical in ('Super/Hiper','Express','Licores','Farmacia','Specialized','Pets')
and closed_at::date >= dateadd(week,-4,date_trunc(week,current_date))
and closed_at::date < date_trunc(week,current_date)
and not(o.store_name ilike any ('%Dummy%', 'Dummie%'))
and o.country != 'AR'

),

final as (
select
country,
store_id,
store_name,
store_type,
vertical,
city_address_id,
brand_group_id,
count(case when wk = dateadd(week,-1,date_trunc(week,current_date))::date then order_id end) as total_orders_w_1,
count(case when wk = dateadd(week,-1,date_trunc(week,current_date))::date and level_1 is not null then order_id end) as total_canceladas_w_1,
case when total_orders_w_1 = 0 then 0 else round(total_canceladas_w_1 / total_orders_w_1,4) end as cancel_rate_w_1 ,
case when total_orders_w_1 = 0 then 0 else round(count(case when wk = dateadd(week,-1,date_trunc(week,current_date))::date and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_1,4) end as partner_cr_w_1,

count(case when wk = dateadd(week,-2,date_trunc(week,current_date))::date then order_id end) as total_orders_w_2,
count(case when wk = dateadd(week,-2,date_trunc(week,current_date))::date and level_1 is not null then order_id end) as total_canceladas_w_2,
case when total_orders_w_2 = 0 then 0 else  round(total_canceladas_w_2 / total_orders_w_2,4) end as cancel_rate_w_2,
case when total_orders_w_2 = 0 then 0 else round(count(case when wk = dateadd(week,-2,date_trunc(week,current_date))::date and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_2,4) end as partner_cr_w_2,

count(case when wk = dateadd(week,-3,date_trunc(week,current_date))::date then order_id end) as total_orders_w_3,
count(case when wk = dateadd(week,-3,date_trunc(week,current_date))::date and level_1 is not null then order_id end) as total_canceladas_w_3,
case when total_orders_w_3 = 0 then 0 else round(total_canceladas_w_3 / total_orders_w_3,4) end as cancel_rate_w_3 ,
case when total_orders_w_3 = 0 then 0 else round(count(case when wk = dateadd(week,-3,date_trunc(week,current_date))::date and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_3,4) end as partner_cr_w_3,

count(case when wk = dateadd(week,-4,date_trunc(week,current_date))::date then order_id end) as total_orders_w_4,
count(case when wk = dateadd(week,-4,date_trunc(week,current_date))::date and level_1 is not null then order_id end) as total_canceladas_w_4,
case when total_orders_w_4 = 0 then 0 else round(total_canceladas_w_4 / total_orders_w_4,4) end as cancel_rate_w_4 ,
case when total_orders_w_4 = 0 then 0 else round(count(case when wk = dateadd(week,-4,date_trunc(week,current_date))::date and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_4,4) end as partner_cr_w_4,

case when (total_canceladas_w_1 >= 5 or total_orders_w_1 >= 10) and cancel_rate_w_1 > 0.1 and  partner_cr_w_1 >= 0.05 then 1 else 0 end as triggered_w_1,

case when (total_canceladas_w_2 >= 5 or total_orders_w_2 >= 10) and cancel_rate_w_2 > 0.1 and  partner_cr_w_2 >= 0.05 then 1 else 0 end as triggered_w_2,

case when (total_canceladas_w_3 >= 5 or total_orders_w_3 >= 10) and cancel_rate_w_3 > 0.1 and  partner_cr_w_3 >= 0.05 then 1 else 0 end as triggered_w_3,

case when (total_canceladas_w_4 >= 5 or total_orders_w_4 >= 10) and cancel_rate_w_4 > 0.1 and  partner_cr_w_4 >= 0.05 then 1 else 0 end as triggered_w_4,

case when triggered_w_1 = 1 and triggered_w_2 = 0 then '1. Alerta OCC'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 0 then '2. Apagado 72 hrs'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 0  then '3. Apagado 96 hrs'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 1  then '4. Apagado 7 dias'
end as status

from base


group by 1,2,3,4,5,6,7
having status is not null
order by country asc, status asc)

SELECT
F.COUNTRY,
F.STORE_ID,
F.STORE_NAME,
F.STORE_TYPE,
F.VERTICAL,
F.BRAND_GROUP_ID,
F.CITY_ADDRESS_ID,
KAM_FRONT,
KAM_OPS,
T.CATEGORY AS TIER,
F.CANCEL_RATE_W_1,
F.PARTNER_CR_W_1,
F.CANCEL_RATE_W_2,
F.PARTNER_CR_W_2,
F.CANCEL_RATE_W_3,
F.PARTNER_CR_W_3,
F.CANCEL_RATE_W_4,
F.PARTNER_CR_W_4,
STATUS
FROM FINAL F
LEFT JOIN OPS_OCC.CPGS_KAMS_VW K ON K.COUNTRY = F.COUNTRY AND K.STORE_TYPE = F.STORE_TYPE
LEFT JOIN ops_occ.group_brand_category T ON T.COUNTRY = F.COUNTRY AND T.VERTICAL = F.VERTICAL AND T.BRAND_GROUP_ID = F.BRAND_GROUP_ID and t.city_address_id=f.city_address_id
where f.store_name not ilike '%petz%'
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "store_id", "store_name", "store_type", "vertical", "kam_front", "kam_ops", "kam_ops", "cancel_rate_w_1", "partner_cr_w_1", "cancel_rate_w_2", "partner_cr_w_2", "cancel_rate_w_3", "partner_cr_w_3","cancel_rate_w_4", "partner_cr_w_4", "status"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_worst_offenders")

            text = '''

            * NONLIVE - ALARMA WORST OFFENDERS *

            :no_entry: Se consideran las tiendas que tuvieron >=10 ordenes creadas o >=5 ordenes canceladas

            :warning: Estructura del proceso:

            Tiendas Worst Offender durante 1 semana: Identificamos las tiendas ofensoras de acuerdo con los criterios anteriores, pero no suspendemos ninguna tienda. Este reporte es solo para que comprueben y corrijan el problema
            Tiendas Worst Offenders durante 2, 3 y 4 Semanas: Con base en el desempeño de las tiendas, que permanezcan ofensoras en los criterios anteriores accionamos las siguientes suspensiones:
            2 semanas → Suspensión durante 72 horas
            3 semanas → Suspensión durante 96 horas
            4 semanas → Suspensión durante 7 días
             
            :alert: Las tiendas que han sido suspendidas por este proceso no serán encendidas hasta que termine el plazo de suspensión :alert:
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_nl_worst_offenders.csv'.format(home)
            df_final = df[["country", "store_id", "store_name", "store_type", "vertical", "tier", "kam_front", "kam_ops", "kam_ops", "cancel_rate_w_1", "partner_cr_w_1", "cancel_rate_w_2", "partner_cr_w_2", "cancel_rate_w_3", "partner_cr_w_3","cancel_rate_w_4", "partner_cr_w_4", "status"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack('* NONLIVE - ALARMA WORST OFFENDERS * :alert: :alert: :alert: NO HAY TIENDAS WORST OFFENDERS EN EL MOMENTO :alert: :alert: :alert:','C02MJQ1Q5H6')


df = products()

run_alarm(df)