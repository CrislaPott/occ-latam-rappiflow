import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from functions import timezones, snippets


def products(country):
  timezone, interval = timezones.country_timezones(country)
  query_so = ''' 
 --no_cache

  WITH I AS

  (

  SELECT RAPPI_STORE_ID, MAX(LAST_INTEGRATION) AS LAST_INTEGRATION
  FROM

  (  SELECT DISTINCT RAPPI_STORE_ID,
  MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
  FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
  WHERE           STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
  GROUP BY        1
  UNION ALL
  SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID,
  MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',EXECUTION_DATE)) AS LAST_INTEGRATION
  FROM            CPGS_DATASCIENCE.{country}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
  WHERE           try_to_date(execution_date)::DATE >= CURRENT_DATE - 7
  GROUP BY        1 )
  GROUP BY 1),


  BRANDS AS 

  (
  SELECT
  distinct i.country,
  s.store_id,
  i.integration_name
  FROM
  (SELECT * FROM (
  SELECT DISTINCT RAPPI_STORE_ID, DELETED_AT, _FIVETRAN_DELETED, DATASOURCE_ID,
  DENSE_RANK() OVER (PARTITION BY RAPPI_STORE_ID ORDER BY ID DESC) AS RANK
  FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE)
  WHERE RANK = 1) nds
  JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE d ON nds.datasource_id = d.id
  JOIN (SELECT *, DENSE_RANK() OVER (PARTITION BY DATASOURCE ORDER BY ID DESC) AS RANK
  FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS WHERE INTEGRATION_NAME not ilike '%bundle%')
  i ON    i.datasource = d.name and i.rank = 1
  JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS_METADATA im ON i.id = im.id_integration 
  LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATION_VERSIONS iv ON iv.id_integration = i.id
  JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s ON s.store_id = nds.rappi_store_id
  LEFT JOIN (SELECT ID, ID_INTEGRATION, ID_TAG, DENSE_RANK() OVER (PARTITION BY ID_INTEGRATION ORDER BY ID DESC) AS RANK 
  FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags) it on it.id_integration = i.id and it.rank = 1
  LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
  WHERE
  coalesce(nds._fivetran_deleted,false) = false
  AND coalesce(d._fivetran_deleted,false) = false
  AND coalesce(s._fivetran_deleted,false) = false 
  AND nds.deleted_at is null AND d.deleted_at is null AND s.deleted_at is null
  AND upper(d.country) = '{country}'
  AND upper(i.country) = '{country}'
  AND ((i.run_mode = 'worker') or (i.run_mode = 'astronomer'and iv.is_enabled = 'true'))
  AND t.name not in ('Deactivated', 'In guarantee')),


  WHITELIST AS 

  (

  SELECT DISTINCT 
  UPPER(COUNTRY) AS COUNTRY,
  STORE_ID,
  STORE_TYPE,
  BRAND_GROUP_ID
  from google_sheets.integrations_wl_cpgs
  WHERE COUNTRY = '{country}'),


  ORDERS AS

  (
  SELECT     S.STORE_ID,
  Count(DISTINCT O.ID) AS TOTAL
  FROM       {country}_CORE_ORDERS_PUBLIC.ORDERS_VW O
  INNER JOIN
  (
  SELECT DISTINCT ORDER_ID,
  NAME,
  STORE_ID,
  TYPE
  FROM            {country}_CORE_ORDERS_PUBLIC.ORDER_STORES_VW
  WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
  ON         O.ID = OS.ORDER_ID
  LEFT JOIN  {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
  ON         S.STORE_ID = OS.STORE_ID
  LEFT JOIN  {country}_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS_VW OM
  ON         OM.ORDER_ID = O.ID
  WHERE      O.STATE NOT IN ('canceled_for_payment_error',
  'canceled_by_fraud',
  'canceled_by_split_error',
  'canceled_by_timeout')
  AND        OS.TYPE NOT IN ('grin',
  'rappi_prime',
  'rentbrella',
  'go_taxi')
  AND        O.PAYMENT_METHOD != 'synthetic'
  AND        S.CITY_ADDRESS_ID != 104
  AND        OM.TYPE NOT IN ('canceled_by_early_regret')
  AND        COALESCE(O.CLOSED_AT, O.UPDATED_AT):: DATE >= CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE -30
  GROUP BY   1
  ORDER BY   2 DESC), 


  STORES AS

  (
  SELECT DISTINCT S.STORE_ID,
  S.NAME,
  S.TYPE,
  S.SUPER_STORE_ID,
  S.CREATED_AT,
  V.VERTICAL,
  B.NAME AS BRAND_NAME,
  ST.BRAND_GROUP_NAME,
  ST.BRAND_GROUP_ID,
  I.integration_name
  FROM            {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
  LEFT JOIN
  (
  SELECT DISTINCT
    CASE
                    WHEN UPPER (VERTICAL_GROUP) IN ('ECOMMERCE',
                                                    'RAPPIFAVOR',
                                                    'RESTAURANTS',
                                                    'WHIM') THEN UPPER (VERTICAL_GROUP)
                    WHEN UPPER (VERTICAL_SUB_GROUP) IN ('PETS') THEN 'PETS'                                
                    WHEN UPPER (VERTICAL_SUB_GROUP) IN                                                                                                                           ('BOOKSTORES_STATIONARY',
                                                        'FASHION',
                                                        'HOME',
                                                       'SPORTS_OUTDOORS',
                                                        'GIFTS',
                                                        'TICKETS',
                                                        'HEALTH_BEAUTY',
                                                        'RAPPISMILE',
                                                        'INFORMATIVE',
                                                        'MEGA',
                                                        'TECH',
                                                        'EVENTS',
                                                        'BABIES_KIDS',
                                                        'SERVICES',
                                                        'BETS',
                                                        'GAMING') THEN 'ECOMMERCE'
                    WHEN UPPER (VERTICAL_SUB_GROUP) = 'PHARMACY' THEN 'FARMACIA'
                    WHEN UPPER (VERTICAL_SUB_GROUP) = 'EXPRESS' THEN 'EXPRESS'
                    WHEN UPPER (VERTICAL_SUB_GROUP) = 'LIQUOR' THEN 'LICORES'
                    WHEN UPPER (VERTICAL_SUB_GROUP) = ('SUPER') THEN 'MERCADOS'
                    WHEN UPPER (VERTICAL_SUB_GROUP) IN ('SPECIALIZED',
                                                        'RESTAURANTS',
                                                        'RAPPICASH',
                                                        'ANTOJOS',
                                                        'RAPPIFAVOR') THEN UPPER (VERTICAL_SUB_GROUP)
                    ELSE 'OTHER'
    END AS VERTICAL,
    STORE_TYPE
  FROM            VERTICALS_LATAM.{country}_VERTICALS_V2

  ) V
  ON              V.STORE_TYPE = S.TYPE
  LEFT JOIN       {country}_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
  ON              B.ID = S.STORE_BRAND_ID
  LEFT JOIN
  (
  SELECT   *
  FROM     (
  SELECT   STORE_BRAND_ID,
        STORE_TYPE,
        LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
        LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
  FROM     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
  JOIN     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
  ON       BRAND_GROUP_ID = BG.ID)
  GROUP BY 1,
  2,
  3,
  4) ST
  ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
  LEFT JOIN       BRANDS I
  ON              I.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
  WHERE           S.DELETED_AT IS NULL
  AND             COALESCE(S.SUPER_STORE_ID, S.STORE_ID) IN (SELECT STORE_ID FROM BRANDS)
  AND             S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM {country}_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE WHERE SUSPENDED = TRUE AND _FIVETRAN_DELETED = FALSE)
  AND             S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM WHITELIST WHERE STORE_TYPE IS NOT NULL)
  AND             S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM WHITELIST WHERE STORE_ID IS NOT NULL)
  AND             S.STORE_ID NOT IN (SELECT DISTINCT SPINOFF_ID FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF)
  AND             ST.BRAND_GROUP_ID NOT IN (SELECT DISTINCT BRAND_GROUP_ID FROM WHITELIST WHERE BRAND_GROUP_ID IS NOT NULL)
  AND             S._FIVETRAN_DELETED = 'FALSE'
  AND             S.CITY_ADDRESS_ID != 104
  AND             V.VERTICAL IN ('EXPRESS',
  'LICORES',
  'MERCADOS',
  'HIPER',
  'FARMACIA',
  'SPECIALIZED', 'PETS') 
  AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = '{country}')),

  CANCEL_RATE AS

  (
  SELECT     GO.STORE_ID,
           COUNT(DISTINCT(
           CASE
                      WHEN O.STATE ILIKE '%cancel%' THEN O.ID
                      ELSE NULL
           END)) / COUNT(DISTINCT O.ID) AS CANCEL_RATE,
           COUNT(DISTINCT(
           CASE
                      WHEN CR.LEVEL_1 ILIKE 'partner_related_errors' THEN O.ID
                      ELSE NULL
           END)) / COUNT(DISTINCT O.ID) AS PARTNER_RELATED,
           COUNT(DISTINCT O.ID) AS ORDERS_TOTAL
    FROM       {country}_CORE_ORDERS_PUBLIC.ORDERS_VW O
    INNER JOIN OPS_GLOBAL.GLOBAL_ORDERS GO
    ON         GO.ORDER_ID = O.ID
    AND        GO.COUNTRY = '{country}'
    LEFT JOIN  OPS_GLOBAL.CANCELLATION_REASONS CR
    ON         CR.ORDER_ID = O.ID
    AND        CR.COUNTRY = '{country}'
    WHERE      O.CREATED_AT::DATE >= CURRENT_DATE -1
    AND        O.STATE NOT IN ('canceled_for_payment_error',
                           'canceled_by_fraud',
                           'canceled_by_split_error',
                           'ongoing',
                           'on_the_route',
                           'in_store',
                           'in_progress',
                           'scheduled',
                           'created',
                           'arrive',
                           'canceled_by_timeout')
    GROUP BY   1),

  STORES_FINAL AS (


  SELECT DISTINCT '{country}' AS COUNTRY,
  S.STORE_ID,
  S.NAME,
  S.SUPER_STORE_ID,
  S.TYPE,
  S.VERTICAL,
  S.BRAND_GROUP_NAME,
  S.BRAND_GROUP_ID,
  S.integration_name,
  COALESCE(C.CANCEL_RATE,0) AS CANCEL_RATE,
  COALESCE(C.ORDERS_TOTAL,0) AS ORDERS_TOTAL,
  COALESCE(C.PARTNER_RELATED,0) AS PARTNER_RELATED,
  I.LAST_INTEGRATION, 
  DATEDIFF(HOUR, I.LAST_INTEGRATION::TIMESTAMP_NTZ,    
  CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::TIMESTAMP_NTZ) AS DIF,
  COALESCE(ORDERS.TOTAL,0) AS ORDERSLAST30D,
  S.CREATED_AT,
  DATEDIFF(DAY, S.CREATED_AT::DATE, CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE) AS STORE_CREATION,
  KAM_FRONT,
  KAM_OPS,
  CASE WHEN COUNTRY = 'MX' AND S.BRAND_GROUP_ID = 3 AND (DIF >= 48 OR DIF IS NULL) THEN 'YES'
  ELSE 'NO' END AS FIRST,
  CASE WHEN (DIF >= 24 OR DIF IS NULL) AND S.BRAND_GROUP_NAME NOT ILIKE '%7 eleven%' THEN 'YES'
  ELSE NULL END AS SECOND
  FROM            STORES S
  LEFT JOIN       I
  ON              I.RAPPI_STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
  LEFT JOIN       ORDERS
  ON              ORDERS.STORE_ID = S.STORE_ID
  LEFT JOIN CANCEL_RATE C
  ON        C.STORE_ID = S.STORE_ID
  LEFT JOIN  OPS_OCC.CPGS_KAMS_VW K
  ON         K.COUNTRY = '{country}'
  AND        K.STORE_TYPE = S.TYPE
  WHERE (ORDERSLAST30D >= 1 OR STORE_CREATION <= 20)
  AND (FIRST = 'YES' OR SECOND = 'YES'))     

  SELECT    '{country}' AS COUNTRY,
  S.STORE_ID,
  S.NAME,
  COALESCE(S.LAST_INTEGRATION::text, 'No integration in the last 7 days') AS LAST_INTEGRATION,
  S.INTEGRATION_NAME,
  S.BRAND_GROUP_NAME,
  S.TYPE,
  COALESCE(S.SUPER_STORE_ID, S.STORE_ID) AS TIENDA_QUE_INTEGRA,
  S.VERTICAL,
  S.ORDERS_TOTAL,
  S.CANCEL_RATE,
  S.PARTNER_RELATED,
  CASE WHEN TIER IS NULL THEN 'C'
  ELSE TIER END AS TIER_GMV,
  CASE WHEN PARTNER_RELATED > 0.07 AND TIER_GMV IN ('C','D') THEN '48 HOURS'
  WHEN PARTNER_RELATED > 0.07 AND TIER_GMV IN ('A','B') THEN '24 HOURS'
  WHEN PARTNER_RELATED >= 0.05 AND PARTNER_RELATED <= 0.07 AND TIER_GMV IN ('C','D') THEN '24 HOURS' 
  WHEN PARTNER_RELATED >= 0.05 AND PARTNER_RELATED <= 0.07 AND TIER_GMV IN ('A','B') THEN '12 HOURS'
  WHEN PARTNER_RELATED < 0.05 THEN 'NO_ACTION'
  WHEN ORDERS_TOTAL = 0 THEN 'NO_ACTION' END AS ACTION,
  KAM_FRONT,
  KAM_OPS
  FROM      STORES_FINAL S
  LEFT JOIN
  (
  SELECT si.store_id as store_id, c.category as tier
  from ops_occ.store_infos_latam si
  left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=si.store_id
  left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=si.brand_group_id and s.city_address_id=c.city_address_id and c.COUNTRY='{country}'
  and c.vertical=(case when si.vertical in ('Mercados') then 'Super/Hiper' else si.vertical end)
  where c.country='{country}') T
  ON        T.STORE_ID = S.STORE_ID
  WHERE ACTION != 'NO_ACTION'
  ORDER BY ORDERS_TOTAL DESC
  '''.format(timezone=timezone, country=country.upper())

  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:
            current_time = timezones.country_current_time(country)
            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "store_id", "name", "last_integration", "integration_name", "brand_group_name", "type", "tienda_que_integra", "vertical", "orders_total", "cancel_rate", "partner_related", "tier_gmv", "action", "kam_front", "kam_ops"]]
            print("uploading df")
            snow.upload_df_occ(to_upload, "cpgs_integrations_report")
            text = '''
            *NONLIVE - Alarma Integrations - REPORT CPGS*
            Nuevas tiendas no se han integrado en las últimas 24 horas y deben ser reportadas.
            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_report_integrations_cpgs.csv'.format(home)
            df_final = df[["country", "store_id", "name", "last_integration", "integration_name", "brand_group_name", "type", "tienda_que_integra", "vertical", "orders_total", "cancel_rate", "partner_related", "tier_gmv", "action", "kam_front", "kam_ops"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        text = '''
        *NONLIVE - Alarma Integrations - REPORT CPGS*
        Sin nuevas tiendas para reportar :verified:
        '''
        slack.bot_slack(text,'C02MJQ1Q5H6')

data = []
countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        df = products(country)
        data.append(df)
    except Exception as e:
        print(e)

df_final = pd.concat(data, ignore_index=True)
run_alarm(df_final)