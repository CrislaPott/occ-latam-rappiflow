import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

    query_so = ''' 
with base as (
  SELECT 
    COUNTRY, 
    PHYSICAL_STORE_ID, 
    STORE_NAME,
    PICKED_BY, 
    RELEASE_TO_RT_FALLBACK_PICKED_BY as FALLBACK 
  FROM 
    CPGS_OPS_SHOPPER_GLOBAL.STORE_CONFIGS_v2 
  WHERE 
    PICKED_BY = 'shopper_or_rt' 
    AND brand_name not ilike '%prueba%' 
    AND not(store_name ilike any ('%prueba%', '%Dummy%', 'Dummie%')) 
    AND RELEASE_TO_RT_FALLBACK_PICKED_BY = 'false' 
    AND picked_by not in ('integrations'))
    
select 
  s.country, 
  s.physical_store_id,
  b.store_name,
  s.store_id, 
  s.type, 
  v.vertical_sub_group as vertical, 
  b.picked_by, 
  b.fallback, 
  kam_front, 
  kam_ops 
from 
  (
    select 
      'BR' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      br_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'CO' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      co_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'MX' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      mx_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'CL' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      cl_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'AR' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      ar_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'PE' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      pe_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'CR' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      cr_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'EC' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      ec_pg_ms_cpgops_stores_ms_public.stores 
    union all 
    select 
      'UY' as country, 
      store_id, 
      physical_store_id, 
      type 
    from 
      uy_pg_ms_cpgops_stores_ms_public.stores
  ) s 
  join base b on b.physical_store_id = s.physical_store_id 
  and b.country = s.country 
  left join OPS_OCC.CPGS_KAMS_VW K on k.country = s.country 
  and k.store_type = s.type 
  left join (
    select 
      distinct 'BR' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.BR_VERTICALS_V2 
    union all 
    select 
      distinct 'CO' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.CO_VERTICALS_V2 
    union all 
    select 
      distinct 'MX' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.MX_VERTICALS_V2 
    union all 
    select 
      distinct 'AR' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.AR_VERTICALS_V2 
    union all 
    select 
      distinct 'CL' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.CL_VERTICALS_V2 
    union all 
    select 
      distinct 'PE' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.PE_VERTICALS_V2 
    union all 
    select 
      distinct 'CR' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.CR_VERTICALS_V2 
    union all 
    select 
      distinct 'EC' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.EC_VERTICALS_V2 
    union all 
    select 
      distinct 'UY' as country, 
      store_type, 
      vertical_sub_group 
    from 
      VERTICALS_LATAM.UY_VERTICALS_V2
  ) v on v.country = s.country 
  and v.store_type = s.type
  
'''
    print('rodando snowflake')
    df = snow.run_query(query_so)
    return df

def run_alarm(df):

    if not df.empty:
        print(df)

        if not df.empty:

            df['checked_at'] = current_time
            to_upload = df
            to_upload = to_upload[
                ["country", "checked_at", "vertical", "physical_store_id", "store_name", "store_id", "type", "kam_front", "kam_ops"]]

            print("uploading df")
            snow.upload_df_occ(to_upload, "nl_configuracion_fallback")

            text = '''
            *NONLIVE - CONFIGURACIÓN DE FALLBACK RT*

            Identificamos tiendas que están configuradas en ERP con picked by = Shopper or RT, pero tenían fallback para RT desactivado. Realizamos la alteración de estas tiendas activando el fallback.

            '''
            print(text)
            home = expanduser("~")
            results_file = '{}/details_nl_fallback.csv'.format(home)
            df_final = df[["country", "vertical", "physical_store_id", "store_name", "store_id", "type", "kam_front", "kam_ops"]]
            df_final.to_csv(results_file, index=False)
            slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

        else:
            print('df null')
    else:
        print('products null')
        slack.bot_slack('*No hay tiendas que necesitan ajuste de fallback en el momento*','C02MJQ1Q5H6')


df = products()

run_alarm(df)