import pandas as pd
import os
from lib import slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def products():

  query_so = """
--no_cache
with basebr as
(
  select
           'BR' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from BR_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join BR_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join BR_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join BR_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join BR_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join BR_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from BR_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'BR')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM BR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'BR')

  
),
final_polygons_br as
(
select    distinct
          basebr.store_id,
          basebr.store_name,
          basebr.vertical,
          basebr.coverage,
          pc.distance = coalesce(basebr.coverage/1000,0)  as distance_ok
  from basebr
  left join gsheets.polygons_standards pc on basebr.country = pc.country
                                                and basebr.polygon_size = pc.polygon_size
                                                and basebr.vertical = pc.vertical_sub_group
                                                and basebr.city_code = pc.city_id
),
para_resumir_br as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_br fp
  where 1 = 1
  group by 1,2,3
),

FINAL_BR AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_br.vertical,
        basebr.brand_id,
        basebr.brand_name,
        basebr.store_id,
        basebr.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_br left join basebr on para_resumir_br.vertical = basebr.vertical
                                                 and para_resumir_br.store_id = basebr.store_id
                                                 and para_resumir_br.store_name = basebr.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_br.vertical is not null 
and basebr.brand_id is not null 
and basebr.brand_name is not null 
and basebr.store_id is not null 
and basebr.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_br.vertical,
        basebr.brand_id,
        basebr.brand_name,
        basebr.store_id,
        basebr.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_br left join basebr on para_resumir_br.vertical = basebr.vertical
                                                 and para_resumir_br.store_id = basebr.store_id
                                                 and para_resumir_br.store_name = basebr.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_br.vertical is not null 
and basebr.brand_id is not null 
and basebr.brand_name is not null 
and basebr.store_id is not null 
and basebr.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)),



ULT_BR AS (

SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_BR F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),


basemx as
(
  select
           'MX' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from MX_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join MX_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join MX_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join MX_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join MX_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join MX_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from MX_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'MX')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM MX_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'MX')
  
),
final_polygons_mx as
(
select    distinct
          basemx.store_id,
          basemx.store_name,
          basemx.vertical,
          basemx.coverage,
          pc.distance = coalesce(basemx.coverage/1000,0)  as distance_ok
  from basemx
  left join gsheets.polygons_standards pc on basemx.country = pc.country
                                                and basemx.polygon_size = pc.polygon_size
                                                and basemx.vertical = pc.vertical_sub_group
                                                and basemx.city_code = pc.city_id
),
para_resumir_mx as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_mx fp
  where 1 = 1
  group by 1,2,3
),
FINAL_MX AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_mx.vertical,
        basemx.brand_id,
        basemx.brand_name,
        basemx.store_id,
        basemx.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_mx left join basemx on para_resumir_mx.vertical = basemx.vertical
                                                 and para_resumir_mx.store_id = basemx.store_id
                                                 and para_resumir_mx.store_name = basemx.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_mx.vertical is not null 
and basemx.brand_id is not null 
and basemx.brand_name is not null 
and basemx.store_id is not null 
and basemx.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_mx.vertical,
        basemx.brand_id,
        basemx.brand_name,
        basemx.store_id,
        basemx.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_mx left join basemx on para_resumir_mx.vertical = basemx.vertical
                                                 and para_resumir_mx.store_id = basemx.store_id
                                                 and para_resumir_mx.store_name = basemx.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_mx.vertical is not null 
and basemx.brand_id is not null 
and basemx.brand_name is not null 
and basemx.store_id is not null 
and basemx.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

),

ULT_MX AS (
SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_MX F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

basecl as
(
  select
           'CL' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from CL_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join CL_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join CL_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join CL_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join CL_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join CL_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from CL_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CL')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM CL_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'CL')
  
),
final_polygons_cl as
(
select    distinct
          basecl.store_id,
          basecl.store_name,
          basecl.vertical,
          basecl.coverage,
          pc.distance = coalesce(basecl.coverage/1000,0)  as distance_ok
  from basecl
  left join gsheets.polygons_standards pc on basecl.country = pc.country
                                                and basecl.polygon_size = pc.polygon_size
                                                and basecl.vertical = pc.vertical_sub_group
                                                and basecl.city_code = pc.city_id
),
para_resumir_cl as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_cl fp
  where 1 = 1
  group by 1,2,3
),

FINAL_CL AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_cl.vertical,
        basecl.brand_id,
        basecl.brand_name,
        basecl.store_id,
        basecl.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_cl left join basecl on para_resumir_cl.vertical = basecl.vertical
                                                 and para_resumir_cl.store_id = basecl.store_id
                                                 and para_resumir_cl.store_name = basecl.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_cl.vertical is not null 
and basecl.brand_id is not null 
and basecl.brand_name is not null 
and basecl.store_id is not null 
and basecl.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_cl.vertical,
        basecl.brand_id,
        basecl.brand_name,
        basecl.store_id,
        basecl.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_cl left join basecl on para_resumir_cl.vertical = basecl.vertical
                                                 and para_resumir_cl.store_id = basecl.store_id
                                                 and para_resumir_cl.store_name = basecl.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_cl.vertical is not null 
and basecl.brand_id is not null 
and basecl.brand_name is not null 
and basecl.store_id is not null 
and basecl.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

),


ULT_CL AS (
SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"

FROM FINAL_MX F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

basear as
(
  select
           'AR' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from AR_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join AR_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join AR_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join AR_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join AR_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join AR_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id  
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from AR_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'AR')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM AR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'AR')
  
),
final_polygons_ar as
(
select    distinct
          basear.store_id,
          basear.store_name,
          basear.vertical,
          basear.coverage,
          pc.distance = coalesce(basear.coverage/1000,0)  as distance_ok
  from basear
  left join gsheets.polygons_standards pc on basear.country = pc.country
                                                and basear.polygon_size = pc.polygon_size
                                                and basear.vertical = pc.vertical_sub_group
                                                and basear.city_code = pc.city_id
),
para_resumir_ar as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_ar fp
  where 1 = 1
  group by 1,2,3
),
FINAL_AR AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_ar.vertical,
        basear.brand_id,
        basear.brand_name,
        basear.store_id,
        basear.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_ar left join basear on para_resumir_ar.vertical = basear.vertical
                                                 and para_resumir_ar.store_id = basear.store_id
                                                 and para_resumir_ar.store_name = basear.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_ar.vertical is not null 
and basear.brand_id is not null 
and basear.brand_name is not null 
and basear.store_id is not null 
and basear.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_ar.vertical,
        basear.brand_id,
        basear.brand_name,
        basear.store_id,
        basear.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_ar left join basear on para_resumir_ar.vertical = basear.vertical
                                                 and para_resumir_ar.store_id = basear.store_id
                                                 and para_resumir_ar.store_name = basear.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_ar.vertical is not null 
and basear.brand_id is not null 
and basear.brand_name is not null 
and basear.store_id is not null 
and basear.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

),


ULT_AR AS (

SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_AR F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

basepe as
(
  select
           'PE' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from PE_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join PE_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join PE_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join PE_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join PE_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join PE_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from PE_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'PE')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM PE_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'PE')
),
final_polygons_pe as
(
select    distinct
          basepe.store_id,
          basepe.store_name,
          basepe.vertical,
          basepe.coverage,
          pc.distance = coalesce(basepe.coverage/1000,0)  as distance_ok
  from basepe
  left join gsheets.polygons_standards pc on basepe.country = pc.country
                                                and basepe.polygon_size = pc.polygon_size
                                                and basepe.vertical = pc.vertical_sub_group
                                                and basepe.city_code = pc.city_id
),
para_resumir_pe as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_pe fp
  where 1 = 1
  group by 1,2,3
),

FINAL_PE AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_pe.vertical,
        basepe.brand_id,
        basepe.brand_name,
        basepe.store_id,
        basepe.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_pe left join basepe on para_resumir_pe.vertical = basepe.vertical
                                                 and para_resumir_pe.store_id = basepe.store_id
                                                 and para_resumir_pe.store_name = basepe.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_pe.vertical is not null 
and basepe.brand_id is not null 
and basepe.brand_name is not null 
and basepe.store_id is not null 
and basepe.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_pe.vertical,
        basepe.brand_id,
        basepe.brand_name,
        basepe.store_id,
        basepe.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_pe left join basepe on para_resumir_pe.vertical = basepe.vertical
                                                 and para_resumir_pe.store_id = basepe.store_id
                                                 and para_resumir_pe.store_name = basepe.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_pe.vertical is not null 
and basepe.brand_id is not null 
and basepe.brand_name is not null 
and basepe.store_id is not null 
and basepe.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)),

ULT_PE AS (
SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_PE F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

basecr as
(
  select
           'CR' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from CR_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join CR_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join CR_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join CR_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join CR_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join CR_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id  
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from CR_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'CR')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM CR_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'CR')
),
final_polygons_cr as
(
select    distinct
          basecr.store_id,
          basecr.store_name,
          basecr.vertical,
          basecr.coverage,
          pc.distance = coalesce(basecr.coverage/1000,0)  as distance_ok
  from basecr
  left join gsheets.polygons_standards pc on basecr.country = pc.country
                                                and basecr.polygon_size = pc.polygon_size
                                                and basecr.vertical = pc.vertical_sub_group
                                                and basecr.city_code = pc.city_id
),
para_resumir_cr as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_cr fp
  where 1 = 1
  group by 1,2,3
),

FINAL_CR AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_CR.vertical,
        basecr.brand_id,
        basecr.brand_name,
        basecr.store_id,
        basecr.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_cr left join basecr on para_resumir_cr.vertical = basecr.vertical
                                                 and para_resumir_cr.store_id = basecr.store_id
                                                 and para_resumir_cr.store_name = basecr.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_CR.vertical is not null 
and basecr.brand_id is not null 
and basecr.brand_name is not null 
and basecr.store_id is not null 
and basecr.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_CR.vertical,
        basecr.brand_id,
        basecr.brand_name,
        basecr.store_id,
        basecr.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_cr left join basecr on para_resumir_cr.vertical = basecr.vertical
                                                 and para_resumir_cr.store_id = basecr.store_id
                                                 and para_resumir_cr.store_name = basecr.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_cr.vertical is not null 
and basecr.brand_id is not null 
and basecr.brand_name is not null 
and basecr.store_id is not null 
and basecr.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)),


ULT_CR AS (

SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_CR F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

baseec as
(
  select
           'EC' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from EC_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join EC_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join EC_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join EC_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join EC_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join EC_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id  
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from EC_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'EC')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM EC_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'EC')
),
final_polygons_ec as
(
select    distinct
          baseec.store_id,
          baseec.store_name,
          baseec.vertical,
          baseec.coverage,
          pc.distance = coalesce(baseec.coverage/1000,0)  as distance_ok
  from baseec
  left join gsheets.polygons_standards pc on baseec.country = pc.country
                                                and baseec.polygon_size = pc.polygon_size
                                                and baseec.vertical = pc.vertical_sub_group
                                                and baseec.city_code = pc.city_id
),
para_resumir_ec as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_ec fp
  where 1 = 1
  group by 1,2,3
),
FINAL_EC AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_ec.vertical,
        baseec.brand_id,
        baseec.brand_name,
        baseec.store_id,
        baseec.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_ec left join baseec on para_resumir_ec.vertical = baseec.vertical
                                                 and para_resumir_ec.store_id = baseec.store_id
                                                 and para_resumir_ec.store_name = baseec.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_ec.vertical is not null 
and baseec.brand_id is not null 
and baseec.brand_name is not null 
and baseec.store_id is not null 
and baseec.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_ec.vertical,
        baseec.brand_id,
        baseec.brand_name,
        baseec.store_id,
        baseec.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_ec left join baseec on para_resumir_ec.vertical = baseec.vertical
                                                 and para_resumir_ec.store_id = baseec.store_id
                                                 and para_resumir_ec.store_name = baseec.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_ec.vertical is not null 
and baseec.brand_id is not null 
and baseec.brand_name is not null 
and baseec.store_id is not null 
and baseec.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

),


ULT_EC AS (

SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
FROM FINAL_EC F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria')),

baseuy as
(
  select
           'UY' as country,
           s.store_id,
           ca.city as city,
           s.city_address_id as city_code,
           s.name as store_name,
           s.store_brand_id as brand_id,
           sb.name as brand_name,
           sub_group as vertical,
           sz.size as polygon_size,
           s.is_enabled,
           s.is_marketplace,
           sz.distance as coverage,
           sz.size as polygons,
           sz.parameters:modified_by as polygon_type
  from UY_PGLR_MS_STORES_PUBLIC.stores_vw s
  inner join UY_grability_public.store_zones_vw sz on sz.store_id = s.store_id
  left join UY_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses_vw ca on ca.id = s.city_address_id
  left join UY_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id = s.store_brand_id
  left join UY_pglr_status_partner_groot_public.partner_store ps on ps.store_id = s.store_id
  left join UY_pglr_status_partner_groot_public.partners p on ps.partner_id = p.id  
  left join
  (
    select distinct store_type,last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from UY_PGLR_MS_STORES_PUBLIC.verticals where coalesce(_fivetran_deleted,false) = false and store_type != 'tech_exito'
  ) v on v.store_type = s.type
  where coalesce(sz._fivetran_deleted,false) = false
       and s.deleted_at is null
       and s.type not like '%prueba%'
       and s.is_marketplace = false
       and s.type != 'tech_exito'
       and s.is_published = true
       AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = 'UY')
       and p.type <> 'callcenter_partner'
       and not(s.name ilike any ('%Dummy%', 'Dummie%'))
       AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID
           FROM UY_PGLR_MS_STORES_PUBLIC.STORE_ATTRIBUTE
           WHERE SUSPENDED = TRUE
           AND _FIVETRAN_DELETED = FALSE)
           AND S.STORE_ID NOT IN (SELECT DISTINCT STORE_ID FROM google_sheets.nl_polygons_whitelist WHERE upper(PAIS) = 'UY')
),
final_polygons_uy as
(
select    distinct
          baseuy.store_id,
          baseuy.store_name,
          baseuy.vertical,
          baseuy.coverage,
          pc.distance = coalesce(baseuy.coverage/1000,0)  as distance_ok
  from baseuy
  left join gsheets.polygons_standards pc on baseuy.country = pc.country
                                                and baseuy.polygon_size = pc.polygon_size
                                                and baseuy.vertical = pc.vertical_sub_group
                                                and baseuy.city_code = pc.city_id
),
para_resumir_uy as
(
select  distinct
        fp.store_id,
        fp.store_name,
        fp.vertical,
        count(1) as total_poligonos,
        count(case when not distance_ok then 1  end) as poligonos_mal_distance,
        count(case when coverage is null then 1 end) as poligonos_distance_null
  from final_polygons_uy fp
  where 1 = 1
  group by 1,2,3
),
FINAL_UY AS(
(select  distinct
        country,
        city,
        city_code,
        para_resumir_uy.vertical,
        baseuy.brand_id,
        baseuy.brand_name,
        baseuy.store_id,
        baseuy.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Cobertura' AS type_error
from para_resumir_uy left join baseuy on para_resumir_uy.vertical = baseuy.vertical
                                                 and para_resumir_uy.store_id = baseuy.store_id
                                                 and para_resumir_uy.store_name = baseuy.store_name
where coverage is null 
and country is not null 
and city is not null 
and para_resumir_uy.vertical is not null 
and baseuy.brand_id is not null 
and baseuy.brand_name is not null 
and baseuy.store_id is not null 
and baseuy.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

union all

(select  distinct
        country,
        city,
        city_code,
        para_resumir_uy.vertical,
        baseuy.brand_id,
        baseuy.brand_name,
        baseuy.store_id,
        baseuy.store_name,
        is_enabled,
        is_marketplace,
        max(coverage) coverage,
        max(polygons) polygons,
        'Sin Poligonos' AS type_error
from para_resumir_uy left join baseuy on para_resumir_uy.vertical = baseuy.vertical
                                                 and para_resumir_uy.store_id = baseuy.store_id
                                                 and para_resumir_uy.store_name = baseuy.store_name
where polygons is null
and country is not null 
and city is not null 
and para_resumir_uy.vertical is not null 
and baseuy.brand_id is not null 
and baseuy.brand_name is not null 
and baseuy.store_id is not null 
and baseuy.store_name is not null
group by 1,2,3,4,5,6,7,8,9,10
order by 4 asc)

),


ULT_UY AS (

SELECT DISTINCT F.COUNTRY ,
        F.CITY AS "Ciudad",
        F.VERTICAL AS "Vertical",
        F.BRAND_NAME AS "BRAND_NAME",
        F.TYPE_ERROR AS "Tipo de error",
        F.STORE_ID AS "Store"
  

  
FROM FINAL_UY F
WHERE F.Vertical IN ('Restaurantes','Express','Licores','Farmacia','Super','Especializada','Especializadas','Mascotas','Bebes y niños','Belleza','Deportes','Floristeria','Hogar','Jugueteria','Libreria','Marcas','Moda','Regalos','Tecnologia','Sex Shop','Papeleria'))

SELECT * FROM ULT_BR
--UNION ALL
--SELECT * FROM ULT_CO
UNION ALL
SELECT * FROM ULT_MX
UNION ALL
SELECT * FROM ULT_AR
UNION ALL
SELECT * FROM ULT_CL
UNION ALL
SELECT * FROM ULT_PE
UNION ALL
SELECT * FROM ULT_EC
UNION ALL
SELECT * FROM ULT_CR
UNION ALL
SELECT * FROM ULT_UY    
"""
  print('rodando snowflake')
  df = snow.run_query(query_so)
  return df

def run_alarm(df):

  if not df.empty:
    print(df)

    if not df.empty:

      df['checked_at'] = current_time
      to_upload = df
      to_upload = to_upload[
        ["country", "checked_at", "ciudad", "vertical", "brand_name", "tipo de error", "store"]]

      print("uploading df")
      snow.upload_df_occ(to_upload, "nl_polygons_problems")

      text = '''
      *NONLIVE - Alarma PIN INCORRECTO O FUERA DE COBERTURA*

      '''
      print(text)
      home = expanduser("~")
      results_file = '{}/details_tiendas_polygons_problems.csv'.format(home)
      df_final = df[["country", "ciudad", "vertical", "brand_name", "tipo de error", "store"]]
      df_final.to_csv(results_file, index=False)
      slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")

    else:
      print('df null')
  else:
    print('products null')
    text = '''
    *NONLIVE - Alarma PIN INCORRECTO O FUERA DE COBERTURA*
    Sin nuevas tiendas para actuar :verified:
    '''
    slack.bot_slack(text,'C02MJQ1Q5H6')


df = products()

run_alarm(df)