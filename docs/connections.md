# Connections

This document intend to list the needed connections to have all the DAGs working. 

It is not mandatory to have al this connections in order to have Airflow working. Check in the header of the DAG File to identify what connections are needed to have a specific DAG working.

The fields explained below are the one required to set up the connections. If some of the fields are not listed, that means the field goes empty on the setup.

**Warning!**: Before setting up these connections, be sure the Fernet Key is set in the `airflow.cfg` file or it was provided as a environment variable. The `cryptography` library is required.

## S3
### example_s3
Connection needed to access the S3 Bucket. The credentials provided here need to have read-write access to the bucket.

Id: `example_s3`  
Type: `S3`  
Login: `<AWS Access Key Id>`  
Password: `<AWS Secret Access Key>`

## Snowflake
### example_snowflake

Id: `example_snowflake`  
Type: `Snowflake`  
Login: `<Snowflake User Name>`  
Password: `<Snowflake Password>`  
Extra:   
```
{
    "account": "<Account>",
    "warehouse": "NORMAL_LOAD",
    "database": "FIVETRAN",
    "role": "<Snowflake User Role>"
}
```

## HTTP
### example_http

id: `example_http`  
Type: `HTTP`  
Host: `https://example.com`  
Login: `<HTTP User>`  
Password: `<HTTP Password>`
