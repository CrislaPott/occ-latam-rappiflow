# Documentation

This section is intended to hold useful documentation of your project.

The [connections.md document](connections.md) may always be present, including the Airflow Connections names and settings required by your project.  
**Never include sensitive information like passwords**. Just write a placeholder as follows:
```
## connection_name
user: '<Service Account>'
password: '<Service Secret Key>'
```

In this template repo we include general guidelines needed for many Rappiflow based projects, like the "Setup Local Kubernetes" document.