# Troubleshooting

### SQL Queries taking way longer than usual in Snowflake.
Given that Rappiflow runs on Kubernetes, it is possible that some tasks get killed by the Task Orchestrator. If that's the case, most probably the error in the logs is: 
```
{jobs.py:2562} INFO - Task exited with return code -9
```

When retrying the Query Job, it is possible that the job never completes. It is highly possible that the problem is caused due to a unfinished SQL Transaction. 

This is preferable to having an inconsistent execution: `DELETE` worked but `INSERT` failed.

To fix this open [Snowflake console](https://hg51401.snowflakecomputing.com/console#/internal/worksheet) and run the following queries
```sql
-- check locks
SHOW LOCKS
-- show unfinished transactions
SHOW TRANSACTIONS
-- rollback the transaction id with the lock
SELECT SYSTEM$ABORT_TRANSACTION(<transaction_id>);
```

Once the locks are clean, run again the task in Airflow.