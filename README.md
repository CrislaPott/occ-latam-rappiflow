# occ-latam-rappiflow

The `occ-latam-rappiflow` repository is meant to be used for creating new rappiflow verticals.

The purpose of this repository is to be a template structure for creating new rappiflow based repositories.

The repository contains utility code to easily develop `DAG`s and perform local testing.

Production [Airflow](https://airflow.apache.org/) is running in a [Kubernetes Cluster](https://github.com/kubernetes/kubernetes/#kubernetes) on AWS, using configuration provided by Astronomer.io, managed by the DataOps team.


## Relevant reading

If you are not familiar with Airflow, we recommend you start by reading some of the following:

- [Key Concepts](https://airflow.apache.org/concepts.html)  
- [Concepts, explained](https://medium.com/@dustinstansbury/understanding-apache-airflows-key-concepts-a96efed52b1a)  
- [Official Tutorial](https://airflow.apache.org/tutorial.html)  
- [Astronomer.io Enterprise Edition](https://www.astronomer.io/docs/ee-overview/)

Rappiflow adds a layer of plugins on top of Apache's Airflow. If you want to know more about these plugins, take a look at the [rappiflow-plugins repository](https://bitbucket.org/rappinc/rappiflow-plugins/).

We also recommend you check out the [Rappiflow confluence space](https://rappidev.atlassian.net/wiki/spaces/DAT/pages/488308932/RappiFlow), where you can find a lot of useful information and tutorials.


## Instruction to upgrade the Rappiflow toolkit

It is pretty easy to upgrade the version of Rappiflow you are using.

You only need to modify the `FROM` instruction in the main Dockerfile of your project:

```
ARG RAPPI_DOCKER_REGISTRY=registry.rappiai.com
FROM ${RAPPI_DOCKER_REGISTRY}/rappiflow:0.1.0
```

After the colon you can set the version code, `0.1.0` in this example. The new releases of Rappiflow will be announced in the Slack channel #dataops_on_schedule and listed in [this Confluence page](https://rappidev.atlassian.net/wiki/spaces/DAT/pages/511934633/Announcements).

The first line defines a Docker registry prefix for the resulting image to be pushed/pulled. It is for compatibility and portability between environments (localhost, DEV cluster, PROD cluster). **It should not be modified**.

The technical details about why it is done this way are explained in [this document](https://rappidev.atlassian.net/wiki/spaces/DAT/pages/513051202/Rappiflow+module+structure).


## Project structure

These are the most relevant files and directories of the project:
```
.
├── .astro/
│   └── config.yaml     # it holds the Astronomer project name
├── .env
├── .env_prod
├── Dockerfile
├── dags/               # all DAGs must be under this folder for airflow to detect them  
│   ├── some_dags/      # you can organize your DAGs in subfolders
│   └── utils/          # utils for your DAGs should be in here
├── development/        # tools for development
│   ├── airflow-core/   # config files for Airflow test environment
│   └── tools/          # useful scripts
├── docs/               # Relevant Reading
├── include/            # should contain extra files you may need
├── packages.txt        # Alpine packages
├── requirements.txt    # Python dependencies
└── tests/
    ├── dags/           # test for dags
    │   └── some_dags/  # you should organize your tests by dag 'type' 
    └── utils/          # test for utils
```

- Don't forget to take a look at the [docs directory](docs/).

- The `.env` file is used to load environment variables to the docker image when running localhost using `astro` cli.

- The `.env_prod` file is used to load environment variables in production.

- `Dockerfile` holds the image that `astro` cli will build and run when you start Airflow locally. This file controls the version of Rappiflow toolkit you're using.

- The `include` folder is emtpy. It's meant to use for any extra files you may need. Astronomer will copy that folder to the docker image.
More about that can be found [here](https://forum.astronomer.io/t/what-is-the-include-directory-that-astro-airflow-init-creates/150).

- The `requirements.txt` file is where you should put your Python dependencies. For example, if one of your DAGs needs a specific library for its execution, then you should add that library to this file.

- The `packages.txt` file is similar to the previous one, but for Alpine packages.

--- 


# Getting started

## Install requirements

- [Docker](https://docs.docker.com/install/) `2.1.0.1` or greater.
- [Astronomer CLI](https://github.com/astronomer/astro-cli#installing-astro) `latest` version.
- (Optional) [Miniconda](https://docs.conda.io/en/latest/miniconda.html) to create a virtual environment to develop locally.


## Setup your SSH keys in Bitbucket

This step is required to easily setup your local environment by running some scripts. In the Atlassian documentation there's a tutorial that explains [how to setup SSH keys](https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html).


## Build the Rappiflow base image

The first step to start using Rappiflow is to build its base Docker image. This setup process is simplified by the script:
```
./development/tools/setup.sh
```
This will do the following:

- Download the [rappiflow-plugins repo](https://bitbucket.org/rappinc/rappiflow-plugins/) to the root directory of your own repo (nevermind, it's listed in `.gitignore`). This step requires SSH keys.
- Checkout the required version of Rappiflow.
- Build the base Docker image for Rappiflow.
- Optionally, create a local conda environment to easily access the Rappiflow toolkit and run linters and tests locally.

To skip the last step, run the script as follows:
```
./development/tools/setup.sh --no-conda-env
```


## Start Airflow

To start Airflow using the Astronomer CLI, run:
```
astro dev start
```

This will start the Web UI. If you ever need to stop that from running, you can do that by running:
```
astro dev stop
```

## Access the Web UI

Once you build and run Airflow, open a web browser and navigate to [http://localhost:8080](http://localhost:8080).
Use the following default credentials to log-in:

> user: admin  
> password: admin

Once you are logged in, you will be taken to Airflow's main interface.


## Local Logs

Each task in a DAG has their logs. You usually access them from Airflow's Web UI.
If you need to access to the `Scheduler` or `WebServer` logs, you can do it by using the `docker logs` command.

- Run `docker ps`.
- Find the ids of the `scheduler` and `webserver` containers.
- Use that id with the `docker logs <container_id> -f` to access the container logs.


## Connections

Connections in Airflow are the way to connect your environment with external tools or services.

The easiest way to [setup connections](https://airflow.apache.org/howto/connection/index.html#creating-a-connection-with-the-ui) in Airflow is using the UI.

It's a good practice to list in the header of each DAG file what connections need to be configured for it to work correctly.


## Pools

To have the infrastructure pressure under control, it is a good practice to set [Pools](https://airflow.apache.org/concepts.html#pools) to manage how many similar tasks can run in parallel.

Check the header of every DAG to see if the `pool` needs to be configured.

--- 


# Development using Docker

You can also run linters and tests using docker, without setting a local virtual environment.

## Build the docker image

First, you need to  *build the docker image*, where the unit tests and linters will run. Keep in mind that you need to repeat this step every time you make changes to your code.

To build the image, run:
```
./development/tools/build.sh
```

Keep in mind that this requires the rappiflow base image to be present locally. You can find how to do it in the **Build the rappiflow base image** section above.

## Run linters and tests

After the image is built, you can run the linters on the code of the docker image:
```
./development/tools/linter.sh
``` 

And also run the unit tests:
```
./development/tools/unit-tests.sh
```

Both scripts only check the code in the most recent Docker image, so if any of the code from the source has changed, you will need to execute the `build.sh` script again.

--- 


# Local development 

## Install development hooks

Install these development hooks, so that every time you push, it automatically builds the image and runs the linters and tests before actually pushing.
```
./development/tools/install-hooks.sh
```

## DAG development

Start Airflow using the Astronomer CLI.
```
astro dev start
```

This will dynamically look for modifications on the `.py` files in the `dags` folder, and automatically update them.


### Example DAGs

Check the `example_dags` folder to see how create your own DAGs.


## Run unit tests

If you setup everything to develop locally, you can run tests with the following command:

```
python -m pytest tests
```


## Guarantee Code Quality with linters

To guarantee code quality, we use three type of linters.

- **Flake8**: to check coding style.
- **PyDocStyle**: to check the docstring documentation.
- **MyPy**: to check python's static typing.
    
If you setup everything to develop locally, you can run them with these commands:
```
flake8 dags/ tests/
pydocstyle dags/ tests/
mypy dags/ tests/
```

--- 


# Deployment

In Astronomer UI, inside the deployment, add (at least) the environment variables located in the file `.env_prod`. More info of some of them:

- `AIRFLOW_ENVIRONMENT`: The value can be `production` when deployed to prod.
- `ASTRONOMER_BASE_DOMAIN`: The value should be the base domain where Astronomer was deployed.

--- 


# Documentation

Be a good citizen and document your `DAGs`, `Operators`, `Queries`, etc, so the _future you_ is able to pick it up and maintain it with ease.

For `DAGs`, write a good file header explaining:

- Data sources and destinations
- Required connections
- Pools
- Any other logic you think worth to mention

Here we provide you with an example of a DAG header docstring:

```
"""
# <NAME OF THE DAG>

SUMMARY:
--------

* DAG Name:
    <NAME_OF_THE_DAG>
* Owner:
    <BUSINESS_OWNER>
* Tech owner:
    <TECH_OWNER>
* Description:
    <DESCRIPTION>
* Output:
    <GENERATED_OUTPUTS>

CONNECTIONS:
------------

* Connection Type <CONNECTION_TYPE_1> with the name `<CONNECTION_NAME_1>`.
* Connection Type <CONNECTION_TYPE_2> with the name `<CONNECTION_NAME_2>`.

"""
```

Feel free to modify it, add some other fields that may be useful to your case.

For queries, add a comment in the header of the query file to explain the overall logic and inline comments to explain complex data manipulations.

For connections, add in the [connections documentation](docs/connections.md) how to setup them, excluding sensitive information such as passwords.

--- 


# Troubleshooting

Things may and _will_ wrong, we've created a Rappiflow confluence page with a lot of useful information, be sure to check it out.
If you have any questions, or you are having trouble with Rappiflow, you can find support in the `#dataops_on_schedule` Slack channel.

--- 


## Configure PyCharm (optional)

- **Configure project interpreter**
    1. Go to `PyCharm` -> `Preferences`
    2. In the `Preferences` window, on the left pane, go to: `project: rappiflow-template` -> `Project Interpreter`
    3. On the right pane, click the wheel icon (on the top right) and select `add`
    4. In the new window, on the left pane, select `Conda environment`. On the right pane, select `Existing Conda Environment`
    5. Select `...` and navigate through your filesystem to your `miniconda` venv. Usually `$HOME/miniconda3/venv/rappiflow-template`

- **Configure project structure**
    1. Go to `PyCharm` -> `Preferences`
    2. In the `Preferences` window, on the left pane, go to `project: rappiflow-template` -> `Project Structure`
    3. On the right pane, select `dags` and mark as `source`

- **Configure tests runner**
    1. Go to `PyCharm` -> `Preferences`
    2. In the `Preferences` window, on the left pane, go to `Tools` -> `Python Integrated Tools`
    3. On the right pane, select `pytest` on `default test runner` inside the `Testing` section

- **Configure test template for test runner**
    1. Go to `Run` -> `Edit Configurations`
    2. On the left pane, select `Templates` -> `Python tests` -> `pytests`
    3. On the right pane, on `Environment section`, go to `Environment variables` and add the following variables:
        - `AIRFLOW_HOME=[path_to_repo]/dev-tools/airflow-core`
    4. On the right pane, on `Environment section`, set the `working directory` to this repo path in your filesystem

