"""Test example Astronomer DAG."""
import pytest
from airflow.models import DagBag


@pytest.fixture(scope='module')
def dag_id() -> str:
    """Return the DAG id to test."""
    return 'zzz_example_astronomer_dag'


@pytest.fixture(scope='module')
def dag_bag() -> DagBag:
    """Obtain the DagBag of the Repository."""
    return DagBag()


def test_task_count(dag_id, dag_bag):
    """Check task count of the dag."""
    dag = dag_bag.get_dag(dag_id)
    assert 12 == len(dag.tasks)


def test_contain_tasks(dag_id, dag_bag):
    """Check the desired tasks are in the place."""
    dag = dag_bag.get_dag(dag_id)
    tasks = dag.tasks
    task_ids = list(map(lambda task: task.task_id, tasks))

    task_count = 12
    expected_tasks = []

    for i in range(1, task_count + 1):
        expected_tasks.append(f'print_date{i}')

    assert expected_tasks == task_ids
