"""Test example Python DAG."""
import pytest
from airflow.models import DagBag


@pytest.fixture(scope='module')
def dag_id() -> str:
    """Return the DAG id to test."""
    return 'zzz_example_python_operator'


@pytest.fixture(scope='module')
def dag_bag() -> DagBag:
    """Obtain the DagBag of the Repository."""
    return DagBag()


def test_task_count(dag_id, dag_bag):
    """Check task count of the dag."""
    dag = dag_bag.get_dag(dag_id)
    assert 6 == len(dag.tasks)


def test_contain_tasks(dag_id, dag_bag):
    """Check the desired tasks are in the place."""
    dag = dag_bag.get_dag(dag_id)
    tasks = dag.tasks
    task_ids = list(map(lambda task: task.task_id, tasks))

    expected_tasks = ['print_the_context', 'sleep_for_0', 'sleep_for_1', 'sleep_for_2', 'sleep_for_3',  'sleep_for_4']

    assert expected_tasks == task_ids
