"""Test example Kubernetes Executor DAG."""
import pytest
from airflow.models import DagBag


@pytest.fixture(scope='module')
def dag_id() -> str:
    """Return the DAG id to test."""
    return 'zzz_example_kubernetes_executor'


@pytest.fixture(scope='module')
def dag_bag() -> DagBag:
    """Obtain the DagBag of the Repository."""
    return DagBag()


def test_task_count(dag_id, dag_bag):
    """Check task count of the dag."""
    dag = dag_bag.get_dag(dag_id)
    assert 4 == len(dag.tasks)


def test_contain_tasks(dag_id, dag_bag):
    """Check the desired tasks are in the place."""
    dag = dag_bag.get_dag(dag_id)
    tasks = dag.tasks
    task_ids = list(map(lambda task: task.task_id, tasks))

    expected_tasks = ['pre_task', 'start_task', 'one_task', 'two_task']

    assert expected_tasks == task_ids
