import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
import pytz

def country_timezones(country):
    if country == 'co':
        timezone = 'Bogota'
        interval = '4h'
    elif country == 'cr':
        timezone = 'Costa_Rica'
        interval = '5h'
    elif country == 'mx':
        timezone = 'Mexico_City'
        interval = '4h'
    elif country == 'cl':
        timezone = 'Santiago'
        interval = '4h'
    elif country == 'pe':
        timezone = 'Lima'
        interval = '4h'
    elif country == 'ec':
        timezone = 'Guayaquil'
        interval = '4h'
    elif country == 'br':
        timezone = 'Buenos_Aires'
        interval = '3h'
    elif country == 'uy':
        timezone = 'Montevideo'
        interval = '3h'
    elif country == 'ar':
        timezone = 'Buenos_Aires'
        interval = '3h'
        
    return timezone, interval

def country_current_time(country):
    if country == 'ar':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'cl':
        tz = pytz.timezone('America/Santiago')
        current_time = datetime.now(tz)
    elif country == 'co':
        tz = pytz.timezone('America/Bogota')
        current_time = datetime.now(tz)
    elif country == 'cr':
        tz = pytz.timezone('America/Costa_Rica')
        current_time = datetime.now(tz)
    elif country == 'ec':
        tz = pytz.timezone('America/Guayaquil')
        current_time = datetime.now(tz)
    elif country == 'br':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'mx':
        tz = pytz.timezone('America/Mexico_City')
        current_time = datetime.now(tz)
    elif country == 'pe':
        tz = pytz.timezone('America/Lima')
        current_time = datetime.now(tz)
    elif country == 'uy':
        tz = pytz.timezone('America/Montevideo')
        current_time = datetime.now(tz)

    return current_time


def slack_channels(country):
    if country == 'co':
        channel_alert = 'C01C1LSQREW'
        channel = 'C010GBS4BT7'
    elif country == 'ar':
        channel_alert = 'C01EEPH2ECU'
        channel = 'C010GB6FJ1W'
    elif country == 'cl':
        channel_alert = 'C01EEPH2ECU'
        channel = 'C0104SW3HS7'
    elif country == 'mx':
        channel_alert = 'C01GMHJRDM2'
        channel = 'C0104SVEZEX'
    elif country == 'uy':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0103HJS1B4'
    elif country == 'ec':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0104SXCA4B'
    elif country == 'cr':
        channel_alert = 'C01E5G49L1K'
        channel = 'G01GQ1F4XFU'
    elif country == 'pe':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0103HK55CJ'
    elif country == 'br':
        channel_alert = 'C01672B528Y'
        channel = 'CU3E4KNTY'

    return channel_alert, channel