import os
import pandas as pd
import requests
import json
import datetime as dt
import numpy as np
import pytz
from datetime import datetime, timedelta, date, time
from lib import slack
from lib import snowflake as snow
from functions import timezones


def CreateOpsGenieAlert(country, count, mean, physical_store_ids, slots_ids, order_ids, range_time):
    
    header = {
        "Authorization": "GenieKey " + 'b46f1a3c-580d-4ad2-97ec-da04d1604078',
        "Content-Type": "application/json"
    }

    x = {
    "message": f'''Store Closed - Slots Bot, {count} suspended Physical Store Ids - {''.join(country).upper()}''',
    "alias": f'''Bot Store Closed Slots: {''.join(country).upper()}''',
    "description":f'''Bot reached the number of {count} suspended Physical Store Ids in the country {''.join(country).upper()} today, in the hour interval from {range_time} vs the mean {mean} in the same hour interval the same day in the last two weeks.
    Physical store ids: {''.join(map(str, physical_store_ids))}
    Slots ids: {''.join(map(str, slots_ids))}
    Order ids: {''.join(map(str, order_ids))}'''
    }


    body = json.dumps(x)
    print(body)
    
    try:
        response = requests.post("https://api.opsgenie.com/v2/alerts",
                                headers=header,
                                data=body)
        jsontemp = json.loads(response.text)
        print(jsontemp)

        if response.status_code == 202:
            return response
    except:
        print('error')

    print(response)


def run_alarm(country):

    timezone, interval = timezones.country_timezones(country)

    query = f'''
    select 
    cast(suspended_at as datetime) as fecha,
    country,
    a.physical_store_id,
    slots_ids, 
    order_ids
    from ops_occ.store_closed_super a
    left join (
        select physical_store_id
        from
        {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.SHOPPER_PHYSICAL_STORES b
        left join FIVETRAN.{country}_PG_MS_SHOPPERS_PUBLIC.SHOPPERS c on c.id=b.id
        where c.type= 'personalshopper'
        ) d
    on a.physical_store_id = d.physical_store_id
    where country in ('{country}')
    AND fecha >= date(convert_timezone('America/{timezone}',current_timestamp())) - interval '15 days'  
    group by 1,2,3,4,5
    order by 2 asc, 1 desc
    '''

    query1 = f'''
    select 
    cast(suspended_at as datetime) as fecha,
    country,
    a.physical_store_id,
    slots_ids, 
    order_ids
    from ops_occ.store_closed_super a
    left join (
        select physical_store_id
        from
        {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.SHOPPER_PHYSICAL_STORES b
        left join FIVETRAN.{country}_PG_MS_SHOPPERS_MS_PUBLIC.SHOPPERS c on c.id=b.id
        where c.type= 'personalshopper'
        ) d
    on a.physical_store_id = d.physical_store_id
    where country in ('{country}')
    AND fecha >= date(convert_timezone('America/{timezone}',current_timestamp())) - interval '15 days'  
    group by 1,2,3,4,5
    order by 2 asc, 1 desc
    '''

    if country == 'co':
        df = snow.run_query(query1)
    elif country == 'ar':
        df = snow.run_query(query)
    elif country == 'cl':
        df = snow.run_query(query)
    elif country == 'mx':
        df = snow.run_query(query1)
    elif country == 'uy':
        df = snow.run_query(query)
    elif country == 'ec':
        df = snow.run_query(query1)
    elif country == 'cr':
        df = snow.run_query(query1)
    elif country == 'pe':
        df = snow.run_query(query1)
    elif country == 'br':
        df = snow.run_query(query1)

    print(df.shape)
    df.head()


    ##########

    now = timezones.country_current_time(country)
    # now = datetime.now(tz)
    # now.strftime('%Y-%m-%d %H:%M:%S')

    day_0 = (now  -  timedelta(days=0)).strftime('%Y-%m-%d')
    day_7 = (now  -  timedelta(days=7)).strftime('%Y-%m-%d')
    day_14 = (now  -  timedelta(days=14)).strftime('%Y-%m-%d')

    dates = [day_0, day_7, day_14]
    dates

    ##########

    df['fecha_hora'] = pd.to_datetime(df['fecha'], format='%d-%m-%y %H:%M:%S.%f').dt.strftime('%Y-%m-%d %H:%M:%S')
    df['hora'] = pd.to_datetime(df['fecha'], format='%d-%m-%y %H:%M:%S.%f').dt.strftime('%H:%M:%S')
    df['fecha_dia'] = pd.to_datetime(df['fecha'], format='%d-%m-%y %H:%M:%S.%f').dt.strftime('%Y-%m-%d')

    df.head()

    ##########

    print(df.shape)
    df = df.loc[(df['fecha_dia'].isin(dates))]
    print(df.shape) 

    ##########

    df.loc[(df['fecha'].dt.time >= time(0,00)) & (df['fecha'].dt.time < time(7,00)), 'range_time'] = '00-07'

    df.loc[(df['fecha'].dt.time >= time(7,00)) & (df['fecha'].dt.time < time(12,00)), 'range_time'] = '07-12'

    df.loc[(df['fecha'].dt.time >= time(12,00)), 'range_time'] = '12-00'

    df['range_time'].value_counts(dropna=False)

    ##########

    df.loc[(df['fecha'].dt.time >= time(0,00)) & (df['fecha'].dt.time < time(7,00)), 'closed_at'] = df['fecha_dia'] + ' 11:00:00'

    df.loc[(df['fecha'].dt.time >= time(7,00)) & (df['fecha'].dt.time < time(12,00)), 'closed_at'] = df['fecha_dia'] + ' 13:00:00'

    df.loc[(df['fecha'].dt.time >= time(12,00)), 'closed_at'] = df['fecha_dia'] + ' 23:59:59'

    df['closed_at'] =  pd.to_datetime(df['closed_at'], format='%Y-%m-%d %H:%M:%S')

    df['closed_at'].value_counts(dropna=False)

    ##########

    df.loc[(df['fecha_dia'] == day_0), 'range_day'] = 'day0'
    df.loc[(df['fecha_dia'] == day_7), 'range_day'] = 'day7'
    df.loc[(df['fecha_dia'] == day_14), 'range_day'] = 'day14'

    df['range_day'].value_counts(dropna=False)


    ########## HISTORY

    df_history= df.loc[(df['range_day'] == 'day7') | (df['range_day'] == 'day14')]
    df_history = df_history.groupby(['range_day','range_time','closed_at'])['physical_store_id'].agg(['count']).reset_index()
    df_history['mean'] = df_history.groupby('range_time')['count'].transform('mean')

    print(df_history.shape)
    df_history.head(10)

    ##########

    df_history = df_history.groupby(['range_time', 'mean'])['count'].agg(['count']).reset_index()
    df_history.drop('count', axis=1, inplace=True)
    df_history


    ########## TODAY

    df_today = df.loc[(df['range_day'] == 'day0')]
    df_today['physical_store_ids'] = df_today.groupby(['range_day','range_time','closed_at', 'country'])['physical_store_id'].transform(lambda x: ','.join(x))
    df_today['slots_ids'] = df_today.groupby(['range_day','range_time','closed_at','physical_store_ids', 'country'])['slots_ids'].transform(lambda x: ','.join(x))
    df_today['order_ids'] = df_today.groupby(['range_day','range_time','closed_at','physical_store_ids', 'slots_ids', 'country'])['order_ids'].transform(lambda x: ','.join(x))
    print(df_today.shape)
    df_today.head()

    ########## 

    df_today = df_today.groupby(['range_time', 'closed_at', 'physical_store_ids', 'slots_ids', 'order_ids', 'country'])['physical_store_ids'].agg(['count']).reset_index()
    print(df_today.shape)
    df_today.head()

    ##########

    df_final = pd.merge(df_today, df_history, left_on=['range_time'], right_on=['range_time'], how='left')
    df_final['mean']  = df_final['mean'] .replace(np.nan, 0)    

    df_final['mean'] = df_final['mean'].astype(int)

    df_final['variation'] = round((df_final['count'] - df_final['mean']) / df_final['mean'], 2) * 100
    df_final['variation'] = df_final['variation'].replace([np.nan, np.inf],0)

    df_final = df_final.loc[(df_final['count'] >= 15)]
    df_final = df_final.loc[(df_final['variation'] >= 60.)]
    df_final = df_final.loc[(df_final['count'] >= df_final['mean'])]

    print(df_final.shape)
    df_final.head()

    ##########

    df_final.to_string()

    ##########

    for index, row in df_final.iterrows():
    
        country = row['country'],
        count = row['count'],
        mean = row['mean'],
        physical_store_ids = row['physical_store_ids'],
        slots_ids = row['slots_ids'],
        order_ids = row['order_ids'],
        range_time = row['range_time']
    
        CreateOpsGenieAlert(country, count, mean, physical_store_ids, slots_ids, order_ids, range_time)

        text= '''
            *Opsgenie - Store Closed Slots :alert:*
            Country: :flag-{country}:
            Bot reached the number of *{count}* suspended Physical Store Ids today, in the hour interval from {range_time} vs the mean *{mean}* in the same hour interval the same day in the last two weeks.
            Physical store ids: {physical_store_ids}
            Slots ids: {slots_ids}
            Order ids: {order_ids}
            '''.format(
                country=row['country'],
                count = row['count'],
                mean = row['mean'],
                physical_store_ids = row['physical_store_ids'],
                slots_ids = row['slots_ids'],
                order_ids = row['order_ids'],
                range_time = row['range_time'])
            
        print(text)
        slack.bot_slack(text,'C02LCB2N2AE')

    ##########

countries = ['ar','co','br','pe','mx','ec','cr','uy','cl']
for country in countries:

    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)




