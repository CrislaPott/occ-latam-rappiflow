import os
import pandas as pd
import requests
import json
from lib import snowflake as snow
from functions import timezones

def CreateOpsGenieAlert(country,brand_group_id, brand_group_name,stores,store_ids):
    header = {
        "Authorization": "GenieKey " + 'b46f1a3c-580d-4ad2-97ec-da04d1604078',
        "Content-Type": "application/json"
    }

    x = {
    "message": f'''Predictive Store Closed Bot, {stores} suspended Group Brands  - {''.join(country).upper()}''',
    "alias": f'''Bot Predictive Store Closed: {''.join(country).upper()} {''.join(map(str, brand_group_name))}''',
    "description":f'''Today the bot reached the number of {stores} suspended group brands in the country {''.join(country).upper()}
    Brand group id: {''.join(map(str, brand_group_id))}
    Brand group name: {''.join(map(str, brand_group_name))}
    Store ids: {''.join(map(str, store_ids))}'''
    }

    body = json.dumps(x)
    print(body)
    
    try:
        response = requests.post("https://api.opsgenie.com/v2/alerts",
                                headers=header,
                                data=body)
        jsontemp = json.loads(response.text)
        print(jsontemp)

        if response.status_code == 202:
            return response
    except:
        print('error')

    print(response)
    
def run_alarm(country):

    query = f'''
    select 
    country,
    brand_group_id,
    brand_group as brand_group_name, 
    count(distinct store_id) as stores, 
    listagg(distinct store_id, ',') as store_ids
    from
    (select a.country,
           case when a.country in ('br','ar','uy','cl') then 'America/Buenos_Aires'
                when a.country in ('co','ec','pe') then 'America/Bogota'
                when a.country in ('cr','mx') then 'America/Costa_Rica'
                end as timezone,
           a.suspended_at,
           a.brand_group_id, brand_group,
           convert_timezone(timezone,'UTC',a.suspended_at::timestamp_ntz)::timestamp_ntz as x, 
           store_id
    from ops_occ.predictive_groupbrand a
    left join ops_occ.predictive_groupbrand_details b on a.brand_group_id=b.brand_group_id and a.suspended_at=b.suspended_at
    left join (select lower(country) as country, brand_group_id, brand_group from OPS_GLOBAL.global_orders group by 1,2,3) c on c.country=a.country and a.brand_group_id=c.brand_group_id
    where x::timestamp_ntz >= convert_timezone('UTC',current_timestamp())::timestamp_ntz - interval '20 minutes'
    )
    group by 1,2,3
    having stores >=50
    '''
    df = snow.run_query(query)
    print(df)
    df.to_string()

    for index, row in df.iterrows():
        country = row['country'],
        brand_group_id = row['brand_group_id'],
        brand_group_name = row['brand_group_name'],
        stores = row['stores'],
        store_ids = row['store_ids'],
        CreateOpsGenieAlert(country,brand_group_id, brand_group_name,stores,store_ids)
        
countries = ['co','br','cl','ar','pe','cr','ec','mx','uy']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)