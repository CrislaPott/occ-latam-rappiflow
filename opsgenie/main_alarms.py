import os, sys, json
import pandas as pd
from lib import opsgenie
from lib import snowflake as snow
from lib import avenue
from functions import timezones

def spike_in_orders(country):
    timezone, interval = timezones.country_timezones(country)
    query = f"""
    with a as (
    select distinct groupbrandid::text as groupbrandid, alarm_at, row_number() over (partition by sp.groupbrandid order by alarm_at) as row_number
    from ops_occ.spike_in_orders sp
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country = '{country}'
    )
    select a.groupbrandid::text as groupbrandid, sb.name as brand_name, max(row_number) as number_of_alarms, max(alarm_at) as last_alarm
    from a
    join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS sb on sb.ID = a.groupbrandid
    group by 1,2
    having last_alarm::timestamp_ntz >= dateadd(minutes,-30,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and number_of_alarms > 2
    """

    df = snow.run_query(query)

    print(df)
    
    for index, row in df.iterrows():
        brand_id = row['groupbrandid'],
        brand_name = row['brand_name'],
        number_of_alarms = row['number_of_alarms'],
        last_alarm = row['last_alarm'],
        country = country

        message = f'''
        The Group Brand {''.join(brand_name)} had {''.join(map(str, number_of_alarms))} alerts today for the reason: spike in created orders
        '''
        alias = f'''Spike in Orders {''.join(brand_name)} - {''.join(country)}'''
        description = f'''Country: {''.join(country).upper()}'''

        opsgenie.CreateAlert(message, alias,description)

countries = ['ar','co','br','pe','mx','ec','cr','uy','cl']
for country in countries:
    print(country)
    try:
        spike_in_orders(country)
    except Exception as e:
        print(e)