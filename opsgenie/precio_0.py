import os, sys, json
import pandas as pd
from lib import opsgenie
from lib import snowflake as snow
from lib import avenue
from functions import timezones

def spike_in_orders(country):
    timezone, interval = timezones.country_timezones(country)
    query = f"""
    select country, alarm_at, count(distinct product_id) as products
     , listagg(distinct product_id,',') as product_ids
     , listagg(distinct store_id,',') store_ids
     , listagg(distinct order_ids,',') order_ids
from ops_occ.product_price_errors
where alarm_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '30 min'
  and unit_price_in_order = 0
  and country = '{country}'
group by 1,2
having products >= 10
    """
    df = snow.run_query(query)

    print(df)
    
    for index, row in df.iterrows():
        alarm_at = row['alarm_at'],
        products = row['products'],
        product_ids = row['product_ids'],
        store_ids = row['store_ids'],
        order_ids = row['order_ids'],
        country = country

        message = f'''
        Alarma products com precio $0 - {''.join(country)}
        Más de {''.join(map(str, products))} productos en una notificatión
        '''
        alias = f'''Precio $0 {''.join(country)}'''
        description = f'''Product IDs: {''.join(product_ids)}
        Order IDs: {''.join(order_ids)}
        Store IDs: {''.join(store_ids)}'''

        opsgenie.CreateAlert(message, alias,description)

countries = ['ar','co','br','pe','mx','ec','cr','uy','cl']
for country in countries:
    print(country)
    try:
        spike_in_orders(country)
    except Exception as e:
        print(e)