import os
import pandas as pd
import requests
import json
from lib import snowflake as snow
from functions import timezones

def CreateOpsGenieAlert(country, alarms, riesgos):
    header = {
        "Authorization": "GenieKey " + 'b46f1a3c-580d-4ad2-97ec-da04d1604078',
        "Content-Type": "application/json"
    }

    x = {
    "message": f'''Risk Alarm Bot, {alarms} risk alarms  - {''.join(country).upper()}''',
    "alias": f'''Bot Risk Alarm: {''.join(country).upper()}''',
    "description":f'''Today the bot reached the number of {alarms} risk alarms in the country {''.join(country).upper()}
    Risks: {''.join(map(str, riesgos))}'''
    }

    body = json.dumps(x)
    print(body)
    
    try:
        response = requests.post("https://api.opsgenie.com/v2/alerts",
                                headers=header,
                                data=body)
        jsontemp = json.loads(response.text)
        print(jsontemp)

        if response.status_code == 202:
            return response
    except:
        print('error')

    print(response)

def run_alarm(country):

    query = f'''
    select country, 
    count(*) as alarms, 
    listagg(distinct risk_id, ',') as riesgos
    from
    (
    select country,
        case when country in ('br','ar','uy','cl') then 'America/Buenos_Aires'
                when country in ('co','ec','pe') then 'America/Bogota'
                when country in ('cr','mx') then 'America/Costa_Rica'
                end as timezone,
        risk_id,
        alarm_at,
        convert_timezone(timezone,'UTC',alarm_at::timestamp_ntz)::timestamp_ntz as x
    from ops_occ.risks_alarm
    where x::timestamp_ntz >= convert_timezone('UTC',current_timestamp())::timestamp_ntz - interval '1h'
    )
    group by 1
    having alarms >=3
    '''
    df = snow.run_query(query)
    print(df)
    df.to_string()

    for index, row in df.iterrows():
        country = row['country'],
        alarms = row['alarms'],
        riesgos = row['riesgos'],
        CreateOpsGenieAlert(country, alarms, riesgos)

countries = ['mx','br','co','ar','cl','pe','uy','ec','cr']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)