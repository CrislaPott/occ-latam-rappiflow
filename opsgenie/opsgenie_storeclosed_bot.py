import os
import pandas as pd
import requests
import json
from lib import snowflake as snow
from functions import timezones

def CreateOpsGenieAlert(country, variation,d0,d7,d14,avg):
    header = {
        "Authorization": "GenieKey " + 'b46f1a3c-580d-4ad2-97ec-da04d1604078',
        "Content-Type": "application/json"
    }

    x = {
    "message": f'''Store Closed Bot {variation}% of Variation - {''.join(country).upper()}''',
    "alias": f'''Bot Store Closed: {''.join(country).upper()}''',
    "description":f'''Number of closed stores in the country {''.join(country).upper()} by the bot reached {variation}% of variation versus last 2 last two weeks
    D0: {''.join(map(str, d0))}
    D7: {''.join(map(str, d7))}
    D14: {''.join(map(str, d14))}
    Average last two weeks: {''.join(map(str, avg))}'''
    }

    body = json.dumps(x)
    print(body)
    
    try:
        response = requests.post("https://api.opsgenie.com/v2/alerts",
                                headers=header,
                                data=body)
        jsontemp = json.loads(response.text)
        print(jsontemp)

        if response.status_code == 202:
            return response
    except:
        print('error')

    print(response)

def run_alarm(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
--no_cache
with base as (select suspended_at::date as day, store_id, country, suspended_at
from ops_occ.store_closed_bot)
, b as (
select country, count(distinct case when day = dateadd(day,-7,convert_timezone('America/{timezone}',current_timestamp()))::date
    and to_char(suspended_at::timestamp_ntz,'hh:mi') <= to_char((dateadd(day,-7,convert_timezone('America/{timezone}',current_timestamp()))),'hh:mi')
    then store_id else null end) as d7,
count(distinct case when day = dateadd(day,-14,convert_timezone('America/{timezone}',current_timestamp()))::date
    and to_char(suspended_at::timestamp_ntz,'hh:mi') <= to_char((dateadd(day,-14,convert_timezone('America/{timezone}',current_timestamp()))),'hh:mi')
    then store_id else null end) as d14,
count(distinct case when day = convert_timezone('America/{timezone}',current_timestamp())::date
    and to_char(suspended_at::timestamp_ntz,'hh:mi') <= to_char((dateadd(day,0,convert_timezone('America/{timezone}',current_timestamp()))),'hh:mi')
    then store_id else null end) d0
from base
group by 1)
select b.*, round(((coalesce(d7,0)+coalesce(d14,0))/2),2) as avg, round((((d0-avg)/nullif(avg,0))*100),2) as variation, ((coalesce(d0,0)- coalesce(avg,0)))
from b
where (((variation >= 50)
    and ((coalesce(d0,0)- coalesce(avg,0)) >= 15)
    ) or (variation <= -80 and coalesce(avg,0) >= 15)
       )

and country in ('{timezone}')
'''
    df = snow.run_query(query)
    print(df)
    df.to_string()

    for index, row in df.iterrows():
        country = row['country'],
        d0 = row['d0'],
        d7 = row['d7'],
        d14 = row['d14'],
        avg = row['avg'],
        variation = row['variation']
        CreateOpsGenieAlert(country,variation, d0,d7,d14,avg)

countries = ['ar','co','br','pe','mx','ec','cr','uy','cl']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)