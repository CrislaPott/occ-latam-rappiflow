import os
import pandas as pd
import requests
import json
from airflow.models import Variable

def CreateAlert(message, alias,description):
    opsgenie_key = 'b46f1a3c-580d-4ad2-97ec-da04d1604078'

    header = {
        "Authorization": "GenieKey " + opsgenie_key,
        "Content-Type": "application/json"
    }

    x = {
    "message": '"{message}"'.format(message=message),
    "alias": '"{alias}"'.format(alias=alias),
    "description": '"{description}"'.format(description=description)
    }

    body = json.dumps(x)
    print(body)

    try:
        response = requests.post("https://api.opsgenie.com/v2/alerts",
                                headers=header,
                                data=body)
        jsontemp = json.loads(response.text)
        print(jsontemp)

        if response.status_code == 202:
            return response

    except:
        print('error')

    print(response)
