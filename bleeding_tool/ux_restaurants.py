import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, cms
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz

def excluding_stores(country):
	timezone, interval = timezones.country_timezones(country)
	query = f'''
	select store_id::text as store_id
	from ops_occ.ux_restaurantes
	where suspended_at >= convert_timezone('America/{timezone}',current_timestamp()) - interval '8h'
	and country='{country}'
	group by 1
	'''
	df = snow.run_query(query)
	return df

def offenders(country):
	timezone, interval = timezones.country_timezones(country)

	query = f"""
	--no_cache
WITH ux AS
  (SELECT DISTINCT order_id
   FROM order_modifications om
   WHERE om.created_at > now() AT TIME ZONE 'America/{timezone}' - interval '8h'
	 AND ((TYPE IN ('cancel_by_support')
		   AND (params ->> 'cancelation_reason')::text IN ('crsan_client_wants_to_change_order',
														   'crsan_client_doesnt_understand_promotion',
														   'crsan_client_doesnt_have_how_to_pay',
														   'crsan_client_wanted_to_change_address_not_allowed',
														   'crsan_client_doesnt_answer'))
		  OR (TYPE IN ('cancel_by_user')
			  AND (params ->> 'cancelation_reason')::text IN ('order_not_wanted',
															  'desagree_value_charged',
															  'wrong_address',
															  'wrong_payment_method',
															  'wrong_products'))) ),
	 gs AS
  (SELECT ux.order_id,
		  os.store_id::text as store_id,
		  max(os.name) AS store_name,
		  os.order_id AS orders
   FROM order_stores os
   LEFT JOIN ux ON os.order_id=ux.order_id
   WHERE os.type IN ('restaurant')
	 AND os.created_at > now() AT TIME ZONE 'America/{timezone}' - interval '8h'
	 and os.store_id not in (900522392,900522388,900522389,900522390,900522391)
   GROUP BY 1,
			2,
			4)
SELECT store_id,
	   store_name,
	   count(DISTINCT order_id) AS cancels,
	   count(DISTINCT orders) AS orders,
	   count(DISTINCT order_id)::float/count(DISTINCT orders)::float AS percentage,
	   string_agg(distinct order_id::text,',') as order_ids
FROM gs
GROUP BY 1,
		 2
	"""

	if country == 'co':
		metrics = redash.run_query(1904, query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
	elif country == 'mx':
		metrics = redash.run_query(1371, query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)

	return metrics

def store_infos(country):
	timezone, interval = timezones.country_timezones(country)

	query = f'''
	select a.store_id::text as storeid,
	(case when category is null then 'No Category' else category end)::text as category
	from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
	left join (select store_id,
					  store_brand_id, max(category) as category
	from ops_global.brands_tiers
	where lower(country) = '{country}' group by 1,2) t on t.store_id=a.store_id and t.store_brand_id=a.store_brand_id
	'''
	df = snow.run_query(query)
	return df

def new_offenders(a,b,d):
	current_time = timezones.country_current_time(country)
	target_date = (current_time + timedelta(hours=8))
	time_closed = int((target_date - current_time).total_seconds() / 60.0)
	print(target_date)
	print(time_closed)

	threshold_topsellers = {
	'ar': '10',
	'br': '10',
	'cl': '10',
	'co': '20',
	'cr': '10',
	'ec': '10',
	'mx': '15',
	'pe': '20',
	'uy': '5'
	}
	threshold_longtail = {
	'ar': '5',
	'br': '5',
	'cl': '5',
	'co': '5',
	'cr': '5',
	'ec': '5',
	'mx': '5',
	'pe': '5',
	'uy': '5'
	}

	try:
		df = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
		df = df.drop(['key_0'], axis=1)
		print(df)

		df = df[(
			(df['category'].isin(['Platinum','Gold','Silver']) & df['cancels'] >= int(threshold_topsellers[country]))
			| ((~df['category'].isin(['Platinum','Gold','Silver'])) & df['cancels'] >= int(threshold_longtail[country]))
			)]

		print("df")
		print(df)

		news = pd.merge(df,d,how='left',left_on=df['store_id'],right_on=d['store_id'])
		news = news[news['store_id_y'].isnull()]
		news = news.rename(columns={'store_id_x': 'store_id'})

		if not news.empty and df.shape[0] <= 10:
			news = news[["store_id","store_name","orders","cancels","percentage","order_ids","category"]]
			for index, row in news.iterrows():
				rows = dict(row)
				store_id = int(row['store_id'])
				reason = "ux_cancellation"
				cms.disable_store(store_id, country, reason, time_closed)
				cms.turnStoreOff(store_id, time_closed, country, reason) == 200
				text = '''
				*Tienda suspendida - UX Restaurantes - {country}*
				La Tienda {store_id} fue remitida a suspensión hasta {data} por cancelamiento ux
				Store Name: {store_name}
				Category: {category}
				Orders: {orders}
				Cancels: {cancels}
				Percentage: {percentage}
				Orders IDs: {order_ids} '''.format(
					store_id=row['store_id'],
					store_name=row['store_name'],
					category=row['category'],
					cancels=row['cancels'],
					orders=row['orders'],
					order_ids=row['order_ids'],
					percentage=round(row['percentage']*100,2),
					data=target_date.strftime("%m/%d/%Y %H:%M:%S"),
					country=country
					)

				slack.bot_slack(text,'C01S0667VKP')
				print(text)

			news['country'] = country
			news['suspended_at'] = current_time
			news['suspended_until'] = target_date
			news = news[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country']]
			snow.upload_df_occ(news, 'ux_restaurantes')

	except Exception as e:
		print(e)

def run_process(country):
	d = excluding_stores(country)
	a = offenders(country)
	b = store_infos(country)
	new_offenders(a,b,d)

countries = ['br','co','mx','ar','cl','uy','ec','pe','cr']
for country in countries:
	try:
		run_process(country)
	except Exception as e:
		print(e)