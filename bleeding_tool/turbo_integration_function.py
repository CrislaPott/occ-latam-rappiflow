#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# !jupyter nbconvert --to script turbo_integration.ipynb


# In[126]:

import os, sys, json
import numpy as np
import pandas as pd
import pytz
from random import randrange
from datetime import datetime, timedelta
from lib import slack, redash, cms, sfx
from lib import snowflake as snow


# In[40]:


def process(country, canal):

    if country == "BR":
        country = "BR"
        country_min = "br"
        time_zones = "Sao_Paulo"
        cond_co = ""
        cond_co1 = ""
        query = True
        BD = 6310
        flag = "\U0001F1E7\U0001F1F7"

    elif country == "MX":
        country = "MX"
        country_min = "mx"
        time_zones = "Mexico_City"
        cond_co = ""
        cond_co1 = ""
        query = True
        BD = 6324
        flag = "\U0001F1F2\U0001F1FD"

    elif country == "CO":
        country = "CO"
        country_min = "co"
        time_zones = "Bogota"
        cond_co = "_co"
        cond_co1 = "_vw"
        query = True
        BD = 6312
        flag = "\U0001F1E8\U0001F1F4"

    elif country == "AR":
        country = "AR"
        country_min = "ar"
        time_zones = "Buenos_Aires"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6309
        flag = "\U0001F1E6\U0001F1F7"

    elif country == "CL":
        country = "CL"
        country_min = "cl"
        time_zones = "Santiago"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6311
        flag = "\U0001F1E8\U0001F1F1"

    elif country == "PE":
        country = "PE"
        country_min = "pe"
        time_zones = "Lima"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6323
        flag = "\U0001F1F5\U0001F1EA"

    elif country == "UY":
        country = "UY"
        country_min = "uy"
        time_zones = "Montevideo"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6322
        flag = "\U0001F1FA\U0001F1FE"

    elif country == "EC":
        country = "EC"
        country_min = "ec"
        time_zones = "Guayaquil"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6447
        flag = "\U0001F1EA\U0001F1E8"

    elif country == "CR":
        country = "CR"
        country_min = "cr"
        time_zones = "Costa_Rica"
        cond_co = ""
        cond_co1 = ""
        query = False
        BD = 6313
        flag = "\U0001F1E8\U0001F1F7"

    # In[41]:

    log = redash.run_query(
        BD,
        """select store_id::text as storeid, created_at::date as created_at 
   from store_attribute where suspended=true""",
    )
    log["storeid"] = log["storeid"].astype("int64")

    print(log.shape)
    log.head()

    # In[42]:

    if query == True:

        print("init_", country)
        df = snow.run_query(
            """
      WITH I AS
      (
      SELECT RAPPI_STORE_ID, 
      MAX(LAST_INTEGRATION) AS LAST_INTEGRATION
      FROM
         (
         SELECT DISTINCT RAPPI_STORE_ID,
         MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
         FROM CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
         WHERE STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
         GROUP BY 1
         UNION ALL
         SELECT DISTINCT VIRTUAL_STORE_ID AS RAPPI_STORE_ID,
         MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',EXECUTION_DATE)) AS LAST_INTEGRATION
         FROM CPGS_DATASCIENCE.{country}_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
         WHERE TRY_TO_DATE(EXECUTION_DATE)::DATE >= CURRENT_DATE - 7
         GROUP BY 1
         )
      GROUP BY 1
      ),

      BRANDS AS
      (
      SELECT distinct i.country,
      st.brand_group_name as brand,
      s.store_id,
      max(i.integration_name) over (partition by s.store_id) as integration_name
      FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE nds
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE d ON nds.datasource_id = d.id
      JOIN 
         (
         SELECT * 
         FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS 
         WHERE INTEGRATION_NAME not ilike '%bundle%'
         ) i 
      ON i.datasource = d.name
      JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS_METADATA im ON i.id = im.id_integration
      JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATION_VERSIONS iv ON iv.id_integration = i.id
      JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s ON s.store_id = nds.rappi_store_id
      LEFT JOIN
         (
         SELECT *
         FROM 
               (
               SELECT STORE_BRAND_ID,
               STORE_TYPE,
               LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
               LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_NAME
               FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
               JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
               ON BRAND_GROUP_ID = BG.ID
               )
         GROUP BY 1,2,3,4
         ) ST
      ON COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
      LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
      LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
      WHERE coalesce(nds._fivetran_deleted,false) = false
      AND coalesce(d._fivetran_deleted,false) = false
      AND coalesce(s._fivetran_deleted,false) = false
      AND nds.deleted_at is null AND d.deleted_at is null AND s.deleted_at is null
      AND d.country = '{country_min}'
      AND i.country = '{country_min}'
      AND iv.is_enabled = 'TRUE'
      AND iv.current_version = 'TRUE'
      AND t.name <> 'Deactivated'
      ),

      -- orders last 30d
      ORDERS AS
      (
      SELECT S.STORE_ID,
      Count(DISTINCT O.ID) AS TOTAL
      FROM {country}_CORE_ORDERS_PUBLIC.ORDERS{cond_co} O
      INNER JOIN
         (
         SELECT DISTINCT ORDER_ID,
         NAME,
         STORE_ID,
         TYPE
         FROM {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{cond_co1}
         WHERE COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE
         ) OS
      ON O.ID = OS.ORDER_ID
      LEFT JOIN  {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID = OS.STORE_ID
      LEFT JOIN  {country}_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM ON OM.ORDER_ID = O.ID
      WHERE O.STATE NOT IN ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error', 'canceled_by_timeout')
      AND OS.TYPE NOT IN ('grin', 'rappi_prime', 'rentbrella', 'go_taxi')
      AND O.PAYMENT_METHOD != 'synthetic'
      AND S.CITY_ADDRESS_ID != 104
      AND OM.TYPE NOT IN ('canceled_by_early_regret')
      AND COALESCE(O.CLOSED_AT, O.UPDATED_AT):: DATE >= CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE -30
      GROUP BY 1
      ORDER BY 2 DESC
      ),

      EQUIVALENCE AS
      (
      SELECT DISTINCT DR.SPINOFF_ID AS STORE_ID
      FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE DS
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D ON DS.DATASOURCE_ID = D.ID
      LEFT JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF DR ON DR.WAREHOUSE_ID = DS.RAPPI_STORE_ID
      WHERE COUNTRY = '{country_min}' 
      AND COALESCE(DS._FIVETRAN_DELETED,FALSE) = FALSE 
      AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE
      AND DR.SPINOFF_ID IS NOT NULL 
      AND DR._FIVETRAN_DELETED IS NULL
      UNION ALL
      SELECT
      DISTINCT DS.RAPPI_STORE_ID AS STORE_ID
      FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE DS
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D ON DS.DATASOURCE_ID = D.ID
      WHERE COUNTRY = '{country_min}' 
      AND COALESCE(DS._FIVETRAN_DELETED,FALSE) = FALSE 
      AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE 
      AND DS.DELETED_AT IS NULL
      ),

      STORES AS
      (
      SELECT DISTINCT S.STORE_ID,
      S.NAME,
      S.TYPE,
      S.SUPER_STORE_ID,
      S.CREATED_AT,
      S.IS_ENABLED,
      B.NAME AS BRAND_NAME,
      ST.BRAND_GROUP_NAME,
      I.integration_name
      FROM {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
      LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_BRANDS{cond_co1} B
      ON B.ID = S.STORE_BRAND_ID
      LEFT JOIN
         (
         SELECT *
         FROM 
               (
               SELECT STORE_BRAND_ID,
               STORE_TYPE,
               LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
               LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
               FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
               JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
               ON BRAND_GROUP_ID = BG.ID
               )
         GROUP BY 1,2,3,4
         ) ST
      ON COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
      LEFT JOIN BRANDS I ON I.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      WHERE S.DELETED_AT IS NULL
      AND COALESCE (S.SUPER_STORE_ID, S.STORE_ID) IN (SELECT STORE_ID FROM BRANDS)
      AND S._FIVETRAN_DELETED = 'FALSE'
      AND S.CITY_ADDRESS_ID != 104
      AND S.TYPE IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = '{country}')
      ),

      STORES_FINAL AS (
      SELECT DISTINCT S.STORE_ID,
      S.NAME,
      S.SUPER_STORE_ID,
      S.TYPE,
      S.IS_ENABLED,
      S.BRAND_GROUP_NAME,
      S.integration_name,
      I.LAST_INTEGRATION,
      DATEDIFF(HOUR, I.LAST_INTEGRATION::TIMESTAMP_NTZ,
      CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::TIMESTAMP_NTZ) AS DIF,
      COALESCE(ORDERS.TOTAL,0) AS ORDERSLAST30D,
      S.CREATED_AT,
      DATEDIFF(DAY, S.CREATED_AT::DATE, CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE) AS STORE_CREATION,
      KAM,
      E.STORE_ID AS EQUIVALENCE,
      PH.PHYSICAL_STORE_ID,
      PS.PICKED_BY,
      ST.SCHEDULED
      FROM STORES S
      LEFT JOIN I ON I.RAPPI_STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      LEFT JOIN ORDERS ON ORDERS.STORE_ID = S.STORE_ID
      LEFT JOIN EQUIVALENCE E ON E.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      LEFT JOIN {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = S.STORE_ID
      LEFT JOIN       
         (
         SELECT PHYSICAL_STORE_ID, PICKED_BY,
         RANK() OVER (PARTITION BY PHYSICAL_STORE_ID ORDER BY _FIVETRAN_SYNCED DESC) AS RANK
         FROM {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
         ) PS
      ON PS.PHYSICAL_STORE_ID = PH.PHYSICAL_STORE_ID AND RANK = 1
      LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST
      ON ST.ID = S.TYPE
      LEFT JOIN
         (SELECT DISTINCT STORE_ID,
         LAST_VALUE (USER_ID) OVER ( PARTITION BY STORE_ID ORDER BY ID, UPDATED_AT) AS USER_ID
         FROM {country}_PGLR_MS_STORES_PUBLIC.STORE_COMMISSIONS{cond_co1}
         ) SC
      ON SC.STORE_ID = S.STORE_ID
      LEFT JOIN
         (SELECT DISTINCT ID AS USER_ID,
         LAST_VALUE (EMAIL) OVER ( PARTITION BY ID ORDER BY CREATED_AT, UPDATED_AT) AS KAM
         FROM {country}_GRABILITY_PUBLIC.USERS{cond_co1}
         ) PU
      ON PU.USER_ID = SC.USER_ID
      )

      SELECT    
      S.STORE_ID,
      S.LAST_INTEGRATION,
      S.NAME,
      S.integration_name,
      S.BRAND_GROUP_NAME,
      S.TYPE,
      S.PHYSICAL_STORE_ID,
      S.PICKED_BY,
      S.SCHEDULED AS HAS_SLOTS,
      COALESCE(S.SUPER_STORE_ID, S.STORE_ID) AS TIENDA_QUE_INTEGRA,
      SP.SCHEDULED AS HAS_SLOTS_TIENDA_QUE_INTEGRA,
      KAM,
      S.IS_ENABLED,
      CASE WHEN TIENDA_QUE_INTEGRA IN (SELECT DISTINCT SPINOFF_ID FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF)
      THEN 'TRUE'
      ELSE 'FALSE'
      END AS SPIN_OFF
      FROM STORES_FINAL S
      LEFT JOIN 
         (
         SELECT DISTINCT S.STORE_ID, S.TYPE, SS.SCHEDULED
         FROM {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
         LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS ON SS.ID = S.TYPE
         WHERE S.SUPER_STORE_ID IS NULL
         ) SP
      ON SP.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      WHERE S.STORE_ID IN
         (
         SELECT DISTINCT STORE_ID
         FROM STORES_FINAL
         WHERE (DIF IS NULL OR DIF >= 3)
         AND (ORDERSLAST30D >= 1 OR STORE_CREATION <= 7)
         )
      """.format(
                country=country,
                timezone=time_zones,
                cond_co=cond_co,
                cond_co1=cond_co1,
                country_min=country_min,
            )
        )

    else:

        print("init_", country)

        df = snow.run_query(
            """
      WITH I AS
      (
      SELECT DISTINCT RAPPI_STORE_ID,
      MAX(CONVERT_TIMEZONE('UTC','America/{timezone}',STOCKER_EXECUTION_DATE)) AS LAST_INTEGRATION
      FROM CPGS_DATASCIENCE.{country}_INTEGRATIONS_LAST_EVENT
      WHERE STOCKER_EXECUTION_DATE::DATE >= CURRENT_DATE - 7
      GROUP BY 1
      ),
      
      BRANDS AS
      (
      SELECT
      distinct i.country,
      st.brand_group_name as brand,
      s.store_id,
      max(i.integration_name) over (partition by s.store_id) as integration_name
      FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE nds
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE d ON nds.datasource_id = d.id
      JOIN 
         (
         SELECT * 
         FROM CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS 
         WHERE INTEGRATION_NAME not ilike '%bundle%'
         ) i 
      ON i.datasource = d.name
      JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATIONS_METADATA im ON i.id = im.id_integration
      JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.INTEGRATION_VERSIONS iv ON iv.id_integration = i.id
      JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s ON s.store_id = nds.rappi_store_id
      LEFT JOIN
         (
         SELECT *
         FROM     
               (
               SELECT STORE_BRAND_ID,
               STORE_TYPE,
               LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
               LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_NAME
               FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
               JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
               ON BRAND_GROUP_ID = BG.ID
               )
         GROUP BY 1,2,3,4
         ) ST
      ON COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
      LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.integration_tags it on it.id_integration = i.id
      LEFT JOIN CO_PG_MS_CPGS_INT_CONFIG_API_PUBLIC.tags t on t.id = it.id_tag
      WHERE coalesce(nds._fivetran_deleted,false) = false
      AND coalesce(d._fivetran_deleted,false) = false
      AND coalesce(s._fivetran_deleted,false) = false
      AND nds.deleted_at is null 
      AND d.deleted_at is null 
      AND s.deleted_at is null
      AND d.country = '{country_min}'
      AND i.country = '{country_min}'
      AND iv.is_enabled = 'TRUE'
      AND iv.current_version = 'TRUE'
      AND t.name <> 'Deactivated'
      ),

      -- orders last 30d
      ORDERS AS
      (
      SELECT S.STORE_ID,
      Count(DISTINCT O.ID) AS TOTAL
      FROM {country}_CORE_ORDERS_PUBLIC.ORDERS{cond_co} O
      INNER JOIN
         (
         SELECT DISTINCT ORDER_ID,
         NAME,
         STORE_ID,
         TYPE
         FROM {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{cond_co1}
         WHERE COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE
         ) OS
      ON O.ID = OS.ORDER_ID
      LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S ON S.STORE_ID = OS.STORE_ID
      LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_MODIFICATIONS OM ON OM.ORDER_ID = O.ID
      WHERE O.STATE NOT IN ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error', 'canceled_by_timeout')
      AND OS.TYPE NOT IN ('grin', 'rappi_prime', 'rentbrella', 'go_taxi')
      AND O.PAYMENT_METHOD != 'synthetic'
      AND S.CITY_ADDRESS_ID != 104
      AND OM.TYPE NOT IN ('canceled_by_early_regret')
      AND COALESCE(O.CLOSED_AT, O.UPDATED_AT):: DATE >= CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE -30
      GROUP BY 1
      ORDER BY 2 DESC
      ),
      
      EQUIVALENCE AS
      (
      SELECT
      DISTINCT DR.SPINOFF_ID AS STORE_ID
      FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE DS
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D ON DS.DATASOURCE_ID = D.ID
      LEFT JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF DR ON DR.WAREHOUSE_ID = DS.RAPPI_STORE_ID
      WHERE COUNTRY = '{country_min}' 
      AND COALESCE(DS._FIVETRAN_DELETED,FALSE) = FALSE 
      AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE
      AND DR.SPINOFF_ID IS NOT NULL 
      AND DR._FIVETRAN_DELETED IS NULL
      UNION ALL
      SELECT
      DISTINCT DS.RAPPI_STORE_ID AS STORE_ID
      FROM
      CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE DS
      JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D ON DS.DATASOURCE_ID = D.ID
      WHERE COUNTRY = '{country_min}' 
      AND COALESCE(DS._FIVETRAN_DELETED,FALSE) = FALSE 
      AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE 
      AND DS.DELETED_AT IS NULL
      ),
      
      STORES AS
      (
      SELECT DISTINCT S.STORE_ID,
      S.NAME,
      S.TYPE,
      S.SUPER_STORE_ID,
      S.CREATED_AT,
      S.IS_ENABLED,
      B.NAME AS BRAND_NAME,
      ST.BRAND_GROUP_NAME,
      I.integration_name
      FROM {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
      LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_BRANDS{cond_co1} B ON B.ID = S.STORE_BRAND_ID
      LEFT JOIN
         (
         SELECT   *
         FROM     
               (
               SELECT STORE_BRAND_ID,
               STORE_TYPE,
               LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
               LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_NAME
               FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
               JOIN {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
               ON BRAND_GROUP_ID = BG.ID
               )
         GROUP BY 1,2,3,4) ST
      ON COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
      LEFT JOIN BRANDS I
      ON I.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      WHERE S.DELETED_AT IS NULL
      AND COALESCE (S.SUPER_STORE_ID, S.STORE_ID) IN (SELECT STORE_ID FROM BRANDS)
      AND S._FIVETRAN_DELETED = 'FALSE'
      AND S.CITY_ADDRESS_ID != 104
      AND S.TYPE IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = '{country}')
      ),
      
      STORES_FINAL AS 
      (
      SELECT DISTINCT S.STORE_ID,
      S.NAME,
      S.SUPER_STORE_ID,
      S.TYPE,
      S.IS_ENABLED,
      S.BRAND_GROUP_NAME,
      S.integration_name,
      I.LAST_INTEGRATION,
      DATEDIFF(HOUR, I.LAST_INTEGRATION::TIMESTAMP_NTZ,
      CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::TIMESTAMP_NTZ) AS DIF,
      COALESCE(ORDERS.TOTAL,0) AS ORDERSLAST30D,
      S.CREATED_AT,
      DATEDIFF(DAY, S.CREATED_AT::DATE, CONVERT_TIMEZONE('America/{timezone}',CURRENT_TIMESTAMP())::DATE) AS STORE_CREATION,
      KAM,
      E.STORE_ID AS EQUIVALENCE,
      PH.PHYSICAL_STORE_ID,
      PS.PICKED_BY,
      ST.SCHEDULED
      FROM STORES S
      LEFT JOIN I ON I.RAPPI_STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      LEFT JOIN ORDERS ON ORDERS.STORE_ID = S.STORE_ID
      LEFT JOIN EQUIVALENCE E ON E.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      LEFT JOIN {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = S.STORE_ID
      LEFT JOIN 
         (
         SELECT PHYSICAL_STORE_ID, PICKED_BY,
         RANK() OVER (PARTITION BY PHYSICAL_STORE_ID ORDER BY _FIVETRAN_SYNCED DESC) AS RANK
         FROM {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.CONFIGS_PHYSICAL_STORES
         ) PS
      ON PS.PHYSICAL_STORE_ID = PH.PHYSICAL_STORE_ID AND RANK = 1
      LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES ST ON ST.ID = S.TYPE
      LEFT JOIN
         (
         SELECT DISTINCT STORE_ID, LAST_VALUE (USER_ID) OVER ( PARTITION BY STORE_ID ORDER BY ID, UPDATED_AT) AS USER_ID
         FROM {country}_PGLR_MS_STORES_PUBLIC.STORE_COMMISSIONS{cond_co1}
         ) SC
      ON SC.STORE_ID = S.STORE_ID
      LEFT JOIN
         (
         SELECT DISTINCT ID AS USER_ID,
         LAST_VALUE (EMAIL) OVER ( PARTITION BY ID ORDER BY CREATED_AT, UPDATED_AT ) AS KAM
         FROM {country}_GRABILITY_PUBLIC.USERS{cond_co1}
         ) PU
         ON PU.USER_ID = SC.USER_ID
      )

      SELECT    
      S.STORE_ID,
      S.LAST_INTEGRATION,
      S.NAME,
      S.integration_name,
      S.BRAND_GROUP_NAME,
      S.TYPE,
      S.PHYSICAL_STORE_ID,
      S.PICKED_BY,
      S.SCHEDULED AS HAS_SLOTS,
      COALESCE(S.SUPER_STORE_ID, S.STORE_ID) AS TIENDA_QUE_INTEGRA,
      SP.SCHEDULED AS HAS_SLOTS_TIENDA_QUE_INTEGRA,
      KAM,
      S.IS_ENABLED,
      CASE WHEN TIENDA_QUE_INTEGRA IN (SELECT DISTINCT SPINOFF_ID FROM {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF)
      THEN 'TRUE'
      ELSE 'FALSE'
      END AS SPIN_OFF
      FROM STORES_FINAL S
      LEFT JOIN 
         (
         SELECT DISTINCT S.STORE_ID, S.TYPE, SS.SCHEDULED
         FROM {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
         LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
         ON SS.ID = S.TYPE
         WHERE S.SUPER_STORE_ID IS NULL
         ) SP
      ON SP.STORE_ID = COALESCE(S.SUPER_STORE_ID, S.STORE_ID)
      WHERE S.STORE_ID IN
         (
         SELECT DISTINCT STORE_ID
         FROM STORES_FINAL
         WHERE (DIF IS NULL OR DIF >= 3)
         AND (ORDERSLAST30D >= 1 OR STORE_CREATION <= 7)
         )
      """.format(
                country=country,
                timezone=time_zones,
                cond_co=cond_co,
                cond_co1=cond_co1,
                country_min=country_min,
            )
        )

    print("end_", country)
    print(df.shape)
    df.head()

    # In[43]:

    if len(df) > 0:

        df1 = pd.merge(df, log, how="left", left_on=["store_id"], right_on=["storeid"])
        df1 = df1[df1["storeid"].isnull()]
        df1 = df1.drop(["storeid"], axis=1)

        print(df1.shape)
        df1.head()

    # In[ ]:

    current_time = datetime.now(pytz.timezone("America/Bogota"))
    print(current_time.strftime("%H:%M"))

    if (
        current_time.strftime("%H:%M") >= "07:00"
        and current_time.strftime("%H:%M") <= "19:00"
    ):
        target_date = current_time + timedelta(minutes=180)
        time_closed = int((target_date - current_time).total_seconds() / 60.0)

        print(target_date)
        print(time_closed)

    # In[56]:

    canal_slack = canal
    if len(df) > 0:

        to_upload = df1
        to_upload["country"] = country_min
        to_upload["suspended_at"] = current_time.strftime("%Y-%m-%d %H:%M:%S")
        snow.upload_df_occ(to_upload,'turbo_no_integration')

        for index, row in df1.iterrows():
            rows = dict(row)
            store_id = str(row["store_id"])
            reason = "turbo_no_integration"
            suspended_at = current_time.strftime("%Y-%m-%d %H:%M:%S")
            cms.turnStoreOff(store_id, time_closed+2, country_min, reason) == 200
            cms.disable_store(store_id, country_min, reason, time_closed) == 200
            sfx.send_events_turnStoreOff(store_id, country, reason, suspended_at)
            main_text = """
         *Tienda suspendida - Proceso Turbo integration - {flag}*:
         <!subteam^S025TJHN0DN>
         La Tienda {store_id} fue remitida a suspensión hasta {target_date} COT
         Duración suspensión: 3h
         Store Name: {store_name}
         Store Type: {store_type}
         Integration Name: {integration_name}
                """.format(
                flag=flag,
                store_id=row["store_id"],
                store_name=row["name"],
                target_date=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                store_type=row["type"],
                integration_name=row["integration_name"],
            )

            print(main_text)
            slack.bot_slack(main_text, canal_slack)

