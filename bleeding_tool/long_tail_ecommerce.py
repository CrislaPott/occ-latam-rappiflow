import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, sfx, cms
from lib import snowflake as snow
from functions import timezones
import time
import pytz
from lib import gsheets

df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Ecommerce Weekly')
df = df[(df["ON/OFF"] == 'ACTIVE')]
countries = df['COUNTRY'].to_list()
print(countries)

def suspended_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::text as storeid, created_at::date as created_at from store_attribute
    where suspended=true
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)
    return metrics

def offenders():
    query = '''
with weekM3 as (

select

go.country,
go.store_id,
go.store_name,
go.brand_group,
1 as weekM3,
count(distinct go.order_id) as total_orders,
count(distinct case when go.count_to_gmv then go.order_id else null end) financial_orders,
coalesce(sum(case when go.count_to_gmv then go.gmv_usd else null end),0) financial_gmv,
coalesce( count(distinct case when go.state_type like ('%canceled%')  then go.order_id else null end),0) as canceled_orders,
(canceled_orders / total_orders) as Can_R

from ops_global.global_orders go
left join (select distinct id, country, acuerdo from GLOBAL_ECOMMERCE.TBL_CANPENTAS) b on go.order_id = b.id and go.country = b.country

where date_trunc(day,go.created_at) >= date_trunc(day,current_date) - interval '22 day'
and date_trunc(day,go.created_at) < date_trunc(day,current_date) - interval '15 day'
--go.created_at >= date_trunc('week', current_date) - interval '1 month'
and go.vertical_group = 'ECOMMERCE'
and go.vertical_sub_group not in ('PETS', 'SERVICES')
and b.acuerdo not in ('LN', 'RUSA')
and go.store_type not ilike'%siniva%'

group by 1,2,3,4,5
having total_orders between 1 and 5 and Can_R >= 0.3

    ),

weekM2 as (

select

go.country,
go.store_id,
go.store_name,
go.brand_group,
1 as weekM2,
count(distinct go.order_id) as total_orders,
count(distinct case when go.count_to_gmv then go.order_id else null end) financial_orders,
coalesce(sum(case when go.count_to_gmv then go.gmv_usd else null end),0) financial_gmv,
coalesce( count(distinct case when go.state_type like ('%canceled%')  then go.order_id else null end),0) as canceled_orders,
(canceled_orders / total_orders) as Can_R

from ops_global.global_orders go
left join (select distinct id, country, acuerdo from GLOBAL_ECOMMERCE.TBL_CANPENTAS) b on go.order_id = b.id and go.country = b.country

where date_trunc(day,go.created_at) >= date_trunc(day,current_date) - interval '15 day'
and date_trunc(day,go.created_at) < date_trunc(day,current_date) - interval '8 day'
--go.created_at >= date_trunc('week', current_date) - interval '1 month'
and go.vertical_group = 'ECOMMERCE'
and go.vertical_sub_group not in ('PETS', 'SERVICES')
and b.acuerdo not in ('LN', 'RUSA')
and go.store_type not ilike'%siniva%'

group by 1,2,3,4,5
having total_orders between 1 and 5 and Can_R >= 0.3

),

weekM1 as (

select

go.country,
go.store_id,
go.store_name,
go.brand_group,
1 as weekM1,
count(distinct go.order_id) as total_orders,
count(distinct case when go.count_to_gmv then go.order_id else null end) financial_orders,
coalesce(sum(case when go.count_to_gmv then go.gmv_usd else null end),0) financial_gmv,
coalesce( count(distinct case when go.state_type like ('%canceled%')  then go.order_id else null end),0) as canceled_orders,
(canceled_orders / total_orders) as Can_R

from ops_global.global_orders go
left join (select distinct id, country, acuerdo from GLOBAL_ECOMMERCE.TBL_CANPENTAS) b on go.order_id = b.id and go.country = b.country

where date_trunc(day,go.created_at) >= date_trunc(day,current_date) - interval '8 day'
and date_trunc(day,go.created_at) < date_trunc(day,current_date)
--go.created_at >= date_trunc('week', current_date) - interval '1 month'
and go.vertical_group = 'ECOMMERCE'
and go.vertical_sub_group not in ('PETS', 'SERVICES')
and b.acuerdo not in ('LN', 'RUSA')
and go.store_type not ilike'%siniva%'

group by 1,2,3,4,5
having total_orders between 1 and 5 and Can_R >= 0.3

),

lastworders as (

select

go.country,
go.store_id,
go.store_name,
go.brand_group,
count(distinct go.order_id) as total_orders,
count(distinct case when go.count_to_gmv then go.order_id else null end) financial_orders,
coalesce(sum(case when go.count_to_gmv then go.gmv_usd else null end),0) financial_gmv,
coalesce( count(distinct case when go.state_type like ('%canceled%')  then go.order_id else null end),0) as canceled_orders,
listagg(distinct case when go.state_type like ('%canceled%')  then go.order_id else null end, ', ') as order_cancels,
(canceled_orders / total_orders) as Can_R

from ops_global.global_orders go
left join (select distinct id, country, acuerdo from GLOBAL_ECOMMERCE.TBL_CANPENTAS) b on go.order_id = b.id and go.country = b.country

where go.created_at >= date_trunc(day, current_date) - interval '1 month'
and go.vertical_group = 'ECOMMERCE'
and go.vertical_sub_group not in ('PETS', 'SERVICES')
and b.acuerdo not in ('LN', 'RUSA')
and go.store_type not ilike'%siniva%'

group by 1,2,3,4

),

emails as (

select 'CO' as country,
store_id,
contact_full_name,
phone,
contact_email
    from CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'BR' as country,
store_id,
contact_full_name,
phone,
contact_email
    from BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'AR' as country,
store_id,
contact_full_name,
phone,
contact_email
    from AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'MX' as country,
store_id,
contact_full_name,
phone,
contact_email
    from MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'CL' as country,
store_id,
contact_full_name,
phone,
contact_email
    from CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'EC' as country,
store_id,
contact_full_name,
phone,
contact_email
    from EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'CR' as country,
store_id,
contact_full_name,
phone,
contact_email
    from CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'PE' as country,
store_id,
contact_full_name,
phone,
contact_email
    from PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

    union all

select 'UY' as country,
store_id,
contact_full_name,
phone,
contact_email
    from UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES where not coalesce(_fivetran_deleted,false)

)

select

weekM3.country,
weekM3.store_id,
weekM3.store_name,
weekM3.brand_group,
weekM3.weekM3,
coalesce(weekM2.weekM2,0) as WeekM2,
coalesce(weekM1.weekM1,0) as WeekM1,
lwo.total_orders,
lwo.financial_orders,
lwo.financial_gmv,
lwo.canceled_orders,
lwo.order_cancels,
lwo.can_r,
coalesce(e.contact_full_name, 'aliado') as contact_full_name,
e.phone,
e.contact_email

from weekM3
left join weekm2 on weekm3.country = weekm2.country and weekm3.store_id = weekm2.store_id
left join weekm1 on weekm3.country = weekm1.country and weekm3.store_id = weekm1.store_id
left join lastworders lwo on lwo.country = weekM3.country and lwo.store_id = weekM3.store_id
left join emails e on e.country = weekM3.country and e.store_id = weekM3.store_id

join (SELECT distinct country, store_id FROM (

    select 'CO' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.CO_VERTICALS_V2 v join CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'BR' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.BR_VERTICALS_V2 v join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'MX' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.MX_VERTICALS_V2 v join MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'AR' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.AR_VERTICALS_V2 v join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'CL' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.CL_VERTICALS_V2 v join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'CR' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.CR_VERTICALS_V2 v join CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'EC' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.EC_VERTICALS_V2 v join EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'PE' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.PE_VERTICALS_V2 v join PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
    UNION ALL
    select 'UY' as country, S.STORE_id as store_id, s.name, v.store_type from VERTICALS_LATAM.UY_VERTICALS_V2 v join UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date  - interval '2 months'
 )
     ) as s on weekm3.country = s.country and weekm3.store_id = s.store_id


where
weekm2 = 1
and weekm1 = 1
    '''
    df = snow.run_query(query)
    return df

def new_offenders(a,b):
    current_time = timezones.country_current_time(country)
    a = a[['country', 'store_id', 'store_name', 'brand_group','weekm3', 'weekm2',
           'weekm1', 'total_orders', 'financial_orders', 'financial_gmv',
           'canceled_orders','order_cancels','can_r']].rename(columns={'can_r':"percentage",
                                                                       'order_cancels':"order_ids",
                                                                       'canceled_orders':'cancels',
                                                                       'total_orders':'orders'})
    if not a.empty:
        a['country'] = a['country'].str.lower()
        target_date = current_time + timedelta(days=7)
        time_closed = int((target_date - current_time).total_seconds() / 60.0)
        print(target_date)
        print(time_closed)
        target_date1 = target_date.strftime("%d/%m/%Y")
        try:
            a['store_id'] = a['store_id'].astype(int)
            b['storeid'] = b['storeid'].astype(int)
            news = pd.merge(a, b, how='left', left_on=a['store_id'], right_on=b['storeid'])
            news = news[news['storeid'].isnull()]
            news = news[news.percentage >= 0.3]
            print(news)
            if not news.empty:
                news = news[["store_id","store_name",'brand_group',"orders","cancels","percentage","order_ids"]]
                for index, row in news.iterrows():
                    rows = dict(row)
                    store_id = row['store_id']
                    reason = "occ_ecomm_weekly"
                    cms.disable_store(int(store_id), country, reason, time_closed)
                    cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
                    try:
                        sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
                    except:
                        print("fail sfx")
                    text2 = '''
                *Tienda suspendida - Processo Ecommerce * :alert:
                Pais: {country} :flag-{country}:
                La Tienda *{store_id}* fue remitida a suspensión hasta {target_date1} por completar 3 semanas seguidas cancelando 30% o más de sus órdenes, com máximo de 5 órdenes por semana.
                Store Name: {store_name}
                Brand Group: {brand_group}
                Total Orders: {orders}
                Cancels: {cancels}
                Cancel Rate: {percentage}%
                Orders IDs: {order_ids} 
                    '''.format(
                        store_id=row['store_id'],
                        store_name=row['store_name'],
                        cancels=row['cancels'],
                        orders=row['orders'],
                        order_ids=row['order_ids'],
                        percentage=round(row['percentage']*100,2),
                        brand_group = row['brand_group'],
                        data=current_time.date(),
                        country=country,
                        target_date1=target_date1
                        )

                    time.sleep(10)
                    slack.bot_slack(text2,'C01S0667VKP')
                    print(text2)

                news['country'] = country
                news['suspended_at'] = current_time
                news['suspended_until'] = target_date.date()
                news = news[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country']]
                print(news)

                snow.upload_df_occ(news, 'ecommerce_weekly')
            else:
                print('news null')

        except Exception as e:
            print(e)
    else:
        print('offenders null')

def run_alarm(country):
    b = suspended_stores(country)
    a = df_offenders[df_offenders.country == country.upper()]
    new_offenders(a,b)

print('run offenders')
df_offenders = offenders()

for country in countries:
    country = country.lower()
    try:
        print(country)
        run_alarm(country)
    except Exception as e:
        print(e)
