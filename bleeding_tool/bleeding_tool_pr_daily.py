import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
# , sfx, cms
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
from lib import gsheets
from time import sleep




## country_list 

df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Worst Offenders Bleeding')
df = df[(df["ON/OFF"] == 'ACTIVE')]
df.top_seller_cancels = df.top_seller_cancels.astype(float)
df.top_seller_cancel_rate = df.top_seller_cancel_rate.astype(float)
df.top_seller_unique_users = df.top_seller_unique_users.astype(float)
df.long_tail_cancels = df.long_tail_cancels.astype(float)
df.long_tail_cancel_rate = df.long_tail_cancel_rate.astype(float)
df.long_tail_unique_users = df.long_tail_unique_users.astype(float)
countries = df['COUNTRY'].unique()
# countries = ['BR']
print(countries)


df['top_seller_cancels'] = 5
df['top_seller_cancel_rate'] = 0.15
df['top_seller_unique_users'] = 0
df['long_tail_cancels'] = 5
df['long_tail_cancel_rate'] = 0.15
df['long_tail_unique_users'] = 0



def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = """select listagg(store_id,',') storeid  from ops_occ.partner_related_test_bleeding_daily
where country = '{country}'
and suspended_at::DATE = convert_timezone('America/{timezone}',current_date)::date
""".format(country=country,timezone=timezone)
    stores = snow.run_query(query)
    stores['check'] = 'check'
    stores['storeid'] = stores['storeid'].astype(str)
    return stores


def excluding_stores_ab(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    select 
    storeid, 
    coalesce(last_percentage,0)::float as last_percentage,
    datediff(minutes, last_suspended_at::timestamp_ntz,convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz)::int as diff
    
    from (
    select 
    store_id::text as storeid, 
    last_value(percentage) over (partition by store_id order by suspended_at asc) as last_percentage,
    last_value(suspended_at) over (partition by store_id order by suspended_at asc) as last_suspended_at
    
    from ops_occ.worst_offenders_bleeding
    where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    ) 
    group by 1,2,3

    UNION ALL

    select 
    storeid, 
    coalesce(last_percentage,0)::float as last_percentage,
    datediff(minutes, last_suspended_at::timestamp_ntz,convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz)::int as diff
    
    from (
    select 
    store_id::text as storeid, 
    last_value(percentage) over (partition by store_id order by suspended_at asc) as last_percentage,
    last_value(suspended_at) over (partition by store_id order by suspended_at asc) as last_suspended_at
    
    from ops_occ.partner_related_test_bleeding
    where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    ) 
    group by 1,2,3
    '''
    df = snow.run_query(query)
    return df

def excluding_stores_cd(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    select 
    storeid, 
    coalesce(last_percentage,0)::float as last_percentage,
    datediff(minutes, last_suspended_at::timestamp_ntz,convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz)::int as diff
    
    from (
    select 
    store_id::text as storeid, 
    last_value(percentage) over (partition by store_id order by suspended_at asc) as last_percentage,
    last_value(suspended_at) over (partition by store_id order by suspended_at asc) as last_suspended_at
    
    from ops_occ.worst_offenders_bleeding
    where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    ) 
    group by 1,2,3


    UNION ALL

        select 
    storeid, 
    coalesce(last_percentage,0)::float as last_percentage,
    datediff(minutes, last_suspended_at::timestamp_ntz,convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz)::int as diff
    
    from (
    select 
    store_id::text as storeid, 
    last_value(percentage) over (partition by store_id order by suspended_at asc) as last_percentage,
    last_value(suspended_at) over (partition by store_id order by suspended_at asc) as last_suspended_at
    
    from ops_occ.partner_related_test_bleeding
    where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    ) 
    group by 1,2,3
    '''
    df = snow.run_query(query)
    return df

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    # if not orders_to_ignore:
    #     comment = '--'
    # else:
    #     comment = ''


    query = """
    --no_cache
    SET TIMEZONE TO 'America/{timezone}';
    with base as (
    SELECT distinct order_id, 
    max(created_at) as finished_at, 
    max(case when type ='cancel_by_user' then 'cancel_by_user' else 'others' end) as cancel_type
    FROM order_modifications om
    WHERE om.created_at::date = current_date
    AND (TYPE IN ('close_order','arrive','finish_order','close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') 
    OR TYPE ILIKE '%cancel%')
    and not (type in ('cancel_by_user') and (params->>'cancelation_reason') ilike '%other%')
    and om.created_at >= now()::date
    group by 1
    )
    select os.store_id::text as store_id,
    count(distinct o.id) as orders,
    count(distinct case when o.state ilike 'cancel_order_by_picker' then o.id else null end) cancels,
    count(distinct case when o.state ilike 'cancel_order_by_picker' then o.application_user_id else null end) as unique_users,
    string_agg(distinct case when o.state ilike 'cancel_order_by_picker' then o.id::text else null end,',') as order_ids
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthetic%'
    inner join order_stores os on os.order_id = o.id and (contact_email not like '%@synth.rappi.com' or contact_email is null)
    where 
    o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
    and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
    and (os.type not in ('rappi_pay','queue_order_flow','soat_webview') or os.type is null)
    and os.type not ilike '%siniva%'
    and os.type not in ('rappi_pay','queue_order_flow','soat_webview', 'casino_siniva_tvs_nc','casino_siniva_nc',
    'agaval_siniva','airpods_siniva','alca_siniva','comprandoando_siniva_nc','electrojaponesa_siniva','fantasiaelectronica_siniva',
    'flamingo_siniva','innovar_siniva','mac_center_watch','iphonexsmax_mc_siniva','ishop_accesorios','kitchenaid_siniva',
    'ktienda_siniva_nc','mansion_siniva','samsung_mundial_siniva_nc','haceb_tech_siniva_nc','exclusiv_siniva_nc',
    'samsung_celulares_siniva_nc','sony_nuevo_siniva','twido_colombia_siniva_nc','smartbuy_siniva_nc')
    and os.name not ilike '%dummy%'
    and (not(cancel_type='cancel_by_user'and extract(epoch from finished_at - o.created_at)/60 < 5))
    group by 1
        """.format(timezone=timezone,country=country)

    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)

    return metrics

def store_infos(country):
    timezone, interval = timezones.country_timezones(country)

    query_slots = f'''
    with ord as (
    select store_id::text as store_id,
    count(distinct order_id) orders
    from OPS_GLOBAL.global_orders
    where lower(country) = '{country}' and created_at::date >= current_date::date -7
    group by 1
    having orders >= 1)
, final as (
    select s.store_id::text as store_id, ps.physical_store_id::text as physical_store_id,
           ps2.name as physical_store_name,
           v.vertical,
           category,
           coalesce(orders,0) as orders_,
           s.city_address_id
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    join {country}_PGLR_MS_STORES_PUBLIC.store_types st on st.id=s.type
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores ps on ps.store_id=s.store_id
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps2 on ps2.id=ps.physical_store_id
    join (select store_type as storetype,
                                   case
                                       when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                       when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                       when vertical_group = 'WHIM' then 'Antojos'
                                       when vertical_group = 'RAPPICASH' then 'RappiCash'
                                       when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                       when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                       when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
                                       when upper (store_type) in ('TURBO') then 'Turbo'
                                       when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
                                       when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                       when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                       when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                       when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                       when upper(vertical_group) in ('CPGS') then 'CPGs'
                                       else vertical_sub_group end as vertical
          from VERTICALS_LATAM.{country}_VERTICALS_V2 v
       ) v on v.storetype = s.type
    left join (
    select * from
    (select store_brand_id,
            last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
            last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
     from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
     join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
     group by 1,2,3
               ) bg on bg.store_brand_id = s.store_brand_id
    left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=bg.brand_group_id
                                           and c.city_address_id=s.city_address_id
                                           and c.COUNTRY='{country.upper()}'
                                           and c.vertical=(case when v.vertical in ('Mercados') then 'Super/Hiper' else v.vertical end)
    left join ord o on o.store_id=s.store_id
    where
    s.type not ilike '%siniva%'
    and s.type not in ('rappi_pay','queue_order_flow','soat_webview', 'casino_siniva_tvs_nc','casino_siniva_nc',
    'agaval_siniva','airpods_siniva','alca_siniva','comprandoando_siniva_nc','electrojaponesa_siniva','fantasiaelectronica_siniva',
    'flamingo_siniva','innovar_siniva','mac_center_watch','iphonexsmax_mc_siniva','ishop_accesorios','kitchenaid_siniva',
    'ktienda_siniva_nc','mansion_siniva','samsung_mundial_siniva_nc','haceb_tech_siniva_nc','exclusiv_siniva_nc',
    'samsung_celulares_siniva_nc','sony_nuevo_siniva','twido_colombia_siniva_nc','smartbuy_siniva_nc')
    and v.vertical in ('Express','Farmacia','Licores','Specialized','Ecommerce')
    and s.deleted_at is null
    group by 1,2,3,4,5,6,7)
, physical as (select physical_store_id, mode(category) as category, mode(vertical) as vertical, mode(city_address_id) as city_address_id from final group by 1)
    select store_id,
           p.physical_store_id,
           physical_store_name,
           p.city_address_id,
           case when p.vertical in ('Licores','Express','Farmacia','Specialized') then 'CPGs Now' else p.vertical end as vertical,
           p.category,
           orders_
    from physical p
    join final f on f.physical_store_id=p.physical_store_id
    group by 1,2,3,4,5,6,7
    '''
    slots = snow.run_query(query_slots)

    return slots

def suspendidas(country):
    query = '''
    select store_id
    from store_attribute
    where suspended=true
    '''

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics

def new_offenders(logsab, logscd, metrics, slots, lista, df_sheets,stores_to_ignore):

    print("df")
    current_time = timezones.country_current_time(country)

    metrics['store_id'] = metrics['store_id'].astype(str)
    slots['store_id'] = slots['store_id'].astype(str)

    news = pd.merge(metrics, slots, how='inner', left_on=metrics['store_id'], right_on=slots['store_id'])
    df = news.drop(['key_0', 'store_id_y'], axis=1).rename(columns={'store_id_x': 'store_id'})

    df['city_address_id'] = df['city_address_id'].astype(str)
    df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)

    df = pd.merge(df, df_sheets[['CITY_ID','CITY_NAME']], how='inner', left_on=df['city_address_id'], right_on=df_sheets['CITY_ID'])
    df.category = df.category.fillna('D')
    print(df)

    ### agrupar o df por physical_id

    df1 = df.groupby(["physical_store_id","physical_store_name",'city_address_id',"vertical","category"])[["cancels","orders","unique_users"]].sum().reset_index()
    df1['percentage'] = ((df1['cancels'] / df1['orders']))

    df_cancels_ids = df[(df['cancels'] >= 1)]

    df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(name='order_ids')
    df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
    df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_1')

    df1['physical_store_id'] = df1['physical_store_id'].astype(str)
    df2['physical_store_id'] = df2['physical_store_id'].astype(str)

    df = pd.merge(df1,df2, how='left', on=['physical_store_id'])
    df = pd.merge(df,df3, how='left', on=['physical_store_id'])

    try:
        slots = slots[['physical_store_id','store_id']]
        df5 = pd.merge(df,slots,how='inner',on=['physical_store_id'])
        print(df5)
        df5 = df5.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_2')
        df = pd.merge(df,df5, how='left',on=['physical_store_id'])
    except:
        df = df
        df['store_ids_2'] = ''

    df = df.rename(columns={'physical_store_id': 'store_id', 'physical_store_name': 'store_name'})
    print(df)

    dfab = df[(df['category'] == 'A') | (df['category'] == 'B')]
    dfcd = df[(df['category'] == 'C') | (df['category'] == 'D')]
    print('dfab')
    print('dfcd')
    dfab = pd.merge(dfab, df_sheets[['CITY_ID', 'top_seller_cancels', 'top_seller_cancel_rate',
                        'top_seller_unique_users']], how='left', left_on=dfab['city_address_id'],
                  right_on=df_sheets['CITY_ID'])
    dfcd = pd.merge(dfcd, df_sheets[['CITY_ID', 'long_tail_cancels', 'long_tail_cancel_rate',
                        'long_tail_unique_users']], how='left', left_on=dfcd['city_address_id'],
                    right_on=df_sheets['CITY_ID'])

    dfab = dfab[(dfab['cancels'] >= dfab['top_seller_cancels']) &
                (dfab['percentage'] >= dfab['top_seller_cancel_rate']) &
                (dfab['unique_users'] >= dfab['top_seller_unique_users'])]

    dfcd = dfcd[(dfcd['cancels'] >= dfcd['long_tail_cancels']) &
                (dfcd['percentage'] >= dfcd['long_tail_cancel_rate']) &
                (dfcd['unique_users'] >= dfcd['long_tail_unique_users'])]
    print(dfab)
    print(dfcd)

    dfab = dfab[['store_id', 'store_name', 'city_address_id', 'vertical', 'category', 'cancels', 'orders', 'unique_users',
                 'percentage', 'order_ids', 'store_ids_1', 'store_ids_2']]
    dfcd = dfcd[['store_id', 'store_name', 'city_address_id', 'vertical', 'category', 'cancels', 'orders', 'unique_users',
         'percentage','order_ids', 'store_ids_1', 'store_ids_2']]

    dfab['store_id'] = dfab['store_id'].astype(str)
    dfab = pd.merge(dfab, logsab, how='left', left_on=dfab['store_id'], right_on=logsab['storeid'])
    # dfab = dfab[dfab['storeid'].isnull() | ((dfab['percentage'] >= dfab['last_percentage'] + 0.20) & (dfab['diff'] > 90))]
    dfab = dfab.drop(['key_0', 'storeid'], axis=1)

    dfcd['store_id'] = dfcd['store_id'].astype(str)
    dfcd = pd.merge(dfcd, logscd, how='left', left_on=dfcd['store_id'], right_on=logscd['storeid'])
    # dfcd = dfcd[dfcd['storeid'].isnull() | ((dfcd['percentage'] >= dfcd['last_percentage']) & (dfcd['diff'] > 270))]
    dfcd = dfcd.drop(['key_0', 'storeid'], axis=1)

    try:
        dfab = dfab.drop(stores_to_ignore, left_on='store_id', right_on='storeid')
        dfab = dfab[dfab['check'] != 'check']
        dfab = dfab.drop(['storeid', 'check'], axis=1)
    except Exception as e:
        print(e)
        print('error to exclude store ab')
        pass

    try:
        dfcd = dfcd.drop(stores_to_ignore, left_on='store_id', right_on='storeid')
        dfcd = dfcd[dfcd['check'] != 'check']
        dfcd = dfcd.drop(['storeid', 'check'], axis=1)
    except Exception as e:
        print(e)
        print('error to exclude store cd')
        pass


    print("dfab")
    print(dfab)
    if not dfab.empty:
        for index, row in dfab.iterrows():

            if row['last_percentage'] > 0.1:
                target_date = (current_time + timedelta(minutes=240))
                time_closed = int((target_date - current_time).total_seconds() / 60.0)
            else:
                target_date = (current_time + timedelta(minutes=240))
                time_closed = int((target_date - current_time).total_seconds() / 60.0)

            rows = dict(row)
            reason = 'occ_bleeding_bot'
            store_ids_1 = row['store_ids_1']
            store_ids_2 = row['store_ids_2']
            try:
                store_ids = list(set(store_ids_1) | set(store_ids_2))
            except:
                store_ids = store_ids_1
            print(store_ids)
            store_ids = list(set(store_ids) - set(lista))
            print(store_ids)

            # if len(store_ids) >= 1:
            #     for store_id in store_ids:
            #         print("as store_ids")
            #         print(int(store_id))
            #         cms.disable_store(int(store_id), country, reason, time_closed)
            #         cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
            #     try:
            #         print("sfx")
            #         sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
            #     except:
            #         print("fail sfx")


            text = '''
            *Bleeding_Bot - Cancel by Picker - Top Seller Stores - Acumulado el día :a::b: *
            Country :flag-{country}:
            :construction: La Tienda physical id {store_id} fué suspendida hasta {data}
            :convenience_store:  *Physical Store Name:* {store_name}
            :department_store:  *Vertical:* {vertical} 
            :medal:  *Category:* {category}
            :dart: *Percentage of Cancellation:* {percentage}%
            :dart: *Cancels:* {cancels}
            :page_facing_up: *Orders IDs:* {order_ids}
            :page_with_curl:  *Store IDs:* {store_ids}
            <@UFBAGS5ST> <@UJA30H2UW> <@UQTV4BZ33>
            '''.format(
            store_id=row['store_id'],
            store_name=row['store_name'],
            vertical=row['vertical'],
            cancels=row['cancels'],
            category=row['category'],
            percentage=round(row['percentage']*100,2),
            order_ids=row['order_ids'],
            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
            store_ids=store_ids,
            country=country
            )
            print(text)

            slack.bot_slack(text, 'C02L9BG8Z9T')

    if not dfab.empty:
        dfab['country'] = country
        dfab['suspended_at'] = current_time
        dfab['suspended_until'] = target_date
        dfab = dfab[['store_id', 'cancels', 'suspended_at', 'suspended_until', 'order_ids', 'vertical', 'country', 'category','percentage']]
        print(dfab)
        snow.upload_df_occ(dfab, 'partner_related_test_bleeding_daily')


    if not dfcd.empty:
        for index, row in dfcd.iterrows():

            if row['last_percentage'] > 0.1:
                target_date = target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(20)
            else:
                target_date = (current_time + timedelta(minutes=240))
                time_closed = int((target_date - current_time).total_seconds() / 60.0)

            rows = dict(row)
            reason = 'occ_bleeding_bot'
            store_ids_1 = row['store_ids_1']
            store_ids_2 = row['store_ids_2']
            try:
                store_ids = list(set(store_ids_1) | set(store_ids_2))
            except:
                store_ids = store_ids_1
            print(store_ids)
            #nao remover stores suspendidas ainda para nao vir em branco nas araujo
            # store_ids = list(set(store_ids) - set(lista))
            print(store_ids)

            # if len(store_ids) >= 1:
            #     for store_id in store_ids:
            #         print("as store_ids")
            #         print(int(store_id))
            #         cms.disable_store(int(store_id), country, reason, time_closed)
            #         cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
            #     try:
            #         print("sfx")
            #         sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
            #     except:
            #         print("fail sfx")


            text = '''
            *Bleeding_Bot - Cancel by Picker - Long Tail Stores - Acumulado el día :c::d: *
            Country :flag-{country}:
            :construction: La Tienda physical id {store_id} fué suspendida hasta {data}
            :convenience_store:  *Physical Store Name:* {store_name}
            :department_store:  *Vertical:* {vertical} 
            :medal:  *Category:* {category}
            :dart: *Percentage of Cancellation:* {percentage}%
            :dart: *Cancels:* {cancels}
            :page_facing_up: *Orders IDs:* {order_ids}
            :page_with_curl:  *Store IDs:* {store_ids}
            <@UFBAGS5ST> <@UJA30H2UW> <@UQTV4BZ33>
            '''.format(
            store_id=row['store_id'],
            store_name=row['store_name'],
            vertical=row['vertical'],
            cancels=row['cancels'],
            category=row['category'],
            percentage=round(row['percentage']*100,2),
            order_ids=row['order_ids'],
            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
            store_ids=store_ids,
            country=country
            )
            print(text)

            slack.bot_slack(text, 'C02L9BG8Z9T')    


    if not dfcd.empty:
        dfcd['country'] = country
        dfcd['suspended_at'] = current_time
        dfcd['suspended_until'] = target_date
        dfcd = dfcd[['store_id', 'cancels', 'suspended_at', 'suspended_until', 'order_ids', 'vertical', 'country', 'category','percentage']]
        print(dfcd)
        snow.upload_df_occ(dfcd, 'partner_related_test_bleeding_daily')

def run_process(country):
    stores_to_ignore = excluding_orders(country)
    metrics = offenders(country)
    if not metrics.empty:
        logsab = excluding_stores_ab(country)
        logscd = excluding_stores_cd(country)
        slots = store_infos(country)
        df_suspendidas = suspendidas(country)
        lista = df_suspendidas['store_id'].to_list()
        lista = [f'{str(i)}' for i in lista]
        df_sheets = df[df.COUNTRY == country.upper()]
        new_offenders(logsab, logscd, metrics, slots, lista, df_sheets, stores_to_ignore)
    else:
        print('empty df')


for country in countries:
    try:
        country = country.lower()
        print(country)
        run_process(country)
    except Exception as e:
        print(e)