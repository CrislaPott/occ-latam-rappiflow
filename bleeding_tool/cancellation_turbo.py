import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack, cms, sfx
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
from lib import gsheets

## country_list 
df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Bleeding - Turbo')
df = df[(df["ON/OFF"] == 'ACTIVE')]
df.MIN_ORDERS = df.MIN_ORDERS.astype(float)
df.CANCAL_RATE = df.CANCAL_RATE.astype(float)
df.CANCELS = df.CANCELS.astype(float)
countries = df['COUNTRY'].unique()
print(countries)

def excluding_stores(country):
	timezone, interval = timezones.country_timezones(country)
	print(timezone)
	query = f'''
	select store_id::text as store_id, 
	case when suspended_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '1h' then '2nd' else flag end as flag
	from ops_occ.turbo_cancellation
	where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	group by 1,2
	'''
	df = snow.run_query(query)
	return df

def offenders(country):
	timezone, interval = timezones.country_timezones(country)

	query = f"""
		--no_cache
SET TIMEZONE TO 'America/{timezone}';

WITH a AS
  (SELECT order_id,
		  max(om.created_at) AS teriminated,
		  max(CASE
				  WHEN TYPE ='cancel_order_by_picker' THEN 'cancel_order_by_picker'
				  
				  WHEN TYPE ='cancel_by_support'
					   AND (params ->> 'cancelation_reason')::text IN ('crsan_delivery_delay','crsan_didnt_cancel_by_automation', 'crsan_incomplete_order_bad_state','crsan_product_not_available','crsan_system_unavailable', 'crsan_badly_located_pin','crsan_price_difference') THEN 'cancel_by_support'
				  WHEN o.state IN ('canceled_by_automation') THEN 'canceled_by_automation'
				  ELSE 'Others'
			  END) AS cancel_type
   FROM order_modifications om
   JOIN orders o ON o.id = om.order_id
   WHERE om.created_at > now()::TIMESTAMP - interval '1h'
	 AND (TYPE IN ('close_order',
				   'arrive',
				   'finish_order',
				   'close_by_restaurant_blind',
				   'close_order_by_partner_pickup',
				   'refuse_order_by_partner',
				   'refuse_partner_after_take',
				   'remove_by_partner_inactivity')
		  OR TYPE ILIKE '%cancel%')
	 and ((params->>'cancelation_reason')::text not in ('crsan_inner_test') or params->>('cancelation_reason')::text is null)
	 AND o.state NOT IN ('canceled_by_fraud',
						 'canceled_for_payment_error',
						 'canceled_by_split_error',
						 'canceled_by_early_regret')
   GROUP BY 1)
SELECT os.store_id::text as store_id,
	   os.name as store_name,
	   count(DISTINCT CASE
						  WHEN cancel_type ILIKE '%cancel%' THEN a.order_id
						  ELSE NULL
					  END) AS cancels,
	   count(DISTINCT a.order_id) AS orders,
	   string_agg(DISTINCT CASE
						  WHEN cancel_type ILIKE '%cancel%' THEN a.order_id::text
						  ELSE NULL
					  END,',') as order_ids
FROM a
JOIN order_stores os ON os.order_id=a.order_id
WHERE (os.store_type_group NOT IN ('restaurant_cargo')
	   OR os.store_type_group IS NULL)
  AND (os.type NOT IN ('rappi_pay',
					   'queue_order_flow','soat_webview')
	   OR os.type IS NULL)
GROUP BY 1,
		 2
	"""

	if country == 'co':
		metrics = redash.run_query(1904, query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
	elif country == 'mx':
		metrics = redash.run_query(7977, query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)

	return metrics

def store_infos(country):
	timezone, interval = timezones.country_timezones(country)

	query = f'''
	select store_id as storeid, physical_store_id::text as physical_store_id, ps.name as physical_name, city_id, c.name as city_name
	from stores s 
	join physical_stores ps on ps.id=s.physical_store_id
	join city c on c.id=ps.city_id
	where (s.name ilike '%turbo%' or type ilike '%turbo%')
	and ps.deleted_at is null
	and s.name not ilike '%Dummy%Store%Rappi%Connect%'
	group by 1,2,3,4,5
	'''

	if country == 'br':
		query_results = redash.run_query(1969, query)
	elif country == 'ar':
		query_results = redash.run_query(1968, query)
	elif country == 'co':
		query_results = redash.run_query(1971, query)
	elif country == 'cl':
		query_results = redash.run_query(1970, query)
	elif country == 'pe':
		query_results = redash.run_query(1973, query)
	elif country == 'ec':
		query_results = redash.run_query(2556, query)
	elif country == 'cr':
		query_results = redash.run_query(2558, query)
	elif country == 'mx':
		query_results = redash.run_query(1972, query)
		
	return query_results


def new_offenders(a,b,df_sheets):
	print("a")
	current_time = timezones.country_current_time(country)
	print("b")
	target_date = (current_time + timedelta(hours=1))
	time_closed = int((target_date - current_time).total_seconds() / 60.0)
	print(target_date)
	print(time_closed)
	current_time1 = timezones.country_current_time(country)
	target_date1 = (current_time1 + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
	time_closed1 = int((target_date1 - current_time).total_seconds() / 60.0)
	print(target_date1)
	print(time_closed1)

	try:
		df = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
		df = df.drop(['key_0'], axis=1)

		df['city_id'] = df['city_id'].astype(str)
		df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)

		df = pd.merge(df, df_sheets[['CITY_ID', 'CITY_NAME']], how='inner', left_on=df['city_id'],
							right_on=df_sheets['CITY_ID'])
		df = df.drop(['key_0', 'CITY_ID', 'CITY_NAME'], axis=1)

		df1 = df.groupby(["physical_store_id","physical_name","city_name",'city_id'])[["orders","cancels"]].sum().reset_index()
		df1['percentage'] = ((df1['cancels'] / df1['orders']))

		df_cancels_ids = df[(df['cancels'] >= 1)]

		df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(name='order_ids')
		df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
		df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids')

		df = pd.merge(df1,df2, how='left', on=['physical_store_id'])
		df = pd.merge(df,df3, how='left', on=['physical_store_id'])


		df = pd.merge(df, df_sheets[['CITY_ID', 'MIN_ORDERS','CANCAL_RATE','CANCELS']], how='left',
								  left_on=df['city_id'], right_on=df_sheets['CITY_ID'])
		print(df)

		if country == 'co':
			df = df[(((df['orders'] >= df['MIN_ORDERS']) & (df['percentage'] >= df['CANCAL_RATE']) & (df['cancels'] >= df['CANCELS']) & (~df['city_name'].isin(['Bogotá'])))
			| ((df['orders'] >= df['MIN_ORDERS']) & (df['percentage'] >= df['CANCAL_RATE']) & (df['cancels'] >= df['CANCELS']) & (df['city_name'].isin(['Bogotá']))))]

		elif country == 'br':
			df = df[(((df['orders'] >= df['MIN_ORDERS']) & (df['percentage'] >= df['CANCAL_RATE']) & (df['cancels'] >= df['CANCELS']) & (~df['city_name'].isin(['Grande São Paulo'])))
			| ((df['orders'] >= df['MIN_ORDERS']) & (df['percentage'] >= df['CANCAL_RATE']) & (df['cancels'] >= df['CANCELS']) & (df['city_name'].isin(['Grande São Paulo']))))]

		else:
			df = df[((df['orders'] >= df['MIN_ORDERS']) & (df['percentage'] >= df['CANCAL_RATE']) & (df['cancels'] >= df['CANCELS']))]

		df = df[['physical_store_id', 'physical_name', 'city_name', 'orders', 'cancels', 'percentage', 'order_ids',
				 'store_ids']]
		print(df)


		if not df.empty:
			d = excluding_stores(country)
		else:
			return

		df = df.rename(columns={'physical_store_id': 'store_id', 'physical_name': 'store_name'})

		print(d)
		not_second = d
		not_second = not_second[(not_second['flag'].isin(['2nd']))]
		print(not_second)

		final_df = pd.merge(df,d,how='left',left_on=df['store_id'],right_on=d['store_id'])
		final_df = final_df.rename(columns={'store_id_x': 'store_id'})
		final_df = final_df.drop(['key_0'], axis=1)
		firststrike = final_df[final_df['store_id_y'].isnull()] ## ainda nao caiu hoje

		secondstrike = final_df[((final_df['store_id_y'].notnull()) & (final_df['flag'].isin(['1st'])))]
		secondstrike = secondstrike.drop(['store_id_y'], axis=1)

		secondstrike = pd.merge(secondstrike,not_second,how='left',left_on=secondstrike['store_id'],right_on=not_second['store_id'])
		secondstrike = secondstrike[secondstrike['store_id_y'].isnull()]
		secondstrike = secondstrike.rename(columns={'store_id_x': 'store_id'})

		print("firststrike")
		if not firststrike.empty:
			firststrike = firststrike[["store_id","store_name","orders","cancels","percentage","order_ids","store_ids"]]
			for index, row in firststrike.iterrows():
				rows = dict(row)
				store_ids = row['store_ids']
				print(store_ids)
				if len(store_ids) >= 1:
					for store_id in store_ids:
						print("as filhas")
						print(int(store_id))
						reason = "occ_turbo_cancellation_1st"
						cms.disable_store(int(store_id), country, reason, time_closed)
						cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
						try:
							print("sfx")
							sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
						except:
							print("fail sfx")
				text = '''
				*Tienda suspendida - Turbo :turbo-10-min:  with 20% of cancellation last hour - :flag-{country}:*
				La Tienda (physical store id: {store_id}) fue remitida a suspension hasta {data} (first strike) :dart: 
				Store Name: {store_name}
				Orders: {orders}
				Cancels: {cancels}
				Percentage: {percentage}%
				Orders IDs: {order_ids} 
				Store IDs: {store_ids}'''.format(
					store_id=row['store_id'],
					store_name=row['store_name'],
					cancels=row['cancels'],
					orders=row['orders'],
					order_ids=row['order_ids'],
					percentage=round(row['percentage']*100,2),
					data=target_date.strftime("%m/%d/%Y %H:%M:%S"),
					country=country,
					store_ids=row['store_ids']
					)

				slack.bot_slack(text,'C01S0667VKP')
				print(text)

		if not secondstrike.empty:
			secondstrike = secondstrike[["store_id","store_name","orders","cancels","percentage","order_ids","store_ids"]]
			for index, row in secondstrike.iterrows():
				rows = dict(row)
				store_ids = row['store_ids']
				print(store_ids)
				if len(store_ids) >= 1:
					for store_id in store_ids:
						print("as filhas")
						print(int(store_id))
						reason = "occ_turbo_cancellation_2nd"
						cms.disable_store(int(store_id), country, reason, time_closed)
						cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
						try:
							print("sfx")
							sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
						except:
							print("fail sfx")
				text = '''
				*Tienda suspendida - Turbo with 20% of cancellations last hour - :flag-{country}:*
				La Tienda (physical store id: {store_id}) fue remitida a suspension hasta {data} (second strike) :dart: 
				Store Name: {store_name}
				Orders: {orders}
				Cancels: {cancels}
				Percentage: {percentage}%
				Orders IDs: {order_ids} 
				Store IDs: {store_ids}'''.format(
					store_id=row['store_id'],
					store_name=row['store_name'],
					cancels=row['cancels'],
					orders=row['orders'],
					order_ids=row['order_ids'],
					percentage=round(row['percentage']*100,2),
					data=target_date1.date(),
					country=country,
					store_ids=row['store_ids']
					)

				slack.bot_slack(text,'C01S0667VKP')
				print(text)

		firststrike['country'] = country
		firststrike['suspended_at'] = current_time
		firststrike['suspended_until'] = target_date
		firststrike['flag'] = '1st'
		firststrike['order_ids'] = [','.join(map(str, l)) for l in firststrike['order_ids']]
		firststrike = firststrike[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country','flag']]
		if not firststrike.empty:
			snow.upload_df_occ(firststrike, 'turbo_cancellation')
		secondstrike['country'] = country
		secondstrike['suspended_at'] = current_time
		secondstrike['suspended_until'] = target_date
		secondstrike['flag'] = '2nd'
		secondstrike['order_ids'] = [','.join(map(str, l)) for l in secondstrike['order_ids']]
		secondstrike = secondstrike[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country','flag']]
		if not secondstrike.empty:
			snow.upload_df_occ(secondstrike, 'turbo_cancellation')

	except Exception as e:
		print(e)

def run_process(country):
	a = offenders(country)
	b = store_infos(country)
	df_sheets = df[df.COUNTRY == country.upper()]
	new_offenders(a,b,df_sheets)

for country in countries:
	country = country.lower()
	try:
		print(country)
		run_process(country)
	except Exception as e:
		print(e)