import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, sfx, cms
from lib import snowflake as snow
from functions import timezones
import time
import pytz

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = f'''
    select store_id::text as storeid, SUSPENDED_UNTIL:: date as suspended_until
    from ops_occ.store_closed_recurrently
    where suspended_at::date >= convert_timezone('America/{timezone}',DATEADD(DD, -8,current_date()))::date
    and country='{country}'
    group by 1,2
    '''
    df = snow.run_query(query)
    return df

def suspended_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::text as storeid, created_at::date as created_at from store_attribute
    where suspended=true
    and suspended_reason not in ('store_closed')
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)
    return metrics

def offenders(country):
    query = '''
        select go.store_id::text as store_id
  ,go.store_name
  , go.store_type
  ,count(distinct case when cr.order_id is not null then go.order_id else null end) as cancels
  ,count(distinct go.order_id) as orders
  ,cancels/orders as percentage
  , listagg(distinct case when cr.order_id is not null then go.order_id::text else null end, ',') as order_ids
  from OPS_GLOBAL.global_orders go
  left join OPS_GLOBAL.cancellation_reasons cr on cr.order_id=go.order_id and level_3 ilike '%store_closed%'
where go.created_at::date >= current_date::date -8
  and go.vertical not in ('Restaurantes','Turbo')
  and go.country='{country}'
group by 1,2,3
having percentage >= 0.50 and cancels >= 3
and count(distinct go.created_at::date) >= 2
    '''.format(country=country.upper())
    df = snow.run_query(query)
    print(df)
    return df

def new_offenders(a,b,e):
    current_time = timezones.country_current_time(country)

    target_date = current_time.replace(day=31, month=12,hour=10,minute=0, second=0, microsecond=0)
    time_closed = int((target_date - current_time).total_seconds() / 60.0)
    print(target_date)
    print(time_closed)
    print(a)
    print(b)
    print(e)

    try:
        df = pd.merge(a, e, how='left', left_on=a['store_id'], right_on=e['storeid'])
        df = df[df['storeid'].isnull()]
        df = df.drop(['key_0','storeid'],axis=1)
        print("aqui 1")
        news = pd.merge(df, b, how='left', left_on=a['store_id'], right_on=b['storeid'])
        news = news[news['storeid'].isnull()]
        print("aqui 2")
        print(news)
        if not news.empty:
            news = news[["store_id","store_name","store_type","orders","cancels","percentage","order_ids"]]
            for index, row in news.iterrows():
                rows = dict(row)
                store_id = int(row['store_id'])
                reason = "occ_store_closed_recurrently"
                
                try:
                	cms.enable_store(store_id, country)
                except Exception as e:
                	print(e)
                try:
                	cms.turn_store_on(store_id, country)
                except Exception as e:
                	print(e)

                cms.turnStoreOff(store_id, time_closed+15, country, reason) == 200
                cms.disable_store(store_id, country, reason, time_closed)
                try:
                    sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
                except:
                    print("fail sfx")
                
                text2 = '''
                *Store Closed Recurrently*
                :flag-{country}: 
                La Tienda {store_id} fue remitida a suspensión hasta 31/12 por store closed
                Store Name: {store_name}
                Store Type: {store_type}
                Orders: {orders}
                Cancels: {cancels}
                Percentage: {percentage}
                Orders IDs: {order_ids} '''.format(
                    store_id=row['store_id'],
                    store_name=row['store_name'],
                    store_type=row['store_type'],
                    cancels=row['cancels'],
                    orders=row['orders'],
                    order_ids=row['order_ids'],
                    percentage=round(row['percentage']*100,2),
                    data=current_time.date(),
                    country=country
                    )

                slack.bot_slack(text2,'C01S0667VKP')
                print(text2)

        news['country'] = country
        news['suspended_at'] = current_time
        news['suspended_until'] = target_date

        news = news[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country']]
        print(news)

        snow.upload_df_occ(news, 'store_closed_recurrently')

    except Exception as e:
        print(e)

def run_alarm(country):
    e = excluding_stores(country)
    b = suspended_stores(country)
    a = offenders(country)
    new_offenders(a,b,e)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
    print(country)
    run_alarm(country)
