import os, sys, json
import numpy as np
import pandas as pd
from random import randrange
from datetime import datetime, timedelta
from lib import slack, redash, cms
from lib import snowflake as snow
from functions import timezones

def suspended_stores(country):
	timezone, interval = timezones.country_timezones(country)
	query = '''select store_id::text as storeid, created_at::date as created_at from store_attribute
	where suspended=true
	'''.format(timezone=timezone, interval=interval)

	if country == 'co':
		metrics = redash.run_query(6312, query)
	elif country == 'ar':
		metrics = redash.run_query(6309, query)
	elif country == 'cl':
		metrics = redash.run_query(6311, query)
	elif country == 'mx':
		metrics = redash.run_query(6324, query)
	elif country == 'uy':
		metrics = redash.run_query(6322, query)
	elif country == 'ec':
		metrics = redash.run_query(6447, query)
	elif country == 'cr':
		metrics = redash.run_query(6313, query)
	elif country == 'pe':
		metrics = redash.run_query(6323, query)
	elif country == 'br':
		metrics = redash.run_query(6310, query)
	return metrics

def run_bot(country,logs):
	queryco = f"""
	WITH CMS_INTEGRATIONS AS (
select 
	distinct 
	RAPPI_store_id, 
	max(stocker_execution_date) AS LAST_CMS_INTEGRATION
from 
	CPGS_DATASCIENCE.CO_INTEGRATIONS_LAST_EVENT
WHERE 
	STOCKER_EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND STOCKER_EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
	),
	
CP_INTEGRATIONS AS (
select 
	distinct 
	VIRTUAL_STORE_ID AS RAPPI_store_id, 
	max(execution_date) AS LAST_CP_INTEGRATION
from 
	CPGS_DATASCIENCE.CO_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
WHERE 
	EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
),

base as (


SELECT 
	G.COUNTRY,
	G.STORE_ID,
	G.STORE_NAME,
	G.brand_name,
	case when G.brand_name in ('Ambiente Gourmet', 'Clinique', 'Croydon', 'Cutis', 'Everlast', 'IMUSA Home&Cook', 'IMUSA Home&Cook Outlet', 'Launica', 'Lec lee', 'MAC', 'Madecentro','Masglo Home','Naf Naf', 'Oboticario', 'Options','Pandora', 'Spirito', 'Sportfitness', 'tiendacereza.com', 'Yoi') then 'YES' else 'NO' END as WEEKENDS_EXC,
	 CASE WHEN G.BRAND_NAME IN ('Calvin Klein', 'Calvin Klein Underwear', 'Cloe', 'Decathlon', 'Guess', 'Hema Artz Hogar','juguetron', 'Juguetron Lego', 'La Palestina', 'Lush Belleza', 'Rapsodia', 'Scappino', 'Secrecy', 'Sunglass Hut', 'Tommy Hilfiger', 'Wet n wild') then 'YES' else 'NO' end as DAILY_EXC,
	LEAST(COALESCE(DATEDIFF('HOUR',LAST_CP_INTEGRATION, current_timestamp),200),COALESCE(DATEDIFF('HOUR',LAST_CMS_INTEGRATION, current_timestamp),200),200)  AS HOURS_SINCE_LAST_INTEGRATION,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null THEN CR.ORDER_ID ELSE NULL END) / COUNT(DISTINCT G.ORDER_ID),3) AS CANCEL_RATE,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null and dayname(G.CLOSED_AT) = 'Sun' THEN CR.ORDER_ID ELSE NULL END) / NULLIF(COUNT(DISTINCT CASE WHEN dayname(G.CLOSED_AT) = 'Sun' THEN G.ORDER_ID ELSE NULL END),0),3) AS SUNDAY_CANCEL_RATE
	
FROM 
	OPS_GLOBAL.GLOBAL_ORDERS G
	LEFT JOIN OPS_GLOBAL.CANCELLATION_REASONS CR
		ON CR.ORDER_ID = G.ORDER_ID
	LEFT JOIN CMS_INTEGRATIONS CMSI ON CMSI.RAPPI_STORE_ID = G.STORE_ID
	LEFT JOIN CP_INTEGRATIONS CPI ON CPI.RAPPI_STORE_ID = G.STORE_ID
	
	WHERE G.COUNTRY = 'CO'
	AND G.CREATED_AT >= CURRENT_DATE - INTERVAL '1 month'
	AND G.VERTICAL_GROUP = 'ECOMMERCE'
	AND G.VERTICAL_SUB_GROUP NOT IN ('PETS', 'SERVICES')
	and g.store_id in (select distinct STORE_ID from (select distinct rappi_store_id AS STORE_ID from CPGS_DATASCIENCE.CO_INTEGRATIONS_LAST_EVENT where STOCKER_execution_date > current_date - interval '1 month' union all select distinct VIRTUAL_STORE_ID AS STORE_ID from CPGS_DATASCIENCE.CO_INTEGRATIONS_NEW_CATALOG_LAST_EVENT where execution_date > current_date - interval '1 month') )
	--AND HOURS_SINCE_LAST_INTEGRATION > 72
	
	--WHITELIST: BRANDS THAT WE CAN'T DEACTIVATE, NO MATTER WHAT
	AND G.BRAND_GROUP NOT IN ('Éxito')
	and G.BRAND_NAME NOT ilike '%Carulla%'
	
	GROUP BY 1,2,3,4,5,6,7
	
)


SELECT 

B.COUNTRY, 
B.STORE_ID::text as store_id,
B.STORE_NAME,
B.brand_name,
B.WEEKENDS_EXC,
B.DAILY_EXC,
B.HOURS_SINCE_LAST_INTEGRATION,
CASE 
	--WHEN DAYNAME(CURRENT_DATE) = 'Sun' AND B.SUNDAY_CANCEL_RATE > 0 AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --NO INTEGRATIONS ON SATS + CANCELLATIONS ON SUNS -> NO SALES ON SUNS
			--Para vender los domingos, deberías tener una integración el sábado o un cancel_rate bajito (<5%) los domingos pero al menos integrar el viernes
	WHEN B.WEEKENDS_EXC = 'NO' AND B.DAILY_EXC = 'NO' AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --THE ONES THAT CAN DAILY, HAVE TO DAILY
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'YES' AND B.HOURS_SINCE_LAST_INTEGRATION > 72 THEN 'YES' --THE ONES THAT CAN'T DAILY HAVE TO EVERY 72HS
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'NO' AND dayname(CURRENT_DATE) NOT IN ('Sat', 'Sun') AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --DAILY ON WEEKDAYS, ELSE FIRST RULE
	ELSE 'NO'
	END AS TURN_OFF
	
FROM BASE B

WHERE TURN_OFF = 'YES'
	"""
	querybr = f"""
	WITH CMS_INTEGRATIONS AS (
select 
	distinct 
	RAPPI_store_id, 
	max(stocker_execution_date) AS LAST_CMS_INTEGRATION
from 
	CPGS_DATASCIENCE.BR_INTEGRATIONS_LAST_EVENT
WHERE 
	STOCKER_EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND STOCKER_EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
	),
	
CP_INTEGRATIONS AS (
select 
	distinct 
	VIRTUAL_STORE_ID AS RAPPI_store_id, 
	max(execution_date) AS LAST_CP_INTEGRATION
from 
	CPGS_DATASCIENCE.BR_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
WHERE 
	EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
),

base as (


SELECT 
	G.COUNTRY,
	G.STORE_ID,
	G.STORE_NAME,
	G.brand_name,
	case when G.brand_name in ('Allied', 'Exclusiva', 'Gall Tecnologia T Nc', 'Hering', 'Lepostichecpgs', 'Motostore Tecnologia', 'Oboticario Ecomm', 'Oticas Carol Moda', 'Quemdisseb', 'Samsung Tecnologia', 'Sephora', 'Severo Roth', 'Varejo Mais Samsung Tecnologia', 'Xiaomi Tecnologia') then 'YES' else 'NO' END as WEEKENDS_EXC,
	CASE WHEN G.BRAND_NAME IN ('Motostore Tecnologia', 'Oboticario Ecomm','Quemdisseb', 'Samsung Tecnologia','Severo Roth', 'Varejo Mais Samsung Tecnologia') then 'YES' else 'NO' end as DAILY_EXC,
	LEAST(COALESCE(DATEDIFF('HOUR',LAST_CP_INTEGRATION, current_timestamp),200),COALESCE(DATEDIFF('HOUR',LAST_CMS_INTEGRATION, current_timestamp),200),200)  AS HOURS_SINCE_LAST_INTEGRATION,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null THEN CR.ORDER_ID ELSE NULL END) / COUNT(DISTINCT G.ORDER_ID),3) AS CANCEL_RATE,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null and dayname(G.CLOSED_AT) = 'Sun' THEN CR.ORDER_ID ELSE NULL END) / NULLIF(COUNT(DISTINCT CASE WHEN dayname(G.CLOSED_AT) = 'Sun' THEN G.ORDER_ID ELSE NULL END),0),3) AS SUNDAY_CANCEL_RATE
	
FROM 
	OPS_GLOBAL.GLOBAL_ORDERS G
	LEFT JOIN OPS_GLOBAL.CANCELLATION_REASONS CR
		ON CR.ORDER_ID = G.ORDER_ID
	LEFT JOIN CMS_INTEGRATIONS CMSI ON CMSI.RAPPI_STORE_ID = G.STORE_ID
	LEFT JOIN CP_INTEGRATIONS CPI ON CPI.RAPPI_STORE_ID = G.STORE_ID
	
	WHERE G.COUNTRY = 'BR'
	AND G.CREATED_AT >= CURRENT_DATE - INTERVAL '1 month'
	AND G.VERTICAL_GROUP = 'ECOMMERCE'
	AND G.VERTICAL_SUB_GROUP NOT IN ('PETS', 'SERVICES')
	and g.store_id in (select distinct STORE_ID from (select distinct rappi_store_id AS STORE_ID from CPGS_DATASCIENCE.BR_INTEGRATIONS_LAST_EVENT where STOCKER_execution_date > current_date - interval '1 month' union all select distinct VIRTUAL_STORE_ID AS STORE_ID from CPGS_DATASCIENCE.BR_INTEGRATIONS_NEW_CATALOG_LAST_EVENT where execution_date > current_date - interval '1 month') )
	--AND HOURS_SINCE_LAST_INTEGRATION > 72
	
	--WHITELIST: BRANDS THAT WE CAN'T DEACTIVATE, NO MATTER WHAT
 --   AND G.BRAND_GROUP NOT IN ('Éxito')
 --   and G.BRAND_NAME NOT ilike '%Carulla%'
	
	GROUP BY 1,2,3,4,5,6,7
	
)


SELECT 

B.COUNTRY, 
B.STORE_ID::text as store_id,
B.STORE_NAME,
B.brand_name,
B.WEEKENDS_EXC,
B.DAILY_EXC,
B.HOURS_SINCE_LAST_INTEGRATION,
CASE 
	--WHEN DAYNAME(CURRENT_DATE) = 'Sun' AND B.SUNDAY_CANCEL_RATE > 0 AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --NO INTEGRATIONS ON SATS + CANCELLATIONS ON SUNS -> NO SALES ON SUNS
			--Para vender los domingos, deberías tener una integración el sábado o un cancel_rate bajito (<5%) los domingos pero al menos integrar el viernes
	WHEN B.WEEKENDS_EXC = 'NO' AND B.DAILY_EXC = 'NO' AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --THE ONES THAT CAN DAILY, HAVE TO DAILY
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'YES' AND B.HOURS_SINCE_LAST_INTEGRATION > 72 THEN 'YES' --THE ONES THAT CAN'T DAILY HAVE TO EVERY 72HS
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'NO' AND dayname(CURRENT_DATE) NOT IN ('Sat', 'Sun') AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --DAILY ON WEEKDAYS, ELSE FIRST RULE
	ELSE 'NO'
	END AS TURN_OFF
	
FROM BASE B

WHERE TURN_OFF = 'YES'
	"""
	querymx = f"""
	WITH CMS_INTEGRATIONS AS (
select 
	distinct 
	RAPPI_store_id, 
	max(stocker_execution_date) AS LAST_CMS_INTEGRATION
from 
	CPGS_DATASCIENCE.MX_INTEGRATIONS_LAST_EVENT
WHERE 
	STOCKER_EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND STOCKER_EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
	),
	
CP_INTEGRATIONS AS (
select 
	distinct 
	VIRTUAL_STORE_ID AS RAPPI_store_id, 
	max(execution_date) AS LAST_CP_INTEGRATION
from 
	CPGS_DATASCIENCE.MX_INTEGRATIONS_NEW_CATALOG_LAST_EVENT
WHERE 
	EXECUTION_DATE >= current_timestamp - INTERVAL '1 month'
	AND EXECUTION_DATE <= CURRENT_TIMESTAMP
GROUP BY
	1
),

base as (


SELECT 
	G.COUNTRY,
	G.STORE_ID,
	G.STORE_NAME,
	G.brand_name,
	case when G.brand_name in ('98 Coast Av M Nc', 'Amovil', 'Anforama', 'At T', 'Bedbath Beyond', 'Calvin Klein', 'Calvin Klein Underwear', 'Cloe', 'Decathlon', 'Dewesi', 'Fantasiasmiguel', 'Guess', 'Hema Artz Hogar', 'Innova', 'juguetron', 'Juguetron Lego', 'La Palestina', 'Lush Belleza', 'Miniso', 'Rapsodia', 'Scappino', 'Secrecy', 'Sunglass Hut', 'Tommy Hilfiger', 'Wet n wild') then 'YES' else 'NO' END as WEEKENDS_EXC,
	CASE WHEN G.BRAND_NAME IN ('Calvin Klein', 'Calvin Klein Underwear', 'Cloe', 'Decathlon', 'Guess', 'Hema Artz Hogar','juguetron', 'Juguetron Lego', 'La Palestina', 'Lush Belleza', 'Miniso', 'Rapsodia', 'Scappino', 'Secrecy', 'Sunglass Hut', 'Tommy Hilfiger', 'Wet n wild') then 'YES' else 'NO' end as DAILY_EXC,
	LEAST(COALESCE(DATEDIFF('HOUR',LAST_CP_INTEGRATION, current_timestamp),200),COALESCE(DATEDIFF('HOUR',LAST_CMS_INTEGRATION, current_timestamp),200),200)  AS HOURS_SINCE_LAST_INTEGRATION,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null THEN CR.ORDER_ID ELSE NULL END) / COUNT(DISTINCT G.ORDER_ID),3) AS CANCEL_RATE,
	ROUND(COUNT(DISTINCT CASE WHEN CR.SUBCATEGORY is not null and dayname(G.CLOSED_AT) = 'Sun' THEN CR.ORDER_ID ELSE NULL END) / NULLIF(COUNT(DISTINCT CASE WHEN dayname(G.CLOSED_AT) = 'Sun' THEN G.ORDER_ID ELSE NULL END),0),3) AS SUNDAY_CANCEL_RATE
	
FROM 
	OPS_GLOBAL.GLOBAL_ORDERS G
	LEFT JOIN OPS_GLOBAL.CANCELLATION_REASONS CR
		ON CR.ORDER_ID = G.ORDER_ID
	LEFT JOIN CMS_INTEGRATIONS CMSI ON CMSI.RAPPI_STORE_ID = G.STORE_ID
	LEFT JOIN CP_INTEGRATIONS CPI ON CPI.RAPPI_STORE_ID = G.STORE_ID
	
	WHERE G.COUNTRY = 'MX'
	AND G.CREATED_AT >= CURRENT_DATE - INTERVAL '1 month'
	AND G.VERTICAL_GROUP = 'ECOMMERCE'
	AND G.VERTICAL_SUB_GROUP NOT IN ('PETS', 'SERVICES')
	and g.store_id in (select distinct STORE_ID from (select distinct rappi_store_id AS STORE_ID from CPGS_DATASCIENCE.MX_INTEGRATIONS_LAST_EVENT where STOCKER_execution_date > current_date - interval '1 month' union all select distinct VIRTUAL_STORE_ID AS STORE_ID from CPGS_DATASCIENCE.MX_INTEGRATIONS_NEW_CATALOG_LAST_EVENT where execution_date > current_date - interval '1 month') )
	--AND HOURS_SINCE_LAST_INTEGRATION > 72
	
	--WHITELIST: BRANDS THAT WE CAN'T DEACTIVATE, NO MATTER WHAT
 --   AND G.BRAND_GROUP NOT IN ('Éxito')
 --   and G.BRAND_NAME NOT ilike '%Carulla%'
 AND G.BRAND_GROUP NOT IN ('Sephora','Gimba','Miniso', 'Lumen', 'Coppel', 'Sanborns')
	
	GROUP BY 1,2,3,4,5,6,7
	
)


SELECT 

B.COUNTRY, 
B.STORE_ID::text as store_id,
B.STORE_NAME,
B.brand_name,
B.WEEKENDS_EXC,
B.DAILY_EXC,
B.HOURS_SINCE_LAST_INTEGRATION,
CASE 
	--WHEN DAYNAME(CURRENT_DATE) = 'Sun' AND B.SUNDAY_CANCEL_RATE > 0 AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --NO INTEGRATIONS ON SATS + CANCELLATIONS ON SUNS -> NO SALES ON SUNS
			--Para vender los domingos, deberías tener una integración el sábado o un cancel_rate bajito (<5%) los domingos pero al menos integrar el viernes
	WHEN B.WEEKENDS_EXC = 'NO' AND B.DAILY_EXC = 'NO' AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --THE ONES THAT CAN DAILY, HAVE TO DAILY
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'YES' AND B.HOURS_SINCE_LAST_INTEGRATION > 72 THEN 'YES' --THE ONES THAT CAN'T DAILY HAVE TO EVERY 72HS
	WHEN B.WEEKENDS_EXC = 'YES' AND B.DAILY_EXC = 'NO' AND dayname(CURRENT_DATE) NOT IN ('Sat', 'Sun') AND B.HOURS_SINCE_LAST_INTEGRATION > 24 THEN 'YES' --DAILY ON WEEKDAYS, ELSE FIRST RULE
	ELSE 'NO'
	END AS TURN_OFF
	
FROM BASE B

WHERE TURN_OFF = 'YES'
	"""

	if country in ['co']:
		df = snow.run_query(queryco)
	elif country in ['br']:
		df = snow.run_query(querybr)
	elif country in ['mx']:
		df = snow.run_query(querymx)

	print(df)

	if current_time.strftime('%H:%M') >= '06:00' and current_time.strftime('%H:%M') <= '18:00':
		target_date = (current_time + timedelta(minutes=230))
		time_closed = int((target_date - current_time).total_seconds() / 60.0)
	elif current_time.strftime('%H:%M') >= '18:10' and current_time.strftime('%H:%M') <= '21:00':
		target_date = (current_time + timedelta(minutes=230))
		time_closed = int((target_date - current_time).total_seconds() / 60.0)

	print(target_date)
	print(time_closed)
	log = logs

	df = pd.merge(df,log,how='left',left_on=df['store_id'],right_on=log['storeid'])
	df = df[df['storeid'].isnull()]
	df = df.drop(['key_0'],axis=1)

	print(df)

	for index, row in df.iterrows():
		rows = dict(row)
		store_id = int(row['store_id'])
		reason = "ecom_no_integration"
		cms.turnStoreOff(store_id, time_closed+2, country, reason) == 200
		cms.disable_store(store_id, country, reason, time_closed) == 200

	to_upload = df
	to_upload['country'] = country
	to_upload['suspended_at'] = current_time
	snow.upload_df_occ(to_upload,'ecommerce_no_integration')

countries = ['co','br','mx']
for country in countries:
	print(country)
	current_time = timezones.country_current_time('co')
	try:
		logs = suspended_stores(country)
		run_bot(country, logs)
	except Exception as e:
		print(e)
