import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, sfx, cms
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
from lib import gsheets

## country_list 
df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'CPGs PIVOT')
df = df[(df["ON/OFF"] == 'ACTIVE')]
df.MIN_ORDERS = df.MIN_ORDERS.astype(float)
countries = df['COUNTRY'].unique()
print(countries)

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    select store_id::text as storeid
    from ops_occ.waiting_time_cpgsnow
    where suspended_at >= convert_timezone('America/{timezone}',current_timestamp()) - interval '2h'
    and country='{country}'
    group by 1
    '''
    df = snow.run_query(query)
    return df

def suspendidas(country):
    query = '''
    select store_id
    from store_attribute
    where suspended=true
    '''

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
        --no_cache
    SET TIMEZONE TO 'America/{timezone}';
    with a as (
    select 
      om.order_id,
      max(case when om.type = 'domiciliary_in_store' then om.created_at end) as rt_in_store,
      max(case when om.type = 'hand_to_domiciliary' then om.created_at end) as hand_to_rt,
      max(case when om.type ilike '%cancel%' then om.created_at end) as canceled_at,
      max(case when om.type = 'taken_visible_order' and params->>'storekeeper_id' is not null then om.created_at else null end) as rt_taken
      from order_modifications om 
      where 
      (om.type in 
      ('domiciliary_in_store', 'hand_to_domiciliary', 'taken_visible_order')
      or om.type ilike '%cancel%')
      and om.created_at > now() - interval '2h'
      group by 1)

      , b as (
      select os.name as store_name, os.store_id
      , o.cooking_time_started_at, cooking_time
      , rt_in_store
      , hand_to_rt
      , a.order_id
      , (cooking_time_started_at + ( cooking_time * interval '1 minute')) as cooking_time_ends_at
      , canceled_at
      , (extract(epoch from (canceled_at - rt_in_store)) / 60) as in_store_to_canceled
      , (extract(epoch from (hand_to_rt - rt_in_store)) / 60) as rt_waiting


      from a 
      join order_stores os on os.order_id=a.order_id 
      join orders o on o.id=a.order_id

      where (rt_in_store is not null or o.state in ('in_progress'))
      and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
      and not os.is_marketplace
      )
      select store_id::text as store_id, store_name, count(distinct order_id) as cancels, string_agg(distinct order_id::text, ',') as order_ids
      from b
      --- in progress/in store
      where hand_to_rt is null 
      and canceled_at is null
      group by 1,2
     -- having count(distinct order_id) >= 15
        """.format(timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)

    return metrics

def store_infos(country):
    timezone, interval = timezones.country_timezones(country)

    query_slots = f'''
    select s.store_id, ps.physical_store_id, ps.name as physical_store_name, s.city_address_id, v.vertical, category
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    join ops_occ.store_infos_latam si on si.store_id=s.store_id and si.country='{country}'
    join {country}_PGLR_MS_STORES_PUBLIC.store_types st on st.id=s.type
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores ps on ps.store_id=s.store_id
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps2 on ps2.id=ps.physical_store_id
    join (select store_type                      as storetype,
                                   case
                                       when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                       when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                       when vertical_group = 'WHIM' then 'Antojos'
                                       when vertical_group = 'RAPPICASH' then 'RappiCash'
                                       when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                       when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                       when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
                                       when upper (store_type) in ('TURBO') then 'Turbo'
                                       when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
                                       when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                       when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                       when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                       when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                       when upper(vertical_group) in ('CPGS') then 'CPGs'
                                       else vertical_sub_group end as vertical
                            from VERTICALS_LATAM.{country}_VERTICALS_V2 v
       ) v on v.storetype = s.type
left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=si.brand_group_id
                                           and s.city_address_id=c.city_address_id
                                           and c.COUNTRY='{country.upper()}' and si.vertical=c.vertical
                                           and c.vertical=(case when si.vertical in ('Mercados') then 'Super/Hiper' else si.vertical end)
    where st.purchase_type in ('now')
      and st.scheduled=false
      and v.vertical in ('Express','Farmacia','Licores','Ecommerce')
      and s.deleted_at is null and s.suggested_state=true
group by 1,2,3,4,5,6
'''
    slots = snow.run_query(query_slots)

    return slots

def new_offenders(logs, metrics,slots, lista, df_sheets):

    print("df")
    current_time = timezones.country_current_time(country)

    try:
        metrics['store_id'] = metrics['store_id'].astype(str)
        slots['store_id'] = slots['store_id'].astype(str)

        news = pd.merge(metrics, slots, how='inner', left_on=metrics['store_id'], right_on=slots['store_id'])
        df = news.drop(['key_0', 'store_id_y'], axis=1).rename(columns={'store_id_x': 'store_id'})
        df.category = df.category.fillna('D')

        df['city_address_id'] = df['city_address_id'].astype(str)
        df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)

        df = pd.merge(df, df_sheets[['CITY_ID']], how='inner', left_on=df['city_address_id'],right_on=df_sheets['CITY_ID'])
        df = df.drop(['key_0', 'CITY_ID'], axis=1)

        ### agrupar o df por physical_id

        df['physical_store_id'] = df['physical_store_id'].astype(str)
        df = pd.merge(df, logs, how='left', left_on=df['physical_store_id'], right_on=logs['storeid'])
        df = df[df['storeid'].isnull()]
        df = df.drop(['key_0', 'storeid'], axis=1)

        if not df.empty:
            print('tiene nuevas tiendas!')

            if country == 'ar':

                df1 = df.groupby(["physical_store_id","physical_store_name","vertical","category",'city_address_id'])[["cancels"]].sum().reset_index()

                df_cancels_ids = df[(df['cancels'] >= 1)]
                df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(name='order_ids')
                df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
                df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_1')

                df = pd.merge(df1,df2, how='left', on=['physical_store_id'])
                df = pd.merge(df,df3, how='left', on=['physical_store_id'])

            else:

                df = df[~df['vertical'].isin(['Ecommerce'])]

                df1 = df.groupby(["physical_store_id", "physical_store_name", "vertical", "category",'city_address_id'])[
                    ["cancels"]].sum().reset_index()

                df_cancels_ids = df[(df['cancels'] >= 1)]
                df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(
                    name='order_ids')
                df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
                df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_1')

                df = pd.merge(df1, df2, how='left', on=['physical_store_id'])
                df = pd.merge(df, df3, how='left', on=['physical_store_id'])


            df['physical_store_id'] = df['physical_store_id'].astype(str)
            slots['physical_store_id'] = slots['physical_store_id'].astype(str)    
            df_aux = pd.merge(df, slots, how='left', on=['physical_store_id'])
            df_aux = df_aux.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_2')
            
            df = pd.merge(df,df_aux, how='left', on=df_aux['physical_store_id']) # added column store_ids_2
            df = df.rename(columns={'physical_store_id_x': 'store_id', 'physical_store_name': 'store_name'})
            df = df.drop(['key_0'], axis=1)
            df = pd.merge(df, df_sheets[['CITY_ID', 'MIN_ORDERS']], how='left',left_on=df['city_address_id'], right_on=df_sheets['CITY_ID'])

            if country == 'cl':
                df = df[(df['cancels'] >= df['MIN_ORDERS'])]
                target_date = (current_time + timedelta(minutes=30))
                time_closed = int((target_date - current_time).total_seconds() / 60.0)
                print(target_date)
                print(time_closed)
                if not df.empty:
                    for index, row in df.iterrows():
                        rows = dict(row)
                        store_id = int(row['store_id'])
                        reason = "occ_peak_orders_in_progress"
                        
                        store_ids_1 = row['store_ids_1']
                        store_ids_2 = row['store_ids_2']
                        try:
                            store_ids = list(set(store_ids_1) | set(store_ids_2))
                        except:
                            store_ids = store_ids_1
                        store_ids = list(set(store_ids) - set(lista))

                        print(store_ids)
                        if len(store_ids) >= 1:
                            for store_id in store_ids:
                                print("as filhas")
                                print(int(store_id))
                                cms.disable_store(int(store_id), country, reason, time_closed)
                                cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
                            try:
                                print("sfx")
                                sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                            except:
                                print("fail sfx")

                        text = '''
                                *Peak orders in progress :clock1: CPGs NOW*
                                Country {country} :flag-{country}:
                                La Tienda physical id :building_construction: {store_id} fué suspendida hasta {data}
                                Physical Store Name: {store_name}
                                Vertical: {vertical} 
                                Ordenes: {cancels}
                                Orders IDs: {order_ids}
                                Store IDs: {store_ids}
                                '''.format(
                            store_id=row['store_id'],
                            store_name=row['store_name'],
                            vertical=row['vertical'],
                            cancels=row['cancels'],
                            order_ids=row['order_ids'],
                            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                            store_ids=store_ids,
                            country=country
                        )
                        print(text)

                        slack.bot_slack(text, 'C01S0667VKP')
                else:
                    print('no son cpgs now')

            elif country == 'ar':
                df = df[(df['cancels'] >= df['MIN_ORDERS'])]
                target_date = (current_time + timedelta(minutes=30))
                time_closed = int((target_date - current_time).total_seconds() / 60.0)
                print(target_date)
                print(time_closed)
                df1 = df[~df['vertical'].isin(['Ecommerce'])]
                df2 = df[df['vertical'].isin(['Ecommerce'])]
                if not df1.empty:
                    for index, row in df1.iterrows():
                        rows = dict(row)
                        store_id = int(row['store_id'])
                        reason = "occ_peak_orders_in_progress"
                        store_ids_1 = row['store_ids_1']
                        store_ids_2 = row['store_ids_2']
                        try:
                            store_ids = list(set(store_ids_1) | set(store_ids_2))
                        except:
                            store_ids = store_ids_1
                        store_ids = list(set(store_ids) - set(lista))
                        print(store_ids)
                        if len(store_ids) >= 1:
                            for store_id in store_ids:
                                print("as filhas")
                                print(int(store_id))
                                cms.disable_store(int(store_id), country, reason, time_closed)
                                cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
                            try:
                                print("sfx")
                                sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                            except:
                                print("fail sfx")

                        text = '''
                        *Peak orders in progress :clock1: CPGs NOW*
                        Country {country} :flag-{country}:
                        La Tienda physical id :building_construction: {store_id} fué suspendida hasta {data}
                        Physical Store Name: {store_name}
                        Vertical: {vertical} 
                        Ordenes: {cancels}
                        Orders IDs: {order_ids}
                        Store IDs: {store_ids}
                        '''.format(
                            store_id=row['store_id'],
                            store_name=row['store_name'],
                            vertical=row['vertical'],
                            cancels=row['cancels'],
                            order_ids=row['order_ids'],
                            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                            store_ids=store_ids,
                            country=country
                        )
                        print(text)

                        slack.bot_slack(text, 'C01S0667VKP')
                else:
                    print('no son cpgs now')
                if not df2.empty:
                    for index, row in df2.iterrows():
                        rows = dict(row)
                        store_id = int(row['store_id'])
                        reason = "occ_peak_orders_in_progress"
                        store_ids_1 = row['store_ids_1']
                        store_ids_2 = row['store_ids_2']
                        try:
                            store_ids = list(set(store_ids_1) | set(store_ids_2))
                        except:
                            store_ids = store_ids_1
                        store_ids = list(set(store_ids) - set(lista))
                        print(store_ids)
                        if len(store_ids) >= 1:
                            for store_id in store_ids:
                                print("as filhas")
                                print(int(store_id))
                                cms.disable_store(int(store_id), country, reason, time_closed)
                                cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
                            try:
                                print("sfx")
                                sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                            except:
                                print("fail sfx")

                        text = '''
                        *Peak orders in progress :clock1: Ecommerce*
                        Country {country} :flag-{country}:
                        La Tienda physical id :building_construction: {store_id} fué suspendida hasta {data}
                        Physical Store Name: {store_name}
                        Vertical: {vertical} 
                        Ordenes: {cancels}
                        Orders IDs: {order_ids}
                        Store IDs: {store_ids}
                        '''.format(
                            store_id=row['store_id'],
                            store_name=row['store_name'],
                            vertical=row['vertical'],
                            cancels=row['cancels'],
                            order_ids=row['order_ids'],
                            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                            store_ids=store_ids,
                            country=country
                        )
                        print(text)

                        slack.bot_slack(text, 'C01S0667VKP')
                else:
                    print('no son ecommerce')
            else:
                df = df[(df['cancels'] >= df['MIN_ORDERS'])]
                dfab = df[(df['category']== 'A') | (df['category']== 'B')]
                dfcd = df[(df['category'] == 'C') | (df['category'] == 'D')]
                if not dfab.empty:
                    target_date = (current_time + timedelta(minutes=30))
                    time_closed = int((target_date - current_time).total_seconds() / 60.0)
                    print(target_date)
                    print(time_closed)
                    if not dfab.empty:
                        for index, row in dfab.iterrows():
                            rows = dict(row)
                            store_id = int(row['store_id'])
                            reason = "occ_peak_orders_in_progress"
                            store_ids_1 = row['store_ids_1']
                            store_ids_2 = row['store_ids_2']
                            try:
                                store_ids = list(set(store_ids_1) | set(store_ids_2))
                            except:
                                store_ids = store_ids_1
                                store_ids = list(set(store_ids) - set(lista))
                            print(store_ids)
                            if len(store_ids) >= 1:
                                for store_id in store_ids:
                                    print("as filhas")
                                    print(int(store_id))
                                    cms.disable_store(int(store_id), country, reason, time_closed)
                                    cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
                                try:
                                    print("sfx")
                                    sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                                except:
                                    print("fail sfx")

                            text = '''
                            *Peak orders in progress :clock1: CPGs NOW*
                            Country {country} :flag-{country}:
                            La Tienda physical id :building_construction: {store_id} fué suspendida hasta {data}
                            Physical Store Name: {store_name}
                            Vertical: {vertical} 
                            Category: {category}
                            Ordenes: {cancels}
                            Orders IDs: {order_ids}
                            Store IDs: {store_ids}
                            '''.format(
                            store_id=row['store_id'],
                            store_name=row['store_name'],
                            vertical=row['vertical'],
                            cancels=row['cancels'],
                            category=row['category'],
                            order_ids=row['order_ids'],
                            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                            store_ids=store_ids,
                            country=country
                            )
                            print(text)

                            slack.bot_slack(text, 'C01S0667VKP')
                else:
                    print('no son cpgs now')

                if not dfcd.empty:
                    target_date = (current_time + timedelta(minutes=60))
                    time_closed = int((target_date - current_time).total_seconds() / 60.0)
                    print(target_date)
                    print(time_closed)
                    if not dfcd.empty:
                        for index, row in dfcd.iterrows():
                            rows = dict(row)
                            store_id = int(row['store_id'])
                            reason = "occ_peak_orders_in_progress"
                            store_ids_1 = row['store_ids_1']
                            store_ids_2 = row['store_ids_2']
                            try:
                                store_ids = list(set(store_ids_1) | set(store_ids_2))
                            except:
                                store_ids = store_ids_1
                                store_ids = list(set(store_ids) - set(lista))
                            print(store_ids)
                            if len(store_ids) >= 1:
                                for store_id in store_ids:
                                    print("as filhas")
                                    print(int(store_id))
                                    cms.disable_store(int(store_id), country, reason, time_closed)
                                    cms.turnStoreOff(int(store_id), time_closed+5, country, reason) == 200
                                try:
                                    print("sfx")
                                    sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                                except:
                                    print("fail sfx")

                            text = '''
                            *Peak orders in progress :clock1: CPGs NOW*
                            Country {country} :flag-{country}:
                            La Tienda physical id :building_construction: {store_id} fué suspendida hasta {data}
                            Physical Store Name: {store_name}
                            Vertical: {vertical}
                            Category: {category} 
                            Ordenes: {cancels}
                            Orders IDs: {order_ids}
                            Store IDs: {store_ids}
                            '''.format(
                            store_id=row['store_id'],
                            store_name=row['store_name'],
                            vertical=row['vertical'],
                            cancels=row['cancels'],
                            category=row['category'],
                            order_ids=row['order_ids'],
                            data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
                            store_ids=store_ids,
                            country=country
                            )
                            print(text)
                            slack.bot_slack(text, 'C01S0667VKP')
                else:
                    print('no son cpgs now')

            df['country'] = country
            df['suspended_at'] = current_time
            df['suspended_until'] = target_date
            df = df[['store_id', 'cancels', 'suspended_at', 'suspended_until', 'order_ids', 'vertical', 'country']]

            print(df)
            snow.upload_df_occ(df, 'waiting_time_cpgsnow')

        else:
            print('news empty')

    except Exception as e:
        print(e)


def run_process(country):
    metrics = offenders(country)
    if not metrics.empty:
        logs = excluding_stores(country)
        slots = store_infos(country)
        df_suspendidas = suspendidas(country)
        lista = df_suspendidas['store_id'].to_list()
        lista = [f'{str(i)}' for i in lista]
        df_sheets = df[df.COUNTRY == country.upper()]
        new_offenders(logs, metrics, slots, lista, df_sheets)
    else:
        print('empty df')

for country in countries:
    country = country.lower()
    try:
        print(country)
        run_process(country)
    except Exception as e:
        print(e)