import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack, cms,sfx
from lib import snowflake as snow
from functions import timezones
import pytz
import time
from lib import gsheets

df_sheet = gsheets.read_sheet('1ARanbVKRJupxVo0bSokEaVbamvPILMjmGaEy1Qu14OI', 'Stores')
df_sheet = df_sheet[['country', 'storeid']]

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = f"""
    with tiendas as (select country, store_id,
                        status,
                        last_checked_at,
                        case when status in ('4. Apagado 7 dias') then dateadd('days',+7,last_checked_at)
when status in ('2. Apagado 72 hrs') then dateadd('hours',+72,last_checked_at)
when status in ('3. Apagado 96 hrs') then dateadd('hours',+96,last_checked_at)
end as suspended_until

    from
(select
       country,
       store_id,
       last_value(status) over (partition by store_id, country order by checked_at asc) as status,
       last_value(checked_at) over (partition by store_id, country order by checked_at asc) as last_checked_at
from ops_occ.nl_worst_offenders
where lower(country) = '{country}'
    )

group by 1,2,3,4)

select tiendas.store_id,
       s.name as store_name,
       country, 
       status, 
       last_checked_at, 
       suspended_until,
       convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz as local_now,
       datediff('minutes',local_now::timestamp_ntz,suspended_until::timestamp_ntz) as to_suspend
from tiendas
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=tiendas.store_id
where status not in ('1. Alerta OCC')
and suspended_until::date > convert_timezone('America/{timezone}',current_timestamp())::date
and s.name not ilike '%petz%'
    """

    metrics = snow.run_query(query)
    return metrics

def enabled_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = f"""
    select store_id as storeid
    from store_attribute
    where suspended=true
    group by 1
    """

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics

def new_offenders(a,b,df_sheets):
    current_time = timezones.country_current_time(country)

    df = pd.merge(a,b,how='left',left_on=a['store_id'],right_on=b['storeid'])
    df = df[df['storeid'].isnull()]
    df = df.drop(['key_0', 'storeid'], axis=1)
    df = pd.merge(df, df_sheets[['storeid']], how='left', left_on=df['store_id'], right_on=df_sheets['storeid'])
    df = df[df['storeid'].isnull()]

    for index, row in df.iterrows():
        rows = dict(row)
        time_closed = int(row['to_suspend'])
        print(time_closed)
        reason = "occ_non_live_worst_offenders"
        store_id = int(row['store_id'])
        cms.turnStoreOff(store_id, time_closed+15, country, reason) == 200
        cms.disable_store(store_id, country, reason, time_closed)
        try:
            print("sfs")
            sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
        except:
            print("fail sfx")

        time.sleep(2)

    df['country'] = country
    df['suspended_at'] = current_time
    df = df[['store_id','store_name','status','last_checked_at','suspended_until','to_suspend','suspended_at','country']]
    print(df)
    snow.upload_df_occ(df, 'nl_worst_offenders_guarantee')

def run_alarm(country):
    df_sheets = df_sheet[df_sheet.country == country.upper()]
    a = offenders(country)
    b = enabled_stores(country)
    new_offenders(a,b,df_sheets)

countries = ['br','co','mx','ec','cr','pe','cl','uy','ar']
for country in countries:
    country = country.lower()
    try:
        print(country)
        run_alarm(country)
    except Exception as e:
        print(e)