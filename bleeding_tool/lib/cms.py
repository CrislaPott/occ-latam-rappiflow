import requests
import json
import os
from airflow.models import Variable

headers = {
    'accept': 'application/json, text/plain, */*',
    'Referer': 'https://retail-cms.rappi.com/stores',
    'Origin': 'https://retail-cms.rappi.com',
    'User-Agent':
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.'
        '79 Safari/537.36',
    'Sec-Fetch-Mode': 'cors',
    'sec-fetch-site': 'cross-site',
    'Content-Type': 'application/json',
    'Authorization': Variable.get("CMS_KEY"),
    'Auth-user': 'kissflow',
    'Cache-Control': "no-cache",
    'x-application-id': 'rappiflow-latam'
}

base_url = {
    'ar': 'https://services.rappi.com.ar',
    'br': 'https://services.rappi.com.br',
    'cl': 'https://services.rappi.cl',
    'co': 'https://services.rappi.com',
    'cr': 'https://services.rappi.co.cr',
    'ec': 'https://services.rappi.com.ec',
    'mx': 'https://services.mxgrability.rappi.com',
    'pe': 'https://services.rappi.pe',
    'uy': 'https://services.rappi.com.uy',
    'test': 'https://services.rappi.com.br'
}

def turnStoreOff(store_id, time_closed, country, reason):
    data = {
        "is_enabled": False,
        "reason": reason,
        "time": time_closed,
    }
    response = requests.put(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/',
            str(store_id)]),
        headers=headers,
        data=json.dumps(data)
    )
    return(response)

def turn_store_on(country, store_id):
    """Turn on one store in CMS."""
    data_on = '{"is_enable":true,"reason":"","time":null}'
    response = requests.put(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/',
            str(store_id)]),
        headers=headers,
        data=data_on
    )
    return response.status_code

def enable_store(store_id, country):
    """Unsuspends one store in CMS."""
    return requests.delete(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/store-attribute/',
            str(store_id)
        ]),
        headers=headers
    ).status_code

def disable_store(store_id, country, reason, time_closed=None):
    """Suspends one store in CMS."""
    data = {
        "store_attribute": {
            "store_id": int(store_id),
            "suspended_reason": reason
        },
        "time": time_closed
    }
    return requests.post(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/store-attribute',
        ]),
        headers=headers,
        data=json.dumps(data)
    ).status_code


def getStoreInfo(store_id, country):
    """Get detailed information about the store in CMS."""
    response = requests.get(
        ''.join([
            base_url[country],
            '/api/cms/gateway/cms-stores/api/cms/stores/',
            str(int(store_id))
        ]),
        headers=headers
    ).json()
    return response


def turnProductOff(store_id, product_id, status, in_stock, starts_at, ends_at, reason):
    data = [{
        "store_id": int(store_id),
        "product_id": int(product_id),
        "status": status,
        "in_stock": in_stock,
        "starts_at": str(starts_at),
        "ends_at": str(ends_at),
        "reason": reason
    }]

    return requests.post(
        ''.join([
            base_url[country],
            '/api/cms/gateway/override/api/cms/override/v3',
        ]),
        headers=headers_product,
        data=json.dumps(data)
    ).status_code