import os
from dotenv import load_dotenv
from airflow.models import Variable
import signalfx

load_dotenv()

SFX_TOKEN = Variable.get("SIGNALFX_TOKEN")
# SFX_TOKEN = 'S848V-140BFSyIYA3NJwcQ'
print('imprimir acá:', SFX_TOKEN)


def send_events_turnStoreOff(store_id, country, reason, suspended_at):
    suspended_at = suspended_at.strftime("%Y-%m-%d %H:%M:%S")

    dimensions = {
        # "env":"traefik-prd-ar"
    }
    dimensions["store_id"] = str(store_id)
    dimensions["reason"] = reason
    dimensions["country"] = country
    dimensions["suspended_at"] = suspended_at

    with signalfx.SignalFx().ingest(SFX_TOKEN) as sfx:
        response = sfx.send_event(
            event_type="turn_store_off_occ", dimensions=dimensions
        )
    print("ok")
    return response


def send_events_turnStoreOn(store_id, country, reason, turned_on_at):

    dimensions = {
        # "env":"traefik-prd-ar"
    }
    dimensions["store_id"] = str(store_id)
    dimensions["reason"] = reason
    dimensions["country"] = country
    dimensions["turned_on_at"] = turned_on_at

    with signalfx.SignalFx().ingest(SFX_TOKEN) as sfx:
        response = sfx.send_event(event_type="turn_store_on_occ", dimensions=dimensions)

    print("ok")
    return response
