from lib import snowflake as snow
from lib import redash, slack, cms, sfx
import pandas as pd
from functions import timezones
import time

new_google_sheet_url = "https://docs.google.com/spreadsheets/d/1LBJgXSdpdur1o_XXhfRsIzS12GjVG-7cJLtF0OywJoo/export?format=csv&gid=0"
sheets = pd.read_csv(new_google_sheet_url, error_bad_lines=False)

def suspended_stores(country):

    query = '''select store_id::text as storeid, created_at::date as created_at from store_attribute
    where suspended=true
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)
    return metrics

def stores():
    query = '''with mitienda_updates as (

select

case
when country = 'Colombia' then 'CO'
when country = 'Brazil' then 'BR'
when country = 'Argentina' then 'AR'
when country = 'Mexico' then 'MX'
when country = 'Chile' then 'CL'
when country = 'Peru' then 'PE'
when country = 'Ecuador' then 'EC'
when country = 'Costa Rica' then 'CR'
when country = 'Uruguay' then 'UY'else null end as country_code,
replace(event_properties:store_id,'""','')::int as store_id,
count(distinct amplitude_id) as last_month_mi_tienda_updates

from AMPLITUDE_SHARE.SCHEMA_277233.EVENTS_277233
where 1=1
and event_type = 'CONFIRM_STORE_PRODUCT_BULK_UPLOAD'
and client_event_time >= current_date - interval '1 month'
and country_code is not null

group by 1,2

),

stores as (

SELECT * FROM (

select 'CO' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.CO_VERTICALS_V2 v join CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'BR' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.BR_VERTICALS_V2 v join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'MX' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.MX_VERTICALS_V2 v join MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'AR' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.AR_VERTICALS_V2 v join AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'CL' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.CL_VERTICALS_V2 v join CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'CR' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.CR_VERTICALS_V2 v join CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'EC' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.EC_VERTICALS_V2 v join EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'PE' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.PE_VERTICALS_V2 v join PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
UNION ALL
select 'UY' as country, S.STORE_id as store_id, s.name, s.contact_email from VERTICALS_LATAM.UY_VERTICALS_V2 v join UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES s on v.store_type = s.type where v.vertical_group = 'ECOMMERCE' and v.vertical_group not in ('SERVICES', 'PETS', '', 'RAPPIUSA') AND not coalesce(v._fivetran_deleted,false) and not coalesce(S._fivetran_deleted, false) AND S.IS_ENABLED and created_at::date <= current_date - interval '2 months'
)

),

cancellations as (

select

go.country,
go.store_id,
go.store_name,
go.brand_name,
count(distinct go.order_id) as total_orders,
listagg(distinct go.order_id, ', ') as orders_cancel,
count(distinct case when go.count_to_gmv then go.order_id else null end) financial_orders,
coalesce(sum(distinct case when go.count_to_gmv then go.gmv_usd else null end),0) financial_gmv,
coalesce(sum(distinct case when go.count_to_gmv = false then go.gmv_usd else null end),0) lost_gmv,
coalesce( count(distinct case when go.state_type like ('%canceled%')  and go.order_state not in ('canceled_by_fraud', 'canceled_by_payment_error') then go.order_id else null end),0) as canceled_orders,
(canceled_orders / total_orders) as can_r

from ops_global.global_orders go
left join (select distinct id, country, acuerdo from GLOBAL_ECOMMERCE.TBL_CANPENTAS) b on go.order_id = b.id and go.country = b.country
LEFT JOIN OPS_GLOBAL.CANCELLATION_REASONS d on go.order_id = d.order_id and go.country = d.country
LEFT JOIN GLOBAL_ECOMMERCE.ACCOUNTS_KAM kams on kams.country = go.country and go.brand_group = kams.brand_group

where go.created_at >= date_trunc('week', current_date) - interval '1 months'
and go.vertical_group = 'ECOMMERCE'
and go.vertical_sub_group not in ('PETS', 'SERVICES')
and b.acuerdo not in ('LN', 'RUSA')

group by 1,2,3,4

),

integrated_stores as (

SELECT
DISTINCT
UPPER(D.COUNTRY) AS COUNTRY,
NDS.RAPPI_STORE_ID AS STORE_ID,
'YES' AS INTEGRA

FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D
JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE NDS ON D.ID = NDS.DATASOURCE_ID
WHERE
COALESCE(NDS._FIVETRAN_DELETED,FALSE) = FALSE AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE AND NDS.DELETED_AT IS NULL AND D.DELETED_AT IS NULL

UNION ALL

/*
SELECT
DISTINCT
UPPER(D.COUNTRY) AS COUNTRY,
DSR.DERIVED_ID AS STORE_ID,
'YES' AS INTEGRA

FROM CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE D
JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.NEW_DATASOURCE_STORE NDS ON D.ID = NDS.DATASOURCE_ID
JOIN CO_PG_MS_CPGS_CATALOG_INTEGRATIONS_DB_PUBLIC.DATASOURCE_STORE_RELATION DSR ON D.ID = DSR.DATASOURCE_ID
WHERE 1=1
AND COALESCE(NDS._FIVETRAN_DELETED,FALSE) = FALSE
AND COALESCE(D._FIVETRAN_DELETED,FALSE) = FALSE
AND COALESCE(DSR._FIVETRAN_DELETED,FALSE) = FALSE
AND NDS.DELETED_AT IS NULL
AND D.DELETED_AT IS NULL
AND DSR.DELETED_AT IS NULL
*/

SELECT
DISTINCT
'BR' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'CO' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'MX' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'AR' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'CL' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'CR' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'EC' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'PE' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

union all

SELECT
DISTINCT
'UY' AS COUNTRY,
SPINOFF_ID AS STORE_ID,
'YES' AS INTEGRA
FROM UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_SPINOFF
WHERE 1=1 AND COALESCE(_FIVETRAN_DELETED, FALSE) = FALSE

)

select

s.country,
s.store_id,
s.contact_email,
s.name,
c.brand_name,
--mt.last_month_mi_tienda_updates,
COALESCE(mt.last_month_mi_tienda_updates,0) AS LAST_MONTH_UPDATES,
i.integra,
COALESCE(c.total_orders, 0) AS TOTAL_ORDERS,
COALESCE(c.financial_orders,0) AS FINANCIAL_ORDERS,
COALESCE(c.financial_gmv,0) AS FINANCIAL_GMV,
COALESCE(c.lost_gmv,0) AS LOST_GMV,
COALESCE(c.canceled_orders,0) AS CANCELED_ORDERS,
c.orders_cancel,
coalesce(c.can_r,0) as cancel_rate

from STORES s
LEFT JOIN mitienda_updates mt on s.store_id = mt.store_id and mt.country_code = s.country
left join cancellations c on s.country = c.country and s.store_id = c.store_id
left join integrated_stores i on s.country = i.country and s.store_id = i.store_id
where i.integra is null
AND COALESCE(c.financial_gmv,0) <= 5700
and cancel_rate >= 0.3
and COALESCE(mt.last_month_mi_tienda_updates,0) = 0
and not (c.brand_name ilike '%xito%' and s.country = 'CO')
and not (c.brand_name ilike '%tai loy%' and s.country = 'PE')
and not (c.brand_name ilike '%tailoy%' and s.country = 'PE')
and not (c.brand_name ilike '%easy%' and s.country = 'AR')
order by 1 desc, 5 desc'''

    df = snow.run_query(query)
    return df

def alarm(df_offenders,df_sheets,df_suspended_stores):

    df = pd.merge(df_offenders, df_sheets, how="left", left_on=['country', 'brand_name'], right_on=['COUNTRY', 'BRAND'])
    df = df[(df['BRAND'].isnull())]
    df = df.drop(['BRAND','COUNTRY'], axis=1)

    df = df.merge(df_suspended_stores, how='left', left_on=['store_id'], right_on=['storeid'])
    df = df[df['storeid'].isnull()]
    df = df.drop(['storeid','created_at'], axis=1)

    if not df.empty:
        time_closed = None
        current_time = timezones.country_current_time(country)
        df['suspended_at'] = current_time
        print(df)

        snow.upload_df_occ(df, 'ecomm_mi_tienda_stock')

        for index, row in df.iterrows():
            rows = dict(row)
            store_id = row['store_id']
            reason = "occ_ecomm_mi_tienda_stock_update"
            cms.disable_store(int(store_id), country, reason, time_closed)
            cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
            try:
                print("")
                sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
            except:
                print("fail sfx")

            text2 = '''
       *Tienda suspendida - Mi Tienda* :alert:
       Pais: {country} :flag-{country}:
       Store ID: {store_id}
       Store Name: {name}
       Brand Name: {brand_name}
       Total Orders: {total_orders}
       Cancels: {canceled_orders}
       Cancel Rate: {cancel_rate}%
       Orders IDs: {orders_cancel} 

       Tiendas que  no integradas que no hayan actualizado su inventario a través de Mi Tienda utilizando la opción "Actualizar via CSV" al menos una vez por mes y tengan un cancel rate > 30% en el último mes, serán suspendidas INDEFINIDAMENTE. 
           '''.format(
                        store_id=row['store_id'],
                        name=row['name'],
                        canceled_orders=row['canceled_orders'],
                        total_orders=row['total_orders'],
                        orders_cancel=row['orders_cancel'],
                        cancel_rate=round(row['cancel_rate']*100,2),
                        brand_name = row['brand_name'],
                        country=country
                        )
            slack.bot_slack(text2, 'C01S0667VKP')
            print(text2)
            time.sleep(2)
    else:
        print('df null')


offenders = stores()

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
  try:
      print(country)
      time.sleep(60)

      df_suspended_stores = suspended_stores(country)
      df_offenders = offenders[offenders.country == country.upper()]
      df_sheets = sheets[sheets.COUNTRY == country.upper()]

      alarm(df_offenders, df_sheets, df_suspended_stores)

  except Exception as e:
    print(e)

