import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from lib import redash, slack, cms
from lib import snowflake as snow
from functions import timezones
import pytz

new_google_sheet_url = "https://docs.google.com/spreadsheets/d/1FV3yhK6qthW4Obz0CdiTyt1KkqfF2esJ_TdzAK9LbLE/export?format=csv&gid=1668470483"
df = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
df = df[(df["ON/OFF"] == 'ACTIVE')]
countries = df['COUNTRY'].to_list()
print(countries)


def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = f'''
    select store_id::text as storeid
    from ops_occ.ecommerce_process
    where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    group by 1
    '''
    df = snow.run_query(query)
    return df

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = f"""
    --no_cache
    SET TIMEZONE TO 'America/{timezone}';
    with base as (SELECT distinct order_id
    FROM order_modifications om
    WHERE om.created_at >= (now())::date
    AND (TYPE IN ('close_order','arrive','finish_order','close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') 
    OR TYPE ILIKE '%cancel%')
    and not (type in ('cancel_by_user') and (params->>'cancelation_reason') ilike '%other%')
    )
    select os.store_id::text as store_id,
    count(distinct o.id) as orders,
    count(distinct case when o.state ilike '%cancel%' then o.id else null end) cancels,
    count(distinct case when o.state ilike '%cancel%' then o.id else null end)::float/count(distinct o.id)::float as percentage,
    string_agg(distinct case when o.state ilike '%cancel%' then o.id::text else null end,',') as order_ids
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthetic%'
    inner join order_stores os on os.order_id = o.id and (contact_email not like '%@synth.rappi.com' or contact_email is null)
    where 
    o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
    and os.store_id not in (900113251)
     and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
    and (os.type not in ('rappi_pay','queue_order_flow','soat_webview') or os.type is null)
    and os.type not ilike '%siniva%'
    and os.type not in ('rappi_pay','queue_order_flow','soat_webview', 'casino_siniva_tvs_nc','casino_siniva_nc',
    'agaval_siniva','airpods_siniva','alca_siniva','comprandoando_siniva_nc','electrojaponesa_siniva','fantasiaelectronica_siniva',
    'flamingo_siniva','innovar_siniva','mac_center_watch','iphonexsmax_mc_siniva','ishop_accesorios','kitchenaid_siniva',
    'ktienda_siniva_nc','mansion_siniva','samsung_mundial_siniva_nc','haceb_tech_siniva_nc','exclusiv_siniva_nc',
    'samsung_celulares_siniva_nc','sony_nuevo_siniva','twido_colombia_siniva_nc','smartbuy_siniva_nc')
    group by 1
    """

    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics

def enabled_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = f"""
    select s.store_id::text as storeid
    from stores s
    where s.is_enabled=true
    group by 1
    """

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics


def top_sellers(country):
    timezone, interval = timezones.country_timezones(country)

    query = f'''
    select ps.physical_store_id::text as id_store
    from {country}_CORE_ORDERS_PUBLIC.orders_vw o
    left join {country}_CORE_ORDERS_CALCULATED_INFORMATION.orders_vw o2 on o2.order_id=o.id
    left join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id and os.store_id=o2.store_id
    left join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores ps on ps.store_id=os.store_id
    join VERTICALS_LATAM.{country}_verticals_v2 v on v.store_type=os.type and vertical_group = 'ECOMMERCE'
    where o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
    and o.created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}',current_timestamp))::date
    and o.created_at::date < convert_timezone('America/{timezone}',current_timestamp)::date
    group by 1
    having count(distinct o.id) >= 7
    '''
    df = snow.run_query(query)
    return df

def ecommerce_stores(country):
    query='''
    with ord as (select store_id::text as store_id, count(distinct order_id) orders from OPS_GLOBAL.global_orders
where lower(country) = '{country}'
and created_at::date >= current_date::date -7
group by 1
having orders >= 1)

    select s.store_id::text as storeid, ps.physical_store_id::text as physical_store_id, ps2.name as physical_store_name,
     'Ecommerce' as vertical, 
     coalesce(orders,0) as orders_
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    join {country}_PGLR_MS_STORES_PUBLIC.store_types st on st.id=s.type
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores ps on ps.store_id=s.store_id
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps2 on ps2.id=ps.physical_store_id
    join (select store_type                      as storetype,
                                   case
                                       when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                       when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                       when vertical_group = 'WHIM' then 'Antojos'
                                       when vertical_group = 'RAPPICASH' then 'RappiCash'
                                       when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                       when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                       when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
                                       when upper (store_type) in ('TURBO') then 'Turbo'
                                       when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
                                       when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                       when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                       when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                       when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                       when upper(vertical_group) in ('CPGS') then 'CPGs'
                                       else vertical_sub_group end as vertical
                            from VERTICALS_LATAM.{country}_VERTICALS_V2 v
       ) v on v.storetype = s.type
    left join ord o on o.store_id=s.store_id
    where v.vertical in ('Ecommerce')
    and s.type not ilike '%siniva%'
    and s.type not in ('rappi_pay','queue_order_flow','soat_webview', 'casino_siniva_tvs_nc','casino_siniva_nc',
    'agaval_siniva','airpods_siniva','alca_siniva','comprandoando_siniva_nc','electrojaponesa_siniva','fantasiaelectronica_siniva',
    'flamingo_siniva','innovar_siniva','mac_center_watch','iphonexsmax_mc_siniva','ishop_accesorios','kitchenaid_siniva',
    'ktienda_siniva_nc','mansion_siniva','samsung_mundial_siniva_nc','haceb_tech_siniva_nc','exclusiv_siniva_nc',
    'samsung_celulares_siniva_nc','sony_nuevo_siniva','twido_colombia_siniva_nc','smartbuy_siniva_nc')
    group by 1,2,3,4,5
    '''.format(country=country)

    metrics = snow.run_query(query)

    return metrics

def new_offenders(a,b,c,e,f):
    current_time = timezones.country_current_time(country)

    target_date = current_time.replace(day=31, month=12,hour=10,minute=0, second=0, microsecond=0)
    time_closed = int((target_date - current_time).total_seconds() / 60.0)
    print(target_date)
    print(time_closed)

    try:
        df = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
        df = df.drop(['key_0','storeid'], axis=1)

        ##colocar top sellers por physical store_ids !!!
        df = pd.merge(df,c,how='left',left_on=df['physical_store_id'],right_on=c['id_store'])
        df = df[df['id_store'].isnull()]
        df = df.drop(['key_0','id_store'], axis=1)

        ##agrupar por physical 
        df1 = df.groupby(["physical_store_id","physical_store_name","vertical"])[["cancels","orders"]].sum().reset_index()
        df1['percentage'] = ((df1['cancels'] / df1['orders']))

        df_cancels_ids = df[(df['cancels'] >= 1)]

        df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(name='order_ids')
        df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
        df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_1')

        df = pd.merge(df1,df2, how='left', on=['physical_store_id'])
        df = pd.merge(df,df3, how='left', on=['physical_store_id'])

        df = df.rename(columns={'physical_store_id': 'store_id', 'physical_store_name': 'store_name'})

        try:
            b = pd.merge(b,f, how='inner', on=['storeid'])
            b = b[(b['orders_'] >= 1)]

            df5 = pd.merge(df,b,how='inner',left_on=df['store_id'],right_on=b['physical_store_id'])
            df5 = df5.drop(['key_0'], axis=1)
            df5 = df5.groupby(["physical_store_id"])["storeid"].apply(list).reset_index(name='store_ids_2')
            df = pd.merge(df,df5, how='left', left_on=df['store_id'],right_on=df5['physical_store_id'])
            df = df.drop(['key_0'], axis=1)
        except:
            df = df
            df['store_ids_2'] = 'nan'

        df = df[((df['cancels'] >= 2) & (df['percentage'] >= 0.50))]

        ##logs
        news = pd.merge(df,e,how='left',left_on=df['store_id'],right_on=e['storeid'])
        news = news[news['storeid'].isnull()]
        news = news.drop(['key_0','storeid'], axis=1)

        if not news.empty:
            news = news[["store_id","store_name","orders","cancels","percentage","order_ids","store_ids_1",'store_ids_2']]
            for index, row in news.iterrows():
                rows = dict(row)
                store_name=row['store_name']
                store_id = int(row['store_id'])
                reason = "long_tail_ecommerce"
                store_ids_1 = row['store_ids_1']
                store_ids_2 = row['store_ids_2']
                try:
                    store_ids = list(set(store_ids_1) | set(store_ids_2))
                except:
                    store_ids = store_ids_1
                if len(store_ids) >= 1:
                    for store_id in store_ids:
                        print(int(store_id))
                        cms.disable_store(int(store_id), country, reason, time_closed)
                        cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
                        try:
                            print("sfx")
                            sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                        except:
                            print("fail sfx")
                text = '''
                *Tienda suspendida - Long Tail Ecommerce* :desktop_computer:
                Country: :flag-{country}:
                La Tienda Physical ID {store_id} fue remitida a suspension hasta 31/12/2021 por cancelamiento :analise-de-cancelamento: 
                Store Name: {store_name} :convenience_store: 
                Orders: {orders}
                Cancels: {cancels}
                Percentage: {percentage}
                Orders IDs: {order_ids} 
                Store IDs: {store_ids}'''.format(
                    store_id=row['store_id'],
                    store_name=store_name,
                    cancels=row['cancels'],
                    orders=row['orders'],
                    store_ids=store_ids,
                    order_ids=row['order_ids'],
                    percentage=round(row['percentage']*100,2),
                    data=current_time.date(),
                    country=country
                    )
                slack.bot_slack(text,'C01S0667VKP')
                print(text)

        news['country'] = country
        news['suspended_at'] = current_time
        news['suspended_until'] = target_date

        news = news[['store_id','cancels','percentage','suspended_at','suspended_until','order_ids','country']]
        snow.upload_df_occ(news, 'ecommerce_process')

    except Exception as e:
        print(e)

def run_alarm(country):
    e = excluding_stores(country)
    a = offenders(country)
    b = ecommerce_stores(country)
    c = top_sellers(country)
    f = enabled_stores(country)
    new_offenders(a,b,c,e,f)


countries = ['co']
for country in countries:
    country = country.lower()
    try:
        print(country)
        run_alarm(country)
    except Exception as e:
        print(e)