from lib import redash, slack
import time
from functions import timezones
from datetime import datetime, timedelta
from os.path import expanduser
import pandas as pd

def stores(country):
    query = '''
SELECT
  DISTINCT s.store_id,
  s.store_brand_id as brand_id,
  b.name as brand_name,
  s.name AS store_name,
  v.group AS vertical,
  v.sub_group,
  stg.store_type,
  s.created_at,
  c.city
FROM
  store_types st
  LEFT JOIN verticals v ON st.id = v.store_type
  LEFT JOIN stores s ON v.store_type = s.type
  LEFT JOIN store_type_groups stg ON st.id = stg.store_type
  left join store_brands b on s.store_brand_id = b.id
  LEft join city_addresses c on s.city_address_id = c.id
WHERE
  v.group = 'CPGs' ---OR v.group = 'ecommerce'
   AND s.deleted_at IS NULL and s.store_id is not null 
   AND v.sub_group = 'Licores'
ORDER BY
  s.name asc'''
    df = redash.run_query(673,query)
    return df
def suspended_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::text as storeid, created_at::date as created_at from store_attribute
    where suspended=true
    '''
    metrics = redash.run_query(6323, query)

    return metrics

def bot_stores(stores_licores,suspended):
    stores_licores['brand_id'] = stores_licores['brand_id'].fillna(0)
    stores_licores['brand_name'] = stores_licores['brand_name'].fillna('Sem Brand')

    stores_licores['store_id'] = stores_licores['store_id'].astype(int)
    suspended['storeid'] = suspended['storeid'].astype(int)
    stores_licores = pd.merge(stores_licores, suspended, how='left', left_on=stores_licores['store_id'], right_on=suspended['storeid'])
    stores_licores = stores_licores[stores_licores['storeid'].isnull()]

    stores_licores1 = stores_licores.groupby(['vertical','brand_id','brand_name'])['store_id'].agg(
            ['count', ('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    print(stores_licores1)

    current_time = timezones.country_current_time(country)
    target_date = (current_time + timedelta(days=1)).replace(hour=9, minute=0, second=0, microsecond=0)
    time_closed = int((target_date - current_time).total_seconds() / 60.0)
    reason = "occ_licores_pe"

    for index, row in stores_licores1.iterrows():
        df_row = stores_licores[(stores_licores['brand_id'] == row['brand_id'])]
        for store_id in df_row['store_id']:
            print(store_id)
            #cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
            #try:
            #    print("")
            #    sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
            #except:
            #    print("fail sfx")

        #time.sleep(30)

    text = '''
    *Proceso de Cierre de Tienda por Ley Seca* :flag-{country}:
    :alert:
    Las tiendas fueron cerradas hasta 09:00:00 AM.
    '''.format(country=country)
    print(text)
    home = expanduser("~")
    results_file = '{}/ley_seca_licores_{country}.xlsx'.format(home, country=country)
    stores_licores.to_excel(results_file, sheet_name='stores', index=False)
    slack.file_upload_channel('C015QR8KT62', text, results_file, "xlsx")

countries = ['pe']
for country in countries:
    try:
        stores_licores = stores(country)
        suspended = suspended_stores(country)
        bot_stores(stores_licores, suspended)
    except Exception as e:
        print(e)

