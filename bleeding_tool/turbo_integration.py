#!/usr/bin/env python
# coding: utf-8

#get_ipython().system('jupyter nbconvert --to script okr_availability.ipynb')



from turbo_integration_function import process
# In[5]:
channel = ['C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP', 'C01S0667VKP']
# channel = ['U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV', 'U026TUGHJTV']
for index, country in enumerate(['BR', 'CO', 'MX', 'AR', 'CL', 'PE', 'UY', 'EC', 'CR']):
    process(country=country, 
            canal=channel[index])  #canal-mio = 'U026TUGHJTV'