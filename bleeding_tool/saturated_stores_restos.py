import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, cms
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz

def excluding_stores(country):
	timezone, interval = timezones.country_timezones(country)
	query = f'''
	select store_id, last_flag, last_suspended_at
from (
		 select store_id, last_flag, max(suspended_at) as last_suspended_at
		 from (select store_id::text                                                          as store_id,
					  suspended_at,
					  last_value(flag) over (partition by store_id order by suspended_at asc) as last_flag
			   from ops_occ.waiting_time_restaurantes
			   where suspended_at::date = convert_timezone('America/{timezone}', current_timestamp())::date
			   and country = '{country}'
			  )
		 group by 1, 2
	 )
where ((last_suspended_at >= convert_timezone('America/{timezone}', current_timestamp())::date - interval '9h'
	   and last_flag ilike '%high%')
		   or
	   (last_suspended_at >= convert_timezone('America/{timezone}', current_timestamp())::date - interval '2h'
	   and last_flag ilike '%low%')
	)
	'''
	df = snow.run_query(query)
	return df

def offenders(country):
	timezone, interval = timezones.country_timezones(country)

	query = f"""
	--no_cache
SET TIMEZONE TO 'America/{timezone}';
with a as (
select 
  om.order_id,
  max(case when om.type = 'domiciliary_in_store' then om.created_at end) as rt_in_store,
  max(case when om.type = 'hand_to_domiciliary' then om.created_at end) as hand_to_rt,
  max(case when om.type ilike '%cancel%' then om.created_at end) as canceled_at,
  max(case when om.type = 'taken_visible_order' and params->>'storekeeper_id' is not null then om.created_at else null end) as rt_taken
  from order_modifications om 
  where 
  (om.type in 
  ('domiciliary_in_store', 'hand_to_domiciliary', 'taken_visible_order')
  or om.type ilike '%cancel%')
  and om.created_at > now() - interval '1h'
  group by 1)
  
  , b as (
  select os.name as store_name, os.store_id
  , o.cooking_time_started_at, cooking_time
  , rt_in_store
  , hand_to_rt
  , a.order_id
  , (cooking_time_started_at + ( cooking_time * interval '1 minute')) as cooking_time_ends_at
  , canceled_at
  , (extract(epoch from (canceled_at - rt_in_store)) / 60) as in_store_to_canceled

  from a 
  join order_stores os on os.order_id=a.order_id and os.type in ('restaurant')
  join orders o on o.id=a.order_id
  
  where rt_in_store is not null
  and hand_to_rt is null
  and canceled_at is not null
  and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
  and not os.is_marketplace
  and os.store_id not in (900522392,900522388,900522389,900522390,900522391)
  )
  
  select store_id::text as store_id, store_name, count(distinct order_id) as cancels, string_agg(distinct order_id::text, ',') as order_ids
  from b
  where in_store_to_canceled >= 15
  and hand_to_rt is null
  group by 1,2
  having count(distinct order_id) >= 1
	"""

	if country == 'co':
		metrics = redash.run_query(1904, query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
	elif country == 'mx':
		metrics = redash.run_query(1371, query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)

	return metrics

def store_infos(country):
	timezone, interval = timezones.country_timezones(country)

	query = f'''
	select country, store_id::text as storeid,
	   case when brand_segmentation in ('1. CORPORATE BRAND','2. FINE DINING','2. LOCAL CHAIN','3. LOCAL CHAIN','4. LOCAL HERO','6. BRAND DEVELOPMENT')
	   then 'Top'
	   else 'LongTail' end as category
from CO_WRITABLE.A_SCORECARD_REST2
where lower(country)='{country}'
group by 1,2,3
	'''
	df = snow.run_query(query)
	return df

def new_offenders(a,b,d):
	channel_alert = timezones.slack_channels(country)

	threshold_topsellers_high = {
	'ar': '3',
	'br': '4',
	'cl': '3',
	'co': '4',
	'cr': '3',
	'ec': '3',
	'mx': '4',
	'pe': '4',
	'uy': '4'
	}
	threshold_long_high = {
	'ar': '2',
	'br': '2',
	'cl': '2',
	'co': '2',
	'cr': '2',
	'ec': '2',
	'mx': '2',
	'pe': '3',
	'uy': '3'
	}
	threshold_topsellers_low = {
	'ar': '2',
	'br': '2',
	'cl': '2',
	'co': '2',
	'cr': '1',
	'ec': '1',
	'mx': '2',
	'pe': '2',
	'uy': '2'
	}
	threshold_long_low = {
	'ar': '1',
	'br': '1',
	'cl': '1',
	'co': '1',
	'cr': '1',
	'ec': '1',
	'mx': '1',
	'pe': '2',
	'uy': '2'
	}

	try:
		df = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
		df = df.drop(['key_0'], axis=1)
		print(df)
		
		conditions  = [df['category'].isin(['Top']) & (df['cancels'] >= int(threshold_topsellers_high[country])),
		df['category'].isin(['Top']) & (df['cancels'] >= int(threshold_topsellers_low[country])),
		df['category'].isin(['LongTail']) & (df['cancels'] >= int(threshold_long_high[country])),
		df['category'].isin(['LongTail']) & (df['cancels'] >= int(threshold_long_low[country]))
		]
		choices = [ "top_high", 'top_low', 'long_high','long_low']

		df["flag"] = np.select(conditions, choices, default=np.nan)
		df = df[df['flag'].isin(['top_high','top_low','long_high','long_low'])]

		news = pd.merge(df,d,how='left',left_on=df['store_id'],right_on=d['store_id'])
		news = news[news['store_id_y'].isnull()]
		news = news.rename(columns={'store_id_x': 'store_id'})

		if not news.empty and df.shape[0] <= 10:
			news = news[["store_id","store_name","cancels","order_ids","category","flag"]]
			for index, row in news.iterrows():
				rows = dict(row)
				store_id = int(row['store_id'])
				reason = "waiting_time_saturated"
				if row['flag'] in ['top_high']:
					timer = '8h'
					current_time = timezones.country_current_time(country)
					target_date = (current_time + timedelta(minutes=480))
					time_closed = int((target_date - current_time).total_seconds() / 60.0)
					print(target_date)
					print(time_closed)
				elif row['flag'] in ['top_low']:
					timer = '1h'
					current_time = timezones.country_current_time(country)
					target_date = (current_time + timedelta(minutes=60))
					time_closed = int((target_date - current_time).total_seconds() / 60.0)
					print(target_date)
					print(time_closed)
				elif row['flag'] in ['long_high']:
					timer = '8h'
					current_time = timezones.country_current_time(country)
					target_date = (current_time + timedelta(minutes=480))
					time_closed = int((target_date - current_time).total_seconds() / 60.0)
					print(target_date)
					print(time_closed)
				elif row['flag'] in ['long_low']:
					timer = '1h'
					current_time = timezones.country_current_time(country)
					target_date = (current_time + timedelta(minutes=60))
					time_closed = int((target_date - current_time).total_seconds() / 60.0)
					print(target_date)
					print(time_closed)

				cms.turnStoreOff(store_id, time_closed+3, country, reason) == 200
				cms.disable_store(store_id, country, reason, time_closed)
				text = '''
				*Tienda suspendida - Waiting Time :clock1: Restaurant*
				:flag-{country}:
				La Tienda {store_id} fue remitida a suspensión hasta {data}
				Strike (duración suspensión): {timer_flag}
				Store Name: {store_name}
				Category: {category} :categorización: 
				Cancels: {cancels}
				Orders IDs: {order_ids} '''.format(
					store_id=row['store_id'],
					store_name=row['store_name'],
					category=row['category'],
					cancels=row['cancels'],
					order_ids=row['order_ids'],
					data=target_date.strftime("%d/%m/%Y %H:%M:%S"),
					country=country,
					timer_flag=timer
					)

				if row['flag'] in ['top_high']:
					slack.bot_slack(text,channel_alert)
				elif row['flag'] in ['long_high']:
					slack.bot_slack(text,channel_alert)

				slack.bot_slack(text,'C01S0667VKP')
				print(text)

			news['country'] = country
			news['suspended_at'] = current_time
			news['suspended_until'] = current_time
			news = news[['store_id','cancels','suspended_at','suspended_until','order_ids','country','flag']]
			snow.upload_df_occ(news, 'waiting_time_restaurantes')

	except Exception as e:
		print(e)

def run_process(country):
	d = excluding_stores(country)
	a = offenders(country)
	b = store_infos(country)
	new_offenders(a,b,d)

countries = ['br','co','mx','pe','ar','cl']
for country in countries:
	print(country)
	try:
		run_process(country)
	except Exception as e:
		print(e)