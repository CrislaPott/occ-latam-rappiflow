import pandas as pd
from datetime import date, datetime
import holidays

def get_national_holidays(country):
	country = country.upper()

	if country in ['CO']:
		holidays_list = holidays.CO()
	elif country in ['BR']:
		holidays_list = holidays.BR()
	elif country in ['MX']:
		holidays_list = holidays.MX()
	elif country in ['PE']:
		holidays_list = holidays.PE()
	elif country in ['CL']:
		holidays_list = holidays.CL()
	elif country in ['EC']:
		holidays_list = holidays.EC()
	elif country in ['CR']:
		holidays_list = holidays.CR()
	elif country in ['AR']:
		holidays_list = holidays.AR()
	elif country in ['UY']:
		holidays_list = holidays.UY()

	return holidays_list


def get_cities_holidays(city_id):
	year = datetime.now().year

	if city_id in ['10']:
		#São Paulo sp grande sp
		holidays_cities_list = [datetime(year=year,month=1,day=25).date(),
								datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=11,day=20).date()]

	if city_id in ['14']:
		#Rio de Janeiro RJ
		holidays_cities_list = [datetime(year=year,month=1,day=20).date(),
								datetime(year=year,month=4,day=23).date(),
								datetime(year=year,month=11,day=20).date()]

	if city_id in ['81']:
		#Aracaju SE
		holidays_cities_list = [datetime(year=year,month=3,day=17).date(),
								datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=7,day=8).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['146']:
		#Balneario Camboriu SC
		holidays_cities_list = [datetime(year=year,month=7,day=20).date()]

	if city_id in ['199']:
		#Barueri sp
		holidays_cities_list = [datetime(year=year,month=6,day=24).date(),
								datetime(year=year, month=7, day=9).date(),
								datetime(year=year, month=11, day=20).date()]

	if city_id in ['18']:
		#Belo Horizonte MG
		holidays_cities_list = [datetime(year=year,month=8,day=15).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['76']:
		# Belém PA
		holidays_cities_list = [datetime(year=year,month=1,day=12).date(),
								datetime(year=year,month=8,day=15).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['31']:
		# Brasilia DF
		holidays_cities_list = [datetime(year=year,month=4,day=21).date(),
								datetime(year=year,month=11,day=30).date()]

	if city_id in ['47']:
		# Campinas SP
		holidays_cities_list = [datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=11,day=20).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['316']:
		# Canasvieiras SC -- Florianópolis
		holidays_cities_list = [datetime(year=year,month=3,day=23).date()]

	if city_id in ['161']:
		# Canoas RS
		holidays_cities_list = [datetime(year=year,month=2,day=2).date(),
								datetime(year=year,month=9,day=20).date(),]

	if city_id in ['29']:
		# Curitiba PR
		holidays_cities_list = [datetime(year=year,month=9,day=8).date()]

	if city_id in ['159']:
		# Diadema SP
		holidays_cities_list = [datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=12,day=8).date(),]
	if city_id in ['157']:
		# Duque De Caxias RJ
		holidays_cities_list = [datetime(year=year,month=4,day=23).date(),
								datetime(year=year,month=6,day=13).date(),
								datetime(year=year,month=11,day=20).date()]

	if city_id in ['51']:
		# Florianópolis SC
		holidays_cities_list = [datetime(year=year,month=3,day=23).date()]

	if city_id in ['30']:
		# Fortaleza CE
		holidays_cities_list = [datetime(year=year,month=3,day=19).date(),
								datetime(year=year,month=3,day=25).date(),
								datetime(year=year,month=4,day=13).date(),
								datetime(year=year,month=8,day=15).date()]

	if city_id in ['48']:
		# Goiânia GO
		holidays_cities_list = [datetime(year=year,month=5,day=24).date(),
								datetime(year=year,month=10,day=24).date()]

	if city_id in ['147']:
		# Jaboatao Dos Guararapes PE
		holidays_cities_list = [datetime(year=year,month=1,day=15).date(),
								datetime(year=year,month=3,day=6).date(),
								datetime(year=year,month=4,day=25).date(),
								datetime(year=year,month=5,day=4).date(),
								datetime(year=year,month=6,day=14).date()]

	if city_id in ['60']:
		# João Pessoa PB
		holidays_cities_list = [datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=8,day=5).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['94']:
		# Juiz de Fora MG
		holidays_cities_list = [datetime(year=year,month=6,day=13).date()]

	if city_id in ['75']:
		# Jundiaí SP
		holidays_cities_list = [datetime(year=year,month=8,day=15).date(),
								datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=11,day=20).date()]
	if city_id in ['95']:
		# Londrina PR
		holidays_cities_list = [datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=12,day=10).date()]

	if city_id in ['97']:
		# Maceió AL
		holidays_cities_list = [datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=6,day=29).date(),
								datetime(year=year,month=8,day=27).date(),
								datetime(year=year,month=9,day=16).date(),
								datetime(year=year,month=11,day=20).date(),
								datetime(year=year,month=11,day=30).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['61']:
		#Natal RN
		holidays_cities_list = [datetime(year=year,month=1,day=6).date(),
								datetime(year=year,month=10,day=3).date(),
								datetime(year=year,month=11,day=21).date()]

	if city_id in ['215']:
		#Nilópolis RJ
		holidays_cities_list = [datetime(year=year,month=4,day=23).date(),
								datetime(year=year,month=8,day=21).date(),
								datetime(year=year,month=11,day=20).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['74']:
		#Niterói RJ
		holidays_cities_list = [datetime(year=year,month=4,day=23).date(),
								datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=11,day=20).date(),
								datetime(year=year,month=11,day=22).date()]

	if city_id in ['156']:
		#Olinda PE
		holidays_cities_list = [datetime(year=year,month=3,day=6).date(),
								datetime(year=year,month=3,day=12).date(),
								datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=8,day=6).date(),
								datetime(year=year,month=11,day=10).date()]

	if city_id in ['218']:
		#Parnamirim RN
		holidays_cities_list = [datetime(year=year,month=5,day=13).date(),
								datetime(year=year,month=10,day=3).date(),
								datetime(year=year,month=12,day=17).date()]

	if city_id in ['320']:
		#Pinhais PR
		holidays_cities_list = [datetime(year=year,month=3,day=20).date()]

	if city_id in ['24']:
		#Porto Alegre RS
		holidays_cities_list = [datetime(year=year,month=2,day=2).date(),
								datetime(year=year,month=9,day=20).date()]

	if city_id in ['33']:
		#Recife PE
		holidays_cities_list = [datetime(year=year,month=3,day=6).date(),
								datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=7,day=16).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['49']:
		#Ribeirão Preto SP
		holidays_cities_list = [datetime(year=year,month=1,day=20).date(),
								datetime(year=year,month=6,day=19).date(),
								datetime(year=year,month=7,day=9).date()]

	if city_id in ['32']:
		#Salvador BA
		holidays_cities_list = [datetime(year=year,month=6,day=24).date(),
								datetime(year=year,month=7,day=2).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['79']:
		#Santos sp
		holidays_cities_list = [datetime(year=year,month=1,day=26).date(),
								datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=9,day=8).date(),
								datetime(year=year,month=11,day=20).date()]

	if city_id in ['321']:
		#Sao Jose sc
		holidays_cities_list = [datetime(year=year,month=3,day=19).date()]

	if city_id in ['144']:
		#Sao Jose Dos Pinhais pr
		holidays_cities_list = [datetime(year=year,month=1,day=8).date(),
								datetime(year=year,month=3,day=19).date()]

	if city_id in ['62']:
		#Sorocaba sp
		holidays_cities_list = [datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=11,day=20).date()]

	if city_id in ['59']:
		#São José do Rio Preto sp
		holidays_cities_list = [datetime(year=year,month=3,day=19).date(),
								datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=12,day=8).date()]

	if city_id in ['50']:
		#São José dos Campos sp
		holidays_cities_list = [datetime(year=year,month=11,day=20).date()]

	if city_id in ['130']:
		#Teresina pi
		holidays_cities_list = [datetime(year=year,month=10,day=19).date(),]

	if city_id in ['126']:
		#Uberlandia mg
		holidays_cities_list = [datetime(year=year,month=8,day=15).date(),
								datetime(year=year,month=8,day=31).date()]

	if city_id in ['233']:
		#Valinhos sp
		holidays_cities_list = [datetime(year=year,month=1,day=20).date(),
								datetime(year=year,month=7,day=9).date(),
								datetime(year=year,month=11,day=20).date()]

	return holidays_cities_list
