import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, sfx, cms
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
from lib import gsheets
## country_list 
df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Waiting Time CPGs')
df = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
df = df[(df["ON/OFF"] == 'ACTIVE')]
countries = df['COUNTRY'].to_list()
print(countries)

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    select store_id::text as storeid
    from ops_occ.waiting_time_cpgsnow_old
    where suspended_at >= convert_timezone('America/{timezone}',current_timestamp()) - interval '2h'
    and country='{country}'
    group by 1
    '''
    df = snow.run_query(query)
    return df

def enabled_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = f"""
    select s.store_id::text as storeid
    from stores s
    left join store_attribute sa on sa.store_id=s.store_id and suspended=true
    where sa.store_id is null
    and s.is_enabled=true
    group by 1
    """

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
    --no_cache
    SET TIMEZONE TO 'America/{timezone}';
    with a as (
    select 
      om.order_id,
      max(case when om.type = 'domiciliary_in_store' then om.created_at end) as rt_in_store,
      max(case when om.type = 'hand_to_domiciliary' then om.created_at end) as hand_to_rt,
      max(case when om.type ilike '%cancel%' then om.created_at end) as canceled_at,
      max(case when om.type = 'taken_visible_order' and params->>'storekeeper_id' is not null then om.created_at else null end) as rt_taken
      from order_modifications om 
      where 
      (om.type in 
      ('domiciliary_in_store', 'hand_to_domiciliary', 'taken_visible_order')
      or om.type ilike '%cancel%')
      and om.created_at > now() - interval '2h'
      group by 1)

      , b as (
      select os.name as store_name, os.store_id
      , o.cooking_time_started_at, cooking_time
      , rt_in_store
      , hand_to_rt
      , a.order_id
      , (cooking_time_started_at + ( cooking_time * interval '1 minute')) as cooking_time_ends_at
      , canceled_at
      , (extract(epoch from (canceled_at - rt_in_store)) / 60) as in_store_to_canceled
      , (extract(epoch from (hand_to_rt - rt_in_store)) / 60) as rt_waiting


      from a 
      join order_stores os on os.order_id=a.order_id 
      join orders o on o.id=a.order_id

      where rt_in_store is not null
      and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
      and not os.is_marketplace
      and o.state not in ('canceled_by_fraud',
             'canceled_for_payment_error',
             'canceled_by_split_error',
             'canceled_by_early_regret')
      )
      select store_id::text as store_id, store_name, count(distinct order_id) as cancels, string_agg(distinct order_id::text, ',') as order_ids
      from b
      where (rt_waiting >= 8 or in_store_to_canceled >= 8)
      group by 1,2
      --having count(distinct order_id) >= 15
        """.format(timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics

def store_infos(country):
    timezone, interval = timezones.country_timezones(country)

    query_slots = '''
    with ord as (select store_id, count(distinct order_id) orders from OPS_GLOBAL.global_orders
where lower(country) = '{country}'
and created_at::date >= current_date::date -7
group by 1
having orders >= 1)

    select s.store_id::text as storeid, ps.physical_store_id::text as physical_store_id, ps.name as physical_store_name, v.vertical,orders
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
    join {country}_PGLR_MS_STORES_PUBLIC.store_types st on st.id=s.type
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores ps on ps.store_id=s.store_id
    join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores ps2 on ps2.id=ps.physical_store_id
    join (select store_type                      as storetype,
                                   case
                                       when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                       when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                       when vertical_group = 'WHIM' then 'Antojos'
                                       when vertical_group = 'RAPPICASH' then 'RappiCash'
                                       when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                       when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                       when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
                                       when upper (store_type) in ('TURBO') then 'Turbo'
                                       when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
                                       when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                       when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                       when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                       when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                       when upper(vertical_group) in ('CPGS') then 'CPGs'
                                       else vertical_sub_group end as vertical
                            from VERTICALS_LATAM.{country}_VERTICALS_V2 v
       ) v on v.storetype = s.type
       left join ord o on o.store_id=s.store_id
    where st.purchase_type in ('now')
      and st.scheduled=false
      and v.vertical in ('Express','Farmacia','Licores')

group by 1,2,3,4,5'''.format(country=country)

    slots = snow.run_query(query_slots)

    return slots

def new_offenders(e, a,b,f):
    channel_alert = timezones.slack_channels(country)

    current_time = timezones.country_current_time(country)
    target_date = (current_time + timedelta(minutes=30))
    time_closed = int((target_date - current_time).total_seconds() / 60.0)

    try:
        df = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
        df = df.drop(['key_0','storeid'], axis=1)

        ##agrupar por physical 
        df1 = df.groupby(["physical_store_id","physical_store_name","vertical"])[["cancels"]].sum().reset_index()

        df_cancels_ids = df[(df['cancels'] >= 1)]

        df2 = df_cancels_ids.groupby(["physical_store_id"])["order_ids"].apply(list).reset_index(name='order_ids')
        df2['order_ids'] = [','.join(map(str, l)) for l in df2['order_ids']]
        df3 = df.groupby(["physical_store_id"])["store_id"].apply(list).reset_index(name='store_ids_1')

        df = pd.merge(df1,df2, how='left', on=['physical_store_id'])
        df = pd.merge(df,df3, how='left', on=['physical_store_id'])

        df = df.rename(columns={'physical_store_id': 'store_id', 'physical_store_name': 'store_name'})

        b = pd.merge(b,f, how='inner', on=['storeid'])
        b = b[(b['orders_'] >= 1)]

        df5 = pd.merge(df,b,how='inner',left_on=df['store_id'],right_on=b['physical_store_id'])
        df5 = df5.drop(['key_0'], axis=1)
        df5 = df5.groupby(["physical_store_id"])["storeid"].apply(list).reset_index(name='store_ids_2')
        df = pd.merge(df,df5, how='left', left_on=df['store_id'],right_on=df5['physical_store_id'])

        df = df[(df['cancels'] >= 10)]

        df = df.drop(['key_0'], axis=1)
        ##logs
        news = pd.merge(df,e,how='left',left_on=df['store_id'],right_on=e['storeid'])
        news = news[news['storeid'].isnull()]
        news = news.drop(['key_0','storeid'], axis=1)

        if not news.empty:
          news = news[["store_id","store_name","cancels","order_ids","store_ids_1",'store_ids_2']]
          for index, row in news.iterrows():
            rows = dict(row)
            store_name=row['store_name']
            store_id = int(row['store_id'])
            reason = "waiting_time_cpgs_now"
            store_ids_1 = row['store_ids_1']
            store_ids_2 = row['store_ids_2']
            store_ids= list(set(store_ids_1) | set(store_ids_2))
            print(store_ids)
            if len(store_ids) >= 1:
              for store_id in store_ids:
                print("as filhas")
                print(int(store_id))
                cms.disable_store(int(store_id), country, reason, time_closed)
                cms.turnStoreOff(int(store_id), time_closed, country, reason) == 200
                try:
                  print("sfx")
                  sfx.send_events_turnStoreOff(int(store_id), country, reason, current_time)
                except:
                  print("fail sfx")
            text = '''
            *Waiting Time CPGs Now* :cpgbags: 
                Country: :flag-{country}:
                Tienda Physical ID {store_id} suspendida hasta {data}
                Store Name: {store_name} :convenience_store:
                Orders con más de 8 minutes: {cancels}
                Orders IDs: {order_ids} 
                Store IDs: {store_ids}
                <@francisca.urzua> <@fernanda.gonzalez> <@ignacio.maschwitz> <@sofia.lueiza> <@ext-c.leiva> <@lucia.gibert>
               <@edgar.zarraga>
                '''.format(
                    store_id=row['store_id'],
                    store_name=store_name,
                    cancels=row['cancels'],
                    store_ids=store_ids,
                    order_ids=row['order_ids'],
                    data=target_date.strftime("%m/%d/%Y %H:%M:%S"),
                    country=country
                    )
            slack.bot_slack(text,'C01S0667VKP')
            print(text)

          news['country'] = country
          news['suspended_at'] = current_time
          news['suspended_until'] = target_date
          news = news[['store_id', 'cancels', 'suspended_at', 'suspended_until', 'order_ids', 'vertical', 'country']]
          snow.upload_df_occ(news, 'waiting_time_cpgsnow_old')

        else:
          print('no son cpgs now')

    except Exception as e:
        print(e)


def run_process(country):
    metrics = offenders(country)
    if not metrics.empty:
        logs = excluding_stores(country)
        slots = store_infos(country)
        f = enabled_stores(country)
        new_offenders(logs, metrics, slots, f)
    else:
        print('empty df')


for country in countries:
  country = country.lower()
  try:
    print(country)
    run_process(country)
  except Exception as e:
    print(e)