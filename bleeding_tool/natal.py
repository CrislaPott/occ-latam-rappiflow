import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
import time
import datetime as dt


def get_orders(country):
	timezone, interval = timezones.country_timezones(country)

	query = """
	SET TIMEZONE TO 'America/{timezone}';
	select os.store_id, os.name, os.type, count(distinct o.id) as orders, string_agg(distinct o.id::text,',') as order_ids
	from orders o
	left join order_stores os on os.order_id=o.id
	where o.created_at >= now()::timestamp - interval '30 minutes'
	and type not in ('queue_order_flow')
	and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
	group by 1,2,3
	""".format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(7973, query)
	elif country == 'ar':
		metrics = redash.run_query(7970, query)
	elif country == 'cl':
		metrics = redash.run_query(7972, query)
	elif country == 'mx':
		metrics = redash.run_query(7977, query)
	elif country == 'uy':
		metrics = redash.run_query(7978, query)
	elif country == 'ec':
		metrics = redash.run_query(7975, query)
	elif country == 'cr':
		metrics = redash.run_query(7974, query)
	elif country == 'pe':
		metrics = redash.run_query(7976, query)
	elif country == 'br':
		metrics = redash.run_query(7971, query)

	return metrics

def store_infos(country):
	timezone, interval = timezones.country_timezones(country)

	query_stores = f'''
	select s.store_id, case when sh.physical_store_id is not null then 'CPGs/Ecomm - Shopper'
                        when v2.sub_group in ('Super','Hiper') then 'CPGs/Ecomm - Shopper'
                        else v.vertical
                   end as vertical
	from {country}_PGLR_MS_STORES_PUBLIC.stores_vw s
	left join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s2 on s2.store_id=s.store_id
	left join {country}_PGLR_MS_STORES_PUBLIC.verticals v2 on v2.store_type=s.type
	left join (select distinct physical_store_id from {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.shifts
	    where starts_at::date between '2022-01-08' and '2022-01-30' and shopper_id is not null) sh on sh.physical_store_id = s2.physical_store_id
	join (select store_type as storetype,
	case
	when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	when vertical_group = 'WHIM' then 'AFC'
	when vertical_group = 'RAPPICASH' then 'AFC'
	when vertical_group = 'RAPPIFAVOR' then 'AFC'
	when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'CPGs'
	when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
	when upper (store_type) in ('TURBO') then 'Turbo'
	when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
	when upper(vertical_sub_group) in ('PHARMACY') then 'CPGs Sin Shopper'
	when upper(vertical_sub_group) in ('LIQUOR') then 'CPGs Now'
	when upper(vertical_sub_group) in ('EXPRESS') and '{country}' = 'br' then 'CPGs Sin Shopper'
	when upper(vertical_sub_group) in ('EXPRESS') then 'CPGs Now'
	when upper(vertical_sub_group) in ('SPECIALIZED') then 'CPGs Sin Shopper'
	when upper(vertical_group) in ('CPGS') then 'CPGs'
	else vertical_sub_group end as vertical
	from VERTICALS_LATAM.{country}_VERTICALS_V2 v
	) v on v.storetype = s.type
where vertical in ('CPGs Sin Shopper','Ecommerce','CPGs/Ecomm - Shopper')
and (case when '{country}' in ('ec') then s.city_address_id=200 end)
group by 1,2
	'''
	
	df = snow.run_query(query_stores)

	return df

def metrics(df,stores):
	current_time = timezones.country_current_time(country)

	df = pd.merge(df, stores, how='inner',on=['store_id'])
	df0 = df.groupby(["vertical"])[["orders"]].sum().reset_index()
	df1 = df.groupby(["vertical"])["order_ids"].apply(list).reset_index(name='order_ids')
	df1['order_ids'] = [','.join(map(str, l)) for l in df1['order_ids']]
	df2 = df.groupby(["vertical"])["store_id"].apply(list).reset_index(name='store_ids')
	df2['store_ids'] = [','.join(map(str, l)) for l in df2['store_ids']]
	df = pd.merge(df0, df1, how='inner',on=['vertical'])
	df = pd.merge(df, df2, how='inner',on=['vertical'])

	google_sheet_url = "https://docs.google.com/spreadsheets/d/1PfOP8zvPF59iBESr8BJWlJfj4lnfxMLZlpcalML-jIM/export?format=csv&gid=75150465"
	sheets = pd.read_csv(google_sheet_url, error_bad_lines=False)
	sheets = sheets[sheets['country'] == country.upper()]

	df24 = sheets[['country','vertical','starts_at_fixo','ends_at_fixo']]
	df25 = sheets[['country','vertical','starts_at_fixo','ends_at_fixo']]
	print(current_time.date())

	print(df24)
	print(df25)

	if current_time.date() == dt.datetime(2022, 1, 10).date():
		df_schedules = df24[(df24['ends_at_fixo'] <= current_time.strftime("%H:%M:%S"))]
		print(df_schedules)
	elif current_time.date() == dt.datetime(2022, 1,11).date():
		df_schedules = df25[ (df25['ends_at_fixo'] <= current_time.strftime("%H:%M:%S")) | (df25['starts_at_fixo'] >= current_time.strftime("%H:%M:%S"))]

	df = pd.merge(df, df_schedules, how='inner',on=['vertical'])
	df = df[df['orders'] >= 5]
	print(df)

	for index, row in df.iterrows():
		text = '''
		*TOQUE DE QUEDA PERÚ/ECUADOR - Tiendas Prendidas Despues Horario limite  :clock930:
		:alert:

		*Country* :flag-{country}:

		*Vertical*: {vertical}

		*Orders:* {orders}

		:page_facing_up: *Orders IDs:* {order_ids}

		:page_with_curl:  *Store IDs:* {store_ids}

		'''.format(
		vertical=row['vertical'],
		order_ids=row['order_ids'],
		store_ids=row['store_ids'],
		orders=row['orders'],
		country=country
		)
		print(text)
		slack.bot_slack(text, 'C029MJCAMS6')

	df['sync_at'] = current_time
	df['country'] = country
	snow.upload_df_occ(df, 'schedule_shutdown')


countries = ['ec','pe']
for country in countries:
	try:
		orders = get_orders(country)
		stores = store_infos(country)
		metrics(orders,stores)
	except Exception as e:
		print(e)
