import requests
import numpy as np 
import pandas as pd 
from lib import gsheets

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

def get_url_apply_eventualities(country):
	mappings = {
		'br': 'https://services.rappi.com.br/api/operation-management/zones/eventualities',
		'co': 'http://services.rappi.com/api/operation-management/zones/eventualities',
		'cl': 'http://services.rappi.cl/api/operation-management/zones/eventualities',
		'pe': 'https://services.rappi.pe/api/operation-management/zones/eventualities',
		'uy': 'https://services.rappi.com.uy/api/operation-management/zones/eventualities',
		'ar': 'http://services.rappi.com.ar/api/operation-management/zones/eventualities',
		'mx': 'https://services.mxgrability.rappi.com/api/operation-management/zones/eventualities',
		'ec': 'https://services.rappi.com.ec/api/operation-management/zones/eventualities',
		'cr': 'https://services.rappi.co.cr/api/operation-management/zones/eventualities'
	}

	return mappings[country]

def get_url_apply_payments(country):
	mappings = {
		'br': 'https://services.rappi.com.br/api/operation-management/zones/payment-methods',
		'co': 'http://services.rappi.com/api/operation-management/zones/payment-methods',
		'cl': 'http://services.rappi.cl/api/operation-management/zones/payment-methods',
		'pe': 'https://services.rappi.pe/api/operation-management/zones/payment-methods',
		'uy': 'https://services.rappi.com.uy/api/operation-management/zones/payment-methods',
		'ar': 'http://services.rappi.com.ar/api/operation-management/zones/payment-methods',
		'mx': 'https://services.mxgrability.rappi.com/api/operation-management/zones/payment-methods',
		'ec': 'https://services.rappi.com.ec/api/operation-management/zones/payment-methods',
		'cr': 'https://services.rappi.co.cr/api/operation-management/zones/payment-methods'
	}

	return mappings[country]

def get_minimum_event_action(event_action):
	try:
		event_action = [event.replace('#', '') for event in event_action]
		event_action = [int(event) for event in event_action]
		event_action = sorted(event_action)
		event_action = event_action[0]
		return event_action
	except:
		return 'nan'

def run_polygon_action(country, city, zone, current_raining, desired_raining):
	if city in ['fictioncity']:
		return put_polygon_action(country, city, zone, desired_raining[0], token=token)
	elif current_raining != 'nan' and 'nan' in desired_raining:
		return put_polygon_action(country, city, zone, 'nan', token=token)
	elif current_raining in ['moderate', 'heavy', 'violent'] and 'light' in desired_raining:
		return put_polygon_action(country, city, zone, 'light', token=token)
	elif current_raining in ['heavy', 'violent'] and 'moderate' in desired_raining:
		return put_polygon_action(country, city, zone, 'moderate', token=token)
	elif current_raining in ['violent'] and 'heavy' in desired_raining:
		return put_polygon_action(country, city, zone, 'heavy', token=token)
	else:
		return 'Nao'

def run_frete_action(country, city, zone, current_event, desired_event):
	try:
		if city in ['fictioncity']:
			return put_frete_action(country, city, zone, desired_event)
		#elif city in ['saopaulo','curitiba','belohorizonte','natal','portoalegre','campinas','florianopolis','fortaleza','brasilia','goiania','sanjosedoscampos','recife','salvador','ribeiraopreto','riodejaneiro','sanjosedoriopreto','joaopessoa','belem','vitoria','manaus','aracaju','santos','blumenau','blumenau','maringa','campogrande','londrina','juizdefora','anapolis','caldasnovas','uberlandia','uberlandia','mogidascruzes','caxiasdosul','teresina','teresina','bauru','campinagrande','uberaba','cuiaba','caruaru','caruaru','voltaredonda','limeira','indaiatuba','montesclaros','montesclaros','fozdoiguacu','santarem','novafriburgo','governadorvaladares','portovelho','portovelho','boavista','pocosdecaldas','pocosdecaldas','marilia','saoluis','presidenteprudente','barreiras','macapa','macapa']:
			#return 'Nao'
		elif city in ['concepcion','talca','rancagua','santiagodechile','puertomontt','calama','laserena','temuco','iquique','antofagasta','vinadelmar','sanjose','lima', 'goiania', 'natal','saopaulo', 'portoalegre', 'campinas', 'jundiai', 'ribeiraopreto', 'santos', 'sorocaba', 'sanjosedoriopreto', 'sanjosedoscampos', 'buenosaires', 'rosario', 'cordoba', 'pinamar', 'tucuman', 'mardelplata', 'laplata', 'mendoza', 'belohorizonte', 'brasilia', 'maceio', 'joaopessoa', 'montevideo', 'cali', 'riodejaneiro', 'curitiba', 'salvador','recife','fortaleza']:
			return 'Nao'
		elif current_event != 'nan' and 'nan' in desired_event:
			return put_frete_action(country, city, zone, desired_event) 
		elif int(current_event) > get_minimum_event_action(desired_event):
			return put_frete_action(country, city, zone, desired_event)
		else:
			return 'Nao'
	except:
		return 'Nao'

def run_payment_action(country, city, zone, payment_methods, desired_payment_methods):
	if city in ['fictioncity']:
		return put_payment_action(country, city, zone, desired_payment_methods)
	elif 'nan' not in payment_methods and 'nan' in desired_payment_methods:
		return put_payment_action(country, city, zone, desired_payment_methods)
	elif (payment_methods != 'cash') and (payment_methods != 'nan') and ('cash' in desired_payment_methods):
		return put_payment_action(country, city, zone, desired_payment_methods)
	else:
		return 'Nao'

def put_polygon_action(country, city, zone, raining_action, token=token):
	try:
		zone_name = '{}|{}|{}'.format(
			country.upper(),
			city.lower(),
			zone.lower()
		)

		event_duration = 10

		if raining_action == 'nan':
			raining_action = 'none'
			event_duration = -1

		url = get_url_apply_eventualities(country)
		headers = {
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0 Waterfox/78.8.0',
			'Accept': 'application/json, text/plain, */*',
			'Accept-Language': 'en-US,en;q=0.5',
			'Authorization': token,
			'Content-Type': 'application/json;charset=utf-8',
		}

		data = '{"zones":[{"name":"%(zone_name)s","eventualities":{"raining":"%(raining_action)s","raining_duration":%(event_duration)s},"scenarios":[{"type":"weather","value":"%(raining_action)s","duration":%(event_duration)s}]}]}' % {
			'zone_name': zone_name, 
			'raining_action': raining_action, 
			'event_duration': event_duration
		}

		response = requests.put(url, headers=headers, data=data)
		if response.status_code == 200:
			return 'Sim'
	except:
		return 'Nao'

def put_frete_action(country, city, zone, event_action, token=token):
	try:
		zone_name = '{}|{}|{}'.format(
			country.upper(),
			city.lower(),
			zone.lower()
		)

		if 'nan' in event_action:
			event_action = 1
			event_duration = -1
		else:
			event_action = get_minimum_event_action(event_action)
			event_duration = 10

		url = get_url_apply_eventualities(country)
		headers = {
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0 Waterfox/78.8.0',
			'Accept': 'application/json, text/plain, */*',
			'Accept-Language': 'en-US,en;q=0.5',
			'Authorization': token,
			'Content-Type': 'application/json;charset=utf-8',
		}

		data = '{"zones":[{"name":"%(zone_name)s","eventualities":{"level":%(event_action)s,"level_duration":%(event_duration)s,"verticals":[{"value":"Antojos","label":"Antojos"},{"value":"CPGs","label":"CPGs"},{"value":"Restaurantes","label":"Restaurantes"},{"value":"Rappicash","label":"Rappicash"},{"value":"Rappifavor","label":"Rappifavor"},{"value":"ecommerce","label":"ECommerce"}],"rt_types":[{"value":"walkers","label":"Walkers","id":0},{"value":"bikes","label":"Bikes","id":1},{"value":"motorbikes","label":"Motorbikes","id":2},{"value":"cars","label":"Cars","id":3}]}}]}' % {
			'zone_name': zone_name, 
			'event_action': event_action,
			'event_duration': event_duration
		}

		response = requests.put(url, headers=headers, data=data)
		if response.status_code == 200:
			return 'Sim'
	except:
		return 'Nao'

def put_payment_action(country, city, zone, payment_methods, token=token):
	try:
		zone_name = '{}|{}|{}'.format(
			country.upper(),
			city.lower(),
			zone.lower()
		)

		url = get_url_apply_payments(country)
		headers = {
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0 Waterfox/78.8.0',
			'Accept': 'application/json, text/plain, */*',
			'Accept-Language': 'en-US,en;q=0.5',
			'Authorization': token,
			'Content-Type': 'application/json;charset=utf-8',
		}

		if 'nan' in payment_methods:
			data = {
				"zones":[
					"{}".format(zone_name)
				],
				"payment_methods":[],
				"duration":-1
			}
		elif 'cash' in payment_methods:
			data = {
				"zones":[
					"{}".format(zone_name)
				],
				"payment_methods":['cash'],
				"duration":10
			}

		data = str(data).replace("\'", '\"')
		# print(data)


		response = requests.put(url, headers=headers, data=data)
		if response.status_code == 200:
			return 'Sim'
	except:
		return 'Nao'
