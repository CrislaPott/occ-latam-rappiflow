import requests
import numpy as np 
import pandas as pd 
import transformers as tr 
from lib import gsheets

token = gsheets.read_sheet('1oOK4jNmedWSKpzrx11i3J7vhFeNh12_iLzTyCFeQsKM', 'token')
token = token.loc[1, 'Token'].strip()

def get_url_metrics(country):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/zones/live-ops',
        'co': 'http://services.rappi.com/api/operation-management/zones/live-ops',
        'cl': 'http://services.rappi.cl/api/operation-management/zones/live-ops',
        'pe': 'https://services.rappi.pe/api/operation-management/zones/live-ops',
        'uy': 'https://services.rappi.com.uy/api/operation-management/zones/live-ops',
        'ar': 'http://services.rappi.com.ar/api/operation-management/zones/live-ops',
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/zones/live-ops',
        'ec': 'https://services.rappi.com.ec/api/operation-management/zones/live-ops',
        'cr': 'https://services.rappi.co.cr/api/operation-management/zones/live-ops'
    }

    return mappings[country]

def get_url_events(country, city):
    mappings = {
        'br': 'https://services.rappi.com.br/api/operation-management/locations/cities/{}/zones'.format(city),
        'co': 'http://services.rappi.com/api/operation-management/locations/cities/{}/zones'.format(city),
        'cl': 'http://services.rappi.cl/api/operation-management/locations/cities/{}/zones'.format(city),
        'pe': 'https://services.rappi.pe/api/operation-management/locations/cities/{}/zones'.format(city),
        'uy': 'https://services.rappi.com.uy/api/operation-management/locations/cities/{}/zones'.format(city),
        'ar': 'http://services.rappi.com.ar/api/operation-management/locations/cities/{}/zones'.format(city),
        'mx': 'https://services.mxgrability.rappi.com/api/operation-management/locations/cities/{}/zones'.format(city),
        'ec': 'https://services.rappi.com.ec/api/operation-management/locations/cities/{}/zones'.format(city),
        'cr': 'https://services.rappi.co.cr/api/operation-management/locations/cities/{}/zones'.format(city)
    }

    return mappings[country]

def get_events(country, city, token=token):
    url = get_url_events(country, city)
    payload = {}
    headers = {
        'Authorization': token
    }

    events = pd.DataFrame()

    response = requests.request("GET", url, headers=headers, data=payload)
    results = response.json()
    for result in results:
        zone_name = result['name'].split('|')[2]
        event = result['event']
        payment_methods = ','.join(list(result['payment_methods'].keys()))

        inserts = [{
            'zone_name': zone_name,
            'event': event,
            'payment_methods': payment_methods
        }]

        inserts = pd.DataFrame(inserts)
        events = pd.concat([events, inserts], ignore_index=True)

    return events


def get_metrics(country, token=token):
    url = get_url_metrics(country)
    payload = {}
    headers = {
        'Authorization': token
    }

    response = requests.request("GET", url, headers=headers, data=payload)
    results = response.json()

    metrics = pd.DataFrame()
    
    for result in results:
        city = result['city']
        zone_id = result['id']
        zone_name = result['name']
        orders = result['total_orders']
        rts = result['total_couriers']
        rts_livres = result['health_check']['free_couriers']
        polygonos = result['demand_mgmt']['triggers']['po']
        saturation = result['saturation']
        unn_orders = result['orders_unassigned']
        cash_off = result['demand_mgmt']['triggers']['pm']

        inserts = [{
            'city': city,
            'zone_id': zone_id,
            'zone_name': zone_name,
            'orders': orders,
            'rts': rts,
            'rts_livres': rts_livres,
            'polygonos': polygonos,
            'cash_off': cash_off,
            'saturation': saturation,
            'unn_orders': unn_orders
        }]
        inserts = pd.DataFrame(inserts)
        metrics = pd.concat([metrics, inserts], ignore_index=True)

    metrics['country'] = country
    metrics['saturation'] = metrics['orders'] / metrics['rts']
    metrics['unn_perc'] = (metrics['unn_orders'] / metrics['orders'])*100

    cities = metrics['city'].unique().tolist()
    events = pd.DataFrame()

    for city in cities:
        city_events = get_events(country, city)
        events = pd.concat([events, city_events], ignore_index=True)

    metrics = metrics.merge(events, on='zone_name', how='left')
    metrics = tr.transform_metrics(metrics)

    return metrics
