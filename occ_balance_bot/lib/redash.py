import os, requests, pprint
import numpy as np
import pandas as pd 
from dotenv import load_dotenv
from redashAPI import RedashAPIClient
from airflow.models import Variable

load_dotenv()

Redash = RedashAPIClient(Variable.get('REDASH_API_KEY'), 'https://redash.rappi.com')

def get_databases(pattern):
    databases = pd.DataFrame(Redash.get('data_sources').json())
    filtered = databases[databases['name'].str.contains(pattern)]
    return filtered[['id', 'name']]

def get_query_details(query_id):
    redash_url = 'https://redash.rappi.com'
    api_key = Variable.get('REDASH_API_KEY')
    s = requests.Session()
    s.headers.update({'Authorization': 'Key {}'.format(api_key)})
    
    response = s.get('{}/api/queries/{}'.format(redash_url, query_id))
    response = response.json()

    return response

def run_query(database_id, query):
    results = Redash.query_and_wait_result(database_id, query, 120)
    df = df = pd.DataFrame(results.json()['query_result']['data']['rows'])
    return df

def run_slow_query(database_id, query):
    results = Redash.query_and_wait_result(database_id, query, 1800)
    df = df = pd.DataFrame(results.json()['query_result']['data']['rows'])
    return df

def run_query_id(query_id):
    redash_url = 'https://redash.rappi.com'
    api_key = Variable.get('REDASH_API_KEY')
    s = requests.Session()
    s.headers.update({'Authorization': 'Key {}'.format(api_key)})
    
    response = s.get('{}/api/queries/{}'.format(redash_url, query_id))
    response = response.json()

    database_id = response['data_source_id']
    query_text = response['query']

    df = run_query(database_id, query_text)
    return df

