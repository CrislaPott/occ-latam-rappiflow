import os
from dotenv import load_dotenv
from slack import WebClient
from slack.errors import SlackApiError
from airflow.models import Variable

load_dotenv()

client = WebClient(token=Variable.get("SLACK_TOKEN"))

def send_message_channel(channel, text):
    client.chat_postMessage(channel=channel, text=text)
    
def send_message_names_channel(channel, text):
    client.chat_postMessage(channel=channel, text=text, link_names=1)
    
def send_message_thread(channel, thread_ts, text):
    client.chat_postMessage(channel=channel, text=text, thread_ts=thread_ts)
    
def file_upload_channel(channel, text, filename, filetype):
    client.files_upload(file=filename,filename=filename,channels=channel,title=filename,initial_comment=text,filetpye=filetype)

def file_upload_thread(channel, thread_ts, text, filename, filetype):
    client.files_upload(file=filename,filename=filename,channels=channel,title=filename,initial_comment=text,filetpye=filetype,thread_ts=thread_ts)