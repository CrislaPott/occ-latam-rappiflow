import numpy as np
import pandas as pd
import transformers as tr
from lib import gsheets


def get_playbook_rules(playbook_sheet):
    rules = gsheets.read_sheet(playbook_sheet, 'rules')
    rules = tr.transform_rules(rules)

    return rules

def get_playbook_zones(playbook_sheet):
    zones = gsheets.read_sheet(playbook_sheet, 'zones')
    return zones

