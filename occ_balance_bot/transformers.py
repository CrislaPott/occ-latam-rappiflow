import numpy as np 
import pandas as pd 

def strip_in_list(target_list):
    return [element.strip() for element in target_list]

def replace_nulls(text):
    if text == 'none':
        return 'nan'
    elif text == '':
        return 'nan'
    else:
        return text

def transform_metrics(metrics):
    for column in ['event', 'payment_methods']:
        metrics[column] = metrics[column].astype(str) 
        metrics[column] = metrics[column].apply(lambda x: replace_nulls(x))
        metrics[column] = metrics[column].str.strip()

    metrics['event'] = metrics['event'].replace({'1': 'nan'})
    metrics['event'] = metrics['event'].str.strip()
    
    return metrics
        
def transform_rules(rules):
    threshold_columns = ['saturation_from', 'saturation_to', 'unassigned_from', 'unassigned_to', 'unn_min', 'polygon','cash']
    columns = [column for column in rules.columns if column not in threshold_columns]

    for column in ['unassigned_from', 'unassigned_to']:
        rules[column] = rules[column].str.replace('%', '')
        rules[column] = rules[column].astype(float)

    for column in ['saturation_from', 'saturation_to']:
        rules[column] = rules[column].str.replace(',', '.')
        rules[column] = rules[column].astype(float)

    for column in ['unn_min']:
        rules[column] = rules[column].astype(float)


    for column in columns:
        rules[column] = rules[column].astype(str) 
        rules[column] = rules[column].str.split(',')
        rules[column] = rules[column].apply(lambda x: strip_in_list(x))

    return rules
