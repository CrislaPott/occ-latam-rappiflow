import os, sys, json 
import numpy as np 
import pandas as pd 
from lib import gsheets

database = gsheets.read_sheet('10cjTjxiKVgW54CyzAAr4ieQDpx2hi7iYJV6MnCVPsfE', 'db')
database = database.sample(frac=1)
database["Modo Acción Bot"].replace({"enabled": "bot", "disabled": "alarm"}, inplace=True)
print(database)

json_db_br = {}
json_db_latam = {}

for index, row in database.iterrows():
    country = row['Pais'].strip()
    dag_name = row['Ciudad o Grupo'].strip()
    playbook_sheet = row['Playbook ID'].strip()
    slack_channel = row['Channel ID'].strip()
    action_mode = row['Modo Acción Bot'].strip()

    if country == 'BR':
        json_db_br["{}".format(index)] = {
            'country': country,
            'dag_name': dag_name,
            'playbook_sheet': playbook_sheet,
            'slack_channel': slack_channel,
            'action_mode': action_mode
        }
    else:
        json_db_latam["{}".format(index)] = {
            'country': country,
            'dag_name': dag_name,
            'playbook_sheet': playbook_sheet,
            'slack_channel': slack_channel,
            'action_mode': action_mode
        }

    with open('dag_arguments_br.json', 'w', encoding='utf-8') as f:
        json.dump(json_db_br, f, ensure_ascii=False, indent=4)
    with open('dag_arguments_latam.json', 'w', encoding='utf-8') as f:
        json.dump(json_db_latam, f, ensure_ascii=False, indent=4)