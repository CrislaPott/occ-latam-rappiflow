import os, sys, arrow
import numpy as np 
import pandas as pd 
import actions as act
from lib import slack
from lib import snowflake as snow
from os.path import expanduser


def compute_matches(metrics, rules, zones):
    target_zones = zones['ID Zona'].unique().tolist()
    target_zones = [int(i) for i in target_zones]

    metrics = metrics[metrics['zone_id'].isin(target_zones)]

    metrics['key'] = 0
    rules['key'] = 0
    matches = metrics.merge(rules, on='key')
    del matches['key']

    matches = matches[
        (matches['unassigned_from'] <= matches['unn_perc']) &
        (matches['saturation_from'] <= matches['saturation']) &
        (matches['unn_perc'] < matches['unassigned_to']) &
        (matches['unn_orders'] > matches['unn_min'])
    ]

    metrics.drop_duplicates(subset=['zone_id'], keep='first', inplace=True)

    return matches


def run_passive_alarm(matches, slack_channel):
    for index, row in matches.iterrows():
        country = row['country']
        country_upper = country.upper()
        city = row['city']
        zone_name = row['zone_name']

        orders = row['orders']
        unn_orders = row['unn_orders']
        rts = row['rts']
        saturation = round(row['saturation'], 2)
        unn_perc = round(row['unn_perc'], 2)
        polygonos = round(row['polygonos'], 2)
        cash_off = round(row['cash_off'], 2)

        if polygonos > 0 and cash_off > 0:
            trigger_text = f"""
            :flag-{country}: *Alarma de Playbook Balance - {country_upper}* :flag-{country}:
    
            <!subteam^SJWTS2PSB>
    
            *Ciudad*: {city}
            *Zona*: {zone_name}
            *Órdenes*: {orders}
            *Órdenes Unassigned*: {unn_orders}
            *RTs*: {rts}
            *Saturación*: {saturation}
            *Unassigned Percentual*: {unn_perc}%
            *Autobalanceo polygon:* :large_green_circle: {polygonos}%
            *Autobalanceo métodos de pago:* :large_green_circle: {cash_off}%
            """
        elif polygonos == 0 and cash_off > 0:
            trigger_text = f"""
            :flag-{country}: *Alarma de Playbook Balance - {country_upper}* :flag-{country}:

            <!subteam^SJWTS2PSB>

            *Ciudad*: {city}
            *Zona*: {zone_name}
            *Órdenes*: {orders}
            *Órdenes Unassigned*: {unn_orders}
            *RTs*: {rts}
            *Saturación*: {saturation}
            *Unassigned Percentual*: {unn_perc}%
            *Autobalanceo polygon:* :red_circle: {polygonos}%
            *Autobalanceo métodos de pago:* :large_green_circle: {cash_off}%
            """
        elif polygonos > 0 and cash_off == 0:
            trigger_text = f"""
            :flag-{country}: *Alarma de Playbook Balance - {country_upper}* :flag-{country}:

            <!subteam^SJWTS2PSB>

            *Ciudad*: {city}
            *Zona*: {zone_name}
            *Órdenes*: {orders}
            *Órdenes Unassigned*: {unn_orders}
            *RTs*: {rts}
            *Saturación*: {saturation}
            *Unassigned Percentual*: {unn_perc}%
            *Autobalanceo polygon:* :large_green_circle: {polygonos}%
            *Autobalanceo métodos de pago:* :red_circle: {cash_off}%
            """

        print(trigger_text)
        slack.send_message_names_channel(slack_channel, trigger_text)

def run_alarm(matches, slack_channel, action_mode):
    if matches.shape[0] == 0:
        print('Sem casos reportados')
        sys.exit(0)
    print("running alarm")
    if action_mode == 'disabled':
        run_passive_alarm(matches, slack_channel)
        matches['datetime_utc'] = str(arrow.utcnow())
        for column in matches.columns.tolist():
            matches[column] = matches[column].astype(str)

        snow.upload_df_occ(matches, 'occ_balance_bot_logs_alarm')

    else:
        print('error')
