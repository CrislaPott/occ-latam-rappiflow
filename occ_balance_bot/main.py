import os, sys
import time
import live_ops as lops
import playbook as play
import balance_bot as bot
from os.path import expanduser

# Exemplo de chamada - python main.py cl 1oqW-cU1aLTuo8LehUJb_OmqdvIULkwUosBBIIP8HFB0 G01PAG7N6B1 passive
if __name__ == "__main__":
    country = sys.argv[1]
    playbook_sheet = sys.argv[2]
    slack_channel = sys.argv[3]
    action_mode = sys.argv[4]

    metrics = lops.get_metrics(country)
    print(metrics)
    time.sleep(3)
    rules = play.get_playbook_rules(playbook_sheet)
    print(rules)
    time.sleep(3)
    zones = play.get_playbook_zones(playbook_sheet)
    print(zones)
    matches = bot.compute_matches(metrics, rules, zones)
    print(matches)
    bot.run_alarm(matches, slack_channel, action_mode)
    if action_mode == 'bot':
        matches = bot.compute_matches(metrics, rules, zones)
        bot.run_alarm(matches, slack_channel, 'disabled')
    print("end")