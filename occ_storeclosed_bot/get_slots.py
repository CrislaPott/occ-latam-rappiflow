import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash
from lib import snowflake as snow
from functions import timezones

def slots_to_close(country,physical_store_id,time):
    timezone, interval = timezones.country_timezones(country)
    query = f"""
        --no_cache
        set timezone= 'America/{timezone}';
        select id as slot_id
        from
        slots
        where
        (datetime_utc_iso::timestamp)::date = date(now()::timestamp)::date
        and datetime_utc_iso::timestamp >= now()::timestamp - interval '1h'
        and datetime_utc_iso::timestamp <= '{time}'
        and physical_store_id in ({physical_store_id})
        and deleted_at is null
        and is_closed=false
        """
    if country == 'co':
        capacidade_slots = redash.run_query(1971, query)
    elif country == 'ar':
        capacidade_slots = redash.run_query(1968, query)
    elif country == 'cl':
        capacidade_slots = redash.run_query(1970, query)
    elif country == 'mx':
        capacidade_slots = redash.run_query(1972, query)
    elif country == 'uy':
        capacidade_slots = redash.run_query(1974, query)
    elif country == 'ec':
        capacidade_slots = redash.run_query(2556, query)
    elif country == 'cr':
        capacidade_slots = redash.run_query(2558, query)
    elif country == 'pe':
        capacidade_slots = redash.run_query(1973, query)
    elif country == 'br':
        capacidade_slots = redash.run_query(1969, query)

    return capacidade_slots['slot_id'].tolist()
