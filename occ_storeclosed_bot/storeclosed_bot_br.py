import os, sys, json
import numpy as np
import pandas as pd
from random import randrange
from datetime import datetime, timedelta
from lib import slack, redash, erp, cms, sfx
from lib import snowflake as snow
from functions import timezones
from functions import diasfestivos
import get_slots as gs
import pytz
import get_data_madrugada as gd
import get_data_manha as gdm
import get_data_tarde as gdt
from lib import gsheets


def suspendidas(country):
    query = '''
    select s.store_id::text as suspendida_id, is_enabled,suspended
    from stores s 
    join store_attribute sa on sa.store_id = s.store_id
    where suspended=true
   
    '''

    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics


def suspendidas_cms_logger(country):
    query = f'''with base as (select distinct store_id,
action,
COALESCE(CONTENT_IS_ENABLE, CONTENT_IS_ENABLED) as CONTENT_IS_ENABLE,
created_at,
rank() OVER (PARTITION BY COUNTRY, STORE_ID ORDER BY CREATED_AT DESC) as rank
from ops_occ.global_cms_logger
where country = '{country}')

select store_id as storeid,
action,
CONTENT_IS_ENABLE,
created_at
from base
where rank = 1'''
    df = snow.run_query(query)
    return df

df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Store Closed - Tiendas CPGs NOW')
df = df[(df["ON/OFF"] == 'ACTIVE') & (df["COUNTRY"].isin(['BR']))]
countries = df['COUNTRY'].unique()
print(countries)


def dataframe(country, tod):
    if tod in ['madrugada']:
        excluding_stores, excluding_brands, excluding_physical_stores, store_infos, base_orders, store_closed_om, chats = gd.data_frames(
            country)
    elif tod in ['manha']:
        excluding_stores, excluding_brands, excluding_physical_stores, store_infos, base_orders, store_closed_om, chats = gdm.data_frames(
            country)
    elif tod in ['tarde']:
        excluding_stores, excluding_brands, excluding_physical_stores, store_infos, base_orders, store_closed_om, chats = gdt.data_frames(
            country)

    cancellation = base_orders[(base_orders['order_id'].notnull())]
    cancellation_super = base_orders[((base_orders['order_id'].notnull()) & (base_orders['bug_place_at'] == 'no'))]
    print(cancellation_super)

    base_orders = base_orders.groupby(["storeid"])["all_order_id"].count().reset_index(name="total_orders")

    ## Final DF for store closed and predictive store closed
    try:
        df = pd.merge(cancellation, store_closed_om, how='left', left_on=cancellation['order_id'],
                      right_on=store_closed_om['order_id_modifications'])
        df = df.drop(['key_0'], axis=1)
    except:
        df = cancellation
        df['order_id_modifications'] = 0

    try:
        df = pd.merge(df, chats, how='left', left_on=df['order_id'], right_on=chats['orderid'])
        df = df.drop(['key_0'], axis=1)
    except:
        df = df
        df['orderid'] = 0

    df['order_id_modifications'] = df['order_id_modifications'].fillna(0)
    df['orderid'] = df['orderid'].fillna(0)
    df = df[(df['order_id_modifications'] != 0) | (df['orderid'] != 0)]
    df['order_id_canceled'] = np.where(df['order_id_modifications'] != 0, df['order_id_modifications'], df['orderid'])
    df = df.drop(['order_id', 'orderid', 'order_id_modifications'], axis=1)

    df1 = df.groupby(["storeid", "store_name"])["order_id_canceled"].count().reset_index(name="total_cancels")
    df2 = df.groupby(["storeid", "store_name"])["order_id_canceled"].apply(list).reset_index(name='order_ids')

    final_cancellation = pd.merge(df1, df2[['storeid', 'order_ids']], how='inner', on=['storeid'])
    final_cancellation['order_ids'] = [','.join(map(str, l)) for l in final_cancellation['order_ids']]

    final_df = pd.merge(base_orders, final_cancellation, how='left', on=['storeid'])

    final_df = pd.merge(final_df, store_infos, how='left', on=['storeid'])

    ## Final Super DF for slot store closed
    try:
        df_super = pd.merge(cancellation_super, store_closed_om, how='left', left_on=cancellation_super['order_id'],
                            right_on=store_closed_om['order_id_modifications'])
        df_super = df_super.drop(['key_0'], axis=1)
    except:
        df_super = cancellation_super
        df_super['order_id_modifications'] = 0

    try:
        df_super = pd.merge(df_super, chats, how='left', left_on=df_super['order_id'], right_on=chats['orderid'])
        df_super = df_super.drop(['key_0'], axis=1)
    except:
        df_super = df_super
        df_super['orderid'] = 0

    df_super['order_id_modifications'] = df_super['order_id_modifications'].fillna(0)
    df_super['orderid'] = df_super['orderid'].fillna(0)
    df_super = df_super[(df_super['order_id_modifications'] != 0) | (df_super['orderid'] != 0)]
    df_super['order_id_canceled'] = np.where(df_super['order_id_modifications'] != 0,
                                             df_super['order_id_modifications'], df_super['orderid'])
    df_super = df_super.drop(['order_id', 'orderid', 'order_id_modifications'], axis=1)

    df_super1 = df_super.groupby(["storeid", "store_name"])["order_id_canceled"].count().reset_index(
        name="total_cancels")
    df_super2 = df_super.groupby(["storeid", "store_name"])["order_id_canceled"].apply(list).reset_index(
        name='order_ids')
    print("dfs")
    print(df_super1)
    print(df_super2)

    try:
        final_cancellation_super = pd.merge(df_super1, df_super2[['storeid', 'order_ids']], how='inner', on=['storeid'])
        final_cancellation_super['order_ids'] = [','.join(map(str, l)) for l in final_cancellation_super['order_ids']]
        final_df_super = pd.merge(base_orders, final_cancellation_super, how='left', on=['storeid'])
        final_df_super = pd.merge(final_df_super, store_infos, how='left', on=['storeid'])
    except:
        final_df_super = df_super1

    return final_df, excluding_stores, store_infos, excluding_brands, excluding_physical_stores, final_df_super


def bot_store_closed(final_df, excluding_stores, tod, df_suspensa, df_sheets, df_suspendidas_cms_logger):
    current_time = timezones.country_current_time(country)
    final_df['city_id'] = final_df['city_id'].astype(str)
    df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)
    final_df = pd.merge(final_df, df_sheets[['CITY_ID', 'MIN_CANCELS']], how='inner', left_on=final_df['city_id'],
                        right_on=df_sheets['CITY_ID'])
    final_df = final_df.drop(['key_0', 'CITY_ID'], axis=1)
    final_normal_df = final_df[(final_df['total_cancels'] > 0)]

    lista = df_suspensa['suspendida_id'].to_list()
    lista = [f'{str(i)}' for i in lista]
    final_normal_df = final_normal_df.rename(columns={'storeid': 'store_id'})

    final_normal_df = pd.merge(final_normal_df, excluding_stores, how='left', left_on=final_normal_df['store_id'],
                               right_on=excluding_stores['storeid'])
    final_normal_df = final_normal_df[final_normal_df['storeid'].isnull()]
    final_normal_df = final_normal_df.drop(['key_0'], axis=1)
    final_normal_df['store_id'] = final_normal_df['store_id'].astype(str)

    final_normal_df = pd.merge(final_normal_df, df_suspensa, how='left', left_on=final_normal_df['store_id'], right_on=df_suspensa['suspendida_id'])

    ### teste is_enabled = true & suspended = true

    final_normal_suspendida_aberta = final_normal_df[final_normal_df.is_enabled == True]

    final_normal_suspendida_aberta = final_normal_suspendida_aberta[['store_id', 'total_orders','total_cancels','vertical','brand_group_id','is_enabled', 'suspended']]
    final_normal_suspendida_aberta['country'] = country
    final_normal_suspendida_aberta['store_id'] = final_normal_suspendida_aberta['store_id'].astype(str)
    final_normal_suspendida_aberta['suspended_at'] = current_time
    snow.upload_df_occ(final_normal_suspendida_aberta, 'is_enabled_suspended_store_closed')

    final_normal_df = final_normal_df[final_normal_df['suspendida_id'].isnull()]
    final_normal_df = final_normal_df.drop(['key_0'], axis=1)
    print(final_normal_df)
    final_normal_df = final_normal_df[final_normal_df.is_enabled != True]
    print(final_normal_df)
    final_normal_df = pd.merge(final_normal_df, df_suspendidas_cms_logger, how='left', left_on=final_normal_df['store_id'],
                               right_on=df_suspendidas_cms_logger['storeid'])
    final_normal_df = final_normal_df[~final_normal_df.content_is_enable.isin(['False','false','FALSE'])]   #diferente de false
    print(final_normal_df)
    print(final_normal_df.vertical.unique())

    final_normal_df['MIN_CANCELS'] = final_normal_df['MIN_CANCELS'].astype(float)
    if country in ['ar', 'br', 'cl', 'ec', 'mx', 'pe', 'uy', 'co']:
        if current_time.weekday() >= 0 and current_time.weekday() <= 3:
            final_normal_df = final_normal_df[
                ((final_normal_df['total_cancels'] >= final_normal_df['MIN_CANCELS']) & (
                    final_normal_df['vertical'].isin(['Express', 'Licores', 'Farmacia', 'Specialized', 'Ecommerce'])))
                | ((final_normal_df['total_cancels'] >= 2000) & (final_normal_df['vertical'].isin(['Turbo'])))
                ]
        elif current_time.weekday() >= 4 and current_time.weekday() <= 6:
            final_normal_df = final_normal_df[
                ((final_normal_df['total_cancels'] >= final_normal_df['MIN_CANCELS']) & (
                    final_normal_df['vertical'].isin(['Express', 'Licores', 'Farmacia', 'Specialized', 'Ecommerce'])))
                | ((final_normal_df['total_cancels'] >= 2000) & (final_normal_df['vertical'].isin(['Turbo'])))
                ]

    final_normal_df.drop_duplicates()
    final_normal_df['country'] = country
    final_normal_df['suspended_at'] = current_time
    if country != 'ec' and country != 'cr':
        holidays_list = diasfestivos.get_national_holidays(country)
        is_holiday = current_time in holidays_list
        cities = final_normal_df['city_id'].unique()
    else:
        is_holiday = None

    city_holliday = []
    for city_id in cities:
        city_id = str(city_id)
        try:
            holidays_list = diasfestivos.get_cities_holidays(city_id)
            is_holiday_city = timezones.country_current_time(country).date() in holidays_list
            if is_holiday_city:
                city_holliday.append(city_id)
        except UnboundLocalError:
            pass

    if tod in ['madrugada']:
        print(current_time.strftime('%d/%m/%Y %H:%M'))
        if is_holiday:
            print('is holiday')
            target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
            time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
        else:
            if (final_normal_df['city_id'].isin(city_holliday)).any():
                print('é feriado')
                print(city_holliday)
                target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
            else:
                target_date = (current_time + timedelta(days=0)).replace(hour=11, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0)

    elif tod in ['manha']:
        print(current_time.strftime('%d/%m/%Y %H:%M'))
        if is_holiday:
            print('is holiday')
            target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
            time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
        else:
            if (final_normal_df['city_id'].isin(city_holliday)).any():
                print('é feriado')
                print(city_holliday)
                target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
            else:
                print('não é feriado')
                target_date = (current_time + timedelta(days=0)).replace(hour=13, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0)

    elif tod in ['tarde']:
        print(current_time.strftime('%d/%m/%Y %H:%M'))
        if is_holiday:
            print('is holiday')
            target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
            time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
        else:
            if (final_normal_df['city_id'].isin(city_holliday)).any():
                print('é feriado')
                print(city_holliday)
                target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)
            else:
                print('não é feriado')
                target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
                time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(50)


    to_upload = final_normal_df[
        ['country', 'store_id', 'store_name', 'total_cancels', 'order_ids', 'filhas', 'suspended_at', 'vertical',
         'category']]
    snow.upload_df_occ(to_upload,'store_closed_bot')

    for index, row in final_normal_df.iterrows():
        rows = dict(row)
        store_id = int(row['store_id'])
        filhas = row['filhas']
        try:
            filhas_list = filhas.strip('][').split(',')
        except:
            print('sem filhas')
        print("lista filhas")
        print(filhas_list)
        filhas_list = list(set(filhas_list) - set(lista))
        print(filhas_list)
        if is_holiday:
            reason = "occ_store_closed_holidays"
        else:
            try:
                if (row['city_id'].isin(city_holliday)).any():
                    reason = "occ_store_closed_holidays"
                else:
                    reason = "occ_store_closed"
            except:
                reason = "occ_store_closed"

        cms.turnStoreOff(store_id, time_closed +2, country, reason) == 200
        cms.disable_store(store_id, country, reason, time_closed)
        try:
        	sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
        except Exception as e:
        	print(e)
        if len(filhas) >= 1:
            for filha in filhas_list:
                print("as filhas")
                print(filha)
                cms.turnStoreOff(filha, time_closed +2,country, reason) == 200
                cms.disable_store(filha, country, reason, time_closed)
                try:
                    sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
                except Exception as e:
                    print(e)

        text2 = '''
        *Tienda suspendida por Store Closed :flag-{country}:*
        La Tienda {store_name} (Store ID: {store_id}) fue remitida a suspensión hasta {data} por {order_id} cancelamiento por Store Closed
        Vertical: {vertical}
        Tier: {category}
        Hijas (suspendidas para CPGs now): {filhas}
        Orders IDs: {order_ids} '''.format(
            store_id=row['store_id'],
            order_id=int(row['total_cancels']),
            order_ids=row['order_ids'],
            data=target_date.strftime('%Y-%m-%d %H:%M:%S UTC%z').replace("UTC-0600", "UTC-6").replace("UTC-0500",
                                                                                                      "UTC-5").replace(
                "UTC-0400", "UTC-4").replace("UTC-0300", "UTC-3"),
            country=country,
            store_name=row['store_name'],
            vertical=row['vertical'],
            category=row['category'],
            filhas="" if row['filhas'] is None else row['filhas']
        )
        print(text2)
    slack.bot_slack(text2,'C01S0667VKP')


for country in countries:
    country = country.lower()
    print(country)
    current_time = timezones.country_current_time(country)
    df_sheets = df[df.COUNTRY == country.upper()]
    df_suspendidas_cms_logger = suspendidas_cms_logger(country)
    if current_time.strftime('%H:%M') >= '00:00' and current_time.strftime('%H:%M') <= '06:59':
        try:
            a, b, c, d, e, f = dataframe(country, "madrugada")
            df_suspensa = suspendidas(country)
            bot_store_closed(a, b, "madrugada", df_suspensa, df_sheets, df_suspendidas_cms_logger)
        except Exception as e:
            print(e)
    elif current_time.strftime('%H:%M') >= '07:00' and current_time.strftime('%H:%M') <= '10:00':
        try:
            a, b, c, d, e, f = dataframe(country, "manha")
            df_suspensa = suspendidas(country)
            bot_store_closed(a, b, "manha", df_suspensa, df_sheets, df_suspendidas_cms_logger)
        except Exception as e:
            print(e)
    elif current_time.strftime('%H:%M') > '10:00':
        try:
            a, b, c, d, e, f = dataframe(country, "tarde")
            df_suspensa = suspendidas(country)
            print(df_suspensa)
            bot_store_closed(a, b, "tarde", df_suspensa, df_sheets, df_suspendidas_cms_logger)
        except Exception as e:
            print(e)
