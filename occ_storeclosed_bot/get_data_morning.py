import pandas as pd
from datetime import datetime, timedelta
from lib import redash
from lib import snowflake as snow
from functions import timezones

def data_frames(country):
	timezone, interval = timezones.country_timezones(country)

	query_excluding_stores = f'''
	--no_cache
	select distinct s.store_id::text as storeid
	from {country}_pg_ms_cpgops_stores_ms_public.physical_stores ps
	inner join {country}_pg_ms_cpgops_stores_ms_public.stores s on (s.physical_store_id = ps.id and not coalesce (s._fivetran_deleted, false))
	inner join {country}_pg_ms_cpgops_stores_ms_public.slots sh on (sh.physical_store_id = ps.id and not coalesce (sh._fivetran_deleted, false) and datetime_utc_iso::date>= dateadd(day,-15,current_date())::date)
	inner join {country}_grability_public.stores_vw gs on (gs.store_id = s.store_id and gs.allow_shopper_session and not coalesce (gs._fivetran_deleted, false))
	left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s2 on s2.store_id=s.store_id and not coalesce (s2._fivetran_deleted, false)
	left join  VERTICALS_LATAM.{country}_VERTICALS_V2 v on v.store_type=s2.type
	where vertical_sub_group not in ('EXPRESS','PHARMACY','LIQUOR')
	union all
	select distinct store_id::text as storeid
	from ops_occ.store_closed_bot
	where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country='{country}'
	union all
	select distinct store_id::text as storeid
	from {country}_pg_ms_cpgops_stores_ms_public.stores
	where name ilike '%Dummy%Store%Rappi%Connect%'
	'''

	query_base_orders = f'''
	--no_cache
	with base as
	(select order_id, created_at, max(case when type in ('payless_barcodes_created',
'payless_fallback_authorized',
'payless_fallback_denied',
'payless_transaction_authorized',
'payless_transaction_cancelled',
'hand_to_domiciliary',
'payless_transaction_declined') then created_at else null end) as falso_storeclosed 
	from order_modifications om
	where om.created_at >= date(now() AT TIME ZONE 'America/{timezone}')
	and om.created_at::time >= '07:00:00'
	and om.created_at::time <= '10:00:00'
	group by 1,2
	)
	select os.store_id::text as storeid,max(os.name) as store_name, case when o.state ilike '%cancel%' and falso_storeclosed is null then b.order_id else null end as order_id,
	b.order_id as all_order_id, case when o.place_at is null then 'yes' else 'no' end as bug_place_at
	from base b
	inner join orders o on o.id=b.order_id and o.payment_method not ilike '%synthetic%'
	inner join calculated_information.orders o2 on o2.order_id=o.id and o2.city_address_id != 104
	join order_stores os on os.order_id=o.id and os.store_id=o2.store_id and os.contact_email not like '%@synth.rappi.com'

	where o.state NOT IN ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and init_store_type not in ('courier','rappi_prime','whim','courier_hours','courier_sammpling','soat_webview')
	and os.type not in ('queue_order_flow')
	group by 1,3,4,5
	'''

	query_store_closed_from_order_modifications = f'''
	--no_cache
	with base as
	(select order_id, created_at
	from order_modifications om
	where om.created_at >= date(now() AT TIME ZONE 'America/{timezone}')
	and (
	(
	(type ILIKE '%cancel%')
	and (params ->> 'cancelation_reason' in ('crsan_closed_store')
	or params ->> 'cancellation_info' ilike '%crsan_closed_store%')
	)
	or 
	(type ='canceled_store_closed')
	)
	group by 1,2)
	select distinct b.order_id as order_id_modifications
	from base b    
	inner join orders o on o.id=b.order_id and o.payment_method not ilike '%synthetic%'
	inner join calculated_information.orders o2 on o2.order_id=o.id and o2.city_address_id != 104
	join order_stores os on os.order_id=o.id and os.store_id=o2.store_id and os.contact_email not like '%@synth.rappi.com'
	where o.state ilike '%cancel%' 
	and o.state NOT IN ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and init_store_type not in ('courier','rappi_prime','whim','courier_hours','courier_sammpling')
	and os.type not in ('queue_order_flow','soat_webview')
	'''

	if country == 'br':
		query_store_closed_from_chat = f'''

	 --no_cache
			with a as (select max(id) as id_maximo from chats)
			,b as (select order_id, data, sender_type 
			from a
			join chats c on c.id >= (a.id_maximo - 500000)
			),
			e AS
	  (SELECT max(id) AS id_maximo
	   FROM notifications) ,
		  f AS
	  ( SELECT order_id,
			   DATA,
			   sender_type
	   FROM e
	   JOIN notifications c ON c.id >= (e.id_maximo - 500000))
			select b.order_id as orderid
			from b
			left join f on f.order_id = b.order_id
			where
			(((b.data SIMILAR TO '%(ta|tá|tao|tão|ja|já|loja|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%' 
			 and b.data SIMILAR TO '%(fexad|fechad|fechou|fechando|fexou|fexa)%'
			)
			and b.data not ilike '%pedido fechado' 
			and b.data not ilike '%fechou a compra%' 
			and b.data not ilike '%pedido já está fechado%'
			and b.data not ilike '%pedido ja esta fechado%')
			or 
			((f.data SIMILAR TO '%(ta|tá|tao|tão|ja|já|loja|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%' 
			 and f.data SIMILAR TO '%(fexad|fechad|fechou|fechando|fexou|fexa)%'
			)
			and f.data not ilike '%pedido fechado' 
			and f.data not ilike '%fechou a compra%' 
			and f.data not ilike '%pedido já está fechado%'
			and f.data not ilike '%pedido ja esta fechado%'))

			and b.sender_type in ('storekeeper')
			group by 1
			'''
	elif country in ['cl', 'ec', 'cr', 'uy', 'ar', 'pe']:
		query_store_closed_from_chat = f'''


	 --no_cache
			with a as (select max(id) as id_maximo from chats)
			,b as (select order_id, data, sender_type 
			from a
			join chats c on c.id >= (a.id_maximo - 500000)
			),
			e AS
	  (SELECT max(id) AS id_maximo
	   FROM notifications) ,
		  f AS
	  ( SELECT order_id,
			   DATA,
			   sender_type
	   FROM e
	   JOIN notifications c ON c.id >= (e.id_maximo - 500000))
			select b.order_id as orderid
			from b
			left join f on f.order_id = b.order_id
			where
			(((b.data SIMILAR TO '%(ta|tá|ya|encuentra|tienda|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%' 
			 and b.data SIMILAR TO '%(cerrad|serrad|cerrando|cerraron|cerro)%'
			) or b.data ilike '%no esta abierto%'
			  or b.data ilike '%no esta prestando servicio%'
			)
			or
			((f.data SIMILAR TO '%(ta|tá|ya|encuentra|tienda|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%' 
			 and f.data SIMILAR TO '%(cerrad|serrad|cerrando|cerraron|cerro)%'
			) or f.data ilike '%no esta abierto%'
			  or f.data ilike '%no esta prestando servicio%'
			))
			and b.sender_type ilike '%storekeeper%'
			group by 1
			'''

	else:
		query_store_closed_from_chat = f'''
	--no_cache
	 WITH a AS
	  (SELECT max(id) AS id_maximo
	   FROM chats) ,
		  b AS
	  ( SELECT order_id,
			   DATA,
			   sender_type
	   FROM a
	   JOIN chats c ON c.id >= (a.id_maximo - 100000)),
	   e AS
	  (SELECT max(id) AS id_maximo
	   FROM notifications) ,
		  f AS
	  ( SELECT order_id,
			   DATA,
			   sender_type
	   FROM e
	   JOIN notifications c ON c.id >= (e.id_maximo - 2500))
	SELECT b.order_id as orderid
	FROM b
	left join f on f.order_id = b.order_id
	WHERE ((b.DATA SIMILAR TO '%(ta|tá|ya|encuentra|tienda|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%'
			AND b.DATA SIMILAR TO '%(cerrad|serrad|cerrando|cerraron|cerro)%' )
		   OR b.DATA ILIKE '%no esta abierto%' 
		   or b.data ilike '%no esta prestando servicio%') or 
		   ((f.DATA SIMILAR TO '%(ta|tá|ya|encuentra|tienda|posto|farm%cia|resta%ante|mercado|local|estabelecimento|reforma|energia)%'
			AND f.DATA SIMILAR TO '%(cerrad|serrad|cerrando|cerraron|cerro)%' )
		   OR f.DATA ILIKE '%no esta abierto%' 
		   or f.data ilike '%no esta prestando servicio%')
		    and b.sender_type in ('storekeeper')
	group by 1
			'''

	query_store_infos = f'''
	--no_cache
	with orders as (select store_id, count(distinct order_id) as orders
	from GLOBAL_FINANCES.global_orders
	where lower(country) = '{country}'
	and created_at::date >= dateadd(day,-30,current_date())::date
	and created_at::date < convert_timezone('America/{timezone}',current_timestamp())::date
	group by 1
	having orders >= 1
	)
	, base as (select bg.brand_group_id::text as brandid, s.store_id::text as store_id, city_address_id, coalesce(orders,0) as orders
	from {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw sb
	left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_brand_id = sb.id and coalesce(s._fivetran_deleted, 'false') = false
	join orders o on o.store_id=s.store_id
	left join (
	   select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
	   ) bg on bg.store_brand_id = s.store_brand_id
	where coalesce(sb._fivetran_deleted, 'false') = false
	group by 1,2,3,4)
	,percentiles as (select brandid, store_id, city_address_id, PERCENT_RANK() over (partition by brandid, city_address_id order by orders) as percentile
	from base
	)
	select a.store_id::text as storeid, 
	a.city_address_id::text as city_id, 
	msc.city as city_name, 
	v.vertical,
	brand_group_id::text as brand_group_id, 
	brand_group_name, 
	coalesce(percentile,0) as percentile_inside_brand,
	max(category) as category,
	listagg(distinct b.store_id::text,',') as filhas,
	max(cpgops.physical_store_id)::text as physical_store_id,
	max(pss.name) as physical_store_name
	from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
	left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw b on b.super_store_id=a.store_id and b.deleted_at is null and not coalesce (b._fivetran_deleted, false)
	left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c on c.id=a.store_brand_id
	join percentiles p on p.store_id=a.store_id and p.store_id::text=a.store_id::text
	left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id=a.store_id
	left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id=cpgops.physical_store_id
	left join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc on msc.id=a.city_address_id
	inner join (select store_type as storetype,
	case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	when vertical_group = 'WHIM' then 'Antojos'
	when vertical_group = 'RAPPICASH' then 'RappiCash'
	when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
	when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
	when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
	when upper (store_type) in ('TURBO') then 'Turbo'
	when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
	when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
	when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
	when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
	when upper (vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
	when upper(vertical_group) in ('CPGS') then 'CPGs'
	else 'Others' end as vertical
	from VERTICALS_LATAM.{country}_VERTICALS_V2
	where vertical not in ('Turbo')
			   ) v on v.storetype=a.type
	left join (select store_id,
					  case when category = 'Platinum' then 'Top'
						  when category in ('Gold','Silver') then 'Mid'
					  else 'Low' end as category1, 
					  case when category1 not in ('Top','Mid','Low') then 'Custom'
					  else category1 end as category
	from ops_global.brands_tiers
	where lower(country) = '{country}') t on t.store_id=a.store_id

	left join (
	   select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
	   ) bg on bg.store_brand_id = a.store_brand_id

	where a.deleted_at is null
	and not coalesce (a._fivetran_deleted, false)
	and a.name not ilike '%turbo%' and a.type not ilike '%turbo%'
	group by 1,2,3,4,5,6,7
	'''

	query_excluding_brands = f'''
	--no_cache
	select brand_group_id::text as brand_group_id, brand_group_id::text as brandgroupid, city_id, 'second' as flag, percentage_sc
	from ops_occ.predictive_groupbrand
	where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	'''

	enabled_stores = f'''
	--no_cache
	select distinct store_id::text as sto_id
	from stores
	where is_enabled=true
	'''

	print("excluding_brands")
	excluding_brands = snow.run_query(query_excluding_brands)
	print("excluding_stores")
	excluding_stores = snow.run_query(query_excluding_stores)
	print("store_infos")
	store_infos = snow.run_query(query_store_infos)

	if country == 'co':
		print("base_orders")
		base_orders = redash.run_query(7973, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7973, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(652, query_store_closed_from_chat)
		print("enabled_stores")
		#enabled_store = redash.run_query(6312, enabled_stores)
	elif country == 'ar':
		print("base orders")
		base_orders = redash.run_query(7970, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7970, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(647, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6309, enabled_stores)
	elif country == 'cl':
		print("base orders")
		base_orders = redash.run_query(7972, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7972, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(649, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6311, enabled_stores)
	elif country == 'mx':
		print("base orders")
		base_orders = redash.run_query(7977, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7977, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(651, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6324, enabled_stores)
	elif country == 'uy':
		print("base orders")
		base_orders = redash.run_query(7978, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7978, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(650, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6322, enabled_stores)
	elif country == 'ec':
		print("base orders")
		base_orders = redash.run_query(7975, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7975, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(2184, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6447, enabled_stores)
	elif country == 'cr':
		print("base orders")
		base_orders = redash.run_query(7974, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7974, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(2183, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6313, enabled_stores)
	elif country == 'pe':
		print("base orders")
		base_orders = redash.run_query(7976, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7976, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(745, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6323, enabled_stores)
	elif country == 'br':
		print("base orders")
		base_orders = redash.run_query(7971, query_base_orders)
		print("store closed om")
		store_closed_om = redash.run_query(7971, query_store_closed_from_order_modifications)
		print("store closed chat")
		chats = redash.run_query(648, query_store_closed_from_chat)
		#enabled_store = redash.run_query(6310, enabled_stores)

	#store_infos = pd.merge(store_infos,enabled_store,how='inner',left_on=store_infos['storeid'],right_on=enabled_store['sto_id'])
	#store_infos = store_infos.drop(['key_0','sto_id'], axis=1)

	return excluding_stores, excluding_brands,store_infos, base_orders, store_closed_om, chats