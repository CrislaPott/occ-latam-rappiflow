import os, sys, json
import numpy as np
import pandas as pd
from random import randrange
from datetime import datetime, timedelta
from lib import redash,slack
from lib import snowflake as snow
from functions import timezones
from functions import diasfestivos
import pytz
import get_data_morning as gdm
import get_data_afternoon as gda
from lib import gsheets

def suspendidas(country):
	query = '''
	select s.store_id::text as suspendida_id, is_enabled,suspended
    from stores s 
    join store_attribute sa on sa.store_id = s.store_id
    where is_enabled=true
    and suspended=true
	'''

	if country == 'co':
		metrics = redash.run_query(6312, query)
	elif country == 'ar':
		metrics = redash.run_query(6309, query)
	elif country == 'cl':
		metrics = redash.run_query(6311, query)
	elif country == 'mx':
		metrics = redash.run_query(6324, query)
	elif country == 'uy':
		metrics = redash.run_query(6322, query)
	elif country == 'ec':
		metrics = redash.run_query(6447, query)
	elif country == 'cr':
		metrics = redash.run_query(6313, query)
	elif country == 'pe':
		metrics = redash.run_query(6323, query)
	elif country == 'br':
		metrics = redash.run_query(6310, query)

	return metrics
def suspendidas_cms_logger(country):
    query = f'''with base as (select distinct store_id,
action,
COALESCE(CONTENT_IS_ENABLE, CONTENT_IS_ENABLED) as CONTENT_IS_ENABLE,
created_at,
rank() OVER (PARTITION BY COUNTRY, STORE_ID ORDER BY CREATED_AT DESC) as rank
from ops_occ.global_cms_logger
where country = '{country}')

select store_id as storeid,
action,
CONTENT_IS_ENABLE,
created_at
from base
where rank = 1'''
    df = snow.run_query(query)
    return df

df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Store Closed - Predictive')
df = df[(df["ON/OFF"] == 'ACTIVE')]
df.STORES_WITH_SC = df.STORES_WITH_SC.astype(float)
df.MIN_ORDERS_SC = df.MIN_ORDERS_SC.astype(float)
df.STORES_WITH_SC_HOLIDAYS = df.STORES_WITH_SC_HOLIDAYS.astype(float)
df.MIN_ORDERS_SC_HOLIDAYS = df.MIN_ORDERS_SC_HOLIDAYS.astype(float)
countries = df['COUNTRY'].unique()
print(countries)

def dataframe(country, tod):
	if tod in ['morning']:
		excluding_stores, excluding_brands, store_infos, base_orders, store_closed_om, chats = gdm.data_frames(country)
	elif tod in ['afternoon']:
		excluding_stores, excluding_brands, store_infos, base_orders, store_closed_om, chats = gda.data_frames(country)

	cancellation = base_orders[(base_orders['order_id'].notnull())]
	print(base_orders)
	base_orders = base_orders.groupby(["storeid"])["all_order_id"].count().reset_index(name="total_orders")

## Final DF for predictive store closed
	try:
		df = pd.merge(cancellation,store_closed_om,how='left',left_on=cancellation['order_id'],right_on=store_closed_om['order_id_modifications'])
		df = df.drop(['key_0'], axis=1)
	except:
		df = cancellation
		df['order_id_modifications'] = 0

	try:
		df = pd.merge(df,chats,how='left',left_on=df['order_id'],right_on=chats['orderid'])
		df = df.drop(['key_0'], axis=1)
	except:
		df = df
		df['orderid'] = 0

	df['order_id_modifications'] = df['order_id_modifications'].fillna(0)
	df['orderid'] = df['orderid'].fillna(0)
	df = df[(df['order_id_modifications'] != 0) | (df['orderid'] != 0)]
	df['order_id_canceled'] = np.where(df['order_id_modifications']!= 0, df['order_id_modifications'],df['orderid'])
	df = df.drop(['order_id','orderid','order_id_modifications'], axis=1)

	df1 = df.groupby(["storeid", "store_name"])["order_id_canceled"].count().reset_index(name="total_cancels")
	df2 = df.groupby(["storeid","store_name"])["order_id_canceled"].apply(list).reset_index(name='order_ids')

	final_cancellation = pd.merge(df1,df2[['storeid','order_ids']], how='inner',on=['storeid'])
	final_cancellation['order_ids'] = [','.join(map(str, l)) for l in final_cancellation['order_ids']]

	final_df = pd.merge(base_orders,final_cancellation, how='left',on=['storeid'])
	final_df = pd.merge(final_df,store_infos,how='left',on=['storeid'])
	final_df['city_id'] = final_df['city_id'].astype(str)
	df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)
	final_df = pd.merge(final_df, df_sheets[['CITY_ID', 'CITY_NAME']], how='inner', left_on=final_df['city_id'],
				  right_on=df_sheets['CITY_ID'])
	final_df = final_df.drop(['key_0','CITY_ID', 'CITY_NAME'], axis=1)

	return final_df, excluding_stores, store_infos, excluding_brands

def predictive_storeclosed_morning(final_df,excluding_stores,store_infos, excluding_brands,df_suspendidas,df_sheets,df_suspendidas_cms_logger):
	current_time = timezones.country_current_time(country)
	## filter verticals in final_df where we have all stores, all orders
	final_df = final_df[(final_df['vertical'].isin(['Express','Licores','Farmacia','Specialized']))]
	## grouping brands and counting the total orders by cities
	qtd_orders_per_brands = final_df.groupby(["brand_group_id","brand_group_name","city_id"])['total_orders'].sum().reset_index(name="total_orders")
	## grouping brands and counting the total store closed cancels by cities
	qtd_storeclosed_per_brands = final_df.groupby(["brand_group_id","city_id"])['total_cancels'].sum().reset_index(name="total_cancels")
	## grouping brands and counting the total of stores by cities in store infos DF
	qtd_stores = store_infos.groupby(["brand_group_id","city_id"])['storeid'].count().reset_index(name="total_stores")
	## taking only store_ids with store closed
	stores_storeclosed = final_df[final_df['total_cancels'] > 0]
	## grouping brands and counting the total os stores with store closed by cities
	qtd_closed_stores = stores_storeclosed.groupby(["brand_group_id","city_id"])['storeid'].count().reset_index(name="closed_stores")
	## grouping brand and gettig a list os store_ids triggers
	lojas_ids = stores_storeclosed.groupby(["brand_group_id","city_id"])['storeid'].apply(list).reset_index(name='store_sc_ids')

	# The next set of merges is in order to build a final brand DF, with all we need, by brands and it's cities
	# From total orders to brands with store closed  - LEFT JOIN
	union_brand_df = pd.merge(qtd_orders_per_brands,qtd_storeclosed_per_brands, how='left', on=['brand_group_id','city_id'])
	# Adding the total stores 
	union_brand_df = pd.merge(union_brand_df,qtd_stores, how='inner', on=['brand_group_id','city_id'])
	# Adding the total closed stores
	union_brand_df = pd.merge(union_brand_df,qtd_closed_stores, how='left', on=['brand_group_id','city_id'])
	# Getting the store_ids with store closed
	union_brand_df = pd.merge(union_brand_df,lojas_ids, how='left', on=['brand_group_id','city_id'])

	# Add the percentage os store closed by brands and it's
	union_brand_df["percentage"] = union_brand_df['total_cancels'] / union_brand_df['total_orders']
	# Filter the minimum threshold

	print("zero")
	print(union_brand_df)
	holidays_list = diasfestivos.get_national_holidays(country)
	is_holiday = current_time in holidays_list
	cities = union_brand_df['city_id'].unique()

	city_holliday = []
	for city_id in cities:
		city_id = str(city_id)
		try:
			holidays_list = diasfestivos.get_cities_holidays(city_id)
			is_holiday_city = timezones.country_current_time(country).date() in holidays_list
			if is_holiday_city:
				city_holliday.append(city_id)
		except UnboundLocalError:
			pass

	if is_holiday:
		print('é feriado')
		union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC_HOLIDAYS',
															 'STORES_WITH_SC_HOLIDAYS']], how='left',
								  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

		union_brand_df = union_brand_df[
			((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC_HOLIDAYS']) &
			 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC_HOLIDAYS']))
		]
	else:
		if (union_brand_df['city_id'].isin(city_holliday)).any():
			print('é feriado')
			print(city_holliday)
			union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC_HOLIDAYS',
																 'STORES_WITH_SC_HOLIDAYS']], how='left',
									  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

			union_brand_df = union_brand_df[
				((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC_HOLIDAYS']) &
				 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC_HOLIDAYS']))
			]
		else:
			print('não é feriado')
			union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC',
																 'STORES_WITH_SC']], how='left',
									  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

			union_brand_df = union_brand_df[
				((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC']) &
				 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC']))]

	union_brand_df = union_brand_df[['brand_group_id', 'brand_group_name', 'city_id',
       'total_orders', 'total_cancels', 'total_stores', 'closed_stores',
       'store_sc_ids', 'order_ids', 'percentage']]

	print("after")
	print(union_brand_df)

	news_brands = pd.merge(union_brand_df,excluding_brands, how='left', on=["brand_group_id","city_id"])
	# Taking only news brands by cities
	news_brands1 = news_brands[news_brands['brandgroupid'].isnull()]
	
	# Add the flag first, because it will be suspended now and the bot needs to have it for future checks
	news_brands1['flag'] = 'first'

	## uninon of new brands (first flag) and brands with the second flag 
	final_brand_df= news_brands1

	## it does not include brands with second flag
	#final_brand_df = pd.merge(final_brand_df,excluding_brands_second, how='left', on=["brand_group_id","city_id"])
	#final_brand_df = final_brand_df[final_brand_df['brandgroupid_y'].isnull()]
	#final_brand_df = final_brand_df.drop(['brandgroupid_y','brandgroupid_x','percentage_sc_x','flag_y','percentage_sc_y'], axis=1)
	#final_brand_df = final_brand_df.rename(columns={'flag_x': 'flag'})

	current_time = timezones.country_current_time(country)

	# DF for the log upload and drop the old percentage sc column in order to set the new percentage
	# set the new percentage of store closed
	to_upload_brand = final_brand_df.rename(columns={'percentage': 'percentage_sc'})
	to_upload_brand['suspended_at'] = current_time
	to_upload_brand['country'] = country
	to_upload_brand['store_sc_ids'] = [','.join(map(str, l)) for l in to_upload_brand['store_sc_ids']]
	to_upload_brand = to_upload_brand[["brand_group_id","total_cancels","closed_stores","percentage_sc","suspended_at","city_id","flag","country","store_sc_ids"]]

	#snow.upload_df_occ(to_upload_brand,'predictive_groupbrand')

	# for each brand_group_id, per citites
	for index, row in final_brand_df.iterrows():
		total_orders = row['total_orders']
		percentage = row['percentage']
		total_cancels = row['total_cancels']
		closed_stores = row['closed_stores']
		total_stores = row['total_stores']
		store_sc_ids = row['store_sc_ids']
		if row['flag'] == 'first':
			perc = 'Percentil 75'
		elif row['flag'] == 'second':
			perc = 'Percentil 75'

		# Take the store_ids from store_infos base that have the same brand_group_id and the same city_id
		stores_list = store_infos[((store_infos['brand_group_id'] == row['brand_group_id']) & (store_infos['city_id'] == row['city_id']))]

		# if it is the first time, then take the stores classified with less than 0.25 or equal to
		if row['flag'] == 'first':
			stores_list = stores_list[(stores_list['percentile_inside_brand'] <= 0.75)]
			stores_list['flag'] = 'percentil_75'
		# else (second) take the greater percencil 
		else:
			stores_list = stores_list[((stores_list['percentile_inside_brand'] > 0.45) & (stores_list['percentile_inside_brand'] <= 0.75))]
			stores_list['flag'] = 'percentil_75'

		# excluding stores that has been suspended already
		stores_list = pd.merge(stores_list,excluding_stores,how='left',left_on=stores_list['storeid'],right_on=excluding_stores['storeid'])

		stores_list = stores_list[stores_list['storeid_y'].isnull()]
		stores_list = stores_list.drop(['key_0','storeid_y'], axis=1)
		stores_list = stores_list.rename(columns={'storeid_x': 'store_id'})

		stores_list['store_id'] = stores_list['store_id'].astype(str)

		stores_list = pd.merge(stores_list, df_suspendidas, how='left', left_on=stores_list['store_id'],right_on=df_suspendidas['suspendida_id'])
		stores_list = stores_list[stores_list['suspendida_id'].isnull()]
		stores_list = stores_list.drop(['key_0'], axis=1)

		stores_list = pd.merge(stores_list, df_suspendidas_cms_logger, how='left',left_on=stores_list['store_id'],right_on=df_suspendidas_cms_logger['storeid'])
		print(stores_list)
		stores_list = stores_list[~stores_list.content_is_enable.isin(['False', 'false', 'FALSE'])]  # diferente de false
		print(stores_list)
		print(stores_list.vertical.unique())

		# in order to avoid a store id from other verticals
		stores_list = stores_list[(stores_list['vertical'].isin(['Express','Licores','Farmacia','Specialized']))]

		store_ids_suspended = stores_list.groupby(["brand_group_id","brand_group_name","city_id"])["store_id"].apply(list).reset_index(name='store_ids')
		store_ids_suspended['store_ids'] = [','.join(map(str, l)) for l in store_ids_suspended['store_ids']]
		store_ids_suspended = store_ids_suspended['store_ids'].to_list()

		if country in ['dev']:
			target_date = (current_time + timedelta(days=0)).replace(hour=11, minute=0, second=0, microsecond=0)
		else:
			target_date = (current_time + timedelta(hours=1))
		time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(40)
		
		stores_list['suspended_at'] = current_time
		stores_list['suspended_until'] = target_date

		to_upload_details = stores_list[["store_id","city_id","brand_group_id","suspended_at","suspended_until","flag"]]

		# suspend closed_stores
		triggers = row['store_sc_ids']
		print("triggers_list")
		print(triggers)
		reason = "predictive_store_closed"
		if len(triggers) >= 1:
			for trigger in triggers:
				print("trigger")
				print(trigger)
				# no go, because the next function will suspend this store (threshold >=1)
				#cms.turnStoreOff(trigger, time_closed,country, reason) == 200

		# in order to iterate and suspend each store_id (preditive action)
		for index, row in stores_list.iterrows():
			rows = dict(row)
			store_id = int(row['store_id'])
			print("store_ids acao preditiva")
			print(store_id)
			if is_holiday:
				reason = "occ_predictive_store_closed_holidays "
			else:
				try:
					if (row['city_id'].isin(city_holliday)).any():
						reason = "occ_predictive_store_closed_holidays"
					else:
						reason = "occ_predictive_store_closed"
				except:
					reason = "occ_predictive_store_closed"

			if store_id not in triggers:
				print(store_id)
				#cms.disable_store(store_id, country, reason, time_closed) == 200
				#cms.turnStoreOff(store_id, time_closed +10, country, reason) == 200
				#try:
				#	sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
				#except:
				#	print("fail sfx")

		text_brand = '''
		*Predictive Group Brand - Store Closed :alert:*
		*Group Brand suspendida por Store Closed :flag-{country}:*
		La Group Brand {store_name} (Group Brand ID: {store_id}) fue remitida a suspensión ({perc}) hasta {data} por cancelamiento por store closed
		*Flag*: {filhas}
		*City ID*: {city_id}
		*Total Orders*: {total_orders}
		*Total Store Closed Orders*: {total_sc_cancels}
		*Tiendas con store closed*: {closed_stores}
		*Store Ids suspendidos con store_closed*: {store_ids_trigger}
		*Store IDs suspendidos preventivamente*: {store_ids_suspended}
		'''.format(
			store_id=row['brand_group_id'],
			order_id=round((percentage*100),2),
			data=target_date.date(),
			country=country,
			city_id=int(row['city_id']*1),
			total_orders = int(total_orders),
			total_sc_cancels = int(total_cancels),
			total_stores=int(total_stores),
			closed_stores = int(closed_stores),
			store_ids_trigger = store_sc_ids,
			store_name=row['brand_group_name'],
			filhas= row['flag'],
			perc = perc,
			store_ids_suspended = store_ids_suspended
			)
		print(text_brand)
		slack.bot_slack(text_brand,'C015QR8KT62')
		#snow.upload_df_occ(to_upload_details,'predictive_groupbrand_details')
		try:
			if (row['city_id'].isin(city_holliday)).any():
				text_brand = '''
						*Predictive Group Brand - Store Closed :alert:*
						*Group Brand suspendida por Store Closed :flag-{country}:*
						La Group Brand {store_name} (Group Brand ID: {store_id}) fue remitida a suspensión ({perc}) hasta {data} por cancelamiento por store closed
						*Flag*: {filhas}
						*City ID*: {city_id}
						*Orders IDs*: {order_ids}
						*Total Orders*: {total_orders}
						*Total Store Closed Orders*: {total_sc_cancels}
						*Tiendas con store closed*: {closed_stores}
						*Store Ids suspendidos con store_closed*: {store_ids_trigger}
						*Store IDs suspendidos preventivamente*: {store_ids_suspended}
						'''.format(
					store_id=row['brand_group_id'],
					order_id=round((percentage * 100), 2),
					data=target_date.date(),
					country=country,
					city_id=int(row['city_id'] * 1),
					total_orders=int(total_orders),
					total_sc_cancels=int(total_cancels),
					total_stores=int(total_stores),
					closed_stores=int(closed_stores),
					store_ids_trigger=store_sc_ids,
					store_name=row['brand_group_name'],
					order_ids=row['order_ids'],
					filhas=row['flag'],
					perc=perc,
					store_ids_suspended=store_ids_suspended
				)
				print(text_brand)
				slack.bot_slack(text_brand,'C015QR8KT62')

		except AttributeError:
			pass

def predictive_storeclosed_afternoon(final_df,excluding_stores,store_infos, excluding_brands, df_suspendidas, df_sheets,df_suspendidas_cms_logger):
	print(final_df.columns)
	current_time = timezones.country_current_time(country)
	## filter verticals in final_df where we have all stores, all orders
	final_df = final_df[(final_df['vertical'].isin(['Express','Licores','Farmacia','Specialized','Turbo']))]

	## grouping brands and counting the total orders by cities
	qtd_orders_per_brands = final_df.groupby(["brand_group_id","brand_group_name","city_id"])['total_orders'].sum().reset_index(name="total_orders")

	## grouping brands and counting the total store closed cancels by cities
	qtd_storeclosed_per_brands = final_df.groupby(["brand_group_id","city_id"])['total_cancels'].sum().reset_index(name="total_cancels")
	## grouping brands and counting the total of stores by cities in store infos DF
	qtd_stores = store_infos.groupby(["brand_group_id","city_id"])['storeid'].count().reset_index(name="total_stores")
	## taking only store_ids with store closed
	stores_storeclosed = final_df[final_df['total_cancels'] > 0]

	## grouping brands and counting the total os stores with store closed by cities
	qtd_closed_stores = stores_storeclosed.groupby(["brand_group_id","city_id"])['storeid'].count().reset_index(name="closed_stores")
	## grouping brand and gettig a list os store_ids triggers
	lojas_ids = stores_storeclosed.groupby(["brand_group_id","city_id"])['storeid'].apply(list).reset_index(name='store_sc_ids')

	order_ids_df = stores_storeclosed.groupby(["brand_group_id","city_id"])['order_ids'].apply(list).reset_index(name='order_ids')
	order_ids_df['order_ids'] = [','.join(map(str, l)) for l in order_ids_df['order_ids']]

	# The next set of merges is in order to build a final brand DF, with all we need, by brands and it's cities
	# From total orders to brands with store closed  - LEFT JOIN
	union_brand_df = pd.merge(qtd_orders_per_brands,qtd_storeclosed_per_brands, how='left', on=['brand_group_id','city_id'])
	# Adding the total stores 
	union_brand_df = pd.merge(union_brand_df,qtd_stores, how='inner', on=['brand_group_id','city_id'])
	# Adding the total closed stores
	union_brand_df = pd.merge(union_brand_df,qtd_closed_stores, how='left', on=['brand_group_id','city_id'])
	# Getting the store_ids with store closed
	union_brand_df = pd.merge(union_brand_df,lojas_ids, how='left', on=['brand_group_id','city_id'])

	union_brand_df = pd.merge(union_brand_df,order_ids_df, how='left', on=['brand_group_id','city_id'])

	# Add the percentage os store closed by brands and it's
	union_brand_df["percentage"] = union_brand_df['total_cancels'] / union_brand_df['total_orders']
	# Filter the minimum threshold

	print("zero")
	print(union_brand_df)

	holidays_list = diasfestivos.get_national_holidays(country)
	is_holiday = current_time in holidays_list
	cities = union_brand_df['city_id'].unique()

	city_holliday = []
	for city_id in cities:
		city_id = str(city_id)
		try:
			holidays_list = diasfestivos.get_cities_holidays(city_id)
			is_holiday_city = timezones.country_current_time(country).date() in holidays_list
			if is_holiday_city:
				city_holliday.append(city_id)
		except UnboundLocalError:
			pass

	if is_holiday:
		print('é feriado')
		union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC_HOLIDAYS',
															 'STORES_WITH_SC_HOLIDAYS']], how='left',
								  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

		union_brand_df = union_brand_df[
			((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC_HOLIDAYS']) &
			 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC_HOLIDAYS']))
		]
	else:
		if (union_brand_df['city_id'].isin(city_holliday)).any():
			print('é feriado')
			print(city_holliday)
			union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC_HOLIDAYS',
																 'STORES_WITH_SC_HOLIDAYS']], how='left',
									  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

			union_brand_df = union_brand_df[
				((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC_HOLIDAYS']) &
				 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC_HOLIDAYS']))
			]
		else:
			print('não é feriado')
			union_brand_df = pd.merge(union_brand_df, df_sheets[['CITY_ID', 'MIN_ORDERS_SC',
																 'STORES_WITH_SC']], how='left',
									  left_on=union_brand_df['city_id'], right_on=df_sheets['CITY_ID'])

			union_brand_df = union_brand_df[
				((union_brand_df['total_cancels'] >= union_brand_df['MIN_ORDERS_SC']) &
				 (union_brand_df['closed_stores'] >= union_brand_df['STORES_WITH_SC']))
			]

	union_brand_df = union_brand_df[['brand_group_id', 'brand_group_name', 'city_id',
       'total_orders', 'total_cancels', 'total_stores', 'closed_stores',
       'store_sc_ids', 'order_ids', 'percentage']]

	print("after")
	print(union_brand_df)

	news_brands = pd.merge(union_brand_df,excluding_brands, how='left', on=["brand_group_id","city_id"])
	# Taking only news brands by cities
	news_brands1 = news_brands[news_brands['brandgroupid'].isnull()]
	
	# Add the flag first, because it will be suspended now and the bot needs to have it for future checks
	news_brands1['flag'] = 'first'

	## uninon of new brands (first flag) and brands with the second flag 
	final_brand_df= news_brands1

	## it does not include brands with second flag
	#final_brand_df = pd.merge(final_brand_df,excluding_brands_second, how='left', on=["brand_group_id","city_id"])
	#final_brand_df = final_brand_df[final_brand_df['brandgroupid_y'].isnull()]
	#final_brand_df = final_brand_df.drop(['brandgroupid_y','brandgroupid_x','percentage_sc_x','flag_y','percentage_sc_y'], axis=1)
	#final_brand_df = final_brand_df.rename(columns={'flag_x': 'flag'})

	current_time = timezones.country_current_time(country)

	# DF for the log upload and drop the old percentage sc column in order to set the new percentage
	# set the new percentage of store closed
	to_upload_brand = final_brand_df.rename(columns={'percentage': 'percentage_sc'})
	to_upload_brand['suspended_at'] = current_time
	to_upload_brand['country'] = country
	to_upload_brand['store_sc_ids'] = [','.join(map(str, l)) for l in to_upload_brand['store_sc_ids']]
	to_upload_brand = to_upload_brand[["brand_group_id","total_cancels","closed_stores","percentage_sc","suspended_at","city_id","flag","country","store_sc_ids"]]

	#snow.upload_df_occ(to_upload_brand,'predictive_groupbrand')

	# for each brand_group_id, per citites
	for index, row in final_brand_df.iterrows():
		total_orders = row['total_orders']
		percentage = row['percentage']
		total_cancels = row['total_cancels']
		closed_stores = row['closed_stores']
		total_stores = row['total_stores']
		store_sc_ids = row['store_sc_ids']
		order_ids = row['order_ids']
		if row['flag'] == 'first':
			perc = 'Percentil 50'
		elif row['flag'] == 'second':
			perc = 'Percentil 50'

		# Take the store_ids from store_infos base that have the same brand_group_id and the same city_id
		stores_list = store_infos[((store_infos['brand_group_id'] == row['brand_group_id']) & (store_infos['city_id'] == row['city_id']))]

		# if it is the first time, then take the stores classified with less than 0.25 or equal to
		if row['flag'] == 'first':
			stores_list = stores_list[(stores_list['percentile_inside_brand'] <= 0.50)]
			stores_list['flag'] = 'percentil_50'
		# else (second) take the greater percencil 
		else:
			stores_list = stores_list[((stores_list['percentile_inside_brand'] > 0.50) & (stores_list['percentile_inside_brand'] <= 0.50))]
			stores_list['flag'] = 'percentil_50'

		# excluding stores that has been suspended already
		stores_list = pd.merge(stores_list,excluding_stores,how='left',left_on=stores_list['storeid'],right_on=excluding_stores['storeid'])

		#stores_list = stores_list[stores_list['storeid_y'].isnull()]
		stores_list = stores_list.drop(['key_0','storeid_y'], axis=1)
		stores_list = stores_list.rename(columns={'storeid_x': 'store_id'})

		stores_list = pd.merge(stores_list, df_suspendidas, how='left', left_on=stores_list['store_id'], right_on=df_suspendidas['suspendida_id'])
		stores_list = stores_list[stores_list['suspendida_id'].isnull()]
		stores_list = stores_list.drop(['key_0'], axis=1)

		stores_list = pd.merge(stores_list, df_suspendidas_cms_logger, how='left', left_on=stores_list['store_id'],right_on=df_suspendidas_cms_logger['storeid'])
		print(stores_list)
		stores_list = stores_list[~stores_list.content_is_enable.isin(['False', 'false', 'FALSE'])]  # diferente de false
		print(stores_list)
		print(stores_list.vertical.unique())

		# in order to avoid a store id from other verticals
		stores_list = stores_list[(stores_list['vertical'].isin(['Express','Licores','Farmacia','Specialized','Turbo']))]

		store_ids_suspended = stores_list.groupby(["brand_group_id","brand_group_name","city_id"])["store_id"].apply(list).reset_index(name='store_ids')
		store_ids_suspended['store_ids'] = [','.join(map(str, l)) for l in store_ids_suspended['store_ids']]
		store_ids_suspended = store_ids_suspended['store_ids'].to_list()


		target_date = (current_time + timedelta(days=1)).replace(hour=0, minute=0, second=0, microsecond=0)
		time_closed = int((target_date - current_time).total_seconds() / 60.0) + randrange(40)

		stores_list['suspended_at'] = current_time
		stores_list['suspended_until'] = target_date

		to_upload_details = stores_list[["store_id","city_id","brand_group_id","suspended_at","suspended_until","flag"]]
		count = to_upload_details['store_id'].count()
		print(count)

		# suspend closed_stores
		triggers = row['store_sc_ids']
		print("triggers_list")
		print(triggers)
		reason = "predictive_store_closed"
		if len(triggers) >= 1:
			for trigger in triggers:
				print("trigger")
				print(trigger)
				# no go, because the next function will suspend this store (threshold >=1)
				#cms.turnStoreOff(trigger, time_closed,country, reason) == 200

		# in order to iterate and suspend each store_id (preditive action)
		for index, row in stores_list.iterrows():
			rows = dict(row)
			store_id = int(row['store_id'])
			print("store_ids acao preditiva")
			print(store_id)
			if is_holiday:
				reason = "occ_predictive_store_closed_holidays "
			else:
				try:
					if (row['city_id'].isin(city_holliday)).any():
						reason = "occ_predictive_store_closed_holidays"
					else:
						reason = "occ_predictive_store_closed"
				except:
					reason = "occ_predictive_store_closed"

			if store_id not in triggers:
				print(store_id)
				#cms.disable_store(store_id, country, reason, time_closed) == 200
				#cms.turnStoreOff(store_id, time_closed +10, country, reason) == 200
				#try:
				#	print("sfx")
				#	sfx.send_events_turnStoreOff(store_id, country, reason, current_time)
				#except:
				#	print("fail sfx")
					
		text_brand = '''
		*Predictive Group Brand - Store Closed :alert:*
		*Group Brand suspendida por Store Closed :flag-{country}:*
		La Group Brand {store_name} (Group Brand ID: {store_id}) fue remitida a suspensión ({perc}) hasta {data} por cancelamiento por store closed
		*Flag*: {filhas}
		*City ID*: {city_id}
		*Total Orders*: {total_orders}
		*Total Store Closed Orders*: {total_sc_cancels}
		*Tiendas con store closed*: {closed_stores}
		*Store Ids suspendidos con store_closed*: {store_ids_trigger}
		*Store IDs suspendidos preventivamente*: {store_ids_suspended}
		'''.format(
			store_id=row['brand_group_id'],
			order_id=round((percentage*100),2),
			data=target_date.date(),
			country=country,
			city_id=int(row['city_id']*1),
			total_orders = int(total_orders),
			total_sc_cancels = int(total_cancels),
			total_stores=int(total_stores),
			closed_stores = int(closed_stores),
			store_ids_trigger = store_sc_ids,
			store_name=row['brand_group_name'],
			filhas= row['flag'],
			perc = perc,
			store_ids_suspended = store_ids_suspended
			)
		print(text_brand)
		slack.bot_slack(text_brand,'C015QR8KT62')
		#snow.upload_df_occ(to_upload_details,'predictive_groupbrand_details')

		try:
			if (row['city_id'].isin(city_holliday)).any():
				text_brand = '''
						*Predictive Group Brand - Store Closed :alert:*
						*Group Brand suspendida por Store Closed :flag-{country}:*
						La Group Brand {store_name} (Group Brand ID: {store_id}) fue remitida a suspensión ({perc}) hasta {data} por cancelamiento por store closed
						*Flag*: {filhas}
						*City ID*: {city_id}
						*Orders IDs*: {order_ids}
						*Total Orders*: {total_orders}
						*Total Store Closed Orders*: {total_sc_cancels}
						*Tiendas con store closed*: {closed_stores}
						*Store Ids suspendidos con store_closed*: {store_ids_trigger}
						*Store IDs suspendidos preventivamente*: {store_ids_suspended}
						'''.format(
					store_id=row['brand_group_id'],
					order_id=round((percentage * 100), 2),
					data=target_date.date(),
					country=country,
					city_id=int(row['city_id'] * 1),
					total_orders=int(total_orders),
					total_sc_cancels=int(total_cancels),
					total_stores=int(total_stores),
					closed_stores=int(closed_stores),
					store_ids_trigger=store_sc_ids,
					store_name=row['brand_group_name'],
					order_ids=row['order_ids'],
					filhas=row['flag'],
					perc=perc,
					store_ids_suspended=store_ids_suspended
				)
				print(text_brand)
				slack.bot_slack(text_brand,'C015QR8KT62')

		except AttributeError:
			pass

		if count >= 50:
			text_brand_occ = '''
			*Predictive Group Brand - Store Closed :alert:*
			*Group Brand suspendida por Store Closed :flag-{country}:*
			La Group Brand {store_name} (Group Brand ID: {store_id}) fue remitida a suspensión ({perc}) hasta {data} por cancelamiento por store closed
			*Flag*: {filhas}
			*City ID*: {city_id}
			*Total Orders*: {total_orders}
			*Total Store Closed Orders*: {total_sc_cancels}
			*Tiendas con store closed*: {closed_stores}
			*Store Ids suspendidos con store_closed*: {store_ids_trigger}
			*Store IDs suspendidos preventivamente*: {store_ids_suspended}
			*Order IDs*: {order_idss}
			'''.format(
				store_id=row['brand_group_id'],
				order_id=round((percentage*100),2),
				data=target_date.date(),
				country=country,
				city_id=int(row['city_id']*1),
				total_orders = int(total_orders),
				total_sc_cancels = int(total_cancels),
				total_stores=int(total_stores),
				closed_stores = int(closed_stores),
				store_ids_trigger = store_sc_ids,
				store_name=row['brand_group_name'],
				filhas= row['flag'],
				perc = perc,
				order_idss=order_ids,
				store_ids_suspended = store_ids_suspended
				)

			print(text_brand_occ)
			slack.bot_slack(text_brand_occ,'C015QR8KT62')


for country in countries:
	country = country.lower()
	print(country)
	current_time = timezones.country_current_time(country)
	df_sheets = df[df.COUNTRY == country.upper()]
	df_suspendidas_cms_logger = suspendidas_cms_logger(country)
	try:
		if current_time.strftime('%H:%M') >= '07:00' and current_time.strftime('%H:%M') <= '10:00':
			try:
				print("morning")
				a,b,c,d = dataframe(country, "morning")
				df_suspendidas = suspendidas(country)
				print("suspendidas")
				print(df_suspendidas)
				predictive_storeclosed_morning(a,b,c,d,df_suspendidas,df_sheets, df_suspendidas_cms_logger)
			except Exception as e:
				print(e)

		if current_time.strftime('%H:%M') >= '10:00' and current_time.strftime('%H:%M') <= '23:50':
			try:
				print("afternoon")
				a,b,c,d = dataframe(country, "afternoon")
				df_suspendidas = suspendidas(country)
				predictive_storeclosed_afternoon(a,b,c,d,df_suspendidas,df_sheets, df_suspendidas_cms_logger)
			except Exception as e:
				print(e)
	except Exception as e:
		print(e)