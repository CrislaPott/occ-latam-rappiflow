import os, sys, json
import numpy as np
import pandas as pd
from random import randrange
from datetime import datetime, timedelta
from lib import slack, erp, cms, sfx
from lib import snowflake as snow
from functions import timezones
import get_slots as gs
import pytz
import get_data as gd
from lib import gsheets

df = gsheets.read_sheet('124cHwzNqDM6DwwJHzhWobBAvtjd44okI_XIqpueK7EU', 'Store Closed - Tiendas SUPER')
df = df[(df["ON/OFF"] == 'ACTIVE')]
df.MIN_CANCELS = df.MIN_CANCELS.astype(float)
countries = df['COUNTRY'].unique()
print(countries)

def dataframe(country):

    excluding_stores, excluding_brands,excluding_physical_stores, store_infos, base_orders, store_closed_om, chats = gd.data_frames(country)

    cancellation = base_orders[(base_orders['order_id'].notnull())]
    cancellation_super = base_orders[((base_orders['order_id'].notnull()) & (base_orders['bug_place_at'] == 'no'))]
    print(cancellation_super)

    base_orders = base_orders.groupby(["storeid"])["all_order_id"].count().reset_index(name="total_orders")

## Final DF for store closed and predictive store closed
    try:
        df = pd.merge(cancellation,store_closed_om,how='left',left_on=cancellation['order_id'],right_on=store_closed_om['order_id_modifications'])
        df = df.drop(['key_0'], axis=1)
    except:
        df = cancellation
        df['order_id_modifications'] = 0

    try:
        df = pd.merge(df,chats,how='left',left_on=df['order_id'],right_on=chats['orderid'])
        df = df.drop(['key_0'], axis=1)
    except:
        df = df
        df['orderid'] = 0

    df['order_id_modifications'] = df['order_id_modifications'].fillna(0)
    df['orderid'] = df['orderid'].fillna(0)
    df = df[(df['order_id_modifications'] != 0) | (df['orderid'] != 0)]
    df['order_id_canceled'] = np.where(df['order_id_modifications']!= 0, df['order_id_modifications'],df['orderid'])
    df = df.drop(['order_id','orderid','order_id_modifications'], axis=1)

    df1 = df.groupby(["storeid", "store_name"])["order_id_canceled"].count().reset_index(name="total_cancels")
    df2 = df.groupby(["storeid","store_name"])["order_id_canceled"].apply(list).reset_index(name='order_ids')

    final_cancellation = pd.merge(df1,df2[['storeid','order_ids']], how='inner',on=['storeid'])
    final_cancellation['order_ids'] = [','.join(map(str, l)) for l in final_cancellation['order_ids']]

    final_df = pd.merge(base_orders,final_cancellation, how='left',on=['storeid'])

    final_df = pd.merge(final_df,store_infos,how='left',on=['storeid'])

## Final Super DF for slot store closed
    try:
        df_super = pd.merge(cancellation_super,store_closed_om,how='left',left_on=cancellation_super['order_id'],right_on=store_closed_om['order_id_modifications'])
        df_super = df_super.drop(['key_0'], axis=1)
    except:
        df_super = cancellation_super
        df_super['order_id_modifications'] = 0

    try:
        df_super = pd.merge(df_super,chats,how='left',left_on=df_super['order_id'],right_on=chats['orderid'])
        df_super = df_super.drop(['key_0'], axis=1)
    except:
        df_super = df_super
        df_super['orderid'] = 0

    df_super['order_id_modifications'] = df_super['order_id_modifications'].fillna(0)
    df_super['orderid'] = df_super['orderid'].fillna(0)
    df_super = df_super[(df_super['order_id_modifications'] != 0) | (df_super['orderid'] != 0)]
    df_super['order_id_canceled'] = np.where(df_super['order_id_modifications']!= 0, df_super['order_id_modifications'],df_super['orderid'])
    df_super = df_super.drop(['order_id','orderid','order_id_modifications'], axis=1)

    df_super1 = df_super.groupby(["storeid", "store_name"])["order_id_canceled"].count().reset_index(name="total_cancels")
    df_super2 = df_super.groupby(["storeid","store_name"])["order_id_canceled"].apply(list).reset_index(name='order_ids')
    print("dfs")
    print(df_super1)
    print(df_super2)

    try:
        final_cancellation_super = pd.merge(df_super1,df_super2[['storeid','order_ids']], how='inner',on=['storeid'])
        final_cancellation_super['order_ids'] = [','.join(map(str, l)) for l in final_cancellation_super['order_ids']]
        final_df_super = pd.merge(base_orders,final_cancellation_super, how='left',on=['storeid'])
        final_df_super = pd.merge(final_df_super,store_infos,how='left',on=['storeid'])
    except:
        final_df_super = df_super1

    return final_df, excluding_stores, store_infos, excluding_brands,excluding_physical_stores, final_df_super


def close_slots(final_df,excluding_physical_stores,df_sheets):
    other_df = final_df[(final_df['vertical'].isin(['Mercados','Specialized','Ecommerce']))]
    final_df = other_df
    final_df = final_df.groupby(["physical_store_id","physical_store_name",'city_id'])['total_cancels'].sum().reset_index(name="total_sc_cancels")
    other_df = other_df.groupby(["physical_store_id"])['order_ids'].apply(list).reset_index(name='order_ids')

    final_df = pd.merge(final_df,other_df[['physical_store_id','order_ids']], how='inner',on=['physical_store_id'])
    final_df['order_ids'] = [','.join(map(str, l)) for l in final_df['order_ids']]

    current_time = timezones.country_current_time(country)
    final_df['city_id'] = final_df['city_id'].astype(str)
    df_sheets['CITY_ID'] = df_sheets['CITY_ID'].astype(str)
    final_df = pd.merge(final_df, df_sheets[['CITY_ID', 'MIN_CANCELS']], how='inner', left_on=final_df['city_id'],
                        right_on=df_sheets['CITY_ID'])
    final_df = final_df.drop(['key_0', 'CITY_ID'], axis=1)

    if current_time.strftime('%H:%M') >= '00:00' and current_time.strftime('%H:%M') <= '07:00':
        final_df = final_df[(final_df['total_sc_cancels'] >= final_df['MIN_CANCELS'])]
    else:
        final_df = final_df[(final_df['total_sc_cancels'] >= final_df['MIN_CANCELS'])]


    final_df = pd.merge(final_df,excluding_physical_stores,how='left',left_on=final_df['physical_store_id'],right_on=excluding_physical_stores['p_store_id'])
    final_df = final_df[(final_df['p_store_id'].isnull()) | (final_df['total_sc_cancels'] >= (final_df['total_cancels'] +2))]
    final_df = final_df.drop(['key_0','p_store_id'], axis=1)

    print(current_time.strftime('%H:%M'))
    if current_time.strftime('%H:%M') >= '00:00' and current_time.strftime('%H:%M') <= '07:00':
        time = current_time.strftime('%Y-%m-%d 11:00')
        print(time)
    elif current_time.strftime('%H:%M') > '07:00' and current_time.strftime('%H:%M') <= '12:00':
        time = current_time.strftime('%Y-%m-%d 13:00')
        print(time)
    elif current_time.strftime('%H:%M') > '12:00' and current_time.strftime('%H:%M') <= '23:59':
        time = current_time.strftime('%Y-%m-%d 23:00')
        print(time)

    final_df.drop_duplicates()
    for index, row in final_df.iterrows():
        try:
            rows = dict(row)
            print(row['physical_store_id'])

            physical_store_id = row['physical_store_id']
            total_cancels = row['total_sc_cancels']
            suspended_at = current_time
            suspended_until = time
            physical_store_name = row['physical_store_name']
            order_ids = row['order_ids']
            reason = 'occ_store_closed'
            slot_ids = gs.slots_to_close(country,physical_store_id,time)

            for slot_id in slot_ids:
                print(slot_id)
                try:
                    erp.change_slot_status(int(slot_id), False, country) == 200
                    try:
                        sfx.send_events_slotDitchesClose(slot_id, physical_store_id, country, reason, current_time)
                    except Exception as e:
                        print(e)
                except Exception as e:
                    print(e)

            query = f'''
            insert into ops_occ.store_closed_super
            values
            ('{physical_store_id}','{total_cancels}','{suspended_at}','{suspended_until}','{slot_ids}','{country}','{order_ids}');
            '''
            snow.inserquery(query)

            text_super = f'''
            *SLOTstore closed*
            Name: {physical_store_name}
            Country: :flag-{country}:
            Physical Store ID: {physical_store_id} 
            Slot IDs: {slot_ids}
            Suspendido hasta: {suspended_until}
            Order IDs: {order_ids}
            '''
            print(text_super)
            slack.bot_slack(text_super,'C01S0667VKP')
        except Exception as e:
            print(e)


for country in countries:
    country = country.lower()
    print(country)
    current_time = timezones.country_current_time(country)
    df_sheets = df[df.COUNTRY == country.upper()]
    try:
        a,b,c,d,e,f = dataframe(country)
        close_slots(f,e,df_sheets)
    except Exception as e:
        print(e)