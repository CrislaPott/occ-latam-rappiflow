import pandas as pd
import numpy as np
from lib import redash
from lib import snowflake as snow
from ast import literal_eval
from pandas.io.json import json_normalize


def getdata(country):
    query = """
{
    "collection": "cmsloggers",
    "query": {
        "url": "/api/cms/gateway/override/api/cms/override/v3",
        "created_at": {
            "$gt": {
                "$humanTime": "1 days ago"
            }
        }
    }
}

    """
    if country == 'co':
        metrics = redash.run_query(4749, query)
    elif country == 'ar':
        metrics = redash.run_query(4748, query)
    elif country == 'cl':
        metrics = redash.run_query(4746, query)
    elif country == 'uy':
        metrics = redash.run_query(4743, query)
    elif country == 'mx':
        metrics = redash.run_query(4750, query)
    elif country == 'ec':
        metrics = redash.run_query(4745, query)
    elif country == 'cr':
        metrics = redash.run_query(4744, query)
    elif country == 'pe':
        metrics = redash.run_query(4747, query)
    elif country == 'br':
        metrics = redash.run_query(4751, query)

    return metrics


countries = ['co', 'br', 'mx', 'ar', 'cl', 'uy', 'ec', 'cr', 'pe']
for country in countries:
    try:
        print('logs: ' + country)
        query1 = "select distinct _id::text as _id, 'reported' as reportado from ops_occ.global_cms_logger_product where country='{country}' and created_at::date >= current_date::date - 1".format(
            country=country)
        df_log = snow.run_query(query1)
        print(df_log)

        print('running redash for: ' + country)
        df = getdata(country)
        print(df)
        df1 = df[['_id', 'user', 'content', 'created_at']]
        df1.content = df1.content.fillna('[]')
        df1.content = df1.content.astype(str)
        df1.content = df1.content.str.replace('null', 'None')
        df1.content = df1.content.apply(literal_eval)
        df1 = df1.explode('content').reset_index(drop=True)
        df1.content = df1.content.fillna({i: {} for i in df.index})
        df2 = json_normalize(df1.content)
        df3 = df1[['_id', 'user', 'created_at']]
        df = pd.concat([df3, df2], axis=1)
        if 'status' not in df.columns:
            df['status'] = np.nan
        if 'price' not in df.columns:
            df['price'] = np.nan
        elif 'in_stock' not in df.columns:
            df['in_stock'] = np.nan
        df = df[['_id', 'user', 'created_at', 'store_id', 'reason', 'starts_at', 'product_id', 'ends_at', 'in_stock',
                 'status',
                 'price']].rename(columns={'store_id': 'content.store_id', 'reason': 'content.reason',
                                           'starts_at': 'content.starts_at', 'product_id': 'content.product_id',
                                           'ends_at': 'content.ends_at', 'in_stock': 'content.in_stock',
                                           'status': 'content.status', 'price': 'content.price'})
        df['country'] = country

        df = pd.merge(df, df_log, how='left', on=['_id'])
        df = df[df['reportado'].isnull()]
        df = df.drop(['reportado'], axis=1)

        print(df)
        if not df.empty:
            print('snowflake')
            snow.upload_df_occ(df, 'global_cms_logger_product')
        else:
            print('vazio')

    except Exception as e:
        print(e)