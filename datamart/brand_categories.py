import os, sys, json
import pandas as pd
from lib import slack
from lib import snowflake as snow

query = """
create or replace table ops_occ.group_brand_category as (
    WITH BASE
             AS (SELECT DISTINCT G.COUNTRY,
                                 G.BRAND_GROUP_ID,
                                 G.BRAND_GROUP,
                                 G.CITY_ADDRESS_ID,
                                 G.VERTICAL,
                                 ROUND(SUM(G.GMV_USD), 2)        AS total,
                                 PERCENT_RANK()
                                         OVER (
                                             PARTITION BY G.COUNTRY,G.VERTICAL,G.CITY_ADDRESS_ID
                                             ORDER BY TOTAL ASC) AS RANK
                 FROM OPS_GLOBAL.GLOBAL_ORDERS G
                 WHERE CREATED_AT :: DATE >= CURRENT_DATE - 30
                   AND COALESCE(BRAND_GROUP_ID :: TEXT, BRAND_GROUP :: TEXT) IS NOT NULL
                 GROUP BY 1,
                          2,
                          3,
                          4,
                          5

                 ORDER BY 7 DESC)
    SELECT COUNTRY,
           BRAND_GROUP_ID,
           BRAND_GROUP,
           CITY_ADDRESS_ID,
           VERTICAL,
           CASE
               WHEN RANK BETWEEN 0 AND 0.25 THEN 'D'
               WHEN RANK BETWEEN 0.25 AND 0.50 THEN 'C'
               WHEN RANK BETWEEN 0.50 AND 0.75 THEN 'B'
               WHEN RANK BETWEEN 0.75 AND 1 THEN 'A'
               END AS CATEGORY
    FROM BASE
)
                    """

df = snow.run_query(query)