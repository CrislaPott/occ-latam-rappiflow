import pandas as pd
import numpy as np
from lib import redash
from lib import snowflake as snow

def getdata(country):
	query = """
	{
    "collection": "cmsloggers",
    "query": {
        "url": {"$regex":"/api/cms/gateway/stores-adm/api/ms/stores-adm/stores/massive/enabled"},
	     
	    "created_at": {
			"$gt": {
				"$humanTime": "1 days ago"
			}
	}
	}
	}
	"""
	if country == 'co':
		metrics = redash.run_query(4749, query)
	elif country == 'ar':
		metrics = redash.run_query(4748, query)
	elif country == 'cl':
		metrics = redash.run_query(4746, query)
	elif country == 'uy':
		metrics = redash.run_query(4743, query)
	elif country == 'mx':
		metrics = redash.run_query(4750, query)
	elif country == 'ec':
		metrics = redash.run_query(4745, query)
	elif country == 'cr':
		metrics = redash.run_query(4744, query)
	elif country == 'pe':
		metrics = redash.run_query(4747, query)
	elif country == 'br':
		metrics = redash.run_query(4751, query)

	return metrics

occ_team = {'OCC_PRODUCT_TEAM_API_KEY':'BOT OCC', 'andrea.gonzalez@rappi.com':'OCC KPIS','higor.silva@rappi.com':'OCC KPIS','danilo.martello@rappi.com':'OCC KPIS',
			'kaliana.anunciacao@rappi.com':'OCC KPIS','sefhora.maciel@rappi.com':'OCC KPIS','samantha.silva@rappi.com':'OCC KPIS',
			'leticia.silva@rappi.com':'OCC KPIS','pamela.rodrigues@rappi.com':'OCC KPIS','bpo-f.gomez@rappi.com':'OCC KPIS',
			'gibran.malagon@rappi.com':'OCC KPIS','bpo-s.charryperez@rappi.com':'OCC KPIS','gustavo.cardoso@rappi.com':'OCC KPIS',
			'laura.misnaza@rappi.com':'OCC KPIS','lirism.molina@rappi.com':'OCC KPIS','victor.gonzalez@rappi.com':'OCC KPIS',
			'eduardo.gonzalezgarc@rappi.com':'OCC Non-Live', 'johan.lopez@rappi.com':'OCC KPIS',
			'isabel.germano@rappi.com':'OCC Non-Live','cayke.emidio@rappi.com':'OCC Non-Live',
			'natalia.aguilera@rappi.com':'OCC Kustomer','alejandra.diaz@rappi.com':'OCC Kustomer','bpo-m.barragan@rappi.com':'OCC Kustomer',
			'ana.moreira@rappi.com':'OCC Kustomer','luismiguel.hernandez@rappi.com':'OCC Kustomer','pamela.toro@rappi.com':'OCC Kustomer',
			'bpo-g.florezbeltran@rappi.com':'OCC Balanceo','eduardo.junior@rappi.com':'OCC Balanceo','luisa.martinrojas@rappi.com':'OCC Balanceo',
			'sharon.maury@rappi.com':'OCC Balanceo','juan.rojasortigoza@rappi.com':'OCC Balanceo','larissa.braga@rappi.com':'OCC Balanceo',
			'eduardo.deus@rappi.com':'OCC Balanceo','arlex.arevalo@rappi.com':'OCC Balanceo','bpo-l.corredor@rappi.com':'OCC Non-Live','steven.aldana@rappi.com':'OCC Non-Live',
			'mayra.barragan@rappi.com':'OCC Kustomer','gaby.florez@rappi.com':'OCC Balanceo','leydi.corredor@rappi.com':'OCC Non-Live','andres.ardila@rappi.com':'OCC Kustomer',
			'fernando.gomez@rappi.com':'OCC KPIS','sebastian.charry@rappi.com':'OCC KPIS','yisel.vega@rappi.com':'OCC Slot Management','leandro.sousa@rappi.com':'OCC Turbo',
			'ext-a.guimaraes@rappi.com':'OCC Turbo','ext-a.msilva@rappi.com':'OCC Turbo'
}

countries = ['co','br','mx','ar','cl','uy','ec','cr','pe']
for country in countries:
	try:
		print('running redash for: ' + country)
		df = getdata(country)
		df['country'] = country
		##df['store_id'] = df['url'].map(lambda x: x.lstrip('/api/cms/gateway/cms-stores/api/cms/stores/enable-disable/'))
		df = df.assign(var1=df['content.store_ids'].str.split(',')).explode('content.store_ids')
		df['action'] = np.where(df['content.enabled'] == True, 'turn_store_on - massive', 'turn_store_off - massive')
		df['content.time'] =  None
		df['content.reason'] =  None
		df['content.is_enabled'] =  None
		df['_id'] = df['_id'].astype(str)
		df = df[['country','user','created_at','content.store_ids', 'action',
			 'content.reason','content.time','content.enabled','content.is_enabled',
			 'url','method','description','status_code','type','tag','_id']].rename(columns={"content.time": "content_time",
																					   "content.reason": "content_reason",
																					   "content.enabled": "content_is_enable",
																					   "content.is_enabled": "content_is_enabled",
																					   "content.store_ids": "store_id"})
		df['team'] = df['user'].map(occ_team)
		query1 = f"""select distinct _id::text as _id, 'reported' as reportado from ops_occ.global_cms_logger where country='{country}'"""
		df_log = snow.run_query(query1)
		print(df_log)
		df = pd.merge(df, df_log, how='left', on=['_id'])
		df = df[df['reportado'].isnull()]
		df = df.drop(['reportado'], axis=1)
		snow.upload_df_occ(df,'global_cms_logger')
	except Exception as e:
		print(e)
