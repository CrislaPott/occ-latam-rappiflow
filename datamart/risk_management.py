import os, sys, json
import pandas as pd
from lib import slack
from lib import snowflake as snow

query = """
CREATE OR REPLACE table ops_occ.RISK_MANAGEMENT
AS (
WITH BR AS (
WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'BR'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),

STORES AS

(
                SELECT DISTINCT 'BR' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            BR_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            BR_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       BR_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       BR_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       BR_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     BR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

AR AS (
WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'AR'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),

STORES AS

(
                SELECT DISTINCT 'AR' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            AR_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            AR_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       AR_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       AR_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       AR_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     AR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

CL AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'CL'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),

STORES AS

(
                SELECT DISTINCT 'CL' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            CL_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            CL_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       CL_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       CL_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       CL_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     CL_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

CO AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'CO'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),

REST AS
(
         SELECT   Count(DISTINCT DAY_OF_WEEK) AS DAYS,
                  STORE_ID,
                  Max(
                  CASE
                           WHEN DAY_OF_WEEK = 'wed' THEN STARTS_TIME
                  END) AS START_WED,
                  Max(
                  CASE
                           WHEN DAY_OF_WEEK = 'wed' THEN ENDS_TIME
                  END) AS END_WED,
                  Max(
                  CASE
                           WHEN DAY_OF_WEEK = 'fri' THEN STARTS_TIME
                  END) AS START_FRI,
                  Max(
                  CASE
                           WHEN DAY_OF_WEEK = 'fri' THEN ENDS_TIME
                  END) AS END_FRI
         FROM     CO_PGLR_MS_STORE_SCHEDULES_PUBLIC.SCHEDULES
         WHERE    _FIVETRAN_DELETED = 'FALSE'
         AND      DAY_OF_WEEK NOT ILIKE '%cal%'
         GROUP BY 2),

CPGS AS
(
          SELECT    COUNT(DISTINCT A.DAY_OF_WEEK) AS DAYS,
                    A.PHYSICAL_STORE_ID,
                    S.STORE_ID,
                    MAX(
                    CASE
                              WHEN A.DAY_OF_WEEK = '3' THEN TO_TIME(A.OPEN_TIME, 'HH24:MI:SS') - INTERVAL '3 hours'
                    END) AS START_WED,
                    MAX(
                    CASE
                              WHEN A.DAY_OF_WEEK = '3' THEN TO_TIME(A.CLOSE_TIME, 'HH24:MI:SS') - INTERVAL '3 hours'
                    END) AS END_WED,
                    MAX(
                    CASE
                              WHEN A.DAY_OF_WEEK = '5' THEN TO_TIME(A.OPEN_TIME, 'HH24:MI:SS') - INTERVAL '3 hours'
                    END) AS START_FRI,
                    MAX(
                    CASE
                              WHEN A.DAY_OF_WEEK = '5' THEN TO_TIME(A.CLOSE_TIME, 'HH24:MI:SS') - INTERVAL '3 hours'
                    END) AS END_FRI
          FROM      CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.PHYSICAL_STORE_OPEN_HOURS A
          JOIN
                    (
                             SELECT   *,
                                      DENSE_RANK() OVER (PARTITION BY PHYSICAL_STORE_ID, DAY_OF_WEEK ORDER BY CREATED_AT DESC) AS RANK
                             FROM     CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.PHYSICAL_STORE_OPEN_HOURS) B
          ON        B.PHYSICAL_STORE_ID = A.PHYSICAL_STORE_ID
          AND       A.DAY_OF_WEEK = B.DAY_OF_WEEK
          AND       A.OPEN_TIME = B.OPEN_TIME
          AND       A.CLOSE_TIME = B.CLOSE_TIME
          AND       B.RANK = 1
          LEFT JOIN CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES S
          ON        S.PHYSICAL_STORE_ID = A.PHYSICAL_STORE_ID
          GROUP BY  2,
                    3),

STORES AS

(
                SELECT DISTINCT 'CO' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            CO_CORE_ORDERS_PUBLIC.ORDERS_VW O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            CO_CORE_ORDERS_PUBLIC.ORDER_STORES_VW
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       CO_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       CO_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       CO_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     CO_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

MX AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'MX'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),


STORES AS

(
                SELECT DISTINCT 'MX' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            MX_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            MX_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       MX_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       MX_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       MX_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     MX_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

PE AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'PE'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),


STORES AS

(
                SELECT DISTINCT 'PE' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            PE_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            PE_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       PE_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       PE_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       PE_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     PE_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

CR AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'CR'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),

STORES AS

(
                SELECT DISTINCT 'CR' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            CR_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            CR_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       CR_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       CR_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       CR_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     CR_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

EC AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'EC'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),


STORES AS

(
                SELECT DISTINCT 'EC' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            EC_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            EC_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       EC_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       EC_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       EC_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     EC_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),



ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC),

UY AS (

WITH GMV AS
(
         SELECT   O.COUNTRY,
                  O.STORE_ID,
                  O.VERTICAL_SUB_GROUP AS VERTICAL,
                  COUNT(DISTINCT O.ORDER_ID) AS FINISHED_ORDERS,
                  Sum(O.GMV_USD) AS GMV
         FROM     GLOBAL_FINANCES.GLOBAL_ORDERS O
         WHERE    O.ORDER_STATE IN ('pending_review',
                                    'finished')
         AND      O.COUNT_TO_GMV = 'TRUE'
         AND      O.STATE_TYPE = 'FINISHED'
         AND      O.DATE_ORDER_CREATED >= CURRENT_DATE -30
         AND      O.COUNTRY = 'UY'
         GROUP BY 1,
                  2,
                  3
         ORDER BY 3 DESC),


STORES AS

(
                SELECT DISTINCT 'UY' AS COUNTRY,
                                S.STORE_ID,
                                G.VERTICAL,
                                S.NAME,
                                S.TYPE AS STORE_TYPE,
                                SS.SCHEDULED,
                                COALESCE(ST.BRAND_GROUP_NAME, B.NAME, 'NO_TIENE') AS BRAND,
                                COUNT(DISTINCT O.ID)                              AS TOTAL,
                                PH.PHYSICAL_STORE_ID,
                                G.GMV,
                                G.FINISHED_ORDERS,
                                COUNT(distinct(CASE
                                WHEN o.state iLIKE '%cancel%' THEN o.id
                                ELSE NULL END)) / count(DISTINCT o.id) AS Cancel_rate
                FROM            UY_CORE_ORDERS_PUBLIC.ORDERS O
                INNER JOIN
                                (
                                                SELECT DISTINCT ORDER_ID,
                                                                NAME,
                                                                STORE_ID,
                                                                TYPE
                                                FROM            UY_CORE_ORDERS_PUBLIC.ORDER_STORES
                                                WHERE           COALESCE(_FIVETRAN_DELETED,FALSE)=FALSE) OS
                ON              O.ID = OS.ORDER_ID
                LEFT JOIN       UY_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON              S.STORE_ID = OS.STORE_ID
                LEFT JOIN       UY_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN       UY_PGLR_MS_STORES_PUBLIC.STORE_TYPES SS
                ON              SS.ID = S.TYPE
                LEFT JOIN       UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH
                ON              PH.STORE_ID = S.STORE_ID
                INNER JOIN      GMV G
                ON              G.STORE_ID = S.STORE_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     UY_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
                WHERE           O.STATE NOT IN ('canceled_for_payment_error',
                                                'canceled_by_fraud',
                                                  'canceled_by_split_error',
                                                  'ongoing',
                                                  'on_the_route',
                                                  'in_store',
                                                  'in_progress',
                                                  'scheduled',
                                                  'created',
                                                  'arrive',
                                                  'canceled_by_timeout')
                AND             OS.TYPE NOT IN ('grin',
                                                'rappi_prime',
                                                'rentbrella',
                                                'go_taxi')
                AND             O.PAYMENT_METHOD != 'synthetic'
                AND             S.CITY_ADDRESS_ID != 104
                AND             O.CREATED_AT:: DATE >= CURRENT_DATE -30
                AND             GMV IS NOT NULL
                GROUP BY        1, 2, 3, 4, 5, 6, 7, 9, 10, 11
                HAVING          TOTAL >= 1
                ORDER BY        TOTAL DESC),

ULT AS (

       SELECT COUNTRY,
              STORE_ID,
              VERTICAL,
              NAME,
              STORE_TYPE,
              SCHEDULED,
              BRAND,
              TOTAL,
              PHYSICAL_STORE_ID,
              GMV,
              CANCEL_RATE,
              FINISHED_ORDERS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '5'
              ELSE '6' END AS DAYS,
              CASE WHEN VERTICAL ILIKE 'REST%' THEN '4'
              ELSE '6' END AS HOURS

       FROM   STORES)

       SELECT COUNTRY,
         STORE_ID,
         NAME,
         VERTICAL,
         STORE_TYPE,
         BRAND,
         DAYS,
         HOURS,
         CANCEL_RATE,
         FINISHED_ORDERS,
         TOTAL                                  AS TOTAL_ORDERS,
         GMV                                    AS GMV_TOTAL,
         ROUND(GMV / FINISHED_ORDERS, 3)        AS GMV_AOV_USD,
         TOTAL / ((DAYS * HOURS) * 4)           AS ORDERS_PER_HOUR,
         FINISHED_ORDERS / ((DAYS * HOURS) * 4) AS FINISHED_ORDERS_PER_HOUR
         FROM ULT
ORDER BY TOTAL_ORDERS DESC)

SELECT * FROM BR
UNION ALL
SELECT * FROM AR
UNION ALL
SELECT * FROM CL
UNION ALL
SELECT * FROM CO
UNION ALL
SELECT * FROM MX
UNION ALL
SELECT * FROM PE
UNION ALL
SELECT * FROM CR
UNION ALL
SELECT * FROM EC
UNION ALL
SELECT * FROM UY);
                    """

df = snow.run_query(query)