#!/bin/sh -e

REPO_ROOT_DIR="$(git rev-parse --show-toplevel)"

echo "Installing hooks..."
ln -svF ${REPO_ROOT_DIR}/development/tools/hooks/* ${REPO_ROOT_DIR}/.git/hooks/
