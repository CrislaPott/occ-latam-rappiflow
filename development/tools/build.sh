#!/bin/bash -e

IMAGE_TAG=${DOCKER_IMAGE_TAG:-'dev'}

# Variables for directory structure.
REPO_ROOT_DIR="$(git rev-parse --show-toplevel)"
REPO_DEVELOPMENT_DIR="${REPO_ROOT_DIR}/development"

# Generate Dockerfile for test.
cat "${REPO_ROOT_DIR}"/Dockerfile - > "${REPO_DEVELOPMENT_DIR}"/Dockerfile.test << EOF

# Install test python packages
COPY development/requirements_test.txt .
RUN pip install -q --no-cache-dir --user -r requirements_test.txt

# Add astro user bin to path to execute pip installed commands
ENV PATH="/home/astro/.local/bin:${PATH}"

# AIRFLOW_HOME must point to the test environment.
ENV AIRFLOW_HOME /usr/local/airflow/development/airflow-core

EOF

docker build -t rappiflow-astronomer:"${IMAGE_TAG}" -f ${REPO_DEVELOPMENT_DIR}/Dockerfile.test ${REPO_ROOT_DIR}
rm ${REPO_DEVELOPMENT_DIR}/Dockerfile.test