#!/bin/bash

# Variables for directory structure.
REPO_ROOT_DIR="$(git rev-parse --show-toplevel)"
RAPPIFLOW_PLUGINS_REPO_NAME="rappiflow-plugins"
VIRTUALENV_NAME="$(basename ${REPO_ROOT_DIR})"

# Get the rappiflow version from Dockerfile.
rappiflow_image_name="$(cat ${REPO_ROOT_DIR}/Dockerfile | grep ARG | head -1 | cut -d"=" -f2)/rappiflow"
rappiflow_version="$(cat ${REPO_ROOT_DIR}/Dockerfile | grep FROM | head -1 | cut -d":" -f2)"

# Clone rappiflow repo and build rappiflow image.
if [[ ! -d ${RAPPIFLOW_PLUGINS_REPO_NAME} ]]; then
    echo ">>> Cloning $RAPPIFLOW_PLUGINS_REPO_NAME repository"
    git clone --quiet --branch ${rappiflow_version} git@bitbucket.org:rappinc/${RAPPIFLOW_PLUGINS_REPO_NAME}.git 2> /dev/null
    [[ $? != 0 ]] && echo "An error ocurred. Check that the tag exists (${rappiflow_version}) and you have your SSH keys configured and enough permissions to read ${RAPPIFLOW_PLUGINS_REPO_NAME} repository." && exit 1
else
    echo ">>> Found ${RAPPIFLOW_PLUGINS_REPO_NAME} repository in ${REPO_ROOT_DIR}"
    cd ${RAPPIFLOW_PLUGINS_REPO_NAME} && git pull --quiet --force origin ${rappiflow_version} && cd ..
fi

if [[ -z $(docker images -q ${rappiflow_image_name}:${rappiflow_version} 2> /dev/null) ]]; then
    echo ">>> Building docker image ${rappiflow_image_name}:${rappiflow_version}"
    docker build -t ${rappiflow_image_name}:${rappiflow_version} -f ${RAPPIFLOW_PLUGINS_REPO_NAME}/Dockerfile ${RAPPIFLOW_PLUGINS_REPO_NAME}/.
else
    echo ">>> Found docker image ${rappiflow_image_name}:${rappiflow_version}"
fi

# Create virtual env.
if [[ "$1" != "--no-conda-env" ]]; then
    echo ">>> Creating conda env..."
    conda env create --force --quiet -n ${VIRTUALENV_NAME} -f ${RAPPIFLOW_PLUGINS_REPO_NAME}/environment.yml
    source activate ${VIRTUALENV_NAME}

    echo ">>> Installing rappiflow library"
    export PYCURL_SSL_LIBRARY=openssl
    export LDFLAGS=-L/usr/local/opt/openssl/lib
    export CPPFLAGS=-I/usr/local/opt/openssl/include
    pip install -q ${RAPPIFLOW_PLUGINS_REPO_NAME}/.

    echo ">>> Installing dependencies from requirements.txt"
    pip install -q -r ${REPO_ROOT_DIR}/requirements.txt

    echo ">>> Adding AIRFLOW_HOME env variable"
    cd "$CONDA_PREFIX"
    mkdir -p ./etc/conda/activate.d
    mkdir -p ./etc/conda/deactivate.d
    echo "#!/bin/sh" > ./etc/conda/activate.d/env_vars.sh
    echo "export AIRFLOW_HOME=$REPO_ROOT_DIR/development/airflow-core" >> ./etc/conda/activate.d/env_vars.sh
    echo "#!/bin/sh" > ./etc/conda/deactivate.d/env_vars.sh
    echo "unset AIRFLOW_HOME" >> ./etc/conda/deactivate.d/env_vars.sh
    cd -

    echo "Venv created, activate it by typing:"
    echo "conda activate ${VIRTUALENV_NAME}"
else
    echo ">>> Skipping creation of conda env"
fi
