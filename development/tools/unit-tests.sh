#!/bin/bash -e

IMAGE_TAG=${DOCKER_IMAGE_TAG:-'dev'}

echo "Running unit tests on the image rappiflow-astronomer:${IMAGE_TAG}"

REPO_ROOT_DIR="$(git rev-parse --show-toplevel)"

docker run --rm \
	-v "${REPO_ROOT_DIR}"/development/airflow-core/:/usr/local/airflow/development/airflow-core/ \
	rappiflow-astronomer:"${IMAGE_TAG}" airflow resetdb -y 2> /dev/null

docker run --rm \
	-v "${REPO_ROOT_DIR}"/development/airflow-core/:/usr/local/airflow/development/airflow-core/ \
	-v "${REPO_ROOT_DIR}"/dags/:/usr/local/airflow/dags/ \
	-v "${REPO_ROOT_DIR}"/tests/:/usr/local/airflow/tests/ \
	rappiflow-astronomer:"${IMAGE_TAG}" pytest /usr/local/airflow/tests
