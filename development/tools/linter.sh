#!/bin/bash -e

IMAGE_TAG=${DOCKER_IMAGE_TAG:-'dev'}

echo "Running linters on the image rappiflow-astronomer:${IMAGE_TAG}"

echo "Checking Flake8..."
docker run --rm rappiflow-astronomer:"${IMAGE_TAG}" flake8 /usr/local/airflow/dags /usr/local/airflow/tests

echo "Checking PyDocStyle..."
docker run --rm rappiflow-astronomer:"${IMAGE_TAG}" pydocstyle /usr/local/airflow/dags /usr/local/airflow/tests

echo "Checking MyPY..."
docker run --rm rappiflow-astronomer:"${IMAGE_TAG}" mypy /usr/local/airflow/dags
