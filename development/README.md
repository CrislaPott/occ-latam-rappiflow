## Development and Contribution

Everything included in this folder is only needed for local development and contribution to the repository. In the case of testing, using `astro dev start` is everything that is needed, and the content of this folder is not even used.

Things like code quality, unit testing are handled inside this folder.