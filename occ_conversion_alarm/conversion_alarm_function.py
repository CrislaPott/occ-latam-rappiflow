#!/usr/bin/env python
# coding: utf-8

# In[106]:


# get_ipython().system('jupyter nbconvert --to script conversion_alarm.ipynb')


# In[2]:

import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from timezones import country_timezones
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)
print(pd.__version__)


# In[3]:

def process_main(country):

    if country=='BR':
        country = 'BR'
        country_min = 'br'
        time_zones = 'Sao_Paulo'
        flag = '\U0001F1E7\U0001F1F7'
        
    elif country=='MX':
        country = 'MX'
        country_min = 'mx'
        time_zones = 'Mexico_City'
        flag = '\U0001F1F2\U0001F1FD'
        
    elif country=='CO':
        country = 'CO'
        country_min = 'co'
        time_zones = 'Bogota'
        flag = '\U0001F1E8\U0001F1F4'

    elif country=='AR':
        country = 'AR'
        country_min = 'ar'
        time_zones = 'Buenos_Aires'
        flag = '\U0001F1E6\U0001F1F7'

    elif country=='CL':
        country = 'CL'
        country_min = 'cl'
        time_zones = 'Santiago'
        flag = '\U0001F1E8\U0001F1F1'

    elif country=='PE':
        country = 'PE'
        country_min = 'pe'
        time_zones = 'Lima'
        flag = '\U0001F1F5\U0001F1EA'

    elif country=='UY':
        country = 'UY'
        country_min = 'uy'
        time_zones = 'Montevideo'
        flag = '\U0001F1FA\U0001F1FE'

    elif country=='EC':
        country = 'EC'
        country_min = 'ec'
        time_zones = 'Guayaquil'
        flag = '\U0001F1EA\U0001F1E8'

    elif country=='CR':
        country = 'CR'
        country_min = 'cr'
        time_zones = 'Costa_Rica'
        flag = '\U0001F1E8\U0001F1F7'


    # # General query

    # In[4]:


    df_country = run_query_s("""
    WITH FACT_CONVERSIONS AS
        (
            SELECT E.SNAPSHOT
                , HOUR(E.LAST_EVENT_TIMESTAMP_LOCAL) AS EVENT_HOUR
                , DATE_TRUNC(HOUR, E.LAST_EVENT_TIMESTAMP_LOCAL) AS EVENT_TIMESTAMP
                , E.COUNTRY
                , E.PLATFORM
                , DG.SK_CONVERSION_GEOGRAPHY
                , FS.SK_CONVERSIONS_SOURCES        /* Dim Funnel Sources */
                , DD.SK_CONVERSIONS_DEMOGRAPHIC    /* Dim Demography */
                    /*
                        These two can be -1 if the user never reached the SST step.
                        On Power BI, we will be handling those.
                    */
                , NVL(S.SK_CONVERSIONS_SUBVERTICAL, -1)    AS SK_CONVERSIONS_SUBVERTICAL
                , NVL(DS.SK_STORE_ID_DETAILS, -1)          AS SK_STORE_ID_DETAILS
                , DS.BRAND AS BRAND
                , COALESCE(DS.SUBVERTICAL, FS.SUBVERTICAL) AS SUBVERTICAL
                , DS._gmv_tier_by_country_and_store_type
                , COUNT(APL) AS APL
                , COUNT(SST) AS SST
                , COUNT(SS) AS SS
                , COUNT(ATC) AS ATC
                , COUNT(IFF(E.COUNT_TO_VSO, ATC,  NULL)) AS ATC_FOR_VSO
                , COUNT(VC) AS VC
                , COUNT(IFF(E.COUNT_TO_VSO, VC,  NULL))  AS VC_FOR_VSO
                , COUNT(IFF(E.COUNT_TO_VSO, VSO,  NULL)) AS VSO
                , COUNT(PTC) AS PTC
                , COUNT(IFF(E.COUNT_TO_VSO, PTC,  NULL)) AS PTC_FOR_VSO
                , COUNT(OP)  AS OP
                , COUNT(OPC) AS OPC
            FROM CPGS_CONVERSIONS.TBL_DAILY_EVENTS E
            LEFT JOIN CPGS_CONVERSIONS.VW_DIM_STORES DS
            ON DS.COUNTRY = E.COUNTRY
            AND DS.STORE_ID = E.STORE_ID
            LEFT JOIN CPGS_CONVERSIONS.VW_DIM_FUNNEL_SOURCES FS
            ON  FS.FUNNEL_SOURCE = E.FUNNEL_SOURCE
            AND FS.COUNTRY = E.COUNTRY
            LEFT JOIN CPGS_CONVERSIONS.VW_DIM_SUBVERTICALS S
            ON S.SUBVERTICAL = COALESCE(DS.SUBVERTICAL, FS.SUBVERTICAL)
            LEFT JOIN CPGS_CONVERSIONS.VW_DIM_GEOGRAPHY_MINIFIED DG
            ON  DG.COUNTRY          = E.COUNTRY
            AND DG.CITY            = NVL(NULLIF(TRIM(E.CITY), ''), 'N/A')
            LEFT JOIN CPGS_CONVERSIONS.VW_DIM_DEMOGRAPHICS_MINIFIED DD
            ON DD.IS_PRIME               = NVL(E.IS_PRIME, FALSE)
            AND DD.USER_TYPE             = UPPER(NVL(E.USER_TYPE, 'N/A'))
            WHERE 
        (SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '7 days' OR
        SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '8 days' OR
        SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '9 days' OR
        SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '0 days' OR
        SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '1 days' OR
        SNAPSHOT  = date(convert_timezone('America/{timezone}',current_timestamp())) - interval '2 days') AND 
            E.COUNTRY = '{country}' 
            GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13
        )
        , APP_LAUNCHES AS
        (
            SELECT 0 AS DATE_TYPE
                , COUNTRY
                , EVENT_TIMESTAMP
                , SUM(APL) AS APL
            FROM FACT_CONVERSIONS
            GROUP BY 1,2,3
        )
    SELECT 0 AS DATE_TYPE /* Daily */
            , F.SNAPSHOT AS SNAPSHOT
            , F.EVENT_TIMESTAMP
            , F.EVENT_HOUR
            , F.PLATFORM
            , F.COUNTRY
            , F.SK_CONVERSION_GEOGRAPHY
            , F.SK_CONVERSIONS_DEMOGRAPHIC
            , F.SK_CONVERSIONS_SOURCES
            , F.SK_CONVERSIONS_SUBVERTICAL
            , F.SK_STORE_ID_DETAILS
            , F.BRAND
            , F.SUBVERTICAL
            , F._gmv_tier_by_country_and_store_type
            , MAX(AP.APL)        AS APL
            , SUM(F.SST)         AS SST
            , SUM(F.SS)          AS SS
            , SUM(F.ATC)         AS ATC
            , SUM(F.ATC_FOR_VSO) AS ATC_FOR_VSO
            , SUM(F.VC)          AS VC
            , SUM(F.VC_FOR_VSO)  AS VC_FOR_VSO
            , SUM(F.VSO)         AS VSO
            , SUM(F.PTC)         AS PTC
            , SUM(F.PTC_FOR_VSO) AS PTC_FOR_VSO
            , SUM(F.OP)          AS OP
            , SUM(F.OPC)         AS OPC
        FROM FACT_CONVERSIONS F
        LEFT JOIN APP_LAUNCHES AP
        ON AP.EVENT_TIMESTAMP = F.EVENT_TIMESTAMP
        AND AP.COUNTRY = F.COUNTRY
        AND AP.DATE_TYPE = 0
        WHERE 1=1
        AND F.SST > 0
        AND F.SK_CONVERSIONS_SUBVERTICAL <> -1
        AND F.SUBVERTICAL IS NOT NULL
        AND F.SUBVERTICAL NOT IN ('OTROS', 'MEGA')
        GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14
    """.format(country = country, min_country = country_min, timezone=time_zones))
    print(df_country.shape)
    df_country.head()


    # # Config dates

    # In[57]:


    ## Get today date with hour by country
    la = pytz.timezone("America/{timezone}".format(timezone=time_zones))
    fmt = '%Y-%m-%d %H:%M:%S'
    now = datetime.now(la)
    now.strftime('%Y-%m-%d %H:%M:%S')


    # In[58]:


    ## Get 3 last hour 
    delta = 3
    last_hour = now - timedelta(hours = delta)
    print(last_hour.strftime('%H'))


    # In[59]:


    ## Get last 24 hour 
    last_24_hour = now - timedelta(hours = 24 + delta)
    print(last_24_hour.strftime('%H'))


    # In[60]:


    ## Get last week (d7)
    date_7 = now - timedelta(days = 7, hours=delta)
    print(date_7.strftime('%Y-%m-%d'))

    ## Get last week (d7) -24
    date_7_24 = now - timedelta(days = 7, hours = 24+delta)
    print(date_7_24.strftime('%Y-%m-%d'))


    # In[61]:


    ## Get today date 
    date_0 = now
    print(date_0.strftime('%Y-%m-%d'))

    ## Get -24 date 
    date_0_24 = now - timedelta(days = 1)
    print(date_0_24.strftime('%Y-%m-%d'))


    # In[62]:


    print('Filter will be: \n \
        day 0: Between %s at %s hours AND %s at %s hours \n \
        day 7: Between %s at %s hours AND %s at %s hours'%(
                                    last_hour.strftime('%Y-%m-%d'),
                                    last_hour.strftime('%H'),
                                    last_24_hour.strftime('%Y-%m-%d'),
                                    last_24_hour.strftime('%H'),
                                    
                                    date_7.strftime('%Y-%m-%d'),
                                    date_7.strftime('%H'),
                                    date_7_24.strftime('%Y-%m-%d'),
                                    date_7_24.strftime('%H'),
    ))


    # last_24_hour


    # # Subvertical - country

    # In[63]:


    ## Cantidad de registros por fecha
    df_country['snapshot'] =  pd.to_datetime(df_country['snapshot'], format='%Y-%m-%d')
    df_country['snapshot'].value_counts().sort_index()

    # In[64]:


    ## Cantidad de registros por hora
    df_country['event_hour'].value_counts().sort_index()


    # In[65]:


    ## Concateno snapshot - event_hoir y lo paso a tipo fecha
    df = df_country.copy()
    df['snapshot_hour'] = df['snapshot'].astype(str) + ' ' + df['event_hour'].astype(str) +':00'
    df['snapshot_hour'] =  pd.to_datetime(df['snapshot_hour'], format='%Y-%m-%d %H:%M')
    df.head()

    # In[66]:


    ## Reviso que tenga las 24 horas D7
    df[((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') ))]['snapshot_hour'].describe()

    # In[67]:


    ## Reviso que tenga las 24 horas D0
    df[((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') ))]['snapshot_hour'].describe()

    # In[68]:


    ## Filtro las 24 horas D0 y D7
    df = df[
        ((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') )) |
        
        ((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') ))
            ]
    print(df['snapshot'].value_counts(),
            df['snapshot_hour'].min(),
        df['snapshot_hour'].max())


    # In[69]:


    ## Genero una columna que identifique D0 y D7
    df.loc[
    ((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') )), 'Day'] = 0

    df.loc[
    ((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') )), 'Day'] = 7

    df['Day'].value_counts(dropna=False)

    # In[18]:


    # #Agrupo por vertical por fecha y hora y sumo OPC, SST
    for metric in ['opc','sst']:
        df['sum_{}'.format(metric)] = df.groupby(['subvertical','Day'])[metric].transform('sum')
        
    ##Agrupo y saco el max de APL
    df['max_apl'] = df.groupby(['subvertical','Day'])['apl'].transform('max')

    print(df.shape)
    df.head()


    # In[19]:


    ## Borro duplicados por vertical, fecha y hora
    cols = ['subvertical','Day','sum_opc','sum_sst','max_apl']
    df = df[cols].drop_duplicates().sort_values(['subvertical','Day'])
    df['Day'] = df['Day'].astype(str)


    # In[20]:

    
    ## pivot de la tabla para obtener las metricas
    df_pivot = df.pivot(index="subvertical", columns="Day", values=["sum_opc",'sum_sst','max_apl'])
    df_pivot.columns = ['_'.join(col).strip() for col in df_pivot.columns.values]
    df_pivot.fillna(0, inplace=True)
    df_pivot

    # In[21]:


    ## Se calcula para cada dia (d0-d7) las metricas main-funel y APL-SST
    days = [0.0, 7.0]
    for Day in days:
        print(Day)
        df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=Day )] = df_pivot['sum_opc_{fecha}'.format(fecha=Day )] / df_pivot['sum_sst_{fecha}'.format(fecha=Day)]
        df_pivot['APL_SST_{fecha}'.format(fecha=Day )] = df_pivot['sum_sst_{fecha}'.format(fecha=Day )] / df_pivot['max_apl_{fecha}'.format(fecha=Day)]

    df_pivot.fillna(0, inplace=True)
    df_pivot.head()

    # In[22]:



    df_pivot['MAIN_FUNEL_DIFF_PORC'] = (df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=days[0] )] - df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=days[1])])
    df_pivot['MAIN_FUNEL_DIFF_PORC'] = round(df_pivot['MAIN_FUNEL_DIFF_PORC'] / df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=days[1] )], 4) * 100

    df_pivot['APL_SST_DIFF_PORC'] = (df_pivot['APL_SST_{fecha}'.format(fecha=days[0] )] - df_pivot['APL_SST_{fecha}'.format(fecha=days[1] )])
    df_pivot['APL_SST_DIFF_PORC'] = round(df_pivot['APL_SST_DIFF_PORC'] / df_pivot['APL_SST_{fecha}'.format(fecha=days[1] )], 4) * 100

    df_pivot.loc[(df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=days[0] )]==0) & 
                (df_pivot['MAIN_FUNEL_{fecha}'.format(fecha=days[1] )]==0),'MAIN_FUNEL_DIFF_PORC'] = 0

    df_pivot.loc[(df_pivot['APL_SST_{fecha}'.format(fecha=days[0] )]==0) & 
                (df_pivot['APL_SST_{fecha}'.format(fecha=days[1] )]==0),'APL_SST_DIFF_PORC'] = 0

    df_pivot


    # In[23]:

    df_pivot['MAIN_FUNEL_DIFF_PORC'].value_counts(dropna=False).sort_index()


    # In[24]:

    df_pivot['APL_SST_DIFF_PORC'].value_counts(dropna=False).sort_index()


    # In[25]:

    print(
    df_pivot.shape,
    '\n Con Alarma: ',
    df_pivot[(df_pivot['MAIN_FUNEL_DIFF_PORC']<-5.) | (df_pivot['APL_SST_DIFF_PORC']<-4.)].shape,
    '\n Sin Alarma o indefinidos: ',
    df_pivot[~( (df_pivot['MAIN_FUNEL_DIFF_PORC']<-5) | (df_pivot['APL_SST_DIFF_PORC']<-4.) )].shape)

    # In[26]:


    df_pivot.reset_index(level=0, inplace=True)
    df_pivot['hora'] = last_hour.strftime('%H')
    df_pivot['MAIN_FUNEL_0.0']=df_pivot['MAIN_FUNEL_0.0']*100
    df_pivot['MAIN_FUNEL_7.0']=df_pivot['MAIN_FUNEL_7.0']*100
    df_pivot['APL_SST_0.0']=df_pivot['APL_SST_0.0']*100
    df_pivot['APL_SST_7.0']=df_pivot['APL_SST_7.0']*100
    df_pivot.head()


    # ## Users

    # In[27]:


    users = run_query_s("""select subvertical, subvert_leader, country_head, 
                team_central, team_central_2, team_central_3, team_central_4, team_central_5, team_central_6, team_central_7, team_central_8, team_central_9,
                team_central_10, team_central_11, team_central_12, team_central_13, team_central_14, team_central_15
                from google_sheets.conversion_subvertical
                where country='{country}'
            """.format(country = country))
    users.head()

    for user in ['subvert_leader', 'country_head', 'team_central', 'team_central_2', 'team_central_3', 'team_central_4', 'team_central_5', 'team_central_6', 'team_central_7',
                'team_central_8', 'team_central_9', 'team_central_10', 'team_central_11', 'team_central_12', 'team_central_13', 'team_central_14', 'team_central_15']:
        users[user] = users[user].astype(str).replace({"@rappi.com": ""}, regex=True)
        users[[user]] = users[[user]].replace(['None', '-'], np.nan)
        users['name_{}'.format(user)] = '<@' + users[user].map(str) + '>'

    users.head()



    # In[28]:


    print(df_pivot.shape)
    users_pivot= pd.merge(df_pivot, users,
                                    left_on=["subvertical"], right_on=["subvertical"], how='left')

    for user in ['subvert_leader', 'country_head', 'team_central', 'team_central_2', 'team_central_3', 'team_central_4', 'team_central_5', 'team_central_6', 'team_central_7',
                'team_central_8', 'team_central_9', 'team_central_10', 'team_central_11', 'team_central_12', 'team_central_13', 'team_central_14', 'team_central_15']:
        users_pivot[['name_{}'.format(user)]] = users_pivot[['name_{}'.format(user)]].fillna('--')
        users_pivot[['name_{}'.format(user)]] = users_pivot[['name_{}'.format(user)]].replace('<@nan>','--')
        
    print(users_pivot.shape)
    users_pivot.head()


    # # Brand - subvertical - country

    # In[29]:


    df_country = df_country.loc[df_country['_gmv_tier_by_country_and_store_type'] == '1-20']
    print(df_country.shape)


    # In[30]:


    df_country['snapshot'] =  pd.to_datetime(df_country['snapshot'], format='%Y-%m-%d')
    df_country['snapshot'].value_counts().sort_index()


    # In[31]:


    df_country['event_hour'].value_counts().sort_index()


    # In[32]:


    ## Concateno snapshot - event_hoir y lo paso a tipo fecha
    df = df_country.copy()
    df['snapshot_hour'] = df['snapshot'].astype(str) + ' ' + df['event_hour'].astype(str) +':00'
    df['snapshot_hour'] =  pd.to_datetime(df['snapshot_hour'], format='%Y-%m-%d %H:%M')
    df.head()


    # In[33]:


    ## Reviso que tenga las 24 horas D7
    df[((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') ))]['snapshot_hour'].describe()

    # In[34]:


    ## Reviso que tenga las 24 horas D0
    df[((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') ))]['snapshot_hour'].describe()

    # In[35]:


    ## Filtro las 24 horas D0 y D7
    df = df[
        ((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') )) |
        
        ((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') ))
            ]
    print(df['snapshot'].value_counts(),
            df['snapshot_hour'].min(),
        df['snapshot_hour'].max())


    # In[36]:


    ## Genero una columna que identifique D0 y D7
    df.loc[
    ((df['snapshot_hour']>=last_24_hour.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=last_hour.strftime('%Y-%m-%d %H:%M') )), 'Day'] = 0

    df.loc[
    ((df['snapshot_hour']>=date_7_24.strftime('%Y-%m-%d %H:%M') ) &
        (df['snapshot_hour']<=date_7.strftime('%Y-%m-%d %H:%M') )), 'Day'] = 7

    df['Day'].value_counts(dropna=False)

    # In[37]:


    #Agrupo por vertical por fecha y hora y sumo OPC, SST
    for metric in ['opc','sst']:
        df['sum_{}'.format(metric)] = df.groupby(['subvertical','brand','Day'])[metric].transform('sum')

    print(df.shape)
    df.head()


    # In[38]:


    cols = ['subvertical', 'brand','Day','sum_opc','sum_sst']
    df = df[cols].drop_duplicates().sort_values(['subvertical','brand','Day'])
    df['Day'] = df['Day'].astype(str)


    # In[39]:

    # df_pivot1 = df.pivot(index=['subvertical', 'brand'], columns='Day', values=["sum_opc",'sum_sst'])
    df_pivot1 = pd.pivot_table(df, index=['subvertical', 'brand'], columns=['Day'], values=["sum_opc",'sum_sst'])
    df_pivot1.columns = ['_'.join(col).strip() for col in df_pivot1.columns.values]
    df_pivot1.fillna(0, inplace=True)
    df_pivot1.head()


    ### Me quedo con sum_sst>150 D0
    print(df_pivot1.shape)
    df_pivot1= df_pivot1[(df_pivot1['sum_sst_0.0']>150) | (df_pivot1['sum_sst_7.0']>150)]
    print(df_pivot1.shape)


    # In[40]:


    ## Se calcula para cada dia (d0-d7) las metricas main-funel y APL-SST
    days = [0.0, 7.0]
    for Day in days:
        print(Day)
        df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=Day )] = df_pivot1['sum_opc_{fecha}'.format(fecha=Day )] / df_pivot1['sum_sst_{fecha}'.format(fecha=Day)]

    df_pivot1.fillna(0, inplace=True)
    df_pivot1.head()


    # In[41]:


    df_pivot1['MAIN_FUNEL_DIFF_PORC'] = (df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=days[0])] - df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=days[1])])
    df_pivot1['MAIN_FUNEL_DIFF_PORC'] = round(df_pivot1['MAIN_FUNEL_DIFF_PORC'] / df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=days[1])], 4) * 100

    df_pivot1.loc[(df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=days[0])]==0) & 
                (df_pivot1['MAIN_FUNEL_{fecha}'.format(fecha=days[1])]==0),'MAIN_FUNEL_DIFF_PORC'] = 0
    df_pivot1.head()



    # In[42]:


    df_pivot1['MAIN_FUNEL_DIFF_PORC'].value_counts(dropna=False).sort_index()

    # In[43]:


    print(
    df_pivot1.shape,
    '\n Con Alarma: ',
    df_pivot1[df_pivot1['MAIN_FUNEL_DIFF_PORC'] < -6.].shape,
    '\n Sin Alarma: ',
    df_pivot1[df_pivot1['MAIN_FUNEL_DIFF_PORC'] >= -6.].shape)

    # In[44]:


    df_pivot1.reset_index(level=['subvertical','brand'], inplace=True)
    df_pivot1['hora'] = last_hour.strftime('%H')
    df_pivot1['MAIN_FUNEL_0.0']=df_pivot1['MAIN_FUNEL_0.0']*100
    df_pivot1['MAIN_FUNEL_7.0']=df_pivot1['MAIN_FUNEL_7.0']*100
    df_pivot1.head()


    # ## Users

    # In[45]:


    users1 = run_query_s("""select brand, kam, kam_2, kam_3,
                subvert_leader, counrey_head, 
                team_central, team_central_2, team_central_3, team_central_4, team_central_5, team_central_6, team_central_7, team_central_8, team_central_9,
                team_central_10, team_central_11, team_central_12, team_central_13, team_central_14, team_central_15
                from google_sheets.conversion_brands
                where country='{country}'
            """.format(country = country))

    for user in ['kam', 'kam_2', 'kam_3', 'subvert_leader', 'counrey_head', 'team_central', 'team_central_2', 'team_central_3', 'team_central_4', 'team_central_5', 
                'team_central_6', 'team_central_7', 'team_central_8', 'team_central_9', 'team_central_10', 'team_central_11', 'team_central_12', 'team_central_13', 
                'team_central_14', 'team_central_15']:
        users1[user] = users1[user].astype(str).replace({"@rappi.com": ""}, regex=True)
        users1[[user]] = users1[[user]].replace(['None', '-'], np.nan)
        users1['name_{}'.format(user)] = '<@' + users1[user].map(str) + '>'


    users1.head()



    # In[46]:


    print(df_pivot1.shape)
    users_pivot1= pd.merge(df_pivot1, users1,
                                    left_on=["brand"], right_on=["brand"], how='left')

    for user in ['kam', 'kam_2', 'kam_3', 'subvert_leader', 'counrey_head', 'team_central', 'team_central_2', 'team_central_3', 'team_central_4', 'team_central_5', 
                'team_central_6', 'team_central_7', 'team_central_8', 'team_central_9', 'team_central_10', 'team_central_11', 'team_central_12', 'team_central_13',
                 'team_central_14', 'team_central_15']:
        users_pivot1[['name_{}'.format(user)]] = users_pivot1[['name_{}'.format(user)]].fillna('--')
        users_pivot1[['name_{}'.format(user)]] = users_pivot1[['name_{}'.format(user)]].replace('<@nan>','--')

    print(users_pivot1.shape)
    users_pivot1.head()

    return users_pivot, users_pivot1