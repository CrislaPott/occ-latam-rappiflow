#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# !jupyter nbconvert --to script Store_with_sales.ipynb


# # Libraries

# In[1]:


import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from timezones import country_timezones
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)

# In[67]:

def process_sales(country):


    if country=='BR':
        country = 'BR'
        country_min = 'br'
        time_zones = 'Sao_Paulo'
        on_list = True
        brands = (148, 748, 1594, 794, 353, 624, 569, 972, 161, 49, 1133, 355, 349, 680, 926, 277, 373, 782)
        view = ''
            
    elif country=='MX':
        country = 'MX'
        country_min = 'mx'
        time_zones = 'Mexico_City'
        on_list = True
        brands = (14, 225, 3, 83, 54, 150, 59, 513, 220, 580, 1641, 85, 244, 283, 490, 227, 216, 321, 520, 666)
        view = ''

    elif country=='CO':
        country = 'CO'
        country_min = 'co'
        time_zones = 'Bogota'
        on_list = True
        brands = (51, 1919, 1264, 1043, 1005, 760, 944, 527, 438, 717, 1443, 227, 188, 162, 86)
        view = '_VW'

    elif country=='AR':
        country = 'AR'
        country_min = 'ar'
        time_zones = 'Buenos_Aires'
        on_list = True
        brands = (46, 32, 78, 109, 154, 429, 182, 16, 54, 74, 111, 153, 65, 178, 72, 19, 64, 103, 577, 294)
        view = ''

    elif country=='CL':
        country = 'CL'
        country_min = 'cl'
        time_zones = 'Santiago'
        on_list = True
        brands = (116, 62, 21, 63, 545, 458, 175, 83, 64, 65, 20, 317, 66, 67, 323, 332, 58, 61)
        view = ''

    elif country=='PE':
        country = 'PE'
        country_min = 'pe'
        time_zones = 'Lima'
        on_list = True
        brands = (290, 111, 182, 204, 264, 78, 33, 168, 375, 34, 571, 194, 143, 50, 16, 363, 802, 66, 156, 325)
        view = ''

    elif country=='UY':
        country = 'UY'
        country_min = 'uy'
        time_zones = 'Montevideo'
        on_list = False
        brands = ''
        view = ''

    elif country=='EC':
        country = 'EC'
        country_min = 'ec'
        time_zones = 'Guayaquil'
        on_list = True
        brands = (248, 19, 27, 201, 149, 43, 55, 164, 45, 49, 53, 59, 308, 65, 124, 79, 82, 87, 90, 93)
        view = ''

    elif country=='CR':
        country = 'CR'
        country_min = 'cr'
        time_zones = 'Costa_Rica'
        on_list = False
        brands = ''
        view = ''


    # In[47]:


    la = pytz.timezone("America/{timezone}".format(timezone=time_zones))
    now = datetime.now(la)
    print(now.strftime('%Y-%m-%d %H:%M:%S'))

    delta = 3
    last_hour = (now - timedelta(hours = delta)).strftime('%H')
    print(last_hour)


    # # Subvertical - country

    # In[68]:


    df_sales = run_query_s("""
    WITH SALES_L24H AS
    (
    SELECT DISTINCT 
    S.TREATED_SUBVERTICAL,
    BRAND_GROUP_ID,
    BRAND_GROUP_NAME,
    COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H
    FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
    LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
    LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
    WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
    AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
    AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
    AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < CONVERT_TIMEZONE('America/{timezone}',DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
    GROUP BY 1,2,3
    ),

    SALES_L24H_LW AS
    (
    SELECT DISTINCT
    S.TREATED_SUBVERTICAL,
    BRAND_GROUP_ID,
    BRAND_GROUP_NAME,
    COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H_LW
    FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
    LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
    LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
    WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
    AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
    AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
    AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
    GROUP BY 1,2,3
    )

    SELECT  
    L.TREATED_SUBVERTICAL, 
    SUM(STORES_SALES_L24H) AS STORES_WITH_SALES_L24H, 
    SUM(STORES_SALES_L24H_LW) AS STORES_WITH_SALES_L24H_LW, 
    (STORES_WITH_SALES_L24H - STORES_WITH_SALES_L24H_LW)/STORES_WITH_SALES_L24H_LW AS delta_sales
    FROM SALES_L24H L 
    LEFT JOIN SALES_L24H_LW LW ON L.TREATED_SUBVERTICAL=LW.TREATED_SUBVERTICAL AND L.BRAND_GROUP_ID=LW.BRAND_GROUP_ID
    GROUP BY 1
    """.format(country = country, timezone=time_zones, view=view))

    print(df_sales.shape)
    df_sales.head()


    # In[43]:


    if (len(df_sales)>0):
        
        df_sales['delta_sales']=df_sales['delta_sales']*100
        df_sales['hora_sales'] = last_hour 

    print(df_sales.shape)
    df_sales.head()


    # In[44]:


    if (len(df_sales)>0):

        print(
        df_sales.shape,
        '\n Con Alarma: ',
        df_sales[(df_sales['delta_sales']<-1.50)].shape,
            
        '\n Sin Alarma o indefinidos: ',
        df_sales[~(df_sales['delta_sales']<-1.50)].shape)


    # In[45]:


    if (len(df_sales)>0):
        
        df_sales_final = df_sales[(df_sales['delta_sales']<-1.50)]
        
    else:
        
        df_sales_final = pd.DataFrame({'treated_subvertical': ['N/A'], 'stores_with_sales_l24h': ['N/A'], 
                                    'stores_with_sales_l24h_lw': ['N/A'], 'delta_sales': ['N/A'], 'hora_sales': ['N/A']})
        
    print(df_sales_final.shape)
    df_sales_final.head()


    # # Brand- subvertical - country

    # In[54]:


    if on_list ==True:
    
        df_sales1 = run_query_s("""
        WITH SALES_L24H AS
        (
        SELECT DISTINCT S.TREATED_SUBVERTICAL,
        BRAND_GROUP_ID,
        BRAND_GROUP_NAME,
        COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H
        FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
        LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
        LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
        WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
        AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < CONVERT_TIMEZONE('America/{timezone}',DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
        GROUP BY 1,2,3
        ),

        SALES_L24H_LW AS
        (
        SELECT DISTINCT S.TREATED_SUBVERTICAL,
        BRAND_GROUP_ID,
        BRAND_GROUP_NAME,
        COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H_LW
        FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
        LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
        LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
        WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
        AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
        GROUP BY 1,2,3
        )

        SELECT
        L.TREATED_SUBVERTICAL,
        L.BRAND_GROUP_ID,
        L.BRAND_GROUP_NAME,
        SUM(STORES_SALES_L24H) AS STORES_WITH_SALES_L24H,
        SUM(STORES_SALES_L24H_LW) AS STORES_WITH_SALES_L24H_LW,
        (STORES_WITH_SALES_L24H - STORES_WITH_SALES_L24H_LW)/STORES_WITH_SALES_L24H_LW AS DELTA_SALES
        FROM SALES_L24H L 
        LEFT JOIN SALES_L24H_LW LW ON L.TREATED_SUBVERTICAL=LW.TREATED_SUBVERTICAL AND L.BRAND_GROUP_ID=LW.BRAND_GROUP_ID
        WHERE L.BRAND_GROUP_ID IN {brands}
        GROUP BY 1,2,3
        """.format(country = country, timezone=time_zones, brands=brands, view=view))

    else:
        
        df_sales1 = run_query_s("""
        WITH SALES_L24H AS
        (
        SELECT DISTINCT S.TREATED_SUBVERTICAL,
        BRAND_GROUP_ID,
        BRAND_GROUP_NAME,
        COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H
        FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
        LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
        LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
        WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
        AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < CONVERT_TIMEZONE('America/{timezone}',DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP)))
        GROUP BY 1,2,3
        ),

        SALES_L24H_LW AS
        (
        SELECT DISTINCT S.TREATED_SUBVERTICAL,
        BRAND_GROUP_ID,
        BRAND_GROUP_NAME,
        COUNT(DISTINCT OS.STORE_ID) AS STORES_SALES_L24H_LW
        FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW VW
        LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES{view} OS ON OS.ORDER_ID = VW.ID
        LEFT JOIN CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK S ON OS.STORE_ID = S.STORE_ID AND S.COUNTRY_CODE = '{country}'
        WHERE S.TREATED_SUBVERTICAL IN ('SUPER', 'FARMA', 'EXPRESS', 'TURBO', 'PETS', 'LICORES', 'SPECIALIZED')
        AND VW.STATE IN ('finished', 'pending_review', 'arrive', 'in_progress')
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ >= DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -27, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
        AND (REPLACE(VW.CREATED_AT :: VARCHAR, '.000 Z') || ' -0300') :: TIMESTAMP_TZ < DATEADD(WEEK, -1, CONVERT_TIMEZONE('America/{timezone}', DATEADD(HOUR, -3, DATE_TRUNC(HOUR, CURRENT_TIMESTAMP))))
        GROUP BY 1,2,3
        )

        SELECT
        L.TREATED_SUBVERTICAL,
        L.BRAND_GROUP_ID,
        L.BRAND_GROUP_NAME,
        SUM(STORES_SALES_L24H) AS STORES_WITH_SALES_L24H,
        SUM(STORES_SALES_L24H_LW) AS STORES_WITH_SALES_L24H_LW,
        (STORES_WITH_SALES_L24H - STORES_WITH_SALES_L24H_LW)/STORES_WITH_SALES_L24H_LW AS DELTA_SALES
        FROM SALES_L24H L 
        LEFT JOIN SALES_L24H_LW LW ON L.TREATED_SUBVERTICAL=LW.TREATED_SUBVERTICAL AND L.BRAND_GROUP_ID=LW.BRAND_GROUP_ID
        GROUP BY 1,2,3
        """.format(country = country, timezone=time_zones, view=view))


    print(df_sales1.shape)
    df_sales1.head()


    # In[36]:


    if (len(df_sales1)>0):
        
        df_sales1['delta_sales']=df_sales1['delta_sales']*100
        df_sales1['hora_sales'] = last_hour 

    print(df_sales1.shape)
    df_sales1.head()


    # In[37]:


    if (len(df_sales1)>0):
        
        print(
        df_sales1.shape,
        '\n Con Alarma: ',
        df_sales1[(df_sales1['delta_sales']<-1.50)].shape,
            
        '\n Sin Alarma o indefinidos: ',
        df_sales1[~(df_sales1['delta_sales']<-1.50)].shape)


    # In[38]:


    if (len(df_sales1)>0):
        
        df_sales1_final = df_sales1[(df_sales1['delta_sales']<-1.50)]
        
    else:
        
        df_sales1_final = pd.DataFrame({'treated_subvertical': ['N/A'], 'brand_group_id': ['N/A'],
                                        'brand_group_name': ['N/A'], 'stores_with_sales_l24h': ['N/A'],
                                        'stores_with_sales_l24h_lw': ['N/A'], 'delta_sales': ['N/A'], 'hora_sales': ['N/A']})
        
    print(df_sales1_final.shape)
    df_sales1_final.head()

    return df_sales_final, df_sales1_final