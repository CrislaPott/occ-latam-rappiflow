#!/usr/bin/env python
# coding: utf-8

# In[25]:


# !jupyter nbconvert --to script Products_in_stock.ipynb


# # Libraries

# In[1]:


import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from timezones import country_timezones
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)


# In[2]:

def process_stock(country):


     if country=='CO':
          country = 'CO'
          interval=-5
          comment = ''
          comment1 = ''

     elif country=='BR':
          country = 'BR'
          interval=-3
          comment = ''
          comment1 = ''

     elif country=='MX':
          country = 'MX'
          interval=-5
          comment = ''
          comment1 = ''          

     elif country=='AR':
          country = 'AR'
          interval=-3
          comment = '/*'
          comment1 = '*/'

     elif country=='CL':
          country = 'CL'
          interval= -4
          comment = '/*'
          comment1 = '*/'

     elif country=='PE':
          country = 'PE'
          interval=-5
          comment = '/*'
          comment1 = '*/'

     elif country=='UY':
          country = 'UY'
          interval=-3
          comment = '/*'
          comment1 = '*/'

     elif country=='EC':
          country = 'EC'
          interval= -5
          comment = '/*'
          comment1 = '*/'

     elif country=='CR':
          country = 'CR'
          interval= -6
          comment = '/*'
          comment1 = '*/'


     # # Subvertical - country

     # In[21]:


     df_stock= run_query_s("""
     with Top20Brands as (
     SELECT COUNTRY
          , VERTICAL_GROUP
          , BRAND_GROUP
          , GMV_USD
          , GMV_USD_L28D
          , ORDERS
          , ORDERS_L28D
          , _AVG_DAILY_ORDERS_T3M
          , CASE
                    WHEN GMV_RANK_LATAM BETWEEN 0 AND 20 THEN '1-20'
                    WHEN GMV_RANK_LATAM BETWEEN 21 AND 50 THEN '21-50'
                    WHEN GMV_RANK_LATAM BETWEEN 51 AND 100 THEN '51-100'
                    WHEN GMV_RANK_LATAM > 100 THEN 'Over 100'
                    ELSE 'Outlier' END AS _GMV_TIER_LATAM
          , CASE
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 0 AND 20 THEN '1-20'
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 21 AND 50 THEN '21-50'
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 51 AND 100 THEN '51-100'
                    WHEN GMV_RANK_BY_COUNTRY > 100 THEN 'Over 100'
                    ELSE 'Outlier' END AS _GMV_TIER_BY_COUNTRY
          , IFF(PARETO_LATAM >= 0.8, 'Y', 'N')      AS _IS_PARETO_LATAM
          , PARETO_LATAM                            AS _PARETO_LATAM_VALUE
          , IFF(PARETO_BY_COUNTRY >= 0.8, 'Y', 'N') AS _IS_PARETO_BY_COUNTRY
          , PARETO_BY_COUNTRY                       AS _PARETO_BY_COUNTRY_VALUE

     FROM (
               SELECT COUNTRY
                    , VERTICAL_GROUP
                    , BRAND_GROUP
                    , GMV_USD
                    , GMV_USD_L28D
                    , ORDERS
                    , ORDERS_L28D
                    , ROUND(ORDERS / DAYS, 2) AS _AVG_DAILY_ORDERS_T3M
                    , RANK() OVER (PARTITION BY VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) DESC NULLS LAST) AS GMV_RANK_LATAM
                    , RANK() OVER (PARTITION BY COUNTRY, VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) DESC NULLS LAST) AS GMV_RANK_BY_COUNTRY
                    , PERCENT_RANK() OVER (PARTITION BY VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) ASC NULLS LAST) AS PARETO_LATAM
                    , PERCENT_RANK() OVER (PARTITION BY COUNTRY, VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) ASC NULLS LAST) AS PARETO_BY_COUNTRY
               FROM (
                         SELECT GO.COUNTRY
                              , LV.VERTICAL_GROUP
                              , CASE WHEN (UPPER(GO.BRAND_GROUP) IS NULL OR UPPER(GO.BRAND_GROUP) = '')
                              THEN UPPER(REPLACE(GO.STORE_TYPE, '_', ' '))
                              ELSE UPPER(GO.BRAND_GROUP) END AS BRAND_GROUP
                              , SUM(GO.GMV_USD) AS GMV_USD
                              , SUM(IFF(GO.DATE_ORDER_CREATED >= DATEADD(DAY, -28, CURRENT_DATE), GO.GMV_USD, 0)) AS GMV_USD_L28D
                              , COUNT(DISTINCT GO.ORDER_ID) AS ORDERS
                              , COUNT(DISTINCT IFF(GO.DATE_ORDER_CREATED >= DATEADD(DAY, -28, CURRENT_DATE), GO.ORDER_ID, NULL)) AS ORDERS_L28D
                              , DATEDIFF(DAY, DATE_TRUNC(MONTH, DATEADD(MONTH, -2, CURRENT_DATE)), CURRENT_DATE) AS DAYS
                         FROM GLOBAL_FINANCES.GLOBAL_ORDERS GO
                         LEFT JOIN CPGS_SALESCAPABILITY.VW_CORE_LATAM_VERTICALS LV
                         ON LV.COUNTRY = GO.COUNTRY AND LV.STORE_TYPE = GO.STORE_TYPE
                         WHERE GO.COUNT_TO_GMV
                         AND GO.VERTICAL_GROUP IN ('CPGS'/*, 'ECOMMERCE'*/)
                         AND GO.DATE_ORDER_CREATED >= DATE_TRUNC(MONTH, DATEADD(MONTH, -2, CURRENT_DATE))
                         GROUP BY 1, 2, 3)
          ) RAW

     ORDER BY VERTICAL_GROUP, COUNTRY, _PARETO_BY_COUNTRY_VALUE DESC),

     OriginTables as (
          select VERTICAL_SUB_GROUP, 
          DATE_RUN, 
          STOCK, 
          STORE_ID, 
          STORE_TYPE, 
          IS_ENABLED, 
          BRAND_GROUP_ID
          from PERSONALIZATION_DATASCIENCE.{country}_STORE_AVAILABILITY_HIST
          {comment}union
          select VERTICAL_SUB_GROUP, DATE_RUN, STOCK, VIRTUAL_STORE_ID as STORE_ID, STORE_TYPE,
               case when STORE_STATUS = 'published' then true else false end as IS_ENABLED, BRAND_GROUP_ID
          from PERSONALIZATION_DATASCIENCE.{country}_CP_VIRTUAL_STORE_AVAILABILITY_HIST{comment1}
     ),

     FixingBrandsNames as (
          select distinct VERTICAL_SUB_GROUP,
                         sl.BRAND_GROUP_NAME, 
                         DATE_RUN,
                         stock,
                         Co.STORE_ID,
                         sl.VERTICAL_GROUP
          from OriginTables as Co
               inner join CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK as sl on co.BRAND_GROUP_ID = sl.brand_group_id
          where COUNTRY_CODE = '{country}' and Co.IS_ENABLED=true and co.STORE_TYPE not ilike '%prueba%'
          and STORE_SUGGESTED_STATE not in ('Closed','Undefined')
     ),

     --por subvertical - pais 
     StockPerStore as (
          select T.COUNTRY,
               VERTICAL_SUB_GROUP,
               STORE_ID,
               BRAND_GROUP_NAME,
               dateadd(hour, {interval}, date_trunc(hour, dateadd(minute, 1, DATE_RUN))) as DATERUN,
               STOCK,
               lag(STOCK, 21) over (partition by STORE_ID order by DATE_RUN)     as STOCK_LW
          from FixingBrandsNames as FB
               inner join Top20Brands as T on T.COUNTRY = '{country}' and FB.BRAND_GROUP_NAME = T.BRAND_GROUP
          and T.VERTICAL_GROUP = FB.VERTICAL_GROUP
          where VERTICAL_SUB_GROUP in ('EXPRESS', 'SUPER', 'TURBO', 'PETS', 'SPECIALIZED', 'LIQUOR', 'PHARMACY')
          and store_id in (select distinct STORE_ID
     from CPGS_SALESCAPABILITY.TBL_MASTER_ORDERS
     where GMV > 0 and country = '{country}' and
     ANALYZED_DATE >= DATEADD(MONTH, -1, CURRENT_DATE))
          order by DATE_RUN desc, STORE_ID asc
     ), 

     pre_subvertical as (
     select COUNTRY,
     CASE VERTICAL_SUB_GROUP WHEN 'LIQUOR' THEN 'LICORES' 
                            WHEN 'PHARMACY' THEN 'FARMA'
                            WHEN 'OTHER' THEN 'OTROS'
                            WHEN 'HEALTH_BEAUTY' THEN 'HEALTH BEAUTY'
                            WHEN 'BOOKSTORES_STATIONARY' THEN 'BOOKSTORES STATIONARY'
                            WHEN 'BABIES_KIDS' THEN 'BABIES KIDS'
                            ELSE VERTICAL_SUB_GROUP END AS VERTICAL_SUB_GROUP,
     DATERUN,
     avg(STOCK) as AVG_STOCK,
     avg(STOCK_LW) as AVG_STOCK_LW,
     case when div0(avg(STOCK), avg(STOCK_LW)) - 1 = -1 then 0 else (avg(STOCK) / avg(STOCK_LW)) - 1 end as delta_stock,
     row_number() over (partition by /*BRAND_GROUP_NAME,*/ VERTICAL_SUB_GROUP order by DATERUN desc) as Orden
     from StockPerStore
     group by COUNTRY, VERTICAL_SUB_GROUP, /*BRAND_GROUP_NAME,*/ DATERUN
     order by DATERUN desc)

     select * from pre_subvertical
     where Orden = 1
     AND delta_stock < -0.015 and round(AVG_STOCK, 0) - round(AVG_STOCK_LW, 0) < -30 and DATERUN >= current_date
     """.format(country = country, interval=interval, comment= comment, comment1= comment1))

     print(df_stock.shape)
     df_stock.head()


     # In[22]:


     if (len(df_stock)>0):
     
          df_stock['delta_stock']=df_stock['delta_stock']*100
          df_stock['hora_stock'] = df_stock['daterun'].dt.strftime('%H')

     print(df_stock.shape)
     df_stock.head()


     # In[23]:


     if (len(df_stock)>0):

          print(
          df_stock.shape,
          '\n Con Alarma: ',
          df_stock[(df_stock['delta_stock']<-1.50)].shape,
          '\n Sin Alarma o indefinidos: ',
          df_stock[~(df_stock['delta_stock']<-1.50)].shape)


     # In[24]:

     if (len(df_stock)>0):
     
          df_stock_final = df_stock[(df_stock['delta_stock']<-1.50)]
     
     else:
          
          df_stock_final = pd.DataFrame({'country': ['{}'.format(country)], 'vertical_sub_group': ['N/A'], 'daterun':['N/A'],
                                   'avg_stock': ['N/A'], 'avg_stock_lw': ['N/A'], 'delta_stock': ['N/A'], 'orden': ['N/A']})
    
     print(df_stock_final.shape)
     df_stock_final.head()


     # # Brand - subvertical- country

     # In[26]:


     df_stock1 = run_query_s("""
     with Top20Brands as (
     SELECT COUNTRY
          , VERTICAL_GROUP
          , BRAND_GROUP
          , GMV_USD
          , GMV_USD_L28D
          , ORDERS
          , ORDERS_L28D
          , _AVG_DAILY_ORDERS_T3M
          , CASE
                    WHEN GMV_RANK_LATAM BETWEEN 0 AND 20 THEN '1-20'
                    WHEN GMV_RANK_LATAM BETWEEN 21 AND 50 THEN '21-50'
                    WHEN GMV_RANK_LATAM BETWEEN 51 AND 100 THEN '51-100'
                    WHEN GMV_RANK_LATAM > 100 THEN 'Over 100'
                    ELSE 'Outlier' END AS _GMV_TIER_LATAM
          , CASE
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 0 AND 20 THEN '1-20'
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 21 AND 50 THEN '21-50'
                    WHEN GMV_RANK_BY_COUNTRY BETWEEN 51 AND 100 THEN '51-100'
                    WHEN GMV_RANK_BY_COUNTRY > 100 THEN 'Over 100'
                    ELSE 'Outlier' END AS _GMV_TIER_BY_COUNTRY
          , IFF(PARETO_LATAM >= 0.8, 'Y', 'N')      AS _IS_PARETO_LATAM
          , PARETO_LATAM                            AS _PARETO_LATAM_VALUE
          , IFF(PARETO_BY_COUNTRY >= 0.8, 'Y', 'N') AS _IS_PARETO_BY_COUNTRY
          , PARETO_BY_COUNTRY                       AS _PARETO_BY_COUNTRY_VALUE

     FROM (
               SELECT COUNTRY
                    , VERTICAL_GROUP
                    , BRAND_GROUP
                    , GMV_USD
                    , GMV_USD_L28D
                    , ORDERS
                    , ORDERS_L28D
                    , ROUND(ORDERS / DAYS, 2) AS _AVG_DAILY_ORDERS_T3M
                    , RANK() OVER (PARTITION BY VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) DESC NULLS LAST) AS GMV_RANK_LATAM
                    , RANK() OVER (PARTITION BY COUNTRY, VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) DESC NULLS LAST) AS GMV_RANK_BY_COUNTRY
                    , PERCENT_RANK() OVER (PARTITION BY VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) ASC NULLS LAST) AS PARETO_LATAM
                    , PERCENT_RANK() OVER (PARTITION BY COUNTRY, VERTICAL_GROUP ORDER BY NVL(GMV_USD, 0) ASC NULLS LAST) AS PARETO_BY_COUNTRY
               FROM (
                         SELECT GO.COUNTRY
                              , LV.VERTICAL_GROUP
                              , CASE WHEN (UPPER(GO.BRAND_GROUP) IS NULL OR UPPER(GO.BRAND_GROUP) = '')
                              THEN UPPER(REPLACE(GO.STORE_TYPE, '_', ' '))
                              ELSE UPPER(GO.BRAND_GROUP) END AS BRAND_GROUP
                              , SUM(GO.GMV_USD) AS GMV_USD
                              , SUM(IFF(GO.DATE_ORDER_CREATED >= DATEADD(DAY, -28, CURRENT_DATE), GO.GMV_USD, 0)) AS GMV_USD_L28D
                              , COUNT(DISTINCT GO.ORDER_ID) AS ORDERS
                              , COUNT(DISTINCT IFF(GO.DATE_ORDER_CREATED >= DATEADD(DAY, -28, CURRENT_DATE), GO.ORDER_ID, NULL)) AS ORDERS_L28D
                              , DATEDIFF(DAY, DATE_TRUNC(MONTH, DATEADD(MONTH, -2, CURRENT_DATE)), CURRENT_DATE) AS DAYS
                         FROM GLOBAL_FINANCES.GLOBAL_ORDERS GO
                         LEFT JOIN CPGS_SALESCAPABILITY.VW_CORE_LATAM_VERTICALS LV
                         ON LV.COUNTRY = GO.COUNTRY AND LV.STORE_TYPE = GO.STORE_TYPE
                         WHERE GO.COUNT_TO_GMV
                         AND GO.VERTICAL_GROUP IN ('CPGS'/*, 'ECOMMERCE'*/)
                         AND GO.DATE_ORDER_CREATED >= DATE_TRUNC(MONTH, DATEADD(MONTH, -2, CURRENT_DATE))
                         GROUP BY 1, 2, 3)
          ) RAW

     ORDER BY VERTICAL_GROUP, COUNTRY, _PARETO_BY_COUNTRY_VALUE DESC),

     OriginTables as (
          select VERTICAL_SUB_GROUP, 
          DATE_RUN, 
          STOCK, 
          STORE_ID, 
          STORE_TYPE, 
          IS_ENABLED, 
          BRAND_GROUP_ID
          from PERSONALIZATION_DATASCIENCE.{country}_STORE_AVAILABILITY_HIST
          {comment}union
          select VERTICAL_SUB_GROUP, DATE_RUN, STOCK, VIRTUAL_STORE_ID as STORE_ID, STORE_TYPE,
               case when STORE_STATUS = 'published' then true else false end as IS_ENABLED, BRAND_GROUP_ID
          from PERSONALIZATION_DATASCIENCE.{country}_CP_VIRTUAL_STORE_AVAILABILITY_HIST{comment1}
     ),

     FixingBrandsNames as (
          select distinct VERTICAL_SUB_GROUP,
                         sl.BRAND_GROUP_NAME, 
                         DATE_RUN,
                         stock,
                         Co.STORE_ID,
                         sl.VERTICAL_GROUP
          from OriginTables as Co
               inner join CPGS_SALESCAPABILITY.TBL_CORE_STORES_LINK as sl on co.BRAND_GROUP_ID = sl.brand_group_id
          where COUNTRY_CODE = '{country}' and Co.IS_ENABLED=true and co.STORE_TYPE not ilike '%prueba%'
          and STORE_SUGGESTED_STATE not in ('Closed','Undefined')
     ),

     --por brand - subvertical 
     StockPerStoreBrands as (
          select T.COUNTRY,
               VERTICAL_SUB_GROUP,
               STORE_ID,
               BRAND_GROUP_NAME,
               dateadd(hour, {interval}, date_trunc(hour, dateadd(minute, 1, DATE_RUN))) as DATERUN,
               STOCK,
               lag(STOCK, 21) over (partition by STORE_ID order by DATE_RUN)     as STOCK_LW
          from FixingBrandsNames as FB
          inner join Top20Brands as T on T.COUNTRY = '{country}' and FB.BRAND_GROUP_NAME = T.BRAND_GROUP
          and T.VERTICAL_GROUP = FB.VERTICAL_GROUP
          where _GMV_TIER_BY_COUNTRY = '1-20'
          and VERTICAL_SUB_GROUP in ('EXPRESS', 'SUPER', 'TURBO', 'PETS', 'SPECIALIZED', 'LIQUOR', 'PHARMACY')
          and store_id in 
          (select distinct STORE_ID
     from CPGS_SALESCAPABILITY.TBL_MASTER_ORDERS
     where GMV > 0 and country = '{country}' and
     ANALYZED_DATE >= DATEADD(MONTH, -1, CURRENT_DATE))
          order by DATE_RUN desc, STORE_ID asc
     ),

     pre_brand as (
     select COUNTRY,
     CASE VERTICAL_SUB_GROUP WHEN 'LIQUOR' THEN 'LICORES' 
                            WHEN 'PHARMACY' THEN 'FARMA'
                            WHEN 'OTHER' THEN 'OTROS'
                            WHEN 'HEALTH_BEAUTY' THEN 'HEALTH BEAUTY'
                            WHEN 'BOOKSTORES_STATIONARY' THEN 'BOOKSTORES STATIONARY'
                            WHEN 'BABIES_KIDS' THEN 'BABIES KIDS'
                            ELSE VERTICAL_SUB_GROUP END AS VERTICAL_SUB_GROUP,
     BRAND_GROUP_NAME,
     DATERUN,
     avg(STOCK) as AVG_STOCK,
     avg(STOCK_LW) as AVG_STOCK_LW,
     case when div0(avg(STOCK), avg(STOCK_LW)) - 1 = -1 then 0 else (avg(STOCK) / avg(STOCK_LW)) - 1 end as delta_stock,
     row_number() over (partition by BRAND_GROUP_NAME, VERTICAL_SUB_GROUP order by DATERUN desc) as Orden
     from StockPerStoreBrands
     group by COUNTRY, VERTICAL_SUB_GROUP, BRAND_GROUP_NAME, DATERUN
     order by DATERUN desc)

     select * from pre_brand
     where Orden = 1
     AND delta_stock < -0.015 and round(AVG_STOCK, 0) - round(AVG_STOCK_LW, 0) < -30 and DATERUN >= current_date

     """.format(country = country, interval=interval, comment= comment, comment1= comment1))
     
     print(df_stock1.shape)
     df_stock1.head()


     # In[28]:


     if (len(df_stock1)>0):
          
          df_stock1['delta_stock']=df_stock1['delta_stock']*100
          df_stock1['hora_stock'] = df_stock1['daterun'].dt.strftime('%H')
     
     print(df_stock1.shape)
     df_stock1.head()


     # In[ ]:


     if (len(df_stock1)>0):
     
          print(
          df_stock1.shape,
          '\n Con Alarma: ',
          df_stock1[(df_stock1['delta_stock']<-1.50)].shape,
          '\n Sin Alarma o indefinidos: ',
          df_stock1[~(df_stock1['delta_stock']<-1.50)].shape)


     # In[29]:


     if (len(df_stock1)>0):
     
          df_stock1_final = df_stock1[(df_stock1['delta_stock']<-1.50)]
          
     else:
    
          df_stock1_final = pd.DataFrame({'country': ['{}'.format(country)], 'vertical_sub_group': ['N/A'],
                                   'brand_group_name': ['N/A'], 'daterun':['N/A'],
                                   'avg_stock': ['N/A'], 'avg_stock_lw': ['N/A'], 'delta_stock': ['N/A'], 'orden': ['N/A']})    
     
     print(df_stock1_final.shape)
     df_stock1_final.head()

     return df_stock_final, df_stock1_final