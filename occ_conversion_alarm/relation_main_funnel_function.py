#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# !jupyter nbconvert --to script relation_main_funnel.ipynb


# In[1]:


import pandas as pd 
import datetime as dt
import numpy as np
import pytz
from snowflak import upload_df_occ, inserquery, run_query as run_query_s
from redash import run_query as run_query_r
from cms import turn_store_on
from slac import bot_slack
from timezones import country_timezones
from datetime import datetime, timedelta
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 100)


# In[2]:

def process_relation(country, canal):

    if country=='BR':
        country = 'BR'
        time_zones = 'Sao_Paulo'
        flag = '\U0001F1E7\U0001F1F7'

    elif country=='MX':
        country = 'MX'
        time_zones = 'Mexico_City'
        flag = '\U0001F1F2\U0001F1FD'

    elif country=='CO':
        country = 'CO'
        time_zones = 'Bogota'
        flag = '\U0001F1E8\U0001F1F4'

    elif country=='AR':
        country = 'AR'
        time_zones = 'Buenos_Aires'
        flag = '\U0001F1E6\U0001F1F7'

    elif country=='CL':
        country = 'CL'
        time_zones = 'Santiago'
        flag = '\U0001F1E8\U0001F1F1'

    elif country=='PE':
        country = 'PE'
        time_zones = 'Lima'
        flag = '\U0001F1F5\U0001F1EA'

    elif country=='UY':
        country = 'UY'
        time_zones = 'Montevideo'
        flag = '\U0001F1FA\U0001F1FE'

    elif country=='EC':
        country = 'EC'
        time_zones = 'Guayaquil'
        flag = '\U0001F1EA\U0001F1E8'

    elif country=='CR':
        country = 'CR'
        time_zones = 'Costa_Rica'
        flag = '\U0001F1E8\U0001F1F7'


    # # Main funnel function

    # In[ ]:

    from conversion_alarm_function import process_main
    users_pivot, users_pivot1 = process_main(country=country)

    # In[4]:

    print(users_pivot.shape)
    users_pivot.head()


    # In[5]:

    print(users_pivot1.shape)
    users_pivot1.head()


    # # Products in stock function

    # In[ ]:

    from Products_in_stock_function import process_stock
    df_stock_final, df_stock1_final = process_stock(country=country)

    # In[7]:

    print(df_stock_final.shape)
    df_stock_final.head()


    # In[8]:


    print(df_stock1_final.shape)
    df_stock1_final.head()


    # # Store with sales function

    # In[ ]:

    from Store_with_sales_function import process_sales
    df_sales_final, df_sales1_final = process_sales(country=country)

    # In[7]:

    print(df_sales_final.shape)
    df_sales_final.head()


    # In[8]:

    print(df_sales1_final.shape)
    df_sales1_final.head()



    # # Relation main funnel- products in stock

    # ## Subvertical - country

    # In[9]:


    stock_funnel = pd.merge(users_pivot, df_stock_final,
                                    left_on=["subvertical"], right_on=["vertical_sub_group"], how='left', indicator=True)

    d={"left_only":"False", "both":"True"}
    stock_funnel['_merge'] = stock_funnel['_merge'].map(d)

    stock_funnel.rename(columns  = {'_merge':'Alarm_Stock'}, inplace=True)

    print(stock_funnel.shape)
    stock_funnel.head()


    # ## Brand - subvertical

    # In[10]:


    stock1_funnel = pd.merge(users_pivot1, df_stock1_final,
                                    left_on=["subvertical", "brand"], right_on=["vertical_sub_group", "brand_group_name"], how='left', indicator=True)

    d={"left_only":"False", "both":"True"}
    stock1_funnel['_merge'] = stock1_funnel['_merge'].map(d)

    stock1_funnel.rename(columns  = {'_merge':'Alarm_Stock'}, inplace=True)

    print(stock1_funnel.shape)
    stock1_funnel.head()


    # # Relation main funnel- store with sales

    # ## Subvertical - country

    # In[13]:

    sales_funnel = pd.merge(stock_funnel, df_sales_final,
                                left_on=["subvertical"], right_on=["treated_subvertical"], how='left', indicator=True)

    d={"left_only":"False", "both":"True"}
    sales_funnel['_merge'] = sales_funnel['_merge'].map(d)

    sales_funnel.rename(columns  = {'_merge':'Alarm_Sales'}, inplace=True)

    print(sales_funnel.shape)
    sales_funnel.head()


    # ## Brand - subvertical

    # In[10]:

    sales1_funnel = pd.merge(stock1_funnel, df_sales1_final,
                                left_on=["subvertical", "brand"], right_on=["treated_subvertical", "brand_group_name"], how='left', indicator=True)

    d={"left_only":"False", "both":"True"}
    sales1_funnel['_merge'] = sales1_funnel['_merge'].map(d)

    sales1_funnel.rename(columns  = {'_merge':'Alarm_Sales'}, inplace=True)

    print(sales1_funnel.shape)
    sales1_funnel.head()

    # # Mensaje 

    # In[12]:


    canal_slack = canal

    for index, row in sales1_funnel.iterrows():

        if row['MAIN_FUNEL_DIFF_PORC'] < -35.:

            main_text = '''      

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}%*
            for Group Brand _{group_brand}_ of the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            Kam={kam}
            Kam_2={kam2}
            kam_3={kam3}
            country_head={country_head}
            subvert_leader={subvert_leader}

            {team_central}, {team_central2}, {team_central3}, {team_central4}, {team_central5}, {team_central6}, {team_central7}, {team_central8}, {team_central9}, {team_central10}, 
            {team_central11}, {team_central12}, {team_central13}, {team_central14}, {team_central15}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                group_brand = row['brand'],
                vertical = row['subvertical'],
                main_funnel_d0 = row[6],
                main_funnel_d7 = row[7],
                flag=flag,
                country=country,
                hora=row['hora'],
                team_central=row['name_team_central'],
                team_central2=row['name_team_central_2'],
                team_central3=row['name_team_central_3'],
                team_central4=row['name_team_central_4'],
                team_central5=row['name_team_central_5'],
                team_central6=row['name_team_central_6'],
                team_central7=row['name_team_central_7'],
                team_central8=row['name_team_central_8'],
                team_central9=row['name_team_central_9'],
                team_central10=row['name_team_central_10'],
                team_central11=row['name_team_central_11'],
                team_central12=row['name_team_central_12'],
                team_central13=row['name_team_central_13'],
                team_central14=row['name_team_central_14'],
                team_central15=row['name_team_central_15'],
                country_head=row['name_counrey_head'],
                subvert_leader=row['name_subvert_leader'],
                kam=row['name_kam'],
                kam2=row['name_kam_2'],
                kam3=row['name_kam_3']
                )

            print(main_text)
            bot_slack(main_text, canal_slack) 


            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 

            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
                
        elif row['MAIN_FUNEL_DIFF_PORC'] < -25.:
                
            main_text = '''      

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}%*
            for Group Brand _{group_brand}_ of the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            Kam={kam}
            Kam_2={kam2}
            kam_3={kam3}
            country_head={country_head}
            subvert_leader={subvert_leader}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                group_brand = row['brand'],
                vertical = row['subvertical'],
                main_funnel_d0 = row[6],
                main_funnel_d7 = row[7],
                flag=flag,
                country=country,
                hora=row['hora'],
                country_head=row['name_counrey_head'],
                subvert_leader=row['name_subvert_leader'],
                kam=row['name_kam'],
                kam2=row['name_kam_2'],
                kam3=row['name_kam_3']
                )

            print(main_text)
            bot_slack(main_text, canal_slack) 
            
                    
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


                
        elif row['MAIN_FUNEL_DIFF_PORC'] < -18.:
            
            main_text = '''      

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}%*
            for Group Brand _{group_brand}_ of the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            Kam={kam}
            Kam_2={kam2}
            kam_3={kam3}
            subvert_leader={subvert_leader}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                group_brand = row['brand'],
                vertical = row['subvertical'],
                main_funnel_d0 = row[6],
                main_funnel_d7 = row[7],
                flag=flag,
                country=country,
                hora=row['hora'],
                subvert_leader=row['name_subvert_leader'],
                kam=row['name_kam'],
                kam2=row['name_kam_2'],
                kam3=row['name_kam_3']
                )

            print(main_text)
            bot_slack(main_text, canal_slack) 
            
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
                
                
        elif row['MAIN_FUNEL_DIFF_PORC'] < -9.:

            main_text = '''      

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}%*
            for Group Brand _{group_brand}_ of the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            Kam={kam}
            Kam_2={kam2}
            kam_3={kam3}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                group_brand = row['brand'],
                vertical = row['subvertical'],
                main_funnel_d0 = row[6],
                main_funnel_d7 = row[7],
                flag=flag,
                country=country,
                hora=row['hora'],
                kam=row['name_kam'],
                kam2=row['name_kam_2'],
                kam3=row['name_kam_3']
                )

            print(main_text)
            bot_slack(main_text, canal_slack)
            
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
                
    ######################################################################################

    for index, row in sales_funnel.iterrows(): 

        if (row['MAIN_FUNEL_DIFF_PORC']<-24.):

            main_text = '''

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}% *
            for the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            country_head={country_head}
            subvert_leader={subvert_leader}

            {team_central}, {team_central2}, {team_central3}, {team_central4}, {team_central5}, {team_central6}, {team_central7}, {team_central8}, {team_central9}, {team_central10}, 
            {team_central11}, {team_central12}, {team_central13}, {team_central14}, {team_central15}


            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                vertical=row['subvertical'],
                main_funnel_d0 = row[7],
                main_funnel_d7 = row[9],
                flag=flag,
                country=country,
                hora=row['hora'],
                team_central=row['name_team_central'],
                team_central2=row['name_team_central_2'],
                team_central3=row['name_team_central_3'],
                team_central4=row['name_team_central_4'],
                team_central5=row['name_team_central_5'],
                team_central6=row['name_team_central_6'],
                team_central7=row['name_team_central_7'],
                team_central8=row['name_team_central_8'],
                team_central9=row['name_team_central_9'],
                team_central10=row['name_team_central_10'],
                team_central11=row['name_team_central_11'],
                team_central12=row['name_team_central_12'],
                team_central13=row['name_team_central_13'],
                team_central14=row['name_team_central_14'],
                team_central15=row['name_team_central_15'],
                country_head=row['name_country_head'],
                subvert_leader=row['name_subvert_leader']
                )

            print(main_text)
            bot_slack(main_text, canal_slack) 
            
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 

            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
                
        elif (row['MAIN_FUNEL_DIFF_PORC']<-16.):
                
            main_text = '''

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}% *
            for the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            country_head={country_head}
            subvert_leader={subvert_leader}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                vertical=row['subvertical'],
                main_funnel_d0 = row[7],
                main_funnel_d7 = row[9],
                flag=flag,
                country=country,
                hora=row['hora'],
                country_head=row['name_country_head'],
                subvert_leader=row['name_subvert_leader']
                )

            print(main_text)
            bot_slack(main_text, canal_slack) 
            
            
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
            
        elif (row['MAIN_FUNEL_DIFF_PORC']<-8.):
                
            main_text = '''

            \U0001F4E2 *Main funnel conversion* in the last 24 hs (queried {hora}hs {country}) dropped *{main_funnel:.1f}% *
            for the subvertical _{vertical}_ from {flag}.

            Main funnel conversion today (last 24hs of {hora}hs {country}): *{main_funnel_d0:.1f}%*
            Main funnel conversion one week ago (same period): *{main_funnel_d7:.1f}%*

            subvert_leader={subvert_leader}

            '''.format(
                main_funnel = row['MAIN_FUNEL_DIFF_PORC'],
                vertical=row['subvertical'],
                main_funnel_d0 = row[7],
                main_funnel_d7 = row[9],
                flag=flag,
                country=country,
                hora=row['hora'],
                subvert_leader=row['name_subvert_leader']
                )

            print(main_text)
            bot_slack(main_text, canal_slack)
            
            if row['Alarm_Stock'] == 'True':

                main_text = '''

                .       • *Products in Stock* dropped *{delta:.1f}%* vs Same Day Last Week,
                passing in average from *{avg_stock_lw:.0f}* to *{avg_stock:.0f}* \U0001F4E6
                '''.format(
                    delta = row['delta_stock'],
                    avg_stock_lw = row['avg_stock_lw'],
                    avg_stock = row['avg_stock']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 


            if row['Alarm_Sales'] == 'True':

                main_text = '''

                .       • *Stores With Sales* dropped *{delta:.1f}%* vs Same Day Last Week, 
                with #STORES WITH SALES L24H = *{stores_with_sales_l24h:.0f}* and #STORES WITH SALES L24H LW = *{stores_with_sales_l24h_lw:.0f}*. 
                '''.format(
                    delta = row['delta_sales'],
                    stores_with_sales_l24h = row['stores_with_sales_l24h'],
                    stores_with_sales_l24h_lw = row['stores_with_sales_l24h_lw']
                    )

                print(main_text)
                bot_slack(main_text, canal_slack) 
