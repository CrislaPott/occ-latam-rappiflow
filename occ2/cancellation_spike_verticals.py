import pandas as pd
from outliers import smirnov_grubbs as grubbs
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones
import json 



def read_parameters():
    filepath = '/usr/local/airflow/occ2/parameters/vertical_cancellation_spike.json'
    with open(filepath, 'r') as f:
        parameters = json.load(f)
    countries = pd.DataFrame.from_dict(parameters['enabled']['countries_enabled'], orient="index")
    verticals = pd.DataFrame.from_dict(parameters['enabled']['verticals_enabled'], orient="index")
    countries_size = pd.DataFrame.from_dict(parameters['country_classification'], orient="index")
    size_timeframe = pd.DataFrame.from_dict(parameters['thresholds']['timeframe_size'], orient="index")
    vertical_min_cancels = pd.DataFrame.from_dict(parameters['thresholds']['vertical_min_cancels'], orient="index")
    countries_timeframe = countries_size.merge(size_timeframe, on='Size')
    countries = countries[countries['Status'] == 'ON']['Country'].to_list()
    countries = [x.lower() for x in countries]
    verticals = verticals[verticals['Status'] == 'ON']['Vertical'].to_list()
    max_conf_interval = parameters['thresholds']['confidence_interval']['max']
    min_conf_interval = parameters['thresholds']['confidence_interval']['min']
    return countries,verticals,max_conf_interval,min_conf_interval,countries_timeframe,vertical_min_cancels


def check_if_triggered(country):
    timezone,interval = timezones.country_timezones(country)
    query = """select vertical as vertical, count(*) as times from ops_occ.new_cancellation_vertical_spike
        where alarm_at::date = convert_timezone('America/{timezone}', current_date)
        and country = '{country}'
        group by 1
        """.format(country=country,timezone=timezone)
    triggered_verticals = snow.run_query(query)
    return triggered_verticals


def cancels(country, country_tf):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	--no_cache
	with base as
	(SELECT order_id
	FROM order_modifications om
	join (select max(created_at) max_c from order_modifications) om2 on om.created_at >= max_c - interval '{timeframe} minutes' 
	WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}' - interval '85 minutes')
	and ((params ->> 'cancelation_reason')::text not ilike '%LIMBO%OCC%' or (params ->> 'cancelation_reason') is null)
	AND (TYPE IN ('close_order','arrive','finish_order','close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') OR TYPE ILIKE '%cancel%')
	GROUP BY 1
	)
	select coalesce(o2.store_id,0) as store_id, coalesce(os.name,'whim') as store_name, case when city_address_id is null then 0 else city_address_id end as city_address_id, 
	case when init_store_type in ('whim') then 'whim' else os.type end as store_type, 
	count(distinct base.order_id) as orders, 
	count(distinct case when o.state ilike '%cancel%' then base.order_id else null end) as canceladas,
	string_agg(distinct case when o.state ilike '%cancel%' then base.order_id::text else null end,';') as order_ids
	from base
	left join calculated_information.orders o2 on o2.order_id=base.order_id
	left join order_stores os on os.order_id=o2.order_id and os.store_id=o2.store_id and os.contact_email not like '%@synth.rappi.com%'
	join orders o on o.id=base.order_id and o.state not in ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error','canceled_by_split_error','canceled_by_early_regret')
	where (os.type not in ('rappi_pay','queue_order_flow','soat_webview') and os.type not ilike '%rappi%pay%' or os.type is null)
	and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
	and o.payment_method not ilike '%synthe%'
	and os.name not ilike '%Dummy Store%'
	group by 1,2,3,4'''.format(timezone=timezone, timeframe = country_tf)

	if country == 'co':
		metrics = redash.run_query(1904, query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
	elif country == 'mx':
		metrics = redash.run_query(1371, query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)

	return metrics

def verticals(country):
	query ='''
	--no_cache
	select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	 when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	 when vertical_group = 'WHIM' then 'Antojos'
	 when vertical_group = 'RAPPICASH' then 'RappiCash'
	 when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
	 when store_type in ('turbo') then 'Turbo'
	 when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
	 when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
	 when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
	 when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
	 when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
	 when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2'''.format(country=country)

	df = snow.run_query(query)
	return df

def cities(country):
	query='''
	--no_cache
	select ca.id, ca.city
	from city_addresses ca
	join countries c on c.id=ca.country_id and code_iso_2='{country}'
	'''.format(country=country.upper())

	if country == 'co':
		metrics = redash.run_query(6164, query)
	elif country == 'ar':
		metrics = redash.run_query(6092, query)
	elif country == 'cl':
		metrics = redash.run_query(6093, query)
	elif country == 'mx':
		metrics = redash.run_query(6152, query)
	elif country == 'uy':
		metrics = redash.run_query(6436, query)
	elif country == 'ec':
		metrics = redash.run_query(6066, query)
	elif country == 'cr':
		metrics = redash.run_query(6094, query)
	elif country == 'pe':
		metrics = redash.run_query(6103, query)
	elif country == 'br':
		metrics = redash.run_query(6151, query)

	return metrics

def store_infos(country):
	if country == 'br':
		tag_id = 318574
	elif country == 'mx':
		tag_id = 65702
	elif country == 'co':
		tag_id = 325885
	elif country == 'ar':
		tag_id = 325778
	elif country == 'cl':
		tag_id = 151
	elif country == 'pe':
		tag_id = 123
	elif country == 'uy':
		tag_id = 325759
	elif country == 'ec':
		tag_id = 121
	elif country == 'cr':
		tag_id = 121

	query_store_infos = """ 
	select a.store_id as id_store, 
	max(a.type) as storetype,
	max(c.id) as brand_id, 
	max(c.name) as brand_name, 
	max(case when t.tag_id is not null then 'ccp' else null end) as tag
    
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c on c.id=a.store_brand_id
    left join {country}_PGLR_MS_STORES_PUBLIC.store_tag t on t.store_id=a.store_id and t.tag_id = {tag_id}
    group by 1
	""".format(country=country,tag_id=tag_id)

	store_infos = snow.run_query(query_store_infos)

	return store_infos

def category(country):
	query = f'''SELECT '{country}' as country1, si.store_id::text as storeid, c.category
	      from ops_occ.store_infos_latam si
	      left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=si.store_id
	      left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=si.brand_group_id and s.city_address_id=c.city_address_id and c.COUNTRY='{country.upper()}'
	      and c.vertical=(case when si.vertical in ('Mercados') then 'Super/Hiper' else si.vertical end)
	      where si.country='{country}'
	      union all
	      (select lower(country) as country1, store_id::text as storeid,
      	  case when brand_segmentation in ('1. CORPORATE BRAND','2. FINE DINING','2. LOCAL CHAIN','3. LOCAL CHAIN','4. LOCAL HERO','6. BRAND DEVELOPMENT')
          then 'Top'
          else 'LongTail' end as category
		  from RESTAURANTS_GLOBAL_POSTSALES.A_SCORECARD_REST4 a
		  where lower(country)='{country}'
		  group by 1,2,3)
	      '''
	df = snow.run_query(query)
	return df

def final(country, country_tf):
    # channel_alert = timezones.slack_channels('partner_operation')

    a = cancels(country, country_tf)
    b = verticals(country)
    c = cities(country)
    d = store_infos(country)
    e = category(country)
    # merging a with b in order to get the verticals

    df = pd.merge(a,b, how='left',left_on=a['store_type'],right_on=b['storetype'])
    df = df.drop(['key_0','storetype'], axis=1)
    grouped_df = df.groupby(["store_id","store_name","vertical","city_address_id","order_ids"])[["canceladas","orders"]].sum().reset_index()
    grouped_df['Percentage'] = round((grouped_df['canceladas'] / grouped_df['orders']) * 100,2)

    # merging grouped_df with c and d in order to get the store cities and other store informations

    final_sheets_df = pd.merge(grouped_df,c, how='left',left_on=grouped_df['city_address_id'],right_on=c['id'])
    final_sheets_df = final_sheets_df.drop(['key_0','id'], axis=1)

    final_sheets_df = pd.merge(final_sheets_df,d, how='left',left_on=final_sheets_df['store_id'],right_on=d['id_store'])
    final_sheets_df = final_sheets_df.drop(['key_0','id_store'], axis=1)
    final_sheets_df  = final_sheets_df[['store_id','store_name',"tag",'storetype','vertical','brand_id','brand_name','city','order_ids','canceladas','orders','Percentage']]

    final_sheets_df['country'] = country
    e = e.drop_duplicates(subset=['storeid'], keep='last')
    e['storeid'] = e['storeid'].astype(str)
    final_sheets_df['store_id'] = final_sheets_df['store_id'].astype(str)
    final_sheets_df = final_sheets_df.merge(e, how='left', left_on=['country', 'store_id'], right_on=['country1', 'storeid'])
    final_sheets_df = final_sheets_df.drop(['country1', 'storeid', 'country'], axis=1)
    final_sheets_df.category = final_sheets_df.category.fillna('D')

    # data by verticals

    vertical_df = df.groupby(["vertical"])[["canceladas","orders"]].sum().reset_index()
    vertical_df['percentage'] = (vertical_df['canceladas'] / vertical_df['orders']) * 100
    vertical_df['country'] = country.upper()
    vertical_df['week'] = 'w0'
    
    return vertical_df[['country','week','vertical','canceladas','orders','percentage']], final_sheets_df


def get_tz(country):
    if country == 'BR':
        current_tz = 'Etc/GMT+7'
    elif country == 'MX':
        current_tz = 'UTC'
    elif country == 'CO':
        current_tz = 'Etc/GMT+7'
    elif country == 'CR':
        current_tz = 'UTC'
    elif country == 'AR':
        current_tz = 'Etc/GMT+7'
    elif country == 'PE':
        current_tz = 'UTC'
    elif country == 'CL':
        current_tz = 'UTC'
    elif country == 'EC':
        current_tz = 'UTC'
    elif country == 'UY':
        current_tz = 'Etc/GMT+7'
    return current_tz
    



def historical_data(country,country_tf):
        country = country.upper()
        current_tz = get_tz(country)
        timezone, timeframes = timezones.country_timezones('co')
        historical_query =     """
with vertical
AS
  (
         SELECT store_type AS store_type,
                CASE
                       WHEN vertical_group = 'ECOMMERCE' THEN 'Ecommerce'
                       WHEN vertical_group = 'RESTAURANTS' THEN 'Restaurantes'
                       WHEN vertical_group = 'WHIM' THEN 'Antojos'
                       WHEN vertical_group = 'RAPPICASH' THEN 'RappiCash'
                       WHEN vertical_group = 'RAPPIFAVOR' THEN 'RappiFavor'
                       WHEN store_type                 IN ('turbo') THEN 'Turbo'
                       WHEN upper (vertical_sub_group) IN ('TURBO') THEN 'Turbo'
                       WHEN upper (vertical_sub_group) IN ('SUPER',
                                                           'HIPER') THEN 'Mercados'
                       WHEN upper (vertical_sub_group) IN ('PHARMACY') THEN 'Farmacia'
                       WHEN upper (vertical_sub_group) IN ('LIQUOR') THEN 'Licores'
                       WHEN upper (vertical_sub_group) IN ('EXPRESS') THEN 'Express'
                       WHEN upper(vertical_group)      IN ('CPGS') THEN 'CPGs'
                       ELSE 'Others'
                end AS sub_group
         FROM   verticals_latam.{country}_verticals_v2 )
  ,created_orders_D0
AS
  (
            SELECT    a.created_at::date AS day,
                      vertical.sub_group as vertical,
                      count(DISTINCT a.id) AS created_orders
            FROM      {country}_core_orders_public.orders_vw a
            LEFT JOIN {country}_core_orders_public.order_product_vw b
            ON        a.id = b.order_id
            LEFT JOIN {country}_core_orders_public.order_stores_vw os
            ON        os.order_id = a.id
            LEFT JOIN {country}_pglr_ms_stores_public.stores_vw s
            ON        s.store_id = os.store_id
            JOIN      vertical
            ON        vertical.store_type = s.type
            WHERE     (a.created_at BETWEEN DATEADD(DD,-7,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-7, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                   or a.created_at BETWEEN DATEADD(DD,-14,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-14, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-21,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-21, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-28,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-28, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-35,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-35, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-42,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-42, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-49,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-49, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                    or a.created_at BETWEEN DATEADD(DD,-56,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-56, CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP))
                      )
            
            AND       coalesce(a._fivetran_deleted,FALSE)=FALSE
            AND       coalesce(b._fivetran_deleted,FALSE)=FALSE
            AND       a.state NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error',
                                      'canceled_by_early_regret' )
            --AND       vertical.sub_group not in ('Restaurantes','Turbo')
            AND       (
                                os.type NOT IN ('rappi_pay',
                                                'queue_order_flow',
                                                'soat_webview')
                      AND       os.type NOT ilike '%rappi%pay%'
                      OR        os.type IS NULL)
            AND       (
                                os.store_type_group NOT IN ('restaurant_cargo')
                      OR        os.store_type_group IS NULL)
            AND       a.payment_method NOT ilike '%synthe%'
            AND       os.name NOT ilike '%Dummy Store%'
            GROUP BY  1,2)
  , cancels_d0
AS
  (
            SELECT    canceled_at::date as  day,
                      vertical.sub_group as vertical,
                      count(DISTINCT z.order_id)                                                                                                                                        AS cancels                                                                                                                                                
            FROM      ops_global.cancellation_reasons z
            JOIN      {country}_core_orders_public.orders_vw a
            ON        z.order_id = a.id
            LEFT JOIN {country}_core_orders_public.order_product_vw b
            ON        a.id = b.order_id
            LEFT JOIN {country}_core_orders_public.order_stores_vw os
            ON        os.order_id = a.id
            AND       os.contact_email NOT LIKE '%@synth.rappi.com%'
            LEFT JOIN {country}_pglr_ms_stores_public.stores_vw s
            ON        s.store_id = os.store_id
            LEFT JOIN      vertical
            ON        vertical.store_type = s.type
            WHERE     a.state NOT IN ('canceled_by_fraud',
                                      'canceled_for_payment_error',
                                      'canceled_by_split_error',
                                      'canceled_by_early_regret' )
            AND       z.country = '{country}'
            AND       (z.canceled_at BETWEEN DATEADD(DD,-7,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-7, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                   or z.canceled_at BETWEEN DATEADD(DD,-14,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-14, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-21,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-21, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-28,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-28, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-35,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-35, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-42,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-42, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-49,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-49, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                    or z.canceled_at BETWEEN DATEADD(DD,-56,DATEADD(minutes,-{timeframe},CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))) AND DATEADD(DD,-56, CONVERT_TIMEZONE('Etc/GMT+7','America/{timezone}', CURRENT_TIMESTAMP))
                      )
            --AND       vertical.sub_group not in ('Restaurantes','Turbo')
            AND       (
                                os.type NOT IN ('rappi_pay',
                                                'queue_order_flow',
                                                'soat_webview')
                      AND       os.type NOT ilike '%rappi%pay%'
                      OR        os.type IS NULL)
            AND       (
                                os.store_type_group NOT IN ('restaurant_cargo')
                      OR        os.store_type_group IS NULL)
            AND       a.payment_method NOT ilike '%synthe%'
            AND       os.name NOT ilike '%Dummy Store%'
            GROUP BY  1,2 )



select '{country}' as country, 
CASE WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 7 then 'w1'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 14 then 'w2'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 21 then 'w3'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 28 then 'w4'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 35 then 'w5'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 42 then 'w6'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 49 then 'w7'
WHEN a.day = CONVERT_TIMEZONE('{current_tz}','America/{timezone}', CURRENT_TIMESTAMP)::DATE - 56 then 'w8' end as week, 
a.vertical,
ZEROIFNULL(b.cancels) as canceladas,
ZEROIFNULL(a.created_orders) as orders
from created_orders_D0 a
left join cancels_d0 b
on a.day = b.day
and a.vertical = b.vertical
order by 1,3,2 asc""".format(country = country, timezone = timezone, current_tz = current_tz, timeframe = country_tf)

        historical = snow.run_query(historical_query)
        historical['percentage'] = (historical['canceladas'] / historical['orders'] * 100)
        return historical



def get_data(country,country_tf):
    today, full = final(country,country_tf)
    historical = historical_data(country,country_tf)
    data = today.append(historical)
    return data, full

def get_vertical_data(country_data):
    vertical_data = {}
    for i in country_data['vertical'].unique():
        vertical_data[i] = country_data[country_data['vertical'] == i].reset_index(drop=True)
    return vertical_data



def add_to_log(vertical,country,orders_criadas,canceladas,cancel_rate,hist_cancel,hist_rate,today):
    sheets = today[(today['vertical']== vertical)].sort_values(by='canceladas', ascending=False)
    
    current_time = timezones.country_current_time(country)
    data = {'country': [country], 'vertical': [i],'orders':[orders_criadas],'cancels':[canceladas],'cancel_rate':[cancel_rate],'historical_cancels':[hist_cancel],'historical_cancel_rate':[hist_rate]}
    to_upload = pd.DataFrame(data=data)    
    to_upload['alarm_at'] = current_time

    snow.upload_df_occ(to_upload, 'new_cancellation_vertical_spike')

    to_upload2 = sheets
    to_upload2['alarm_at'] = current_time
    to_upload2['country'] = country
    snow.upload_df_occ(to_upload2, 'new_cancellation_vertical_spike_details')



def send_data(country_tf,vertical,country,orders_criadas,canceladas,cancel_rate,hist_cancel,hist_rate,today,incidences=0):
    sheets = today[(today['vertical']== vertical)].sort_values(by='canceladas', ascending=False)
    home = expanduser("~")
    results_file = '{}/{}_details.xlsx'.format(home, vertical)
    sheets.to_excel(results_file,sheet_name='Stores', index=False)
    book = load_workbook(results_file)
    writer = pd.ExcelWriter(results_file, engine='openpyxl')
    writer.book = book
    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

    sheets_brands = sheets.groupby(["brand_id","brand_name","vertical"])[["canceladas","orders"]].sum().reset_index()
    sheets_brands['Percentage'] = round((sheets_brands['canceladas'] / sheets_brands['orders']) * 100,2)
    sheets_brands = sheets_brands.sort_values(by='canceladas', ascending=False)

    sheets_cities = sheets.groupby(["city"])[["canceladas","orders"]].sum().reset_index()
    sheets_cities['Percentage'] = round((sheets_cities['canceladas'] / sheets_cities['orders']) * 100,2)
    sheets_cities = sheets_cities.sort_values(by='canceladas', ascending=False)

    sheets_brands.to_excel(writer, "Brands", index=False)
    sheets_cities.to_excel(writer, "Cities", index=False)

    writer.save()
    writer.close()

    text = '''
    :warning: *TESTE - NUEVO Pico de Cancelación en Vertical* :warning: 
    :flag-{country}:
    Pais: {country}
    Vertical *{vertical}* alcanzó el *{cancel_rate}% de cancelación* en los últimos {timeframe} minutos :chart_with_upwards_trend:
    *Cancelados:* {canceladas}
    *Ordenes:* {orders}
    *Incidencias hoy hasta ahora:* {incidencias}
    Lista de tiendas infractoras  :arrow_down:
    '''.format(country = country.upper(), vertical = vertical, orders = orders_criadas, canceladas = canceladas, cancel_rate = cancel_rate,incidencias=incidences, timeframe = country_tf)
    slack.file_upload_channel('C02L5JX5CT0',text, results_file, 'xlsx')

    current_time = timezones.country_current_time(country)
    data = {'country': [country], 'vertical': [vertical],'orders':[orders_criadas],'cancels':[canceladas],'cancel_rate':[cancel_rate],'historical_cancels':[hist_cancel],'historical_cancel_rate':[hist_rate]}
    to_upload = pd.DataFrame(data=data)    
    to_upload['alarm_at'] = current_time

    snow.upload_df_occ(to_upload, 'new_cancellation_vertical_spike')

    to_upload2 = sheets
    to_upload2['alarm_at'] = current_time
    to_upload2['country'] = country
    snow.upload_df_occ(to_upload2, 'new_cancellation_vertical_spike_details')


def run_alarm(verticals_data,country,today,max_conf_int,min_conf_int,vertical_cancels,country_tf):
    #Gera os dados do pais para as verticais hoje
    triggered_verticals = check_if_triggered(country)
    for i in today['vertical'].unique():
        print(country+' '+i+' started.')
        #Pegando o threshold de cancels minimo da vertical
        vertical_cancel = vertical_cancels.loc[vertical_cancels['Vertical'] == i]['Min'].iloc[0]
        #Convertendo dados historicos para lista pois o grubbs requer os dados em lista
        weeks = verticals_data[i]['week'].tolist()
        orders_criadas = verticals_data[i]['orders'].tolist()
        canceladas = verticals_data[i]['canceladas'].tolist()
        cancel_rate = verticals_data[i]['percentage'].tolist()
        #Pegando os valores de d0 dentro da lista para usar no print e thresholds
        today_created = orders_criadas[0]
        today_cancels = canceladas[0]
        today_cancel_rate = round(cancel_rate[0],2)
        #Salvando o historico de cancel e cancelrate num string para enviar para o log
        hist_cancels = ", ".join([str(i) for i in canceladas[1:]])
        hist_cancel_rate = ", ".join([str(round(i,2)) for i in cancel_rate[1:]])
        #Verificar se a quantidade de cancels da vertical e maior do que o threshold
        if today_cancels >= vertical_cancel:
            #Verificar se ja disparou hoje
            if len(triggered_verticals.loc[triggered_verticals['vertical'] == i]['times']) > 0:
                todays_incidences = triggered_verticals.loc[triggered_verticals['vertical'] == i]['times'].iloc[0]
                check_cancels = grubbs.max_test_indices(canceladas, alpha = min_conf_int)
                check_cancel_rate = grubbs.max_test_indices(cancel_rate, alpha = min_conf_int)
                #Se ja disparou hoje testa se os dados sao outliers com 75
                if ('w0' in weeks) & (0 in check_cancels) & (0 in check_cancel_rate):
                    print(i + " deve disparar com reincidencia")
                    send_data(country_tf, i ,country, today_created, today_cancels, today_cancel_rate, hist_cancels, hist_cancel_rate, today, todays_incidences)
                    print(i + " reincidencia")
                else:
                    print(i + " ja disparou e nao disparou novamente")
            else:
                check_cancels_95 = grubbs.max_test_indices(canceladas, alpha = max_conf_int)
                check_cancel_rate_95 = grubbs.max_test_indices(cancel_rate, alpha = max_conf_int)
                #Se ainda nao disparou hoje testar primeiro com 95
                if ('w0' in weeks) & (0 in check_cancels_95) & (0 in check_cancel_rate_95):
                    print(i + " deve disparar pela primeira vez com 95")
                    send_data(country_tf, i ,country, today_created, today_cancels, today_cancel_rate, hist_cancels, hist_cancel_rate, today)
                    print(i + " primeira vez 95")
                else:
                    check_cancels_75 = grubbs.max_test_indices(canceladas, alpha = min_conf_int)
                    check_cancel_rate_75 = grubbs.max_test_indices(cancel_rate, alpha = min_conf_int)
                    #Se ainda nao disparou e nao caiu com 95 testar com 75
                    if ('w0' in weeks) & (0 in check_cancels_75) & (0 in check_cancel_rate_75):
                        print(i+ " deve disparar primeira vez com 75")
                        add_to_log(i ,country, today_created, today_cancels, today_cancel_rate, hist_cancels, hist_cancel_rate, today)
                        print(i + " primeira vez 75")
                    else:
                        print("OK")
        else:
            print("Nao bateu threshold de min cancels por vertical")
            


countries_list, verticals_list, max_interval, min_interval, countries_timeframe, vertical_min_cancels = read_parameters()
print(countries_list)
print(verticals_list)
for i in countries_list:
    try:
        country_tf = countries_timeframe.loc[countries_timeframe['Country'] == i.upper()]['Timeframe'].iloc[0]
        raw_data, today = get_data(i,country_tf)
        today =  today.loc[today['vertical'].isin(verticals_list)]
        print(today)
        print(i + " raw data done")
        verticals_data = get_vertical_data(raw_data)
        print(verticals_data)
        print(i + " verticals data done")
        run_alarm(verticals_data,i,today,max_interval,min_interval,vertical_min_cancels,country_tf)
        print(i+" done")
    except Exception as e:
        print(e)
        print("Error en "+i)
        pass