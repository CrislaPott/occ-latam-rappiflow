import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones, snippets
import regex as re


def query_eletronics(country):
    query = '''
(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
             join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id
    where name like '% Garmin %'
       or name like '%Garmin %'
       or name like '%Amazfit%'
       or name like '%smartwatch%'
       or name like '%haylou%'
       or name ilike '%projetor led%'
       or name ilike '%receiver%'
       or name ilike '%yamaha%'
       or name ilike '%telescopio%'
       or name ilike '%telescópio%'
       or name ilike '%telescopio%'
       or name ilike '%telescópio%'
       or name ilike '% Go Pro %'
       or name ilike 'Go Pro %'
       or name ilike 'Drone %'
       or name ilike 'echo %'
       or name ilike '% echo %'
       or name ilike '%homepod %'
       or name ilike 'multifuncional%'
       or name ilike '%epson%'
       or name ilike '%impressora%'
       or name ilike '%deskjet%'
       or name ilike '%notebook%'
       or name like 'Tablet%'
       or name like '% Garmin%'
or name like '%Amazfit%'
or name like '%smartwatch%'
or name like '%haylou%'
or name ilike '% televisor led%'
or name ilike '%barra de sonido%'
or name ilike '%laser%'
or name ilike '% Go Pro %'
or name ilike 'Iphone %'
or name ilike 'Drone %'
or name ilike '% echo%'
or name ilike '%homepod %'
or name ilike '%multifuncional%'
or name ilike '%epson%'
or name ilike '%impressora%'
or name ilike '%deskjet%'
or name ilike '%notebook%'
or name like 'Tablet%'
or name ilike '%Ipad%'
or name ilike '%pioneer%'
or name ilike '%beats%'
or name ilike '%Apple watch%'
or name ilike '%HUAWEI%'
or name ilike '%airpods%'
or name ilike '%JBL%'
or name ilike '%Bose%'
or name like '%MacBook%'
or name ilike '%Sony%'
or name ilike '%Xiaomi%'
or name ilike '%Audio-Technica%'
or name ilike '%Hudson%'
or name ilike '%Samsung%'
or name ilike '%galaxy%'
or name ilike '%hp Book%'
or name ilike '%yamaha%'
or name ilike '%economodo%'
or name ilike '%Ge Force%'
or name ilike '%Procesador%'
or name ilike '%nintendo%'
or name ilike '%consola%'
or name ilike '%homepod%'
or name ilike '%deskjet%'
or name ilike '%deco mesh%'
or name ilike '%lenovo%'
or name ilike '%alexa%'
or name ilike '%disco duro%'
or name ilike '%hytab%'
or name ilike '%nikon%'
or name ilike '%canon%'
or name ilike '%monitor%' 
or name ilike '%kaley%'
or name like '%Amazfit%'
   or name like '%smartwatch%'
   or name like '%haylou%'
   or name ilike '%projetor led%'
   or name ilike '%receiver%'
   or name ilike '%yamaha%'
   or name ilike '%telescopio%' or name ilike '%telescópio%'
   or name ilike '% Go Pro %'
   or name ilike 'Go Pro %'
   or name ilike 'Drone %'
   or name ilike 'echo %'
   or name ilike '% echo %'
   or name ilike '%homepod %'
   or name ilike 'multifuncional%'
   or name ilike '%epson%'
   or name ilike '%impressora%'
   or name ilike '%deskjet%'
   or name ilike '%notebook%'
   or name like 'Tablet%'
   or name ilike 'Tablet%'
   or name ilike '%pioneer%'
   or name ilike '%beats%'
   or name ilike '%SKULLCANDY%'
   or name ilike '%HUAWEI%'
   or name ilike '%Sennheiser%'
   or name ilike '%Onkyo%'
   or name ilike '%Bose%'
   or name like '%KEF%'
   or name ilike '%Harman Kardon%'
   or name ilike '%denon%'
   or name ilike '%Audio-Technica%'
   or name ilike '%Hudson%'
   or name ilike '%Edifier%'
   or name ilike '%projetor led%'
   or name ilike '%receiver%'
   or name ilike '%yamaha%'
   or name ilike '%telescopio%' 
   or name ilike '%Ge Force%'
   or name ilike '%Procesador%'
   or name ilike '% Go Pro %'
   or name ilike '%nintendo%'
   or name ilike '%Drone%'
   or name ilike '%consola%'
   or name ilike '% echo %'
   or name ilike '%homepod %'
   or name ilike '%multifuncional%'
   or name ilike '%epson%'
   or name ilike '%impresora%'
   or name ilike '%deskjet%'
   or name ilike '%deco mesh%'
   or name ilike '%lenovo%'
   or name ilike '%alexa%'
   or name ilike '%disco duro%'
   or name ilike '%hytab%'
   or name ilike '%nikon%'
   or name ilike '%canon%'
   or name ilike '%monitor%'
        and ps.price <= 500
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
    '''.format(country=country)

    df = snow.run_query(query)
    return df


def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select groupbrandid::text as identification_id, last_growth
    from
    (select groupbrandid, last_value(growth) over (partition by groupbrandid order by alarm_at asc) as last_growth
    from ops_occ.spike_in_orders
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '6h'
    and country='{country}'
    )
    group by 1,2
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df


def store_history(country):
    timezone, interval = timezones.country_timezones(country)
    query = """
    --no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          total_value,
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp())))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D14'
          END AS calendar_day

   FROM {country}_CORE_ORDERS_PUBLIC.orders_vw b
   WHERE (created_at >= date(convert_timezone('America/{timezone}',current_timestamp())) - interval '14 days')
     and created_at < date(convert_timezone('America/{timezone}',current_timestamp()))
   AND to_char(created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
   and to_char(created_at,'HH:MI') < to_char(dateadd(minutes,0,convert_timezone('America/{timezone}',current_timestamp())),'HH:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_hour AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          op.units,
          opd.name as product_name, 
          calendar_day,
          b.total_value as total_value

   FROM base b
   INNER JOIN {country}_CORE_ORDERS_PUBLIC.order_stores_vw os ON os.order_id = b.order_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=b.order_id and os.store_id=op.store_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )

SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name, total_value, units

   FROM day_hour d

   group by 1,2,3,4,5,6,7,d.total_value, units
    """.format(country=country, timezone=timezone)

    df = snow.run_query(query)
    return df


def country_query(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
--no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          total_value,
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D14'
          END AS calendar_day

   FROM orders b
   WHERE (created_at >= date(now() AT TIME ZONE 'America/{timezone}'))
   AND to_char(created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_hour AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          op.units,
          opd.name as product_name, 
          calendar_day,
          b.total_value

   FROM base b
   INNER JOIN order_stores os ON os.order_id = b.order_id
   AND (contact_email NOT LIKE '%@synth.rappi.com'
        OR contact_email IS NULL)
   left join order_product op on op.order_id=b.order_id and os.store_id=op.store_id
   left join order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )

SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name,total_value, units

   FROM day_hour d

   group by 1,2,3,4,5,6,7,d.total_value, units
   """.format(country=country, timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)

    return metrics


def store_infos(country):
    query = """
    --no_cache
    select a.store_id::text as storeid, bg.brand_group_id::text as store_brand_id, bg.brand_group_name as brand_name, 
    case when v.vertical in ('Express','Licores','Farmacia','Turbo') then 'Now'
         when v.vertical in ('Mercados') then 'Mercados'
         when v.vertical in ('Ecommerce') then 'Ecommerce' else null end as vertical
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw b on b.id=a.store_brand_id
    inner join (
    select store_type as storetype,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
    when upper (store_type) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype=a.type

    left join (
       select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
       ) bg on bg.store_brand_id = a.store_brand_id

    where a.deleted_at is null
    and not coalesce (a._fivetran_deleted, false)
    and vertical in ('Now','Mercados','Ecommerce')
    group by 1,2,3,4""".format(country=country)

    df = snow.run_query(query)
    return df


def alarm(a, b, ele):
    channel_alert = timezones.slack_channels('commercial_alarms')

    df0 = pd.merge(a, b, how='inner', left_on=a['store_id'], right_on=b['storeid'])
    df0 = df0.drop(['key_0'], axis=1)
    df0['total_value'] = pd.to_numeric(df0['total_value'], errors='coerce')

    df1_d0 = df0[((df0['calendar_day'] == 'D0'))]

    df_for_ids = df1_d0.drop_duplicates(subset=['store_id', 'store_brand_id', 'vertical', 'order_id'])
    order_ids = df_for_ids.groupby(["store_id"])["order_id"].apply(list).reset_index(name='order_ids_d0')
    df1_d0 = df1_d0.drop_duplicates(subset=["store_brand_id", "brand_name", "vertical", "order_id"])

    #####store_ids
    df1_d0_store = df1_d0.groupby(["store_brand_id", "brand_name", "vertical"])['store_id'].agg(
        [('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()

    df1_d01 = df1_d0.groupby(["store_brand_id", "brand_name", "vertical"])['order_id'].count().reset_index(
        name="orders_d0")
    df1_d02 = df1_d0.groupby(["store_brand_id", "brand_name", "vertical"])['total_value'].mean().reset_index(
        name="avg_total_value_d0")
    df1_d0_store1 = pd.merge(df1_d01, df1_d0_store, on=["store_brand_id", "brand_name", "vertical"])
    df1_d0_store = pd.merge(df1_d0_store1, df1_d02, on=["store_brand_id", "brand_name", "vertical"])

    df1_d7 = df0[((df0['calendar_day'] == 'D7'))]
    df1_d7 = df1_d7.drop_duplicates(subset=["store_brand_id", "brand_name", "vertical", "order_id"])
    df1_d71 = df1_d7.groupby(["store_brand_id", "brand_name", "vertical"])['order_id'].count().reset_index(
        name="orders_d7")
    df1_d72 = df1_d7.groupby(["store_brand_id", "brand_name", "vertical"])['order_id'].mean().reset_index(
        name="avg_total_value_d7")
    df1_d7 = pd.merge(df1_d71, df1_d72, on=["store_brand_id", "brand_name", "vertical"])

    df1_d14 = df0[((df0['calendar_day'] == 'D14'))]
    df1_d14 = df1_d14.drop_duplicates(subset=["store_brand_id", "brand_name", "vertical", "order_id"])
    df1_d141 = df1_d14.groupby(["store_brand_id", "brand_name", "vertical"])['order_id'].count().reset_index(
        name="orders_d14")
    df1_d142 = df1_d14.groupby(["store_brand_id", "brand_name", "vertical"])['order_id'].mean().reset_index(
        name="avg_total_value_d14")
    df1_d14 = pd.merge(df1_d141, df1_d142, on=["store_brand_id", "brand_name", "vertical"])

    final_df = pd.concat([df1_d7, df1_d14, df1_d0_store], axis=0, ignore_index=True)

    final_df['orders_d0'].fillna(0, inplace=True)
    final_df['orders_d7'].fillna(0, inplace=True)
    final_df['orders_d14'].fillna(0, inplace=True)

    df1 = final_df
    df1_store = df1[(df1['store_id'].notna())]
    df1_store = df1_store.groupby(["store_brand_id", "brand_name", "vertical"])['store_id'].agg(
        [('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()

    lista1 = []
    for index, row in df1_store.iterrows():
        data = re.findall(r'[0-9]{6,}', str(row['store_id']))
        newlist = list(set(data))
        lista1.append(newlist)
    df1_store['store_ids'] = lista1
    df1_store = df1_store.drop(['store_id'], axis=1)
    df1_store['store_ids'] = df1_store['store_ids'].astype(str)

    df1_1 = df1.groupby(["store_brand_id", "brand_name", "vertical"])[
        ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
    df1_2 = df1.groupby(["store_brand_id", "brand_name", "vertical"])[
        ["avg_total_value_d0", "avg_total_value_d7", "avg_total_value_d14"]].mean().reset_index()
    df1 = pd.merge(df1_1, df1_2, on=["store_brand_id", "brand_name", "vertical"])

    df1['average_last_weeks'] = ((df1['orders_d7'] + df1['orders_d14']) / 2)
    df1['growth'] = round((((df1['orders_d0'] - df1['average_last_weeks']) / df1['average_last_weeks']) * 100), 2)
    df1['growth'] = df1['growth'].replace(np.inf, np.nan)

    df1 = df1[((df1['vertical'].isin(['Now', 'Mercados'])) & (df1['growth'] >= 40) & (
            (df1['orders_d0'] - df1['average_last_weeks']) >= 15))
              | ((df1['vertical'].isin(['Now', 'Mercados'])) & (df1['orders_d7'] == 0) & (df1['orders_d14'] == 0) & (
            df1['orders_d0'] >= 15))
              | ((df1['vertical'].isin(['Ecommerce'])) & (df1['orders_d7'] == 0) & (df1['orders_d14'] == 0) & (
            df1['orders_d0'] >= 20))
              | ((df1['vertical'].isin(['Ecommerce'])) & ((df1['orders_d0'] - df1['average_last_weeks']) >= 20) & (
            df1['growth'] >= 30))
              ]

    if not df1.empty:
        df1 = pd.merge(df1, df1_store, how='left', left_on=df1['store_brand_id'], right_on=df1_store['store_brand_id'])[
            ['store_brand_id_x', 'brand_name_x', 'vertical_x', 'orders_d0',
             'orders_d7', 'orders_d14', 'average_last_weeks', 'growth', 'store_ids']].rename(
            columns={'store_brand_id_x': 'store_brand_id', 'brand_name_x': 'brand_name', 'vertical_x': 'vertical'})

        print("proof")
        print(df1)

        #c = excluding_stores(country)
        #not_df = c
        #not_df['last_growth'] = not_df['last_growth'] + 30  # more than 15% of variation
        #print(not_df)

        #df1 = pd.merge(df1, not_df, how='left', left_on=df1['store_brand_id'], right_on=not_df['identification_id'])
        #df1 = df1[(df1['identification_id'].isnull()) | (df1['growth'] > df1['last_growth'])]
        #df1 = df1.drop(['key_0', 'identification_id', 'last_growth'], axis=1)

        df1 = df1.drop_duplicates(subset=['store_brand_id'])

        current_time = timezones.country_current_time(country)

        to_upload = df1.rename(columns={"store_brand_id": "groupbrandid"})
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        to_upload = to_upload[
            ['groupbrandid', 'vertical', 'growth', 'alarm_at', 'country', 'orders_d0', 'orders_d7', 'orders_d14',
             'brand_name', 'store_ids']]
        print(to_upload)

        df1 = df1.drop(['store_ids'], axis=1)

        for index, row in df1.iterrows():
            rows = dict(row)
            df_row = df0[((df0['store_brand_id'] == row['store_brand_id']) & (df0['vertical'] == row['vertical']))]
            df_row_0 = df_row[((df_row['calendar_day'] == 'D0'))]
            df_row_0 = df_row_0.fillna(0)
            df_row_0 = df_row_0.drop_duplicates(
                subset=["store_id", "store_name", "coupon_code", "payment_method", "application_user_id",
                        "order_id"]).reset_index()
            df_row_0 = \
                df_row_0.groupby(["store_id", "store_name", "coupon_code", "payment_method", "application_user_id"])[
                    'order_id'].count().reset_index(name="orders_d0")

            df_row_7 = df_row[((df_row['calendar_day'] == 'D7'))]
            df_row_7 = df_row_7.fillna(0)
            df_row_7 = df_row_7.drop_duplicates(
                subset=["store_id", "store_name", "coupon_code", "payment_method", "application_user_id", "order_id"])
            df_row_7 = \
                df_row_7.groupby(["store_id", "store_name", "coupon_code", "payment_method", "application_user_id"])[
                    'order_id'].count().reset_index(name="orders_d7")

            df_row_14 = df_row[((df_row['calendar_day'] == 'D14'))]
            df_row_14 = df_row_14.fillna(0)
            df_row_14 = df_row_14.drop_duplicates(
                subset=["store_id", "store_name", "coupon_code", "payment_method", "application_user_id", "order_id"])
            df_row_14 = \
                df_row_14.groupby(["store_id", "store_name", "coupon_code", "payment_method", "application_user_id"])[
                    'order_id'].count().reset_index(name="orders_d14")

            df_row_final = pd.concat([df_row_7, df_row_14, df_row_0], axis=0, ignore_index=True)

            df_row_final['orders_d0'].fillna(0, inplace=True)
            df_row_final['orders_d7'].fillna(0, inplace=True)
            df_row_final['orders_d14'].fillna(0, inplace=True)

            df2 = df_row_final.groupby(["store_id", "store_name"])[
                ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()

            soma = sum(df2['orders_d0'])
            df2['share_d0'] = round(((df2['orders_d0'] / soma) * 100), 2)
            df2['average_last_weeks'] = ((df2['orders_d7'] + df2['orders_d14']) / 2)
            df2['growth'] = round((((df2['orders_d0'] - df2['average_last_weeks']) / df2['average_last_weeks']) * 100),2)
            df2 = pd.merge(df2, order_ids, on=['store_id'], how='left')
            df2 = df2[
                ['store_id', "store_name", 'orders_d0', 'share_d0', 'orders_d7', 'orders_d14', 'average_last_weeks',
                 'growth', 'order_ids_d0']]
            df2 = df2.sort_values(by='orders_d0', ascending=False)
            home = expanduser("~")
            results_file = '{}/details.xlsx'.format(home)
            df2.to_excel(results_file, sheet_name='stores', index=False)
            book = load_workbook(results_file)
            writer = pd.ExcelWriter(results_file, engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

            df3 = df_row_final.groupby(["coupon_code"])[["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df3['orders_d0'])
            df3['share_d0'] = round(((df3['orders_d0'] / soma) * 100), 2)
            df3['average_last_weeks'] = ((df3['orders_d7'] + df3['orders_d14']) / 2)
            df3['growth'] = round((((df3['orders_d0'] - df3['average_last_weeks']) / df3['average_last_weeks']) * 100), 2)
            df3 = df3[
                ['coupon_code', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks", "growth"]]
            df3 = df3.sort_values(by='orders_d0', ascending=False)
            df3.to_excel(writer, "coupon_code", index=False)

            df4 = df_row_final.groupby(["payment_method"])[["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df4['orders_d0'])
            df4['share_d0'] = round(((df4['orders_d0'] / soma) * 100), 2)
            df4['average_last_weeks'] = ((df4['orders_d7'] + df4['orders_d14']) / 2)
            df4['growth'] = round((((df4['orders_d0'] - df4['average_last_weeks']) / df4['average_last_weeks']) * 100),2)
            df4 = df4[
                ['payment_method', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks", "growth"]]
            df4 = df4.sort_values(by='orders_d0', ascending=False)
            df4.to_excel(writer, "payment_method", index=False)

            df5 = df_row_final.groupby(["application_user_id"])[
                ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df4['orders_d0'])
            df5['share_d0'] = round(((df5['orders_d0'] / soma) * 100), 2)
            df5['average_last_weeks'] = ((df5['orders_d7'] + df5['orders_d14']) / 2)
            df5['growth'] = round((((df5['orders_d0'] - df5['average_last_weeks']) / df4['average_last_weeks']) * 100),2)
            df5 = df5[['application_user_id', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks",
                       "growth"]]
            df5 = df5.sort_values(by='orders_d0', ascending=False)
            df5.to_excel(writer, "application_user_id", index=False)

            prod0 = df_row
            prod0 = prod0[((prod0['calendar_day'] == 'D0'))]
            prod0 = prod0.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
            prod01 = prod0.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d0")
            prod02 = prod0.groupby(["product_id", "product_name"])['total_value'].sum().reset_index(
                name="total_value_d0")
            prod03 = prod0.groupby(["product_id", "product_name"])["units"].sum().reset_index(name="total_units_d0")

            prod0 = pd.merge(prod01, prod02, on=["product_id", "product_name"])
            prod0 = pd.merge(prod0, prod03, on=["product_id", "product_name"])
            prod0['total_value_d0'] = round(prod0['total_value_d0'], 2)

            prod7 = df_row
            prod7 = prod7[((prod7['calendar_day'] == 'D7'))]
            prod7 = prod7.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
            prod71 = prod7.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d7")
            prod72 = prod7.groupby(["product_id", "product_name"])['total_value'].sum().reset_index(
                name="total_value_d7")
            prod73 = prod7.groupby(["product_id", "product_name"])["units"].sum().reset_index(name="total_units_d7")
            prod7 = pd.merge(prod71, prod72, on=["product_id", "product_name"])
            prod7 = pd.merge(prod7, prod73, on=["product_id", "product_name"])
            prod7['total_value_d7'] = round(prod7['total_value_d7'], 2)

            prod14 = df_row
            prod14 = prod14[((prod14['calendar_day'] == 'D14'))]
            prod14 = prod14.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
            prod141 = prod14.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d14")
            prod142 = prod14.groupby(["product_id", "product_name"])['total_value'].sum().reset_index(
                name="total_value_d14")
            prod143 = prod14.groupby(["product_id", "product_name"])["units"].sum().reset_index(name="total_units_d14")
            prod14 = pd.merge(prod141, prod142, on=["product_id", "product_name"])
            prod14 = pd.merge(prod14, prod143, on=["product_id", "product_name"])
            prod14['total_value_d14'] = round(prod14['total_value_d14'], 2)

            prod_final = pd.concat([prod0, prod7, prod14], axis=0, ignore_index=True)

            prod_final['orders_d0'].fillna(0, inplace=True)
            prod_final['orders_d7'].fillna(0, inplace=True)
            prod_final['orders_d14'].fillna(0, inplace=True)

            prod_final1 = prod_final.groupby(["product_id", "product_name"])[
                ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            prod_final2 = prod_final.groupby(["product_id", "product_name"])[
                ["total_value_d0", "total_value_d7", "total_value_d14"]].sum().reset_index()
            prod_final3 = prod_final.groupby(["product_id", "product_name"])[
                ["total_units_d0", "total_units_d7", "total_units_d14"]].sum().reset_index()
            prod_final = pd.merge(prod_final1, prod_final2, on=["product_id", "product_name"])
            prod_final = pd.merge(prod_final, prod_final3, on=["product_id", "product_name"])

            prod_final = pd.merge(prod_final, ele, how='left', on=["product_id"])

            soma = sum(prod_final['orders_d0'])
            prod_final['share_d0'] = round(((prod_final['orders_d0'] / soma) * 100), 2)
            prod_final['average_last_weeks'] = ((prod_final['orders_d7'] + prod_final['orders_d14']) / 2)
            prod_final['growth'] = round((((prod_final['orders_d0'] - prod_final['average_last_weeks']) / prod_final[
                'average_last_weeks']) * 100), 2)
            prod_final['index'] = (prod_final['total_value_d0'] / prod_final['total_units_d0'])
            prod_final = prod_final[
                ['product_id', "product_name", "orders_d0", "orders_d7", "orders_d14", "average_last_weeks", "growth",
                 'total_value_d0', 'total_value_d7', 'total_value_d14', 'total_units_d0', 'total_units_d7',
                 'total_units_d14', 'index', 'product_flag']]

            prod_final = prod_final.sort_values(by='orders_d0', ascending=False)
            prod_final.to_excel(writer, "product_id", index=False)

            vertical0 = row['vertical']

            orderd01 = prod_final['orders_d0'] >= 10
            orderd02 = prod_final['orders_d0'] >= 12

            prod_final_growth = prod_final['growth'] >= 300

            if country in ['br'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                (orderd01 & (prod_final['index'] <= 10)).any() or (orderd01 & prod_final['product_flag'].notnull()).any()):
                t = 2
            elif country in ['co'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                (orderd02 & (prod_final['index'] <= 7170)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['mx'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 40)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['cl'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 1472)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['ar'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 182)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['pe'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 7.26)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['uy'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 78)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['ec'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 2)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['cr'] and vertical0 in ['Now', 'Mercados'] and row['growth'] >= 40 and (
                    (orderd02 & (prod_final['index'] <= 1114)).any() or (orderd01 & prod_final['product_flag'].nornull()).any()):
                t = 2
            elif country in ['cr', 'br', 'co', 'mx', 'pe', 'uy', 'cr', 'ec', 'ar'] and vertical0 in ['Ecommerce'] and ((orderd01 & prod_final['product_flag'].notnull()).any()):
                t = 2
            elif country in ['cr', 'br', 'co', 'mx', 'pe', 'uy', 'cr', 'ec', 'ar'] and vertical0 in ['Ecommerce'] and row['growth'] >= 40:
                t = 1
            elif country in ['cr', 'br', 'co', 'mx', 'pe', 'uy', 'cr', 'ec', 'ar'] and vertical0 in ['Now','Mercados'] and row['growth'] >= 100 and (row['orders_d0'] - row['average_last_weeks']) >= 15:
                t = 1
            else:
                print("outside")
                t = 0

            if t == 1 or t == 2:
                writer.save()

                if ((prod_final_growth | prod_final['growth'] == np.inf) & orderd01).any():
                    stores = df2['store_id'][0:5].tolist()

                    target_date = (current_time + timedelta(minutes=20))
                    time_closed = int((target_date - current_time).total_seconds() / 60.0)
                    reason = "spike_in_orders"
                    if len(stores) >= 1:
                        for store_id in stores:
                            print("as store_ids")
                            print(int(store_id))
                            # cms.disable_store(int(store_id), country, reason, time_closed)
                            # cms.turnStoreOff(int(store_id), time_closed + 5, country, reason) == 200

                    if t == 1:
                        text = '''
                  *Alarma de pico de ordenes creadas :alert: :arrow_up:* 
                  :flag-{country}:
                   Pais: {country}
                  *Brand Group* {brand_name}
                  *Brand Group ID:* {store_brand_id}
                  *Vertical:* {vertical}
                  *Threshold:* Alcanzó *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* misma hora/dia semanas pasadas
                  *Porcentaje de crecimiento:* {growth}%
            
                  *Las tiendas {stores} fueron suspendidas por 20 minutos.*
                  Adjunto más detalles por tiendas, cupones, métodos de pagos, top products y users'''.format(
                            store_brand_id=row['store_brand_id'],
                            brand_name=row['brand_name'],
                            vertical=row['vertical'],
                            d0=row['orders_d0'],
                            d7=row['orders_d7'],
                            average_last_weeks=row['average_last_weeks'],
                            growth=round(row['growth'], 2),
                            stores= stores,
                            country=country)

                    elif t == 2:
                        text = '''
                  *Alarma de pico de ordenes creadas :alert: :arrow_up:* 
                  :flag-{country}:
                   Pais: {country}
                  *Brand Group* {brand_name}
                  *Brand Group ID:* {store_brand_id}
                  *Vertical:* {vertical}
                  *Threshold:* Alcanzó *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* misma hora/dia semanas pasadas
                  *Porcentaje de crecimiento:* {growth}%
                  Adjunto más detalles por tiendas, cupones, métodos de pagos, top products y users
                  
                  *Las tiendas {stores} fueron suspendidas por 20 minutos.*
                  <!subteam^S0191S07TND> <@andrea.gonzalez>
                  :red_warning::red_warning::red_warning: *Trigger de alarma por precios muy bajos. Existe sospecha de productos con errores* :red_warning::red_warning::red_warning: :alert:'''.format(
                            store_brand_id=row['store_brand_id'],
                            brand_name=row['brand_name'],
                            vertical=row['vertical'],
                            d0=row['orders_d0'],
                            d7=row['orders_d7'],
                            average_last_weeks=row['average_last_weeks'],
                            growth=round(row['growth'], 2),
                            stores=stores,
                            country=country)
                else:

                    if t == 1:
                        text = '''
                    *Alarma de pico de ordenes creadas :alert: :arrow_up:* 
                    :flag-{country}:
                     Pais: {country}
                    *Brand Group* {brand_name}
                    *Brand Group ID:* {store_brand_id}
                    *Vertical:* {vertical}
                    *Threshold:* Alcanzó *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* misma hora/dia semanas pasadas
                    *Porcentaje de crecimiento:* {growth}%

                    Adjunto más detalles por tiendas, cupones, métodos de pagos, top products y users'''.format(
                            store_brand_id=row['store_brand_id'],
                            brand_name=row['brand_name'],
                            vertical=row['vertical'],
                            d0=row['orders_d0'],
                            d7=row['orders_d7'],
                            average_last_weeks=row['average_last_weeks'],
                            growth=round(row['growth'], 2),
                            stores=stores,
                            country=country)

                    elif t == 2:
                        text = '''
                    *Alarma de pico de ordenes creadas :alert: :arrow_up:* 
                    :flag-{country}:
                     Pais: {country}
                    *Brand Group* {brand_name}
                    *Brand Group ID:* {store_brand_id}
                    *Vertical:* {vertical}
                    *Threshold:* Alcanzó *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* misma hora/dia semanas pasadas
                    *Porcentaje de crecimiento:* {growth}%
                    Adjunto más detalles por tiendas, cupones, métodos de pagos, top products y users

                    <!subteam^S0191S07TND> <@andrea.gonzalez>
                    :red_warning::red_warning::red_warning: *Trigger de alarma por precios muy bajos. Existe sospecha de productos con errores* :red_warning::red_warning::red_warning: :alert:'''.format(
                            store_brand_id=row['store_brand_id'],
                            brand_name=row['brand_name'],
                            vertical=row['vertical'],
                            d0=row['orders_d0'],
                            d7=row['orders_d7'],
                            average_last_weeks=row['average_last_weeks'],
                            growth=round(row['growth'], 2),
                            stores=stores,
                            country=country)


                print(text)
                #slack.file_upload_channel('C029MJCAMS6', text, results_file, 'xlsx')

                if os.path.exists(results_file):
                    os.remove(results_file)

                to_upload_0 = to_upload[(to_upload['groupbrandid'] == row['store_brand_id'])]
                print(to_upload_0)
                #snow.upload_df_occ(to_upload_0,'spike_in_orders')


            else:
                print("outside product threshold")
    else:
        print('df1 null')


countries = ['br','mx','co','pe','ar','cl','ec','cr','uy']

for country in countries:
    try:
        print(country)
        a = country_query(country)
        d = store_history(country)
        a = pd.concat([a, d])
        b = store_infos(country)
        ele = query_eletronics(country)
        alarm(a, b, ele)
    except Exception as e:
        print(e)