from lib import snowflake as snow
from lib import redash, slack
from functions import timezones
import pandas as pd
from os.path import expanduser

def excluding_orders(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select distinct product_id as productid from ops_occ.orders_products_unit
where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
 and country='{country}'
 '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def country_query(country):
    timezone, interval = timezones.country_timezones(country)
    query= '''
 --no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          total_value,
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id
   FROM orders b
   WHERE (created_at >= date(now() AT TIME ZONE 'America/{timezone}'))
   
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret'))
SELECT    os.store_id::text as store_id,
          os.name,
          b.order_id,
          op.product_id,
          opd.name as product_name, 
          op.units,
          b.total_value
   FROM base b
   INNER JOIN order_stores os ON os.order_id = b.order_id
   AND (contact_email NOT LIKE '%@synth.rappi.com'
        OR contact_email IS NULL)
   left join order_product op on op.order_id=b.order_id and os.store_id=op.store_id
   left join order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     and op.sale_type not in ('WU','WW')
    
     
     group by 1,2,3,4,5,6,7
    '''.format(country=country, timezone=timezone)
    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)
    return metrics


def store_infos(country):
    query = """
    --no_cache
    select a.store_id::text as storeid, bg.brand_group_id::text as store_brand_id, bg.brand_group_name as brand_name, 
    case when v.vertical in ('Express','Licores','Farmacia','Turbo','Specialized') then 'Now'
         when v.vertical in ('Mercados') then 'Mercados'
         when v.vertical in ('Ecommerce') then 'Ecommerce' else null end as vertical
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw b on b.id=a.store_brand_id
    inner join (
    select store_type as storetype,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
    when upper (store_type) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper (vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype=a.type

    left join (
       select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
       ) bg on bg.store_brand_id = a.store_brand_id

    where a.deleted_at is null
    and not coalesce (a._fivetran_deleted, false)
    and vertical in ('Now','Mercados','Ecommerce')
    group by 1,2,3,4""".format(country=country)

    df = snow.run_query(query)
    return df

def alarm(a, b, c):

    channel_alert = timezones.slack_channels('commercial_alarms')
    df = pd.merge(a, b, how='inner', left_on=a['store_id'], right_on=b['storeid'])
    df = df.drop(['key_0'], axis=1)
    df = df[['store_id', 'order_id', 'name','product_id', 'product_name', 'total_value', 'units',
        'store_brand_id', 'brand_name', 'vertical']].rename(columns={'name':'store_name','total_value':'sum_total_value'})

    df = df[((df['vertical'].isin(['Now', 'Mercados'])) & (df['units'] >= 100)
              | (df['vertical'].isin(['Ecommerce'])) & (df['units'] >= 50)
              )]

    df1 = pd.merge(df, c, how='left', left_on=df['product_id'], right_on=c['productid'])
    df1 = df1[df1['productid'].isnull()]
    df1 = df1.drop(['key_0', 'productid'], axis=1)
    print(df1)
    if not df1.empty:
        current_time = timezones.country_current_time(country)

        df_product = df1.groupby(['store_brand_id','brand_name','product_id','product_name','vertical'])['order_id'].agg(
            ['count', ('order_id', lambda x: ', '.join(map(str, x)))]).reset_index()
        df_product = df_product[(df_product['count'] >= 2)]

        print(df_product)

        if not df_product.empty:
            for index, row in df_product.iterrows():
                df_row = df1[((df1['store_brand_id'] == row['store_brand_id']) & (df1['product_id'] == row['product_id']))]

                df_row_vertical = str(df_row.vertical.unique()).replace('[','').replace(']','').replace("'",'')
                df_row_store = str(df_row.store_id.unique()).replace('[','').replace(']','').replace("'",'')
                df_row_store_name = str(df_row.store_name.unique()).replace('[','').replace(']','').replace("'",'')
                df_row_order = str(df_row.order_id.unique()).replace('[','').replace(']','').replace("'",'').replace(" ",', ')
                df_row_product = str(df_row.product_id.unique()).replace('[','').replace(']','').replace("'",'')
                df_row_prod_name = str(df_row.product_name.unique()).replace('[','').replace(']','').replace("'",'')
                df_row_units = str(list(df_row.units)).replace('[','').replace(']','').replace("'",'')

                text = '''
                *Alarma Limitador de SKUs* :alert:
                :flag-{country}:
                *Pais*: {country}
                *Vertical*: {vertical}
                *Brand Group*: {brand_name} {store_brand_id}
                *Store ID*: {store_id}
                *Store Name*: {store_name}
                *Product ID*: {product_id}
                *Product Name*: {product_name}
                *Order ID*: {order_id}
                *Units*: {units}
                '''.format(country= country,
                           vertical=df_row_vertical,
                           brand_name = row['brand_name'],
                           store_brand_id = row['store_brand_id'],
                           store_id=df_row_store,
                           store_name=df_row_store_name,
                           order_id=df_row_order,
                           product_name=df_row_prod_name,
                           product_id=df_row_product,
                           units=df_row_units)

                print(text)

                slack.bot_slack(text,channel_alert)

                to_upload = df_row.copy()
                to_upload['alarm_at'] = current_time
                to_upload['country'] = country
                to_upload = to_upload[
                    ['store_id', 'order_id', 'store_name', 'product_id', 'product_name', 'sum_total_value',
                     'units', 'vertical', 'alarm_at', 'country']]
                print(to_upload)
                print('snowflake')

                snow.upload_df_occ(to_upload, 'orders_products_unit')
    else:
        print('null')


countries = ['mx','br','co','cl','uy','cr','ec','pe','ar']
for country in countries:
    try:
        print(country)
        a = country_query(country)
        b = store_infos(country)
        c = excluding_orders(country)
        alarm(a, b, c)
    except Exception as e:
        print(e)