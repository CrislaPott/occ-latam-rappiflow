import pandas as pd
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

def cancels(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    --no_cache
    with o as (
    select id from orders o
    where (o.PAYMENT_METHOD = 'dc' or PAYMENT_METHOD = 'pos_terminal' or PAYMENT_METHOD = 'posterminal_debitcard') 
    and o.created_at >= now() at time zone 'America/{timezone}' - interval '20 minutes'
    and o.state not in ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error')
    )
    select o.id as order_id, os.name as store_name,os.store_id
    from o
    inner join order_stores os on os.order_id = o.id and os.IS_MARKETPLACE = false
    and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
    and (os.type not in ('rappi_pay','queue_order_flow') or os.type is null)
    group by
    1,2,3
    '''

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics


def run_alarm(country):
    channel_alert = timezones.slack_channels('partner_operation')
    current_time = timezones.country_current_time(country)
    df = cancels(country)
    print(df)

    to_upload = df
    to_upload['country'] = country
    to_upload['alarm_at'] = current_time
    snow.upload_df_occ(to_upload, 'wrong_payment')

    for index, row in df.iterrows():
        rows = dict(row)
        text = '''
        *Wrong Payment Method*
        Country :flag-{country}:
        Pais: {country}
        La tienda {name} (Store ID: {store_id}) recibió el pedido {order_id} con el método de pago incorrecto
        '''.format(
            name = row['store_name'],
            store_id = row['store_id'],
            order_id = row['order_id'],
            country = country
            )

        print(text)
        slack.bot_slack(text,channel_alert)

countries = ['br','ar','uy','cl','pe','mx','co','ec','cr']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)