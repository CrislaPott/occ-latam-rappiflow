import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import slack, redash
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def country_query(country):
    timezone, interval = timezones.country_timezones(country)
    query = f"""--no_cache
    with a as (select id, state, payment_method 
    from orders 
    where created_at >= (now() at time zone 'America/{timezone}' - interval '2h')
    )
    select store_id::text as store_id, max(os.name) as store_name, count(distinct o.id) as orders, string_agg(distinct o.id::text,',') as order_ids
    from a o
    inner join order_storekeepers o_sk on o_sk.order_id=o.id
    inner join order_stores os on os.order_id = o.id and (os.is_marketplace = true or os.type in ('soat_webview')) and (contact_email not like '%@synth.rappi.com' or contact_email is null)
    where payment_method not in ('synthetic')
    group by 1
    """
    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)
    return metrics

def logs():

    query_excluding_stores = f'''
    --no_cache
    select distinct store_id::text as storeid,
    country,
    alarm_at 
    from ops_occ.rt_marketplace
    where alarm_at::date = current_date
    '''
    excluding_stores = snow.run_query(query_excluding_stores)

    return excluding_stores

def news(a,excluding_stores):
    channel_alert = timezones.slack_channels('trend_alarms')

    df = pd.merge(a,excluding_stores,how='left',left_on=a['store_id'],right_on=excluding_stores['storeid'])
    df = df[df['storeid'].isnull()]
    df = df.drop(['key_0','storeid','country','alarm_at'], axis=1)
    print(df)

    if df.shape[0]>=3:
        home = expanduser("~")
        results_file = '{}/details.xlsx'.format(home)
        df.to_excel(results_file,sheet_name='stores', index=False)
        text = '''*RT in marketplace Stores* :alert:
        :flag-{country}:
        Pais: {country}'''.format(country = country)
        print(text)
        slack.file_upload_channel(channel_alert,text,results_file,'xlsx')
        current_time = timezones.country_current_time(country)
        df['country'] = country
        df['alarm_at'] = current_time
        snow.upload_df_occ(df,'rt_marketplace')

    else:
        print('null')

countries = ['ar','cl','pe','br','uy','co','ec','cr','mx']
logs_1 = logs()
print(logs_1)
for country in countries:
    print(country)
    try:
        a = country_query(country)
        excluding_stores=logs_1[logs_1['country']==country]
        current_time = timezones.country_current_time(country).replace(tzinfo=None)
        print(current_time - excluding_stores['alarm_at'])
        excluding_stores= excluding_stores[(current_time - excluding_stores['alarm_at'])>pd.Timedelta(2,'h')]
        news(a,excluding_stores)
    except Exception as e:
        print(e)