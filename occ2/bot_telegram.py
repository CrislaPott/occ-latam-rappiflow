#!/usr/bin/env python
# coding: utf-8

# In[1]:


# !jupyter nbconvert --to script bot_telegram.ipynb


# In[1]:

def send_telegram(email_stores, email_prod, growth, d0, average, email, brand_name, complete_report, vertical, country):

	import matplotlib.pylab as plt
	from pandas.plotting import table
	from os.path import expanduser

	telegram_token = '1903200994:AAH_HycPXkAZOQCzGHtEQuoP7g-DJUGrLNw'
	URL = "https://api.telegram.org/bot{}/".format(telegram_token)
	import requests
	import json
	import os
	def get_url(url):
		response = requests.get(url,verify=True)
		content = response.content.decode("utf8")
		return content


	def get_json_from_url(url):
		content = get_url(url)
		js = json.loads(content)
		return js


	def get_updates():
		url = URL + "getUpdates"
		js = get_json_from_url(url)
		return js


	def get_last_chat_id_and_text(updates):
		num_updates = len(updates["result"])
		last_update = num_updates - 1
		text = updates["result"][last_update]["message"]["text"]
		chat_id = updates["result"][last_update]["message"]["chat"]["id"]
		return (text, chat_id)

	def send_message(text, chat_id):
		url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
		get_url(url)
		
	def f7(seq):
		seen = set()
		seen_add = seen.add
		return [x for x in seq if not (x in seen or seen_add(x))]


	# In[2]:


	#file = 'chats_id.csv'
	import pandas as pd
	import csv


	# last_updates = get_updates()
	# print(last_updates)
	# print(len(last_updates))
	# num_updates = len(last_updates["result"])
	# for i in range(0, num_updates):
	# 	print(i)
	# #     text = last_updates["result"][i]["message"]["text"]

	# 	chat_id = last_updates["result"][i]["message"]["chat"]["id"]
	# 	first_name = last_updates["result"][i]["message"]["chat"]["first_name"]
	# 	try:
	# 		last_name = last_updates["result"][i]["message"]["chat"]["last_name"]
	# 	except:
	# 		last_name =''
	# 	fields=[chat_id, first_name, last_name]
			
	# 	home = expanduser("~")
	# 	csv_file = '{}/chats_id.csv'.format(home)
	# 	with open(csv_file, 'a', newline='') as f:
	# 		writer = csv.writer(f)
	# 		writer.writerow(fields)
			
	# 	# df = pd.read_csv(csv_file)
	# 	# df.columns = ['chats_id','first_name','last_name']

	# 	# df.drop_duplicates( inplace=True)
	# 	# df.to_csv(csv_file, index= False)
	# 	# print(chat_id, first_name, last_name)


	# # In[3]:
	csv_file = 'chats_id.csv'
	print('reading file')
	df = pd.read_csv(csv_file)
	print('readed')
	print(df)
	df.columns = ['chats_id','first_name','last_name']
	df.drop_duplicates( inplace=True)
	print(df)
	#df = df.head(2)

	data = {'chats_id':[1530031754, 1169367749, 1831503285, 1470350535, 1560642005, 1840909661, 1943209330, 1563766451],
        'first_name':['Aleja',   'Aleja',    'Aleja',    'Leandro',  'Yasmin',   'Crisla',   'Pedro',    'Jorge'],
        'last_name':['Aleja',   'Aleja',    'Aleja',    'Leandro',  'Yasmin',   'Crisla',   'Pedro',    'Jorge']}
 
	c_id = pd.DataFrame(data)
	print(c_id)
	concat_ids = pd.concat([df, c_id])
	df.drop_duplicates(subset =["chats_id"], inplace = True)
	print('FINAL CONTACTS HISTORY')
	print(df)


	ids_tels = list(df['chats_id'].values)
	name_tels = list(df['first_name'].values)

	# ids_tels_base = [1530031754, 1169367749, 1831503285, 1470350535, 1560642005, 1840909661, 1943209330, 1563766451]

	# name_tels_base = ['Aleja',   'Aleja',    'Edgar',    'Leandro',  'Yasmin',   'Crisla',   'Pedro',    'Jorge']

	# from collections import OrderedDict

	# id_tels = list(OrderedDict.fromkeys( ids_tels + ids_tels_base ))
	# name_tels = list(OrderedDict.fromkeys( name_tels + name_tels_base ))

	print('ids: ' ,ids_tels)
	print('names: ',name_tels)

	def launch_bot(ids_tels, name_tels):

		ids_tels = f7(ids_tels)
		name_tels = f7(name_tels)

		print(ids_tels)
		print(name_tels)

		for idx, id_tel in enumerate(ids_tels):
			msg = "%s, reset  \n  \n \U0001F601 \n  By: Aleja \U000026A1" % (
				name_tels[idx])
			test = send_message(msg, chat_id=id_tel)
			print(msg)
			
	print(name_tels)


	# In[5]:


	# # Bot
	# ARG, COL

	URL = "https://api.telegram.org/bot{}/".format(telegram_token)

	print(telegram_token)
	#launch_bot(ids_tels, name_tels)

	# # Unir todos los procesos


	# In[6]:


	import string
	import matplotlib.pyplot as plt
	import pandas as pd
	import numpy as np
	from pandas.plotting import table
	import dataframe_image as dfi
	import six


	df = email_stores.copy()

	df['store_name'] = df['store_name'].str[:31]
	
	# fig, ax = plt.subplots(figsize=(24, 3)) 
	# ax.xaxis.set_visible(False)  
	# ax.yaxis.set_visible(False)  
	# ax.set_frame_on(False)  
	# tab = table(ax, df, loc='upper right')  
	# tab.auto_set_font_size(False)
	# tab.set_fontsize(8) 
	# home = expanduser("~")
	# results_file = '{}/dataframe.png'.format(home)
	# plt.savefig(results_file)


	home = expanduser("~")
	results_file = '{}/dataframe.png'.format(home)

	def render_mpl_table(df, col_width=3.0, row_height=0.625, font_size=14,
						header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
						bbox=[0, 0, 1, 1], header_columns=0,
						ax=None, **kwargs):
		if ax is None:
			size = (np.array(df.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
			fig, ax = plt.subplots(figsize=size)
			ax.axis('off')

		mpl_table = ax.table(cellText=df.values, bbox=bbox, colLabels=df.columns, **kwargs)

		mpl_table.auto_set_font_size(False)
		mpl_table.set_fontsize(font_size)

		for k, cell in  six.iteritems(mpl_table._cells):
			cell.set_edgecolor(edge_color)
			if k[0] == 0 or k[1] < header_columns:
				cell.set_text_props(weight='bold', color='w')
				cell.set_facecolor(header_color)
			else:
				cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
				
		fig.savefig(results_file) 
		return ax

	render_mpl_table(df, header_columns=0, col_width=5.0)

	################################################################

	df2 = email_prod.copy()

	df2['product_name'] = df2['product_name'].str[:35]
	
	
	# fig, ax = plt.subplots(figsize=(24, 3)) 
	# ax.xaxis.set_visible(False)  
	# ax.yaxis.set_visible(False)  
	# ax.set_frame_on(False)  
	# tab = table(ax, df2, loc='upper right')  
	# tab.auto_set_font_size(False)
	# tab.set_fontsize(8) 
	# home = expanduser("~")
	# results_file2 = '{}/dataframe2.png'.format(home)
	# plt.savefig(results_file2)


	home = expanduser("~")
	results_file2 = '{}/dataframe2.png'.format(home)

	def render_mpl_table(df2, col_width=3.0, row_height=0.625, font_size=14,
						header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
						bbox=[0, 0, 1, 1], header_columns=0,
						ax=None, **kwargs):
		if ax is None:
			size = (np.array(df2.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
			fig, ax = plt.subplots(figsize=size)
			ax.axis('off')

		mpl_table = ax.table(cellText=df2.values, bbox=bbox, colLabels=df2.columns, **kwargs)

		mpl_table.auto_set_font_size(False)
		mpl_table.set_fontsize(font_size)

		for k, cell in  six.iteritems(mpl_table._cells):
			cell.set_edgecolor(edge_color)
			if k[0] == 0 or k[1] < header_columns:
				cell.set_text_props(weight='bold', color='w')
				cell.set_facecolor(header_color)
			else:
				cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
				
		fig.savefig(results_file2) 
		return ax

	render_mpl_table(df2, header_columns=0, col_width=5.0)


	# In[7]:


	import telegram

	# PHOTO_PATH = 'dataframe.png'
	FILE_PATH = complete_report

	for idx, chat_id in  enumerate(list(ids_tels)):
		print(name_tels[idx])
		chat_id = int(chat_id)
		bot = telegram.Bot(token=telegram_token)
		
		
		bot.send_message(chat_id=chat_id, 
					text=f''' 
		<b>📣 Equipo OCC te Informa 📣</b>

		
		Hay una caída de ventas creadas en los ultimos 60 minutos de <b><strong>{"".join(map(str, growth))}%</strong></b> en el Group Brand <u>{"".join(brand_name)}</u> de {"".join(map(str, country))}
		
		Pedidos creados Hoy (última hora): <b>{"".join(map(str, d0))}</b> 😫 
		Promedio mismo día L2W (mismo periodo): <b>{"".join(map(str, average))}</b> 
		
		<i>Top 10 tiendas con pedidos en las últimas 2 semanas y su status en CMS: </i>
		''', parse_mode=telegram.ParseMode.HTML)
		
		bot.send_photo(chat_id=chat_id, photo=open(results_file, 'rb'))
		
		
		bot.send_message(chat_id=chat_id, 
					text=f'''
		<i>Top 15 items con mayor variación de ventas hoy Vs. últimas semanas :</i>
		''', parse_mode=telegram.ParseMode.HTML)    
		
		bot.send_photo(chat_id=chat_id, photo=open(results_file2, 'rb'))

		
		bot.send_message(chat_id=chat_id, 
					text=f'''
		Para obtener más información y ayuda, envie una msg en slack <a href="https://rappiplus.slack.com/archives/CS7UQAMLG"> occ_atendimento</a>
		''', parse_mode=telegram.ParseMode.HTML)    

		bot.send_document(chat_id=chat_id, document=open(FILE_PATH, 'rb'))


		# bot.send_message(chat_id=chat_id, 
		#              text=f'''
		# Para escalar: <a href="https://app.useavenue.com/api/escalations/58a7aa27-645b-422e-b03d-a1ffe402e254/escalate"> clic aquí</a>
		# ''', parse_mode=telegram.ParseMode.HTML)  
		

