import pandas as pd 
from os.path import expanduser
from openpyxl import load_workbook
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from time import sleep




def remove_verticals(orders, db_id,country):
       timezone, timeframe = timezones.country_timezones(country)
       if not orders.empty:
              order_list = ", ".join(map(str, orders['order_id'].to_list()))
              get_vertical_query = """ SET TIMEZONE TO 'America/{timezone}';
              select  a.order_id, b.group as vertical, a.type as order_type,c.state as is_closed_order, 
              case when place_at is not null and place_at <= now() then 'yes'
                     when place_at is null then 'yes'
                     else 'no' END AS is_past_placed_at from order_stores as a
                     join verticals as b
                     on a.store_type_group = b.store_type
                     join orders as c
                     on a.order_id = c.id
              where order_id in ({order_list})
              and state not ilike '%cancel%'
              and state not in ('finished','pending_review')
              """.format(order_list=order_list,timezone=timezone)
              verticals = redash.run_query(db_id,get_vertical_query)
              orders = orders.merge(verticals, on='order_id')
              orders = orders[(orders['vertical'] != "Restaurantes") & (orders['vertical'] != "Rappifavor") & (orders['order_type'] != "queue_order_flow") & (orders['is_past_placed_at'] != 'no')]
              orders.drop(columns=['vertical','order_type','is_closed_order','is_past_placed_at'], inplace = True)
       return orders






def send_data(results,alarm_type,timeframe):
       if not results.empty:
              results['order_id'] = results['order_id'].astype(int)
              results['order_id'] = results['order_id'].astype(str)
              results = results.groupby(['country','store_id','is_now'])['order_id'].agg(','.join).reset_index(name='order_list').merge(\
                     results.groupby(['country','store_id','is_now'])['order_id'].count().reset_index(name='orders'), \
                     on=['country','store_id','is_now'])
              if alarm_type == 'RELEASED_TO_INTEGRATION':
                     results = results[results['orders'] >= 3]
              current_time = timezones.country_current_time('co')
              to_upload = results.copy()
              to_upload['alarm_at'] = current_time
              to_upload['limbo_type'] = alarm_type
              print(to_upload)
              snow.upload_df_occ(to_upload,'limbo_alarms_events')

              for i in results['country'].unique():
                     home = expanduser("~")
                     results_file = '{}/limbo_teste_{country}.xlsx'.format(home,country = i)
                     divide_by_country = results[results['country'] == i]
                     now = divide_by_country[divide_by_country['is_now'] == 'NOW']
                     not_now = divide_by_country[divide_by_country['is_now'] == 'NOT_NOW']
                     not_now.to_excel(results_file, sheet_name = "NOT NOW", index=False)
                     book = load_workbook(results_file)
                     writer = pd.ExcelWriter(results_file, engine='openpyxl')
                     writer.book = book
                     writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
                     now.to_excel(writer, sheet_name = "NOW", index=False)
                     writer.save()
                     writer.close()
                     if alarm_type == 'RELEASED_TO_INTEGRATION': 
                            text = ':alert: *LIMBO - {alarm_type} :flag-{country}:* :alert:\n\n :warning: As seguintes orders não tem eventos novos há pelo menos {minutes} minutos.'.format(alarm_type = alarm_type.replace("_", " "), minutes=timeframe,country =i)
                            slack.file_upload_channel('C02L9BG8Z9T',text,results_file,'xlsx')
                     else:
                            text = ':alert: *TESTE DE LIMBO - {alarm_type} :flag-{country}:* :alert:\n\n :warning: As seguintes orders não tem eventos novos há pelo menos {minutes} minutos.'.format(alarm_type = alarm_type.replace("_", " "), minutes=timeframe,country =i)
                            slack.file_upload_channel('C036PQNDQ1E',text,results_file,'xlsx')
                     sleep(1)
       else:
              print("no stores")
              print(alarm_type)




limbo = """WITH BASE_ORDERS AS (   SELECT   distinct on (b.order_id) b.order_id,
                         b.id AS latest_event
                FROM     order_events b
                WHERE  b.created_at >= now() - interval '80 minutes'
                ORDER BY 1,2 DESC
                )
                ,

LIMBO_CHECKER AS (SELECT a.order_id,
       d.store_id,
       a.type,
       a.NAME,
       a.params->>'event' "event",
       a.created_at at time zone 'America/Buenos_Aires' ,
       extract(epoch FROM now() - a.created_at )/60 AS minutes_difference,
       CASE WHEN d.is_placed_now = 'true' then 'NOW' else 'NOT_NOW' end as is_now,
       CASE WHEN name in ('order_to_sh','released_to_picker','released_to_shopper','order_assigned') and extract(epoch FROM now() - a.created_at )/60 >= 40 then 'YES'
       WHEN name in ('assign_to_shopper','order_to_shopper','handshake_code_request') and extract(epoch FROM now() - a.created_at )/60 >= 25 then 'YES'
       WHEN name in ('released_to_integration') and extract(epoch FROM now() - a.created_at )/60 >= 40 then 'YES'
       ELSE 'NO' END as TRIGGERED
FROM   order_events a
JOIN BASE_ORDERS c
ON     a.order_id = c.order_id
AND    a.id = c.latest_event
JOIN order_summary d
on a.order_id = d.order_id
WHERE  type = 'event'
AND      d.state NOT ilike '%cancel%'
AND      d.state NOT IN ('finished',
                         'pending_review')
AND    NAME IN ('order_to_sh',
                'released_to_integration',
                'released_to_picker',
                'released_to_shopper',
                'assign_to_shopper',
                'order_to_shopper',
                'handshake_code_request',
                'order_assigned'
                    )
ORDER BY 1 ASC)

SELECT * From LIMBO_CHECKER
WHERE TRIGGERED = 'YES'
"""


final = pd.DataFrame()

try:
       br = redash.run_query(2447,limbo)
       if not br.empty:
              br['country'] = 'br'
              br = remove_verticals(br,1338,'br')
       final = final.append(br)
       print('BR done') 
except:
       print('BR error') 
       pass   
try: 
       ar = redash.run_query(2430,limbo)
       if not ar.empty:
              ar['country'] = 'ar'
              ar = remove_verticals(ar,1337,'ar')
       final = final.append(ar)
       print('AR done')
except Exception as e:
       print('AR error')
       print(e)
       pass
try:
       cr = redash.run_query(2552,limbo)
       if not cr.empty:
              cr['country'] = 'cr'
              cr = remove_verticals(cr,1921,'cr')
       final = final.append(cr)
       print('CR done')
except:
       print('CR error')
       pass
try:
       co = redash.run_query(2449,limbo)
       if not co.empty:
              co['country'] = 'co'
              co = remove_verticals(co,1904,'co')
       final = final.append(co)
       print('CO done')
except Exception as e:
       print('CO error')
       print(e)
       pass
try:
       cl = redash.run_query(2431,limbo)
       if not cl.empty:
              cl['country'] = 'cl'
              cl = remove_verticals(cl,1155,'cl')
       final = final.append(cl)
       print('CL done')
except:
       print('CL error')
       pass
try:
       ec = redash.run_query(2559,limbo)
       if not ec.empty:
              ec['country'] = 'ec'
              ec = remove_verticals(ec,1922,'ec')
       final = final.append(ec)
       print('EC done')
except:
       print('EC error')
       pass
try:
       mx = redash.run_query(2450,limbo)
       if not mx.empty:
              mx['country'] = 'mx'
              mx = remove_verticals(mx,1371,'mx')
       final = final.append(mx)
       print('MX done')
except Exception as e:
       print('MX error')
       print(e)
       pass
try:
       pe = redash.run_query(2448,limbo)
       if not pe.empty:
              pe['country'] = 'pe'
              pe = remove_verticals(pe,1157,'pe')
       final = final.append(pe)
       print('PE done')
except Exception as e:
       print('PE error')
       print(e)
       pass
try:
       uy = redash.run_query(2426,limbo)
       if not uy.empty:
              uy['country'] = 'uy'
              uy = remove_verticals(uy,1156,'uy')
       final = final.append(uy)
       print('UY done')
except:
       print('UY error')
       pass




order_to_shopper = final[final['name'] == 'order_to_shopper']
assign_to_shopper = final[final['name'] == 'assign_to_shopper']
order_to_sh = final[final['name'] == 'order_to_sh']
handshake_code_request = final[final['name'] == 'handshake_code_request']
released_to_integration = final[final['name'] == 'released_to_integration']
released_to_picker = final[(final['name'] == 'released_to_picker') | (final['name'] == 'released_to_shopper') | (final['name'] == 'order_assigned')]



if not order_to_shopper.empty:
    send_data(order_to_shopper, 'ORDER_TO_SHOPPER',25)
else:
    print('no order_to_shopper')

if not assign_to_shopper.empty:
    send_data(assign_to_shopper, 'ASSIGN_TO_SHOPPER',25)
else:
    print('no assign_to_shopper')

if not order_to_sh.empty:
    send_data(order_to_sh, 'ORDER_TO_SH', 40)
else:
    print('no order_to_sh')

if not handshake_code_request.empty:
    send_data(handshake_code_request, 'HANDSHAKE_CODE_REQUEST', 25)
else:
    print('no handshake_code_request')

if not released_to_integration.empty:
    send_data(released_to_integration, 'RELEASED_TO_INTEGRATION', 40)
else:
    print('no released_to_integration')

if not released_to_picker.empty:
    send_data(released_to_picker, 'RELEASED_TO_PICKER', 40)
else:
    print('no released_to_picker')