import os, sys, json
import numpy as np
import pandas as pd
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

def offenders(country):
    timezone, interval = timezones.country_timezones(country)
    query = """
     set time zone 'America/{timezone}';
     select 
     now()::timestamp as local_time,
     store_id,
     count(distinct case when type='queue_order_flow' then o.id else null end) as fake_orders,
     count(distinct case when type not in ('queue_order_flow') then o.id else null end) as real_orders,
     (count(distinct case when type not in ('queue_order_flow') then o.id else null end) - count(distinct case when type='queue_order_flow' then o.id else null end)) as diff

     from orders o 
     left join order_stores os on os.order_id=o.id
     where o.created_at >= now() - interval '15 minutes'
     and store_type_group='express'
     group by 1,2
     having (count(distinct case when type not in ('queue_order_flow') then o.id else null end) - count(distinct case when type='queue_order_flow' then o.id else null end)) >= 3
     """.format(countrt=country,timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    if country == 'br':
        query = '''
with logs as (select store_id from ops_occ.turbo_fake_orders_alarms where sync_at::date =current_date::date)
select local_time::date as day
, country
, store_id
, count(*) as alarms
, listagg(distinct to_char(local_time::timestamp_ntz,'HH:MI'), ', ') as times
, avg(real_orders) as avg_real_orders
, avg(fake_orders) as avg_fake_orders
from ops_occ.turbo_fake_orders
where local_time::date = current_date::date
and country = 'br'
and store_id not in (select * from logs)
group by 1,2,3
having alarms >= 3
'''
        df = snow.run_query(query)
    else:
        df = pd.DataFrame(data=None)

    return metrics, df


def run_alarm(a, b):
    channel_alert = timezones.slack_channels('tech_integration')

    df = a
    current_time = timezones.country_current_time(country)
    df['sync_at'] = current_time
    df['country'] = country
    print(df)

    if not df.empty:
      snow.upload_df_occ(df,'turbo_fake_orders')
    else:
      pass

    if not b.empty:
        for index, row in b.iterrows():

            text= '''*Alarm Turbo Fake Orders* :alert:
Country: {country} :flag-{country}:  
Store Id: {store_id}
Alarms con fake orders en D0: {alarms}
Promedio de orders reais: {avg_real_orders}
Promedio de fake orders: {avg_fake_orders}
Hora de las órdenes: {times}
        '''. format(
            country=country,
            store_id=row['store_id'],
            alarms=row['alarms'],
            avg_real_orders=round(row['avg_real_orders'],2),
            avg_fake_orders=round(row['avg_fake_orders'], 2),
            times=row['times'])

            print(text)
            slack.bot_slack(text, channel_alert)

        b['sync_at'] = current_time
        b['country'] = country
        b=b[['country', 'store_id', 'alarms', 'times', 'avg_real_orders','avg_fake_orders', 'sync_at']]
        print(b)
        snow.upload_df_occ(b, 'turbo_fake_orders_alarms')




countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
  try:
    print(country)
    a, b = offenders(country)
    run_alarm(a, b)

  except Exception as e:
    print(e)