import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones, snippets

def logs(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select order_id::text as orderid
    from ops_occ.fraud_replacement_rts
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)

    return df


def get_dataframe(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''
    --no_cache
    with om as (select order_id,
                       coalesce(om.params:removed_product_id, om.params:product_id) as product_id,
                       om.params:storekeeper_id as rt_id
    from {country}_core_orders_public.order_modifications_vw om
    where coalesce(om._fivetran_deleted, 'false') = false
          and created_at::date >= convert_timezone('America/Sao_Paulo',current_timestamp())::date
          and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
                        'shopper_remove_whim','support_remove_product','support_remove_whim')
        )
    , final as (
    select o.id as order_id
         , osk.storekeeper_id
         , max(op.store_id) as store_id
         , count(distinct op.product_id) as products
         , count(distinct (case when om.order_id is not null and osk.storekeeper_id is not null and 
         (ch.data ilike '%instabilidade%'or ch.data ilike '%bug%' or ch.data ilike '%erro%' or ch.data ilike '%inestabilidad%') 
         then op.product_id else null end)) as modified_products
         , modified_products/products as percentage
    from {country}_core_orders_public.orders_vw o
    left join {country}_core_orders_public.order_product_vw op on op.order_id = o.id
    left join om on om.order_id = op.order_id and om.product_id = op.product_id
    left join {country}_CORE_ORDERS_PUBLIC.order_storekeepers osk on osk.order_id=o.id and osk.storekeeper_id=om.rt_id and type = 'rappitendero'
    left join {country}_PG_MS_REALTIME_INTERFACE_PUBLIC.CHATS ch on ch.order_id = o.id
    where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/{timezone}',current_timestamp())::date
    and coalesce(o._fivetran_deleted, 'false') = false
    group by 1,2)
select order_id, store_id, listagg(distinct storekeeper_id,',') as rt_id
     , sum(products) as products, sum(modified_products) as modified_products
     , sum(modified_products)/sum(products) as percentage
from final
group by 1,2
having sum(modified_products) >= 3 and sum(modified_products)/sum(products) >= 0.50
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def alarm_rt(df1,df2):
    channel_alert = timezones.slack_channels('rt_operation')
    current_time = timezones.country_current_time(country)


    df1['order_id'] = df1['order_id'].astype(str)

    df = pd.merge(df1, df2, how='left', left_on=df1['order_id'], right_on=df2['orderid'])
    df = df[(df['orderid'].isnull())]
    df = df.drop(['key_0', 'orderid'], axis=1)
    print(df)

    df['country'] = country
    df['alarm_at'] = current_time

    snow.upload_df_occ(df, 'fraud_replacement_rts')

    if not df.empty:
        try:
            for index, row in df.iterrows():
                row = dict(row)
                text = '''*Alarma - RT fraudulento*:alert:
                País: {country} :flag-{country}:
                El pedido (Order ID: {order_id}) en la tienda (Store ID: {store_id}) tiene {percentage}% de reemplazo de producto realizado por RT (RT ID:{rt_id}).
                Total de productos elegidos por el cliente: {products}      
                Total de productos modificados por RT: {modified_products}
                '''. format(
                    order_id=row['order_id'],
                    store_id=row['store_id'],
                    percentage=round(row['percentage'], 2)*100,
                    rt_id=row['rt_id'],
                    products=row['products'],
                    modified_products=row['modified_products'],
                    country=country
                )
                print(text)
                slack.bot_slack(text, channel_alert)

        except Exception as e:
            print(e)

    else:
        print('no se aplica')


countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        df1 = get_dataframe(country)
        df2 = logs(country)
        alarm_rt(df1,df2)
    except Exception as e:
        print(e)