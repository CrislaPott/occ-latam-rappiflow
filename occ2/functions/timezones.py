import os, sys, json, requests, time, arrow
import numpy as np
import pandas as pd
from datetime import date, datetime, timedelta
import pytz

def country_timezones(country):
    if country == 'co':
        timezone = 'Bogota'
        interval = '5h'
    elif country == 'cr':
        timezone = 'Costa_Rica'
        interval = '6h'
    elif country == 'mx':
        timezone = 'Mexico_City'
        interval = '6h'
    elif country == 'cl':
        timezone = 'Santiago'
        interval = '3h'
    elif country == 'pe':
        timezone = 'Lima'
        interval = '5h'
    elif country == 'ec':
        timezone = 'Guayaquil'
        interval = '5h'
    elif country == 'br':
        timezone = 'Buenos_Aires'
        interval = '3h'
    elif country == 'uy':
        timezone = 'Montevideo'
        interval = '3h'
    elif country == 'ar':
        timezone = 'Buenos_Aires'
        interval = '3h'
        
    return timezone, interval

def country_current_time(country):
    if country == 'ar':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'cl':
        tz = pytz.timezone('America/Santiago')
        current_time = datetime.now(tz)
    elif country == 'co':
        tz = pytz.timezone('America/Bogota')
        current_time = datetime.now(tz)
    elif country == 'cr':
        tz = pytz.timezone('America/Costa_Rica')
        current_time = datetime.now(tz)
    elif country == 'ec':
        tz = pytz.timezone('America/Guayaquil')
        current_time = datetime.now(tz)
    elif country == 'br':
        tz = pytz.timezone('America/Buenos_Aires')
        current_time = datetime.now(tz)
    elif country == 'mx':
        tz = pytz.timezone('America/Mexico_City')
        current_time = datetime.now(tz)
    elif country == 'pe':
        tz = pytz.timezone('America/Lima')
        current_time = datetime.now(tz)
    elif country == 'uy':
        tz = pytz.timezone('America/Montevideo')
        current_time = datetime.now(tz)

    return current_time

## tipos de alarmes: Operación RTs   Operación Aliados   Tech- Integración   Commercial Alerts       Trend


def slack_channels(alarm_type):
    if alarm_type == 'rt_operation':
        channel_alert = 'C02L9B59BQV'
    elif alarm_type == 'partner_operation':
        channel_alert = 'C02L5JX5CT0'
    elif alarm_type == 'tech_integration':
        channel_alert = 'C02L9BG8Z9T'
    elif alarm_type == 'commercial_alarms':
        channel_alert = 'C029MJCAMS6'
    elif alarm_type == 'trend_alarms':
        channel_alert = 'C02LCB2N2AE'
    elif alarm_type == 'occ_external_alarms':
        channel_alert = 'C02LF1U8W07'

    return channel_alert