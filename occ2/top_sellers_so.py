import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones, snippets
from os.path import expanduser

def products(country):
	timezone, interval = timezones.country_timezones(country)

	query_so = ''' 
--no_cache

with base as (

select distinct country_code as country,  store_product_id as product_id
from
CPGS_SALESCAPABILITY.TBL_CORE_TOP_SELLER_AVAILABILITY_CP
where status = 'published'
and available = 'true'
and discontinued = 'false'

union all

select distinct country_code as country, product_id
from CPGS_SALESCAPABILITY.TBL_CORE_CHILD_PRODUCTS_AVAILABILITY
where is_available = 'true'
and is_discontinued = 'false'),

final as (
select om.order_id,
coalesce(om.params:removed_product_id, om.params:product_id) as product_id,
s.store_id,
coalesce(st.brand_group_name,b.name) as brand,
count(om.order_id) OVER () AS total,
count(product_id) OVER (partition by brand) as total_brand,
case when product_id in (select product_id from base where country = '{country}')
then 1 else 0 end as count
from {country}_core_orders_public.order_modifications_vw om
join {country}_core_orders_public.order_stores_vw os on os.order_id = om.order_id
left join {country}_PGLR_MS_STORES_PUBLIC.STORES_VW s on s.store_id = os.store_id
left join VERTICALS_LATAM.{country}_VERTICALS_V2 v on v.store_type = s.type
                LEFT JOIN       {country}_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_VW B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              COALESCE(ST.STORE_BRAND_ID::TEXT, ST.STORE_TYPE) = COALESCE(S.STORE_BRAND_ID::TEXT, S.TYPE)
where (coalesce(om._fivetran_deleted, 'false') = false
       and om.created_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  )
  and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
            'shopper_remove_whim','support_remove_product','support_remove_whim')
 and product_id is not null
 and v.vertical_group = 'CPGS'
 AND S.TYPE NOT IN (SELECT DISTINCT STORE_TYPE FROM OPS_OCC.TURBO_STORE_TYPE WHERE COUNTRY = '{country}')),

ult as (
select order_id,
       product_id,
       store_id,
       brand,
       total::text as total,
       total_brand::text as total_brand,
       sum(count) OVER () as total_stockout,
       sum(count) OVER (PARTITION BY BRAND) as total_brand_stockout,
       (sum(count) OVER ()) / total  as total_percentage,
       (sum(count) OVER (PARTITION BY BRAND)) / total_brand as brand_percentage,
       count
from final)

SELECT DISTINCT 'COUNTRY' AS PROCESS, BRAND, PRODUCT_ID, STORE_ID, COUNT(PRODUCT_ID) OVER (PARTITION BY PRODUCT_ID) AS PRODUCT_appearances,
                COUNT(STORE_ID) OVER (PARTITION BY STORE_ID) AS STORE_appearances, TOTAL AS TOTAL_STOCKOUTS, TOTAL_STOCKOUT AS TOTAL_STOCKOUT_TS, total_percentage AS TS_PERCENTAGE,
       LISTAGG(DISTINCT ORDER_ID, ',') within group (order by order_id) OVER (PARTITION BY PRODUCT_ID) AS ORDER_IDS
FROM ULT
WHERE COUNT = 1
AND TOTAL_PERCENTAGE >= 0.30
AND TOTAL_STOCKOUT >= 15

UNION ALL

select DISTINCT 'BRAND' AS PROCESS,  BRAND, PRODUCT_ID, STORE_ID, COUNT(PRODUCT_ID) OVER (PARTITION BY PRODUCT_ID) AS PRODUCT_appearances,
                COUNT(STORE_ID) OVER (PARTITION BY STORE_ID) AS STORE_appearances,
    TOTAL_BRAND AS TOTAL_STOCKOUTS, TOTAL_BRAND_STOCKOUT AS TOTAL_STOCKOUT_TS, BRAND_PERCENTAGE AS TS_PERCENTAGE,
       LISTAGG(DISTINCT ORDER_ID, ',') within group (order by order_id) OVER (PARTITION BY PRODUCT_ID, BRAND) AS ORDER_IDS
FROM ULT
WHERE COUNT = 1
AND brand_percentage >= 0.30
AND TOTAL_BRAND_STOCKOUT >= 10
ORDER BY PRODUCT_appearances DESC, STORE_appearances DESC
'''.format(timezone=timezone, country=country.upper())
	df = snow.run_query(query_so)
	return df

def reported_products(country):
	timezone, interval = timezones.country_timezones(country)
	query = f'''
	select distinct brand as brand_
	from ops_occ.product_ts_stockout
	where checked_at::timestamp_ntz >= dateadd(hour,-6,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
	and country = '{country}'
	'''
	df0 = snow.run_query(query)
	return df0

def run_alarm(df):
	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('occ_external_alarms')

	if not df.empty:
		df0 = reported_products(country)
		df = pd.merge(df, df0, how='left', left_on=df['brand'], right_on=df0['brand_'])
		df = df[(df['brand_'].isnull())]
		df = df.drop(['key_0', 'brand_'], axis=1)
		df = df.drop_duplicates()
		print(df)

		if not df.empty:

			df['checked_at'] = current_time
			df['country'] = country
			to_upload = df
			to_upload = to_upload[
				["country", "checked_at", "process", "brand", "product_id", "store_id", "product_appearances",
				 "store_appearances", "total_stockouts", "total_stockout_ts", "ts_percentage", "order_ids"]]

			print("uploading df")
			snow.upload_df_occ(to_upload, "product_ts_stockout")

			text = '''
			*Externo - Alarma Products/Top_Sellers - Stockout :avocado::grapes:*
			*Vertical Group CPGs*
			:flag-{country}:
			Pais: {country}
			Nuevos productos top sellers alcanzaron el threshold de stockout
			:busts_in_silhouette:<@laura.riaga> <@santiago.diazb>
			'''.format(country=country)
			print(text)
			home = expanduser("~")
			results_file = '{}/details_ts_stockout{country}.xlsx'.format(home, country=country)
			df_final = df[["process", "brand", "product_id", "store_id", "product_appearances", "store_appearances",
						   "total_stockouts", "total_stockout_ts", "ts_percentage", "order_ids"]]
			df_final.to_excel(results_file, sheet_name='stockout', index=False)
			slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

		else:
			print('df null')
	else:
		print('products null')


countries = snippets.country_list()
for country in countries:
	print(country)
	try:
		df = products(country)
		run_alarm(df)
	except Exception as e:
		print(e)
