import pandas as pd
import os
from lib import slack, redash
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz
from functions import timezones

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def excluding_products():
    query = '''  select distinct country, store_id as store, product_id as produto from ops_occ.kpis_elastic_low_prices
  where checked_at::date = current_date'''
    df = snow.run_query(query)
    return df


def get_products(country):
    if country.upper() in ['CO','MX','BR']:
        query = '''
WITH BASE AS 
(
  SELECT
    * 
  FROM
    (
(
      SELECT
        K.COUNTRY_CODE, K.TIMESTAMP, K.ID, TRY_TO_NUMBER(SPLIT_PART(K.ID, '_', 1)) AS STORE_ID, TRY_TO_NUMBER(SPLIT_PART(ID, '_', 2)) AS PRODUCT_ID, K.IS_AVAILABLE, K.PRICE, K.IN_STOCK 
      FROM
        CPGS_DATASCIENCE.LOGSTASH_PRODUCT_STORE_CONTENT AS K 
      WHERE
        K.IS_AVAILABLE IS NOT NULL 
        AND K.IN_STOCK IS NOT NULL 
        AND K.TIMESTAMP::DATE >= '2022-02-15'::DATE 
        AND (ABS(K.PRICE) = 1 OR ABS(K.PRICE) = 0)
        AND K.IS_AVAILABLE = 'TRUE' 
        AND K.IS_DISCONTINUED = 'FALSE' 
        AND K.IN_STOCK = 'TRUE' QUALIFY K.TIMESTAMP = MAX(K.TIMESTAMP) OVER (PARTITION BY K.ID, K.COUNTRY_CODE) ) 
      UNION ALL
(
      SELECT
        K.COUNTRY_CODE, K.TIMESTAMP, K.ID, TRY_TO_NUMBER(SPLIT_PART(K.ID, '_', 1)) AS STORE_ID, TRY_TO_NUMBER(SPLIT_PART(ID, '_', 2)) AS PRODUCT_ID, K.IS_AVAILABLE, K.PRICE, K.IN_STOCK 
      FROM
        CPGS_DATASCIENCE.LOGSTASH_PRODUCT_STORE_PRIORITY_CONTENT AS K 
      WHERE
        K.IS_AVAILABLE IS NOT NULL 
        AND K.IN_STOCK IS NOT NULL 
        AND K.TIMESTAMP::DATE >= '2022-02-15'::DATE 
        AND (ABS(K.PRICE) = 1 OR ABS(K.PRICE) = 0)
        AND K.IS_AVAILABLE = 'TRUE' 
        AND K.IS_DISCONTINUED = 'FALSE' 
        AND K.IN_STOCK = 'TRUE' QUALIFY K.TIMESTAMP = MAX(K.TIMESTAMP) OVER (PARTITION BY K.ID, K.COUNTRY_CODE) )
    )
    QUALIFY TIMESTAMP = MAX(TIMESTAMP) OVER (PARTITION BY ID, COUNTRY_CODE)
)
,
CP AS 
(
  SELECT DISTINCT
    '{country}' AS COUNTRY,
    VS.ID AS STORE_ID,
    PS.RETAILER_PRODUCT_ID AS PRODUCT_ID,
    ROUND(PS.PRICE) AS PRICE,
    PRO.NAME AS PRODUCT_NAME,
    S.ID AS CP_STORE_ID 
  FROM
    {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.PRODUCT AS PS 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.STORE AS S 
      ON S.ID = PS.STORE_ID 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.VIRTUAL_STORE AS VS 
      ON VS.STORE_ID = S.ID 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.RETAILER_PRODUCT AS PRO 
      ON PS.RETAILER_PRODUCT_ID = PRO.ID 
  WHERE
    PS.STATUS = 'published' 
    AND PS.IN_STOCK = 'TRUE' 
    AND COALESCE(PS._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(S._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(VS._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(PRO._FIVETRAN_DELETED, FALSE) = FALSE 
    AND ABS(PS.PRICE) >= 2 
    AND REPLACE(GET_PATH(PRO.SELL_TYPE, 'type'), '"', '') != 'WB'
)
SELECT
  B.COUNTRY_CODE AS COUNTRY,
  B.STORE_ID,
  B.PRODUCT_ID,
  CP.PRODUCT_NAME,
  ROUND(B.PRICE, 2) AS PRICE_ELASTIC,
  ROUND(CP.PRICE, 2) AS PRICE_CP,
  S.STORETYPE_,
  S.VERTICAL,
  CP.CP_STORE_ID 
FROM
  BASE B 
  JOIN
    OPS_OCC.STORE_INFOS_LATAM S 
    ON S.STORE_ID = B.STORE_ID 
    AND UPPER(S.COUNTRY) = '{country}' 
  JOIN
    CP 
    ON CP.COUNTRY = UPPER(B.COUNTRY_CODE) 
    AND CP.PRODUCT_ID = B.PRODUCT_ID 
    AND CP.STORE_ID = B.STORE_ID 
WHERE
  NOT(S.STORETYPE_ ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%')) 
  AND PRICE_CP != 0 
  AND VERTICAL IN 
  (
    'Mercados',
    'Farmacia',
    'Licores',
    'Specialized',
    'Express',
    'CPGs',
    'Ecommerce'
  )
  AND NOT(CP.PRODUCT_NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%', '%test%', '%soat%')) 
  QUALIFY COUNT(*) OVER () >= 50'''.format(country=country)
    else:
        query = '''
WITH BASE AS 
(
  SELECT
    * 
  FROM
    (
(
      SELECT
        K.COUNTRY_CODE, K.TIMESTAMP, K.ID, TRY_TO_NUMBER(SPLIT_PART(K.ID, '_', 1)) AS STORE_ID, TRY_TO_NUMBER(SPLIT_PART(ID, '_', 2)) AS PRODUCT_ID, K.IS_AVAILABLE, K.PRICE, K.IN_STOCK 
      FROM
        CPGS_DATASCIENCE.LOGSTASH_PRODUCT_STORE_CONTENT AS K 
      WHERE
        K.IS_AVAILABLE IS NOT NULL 
        AND K.IN_STOCK IS NOT NULL 
        AND K.TIMESTAMP::DATE >= '2022-02-15'::DATE 
        AND ABS(K.PRICE) = 1 
        AND K.IS_AVAILABLE = 'TRUE' 
        AND K.IS_DISCONTINUED = 'FALSE' 
        AND K.IN_STOCK = 'TRUE' QUALIFY K.TIMESTAMP = MAX(K.TIMESTAMP) OVER (PARTITION BY K.ID, K.COUNTRY_CODE) ) 
      UNION ALL
(
      SELECT
        K.COUNTRY_CODE, K.TIMESTAMP, K.ID, TRY_TO_NUMBER(SPLIT_PART(K.ID, '_', 1)) AS STORE_ID, TRY_TO_NUMBER(SPLIT_PART(ID, '_', 2)) AS PRODUCT_ID, K.IS_AVAILABLE, K.PRICE, K.IN_STOCK 
      FROM
        CPGS_DATASCIENCE.LOGSTASH_PRODUCT_STORE_PRIORITY_CONTENT AS K 
      WHERE
        K.IS_AVAILABLE IS NOT NULL 
        AND K.IN_STOCK IS NOT NULL 
        AND K.TIMESTAMP::DATE >= '2022-02-15'::DATE 
        AND ABS(K.PRICE) = 1 
        AND K.IS_AVAILABLE = 'TRUE' 
        AND K.IS_DISCONTINUED = 'FALSE' 
        AND K.IN_STOCK = 'TRUE' QUALIFY K.TIMESTAMP = MAX(K.TIMESTAMP) OVER (PARTITION BY K.ID, K.COUNTRY_CODE) )
    )
    QUALIFY TIMESTAMP = MAX(TIMESTAMP) OVER (PARTITION BY ID, COUNTRY_CODE)
)
,
CP AS 
(
  SELECT DISTINCT
    '{country}' AS COUNTRY,
    VS.ID AS STORE_ID,
    PS.RETAILER_PRODUCT_ID AS PRODUCT_ID,
    ROUND(PS.PRICE) AS PRICE,
    PRO.NAME AS PRODUCT_NAME,
    S.ID AS CP_STORE_ID 
  FROM
    {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.PRODUCT AS PS 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.STORE AS S 
      ON S.ID = PS.STORE_ID 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.VIRTUAL_STORE AS VS 
      ON VS.STORE_ID = S.ID 
    JOIN
      {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.RETAILER_PRODUCT AS PRO 
      ON PS.RETAILER_PRODUCT_ID = PRO.ID 
  WHERE
    PS.STATUS = 'published' 
    AND PS.IN_STOCK = 'TRUE' 
    AND COALESCE(PS._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(S._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(VS._FIVETRAN_DELETED, FALSE) = FALSE 
    AND COALESCE(PRO._FIVETRAN_DELETED, FALSE) = FALSE 
    AND ABS(PS.PRICE) >= 2 
    AND REPLACE(GET_PATH(PRO.SELL_TYPE, 'type'), '"', '') != 'WB'
)
SELECT
  B.COUNTRY_CODE AS COUNTRY,
  B.STORE_ID,
  B.PRODUCT_ID,
  CP.PRODUCT_NAME,
  ROUND(B.PRICE, 2) AS PRICE_ELASTIC,
  ROUND(CP.PRICE, 2) AS PRICE_CP,
  S.STORETYPE_,
  S.VERTICAL,
  CP.CP_STORE_ID 
FROM
  BASE B 
  JOIN
    OPS_OCC.STORE_INFOS_LATAM S 
    ON S.STORE_ID = B.STORE_ID 
    AND UPPER(S.COUNTRY) = '{country}' 
  JOIN
    CP 
    ON CP.COUNTRY = UPPER(B.COUNTRY_CODE) 
    AND CP.PRODUCT_ID = B.PRODUCT_ID 
    AND CP.STORE_ID = B.STORE_ID 
WHERE
  NOT(S.STORETYPE_ ILIKE ANY ('courier_hours', 'courier_sampling', '%restaurant%', '%sampling%', '%soat%')) 
  AND PRICE_CP != 0 
  AND VERTICAL IN 
  (
    'Mercados',
    'Farmacia',
    'Licores',
    'Specialized',
    'Express',
    'CPGs',
    'Ecommerce'
  )
  AND NOT(CP.PRODUCT_NAME ILIKE ANY ('%tarjeta%', '%regalo%', '%presente%', '%test%', '%soat%')) 
  QUALIFY COUNT(*) OVER () >= 50'''.format(country=country)
    df = snow.run_query(query)
    return df

def products_oa(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::int as storeid, product_id::int as productid from override_availability
            where ends_at >= now() at time zone 'America/{timezone}'
            and (in_stock = False or status = 'unpublished')
            order by ends_at desc
            '''.format(timezone=timezone)

    if country == 'ar':
      df = redash.run_query(6519, query)
    elif country == 'br':
      df = redash.run_query(6520, query)
    elif country == 'cl':
      df = redash.run_query(6521, query)
    elif country == 'co':
      df = redash.run_query(6522, query)
    elif country == 'cr':
      df = redash.run_query(6523, query)
    elif country == 'ec':
      df = redash.run_query(6524, query)
    elif country == 'mx':
      df = redash.run_query(6526, query)
    elif country == 'pe':
      df = redash.run_query(6525, query)
    elif country == 'uy':
      df = redash.run_query(6527, query)

    return df

def run_alarm(df,df_oa, df3):
    if not df.empty:
        print(df)
        if not df_oa.empty:
            df_oa['productid'] = df_oa['productid'].astype(int).astype(str)
            df_oa['storeid'] = df_oa['storeid'].astype(int).astype(str)
            df['product_id'] = df['product_id'].astype(str)
            df['cp_store_id'] = df['cp_store_id'].astype(str)
            df3['produto'] = df3['produto'].astype(str)
            df3['store'] = df3['store'].astype(str)         

            df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'cp_store_id'], right_on=['productid', 'storeid'])
            df = df[(df['productid'].isnull())]
            df = df.drop(['productid', 'storeid'], axis=1)

            df = pd.merge(df, df3, how="left", left_on=['product_id', 'cp_store_id'], right_on=['produto', 'store'])
            df = df[(df['produto'].isnull())]
            df = df.drop(['produto', 'store', 'country_y'], axis=1).rename(columns={'country_x':'country'})

            if df.shape[0] >= 50:      
                df['checked_at'] = current_time
                to_upload = df
                print("uploading df")
                snow.upload_df_occ(to_upload, "kpis_elastic_low_prices")
                count = df.shape[0]
                text = '''
                *Alarma Low Prices en Elastic*
                Country :flag-{country}:
                Pais: {country}
                Se han encontrado {count} productos con precio incorretos en elastic.
                '''.format(count = count, country = country)
                print(text)
                home = expanduser("~")
                results_file = '{}/details_kpis_elastic_low_prices.csv'.format(home)
                df_final = df[["country", "product_id", "store_id", "product_name", "price_elastic", "price_cp", "storetype_", "vertical"]]
                df_final.to_csv(results_file, index=False)
                slack.file_upload_channel('C02L5JX5CT0', text, results_file, "csv")
    else:
        print('products null')


countries = ['uy', 'pe', 'ar', 'cl', 'ec', 'cr', 'br', 'mx', 'co']
for country in countries:
    print(country)
    try:
        df = get_products(country.upper())
        df_oa = products_oa(country)
        df3 = excluding_products()
        run_alarm(df, df_oa, df3)
    except Exception as e:
        print(e)