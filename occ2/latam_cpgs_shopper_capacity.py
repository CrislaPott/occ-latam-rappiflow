import os, sys, json
import pandas as pd
import numpy as np
import gspread
from df2gspread import df2gspread as d2g
from oauth2client.service_account import ServiceAccountCredentials
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow

load_dotenv()

def empty_df_to_gsheets(df,country):
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('jsonFileFromGoogle.json', scope)
    gc = gspread.authorize(credentials)
    spreadsheet_key = '1YkJVct9hUhWHGg6UIt_sy5KZMIR4E4jQzLZZ0E1Die8'
    wks_name = country.upper()
    d2g.upload(df,spreadsheet_key, wks_name, credentials=credentials,row_names=False)
    current_time_co = datetime.utcnow() - timedelta(hours=5)
    current_time_co = current_time_co.strftime("%Y-%m-%d %H:%M")
    spreadsheet = gc.open_by_key(spreadsheet_key)
    panel = spreadsheet.get_worksheet(0)
    if country =='ar':
        cell = 'B17'
    elif country =='br':
        cell = 'B18'
    elif country =='cl':
        cell = 'B19'
    elif country =='co':
        cell = 'B20'
    elif country =='cr':
        cell = 'B21'
    elif country =='ec':
        cell = 'B22'
    elif country =='mx':
        cell = 'B23'
    elif country =='pe':
        cell = 'B24'
    elif country =='uy':
        cell = 'B25'    
    panel.update(cell, current_time_co)

def to_gsheets(dff,country):
    mappings_sp = {264: 'no hacer',265: 'no hacer',278: 'no hacer',284: 'no hacer',286: 'no hacer',335: 'no hacer',345: 'no hacer',349: 'no hacer',360: 'no hacer',369: 'no hacer',503: 'no hacer',508: 'no hacer',532: 'no hacer',582: 'no hacer',642: 'no hacer',649: 'no hacer',7571: 'no hacer',7582: 'no hacer',7583: 'no hacer',7605: 'no hacer',7610: 'no hacer',7924: 'no hacer',7925: 'no hacer',7948: 'no hacer',7952: 'no hacer',7963: 'no hacer',8001: 'no hacer',8047: 'no hacer',8706: 'no hacer',8724: 'no hacer',8762: 'no hacer',8823: 'no hacer',8833: 'no hacer',8868: 'no hacer',9130: 'no hacer',9131: 'no hacer',9138: 'no hacer',9215: 'no hacer',9553: 'no hacer',9300: 'no hacer',9301: 'no hacer',9426: 'no hacer',9552: 'no hacer',11679: 'no hacer',12147: 'no hacer',13317: 'no hacer',13789: 'no hacer',13820: 'no hacer',14257: 'no hacer',14258: 'no hacer',14264: 'no hacer',14265: 'no hacer',14267: 'no hacer',14269: 'no hacer',14409: 'no hacer',14498: 'no hacer',14991: 'no hacer',14507: 'no hacer',14515: 'no hacer',14770: 'no hacer',14909: 'no hacer',15112: 'no hacer',15152: 'no hacer',15154: 'no hacer',15272: 'no hacer',15274: 'no hacer',32139: 'no hacer',32140: 'no hacer',32141: 'no hacer',32469: 'no hacer',32807: 'no hacer',33175: 'no hacer',33188: 'no hacer',33369: 'no hacer',33370: 'no hacer',33372: 'no hacer',33410: 'no hacer',33413: 'no hacer',33414: 'no hacer',33415: 'no hacer',33416: 'no hacer',33417: 'no hacer',33423: 'no hacer',33427: 'no hacer',33428: 'no hacer',33820: 'no hacer',35289: 'no hacer',35291: 'no hacer',19: 'RJ+SP',38: 'RJ+SP',40: 'RJ+SP',46: 'RJ+SP',50: 'RJ+SP',276: 'RJ+SP',302: 'RJ+SP',304: 'RJ+SP',310: 'RJ+SP',311: 'RJ+SP',313: 'RJ+SP',340: 'RJ+SP',384: 'RJ+SP',392: 'RJ+SP',407: 'RJ+SP',433: 'RJ+SP',462: 'RJ+SP',467: 'RJ+SP',468: 'RJ+SP',520: 'RJ+SP',537: 'RJ+SP',626: 'RJ+SP',700: 'RJ+SP',726: 'RJ+SP',7577: 'RJ+SP',7578: 'RJ+SP',7593: 'RJ+SP',7597: 'RJ+SP',7600: 'RJ+SP',7601: 'RJ+SP',7602: 'RJ+SP',7612: 'RJ+SP',7842: 'RJ+SP',7876: 'RJ+SP',7923: 'RJ+SP',7947: 'RJ+SP',8157: 'RJ+SP',8162: 'RJ+SP',8188: 'RJ+SP',8396: 'RJ+SP',8397: 'RJ+SP',8717: 'RJ+SP',8718: 'RJ+SP',8723: 'RJ+SP',8725: 'RJ+SP',8726: 'RJ+SP',8727: 'RJ+SP',8730: 'RJ+SP',8764: 'RJ+SP',8778: 'RJ+SP',8780: 'RJ+SP',8781: 'RJ+SP',8784: 'RJ+SP',8785: 'RJ+SP',8786: 'RJ+SP',8787: 'RJ+SP',8789: 'RJ+SP',8791: 'RJ+SP',8793: 'RJ+SP',8813: 'RJ+SP',8816: 'RJ+SP',8819: 'RJ+SP',8820: 'RJ+SP',8821: 'RJ+SP',8822: 'RJ+SP',8874: 'RJ+SP',9132: 'RJ+SP',9133: 'RJ+SP',9134: 'RJ+SP',9135: 'RJ+SP',9137: 'RJ+SP',9139: 'RJ+SP',9140: 'RJ+SP',9177: 'RJ+SP',9178: 'RJ+SP',9180: 'RJ+SP',9181: 'RJ+SP',9205: 'RJ+SP',9218: 'RJ+SP',9221: 'RJ+SP',9222: 'RJ+SP',9223: 'RJ+SP',9224: 'RJ+SP',9225: 'RJ+SP',9417: 'RJ+SP',9420: 'RJ+SP',9454: 'RJ+SP',9504: 'RJ+SP',9514: 'RJ+SP',9536: 'RJ+SP',9597: 'RJ+SP',9597: 'RJ+SP',9641: 'RJ+SP',9649: 'RJ+SP',9658: 'RJ+SP',9710: 'RJ+SP',9741: 'RJ+SP',10076: 'RJ+SP',10464: 'RJ+SP',10542: 'RJ+SP',11260: 'RJ+SP',11277: 'RJ+SP',11278: 'RJ+SP',11374: 'RJ+SP',11889: 'RJ+SP',12047: 'RJ+SP',12106: 'RJ+SP',12246: 'RJ+SP',12823: 'RJ+SP',12824: 'RJ+SP',13179: 'RJ+SP',13240: 'RJ+SP',13313: 'RJ+SP',13981: 'RJ+SP',14224: 'RJ+SP',14244: 'RJ+SP',14246: 'RJ+SP',14259: 'RJ+SP',14260: 'RJ+SP',14261: 'RJ+SP',14263: 'RJ+SP',14271: 'RJ+SP',14394: 'RJ+SP',14395: 'RJ+SP',14518: 'RJ+SP',14519: 'RJ+SP',14750: 'RJ+SP',14768: 'RJ+SP',14849: 'RJ+SP',15205: 'RJ+SP',15243: 'RJ+SP',15794: 'RJ+SP',15990: 'RJ+SP',16012: 'RJ+SP',16014: 'RJ+SP',16020: 'RJ+SP',16025: 'RJ+SP',31957: 'RJ+SP',32043: 'RJ+SP',32251: 'RJ+SP',32252: 'RJ+SP',32470: 'RJ+SP',32471: 'RJ+SP',32803: 'RJ+SP',32804: 'RJ+SP',32805: 'RJ+SP',32806: 'RJ+SP',32831: 'RJ+SP',328: 'RJ+SP',32: 'RJ+SP',32833: 'RJ+SP',32898: 'RJ+SP',33177: 'RJ+SP',33178: 'RJ+SP',33179: 'RJ+SP',33180: 'RJ+SP',33366: 'RJ+SP',33367: 'RJ+SP',33368: 'RJ+SP',33419: 'RJ+SP',33420: 'RJ+SP',33424: 'RJ+SP',33818: 'RJ+SP',33819: 'RJ+SP',33821: 'RJ+SP',35083: 'RJ+SP',35084: 'RJ+SP',43440: 'RJ+SP',4005: 'RJ+SP',44006: 'RJ+SP'}
    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('jsonFileFromGoogle.json', scope)
    gc = gspread.authorize(credentials)
    spreadsheet_key = '1YkJVct9hUhWHGg6UIt_sy5KZMIR4E4jQzLZZ0E1Die8'
    wks_name = country.upper()
    dff.columns = [x.lower() for x in dff.keys()]
    dff['PERC_CPCT_PREENC'] = (dff['items_faltando'] / dff['capacidade'])
    dff['STATUS'] = np.where(
        dff['PERC_CPCT_PREENC'].between(0.0, 0.59, inclusive=False), 
    'Ocioso', 
     np.where(
        dff['PERC_CPCT_PREENC'].between(1, 10000000, inclusive=False), 'Saturado', 'Normal')
     )
    dff = dff.rename(columns={"physical_store_id": "PID","shopper_id": "PSID","init_break": "INICIO DESCANSO","shopper_active": "PSA","capacidade": "CPCT","missing_items_ct": "SKUS CT","missing_items_wo_ct": "SKUS SIN CT","orders_wo_ct": "ORDERS SIN CT","orders_wo_picked_items": "ORDERS SIN ITEMS","s0": "S0","s1": "S1","s2": "S2","s3": "S3","splus": "S+","horario_entrada": "HORARIO COMENZAR"})
    dff = dff[['PID','PSID','INICIO DESCANSO','PSA','CPCT','SKUS CT','SKUS SIN CT','PERC_CPCT_PREENC','STATUS','ORDERS SIN CT','ORDERS SIN ITEMS','S0','S1','S2','S3','S+','HORARIO COMENZAR']]

    if country == 'br':
        dff['Lojas_em_teste'] = dff['PID'].map(mappings_sp)

    dff = dff.iloc[:,0:18].fillna(0)
    dff.sort_values(by=['PERC_CPCT_PREENC'], inplace=True,ascending=False)
    dff['PERC_CPCT_PREENC'] = dff['PERC_CPCT_PREENC'].astype(float).map("{:.2%}".format)
    dff[["CPCT","SKUS CT","SKUS SIN CT","S0","S1","S1","S3","S+"]] = dff[["CPCT","SKUS CT","SKUS SIN CT","S0","S1","S1","S3","S+"]].astype(int)
    #d2g.upload(dff, spreadsheet_key, wks_name, credentials=credentials, row_names=False)
    spreadsheet = gc.open_by_key(spreadsheet_key)
    values = [dff.columns.values.tolist()]
    values.extend(dff.values.tolist())
    spreadsheet.values_update(wks_name, params={'valueInputOption': 'USER_ENTERED'}, body={'values': values})
    current_time_co = datetime.utcnow() - timedelta(hours=5)
    current_time_co = current_time_co.strftime("%Y-%m-%d %H:%M")
    panel = spreadsheet.get_worksheet(0)
    if country =='ar':
        cell = 'B17'
    elif country =='br':
        cell = 'B18'
    elif country =='cl':
        cell = 'B19'
    elif country =='co':
        cell = 'B20'
    elif country =='cr':
        cell = 'B21'
    elif country =='ec':
        cell = 'B22'
    elif country =='mx':
        cell = 'B23'
    elif country =='pe':
        cell = 'B24'
    elif country =='uy':
        cell = 'B25'    
    panel.update(cell, current_time_co)

def slot_capacities(country):
    query = """
select a.physical_store_id,
       case when ss.physical_stores_id is not null then capacity*2 else capacity end as capacity
from (
select s.physical_store_id, max(items_per_shopper)::int as capacity
    from {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.slots s
    where type = 'SHOPPER'
    and datetime_utc_iso >= current_date::date
    group by 1) a
left join (select distinct physical_stores_id
                     from {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.SLOT_CONFIGS_PHYSICAL_STORES
                     where slot_offset=120
               ) ss on ss.physical_stores_id=a.physical_store_id
group by 1,2""".format(country=country)
    df = snow.run_query(query)
    return df

def personal_shoppers(country):
    if country == 'ar' or country == 'cl' or country == 'uy':
        query = """
        select *
        from {country}_PG_MS_SHOPPERS_PUBLIC.shoppers
        """.format(country=country)
    else:
        query = """
        select *
        from {country}_PG_MS_SHOPPERS_MS_PUBLIC.shoppers
        """.format(country=country)

    df = snow.run_query(query)
    return df

def orders(country):
    if country == 'co' or country == 'pe' or country == 'ec':
        timezone = 'Bogota'
    elif country == 'cr' or country == 'mx':
        timezone = 'Costa_Rica'
    else:
        timezone = 'Buenos_Aires'

    query = """
    --no_cache
    select
    physical_store_id,
    shopper_id,
    sum(products_qty - osc.picked_items)::int as missing_items,
    string_agg(os.order_id::text,',') as orders,
    sum(case when os.cooking_time_started_at is not null then products_qty - osc.picked_items else 0 end)::int as missing_items_ct,
    sum(case when os.cooking_time_started_at is null then products_qty - osc.picked_items else 0 end)::int as missing_items_wo_ct,
    string_agg(distinct case when os.cooking_time_started_at is null then os.order_id::text else null end,'/') as orders_wo_ct,
    sum(case when place_at at time zone 'America/{timezone}' <= now() at time zone 'America/{timezone}' + interval '1 hour' then products_qty - osc.picked_items else 0 end)::int as s0,
    sum(case when place_at at time zone 'America/{timezone}' > now() at time zone 'America/{timezone}' + interval '1 hour' and place_at at time zone 'America/{timezone}' <= now() at time zone 'America/{timezone}' + interval '2 hour' then products_qty - osc.picked_items else 0 end)::int as s1,
    sum(case when place_at at time zone 'America/{timezone}' > now() at time zone 'America/{timezone}' + interval '2 hour' and place_at at time zone 'America/{timezone}' <= now() at time zone 'America/{timezone}' + interval '3 hour' then products_qty - osc.picked_items else 0 end)::int as s2,
    sum(case when place_at at time zone 'America/{timezone}' > now() at time zone 'America/{timezone}' + interval '3 hour' and place_at at time zone 'America/{timezone}' <= now() at time zone 'America/{timezone}' + interval '4 hour' then products_qty - osc.picked_items else 0 end)::int as s3,
    sum(case when place_at at time zone 'America/{timezone}' > now() at time zone 'America/{timezone}' + interval '4 hour' then products_qty - osc.picked_items else 0 end)::int as splus,
    string_agg(distinct case when os.cooking_time_started_at is not null and osc.picked_items = 0 then os.order_id::text else null end,'/') as orders_wo_picked_items
from
    order_summary os
left join
    order_summary_calculated osc on
        osc.order_id = os.order_id
where
    state in ('assigned', 'in_picking')
    and date(place_at at time zone 'America/{timezone}') = date(now() at time zone 'America/{timezone}')
    and shopper_id is not null
group by
    1
    , 2
    """.format(country=country,timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(2453, query)
    elif country == 'ar':
        metrics = redash.run_query(2427, query)
    elif country == 'cl':
        metrics = redash.run_query(3685, query)
    elif country == 'mx':
        metrics = redash.run_query(2454, query)
    elif country == 'uy':
        metrics = redash.run_query(2429, query)
    elif country == 'ec':
        metrics = redash.run_query(2555, query)
    elif country == 'cr':
        metrics = redash.run_query(2551, query)
    elif country == 'pe':
        metrics = redash.run_query(2452, query)
    elif country == 'br':
        metrics = redash.run_query(2451, query)

    return metrics

def online_shoppers(country):
    if country == 'co' or country == 'pe' or country == 'ec':
        timezone = 'Bogota'
    elif country == 'cr' or country == 'mx':
        timezone = 'Costa_Rica'
    else:
        timezone = 'Buenos_Aires'

    query = """
    --no_cache
with break as (
select
    shopper_id,
    case when count(*) >= 2 then min(ends_at) else null end as init_break,
    min(starts_at at time zone 'America/{timezone}') as horario_entrada
from
    shifts
where
    (starts_at at time zone 'America/{timezone}')::date = (now() at time zone 'America/{timezone}')::date
group by
    1
)
select distinct
    sl.shopper_id,
    sl.physical_store_id,
    init_break::timestamp init_break,
    horario_entrada::timestamp horario_entrada,
    case when count(distinct activated_at) > count(distinct deactivated_at) then true else false end as shopper_active
from
    shopper_physical_store_logs sl
inner join 
    break b on 
        b.shopper_id = sl.shopper_id
where
    -- activated_at is not null
    -- and deactivated_at is null
    -- and 
    activated_at >= now() - interval '1 day'
    
    group by 1,2,3,4
    """.format(country=country,timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(1971, query)
    elif country == 'ar':
        metrics = redash.run_query(1968, query)
    elif country == 'cl':
        metrics = redash.run_query(1970, query)
    elif country == 'mx':
        metrics = redash.run_query(1972, query)
    elif country == 'uy':
        metrics = redash.run_query(1974, query)
    elif country == 'ec':
        metrics = redash.run_query(2556, query)
    elif country == 'cr':
        metrics = redash.run_query(2558, query)
    elif country == 'pe':
        metrics = redash.run_query(1973, query)
    elif country == 'br':
        metrics = redash.run_query(1969, query)

    return metrics

def main(country):
    try:
        df_ord = orders(country)
        df_str = slot_capacities(country)
        df_onsp = online_shoppers(country)
        df_pk = personal_shoppers(country)
        new_df = pd.merge(df_onsp,df_pk[['id','type']],how='left',left_on=df_onsp['shopper_id'],right_on=df_pk['id'])
        df_cond = new_df[new_df['type']=='personalshopper'][['shopper_id','physical_store_id','init_break','horario_entrada','shopper_active']]
        df_cap = pd.merge(df_cond,df_str,how='left',left_on=df_cond['physical_store_id'],right_on=df_str['physical_store_id'])[['shopper_id','physical_store_id_x','capacity','init_break','horario_entrada','shopper_active']]
        df_cap = df_cap.rename(columns={"physical_store_id_x": "physical_store_id"})
        df = pd.merge(df_cap,df_ord,on=['shopper_id','physical_store_id'],how='left')[['physical_store_id','shopper_id','capacity','missing_items','missing_items_ct','missing_items_wo_ct','s0','s1','s2','s3','splus','init_break','horario_entrada','orders_wo_ct','orders_wo_picked_items','shopper_active']]
        df2 = df.iloc[:,0:11].fillna(0)
        dff = pd.merge(df2,df[['shopper_id','physical_store_id','orders_wo_ct','orders_wo_picked_items','init_break','horario_entrada','shopper_active']],on=['shopper_id','physical_store_id'],how='left')
        dff = dff[['physical_store_id','shopper_id','capacity','missing_items','missing_items_ct','missing_items_wo_ct','orders_wo_ct','orders_wo_picked_items','s0','s1','s2','s3','splus','init_break','horario_entrada','shopper_active']]
        new_columns = dff.columns.values
        new_columns[0] = 'physical_store_id'
        new_columns[1] = 'shopper_id'
        new_columns[2] = 'capacidade'
        new_columns[3] = 'items_faltando'
        dff.columns = new_columns
        to_gsheets(dff,country)
    except:
        column_names = ['PID','PSID','INICIO DESCANSO','PSA','CPCT','SKUS CT','SKUS SIN CT','PERC_CPCT_PREENC','STATUS','ORDERS SIN CT','ORDERS SIN ITEMS','S0','S1','S2','S3','S+','HORARIO COMENZAR']
        df = pd.DataFrame(columns = column_names, index=range(1))
        print(df)
        empty_df_to_gsheets(df,country)

countries = ['uy','br','co','mx','ar','cl','cr','ec','pe']
for country in countries:
    print(country)
    try:
        main(country)
    except Exception as e:
        print(e)