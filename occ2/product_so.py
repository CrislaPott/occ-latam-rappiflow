import pandas as pd
from lib import snowflake, slack
from functions import timezones
from os.path import expanduser

def stockout_store_br(country):
    timezone, interval = timezones.country_timezones(country)
    if country in ['co','mx','br']:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )
        """
    else:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id
        )
        """

    query = f"""
    --no_cache
    with om as (select order_id,coalesce(om.params:removed_product_id, om.params:product_id) as product_id
    from br_core_orders_public.order_modifications_vw om
        where (
        created_at::date >= convert_timezone('America/Sao_Paulo',current_timestamp())::date
        )
        and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
                        'shopper_remove_whim','support_remove_product','support_remove_whim')
        )

    ,base_stockout as (
    select o.id             as order_id
         , max(op.store_id) as store_id
         , op.product_id
         , p.name
         , (case
                 when
                     om.order_id is not null
                     then 1
                 else 0 end )  as stock_out
            , max(balance_price) as balance_price
            , max(price) as price

    from br_core_orders_public.orders_vw o
             join br_core_orders_public.order_product_vw op on op.order_id = o.id
             left join br_pglr_ms_grability_products_public.products_vw p on p.id = op.product_id
             left join br_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=op.store_id
         left join om on om.order_id = op.order_id and om.product_id = op.product_id
        left join br_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
left join br_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag

    where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/Sao_Paulo',current_timestamp())::date
    --and (pg.tag ilike '%black%friday%' or pg.tag ilike '%ofertas%')
    group by 1, 3, 4, 5
)

,pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
)
,

""" + query_cp + f"""
, final as (
select sb.id brand_id, sb.name as brand_name, bs.store_id::text as store_id, s.name as store_name,
         product_id::text as product_id, bs.name as product_name, balance_price, price, v.vertical,
         count(distinct case when stock_out >=1 then order_id else null end) as orders_so, count(distinct order_id) as all_orders, orders_so/all_orders as percentage_so,
         listagg(distinct case when stock_out >=1 then order_id::text else null end, ',') as order_ids
from base_stockout bs
join br_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
join br_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
join (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then
                                           case when upper (vertical_sub_group) in ('BABIES_KIDS') then 'Babies_Kids'
                                           when upper (vertical_sub_group) in ('TECH') then 'Tech'
                                           else 'Ecommerce'
                                           end
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when upper (store_type) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper (vertical_sub_group) in ('BABIES_KIDS') then 'Babies_Kids'
     when upper (vertical_sub_group) in ('TECH') then 'Tech'
     when upper(vertical_group) in ('CPGS') then 'CPGs'

else 'Others' end as vertical
from VERTICALS_LATAM.br_VERTICALS_V2 where store_type not in ('soat_webview')) v on v.storetype = s.type
where v.vertical  in ('Express','Farmacia','Licores','Mercados','CPGs','Turbo')
group by 1,2,3,4,5,6,7,8,9
having orders_so >= 2)

,
reported as (select distinct product_id::text as product_id, store_id::text as store_id
    from ops_occ.special_days_so_products 
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country = '{country}'
    group by 1,2)

, ultimo as (
select f.brand_id, f.brand_name, f.store_id, f.store_name, f.product_id, f.product_name, f.balance_price, f.price, f.vertical,
         orders_so, all_orders, percentage_so, order_ids, b.SUPER_STORE_ID, b.in_content

    from final f
    left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=f.store_id and lower(b.COUNTRY_CODE)= '{country}'
    )

select   f.brand_id, 
         f.brand_name, 
         f.store_id, 
         CASE WHEN (SUPER_STORE_ID=-1 or pscp.product_id is null) then f.store_id else SUPER_STORE_ID end as super_store_id,
         f.store_name,
         case when f.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
         f.product_id, 
         (case when f.in_content=true and pscp.product_id is not null then pscp.store_product_id else f.product_id end)::int as store_product_id,
         f.product_name, f.balance_price, f.price, f.vertical,
         orders_so, all_orders, percentage_so, order_ids
    
    from ultimo f
    left join pscms on pscms.store_id::int = f.store_id::int and pscms.product_id::int = f.product_id::int
    left join pscp on pscp.store_id::int = f.store_id::int and pscp.product_id::int = f.product_id::int
    left join reported rp on rp.store_id=f.store_id::int and rp.product_id::int=f.product_id::int

    """
    df = snowflake.run_query(query)
    return df

def run_alarm(df):
    channel_alert = timezones.slack_channels('trend_alarms')
    current_time = timezones.country_current_time(country)

    print(df)

    df['country'] = country
    df['alarm_at'] = current_time
    to_upload = df[['brand_id', 'brand_name', 'store_id', 'store_name', 'product_id','product_name', 'balance_price', 'price', 'vertical', 'orders_so',
       'all_orders', 'percentage_so', 'order_ids', 'alarm_at', 'country']]
    snowflake.upload_df_occ(to_upload,'special_days_so_products')

    df=df[['brand_id', 'brand_name', 'store_id', 'super_store_id', 'store_name', 'product_plataform', 'product_id','store_product_id', 'product_name', 'balance_price', 'price', 'vertical', 'orders_so',
       'all_orders', 'percentage_so', 'order_ids', 'country']]

    if not df.empty:
        text = '''
        Alarma - Productos ofensores Stockout *CPGs y Turbo*- Black Friday :alert:
        Country: {country} :flag-{country}:
        Nuevos productos alcanzarón el threshold de stockout!
        '''.format(country=country)

        print(text)
        home = expanduser("~")
        results_file = '{}/stockout_black_friday_cpgs.xlsx'.format(home)
        df.to_excel(results_file, sheet_name='stockout', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")


country = 'br'
try:
    df = stockout_store_br(country)
    run_alarm(df)
except Exception as e:
    print(e)