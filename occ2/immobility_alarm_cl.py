import pandas as pd
import numpy as np
import geopy.distance
from lib import slack, redash
from lib import snowflake as snow
from os.path import expanduser
from datetime import datetime
import pytz


def get_delayed_orders():
      co_orders_query = """SET TIME ZONE 'America/Santiago';

      WITH Base AS(
      SELECT storekeeper_id,
            type,
            order_id, 
            created_at,
            ROW_NUMBER() OVER(PARTITION BY storekeeper_id ORDER BY created_at DESC) AS ranking, 
            ROW_NUMBER() OVER(PARTITION BY order_id ORDER BY created_at DESC) AS ranking_order, 
            LAG(created_at) OVER(PARTITION BY storekeeper_id,order_id ORDER BY created_at ASC) AS estado_anterior,
            MAX(created_at) OVER() AS ultima_actualizacion
      FROM order_modifications 
      WHERE created_at >= 'now'::timestamp - interval '5 hours'
            AND storekeeper_id > 0),
            
      
      tienda AS (
      SELECT order_id,
            storekeeper_id
      FROM order_modifications 
      WHERE created_at >= 'now'::timestamp - interval '5 hours'
            AND storekeeper_id > 0
            AND type IN ('domiciliary_in_store','hand_to_domiciliary','delivery_to_rappitendero')) --shopper_products_delivery_to_sk

      SELECT b.storekeeper_id,
            b.type,
            b.order_id,
            b.created_at,
            'now'::timestamp - COALESCE(b.estado_anterior,b.created_at) AS minutos, 
            o.store_type_store,
            o.city_address_id,
            b.ultima_actualizacion
      FROM Base b
      LEFT JOIN calculated_information.orders o 
            ON b.order_id = o.order_id
      LEFT JOIN tienda t
            ON b.order_id = t.order_id AND b.storekeeper_id= t.storekeeper_id
      WHERE b.ranking= 1
            AND b.ranking_order = 1
            AND b.type IN ('taken_visible_order','assign_rt_hand_to_domiciliary','eta_addition','ready_for_pick_up')
            AND (b.created_at < 'now'::timestamp - interval '15 minutes' OR b.estado_anterior < 'now'::timestamp - interval '15 minutes')
            --AND o.city_address_id IN (1)
            AND t.order_id IS NULL 
            AND store_type_store NOT IN  ('courier_hours') --<> 
            AND store_type_store <> 'queue_order_flow'
      ORDER BY 5 DESC


      """
      co_orders = redash.run_query(1155, co_orders_query )
      co_orders['order_id'] = co_orders['order_id'].astype(str)
      orders_list = ", ".join(map(str, co_orders['order_id'].to_list()))
      return orders_list,co_orders[['order_id']]




def get_rt_last_lat_long(rts_list):
    get_rt_last_lat_long = """SET TIME ZONE 'America/Santiago';
    with rt_post as (select a.courier_id, max(id) as id from courier_dispatcher.courier_iterations a
where courier_id in ({rts})
and created_at >= now() - interval '20 minutes'
group by 1)

select a.courier_id as rt_id, a.id, created_at,payload->>'lat' "latitude", payload->>'lng' "longitude" from courier_dispatcher.courier_iterations a
join rt_post c
on c.courier_id = a.courier_id 
and a.id = c.id
order by a.id desc""".format(rts = rts_list)
    rt_last_locations = redash.run_query(309, get_rt_last_lat_long )
    rt_last_locations['rt_latest_loc'] = rt_last_locations['latitude'].astype(str) + ' ' + rt_last_locations['longitude'].astype(str)
    return rt_last_locations[['rt_id','rt_latest_loc']]




def get_rt_first_lat_long(orders,rts):
    get_rt_first_lat_long = """SET TIME ZONE 'America/Santiago';
    WITH order_id AS
  (
                  SELECT ROW_NUMBER() OVER() as id,*
FROM regexp_split_to_table('{order_list}' , E',') as order_id  )

,rts AS
  (
         SELECT ROW_NUMBER() OVER() as id,*
         FROM   regexp_split_to_table(       '{rt_list}'     ,E',') as rt_id)


,orders as (
  SELECT order_id::numeric as order_id,rt_id::numeric as rt_id
FROM   order_id
       JOIN rts
         ON order_id.id = rts.id)


,it_ad as (select distinct a.order_id, iteration_id, courier_id,order_data->>'to_be_taken' "taken" from courier_dispatcher.courier_order_matches a 
join orders b  
on a.order_id = b.order_id
and a.courier_id = b.rt_id
where created_at >= now() - interval '2 hour')

select b.order_id,id,created_at,payload->>'lat' "latitude", payload->>'lng' "longitude" from courier_dispatcher.courier_iterations a
join it_ad b 
on a.iteration_id = b.iteration_id 
and a.courier_id = b.courier_id
where b.taken = 'true'
order by a.id desc""".format(order_list = orders, rt_list=rts)
    rt_first_locations = redash.run_query(309, get_rt_first_lat_long )
    rt_first_locations['rt_first_loc'] = rt_first_locations['latitude'].astype(str) + ' ' + rt_first_locations['longitude'].astype(str)
    rt_first_locations.sort_values(by=['order_id','id'],ascending = False, inplace = True)
    rt_first_locations.drop_duplicates(subset='order_id', keep='first', inplace = True)
    return rt_first_locations[['order_id','rt_first_loc']]





def get_stores_loc(orders):
    get_stores_lat_long =  """select order_id, store_id, lat,lng  from order_stores
where order_id in ({orders_id})""".format(orders_id = orders)
    stores_lat_long = redash.run_query(1155,get_stores_lat_long)
    stores_lat_long['store_loc'] = stores_lat_long['lat'].astype(str) + ' ' + stores_lat_long['lng'].astype(str)
    return stores_lat_long[['order_id','store_loc']]




def get_dist(order_rt,stores_loc,rt_first_loc,rt_last_loc):
    final = order_rt.merge(stores_loc, on ='order_id',how='left')
    final = final.merge(rt_first_loc, on = 'order_id',how='left')
    final = final.merge(rt_last_loc, on = 'rt_id',how='left')
    final_errors = final[(final['rt_first_loc'].isna()) | (final['rt_latest_loc'].isna())]
    final_errors['distance_from_first_pos'] = 'NO_LAT_LONG'
    final_errors['distance_from_latest_pos'] = 'NO_LAT_LONG'
    final_errors = final_errors.fillna('NO_LAT_LONG')
    final = final[~(final['rt_first_loc'].isna()) & ~(final['rt_latest_loc'].isna())]
    final['distance_from_first_pos'] = final.apply(lambda x: geopy.distance.geodesic(x['rt_first_loc'],x['store_loc']).km, axis = 1)
    final['distance_from_latest_pos'] = final.apply(lambda x: geopy.distance.geodesic(x['rt_latest_loc'],x['store_loc']).km, axis = 1)
    final = final.append(final_errors)
    return final




def filtered_orders():
    filtered_orders  = snow.run_query("""select distinct order_id from ops_occ.occ_immobility_alarm
    where country = 'CL' """)
    return filtered_orders




def get_only_new_orders(orders, filtered):
    tz = pytz.timezone('America/Santiago')
    current_time = datetime.now(tz)
    orders['alarm_at'] = current_time
    orders['checked_at'] = orders['alarm_at'].dt.tz_localize(None) 
    orders['order_id'] = orders['order_id'].astype(int)
    try:
        filtered['order_id'] = filtered['order_id'].astype(int)
        which_orders = orders.merge(filtered, on='order_id', how='left', indicator = True)
        which_orders = which_orders[which_orders['_merge']=='left_only']
        which_orders.drop(columns='_merge', inplace = True)
    except:
        which_orders = orders
    return which_orders





def remove_bundles(orders,rt_list):
    rts_in_bundle = """SET TIME ZONE 'America/Santiago';
    with base_orders as (select distinct storekeeper_id as rt_id, order_id from order_storekeepers
where storekeeper_id in ({rts})
and is_current = true
and type = 'rappitendero'
and created_at >= now() - interval '1 hour')

select rt_id, count(distinct order_id) as active_orders from orders a
join base_orders b
on a.id = b.order_id
and state not ilike '%cancel%'
and state not in ('finished','pending_review')
group by 1""".format(rts=rt_list)
    check_for_bundle = redash.run_query(1155, rts_in_bundle)
    orders['rt_id'] = orders['rt_id'].astype(int)
    remove_bundled_orders = orders.merge(check_for_bundle, on='rt_id', how='left')
    # remove_bundled_orders = remove_bundled_orders[(remove_bundled_orders['active_orders'] == 1) | (remove_bundled_orders['active_orders'].isna())].drop_duplicates()
    remove_bundled_orders = remove_bundled_orders.fillna(1).drop_duplicates()
    co_orders = ", ".join(map(str, remove_bundled_orders['order_id'].to_list()))
    co_rts = ", ".join(map(str, remove_bundled_orders['rt_id'].to_list()))
    return co_orders, co_rts,remove_bundled_orders[['order_id','rt_id','active_orders']]




def get_order_rt(orders):
    rt_query = """select distinct storekeeper_id as rt_id, order_id from order_storekeepers
where order_id in ({order})
and is_current = true
and type = 'rappitendero'""".format(order = orders)
    rt_ids = redash.run_query(1155, rt_query)
    rt_ids['rt_id'] = rt_ids['rt_id'].astype(str)
    rt_list = ", ".join(map(str, rt_ids['rt_id'].to_list()))
    return rt_ids, rt_list



def get_rt_last_lat_long_fall_back(rts_list):
    get_rt_last_lat_long = """SET TIME ZONE 'America/Mexico_City';
    select distinct on (storekeeper_id) storekeeper_id as rt_id, id, lat as latitude, lng as longitude  from sk_positions_events
where storekeeper_id in ({rts})
and created_at >=  now() - interval '20 minutes'
order by 1,2 desc""".format(rts = rts_list)
    rt_last_locations = redash.run_query(2431, get_rt_last_lat_long )
    rt_last_locations['rt_latest_loc'] = rt_last_locations['latitude'].astype(str) + ' ' + rt_last_locations['longitude'].astype(str)
    return rt_last_locations[['rt_id','rt_latest_loc']]   


#check orders that previously triggered the alarm
previous_orders =  filtered_orders()
print("previous orders")


#get delayed orders
delayed_orders,fallback_orders = get_delayed_orders()
print("delayed orders")

try:
    #get active rts for each order
    orders, rts = get_order_rt(delayed_orders)

    #removing orders in bundle
    orders_to_check,rts_to_check,order_rt_list = remove_bundles(orders,rts)

    #get most recent rt location
    try:
        rt_last_loc = get_rt_last_lat_long(rts_to_check)
        print("latest loc rt")
    except:
        try:
            print('try fallback')
            rt_last_loc = get_rt_last_lat_long_fall_back(rts_to_check)
            print("latest loc rt")
        except Exception as e:
            print(e)
            print('fallback error')


    #get rt location when they first took the order
    rt_first_loc = get_rt_first_lat_long(orders_to_check,rts_to_check)
    print("first loc rt")


    #get order's store's location
    stores_loc = get_stores_loc(orders_to_check)
    print("store loc")


    #compare distance between rt first location and store location and between rt latest location and store location
    distances = get_dist(order_rt_list,stores_loc,rt_first_loc,rt_last_loc)
    print("distances")


    #apply thresholds: 1- RT is currently farther from the store than they were when they first took the order and is at least 6km far from the store
    #2 - RT did go farther than 100m from their location when they first took the order and is at least 100m far from the store.
    final_errors = distances[(distances['distance_from_first_pos'] == 'NO_LAT_LONG') | (distances['distance_from_latest_pos'] == 'NO_LAT_LONG')]
    final = distances[~(distances['distance_from_first_pos'] == 'NO_LAT_LONG') | ~(distances['distance_from_latest_pos'] == 'NO_LAT_LONG')]
    final = final[((final['distance_from_first_pos'] < final['distance_from_latest_pos'] ) & (final['distance_from_latest_pos'] >= 4.5)) | 
    ((abs(final['distance_from_first_pos'] - final['distance_from_latest_pos']) <= 0.1) &  (final['distance_from_latest_pos'] > 0.1))]
    final = final.append(final_errors)
except Exception as e:
    print(e)
    fallback_orders['store_loc'] = 'NO_LAT_LONG'
    fallback_orders['rt_first_loc'] = 'NO_LAT_LONG'
    fallback_orders['distance_from_first_pos'] = 'NO_LAT_LONG'
    fallback_orders['distance_from_latest_pos'] = 'NO_LAT_LONG'
    fallback_orders['active_orders'] = 'NO_LAT_LONG'
    fallback_orders['rt_latest_loc'] = 'NO_LAT_LONG'
    final = fallback_orders[['active_orders','order_id','store_loc','rt_first_loc','rt_latest_loc','distance_from_first_pos','distance_from_latest_pos']]
    print('erro')


#removing repeated orders
final = get_only_new_orders(final,previous_orders)
print("new orders")

if not final.empty:
    final_excel = final[['active_orders','order_id','store_loc','rt_first_loc','distance_from_first_pos','distance_from_latest_pos','checked_at']]
    final_excel['lupe'] =  final_excel['lupe'] =  final_excel.apply(lambda x: '=HYPERLINK("https://lupe.rappi.com/support/search-orders/cl/orderId/'+str(x['order_id'])+'","LUPE")',axis = 1)
    final_snowflake = final[['order_id','store_loc','rt_first_loc','rt_latest_loc','distance_from_first_pos','distance_from_latest_pos','alarm_at']]
    final_snowflake[['rt_first_loc','rt_latest_loc','distance_from_first_pos','distance_from_latest_pos']] = final_snowflake[['rt_first_loc','rt_latest_loc','distance_from_first_pos','distance_from_latest_pos']].astype(str)
    final_snowflake['country'] = 'CL'
    snow.upload_df_occ(final_snowflake,'occ_immobility_alarm')

    text = '''
*IMMOBILITY ALARM - TEST - :flag-cl:*
'''

    home = expanduser("~")
    results_file = '{}/immobility_alarm_orders.xlsx'.format(home)
    final_excel.to_excel(results_file, sheet_name='orders', index=False)
    slack.file_upload_channel('C035PCRR60J', text, results_file, "xlsx")
else:
    print("no new orders")
    pass

