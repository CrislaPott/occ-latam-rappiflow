import pandas as pd
import os
from lib import slack, redash
from lib import snowflake as snow
from os.path import expanduser
from datetime import datetime
import pytz
from functions import timezones


def products_with_high_discounts():
    base_high_discounted_products = snow.run_query("""
WITH VERTICAL as (select store_type as store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as sub_group
from VERTICALS_LATAM.BR_VERTICALS_V2
),

base
AS
  (
            SELECT    'BR'          AS country_code,
                      ps.id         AS product_store_id,
                      vp.id         AS virtual_product_id,
                      vp.product_id AS product_id,
                      vs.id         AS virtual_store_id,
                      s.id          AS store_id,
                      CASE
                                WHEN dc.store_id IS NULL THEN 'NO_CASHLESS'
                                ELSE 'CASHLESS'
                      end       AS delivery_condition,
                      ps.price  AS price,
                      p.id      AS parent_id,
                      ps.price  AS balance_price,
                      v."GROUP" AS vertical_group,
                      VERTICAL.SUB_GROUP AS sub_VERTICAL,
                      ca.city   AS city,
                      r.name    AS brand_name,
                      vs.store_type,
                      p.SELL_TYPE:type AS sale_type,
                      p.name,
                      CASE
                                WHEN ps.status = 'published' THEN TRUE
                                ELSE FALSE
                      end AS is_available,
                      ps.in_stock,
                      ps.retailer_markdown,
                      gop.global_offer_id,
                      --gop.MESSAGE,
                      gop.discounts,
                      gop.total_discount                                                     AS total_discount_global_offers,
                      (coalesce(gop.total_discount,0)+abs(coalesce(ps.retailer_markdown,0))) AS total_discount
            FROM      br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product ps
            JOIN      br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s
            ON        s.id = ps.store_id
            AND       NOT coalesce(s._fivetran_deleted, FALSE)
            AND       s.status='published'
            JOIN      br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.retailer r
            ON        r.id = s.retailer_id
            AND       NOT coalesce(r._fivetran_deleted, FALSE)
            AND       r.status='published'
            JOIN      br_pglr_ms_cpgs_clg_pm_public.retailer_product rp
            ON        rp.id = ps.retailer_product_id
            AND       NOT coalesce(rp._fivetran_deleted, FALSE)
            AND       rp.status ='published'
            JOIN      br_pglr_ms_cpgs_clg_pm_public.product p
            ON        p.id = rp.product_id
            AND       NOT coalesce(p._fivetran_deleted, FALSE)
            AND       p.status ='published'
            JOIN      br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs
            ON        vs.store_id = s.id
            AND       NOT coalesce(vs._fivetran_deleted, FALSE)
                      -- AND vs.status='published'
            JOIN      br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_product vp
            ON        vp.product_id = ps.id
            AND       vp.virtual_store_id = vs.id
            AND       NOT coalesce(vp._fivetran_deleted, FALSE)
                      -- AND vp.status='published'
            JOIN      br_pglr_ms_stores_public.stores_vw st
            ON        st.store_id=vs.id
            AND       NOT st._fivetran_deleted
            AND       st.is_enabled
            AND       st.deleted_at IS NULL
            LEFT JOIN
                      (
                                      SELECT DISTINCT store_id
                                      FROM            (
                                                                      SELECT DISTINCT conditionable_id AS store_id
                                                                      FROM            br_pglr_ms_stores_public.delivery_conditions dc
                                                                      WHERE           type = 'cashless_required'
                                                                      AND             PARAMS:cashless = 'true'
                                                                      AND             NOT _fivetran_deleted
                                                                      UNION ALL
                                                                      SELECT DISTINCT conditionable_id AS store_id
                                                                      FROM            br_pglr_ms_stores_public.delivery_conditions dc
                                                                      WHERE           type = 'no_pay'
                                                                      AND             PARAMS:no_pay = 'true'
                                                                      AND             NOT _fivetran_deleted ) ) dc
            ON        dc.store_id=st.store_id
            JOIN      br_pglr_ms_stores_public.verticals v
            ON        v.store_type=st.type
            AND       NOT v._fivetran_deleted
            LEFT JOIN VERTICAL 
            ON        vertical.STORE_TYPE = ST.type
            LEFT JOIN br_grability_public.city_addresses_vw ca
            ON        ca.id=st.city_address_id
            AND       NOT ca._fivetran_deleted
            JOIN      cpgs_datascience.lookup_store ls
            ON        ls.store_id = vs.id
            AND       ls.country_code = 'BR'
            LEFT JOIN
                      (
                               SELECT   gop.product_id,
                                        gop.store_id,
                                        listagg(gop.global_offer_id,',') within GROUP (ORDER BY gop.global_offer_id) AS global_offer_id,
                                        listagg(go.message,',') within GROUP (ORDER BY gop.global_offer_id)          AS message,
                                        listagg(gop.value,',') within GROUP (ORDER BY gop.global_offer_id)           AS discounts,
                                        sum(gop.value)                                                               AS total_discount
                               FROM     br_pg_ms_global_offers_public.global_offer_products gop
                               JOIN     br_pg_ms_global_offers_public.global_offers go
                               ON       go.id = gop.global_offer_id
                               JOIN     "GLOBAL_GROWTH"."CP_GO_TRACKING_1" t
                               ON       t.global_offer_id=gop.global_offer_id
                               AND      t.country='BR'
                               AND      t.total_discount_order_lc<t.budget
                               WHERE    convert_timezone('America/Bogota', CURRENT_TIMESTAMP()) BETWEEN starts_at AND      ends_at
                               AND      active = 'TRUE'
                               GROUP BY 1,
                                        2 )gop
            ON        gop.product_id=vp.id
            AND       gop.store_id=vs.id
            WHERE     NOT coalesce(ps._fivetran_deleted, FALSE)
            AND       v."GROUP" ilike any ('%CPGs%')
            AND       ps.status ='published'
            AND       ps.in_stock = TRUE
            AND       st.type NOT LIKE '%prueba%'
                      -- AND ps.price>0
            AND       ls.in_content=TRUE
                      -- and mu.store_id is null
                      --and logFP.product_id is null
            AND       NOT (
                                p.name ilike any ('%rappi%','%favor%','%soat%'))
            AND       (
                                coalesce(gop.total_discount,0)+abs(coalesce(ps.retailer_markdown,0))) > 90
            ORDER BY  (coalesce(gop.total_discount,0)         +abs(coalesce(ps.retailer_markdown,0))) DESC )
  SELECT *
  FROM   base a
""")
    final_discounted_products = base_high_discounted_products[['country_code','brand_name','virtual_store_id','product_id','product_store_id','name','vertical_group','sub_vertical','price','retailer_markdown','global_offer_id','total_discount_global_offers','total_discount']]
    final_discounted_products.rename(columns ={'country_code':'country','virtual_store_id':'store_id' ,'retailer_markdown':'markdown','vertical_group':'vertical'}, inplace = True)
    return final_discounted_products

def products_oa(country):
    # timezone, interval = timezones.country_timezones(country)
    timezone = 'Buenos_Aires'
    query = '''select store_id::int as storeid, product_id::int as productid from override_availability
    	    where ends_at >= now() at time zone 'America/{timezone}'
    	    and (in_stock = False or status = 'unpublished')
    	    order by ends_at desc
    	    '''.format(timezone=timezone)

    if country == 'ar':
      df = redash.run_query(6519, query)
    elif country == 'br':
      df = redash.run_query(6520, query)
    elif country == 'cl':
      df = redash.run_query(6521, query)
    elif country == 'co':
      df = redash.run_query(6522, query)
    elif country == 'cr':
      df = redash.run_query(6523, query)
    elif country == 'ec':
      df = redash.run_query(6524, query)
    elif country == 'mx':
      df = redash.run_query(6526, query)
    elif country == 'pe':
      df = redash.run_query(6525, query)
    elif country == 'uy':
      df = redash.run_query(6527, query)

    return df


tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def run_alarm(df,df_oa,country):

    if not df.empty:
        print(df)
        if not df_oa.empty:
          df_oa['productid'] = df_oa['productid'].astype(int).astype(str)
          df_oa['storeid'] = df_oa['storeid'].astype(int).astype(str)
          df['product_id'] = df['product_id'].astype(str)
          df['store_id'] = df['store_id'].astype(str)

          df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'store_id'], right_on=['productid', 'storeid'])
          df = df[(df['productid'].isnull()) & (df['storeid'].isnull())]
          df = df.drop(['productid', 'storeid'], axis=1)

        print(df)
        if not df.empty:

          df['checked_at'] = current_time
          to_upload = df
          to_upload = to_upload[
            ['country','brand_name','store_id','product_id','product_store_id','name','vertical','sub_vertical','price','markdown','global_offer_id','total_discount_global_offers','total_discount','checked_at']]

          print("uploading df")
          snow.upload_df_occ(to_upload, "nonlive_heavily_discounted_products")
          df['alarm_at'] = current_time
          df['alarm_at'] = df['alarm_at'].dt.tz_localize(None) 
          text = '''
          *NONLIVE - ALARMA PRODUCTOS CON ALTO DESCUENTO - MARKDOWN Y/O GLOBAL OFFER*
          '''.format(country=country)
          home = expanduser("~")
          results_file = '{}/details_global_offers_high_discounts_{country}.xlsx'.format(home, country=country)
          df_final = df[['country','brand_name','store_id','product_id','product_store_id','name','vertical','sub_vertical','price','markdown','global_offer_id','total_discount_global_offers','total_discount','alarm_at']]
          df_final.to_excel(results_file, sheet_name='products_with_discount', index=False)
          slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "xlsx")


        else:
          print('df null')
    else:
        print('products null')
        slack.bot_slack(' *NONLIVE - ALARMA PRODUCTOS CON ALTO DESCUENTO - MARKDOWN Y/O GLOBAL OFFER* \n :ok_qa: No hay produtos con problemas de descuento. :ok_qa:','C02MJQ1Q5H6')

try:
  base_products = products_with_high_discounts()
  products_override = products_oa('br')
  run_alarm(base_products, products_override,'br')
except Exception as e:
    print(e)