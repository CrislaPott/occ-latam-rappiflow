import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from lib import redash, slack, cms
from lib import snowflake as snow
from functions import timezones
import pytz

load_dotenv()

def excluding_stores(country):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	select identification_id, metric 
	from
	(select store_id::text as identification_id,
	last_value(fallbacks) over (partition by store_id order by suspended_at asc) as metric
	from ops_occ.fallback_process
	where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country='{country}'
	)
	group by 1,2
	union all 
	select store_id::text as identification_id, 1000 as metric
	from ops_occ.fallback_process
	where suspended_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and extract(hour from suspended_at) = extract(hour from convert_timezone('America/{timezone}',current_timestamp()))
	and country='{country}'
	group by 1,2
	'''.format(country=country, timezone=timezone)
	df = snow.run_query(query)
	return df

def fallback(country):
	current_time = timezones.country_current_time(country)
	timezone, interval = timezones.country_timezones(country)

	query = '''
	--no_cache
	SET TIMEZONE TO 'America/{timezone}';
	with a as (
	select order_id, 
	min(case when name in ('order_to_sh','changed_to_pre_assign') then created_at else null end) as order_to_sh_at,
	min(case when (params->'group')::varchar ilike '%release-to-rt-by-fallback%' and params->'shoppers_online' = 'false' then created_at else null end) as released_to_rt_at,
	min(case when name in ('integration_order_creation_notified') then created_at else null end) as integration_at

	from order_events
	where extract('hour' from created_at) = extract('hour' from now()::timestamp)
	and created_at >= now()::timestamp - interval '60 minutes'
	group by 1)
	, b as (
	select os.store_id, store_type_store, a.order_id, order_to_sh_at, case when released_to_rt_at is not null then 'fallback' else 'no fallback' end as has_fallback
	from a
	inner join order_summary os on os.order_id=a.order_id and os.store_type in ('express_picker')
	where order_to_sh_at is not null
	and store_id is not null
	and integration_at is null
	order by store_id asc
	)
	, c as (
	select store_id, order_id, store_type_store, order_to_sh_at, has_fallback, 
	row_number() over (partition by store_id order by order_to_sh_at asc) first_row, 
	row_number() over (partition by store_id, has_fallback order by order_to_sh_at asc) as second_row
	from b
	order by store_id asc
	)
	, d as (
	select *,  first_row-second_row as grp from c
	)
	, e as (
	select *, row_number() over (partition by store_id, grp order by order_to_sh_at asc) as row_num 
	from d
	)
	select store_id::text as store_id,store_type_store, max(row_num) as total_cancels , string_agg(distinct order_id::text, ',') as order_ids
	from e
	where has_fallback='fallback'
	group by 1,2
	having sum(row_num) >=1
	order by store_id asc
	'''.format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(2449, query)
	elif country == 'ar':
		metrics = redash.run_query(2430, query)
	elif country == 'cl':
		metrics = redash.run_query(2431, query)
	elif country == 'mx':
		metrics = redash.run_query(2450, query)
	elif country == 'uy':
		metrics = redash.run_query(2426, query)
	elif country == 'ec':
		metrics = redash.run_query(2559, query)
	elif country == 'cr':
		metrics = redash.run_query(2552, query)
	elif country == 'pe':
		metrics = redash.run_query(2448, query)
	elif country == 'br':
		metrics = redash.run_query(2447, query)

	return metrics

def store_infos(country):
	query='''
		--no_cache
	select a.store_id::text as storeid, a.name as store_name, a.city_address_id, coalesce(a.super_store_id,null)::text as mae,
	v.vertical , max(category) as category,
	listagg(distinct b.store_id::text,',') as filhas
	from {country}_PGLR_MS_STORES_PUBLIC.stores a
	left join {country}_PGLR_MS_STORES_PUBLIC.stores b on b.super_store_id=a.store_id and b.deleted_at is null and not coalesce (b._fivetran_deleted, false)
	--left join stores c on c.super_store_id=a.super_store_id and c.store_id<>a.store_id and c.deleted_at is null
	inner join (select store_type as storetype,
	case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	when vertical_group = 'WHIM' then 'Antojos'
	when vertical_group = 'RAPPICASH' then 'RappiCash'
	when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
	when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
	when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
	when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
	when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
	when upper(vertical_group) in ('CPGS') then 'CPGs'
	else 'Others' end as vertical
	from VERTICALS_LATAM.{country}_VERTICALS_V2
			   ) v on v.storetype=a.type
	left join (select store_id,
					  case when category = 'Platinum' then 'Top'
						   when category in ('Gold','Silver') then 'Mid'
					  else 'Low' end as category1, 
					  case when category1 not in ('Top','Mid','Low') then 'Custom'
					  else category1 end as category
	from ops_global.brands_tiers
	where country = '{upper_country}') t on t.store_id=a.store_id

	where a.deleted_at is null
	and not coalesce (a._fivetran_deleted, false)
	and vertical in ('Licores')
	group by 1,2,3,4,5'''.format(country=country, upper_country=country.upper())

	df = snow.run_query(query)
	return df

def offenders(a,d,e):
	channel, channel_alert = timezones.slack_channels(country)

	final_df = a
	final_df = pd.merge(final_df,d,how='inner',left_on=final_df['store_id'],right_on=d['storeid'])
	final_df = final_df.drop(['key_0','storeid'], axis=1)

	final_df = pd.merge(final_df,e,how='left',left_on=final_df['store_id'],right_on=e['identification_id'])
	final_df = final_df[(final_df['identification_id'].isnull()) | (final_df['total_cancels'] > final_df['metric'])] 
	final_df = final_df.drop(['key_0','identification_id','metric'], axis=1)

	current_time = timezones.country_current_time(country)

	target_date = (current_time + timedelta(hours=1)).replace(minute=0, second=0, microsecond=0)
	time_closed = int((target_date - current_time).total_seconds() / 60.0)
	print(target_date)
	print(time_closed)

	final_df['country'] = country
	final_df['suspended_at'] = current_time

	to_upload = final_df.rename(columns={"total_cancels": "fallbacks"})
	to_upload['suspended_at'] = current_time
	to_upload['suspended_until'] = target_date
	to_upload['country'] = country
	to_upload = to_upload[['store_id','fallbacks','suspended_at','suspended_until','country','order_ids']]

	for index, row in final_df.iterrows():
		rows = dict(row)
		reason = "fallback_to_rt"
		cms.turnStoreOff(store_id, time_closed, country, reason) == 200
		text = '''
		*Tienda suspendida por processo Fallback [Licores] - {country}*
		La Tienda {store_name} (Store ID: {store_id}) fue remitida a suspensión hasta {data} por {order_id} ordene(s) in a row con fallback en los ultimos 30 minutos
		Vertical: {vertical}
		Orders IDs: {order_ids} '''.format(
			store_id=row['store_id'],
			order_id=row['total_cancels'],
			order_ids=row['order_ids'],
			data=target_date,
			country=country,
			store_name=row['store_name'], 
			vertical=row['vertical']
			)
		print(text)
		slack.bot_slack(text2,'C01S0667VKP')
		print(to_upload)
		snow.upload_df_occ(to_upload,'fallback_process')

def run_alarm(country):
	print("excluding_stores")
	e = excluding_stores(country)
	print("getting fallback")
	a = fallback(country)
	print("store_infos")
	d = store_infos(country)
	offenders(a,d,e)

countries = ['br','ar','cl','pe','ec','uy','cr','co','mx']
for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)
