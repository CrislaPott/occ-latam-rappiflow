import os, sys, json
import pandas as pd
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def reported_products(country):
	timezone, interval = timezones.country_timezones(country)

	query = f"""
	select distinct product_id::text as product_id, store_id::text as store_id, 'no' as flag, country 
	from ops_occ.product_price_errors
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	group by 1,2,3,4
	"""
	df = snow.run_query(query)
	return df

def data(country):
	timezone, interval = timezones.country_timezones(country)

	query_latam = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.store_id != 0
	and os.type not in ('grin','rentbrella','sampling_card_rt_test','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview','mensajeria')
	and os.type not ilike '%flow%'
	and os.type not ilike 'soat%'
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price = 0)
	and op.product_id != 0
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	query_latam_3decimal = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.store_id != 0
	and os.type not in ('grin','rentbrella','sampling_card_rt_test','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview','mensajeria')
	and os.type not ilike '%flow%'
	and os.type not ilike 'soat%'
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price = 0)
	and op.product_id != 0
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	query_br = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.type not in ('grin','rentbrella','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview')
	and os.type not ilike '%flow%'
	and os.store_id != 0
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price = 0)
	and op.product_id != 0
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(1904, query_latam_3decimal)
	elif country == 'ar':
		metrics = redash.run_query(1337, query_latam_3decimal)
	elif country == 'cl':
		metrics = redash.run_query(1155, query_latam_3decimal)
	elif country == 'mx':
		metrics = redash.run_query(1371, query_latam_3decimal)
	elif country == 'uy':
		metrics = redash.run_query(1156, query_latam)
	elif country == 'ec':
		metrics = redash.run_query(1922, query_latam)
	elif country == 'cr':
		metrics = redash.run_query(1921, query_latam)
	elif country == 'pe':
		metrics = redash.run_query(1157, query_latam)
	elif country == 'br':
		metrics = redash.run_query(1338, query_br)

	return metrics

def offenders(df,rp):
	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('partner_operation')

	print(df)
	if not df.empty:
		merged = pd.merge(df, rp, how='left', on=['product_id','store_id'])
		news = merged[merged['flag'].isnull()]
		print('news')
		print(news)

		final = news[['product_id','product_name','store_id','name','type','orders','unit_price_in_order','order_ids']]
		to_upload = final[['product_id','store_id','unit_price_in_order','order_ids']]
		to_upload['alarm_at'] = current_time
		to_upload['country'] = country

		to_upload = to_upload[['product_id','store_id','unit_price_in_order','alarm_at','country','order_ids']]

		snow.upload_df_occ(to_upload, 'product_price_errors')

		if not final.empty:
			home = expanduser("~")
			results_file = '{}/products0.xlsx'.format(home)
			final.to_excel(results_file,sheet_name='produts', index=False)
			if country in ['mx','cl','co','ar']:
				text='''
				*:smiling_imp: Producto con precio = $0 :exclamation: :exclamation: :exclamation: *
				Pais: {country} :flag-{country}:
				:alert:
				Los productos en adjunto recibieron pedidos con un valor cero*'''.format(country=country)
			else:
				text = '''
				*:smiling_imp:  Producto con precio = $0 :exclamation: :exclamation: :exclamation: *
				Pais: {country} :flag-{country}:
				:alert:
				Los productos en adjunto recibieron pedidos con un valor cero*'''.format(country=country)
			print(text)
			slack.file_upload_channel(channel_alert, text, results_file, 'xlsx')

			if os.path.exists(results_file):
				os.remove(results_file)
		else:
			print("final null")
	else:
		print('df null')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
	print(country)
	try:
		df = data(country)
		rp = reported_products(country)
		offenders(df,rp)
	except Exception as e:
		print(e)