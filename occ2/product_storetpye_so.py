import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def stockout(country):
    timezone, interval = timezones.country_timezones(country)
    current_time = timezones.country_current_time(country)
    channel_alert = timezones.slack_channels('trend_alarms')

    if country in ['co','mx','br']:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )
        """
    else:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

        where p.id=1006
        )
        """
    if country in ['br']:
        query_so = f"""
    --no_cache
with om as (select order_id,coalesce(om.params:removed_product_id, om.params:product_id) as product_id
from {country}_core_orders_public.order_modifications_vw om
where (coalesce(om._fivetran_deleted, 'false') = false
           and created_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
    )
  and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
                        'shopper_remove_whim','support_remove_product','support_remove_whim')
    )
,base_stockout as (
    select o.id as order_id
         , max(op.store_id) as store_id
         , op.product_id
         , (case when om.order_id is not null then 1 else 0 end )  as stock_out
    from {country}_core_orders_public.orders_vw o
    join {country}_core_orders_public.order_product_vw op on op.order_id = o.id
    left join om on om.order_id = op.order_id and om.product_id = op.product_id

    where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/{timezone}',current_timestamp())::date
    group by 1,3,4
    )

, final as (
select   sb.id brand_id,
         sb.name as brand_name, 
         s.type as store_type,
         bs.product_id, 
         max(p.name) as product_name, 
         max(balance_price) as balance_price, 
         max(price) as price, 
         v.vertical,
         count(distinct case when stock_out >=1 then order_id else null end) as orders_so,
         count(distinct order_id) as all_orders,
         orders_so/all_orders as percentage_so,
         listagg(distinct bs.store_id::text,',') as store_ids
from base_stockout bs
join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
join (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type = 'turbo' then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2 where store_type not in ('soat_webview','turbo')) v on v.storetype = s.type
join {country}_pglr_ms_grability_products_public.products_vw p on p.id = bs.product_id and sale_type not in ('WW')
join {country}_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=bs.store_id
left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag

where v.vertical in ('Ecommerce')
and s.type in ('oboticarioecomm','quemdisseb','thegoodies','tetecastanha_flores','foundit_shopper','kopenhagennovo','brasilcacau','lindt_shopper',
'brasilcacau','provence_loja','nestle_ecommerce','nestle_nespresso_market','nestle_nespresso_express','nestle_nespresso','nestle_nespresso_natal',
'bresil_loja','granado_enc','multicoisas','sephora_enc','thebodyshop','rihappy','beautybox_enc','lebiscuit','kalunga_enc','gimba')
group by 1,2,3,4,8
having orders_so >= 1)

,
pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
)
,

wh as (
    select distinct p.id::text as productid
    from {country}_pglr_ms_grability_products_public.products_vw p
    inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='{country}'
    )
,
reported_products as (
    select distinct product_id as productid
    from ops_occ.product_brand_stockout
    where checked_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country = '{country}'
)
,
"""
    else:
        query_so = f"""
     --no_cache
with om as (select order_id,coalesce(om.params:removed_product_id, om.params:product_id) as product_id
from {country}_core_orders_public.order_modifications_vw om
where (coalesce(om._fivetran_deleted, 'false') = false
and created_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
)
and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
                                        'shopper_remove_whim','support_remove_product','support_remove_whim')
 )
,base_stockout as (
select o.id as order_id
, max(op.store_id) as store_id
, op.product_id
, (case when om.order_id is not null then 1 else 0 end )  as stock_out
from {country}_core_orders_public.orders_vw o
join {country}_core_orders_public.order_product_vw op on op.order_id = o.id
   left join om on om.order_id = op.order_id and om.product_id = op.product_id

   where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/{timezone}',current_timestamp())::date
   group by 1,3,4
   )

, final as (
select   sb.id brand_id,
        sb.name as brand_name, 
        s.type as store_type,
        bs.product_id, 
        max(p.name) as product_name, 
        max(balance_price) as balance_price, 
        max(price) as price, 
        v.vertical,
        count(distinct case when stock_out >=1 then order_id else null end) as orders_so,
        count(distinct order_id) as all_orders,
        orders_so/all_orders as percentage_so,
        listagg(distinct bs.store_id::text,',') as store_ids
from base_stockout bs
join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
join (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when store_type = 'turbo' then 'Turbo'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2 where store_type not in ('soat_webview','turbo')) v on v.storetype = s.type
join {country}_pglr_ms_grability_products_public.products_vw p on p.id = bs.product_id and sale_type not in ('WW')
join {country}_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=bs.store_id
left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag

where v.vertical in ('Ecommerce')
group by 1,2,3,4,8
having orders_so >= 2 and percentage_so >= 0.10)

,
pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
)
,

wh as (
   select distinct p.id::text as productid
   from {country}_pglr_ms_grability_products_public.products_vw p
   inner join ops_occ.CORE_PRODUCT_TOP_SELLERS_V2 whitelist on whitelist.parent_id = p.parent_id and lower(country_code)='{country}'
   )
,
reported_products as (
   select distinct product_id as productid
   from ops_occ.product_brand_stockout
   where checked_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
   and country = '{country}'
)
,
"""
    query_so = query_so + query_cp + f"""

, final2 as (
select brand_id, brand_name, f.store_type, f.product_id, product_name, f.balance_price, f.price, f.vertical, orders_so, all_orders, percentage_so, b.SUPER_STORE_ID, ps.store_id as store_id, b.in_content
from final f
join {country}_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=f.product_id
join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=ps.store_id and lower(b.COUNTRY_CODE)= '{country}'
where s.deleted_at is null and s.suggested_state=true
)

select f.store_type,
       brand_id,
       brand_name,
       case when f.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       f.product_id,
       case when f.in_content=true and pscp.product_id is not null then pscp.store_product_id else f.product_id end as store_product_id,
       product_name,
       f.balance_price,
       f.price,
       vertical,
       orders_so,
       all_orders,
       percentage_so,
       CASE WHEN (SUPER_STORE_ID=-1 or pscp.product_id is null) then f.store_id else SUPER_STORE_ID end as store_ids
       from final2 f
       left join pscms on pscms.store_id::int = f.store_id::int and pscms.product_id::int = f.product_id::int
       left join pscp on pscp.store_id::int = f.store_id::int and pscp.product_id::int = f.product_id::int
       left join reported_products rp on rp.productid::int = f.product_id::int
       left join wh on wh.productid::int = f.product_id::int
       where rp.productid is null and wh.productid is null
       """

    df = snow.run_query(query_so)
    df = df[(df['vertical'] != 'Turbo')]

    df = df[df['vertical'].isin(['Ecommerce'])]

    df['checked_at'] = current_time
    df['country'] = country
    df['store_id'] = 0
    to_upload = df
    to_upload = to_upload[
        ["brand_id", "brand_name", "product_id", "product_name", "orders_so", "all_orders", "percentage_so",
         "checked_at", "country", "store_id", "store_ids"]]

    print("uploading df")
    snow.upload_df_occ(to_upload, "product_brand_stockout")

    print(df)
    if not df.empty:
        df_ecommerce = df[
            ["store_type", "brand_id", "brand_name", "vertical", "store_ids", "product_plataform", "product_id",
             "store_product_id", "product_name", "balance_price", "price", "orders_so", "all_orders", "percentage_so"]]
        home = expanduser("~")
        results_file2 = '{home}/details_ecommerce{country}.xlsx'.format(home=home, country=country)
        df_ecommerce.to_excel(results_file2,index=False)
        text = f'''
            *Alarma Product/Store Type - Stockout :alert:*
            *Vertical Group Ecommerce*
            :flag-{country}:
            Pais: {country}
            Nuevos productos alcanzaron el threshold de stockout
            '''
        print(text)
        slack.file_upload_channel(channel_alert, text, results_file2, 'xlsx')

        if os.path.exists(results_file2):
            os.remove(results_file2)


countries = ['uy', 'pe', 'ar', 'cl', 'ec', 'cr', 'br', 'mx', 'co']
for country in countries:
    print(country)
    try:

        stockout(country)
    except Exception as e:
        print(e)


