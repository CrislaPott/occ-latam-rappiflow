import pandas as pd
from lib import snowflake, slack, redash
from functions import timezones
from os.path import expanduser

new_google_sheet_url = "https://docs.google.com/spreadsheets/d/197xBS8yOElM_LSkY9nY6WfjS1L-RErsfLLPqlNE0luw/export?format=csv&gid=0"
df_sheets = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
df_sheets.columns = ['country', 'storetype', 'threshold']
countries = list(dict.fromkeys(df_sheets['country'].to_list()))
print(countries)
df_sheets.country = df_sheets.country.str.lower()


def stockout_store_br(country):
    timezone, interval = timezones.country_timezones(country)
    if country in ['co','mx','br']:
        query_cp = f""" 
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )
        """
    else:
        query_cp = f""" 
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id
        )
        """

    query = f"""
    --no_cache
    with om as (select order_id,coalesce(om.params:removed_product_id, om.params:product_id) as product_id
    from {country}_core_orders_public.order_modifications_vw om
        where (
        created_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
        )
        and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
                        'shopper_remove_whim','support_remove_product','support_remove_whim')
        )

    ,base_stockout as (
    select o.id             as order_id
         , max(op.store_id) as store_id
         , op.product_id
         , p.name
         , (case
                 when
                     om.order_id is not null
                     then 1
                 else 0 end )  as stock_out
            , max(balance_price) as balance_price
            , max(price) as price

    from {country}_core_orders_public.orders_vw o
             join {country}_core_orders_public.order_product_vw op on op.order_id = o.id
             left join {country}_pglr_ms_grability_products_public.products_vw p on p.id = op.product_id
             left join {country}_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=op.store_id
         left join om on om.order_id = op.order_id and om.product_id = op.product_id
        left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
left join {country}_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag

    where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/{timezone}',current_timestamp())::date
    --and (pg.tag ilike '%black%friday%' or pg.tag ilike '%ofertas%')
    group by 1, 3, 4, 5
)

,pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
)
,

""" + query_cp + f""" 

, final as (
select sb.id brand_id, sb.name as brand_name, bs.store_id::text as store_id, s.name as store_name,
         product_id::text as product_id, bs.name as product_name, balance_price, price,v.storetype, v.vertical,
         count(distinct case when stock_out >=1 then order_id else null end) as orders_so, count(distinct order_id) as all_orders, orders_so/all_orders as percentage_so,
         listagg(distinct case when stock_out >=1 then order_id::text else null end, ',') as order_ids
from base_stockout bs
join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
join (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then
                                           case when upper (vertical_sub_group) in ('BABIES_KIDS') then 'Babies_Kids'
                                           when upper (vertical_sub_group) in ('TECH') then 'Tech'
                                           else 'Ecommerce'
                                           end
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when upper (store_type) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper (vertical_sub_group) in ('BABIES_KIDS') then 'Babies_Kids'
     when upper (vertical_sub_group) in ('TECH') then 'Tech'
     when upper(vertical_group) in ('CPGS') then 'CPGs'

else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2 where store_type not in ('soat_webview')) v on v.storetype = s.type
where v.vertical  in ('Ecommerce')
group by 1,2,3,4,5,6,7,8,9,10
)

, 
reported as (select distinct product_id::text as product_id, store_id::text as store_id
    from ops_occ.special_days_so_products 
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country = '{country}'
    group by 1,2)


,wh as (select retailer_product_id::text as productid
       from CPGS_SALESCAPABILITY.TBL_CORE_PRODUCTS_IDEAL_BASKET_BY_STORE_CP
       where lower(country_code)= '{country}'
       and is_covered=true

       )

, ultimo as (
select f.brand_id, f.brand_name, f.store_id, f.store_name, f.product_id, f.product_name, f.balance_price, f.price,f.storetype, f.vertical,
         orders_so, all_orders, percentage_so, order_ids, b.SUPER_STORE_ID, b.in_content

    from final f
    left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=f.store_id and lower(b.COUNTRY_CODE)= '{country}'
    )

select   f.brand_id, 
         f.brand_name, 
         f.store_id, 
         (CASE WHEN (SUPER_STORE_ID=-1 or pscp.product_id is null) then f.store_id else SUPER_STORE_ID end)::int as super_store_id,
         f.store_name,
         case when f.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
         f.product_id, 
         (case when f.in_content=true and pscp.product_id is not null then pscp.store_product_id else f.product_id end)::int as store_product_id,
         f.product_name, f.balance_price, f.price, f.storetype, f.vertical,
         orders_so, all_orders, percentage_so, order_ids
    
    from ultimo f
    left join pscms on pscms.store_id::int = f.store_id::int and pscms.product_id::int = f.product_id::int
    left join pscp on pscp.store_id::int = f.store_id::int and pscp.product_id::int = f.product_id::int
    left join reported rp on rp.store_id=f.store_id::int and rp.product_id::int=f.product_id::int
    left join wh on wh.productid::int = f.product_id::int 

    where rp.product_id is null and wh.productid is null
    """
    df = snowflake.run_query(query)

    query_oa = '''select store_id::int as storeid, product_id::int as productid from override_availability
    where ends_at >= now() at time zone 'America/{timezone}'
    and status in ('unpublished')
    order by ends_at desc
    '''.format(timezone=timezone)

    if country == 'ar':
        df_oa = redash.run_query(6519, query_oa)
    elif country == 'br':
        df_oa = redash.run_query(6520, query_oa)
    elif country == 'cl':
        df_oa = redash.run_query(6521, query_oa)
    elif country == 'co':
        df_oa = redash.run_query(6522, query_oa)
    elif country == 'cr':
        df_oa = redash.run_query(6523, query_oa)
    elif country == 'ec':
        df_oa = redash.run_query(6524, query_oa)
    elif country == 'mx':
        df_oa = redash.run_query(6526, query_oa)
    elif country == 'pe':
        df_oa = redash.run_query(6525, query_oa)
    elif country == 'uy':
        df_oa = redash.run_query(6527, query_oa)

    return df, df_oa

def run_alarm(df,df1,df_oa):
    channel_alert = timezones.slack_channels('trend_alarms')
    current_time = timezones.country_current_time(country)
    store_type = df1[['storetype', 'threshold']]
    store_type.storetype = store_type.storetype.str.strip()
    df = pd.merge(df,store_type,how='inner',on=['storetype'])
    df= df[df['orders_so'] >= df['threshold']]
    print(df)
    if not df_oa.empty:
        df_oa['productid'] = df_oa['productid'].astype(int).astype(str)
        df_oa['storeid'] = df_oa['storeid'].astype(int).astype(str)
        df['product_id'] = df['product_id'].astype(str)
        df['store_id'] = df['store_id'].astype(str)

        df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'store_id'], right_on=['productid', 'storeid'])
        df = df[(df['productid'].isnull()) & (df['storeid'].isnull())]
        df = df.drop(['productid', 'storeid'], axis=1)

    print(df)
    df['country'] = country
    df['alarm_at'] = current_time
    to_upload = df[['brand_id', 'brand_name', 'store_id', 'store_name', 'product_id','product_name', 'balance_price', 'price', 'vertical', 'orders_so',
       'all_orders', 'percentage_so', 'order_ids', 'alarm_at', 'country']]
    snowflake.upload_df_occ(to_upload,'special_days_so_products')

    df=df[['brand_id', 'brand_name', 'store_id', 'super_store_id', 'store_name', 'product_plataform', 'product_id','store_product_id', 'product_name', 'balance_price', 'price','storetype', 'vertical', 'orders_so',
       'all_orders', 'percentage_so', 'order_ids', 'country']]

    if not df.empty:
        text = '''
        *Productos ofensores Stockout Ecommerce* 
        :alert:
        Country: {country} :flag-{country}:
        Nuevos productos alcanzarón el threshold de stockout!
        '''.format(country=country)

        print(text)
        home = expanduser("~")
        results_file = '{}/stockout_ecommerce.xlsx'.format(home)
        df.to_excel(results_file, sheet_name='stockout', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")


for country in countries:
    country = country.lower()
    try:
        print(country)
        df1 = df_sheets[df_sheets.country == country]
        df, df_oa = stockout_store_br(country)
        run_alarm(df,df1,df_oa)
    except Exception as e:
        print(e)