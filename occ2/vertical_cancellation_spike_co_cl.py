#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# get_ipython().system('jupyter nbconvert --to script okr_availability.ipynb')


# In[1]:


from vertical_cancellation_spike_co_cl_function import process


# In[5]:


channel = [
    "C02L5JX5CT0",
    "C02L5JX5CT0",
]
for index, country in enumerate(["CO", "CL"]):
    try:
        process(country=country, canal=channel[index])  # canal-mio = 'U026TUGHJTV'
    except Exception as e:
        print(e)
