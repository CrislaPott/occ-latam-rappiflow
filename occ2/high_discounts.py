from lib import snowflake as snow
from lib import slack
from functions import timezones
import pandas as pd
from os.path import expanduser

def logs(country):
  timezone, interval = timezones.country_timezones(country)
  query_brand = '''
  select distinct store_brand_id as storebrandid from ops_occ.high_discounts_brand
  where country = '{country}'
  and alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  and alarm_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '4h'
  '''.format(country=country, timezone=timezone)

  query_physical = '''
(select distinct physical_store_id as physicalstoreid from ops_occ.high_discounts_physical
  where country = '{country}'
  and alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  and alarm_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '4h'
  )
  '''.format(country=country, timezone=timezone)

  df1 = snow.run_query(query_physical)
  df2 = snow.run_query(query_brand)

  return df1,df2

def discounts(country):
    timezone, interval = timezones.country_timezones(country)

    if country in ['co','mx','br']:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )
        """
    else:
        query_cp = f""" 
        --no_cache
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join {country}_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

        )
        """

    query = f'''with VERTICAL as (select store_type as store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as sub_group
from VERTICALS_LATAM.{country}_VERTICALS_V2
),

ids as (
        select a.id,
               max(total_value) as total_value,
               sum(total_price) as total_price,
               s.store_id,
               s.name,
               listagg(b.product_id,', ') as product_id,
               listagg(opd.name,', ') as product_name,
               sub_group as vertical,
               a.created_at
        from {country}_core_orders_public.orders_vw a
            LEFT join {country}_core_orders_public.order_product_vw B
                on a.id = b.order_id
            join {country}_CORE_ORDERS_PUBLIC.order_product_detail opd on opd.order_product_id=b.id
            LEFT JOIN {country}_CORE_ORDERS_PUBLIC.ORDER_STORES_VW OS
                ON         OS.ORDER_ID = A.ID
            LEFT JOIN {country}_PGLR_MS_STORES_PUBLIC.STORES_VW S
                ON         S.STORE_ID = OS.STORE_ID
            JOIN vertical
                ON vertical.STORE_TYPE = S.type
        where COALESCE(a._fivetran_deleted,false)=false
        and COALESCE(b._fivetran_deleted,false)=false
        and a.created_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
        and a.created_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '2h'
        AND a.state NOT IN ('canceled_by_fraud',
                            'canceled_for_payment_error',
                            'canceled_by_split_error',
                            'canceled_by_early_regret')
        group by 1,4,5,8,9),

        ''' + query_cp + f'''
,

orders as (
                    SELECT a.ORDER_ID,
                    listagg(distinct b.product_id, ',') as product_id,
                    listagg(distinct p.name,',') as product_name,
                    listagg(distinct case when x.in_content=true then pscp.store_product_id end,',') as store_product_id,
                    SUM(B.VALUE) AS DISCOUNT_VALUE
                            FROM {country}_core_orders_public.order_discounts_vw a
                            join {country}_core_orders_public.order_discount_details_vw b on b.order_discount_id =  a.id
                            join {country}_core_orders_public.order_stores_vw os on os.order_id=a.order_id
                            left join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p on p.id=b.product_id
                            left join CPGS_DATASCIENCE.LOOKUP_STORE x on x.store_id=os.store_id and lower(x.COUNTRY_CODE)= '{country}'
                            left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = b.product_id::int

                            WHERE b.PRODUCT_ID IS NOT NULL
                                AND COALESCE(B._FIVETRAN_DELETED, false) = false
                                AND COALESCE(A._FIVETRAN_DELETED, false) = false
                                AND TYPE_DESCRIPTION NOT IN ('service_fee',
                                                             'free_shipping',
                                                             'shipping',
                                                             'eta_min_or_free_order',
                                                             'eta_min_or_cashback',
                                                             'prime_discount_shipping')
                            GROUP BY 1),


reason as (select distinct order_id,
                order_discount_id,
                sum(total_discount) over (partition by order_id),
                case when rank = 1 then concat('R$',round(total_discount)::text, ' - ', source) else null end as reason
from

     (select b.order_id,
             a.order_discount_id,
       sum(a.value) as total_discount,
       case when OFFERTABLE_TYPE ilike '%globaloffers%' then 'Global_Offer'
       when OFFERTABLE_TYPE = 'offer-base-price' then 'Markdown'
       when OFFERTABLE_TYPE = 'PrimeDetail' then 'Prime'
       else 'Indistinct Offer' end as source,
       rank() over (partition by b.ORDER_ID order by total_discount desc) as rank

    from {country}_core_orders_public.order_discount_details_vw a
    join {country}_core_orders_public.order_discounts_vw b on b.id = a.ORDER_DISCOUNT_ID
    join {country}_core_orders_public.orders_vw o on o.id = b.order_id
    where extract(year from o.created_at::date) >= '2022'
    and product_id is not null
    AND a.TYPE_DESCRIPTION NOT IN ('service_fee',
                                 'free_shipping',
                                 'shipping',
                                 'eta_min_or_free_order',
                                 'eta_min_or_cashback',
                                 'prime_discount_shipping')
    AND COALESCE(a._FIVETRAN_DELETED, false) = false
    group by 1,2,4))                             


select IDS.CREATED_AT,
       IDS.ID AS ORDER_ID,
       ZEROIFNULL(TOTAL_PRICE) + ZEROIFNULL(ADDED_PRICE) AS TOTAL_ORDER_VALUE,
       ZEROIFNULL(DISCOUNT_VALUE) AS TOTAL_ORDER_DISCOUNT,
       TOTAL_VALUE,
       ZEROIFNULL(DIV0(TOTAL_ORDER_DISCOUNT,TOTAL_ORDER_VALUE)) AS DISCOUNT,
       ids.STORE_ID,
       s.physical_store_id,
       s.STORE_BRAND_ID,
       ids.NAME,
       a.PRODUCT_ID,
       a.product_name,
       a.store_product_id,
       VERTICAL,
       r.reason as most_offensive_discount
       from orders A
             RIGHT join ids
                on a.order_id = ids.id
            join {country}_PG_MS_CPGOPS_STORES_MS_PUBLIC.stores s on s.store_id = ids.store_id
             LEFT JOIN (SELECT ORDER_ID, SUM(PRICE) AS ADDED_PRICE from {country}_CORE_ORDERS_PUBLIC.ORDER_WHIMS
                   WHERE COALESCE(_FIVETRAN_DELETED, false) = false
                   AND STATE = 'confirmed'
                   GROUP BY 1) ADDED
                ON ids.ID = ADDED.ORDER_ID
             LEFT JOIN reason r on r.order_id = ids.id and r.reason is not null   
             WHERE VERTICAL NOT IN ('Turbo','RappiFavor', 'Others','Restaurantes')
             having discount >= 0.80

'''
    df = snow.run_query(query)
    return df

def alarm(df,b,c):
    channel_alert = timezones.slack_channels('commercial_alarms')
    current_time = timezones.country_current_time(country)
    print(df)
    df['physical_store_id'] = df['physical_store_id'].astype(float)
    df['store_brand_id'] = df['store_brand_id'].astype(float)

    df1 = df.groupby(['physical_store_id', 'name', 'vertical'])['order_id'].agg(['count', ('order_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df11 = df.groupby(['physical_store_id', 'name', 'vertical'])['product_id'].agg([('product_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df1111 = df.groupby(['physical_store_id', 'name', 'vertical'])['store_product_id'].agg([('store_product_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df111 = df.groupby(['physical_store_id', 'name', 'vertical'])['product_name'].agg([('product_name', lambda x: ', '.join(map(str, x)))]).reset_index()
    df_physical = pd.merge(df1,df11, on=['physical_store_id', 'name', 'vertical'])
    df_physical = pd.merge(df_physical, df111, on=['physical_store_id', 'name', 'vertical'])
    df_physical = pd.merge(df_physical, df1111, on=['physical_store_id', 'name', 'vertical'])
    df_physical = df_physical[df_physical['count'] >= 3]

    b['physicalstoreid'] = b['physicalstoreid'].astype(float)
    df_physical = pd.merge(df_physical,b, how='left', left_on=df_physical['physical_store_id'], right_on=b['physicalstoreid'])
    df_physical = df_physical[(df_physical['physicalstoreid'].isnull())]
    df_physical = df_physical.drop(['key_0','physicalstoreid'], axis=1)
    df_physical = df_physical[df_physical['physical_store_id'].notna()==True]

    if not df_physical.empty:

        df_physical['alarm_at'] = current_time
        df_physical['country'] = country
        print('snowflake physical_store_id')

        df_physical1=df_physical[['physical_store_id', 'name', 'vertical', 'count', 'order_id', 'product_id', 'alarm_at', 'country']]
        snow.upload_df_occ(df_physical1,'high_discounts_physical')

    else: 
        print('df_physical null')

    df2 = df.groupby(['store_brand_id'])['order_id'].agg(['count', ('order_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df22 = df.groupby(['store_brand_id'])['product_id'].agg([('product_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df222 = df.groupby(['store_brand_id'])['product_name'].agg([('product_name', lambda x: ', '.join(map(str, x)))]).reset_index()
    df2222 = df.groupby(['store_brand_id'])['store_product_id'].agg([('store_product_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df_brand = pd.merge(df2, df22, on=['store_brand_id'])
    df_brand = pd.merge(df_brand, df222, on=['store_brand_id'])
    df_brand = pd.merge(df_brand, df2222, on=['store_brand_id'])
    df_brand = df_brand[df_brand['count'] >= 5]

    c['storebrandid'] = c['storebrandid'].astype(float)
    df_brand = pd.merge(df_brand,c, how='left', left_on=df_brand['store_brand_id'], right_on=c['storebrandid'])
    df_brand = df_brand[(df_brand['storebrandid'].isnull())]
    df_brand = df_brand.drop(['key_0','storebrandid'], axis=1)
    df_brand = df_brand[df_brand['store_brand_id'].notna() == True]

    if not df_brand.empty:
        df_brand['alarm_at'] = current_time
        df_brand['country'] = country
        print('snowflake store_brand_id')
        df_brand1=df_brand[['store_brand_id', 'count', 'order_id','product_id', 'alarm_at', 'country']]
        snow.upload_df_occ(df_brand1,'high_discounts_brand')
    else:
        print('df_brand null')

    if not df_physical.empty:
        df_physical_final = pd.DataFrame()
        for index, row in df_physical.iterrows():
            df_row = df[(df['physical_store_id'] == row['physical_store_id'])]
            df_row['created_at'] = df_row['created_at'].dt.tz_localize(None)
            df_physical_final = df_physical_final.append(df_row, ignore_index=True)

        if not df_physical_final.empty:
            text = '''
            *Alarma - High discounts - Physical Store ID :alert:*
            :flag-{country}:
            Pais: {country} 
            Nuevos Physical Store IDs con órdenes con grandes descuentos alcanzarón el threshold.
            '''.format(country=country)
            print(text)
            home = expanduser("~")
            results_file = '{}/high_discounts_physical_id_{country}.xlsx'.format(home, country=country)
            df_final = df_physical_final[['created_at', 'physical_store_id', 'name','order_id', 'total_order_value', 'total_order_discount', 'total_value', 'discount',
                      'product_id','product_name', 'vertical','store_product_id', 'most_offensive_discount']]
            df_final.to_excel(results_file, sheet_name='order_ids', index=False)
            slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
    else:
        print('df_physical empty')

    if not df_brand.empty:
        df_brand_final = pd.DataFrame()
        for index, row in df_brand.iterrows():
            df_row = df[(df['store_brand_id'] == row['store_brand_id'])]
            df_row['created_at'] = df_row['created_at'].dt.tz_localize(None)
            df_brand_final = df_brand_final.append(df_row, ignore_index=True)

        if not df_brand_final.empty:
            text = '''
            *Alarma - High discounts - Brand ID :alert:*
            :flag-{country}:
            Pais: {country} 
            Nuevos Brand IDs con órdenes con grandes descuentos alcanzarón el threshold.
            '''.format(country=country)
            print(text)
            home = expanduser("~")
            results_file = '{}/high_discounts_brand_id_{country}.xlsx'.format(home, country=country)
            df_final = df_brand_final[
                ['created_at','store_brand_id','store_id', 'name','order_id', 'total_order_value', 'total_order_discount', 'total_value', 'discount',
                  'product_id','product_name', 'vertical','store_product_id', 'most_offensive_discount']]
            df_final.to_excel(results_file, sheet_name='order_ids', index=False)
            slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
    else:
        print('df_brand empty')


countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']

for country in countries:
  try:
      print(country)
      b,c = logs(country)
      a = discounts(country)
      alarm(a,b,c)
  except Exception as e:
      print(e)
