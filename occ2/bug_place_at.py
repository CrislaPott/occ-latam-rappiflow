import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

def logs(country):
	timezone, interval = timezones.country_timezones(country)
	query = f"""
	select distinct physical_store_id as physical_storeid  from ops_occ.bug_place_at 
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	"""
	df = snow.run_query(query)
	return df

def get_bug_place_at(country):
	query = f"""
	select cpgops.physical_store_id,pss.name as store_name, count(distinct o.id) as orders, listagg(distinct o.id::text, ',') as order_ids
from {country}_CORE_ORDERS_PUBLIC.orders_vw o
left join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = os.store_id
left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
left join (select distinct order_id
from {country}_core_orders_public.order_modifications
where created_at::date >= dateadd(day,-3,current_date())::date
and get_path(params,'cancelation_reason')::text in ('crsan_inner_test')
) od on od.order_id = o.id
where o.place_at::date > dateadd(day,+10,current_date())::date
and pss.name not ilike '%rappi%pay%' 
and o.state not like '%canceled_by_fraud%'
and o.state not like '%canceled_by_early_regret%'
and cpgops.type not ilike '%ecommerce_national_temporal%'
and od.order_id is null
and o.created_at::date >= dateadd(day,-2,current_date())::date
group by 1,2
having count(distinct o.id) >= 2
	"""
	df = snow.run_query(query)
	return df

def run_alarm(df1,df2):
	channel_alert = timezones.slack_channels('commercial_alarms')
	current_time = timezones.country_current_time(country)

	df = pd.merge(df1, df2, how='left', left_on=df1['physical_store_id'], right_on=df2['physical_storeid'])
	df = df[(df['physical_storeid'].isnull())]
	df = df.drop(['key_0','physical_storeid'], axis=1)
	print(df)

	df['country'] = country
	df['alarm_at'] = current_time    
	snow.upload_df_occ(df,'bug_place_at')


	for index, row in df.iterrows():
		rows = dict(row)

		text = '''
		*Alarma Bug Place AT en Tiendas físicas* :alert:
		Country :flag-{country}:
		Pais: {country}
		La tienda {store_name} (Store ID: {physical_store_id}) alcanzó {orders} pedidos con place_at depués de 10 días
		Order IDs: {order_ids}
		'''.format(
			store_name = row['store_name'],
			physical_store_id = row['physical_store_id'],
			orders = row['orders'],
			order_ids = row['order_ids'],
			country = country
			)

		print(text)
		slack.bot_slack(text,channel_alert)

countries = ['br','ar','uy','cl','pe','mx','co','ec','cr']
for country in countries:
	print(country)
	try:
		df2 = logs(country)
		df1 = get_bug_place_at(country)
		run_alarm(df1,df2)
	except Exception as e:
		print(e)