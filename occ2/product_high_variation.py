import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones, snippets


def logs(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''
        select brand_group_id::text as brand_groupid
        from ops_occ.products_high_variation_stock
        where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    	and country = '{country}'
        '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)

    return df


def get_dataframe():
    query = '''
--no_cache
with base0 as(

select distinct store_id, country,
BRAND_group as brand_group, brand_group_id as brand_group_id
from ops_global.global_orders  where vertical in ('Express', 'Super/Hiper', 'Licores','Farmacia')
),

base_2 as(
select store_id, country,
brand_group,brand_group_id,
row_number() over(partition by  store_id, country order by brand_group_id) as rownumber
from base0
),

base_3 as (
select * from base_2
where rownumber = 1
),

total_product_por_tienda as (

select 'CO' AS COUNTRY, store_id, product_id from co_pglr_ms_grability_products_public.product_store_vw
union all

select 'BR' AS COUNTRY, store_id, product_id from BR_pglr_ms_grability_products_public.product_store_vw
UNION ALL

select 'AR' AS COUNTRY, store_id, product_id from AR_pglr_ms_grability_products_public.product_store_vw
UNION ALL

select 'EC' AS COUNTRY, store_id, product_id from EC_pglr_ms_grability_products_public.product_store_vw
UNION ALL

select 'MX' AS COUNTRY, store_id, product_id from MX_pglr_ms_grability_products_public.product_store_vw
UNION ALL

select 'CL' AS COUNTRY, store_id, product_id from CL_pglr_ms_grability_products_public.product_store_vw
UNION ALL

select 'CR' AS COUNTRY, store_id, product_id from CR_pglr_ms_grability_products_public.product_store_vw
)
,

base_4 as(
SELECT distinct p.country as country_code,  p.product_id, b.brand_group, b.brand_group_id
from total_product_por_tienda p
left join base_3 b on b.store_id  = p.store_id AND p.country = b.country
where b.brand_group is not null
),

created_at_product as (
select 'CO' as country, id,  created_at::date as created_at from co_pglr_ms_grability_products_public.products_vw
union all
select 'BR' as country, id,  created_at::date as created_at from BR_pglr_ms_grability_products_public.products_vw
union all
select 'CL' as country, id,  created_at::date as created_at from CL_pglr_ms_grability_products_public.products_vw
union all
select 'AR' as country, id,  created_at::date as created_at from AR_pglr_ms_grability_products_public.products_vw
union all
select 'EC' as country, id,  created_at::date as created_at from EC_pglr_ms_grability_products_public.products_vw
union all
select 'MX' as country, id,  created_at::date as created_at from MX_pglr_ms_grability_products_public.products_vw
union all
select 'CR' as country , id,  created_at::date as created_at from CR_pglr_ms_grability_products_public.products_vw

),

basemes as (
select * from base_4 b
left join created_at_product c on b.country_code = c.country and b.product_id = c.id
where  date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '31 days'
),

base as (
select * from base_4 b
left join created_at_product c on b.country_code = c.country and b.product_id = c.id
where  date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '9 days'
),

total_product_1_semana as(
select distinct country, brand_group, brand_group_id from base
where   date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '9 days'
and  date_trunc('days', created_at::date ) < date_trunc('days' ,current_date) - interval '1 days'),

total_product_previous_1_semana as(
select country, brand_group,brand_group_id, count(product_id) as total_product_previous_7_dias  from base
where   date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '9 days'
and  date_trunc('days', created_at::date ) < date_trunc('days' ,current_date) - interval '1 days'
group by 1,2,3),

total_product_yesterday as (
select country, brand_group,brand_group_id, count(product_id) as total_product_yesterday  from base
where   date_trunc('days', created_at::date ) = date_trunc('days' ,current_date) - interval '1 days'
group by 1,2,3 order by total_product_yesterday desc

),

/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------*/


basemes as (
select * from base_4 b
left join created_at_product c on b.country_code = c.country and b.product_id = c.id
where  date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '31 days'
),

total_product_previous_1_mes as(
select country,  brand_group,brand_group_id, count(product_id) as total_product_previous_1_mes  from basemes
where   date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '31 days'
and  date_trunc('days', created_at::date ) < date_trunc('days' ,current_date) - interval '1 days'
group by 1,2,3),

/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------*/


max_total_product_previous_1_semana as(
select country,  brand_group, created_at, brand_group_id, count(product_id) as total_product_previous_7_dias  from base
where   date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '9 days'
and  date_trunc('days', created_at::date ) < date_trunc('days' ,current_date) - interval '1 days'
group by 1,2,3,4),

max_1_semana as
(
select country,  brand_group , brand_group_id,  max(total_product_previous_7_dias) as max_product_previous_7_dias  from  max_total_product_previous_1_semana
group by 1,2,3

),

max_total_product_previous_1_mes as(
select country,  brand_group,brand_group_id , created_at, count(product_id) as total_product_previous_1_mes  from basemes
where   date_trunc('days', created_at::date ) >= date_trunc('days' ,current_date) - interval '30 days'
and  date_trunc('days', created_at::date ) < date_trunc('days' ,current_date) - interval '1 days'
group by 1,2,3,4 ),

max_1_mes as
(
select country,  brand_group, brand_group_id , max(total_product_previous_1_mes) as max_product_previous_1_mes  from  max_total_product_previous_1_mes
group by 1,2,3

),

base_final as(
select b.country,  b.brand_group, b.brand_group_id,
case when total_product_yesterday is null then 0 else total_product_yesterday end as total_product_yesterday,
max_product_previous_7_dias,
max_product_previous_1_mes,
total_product_previous_7_dias,
total_product_previous_1_mes ,
total_product_yesterday/MAX_PRODUCT_PREVIOUS_7_DIAS as percent_max_7_dias,
total_product_yesterday/max_product_previous_1_mes as percent_max_30_dias
from total_product_1_semana b
left join total_product_yesterday y on  b.brand_group = y.brand_group and b.country = y.country
left join total_product_previous_1_semana s on  b.brand_group = s.brand_group and b.country = s.country
left join total_product_previous_1_mes m on b.brand_group = m.brand_group and b.country = m.country
left join max_1_semana ms on b.brand_group = ms.brand_group and b.country = ms.country
left join max_1_mes mm on  b.brand_group = mm.brand_group and b.country = mm.country
order by TOTAL_PRODUCT_YESTERDAY desc

)

select * from base_final b where total_product_yesterday > 30 and percent_max_30_dias >= 2'''
    df = snow.run_query(query)

    return df


def alarm_high_variation(df1, df2):
    df1['country'] = df1['country'].str.lower()

    df1['brand_group_id'] = df1['brand_group_id'].astype(str)

    df = pd.merge(df1, df2, how='left', left_on=df1['brand_group_id'], right_on=df2['brand_groupid'])
    df = df[(df['brand_groupid'].isnull())]
    df = df.drop(['key_0', 'brand_groupid'], axis=1)

    if not df.empty:
        try:
            for index, row in df.iterrows():
                row = dict(row)
                percentage = round(row['total_product_yesterday'] / row['total_product_previous_7_dias'], 2) * 100
                text = '''*Alarma - Productos con alta variacion de stock* :alert:
                País: {country} :flag-{country}:
                Group Brand (Brand_Group_ID): {brand_group} ({brand_group_id})
                Ayer se crearon {total_product_yesterday} productos. Comparado con d-7 ({total_product_previous_7_dias} productos), tuvimos un aumento del {percentage}%.
                '''.format(
                    country=row['country'],
                    brand_group=row['brand_group'],
                    brand_group_id=row['brand_group_id'],
                    total_product_yesterday=row['total_product_yesterday'],
                    total_product_previous_7_dias=row['total_product_previous_7_dias'],
                    percentage=percentage)

                print(text)

                countries = ['mx', 'br', 'co', 'cl', 'uy', 'cr', 'ec', 'pe', 'ar']
                for country in countries:
                    if row['country'] == country:
                        print('slack ', row['country'])
                        channel_alert = timezones.slack_channels('commercial_alarms')
                        current_time = timezones.country_current_time(row['country'])
                        df['alarm_at'] = current_time
                        slack.bot_slack(text, channel_alert)


        except Exception as e:
            print(e)
    else:
        print('no se aplica')

    print(df)
    snow.upload_df_occ(df, 'products_high_variation_stock')


df2_ = []
countries = snippets.country_list()
for country in countries:
    try:
        df = logs(country)
        df2_.append(df)
        df2 = pd.concat(df2_, ignore_index=True)
    except Exception as e:
        print(e)

df1 = get_dataframe()
alarm_high_variation(df1, df2)
