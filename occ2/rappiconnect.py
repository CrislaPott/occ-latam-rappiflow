import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from os.path import expanduser
from lib import snowflake as snow
from functions import timezones, snippets

def reported_stores(country):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	select store_id::text as store_id, last_percentage
	from
	(select store_id, last_value(percentage) over (partition by store_id order by alarm_at asc) as last_percentage
	from ops_occ.rappi_connect
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and alarm_at::timestamp_ntz >= dateadd(minutes,-30,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
	and country='{country}'
	)
	group by 1,2
	'''.format(country=country, timezone=timezone)
	df = snow.run_query(query)
	return df

def reported_brand(country):
	timezone, interval = timezones.country_timezones(country)

	query2 = '''
	select brand_id::text as brand_id, last_percentage
	from
	(select brand_id, last_value(percentage) over (partition by brand_id order by alarm_at asc) as last_percentage
	from ops_occ.rappi_connect_brands
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and alarm_at::timestamp_ntz >= dateadd(minutes,-30,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
	and country='{country}'
	)
	group by 1,2
	'''.format(country=country, timezone=timezone)
	df2 = snow.run_query(query2)

	return df2

def reported_brand_group(country):
	timezone, interval = timezones.country_timezones(country)

	query3 = '''
	select brand_group_id::text as brand_group_id, last_percentage
	from
	(select brand_group_id, last_value(percentage) over (partition by brand_group_id order by alarm_at asc) as last_percentage
	from ops_occ.rappi_connect_group_brands
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and alarm_at::timestamp_ntz >= dateadd(minutes,-30,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
	and country='{country}'
	)
	group by 1,2
	'''.format(country=country, timezone=timezone)
	df3 = snow.run_query(query3)

	return df3

def offenders(country):
	timezone, interval = timezones.country_timezones(country)

	rappi_connect_query = """
	select
	s.store_id::text as store_id
	from stores s
	join owners o on o.id=s.owner_id
	where rappi_connect=true
	"""

	query = """
	--no_cache
	with base as
	(
	SELECT order_id, max(created_at) as termined
	FROM order_modifications om
	WHERE om.created_at >= now() AT TIME ZONE 'America/{timezone}' - interval '30 minutes'
	AND (TYPE IN ('close_order','arrive','finish_order',
	'close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') 
	OR TYPE ILIKE '%cancel%')
	GROUP BY 1
	)
	, inter as (
	select os.store_id::text as store_id, max(os.name) as name, max(os.type) as type,
	count(distinct case when o.state='cancel_order_by_picker' then o.id else null end) cancels,
	count(distinct o.id) as orders,
	count(distinct case when o.state='cancel_order_by_picker' then o.id else null end)::float/count(distinct o.id)::float as percentage,
	string_agg(distinct case when o.state='cancel_order_by_picker' then o.id::text else null end,',') as order_ids
	from base
	inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
	inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
	where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error')
	and os.name not ilike '%Dummy%Store%Rappi%Connect%'
	and os.type NOT IN ('courier_hours',
						 'courier_sampling','restaurant','queue_order_flow',
						 'soat_webview')
     and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
	 AND os.type not ilike '%rappi%pay%'
	 and os.name not ilike '%Dummy%Store%Rappi%Connect%'
	 AND payment_method NOT IN ('synthetic')
	 group by 1)

	select * from inter
	where percentage >= 0.10 and cancels >= 3
	""".format(countrt=country,timezone=timezone)

	query_petz = """
	--no_cache
	with base as
	(
	SELECT order_id, max(created_at) as termined
	FROM order_modifications om
	WHERE om.created_at >= now() AT TIME ZONE 'America/{timezone}' - interval '30 minutes'
	AND (TYPE IN ('close_order','arrive','finish_order',
	'close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') 
	OR TYPE ILIKE '%cancel%')
	GROUP BY 1
	)
	, inter as (
	select store_id::text as store_id,
	case when o.state='cancel_order_by_picker' then o.id else null end cancels,
	o.id as orders
	from base
	inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
	inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
	where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and os.name not ilike '%Dummy%Store%Rappi%Connect%'
	and os.name not ilike '%Dummy%Store%Rappi%Connect%'
	and os.type NOT IN ('courier_hours',
						 'courier_sampling','restaurant','queue_order_flow','soat_webview')
     and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
	 AND os.type not ilike '%rappi%pay%'
	 and os.name not ilike '%Dummy%Store%Rappi%Connect%'
	 AND payment_method NOT IN ('synthetic')
	)
	select * from inter
	""".format(timezone=timezone)

	query_store_infos = """
	select 'br'                     as country,
           a.store_id::text         as store_id,
           a.name                   as store_name,
           a.type                   as store_type,
           c.id::text               as brand_id,
           c.name                   as brand_name,
           a.city_address_id        as city_id,
           msc.city                 as city_name,
           v.vertical,
           brand_group_id::text     as brand_group_id,
           brand_group_name,
           cpgops.physical_store_id as physical_store_id,
           pss.name                 as physical_store_name
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
             left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                       on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
             left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
             left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
             left join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                       on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
             inner join (select store_type                      as storetype,
                                case
                                    when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                    when vertical_group = 'WHIM' then 'Antojos'
                                    when vertical_group = 'RAPPICASH' then 'RappiCash'
                                    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                    when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                    when upper(store_type) in ('TURBO') then 'Turbo'
                                    when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
                                    when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                    when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                    when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                    when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                    when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                    when upper(vertical_group) in ('CPGS') then 'CPGs'
                                    else vertical_sub_group end as vertical
                         from VERTICALS_LATAM.{country}_VERTICALS_V2
    ) v on v.storetype = a.type

             left join (
        select *
        from (select store_brand_id,
                     last_value(brand_group_id)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_id,
                     last_value(bg.name)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_name
              from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                       join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
        group by 1, 2, 3
    ) bg on bg.store_brand_id = a.store_brand_id

    where not coalesce(a._fivetran_deleted, false)
    --and v.vertical not in ('Turbo')
    """.format(timezone=timezone, country=country)

	if country == 'co':
		metrics = redash.run_query(1904, query)
		petz = redash.run_query(1904, query_petz)
		rappi_connect_stores = redash.run_query(4486,rappi_connect_query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
		petz = redash.run_query(1337, query_petz)
		rappi_connect_stores = redash.run_query(4487,rappi_connect_query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
		petz = redash.run_query(1155, query_petz)
		rappi_connect_stores = redash.run_query(4489,rappi_connect_query)
	elif country == 'mx':
		metrics = redash.run_query(7977, query)
		petz = redash.run_query(7977, query_petz)
		rappi_connect_stores = redash.run_query(4291,rappi_connect_query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
		petz = redash.run_query(1156, query_petz)
		rappi_connect_stores = redash.run_query(4492,rappi_connect_query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
		petz = redash.run_query(1922, query_petz)
		rappi_connect_stores = redash.run_query(4491,rappi_connect_query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
		petz = redash.run_query(1921, query_petz)
		rappi_connect_stores = redash.run_query(4490,rappi_connect_query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
		petz = redash.run_query(1157, query_petz)
		rappi_connect_stores = redash.run_query(4493,rappi_connect_query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)
		petz = redash.run_query(1338, query_petz)
		rappi_connect_stores = redash.run_query(4488,rappi_connect_query)

	store_infos = snow.run_query(query_store_infos)

	if country == 'br':
		return metrics, petz, rappi_connect_stores, store_infos
	else:
		return metrics, petz, rappi_connect_stores, store_infos

def alarm(country):
	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('tech_integration')

	if country in ['br']:
		a, b, c, d= offenders(country)
	else:
		a, b, c, d = offenders(country)


	if not a.empty:
		a = pd.merge(a, c, how='inner', on=['store_id'])
		rp = reported_stores(country)
		a = pd.merge(a, rp, how='left', on=['store_id'])
		a = pd.merge(a, d['store_id'], how='inner', on=['store_id'])
		a = a[(a['last_percentage'].isnull()) | (a['percentage'] >= a['last_percentage'] + 20)]
		print(a)
		if not a.empty:
			to_upload = a[['store_id','percentage','orders','cancels','order_ids']]
			to_upload['country'] = country
			to_upload['alarm_at'] = current_time
			snow.upload_df_occ(to_upload, 'rappi_connect')

		try:
			for index, row in a.iterrows():
				rows = dict(row)
				text = '''*Alarma de RappiConnect* :alert:
				Country: {country} :flag-{country}:
				La tinda {store_name} (Store Id: {store_id}) ancalzó {percentage}% de cancelacion por picker en los ultimos 30 minutos
				Total Ordenes: {orders}
				Total Canceladas: {cancels}
				Order_IDs: {order_ids}
				'''.format(
					store_id=row['store_id'],
					store_name=row['name'],
					orders=row['orders'],
					cancels=row['cancels'],
					percentage=round(row['percentage']*100,2),
					order_ids=row['order_ids'],
					country = country
					)

				print(text)
				home = expanduser("~")
				results_file = '{}/rappi_connect_store_{country}.xlsx'.format(home, country=country)
				a.to_excel(results_file, sheet_name='store', index=False)
				slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

		except Exception as e:
			print(e)
	else:
		print('a null')

	if not b.empty:
		df = pd.merge(b, c, how='inner', on=['store_id'])
		print(df)
		df = pd.merge(df, d, how='inner', on=['store_id'])
		print(df)

		## brands

		b0 = df.groupby(["brand_id", "brand_name"])["cancels"].count().reset_index(name="total_cancels")
		b1 = df.groupby(["brand_id", "brand_name"])["orders"].count().reset_index(name="total_orders")
		b3 = df.groupby(["brand_id", "cancels"])["store_id"].agg([('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()

		b2 = df[(df['cancels'].notnull())]
		b2 = b2.groupby(["brand_id","brand_name"])["cancels"].agg([('order_ids', lambda x: ', '.join(map(str, x)))]).reset_index()

		b = pd.merge(b0,b1[['brand_id','total_orders']], how='inner',on=['brand_id'])
		b = pd.merge(b,b2[['brand_id','order_ids']], how='inner',on=['brand_id'])
		b = pd.merge(b,b3, how='left',left_on=b['brand_id'],right_on=b3['brand_id'])[['brand_id_x', 'brand_name', 'total_cancels', 'total_orders',
       'order_ids', 'cancels', 'store_id']].rename(columns={'brand_id_x':'brand_id','store_id':'store_ids'})
		print(b)


		b["percentage"] = b['total_cancels'] / b['total_orders']


		## group brands

		g0 = df.groupby(["brand_group_id", "brand_group_name"])["cancels"].count().reset_index(name="total_cancels")
		g1 = df.groupby(["brand_group_id", "brand_group_name"])["orders"].count().reset_index(name="total_orders")
		g2 = df[(df['cancels'].notnull())]

		g2 = g2.groupby(["brand_group_id","brand_group_name"])["cancels"].agg([('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()

		g3 = df.groupby(["brand_group_id","cancels"])["store_id"].agg([('order_ids', lambda x: ', '.join(map(str, x)))]).reset_index()

		g = pd.merge(g0,g1[['brand_group_id','total_orders']], how='inner',on=['brand_group_id'])
		g = pd.merge(g,g2[['brand_group_id','order_ids']], how='inner',on=['brand_group_id'])
		g = pd.merge(g, g3, how='left', left_on=g['brand_group_id'], right_on=g3['brand_group_id'])[[ 'brand_group_id_x', 'brand_group_name', 'total_cancels',
       'total_orders', 'order_ids','cancels', 'store_id']].rename(columns={'brand_group_id_x':'brand_group_id','store_id':'store_ids'})

		g["percentage"] = g['total_cancels'] / g['total_orders']

		##final
		# brand
		b = b[(b['total_cancels'] >= 4) & (b['percentage'] >= 0.10)]

		# group brand
		g = g[(g['total_cancels'] >= 15) & (g['percentage'] >= 0.10)]

		if not b.empty:
			rp2=reported_brand(country)
			b = pd.merge(b, rp2, how='left', on=['brand_id'])
			b = b[(b['last_percentage'].isnull()) | (b['percentage'] >= (b['last_percentage'] + 0.20))]

			b['country'] = country
			b['alarm_at'] = current_time
			to_upload2 = b[['brand_id', 'brand_name', 'total_orders', 'total_cancels', 'order_ids', 'percentage','country','alarm_at','store_ids']]
			print(to_upload2)
			snow.upload_df_occ(to_upload2, 'rappi_connect_brands')
			b['alarm_at'] = current_time.replace(tzinfo=None)
			try:
				for index, row in b.iterrows():
					rows = dict(row)
					text = '''*Alarma Rappi Connect - Brands* :alert:
					Country: {country} :flag-{country}:
					La brand {brand_name} ancalzó {percentage}% de cancelacion en los ultimos 30 minutos
					Total Ordenes: {orders}
					Total Canceladas: {cancels}
					Order_IDs: {order_ids}
					Store_IDs: {store_ids}
					'''.format(
						orders=row['total_orders'],
						cancels=row['total_cancels'],
						percentage=round(row['percentage']*100,2),
						order_ids=row['order_ids'],
						brand_name = row['brand_name'],
						store_ids=row['store_ids'],
						country = country
						)

					print(text)
					home = expanduser("~")
					results_file = '{}/rappi_connect_brand_{country}.xlsx'.format(home, country=country)
					b.to_excel(results_file, sheet_name='brands', index=False)
					slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

			except Exception as e:
				print(e)
		else:
			print('b null')

		if not g.empty:
			rp3=reported_brand_group(country)
			g = pd.merge(g, rp3, how='left', on=['brand_group_id'])
			g = g[(g['last_percentage'].isnull()) | (g['percentage'] >= (g['last_percentage'] + 0.20))]
			g['country'] = country
			g['alarm_at'] = current_time
			to_upload2 = g[
				['brand_group_id', 'brand_group_name', 'total_orders', 'total_cancels', 'order_ids', 'percentage',
				 'country', 'alarm_at', 'store_ids']]
			snow.upload_df_occ(to_upload2, 'rappi_connect_group_brands')
			g['alarm_at'] = current_time.replace(tzinfo=None)
			try:
				for index, row in g.iterrows():
					rows = dict(row)
					text = '''*Alarma Rappi Connect - Group Brands* :alert:
					Country: {country} :flag-{country}:
					La Group brand {brand_name} ancalzó {percentage}% de cancelacion en los ultimos 30 minutos
					Total Ordenes: {orders}
					Total Canceladas: {cancels}
					Order_IDs: {order_ids}
					Store_IDs: {store_ids}
					'''.format(
						orders=row['total_orders'],
						cancels=row['total_cancels'],
						percentage=round(row['percentage']*100,2),
						order_ids=row['order_ids'],
						brand_name = row['brand_group_name'],
						store_ids=row['store_ids'],
						country = country
						)

					print(text)
					home = expanduser("~")
					results_file = '{}/rappi_connect_groupbrand_{country}.xlsx'.format(home, country=country)
					g.to_excel(results_file, sheet_name='groupbrand', index=False)
					slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

			except Exception as e:
				print(e)
		else:
			print('g null')
	else:
		print('no se aplica')

def run_alarm(country):
	alarm(country)

countries = snippets.country_list()
for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)