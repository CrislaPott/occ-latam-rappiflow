import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

load_dotenv()

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select distinct brand_id::text as brandid
    from ops_occ.defect_rate
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df


def get_offenders(country):
    query = """
    --no_cache
with defects as (
    select lower(country)::text                                                         as country_code
         , order_id::text                                                        as order_id
         , user_type::text                                                       as user_type
         , channel::text                                                         as channel
         , regexp_replace(regexp_replace(trim(translate(lower(name::text), 'áéíóúãõâêôàç', 'aeiouaoaeoac')),
                                         '[^([:alpha:]|À-ú|\\s)]'), '\\s+', ' ') as description
         , regexp_replace(regexp_replace(trim(translate(lower(preview::text), 'áéíóúãõâêôàç', 'aeiouaoaeoac')),
                                         '[^([:alpha:]|À-ú|\\s)]'), '\\s+', ' ') as preview
         , case
        /* Customer - Order status delay */
               when regexp_like(lower(description), '.*((pedido|order|orden).*(demor|took.*long|tard)).*')
                   then 'Order status / delay|unidentified|Customer'
        /* Customer - Incomplete Damaged Order */
               when regexp_like(lower(description),
                                '.*((pedido|order|ite.*|produto|product).*(mau estado|poor condition|mal estado)|(n.*lleg|n.*cheg|was not).*(buen estado|mau estado|good condition)).*')
                   then 'Incomplete or Damaged Orders|Product in poor condition|Customer'
               when regexp_like(lower(description),
                                '.*((pedido|order|orden|produto|product).*(dif.*erent|misplace)|(dif.*erent).*(esper|expect)).*')
                   then 'Incomplete or Damaged Orders|different product|Customer'
               when regexp_like(lower(description),
                                '.*((pedido|order|orden).*i.complet|faltou.*pedido|(cheg|arriv|lleg).*i.complet).*')
                   then 'Incomplete or Damaged Orders|Missing product|Customer'
               WHEN lower(user_type) = 'end_users' and regexp_like(lower(description),
                                                                   '.*((pedido|order|orden|produto|product).*(retorno|return|devol)|(retorno|return|devol).*(pedido|order|orden|produto|product)).*')
                   then 'Incomplete or Damaged Orders|Return of products|Customer'
        /* Customer - Order Never Arrived */
               when regexp_like(lower(description),
                                '.*((pedido|order|orden).*(n.*o|not).*(cheg|entreg|arriv|deliver|lleg)|n.*(cheg|arriv|lleg)).*')
                   then 'Order Never Arrived|Request not delivered|Customer'
        /* Customer - Disagree with Charge */
               when regexp_like(lower(description),
                                '.*((cobran.a|cobro).*(rappi.*prime|benef.cio)|(rappi.*prime|benefit).*charge).*')
                   then 'Disagree with Charge|Collection membership RappiPrime|Customer'
               when regexp_like(lower(description),
                                '.*(prime.*cobr.*frete|prime.*charg.*shipping|prime.*cobrar*.env.o.*)')
                   then 'Disagree with Charge|Shipping fee|Customer'
               when regexp_like(lower(description), '.*((confer|consult|check|a.uda).*(nota fiscal|invoice|factura)).*')
                   then 'Disagree with Charge|I bill my order|Customer'
               when regexp_like(lower(description), '.*(indique um amig|refer and win|refiere y gana).*')
                   then 'Disagree with Charge|Refer and win|Customer'
               when regexp_like(lower(description),
                                '.*(problem.*desc.*nto|problem.*promo|cupom.*n.*funcion|issue.*discount|promo.*n.*aplic|promo.*not.*apply|descuentos.*promo|descontos.*promo|discounts.*promos).*')
                   then 'Disagree with Charge|Promotions - Coupon|Customer'
               when regexp_like(lower(description),
                                '.*((d.vida|doubt|duda|pergunta|question|pregunta|n.*aplic|didn.*appl).*(cupo|coupon|disc.*nt|discount.*|promo)|(q.*ero.*usar|want.*use).*(cupon|coupon)).*')
                   then 'Disagree with Charge|Promotions - Coupon|Customer'
               when regexp_like(lower(description),
                                '.*((d.vida|doubt|duda|pergunta|question|pregunta|problem).*(cashback)|funciona.*cashback|cashback.*work).*')
                   then 'Disagree with Charge|Promotions - CashBack|Customer'
               when regexp_like(lower(description),
                                '.*((d.vida|doubt|duda|pergunta|question|pregunta).*rappicr.dit|(quiero saber|quero saber|want to know).*rappicr.dit).*')
                   then 'Disagree with Charge|Promotions - Rappi Credits|Customer'
               when regexp_like(lower(description),
                                '.*((duas|two|dos).*(cobran|charge|cobro)|(cobran|charge|cobro).*(duplic|double|twice)).*')
                   then 'Disagree with Charge|Other charges|Customer'
               when regexp_like(lower(description),
                                '.*((d.vida|doubt|duda|pergunta|question|pregunta).*(cobrança|charge|cobro|pagamento|payment).*|.*((pedido|order).*(cobra.*|charged))).*')
                   then 'Disagree with Charge|unidentified|Customer'
               when regexp_like(lower(description), '.*(cancel.*(sem custo|cobrad.*|with charge|sin costo)).*')
                   then 'Disagree with Charge|Cancelations|Customer'
               when regexp_like(lower(description),
                                '.*(cancel.*(com custo|cobrad|with charge|con costo)|(cobra|charge).*cancel|cancel.*(pedido|order).*(cobra|charge)).*')
                   then 'Disagree with Charge|Cancelations|Customer'
               when regexp_like(lower(description), '.*((por qu|why).*(pedido|order|orden|.*).*(cancel)).*')
                   then 'Disagree with Charge|Cancelations|Customer'
               when regexp_like(lower(description),
                                '.*((n.*recon|unrecongnize).*(cobrança|trans|collection|transaction|cobro)).*')
                   then 'Disagree with Charge|Unrecognized collection|Customer'
               when regexp_like(lower(description), '.*((saldo|balance).*(n.*corresp|incorrect)).*')
                   then 'Disagree with Charge|Debt|Customer'
        /* Customer - Account User */
               when regexp_like(lower(description),
                                '.*((cancel|excluir|elimin|delete).*(conta|usu.rio|account|user|cuenta)).*')
                   then 'Account User|Delete account|Customer'
               when regexp_like(lower(description), '.*((desativar|eliminar)*not|turn.*not|deshabilitar.*not).*')
                   then 'Account User|Disable notifications|Customer'
               when regexp_like(lower(description),
                                '.*((n|can.t).*(utilizar|usar|use).*(m.todo|method|payment|pagamento)).*')
                   then 'Account User|I can not use a payment method|Customer'
               when regexp_like(lower(description),
                                '.*((cadastr|register|registr).*(nov|new|nuevo).*(m.todo|pagamento|method|cart|card)).*')
                   then 'Account User|I can not register a new method|Customer'
               when regexp_like(lower(description),
                                '.*((alterar|adicionar).*endere.o|(change|add).*address|(cambiar|agregar).*direcci.n)')
                   then 'Account User|Change or add an address|Customer'
               when regexp_like(lower(description), '.*(cancel.*rappi.*prime.*)') then 'Account User|Cancel prime'
               when regexp_like(lower(description), '.*((conta|account|cuenta|usu.rio|user).*(bloque|block)).*')
                   then 'Account User|User blocked|Customer'
               when regexp_like(lower(description), '.*((n.*consigo|n.*puedo|can).*(pedido|order)).*')
                   then 'Account User|unidentified|Customer'
               when regexp_like(lower(description),
                                '.*(alterar.*e-mail|alterar.*telefone|change*e-mail|change*phone|cambiar.*correo|cambiar.*tel.efono)')
                   then 'Account User|I want to change my email|Customer'
               when regexp_like(lower(description),
                                '.*(seguran.a.*conta|account.*security|account.*safety|seguridad.*cuenta)')
                   then 'Account User|unidentified|Customer'
               when regexp_like(lower(description),
                                '.*(pricesmart.*(conta|account|cuenta)|(conta|account|cuenta).*pricesmart).*')
                   then 'Account User|Membership Pricesmart not valid|Customer'
        /* Customer - Problems with RT */
               when regexp_like(lower(description),
                                '.*((problem|incident|trouble).*(entregador|delivery|deliveryman|repartidor|rappitendero|rt)).*')
                   then 'Problems with RT|Issues with Rappi|Customer'
        /* Customer - New Bets */
               when regexp_like(lower(description), '.*((cancel).*(policy|poliza)).*')
                   then 'New Bets|(SOAT) Cancel Poliza|Customer'
               when regexp_like(lower(description), '.*((modific|modify).*(policy|poliza)).*')
                   then 'New Bets|(SOAT) Modify policy Facts|Customer'
               when regexp_like(lower(description), '.*((n.*recib.*poliza).*(policy.*received)).*')
                   then 'New Bets|(SOAT) No policy received ml|Customer'
               when regexp_like(lower(description), '.*(n.*receb.*ingresso|n.*received.*ticket|n.*recib.*boleto).*')
                   then 'New Bets|(Events) I cant see my event tickets|Customer'
               when regexp_like(lower(description), '.*(n.*acessar.*evento|n.*acceder.*evento|can.*access.*event).*')
                   then 'New Bets|(Events) Other|Customer'
               when regexp_like(lower(description),
                                '.*(cancelar.*ingresso|cancel.*ticket|cancel.*purchase|cancel.*compra).*')
                   then 'New Bets|(Events) Other|Customer'
               when regexp_like(lower(description),
                                '.*((punto|ponto).*([uú]ltima compra)|point.*last order|(punto|ponto).*rappirewards|rappirewards.*point).*')
                   then 'New Bets|(Rewards) I want to know how many points I got my last purchase|Customer'
        /*Customer - Travel*/
               when regexp_like(lower(description),
                                '.*((cambiar|alterar|change).*(reserva|reservation)|(reserva|reservation).*(cambio|alterar|modify)).*')
                   then 'Travel|I want to change my reservation|Customer'
               when regexp_like(lower(description), '.*((modifica|mudança|change).*(vuelo|voo|flight)).*')
                   then 'Travel|Flight changes|Customer'
               when regexp_like(lower(description), '.*(cancelar.*reserva|cancel.*reservation|cancelar.*reserva).*')
                   then 'Travel|How do I cancel my reservation?|Customer'
               when regexp_like(lower(description), '.*(cancelar.*boleto|cancel.*passagem|cancelar.*ticket).*')
                   then 'Travel|How do I cancel my ticket Bus?|Customer'
               when regexp_like(lower(description), '.*(alterar.*nota fiscal|change.*invoice|cambiar.*facturación).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description),
                                '.*((destino|aeroporto).*ajuda|(destination|airport).*(help|support)|(destino|aeropuerto).*(inconveniente|ayuda)).*')
                   then 'Travel|I''m on arrival or at the airport and have a drawback|Customer'
               when regexp_like(lower(description),
                                '.*((reserv|book|make).*(vuelo|voo|flight|hotel|hotel reservation|autob[uú]s|[oô]nibus|bus ticket)).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description), '.*(solicitar.*servi.os|book.*service|solicitar.*servicio).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description), '.*((n.*visualizar|n.*ver|can.t.*see).*(reserva|reservations)).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description), '.*((reserva|reservation).*(pend.*nte|pending)).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description), '.*((status|reservation).*(reserva|status)).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description),
                                '.*((qu.*receber|qu.*recibir|want.*receive).*(boleto|bilhete|ticket|voucher)).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description),
                                '.*(n.*recebi.*voucher|n.*recebi.*comprovante|n.*received.*voucher|n.*received.*proof of purchase|n.*recib.*voucher|n.*recib.*comprobante).*')
                   then 'Travel|Other|Customer'
               when regexp_like(lower(description), '.*((comprobante|comprovante|confirmation voucher).*(e.*mail)).*')
                   then 'Travel|Other|Customer'
        /* RT - Debt */
               when regexp_like(lower(description),
                                '.*((cancel|liber).*(pedido|order|orden).*(d.vida|debt|deuda)|(d.vida|debt|deuda).*(pedido|order|orden).*(cancel|liber)|(pedido|order|orden).*(cancel|liber)).*')
                   then 'Debt|Order canceled / released|RT'
               when regexp_like(lower(description),
                                '.*((d[ií]vida|debt|deuda).*(pedido|order|orden)|(tenho|tengo|n.*ent.*ndo).*(d[ií]vida|debt|deuda)).*')
                   then 'Debt|unidentified|RT'
               when regexp_like(lower(description),
                                '.*((pergunta|d[uú]vida|pregunta|duda|n.*ent.*ndo).*(d[ií]vida|deuda)).*')
                   then 'Debt|unidentified|RT'
               when regexp_like(lower(description), '.*(pag.*(d[ií]vida|deuda).*(refle|contabiliz)).*')
                   then 'Debt|Uncharged debt payment|RT'
               when regexp_like(lower(description), '.*(devoluc|return).*') then 'Debt|Returns|RT'
               when regexp_like(lower(description), '.*(donac|doa).*(producto|produto).*') then 'Debt|unidentified|RT'
        /* RT - Account RT */
               when regexp_like(lower(description), '.*((alter|exclu|cambi|elimin).*(dados|datos).*ap).*')
                   then 'Account RT|unidentified|RT'
               when regexp_like(lower(description), '.*(n.*rec.b.*not).*') then 'Account RT|unidentified|RT'
               when regexp_like(lower(description), '.*(erro.*tec).*') then 'Account RT|unidentified|RT'
               when regexp_like(lower(description), '.*((troc|camb).*(ve[ií]culo|carro|coche|moto)).*')
                   then 'Account RT|Vehicle (Motorcycle / Car)|RT'
        /* RT - Courier Blocked */
               when regexp_like(lower(description), '.*((bloqueo|block|inhabilitado).*permanent|(desativado)).*')
                   then 'Courier Blocked|Lock RT|RT'
        /* RT - COVID-19*/
               when regexp_like(lower(description), '.*((diagnosticad).*covid).*') then 'COVID 19|I have COVID - 19|RT'
               when regexp_like(lower(description), '.*((s.ntomas).*covid).*')
                   then 'COVID 19|I have symptoms of COVID - 19|RT'
        /* RT - Price Diff Dispersions*/
               when regexp_like(lower(description), '.*((n.*rec.b|didn.*receiv).*disper.*(pedido|order)).*')
                   then 'Price Diff / Dispersions|Dispersion emergency|RT'
               when regexp_like(lower(description), '.*((disper|liber).*(men|less).*(pedido|order|produ)).*')
                   then 'Price Diff / Dispersions|Dispersion emergency|RT'
               when regexp_like(lower(description), '.*((activac|ativa|erro|retir).*(tarjeta|cart)).*')
                   then 'Price Diff / Dispersions|RT with card problems|RT'
               when regexp_like(lower(description), '.*((diferencia|diferen).*(pre)).*')
                   then 'Price Diff / Dispersions|unidentified|RT'
        /* RT - Payments*/
               when regexp_like(lower(description),
                                '.*((no veo|n.*vejo).*(pedido|order|entrega).*(reali|f)|(sum|desaparec).*(pedido|orden)).*')
                   then 'RT Payment|RT does not see an order in profits|RT'
               when regexp_like(lower(description), '.*((pergunta|d[uú]vida|pregunta|duda).*(ganho|ganancia)).*')
                   then 'RT Payment|general inquiries weekly payments|RT'
               when regexp_like(lower(description), '.*(n.*rec.b.*(pag)).*')
                   then 'RT Payment|RT not received weekly payment|RT'
               when regexp_like(lower(description), '.*(smartmei).*')
                   then 'RT Payment|general inquiries weekly payments|RT'
               when regexp_like(lower(description), '(promociones|promoções)') then 'RT Payment|promotions|RT'
        /* RT - New RT*/
               when regexp_like(lower(description), '.*((n.*rec.b|didn.*receiv).*disper.*(pagu|paid).*(meu|mi|own)).*')
                   then 'New Rts|I did not receive the dispersion of this order / pay with my money|RT'
               when regexp_like(lower(description), '.*((disper|liber).*(men|less).*(pagu|paid).*(meu|mi|own)).*')
                   then 'New Rts|I scattered less than they were worth the products / paid with my money|RT'
        /* Pickers and Partners*/
               when regexp_like(lower(description), '.*(atendimento picker).*') then 'unidentified|unidentified|Picker'
               when regexp_like(lower(description), '.*(atendimento shopper).*')
                   then 'unidentified|unidentified|Shopper'
               when regexp_like(lower(description), '.*(atendimento.*(partner|parceiro)).*')
                   then 'unidentified|unidentified|Partner'
               when regexp_like(lower(description), '(bate-papo de resgate|rescue chat|chat de rescate)')
                   then 'unidentified|unidentified|Customer'
               else 'unidentified' end                                           as classifier
         , case
               when country in ('br', 'cl', 'ar', 'uy') then (created_at::timestamp_ntz - interval '5h')
               when country in ('co', 'pe', 'ec') then (created_at::timestamp_ntz - interval '7h')
               when country in ('mx', 'cr') then (created_at::timestamp_ntz - interval '8h')
               else created_at
        end                                                                      as created_at_local_country

    from (
             select country, order_id, created_at, user_type, channel, name, preview
             from CO_PG_MS_KUSTOMER_ETL_PUBLIC.conversations
             where created_at::date >= dateadd(day, -2, current_date())::date
             group by 1, 2, 3, 4, 5, 6,7   
         )
    where lower(country) in ('br', 'co', 'mx', 'pe', 'cl', 'ar', 'cr', 'ec', 'uy')
      and (case
               when country in ('br', 'cl', 'ar', 'uy') then (created_at::timestamp_ntz - interval '5h')
               when country in ('co', 'pe', 'ec') then (created_at::timestamp_ntz - interval '7h')
               when country in ('mx', 'cr') then (created_at::timestamp_ntz - interval '8h')
               else created_at::timestamp_ntz
        end)::date = (case
                          when country in ('co', 'pe', 'ec') then convert_timezone('America/Bogota', current_timestamp)::date
                          when country in ('br', 'cl', 'ar', 'uy')
                              then convert_timezone('America/Buenos_Aires', current_timestamp)::date
                          when country in ('cr', 'mx') then convert_timezone('America/Costa_Rica', current_timestamp)::date
                          when country in ('br', 'cl', 'ar', 'uy')
                              then convert_timezone('America/Costa_Rica', current_timestamp)::date
        end)::date
      and lower(user_type) = 'end_users'
      and (classifier in
           ('Order Never Arrived|Request not delivered|Customer','Incomplete or Damaged Orders|Missing product|Customer','Incomplete or Damaged Orders|Product in poor condition|Customer','Incomplete or Damaged Orders|different product|Customer')
        or classifier ilike '%Disagree with Charge%'
        )
)
select sb.name, sb.id::text as brand_id, '{country}' as country,
count(distinct o.id) as orders,
count(distinct d.order_id) as tickets,
tickets/orders as defect_rate,
count(distinct case when classifier ilike '%Disagree with Charge%' then d.order_id else null end)/count(distinct o.id) as doubts_with_charge,
count(distinct case when classifier ilike '%Missing%' then d.order_id else null end)/count(distinct o.id) as missing_item,
count(distinct case when classifier ilike '%different product%' then d.order_id else null end)/count(distinct o.id) as wrong_item,
count(distinct case when classifier ilike '%poor condition%' then d.order_id else null end)/count(distinct o.id) as damaged_item,
count(distinct case when classifier ilike '%Arrived%' then d.order_id else null end)/count(distinct o.id) as order_didnt_arrive,
listagg(distinct d.order_id, ', ') as order_ids,
s.store_id as store_id

from {country}_CORE_ORDERS_PUBLIC.orders_vw o
left join defects d on o.id::text=d.order_id and try_to_number(d.order_id)=true and d.country_code='{country}'
left join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id and coalesce(os._fivetran_deleted,'false')=false
join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=os.store_id and coalesce(s._fivetran_deleted,'false')=false
left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id and coalesce(sb._fivetran_deleted,'false')=false
left join  (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2) v on v.storetype=s.type
where
coalesce(o.closed_at,o.updated_at)::date =
                                          (case
                                           when country in ('co','pe','ec') then convert_timezone('America/Bogota',current_timestamp)::date
                                           when country in ('br','cl','ar','uy') then convert_timezone('America/Buenos_Aires',current_timestamp)::date
                                           when country in ('cr','mx') then convert_timezone('America/Costa_Rica',current_timestamp)::date
                                           end)::date
and coalesce(o._fivetran_deleted,'false')=false
and v.vertical in ('Ecommerce','Express','Farmacia','Licores')
group by 1,2,s.store_id

having defect_rate >= 0.05
and tickets >= 5
    """.format(country=country)

    df = snow.run_query(query)
    return df

def slack_alert(df,b):
    channel_alert = timezones.slack_channels('commercial_alarms')
    current_time = timezones.country_current_time(country)

    print(df)
    df = pd.merge(df,b,how='left',left_on=df['brand_id'],right_on=b['brandid'])
    df = df[df['brandid'].isnull()]
    df = df.drop(['key_0','brandid'], axis=1)

    df['alarm_at'] = current_time

    for index, row in df.iterrows():
        rows = dict(row)
        text = '''
            *Alarma de Defect Rate - :flag-{country}:  :alert:*
            Pais: {country}
            <!subteam^S018ASNLN5T>
            La Brand {brand} alcanzó {defect_rate}% de defect rate en el dia de hoy
            Ordenes: {orders}
            Tickets: {tickets}
            missing_item: {missing_item}%
            order_didnt_arrive: {order_didnt_arrive}%
            wrong_item: {wrong_item}%
            doubts_with_charge: {doubts_with_charge}%
            damaged_item: {damaged_item}%
            Orders IDs: {order_ids} '''.format(
                brand=row['name'],
                defect_rate=round(row['defect_rate']*100,2),
                order_ids=row['order_ids'],
                country=country,
                tickets=row['tickets'],
                missing_item=round(row['missing_item']*100,2),
                order_didnt_arrive=round(row['order_didnt_arrive']*100,2),
                wrong_item=round(row['wrong_item']*100,2),
                doubts_with_charge=round(row['doubts_with_charge']*100,2),
                damaged_item=round(row['damaged_item']*100,2),
                orders=row['orders'],
                )
        print(text)
        slack.bot_slack(text,channel_alert)
    df = df.rename(columns={"defect_rate": "metric"})
    to_upload = df[['country','brand_id','orders','tickets','metric','order_ids','store_id','alarm_at']]
    print(to_upload)
    snow.upload_df_occ(to_upload,'defect_rate')

def run_alarm(country):
    b = excluding_stores(country)
    df = get_offenders(country)
    slack_alert(df,b)

countries = ['mx','br','co','ar','cl','pe','uy','cr','ec']

for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)

