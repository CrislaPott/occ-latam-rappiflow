from lib import snowflake as snow
from lib import slack,redash
from functions import timezones
import pandas as pd
from os.path import expanduser

def logs():
    query = '''select distinct RETAILER_PRODUCT_ID as RETAILERPRODUCTID ,STORE_PRODUCT_ID as STOREPRODUCTID from ops_occ.product_ley_seca
where sync_at::date = current_date'''
    df = snow.run_query(query)
    return df

def products():
    query = '''
with tag_alcohol as (

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.CO_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.CO_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'


union all

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.BR_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.BR_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.MX_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.AR_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.AR_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.CL_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.CL_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.PE_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.PE_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.EC_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.EC_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.CR_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.CR_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

UNION ALL

select tag.country ,tag.name, S.id as retailer_product_id,S.product_id, tag.SELL_RESTRICTION:alcoholicBeverage as alcoholic
from FIVETRAN.UY_PGLR_MS_CPGS_CLG_PM_PUBLIC.RETAILER_PRODUCT as S
join FIVETRAN.UY_PGLR_MS_CPGS_CLG_PM_PUBLIC.product as tag
on tag.id = S.product_id
and tag.country = S.country
join cpgs_local_analytics.analytics_retailer_product as rtp  
on s.id = rtp.retailer_product_id
and s.country = rtp.country
and rtp.cat1_name = 'Bebidas'
where alcoholic = 'true'

  )

,


stores_enabled as (select DISTINCT 'BR' AS COUNTRY, store_id  from BR_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'AR' AS COUNTRY, store_id  from AR_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'CR' AS COUNTRY, store_id  from CR_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'CL' AS COUNTRY, store_id  from CL_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'CO' AS COUNTRY, store_id  from CO_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'EC' AS COUNTRY, store_id  from EC_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'MX' AS COUNTRY, store_id  from MX_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'PE' AS COUNTRY, store_id  from PE_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE

UNION ALL

select DISTINCT 'UY' AS COUNTRY, store_id  from UY_PGLR_MS_STORES_PUBLIC.STORES_VW
where is_enabled = TRUE)
,


pscp as
(
  select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
join mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
join mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from ar_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join ar_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join ar_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from cl_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join cl_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join cl_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from pe_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join pe_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join pe_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from ec_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join ec_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join ec_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from cr_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join cr_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join cr_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

union all

select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
from uy_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
join uy_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
join uy_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id

)

SELECT  DISTINCT A.*
       , B.RETAILER_PRODUCT_ID
        , tag_alcohol.product_id
       , tag_alcohol.NAME
       , B.STATUS
       , tag_alcohol.alcoholic
        , pscp.store_product_id

FROM CPGS_OPS_SHOPPER_GLOBAL.STORES_WITH_DRY_LAW_ACTIVE A

JOIN CPGS_OPS_SHOPPER_GLOBAL.PRIDUCT_STORE_CONTENT_PORTAL B
  ON A.COUNTRY = B.COUNTRY
 AND A.STORE_ID = B.STORE_ID
 AND B.STATUS = 'published'

JOIN STORES_ENABLED SE
  ON A.COUNTRY = SE.COUNTRY
  AND A.STORE_ID = SE.STORE_ID

JOIN tag_alcohol
  ON tag_alcohol.RETAILER_PRODUCT_ID::VARCHAR = B.RETAILER_PRODUCT_ID::VARCHAR
 AND tag_alcohol.COUNTRY::VARCHAR = B.COUNTRY::VARCHAR

left join pscp on upper(pscp.country)=a.country and pscp.store_id=a.store_id and b.retailer_product_id=pscp.product_id
left join (select id as task_id, is_active from mx_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from br_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from co_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from ar_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from cl_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from ec_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from uy_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from pe_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
union all
select id as task_id, is_active from cr_PG_MS_CMS_PRODUCT_STORE_PUBLIC.STORE_TAG_ACTIVE
where ENDS_AT::date >= current_date
) sta on sta.task_id::int = a.task_id::int
where sta.is_active = true
order by city, store_id
'''
    df = snow.run_query(query)
    return df

def products_oa(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select store_id::int as storeid, product_id::int as productid from override_availability
    	    where ends_at >= now() at time zone 'America/{timezone}'
    	    and (in_stock = False or status = 'unpublished')
    	    order by ends_at desc
    	    '''.format(timezone=timezone)

    if country == 'ar':
        df = redash.run_query(6519, query)
    elif country == 'br':
        df = redash.run_query(6520, query)
    elif country == 'cl':
        df = redash.run_query(6521, query)
    elif country == 'co':
        df = redash.run_query(6522, query)
    elif country == 'cr':
        df = redash.run_query(6523, query)
    elif country == 'ec':
        df = redash.run_query(6524, query)
    elif country == 'mx':
        df = redash.run_query(6526, query)
    elif country == 'pe':
        df = redash.run_query(6525, query)
    elif country == 'uy':
        df = redash.run_query(6527, query)

    return df

def alarm(a,b,c):
    channel_alert = timezones.slack_channels('tech_integration')
    current_time = timezones.country_current_time('co')

    df = pd.merge(a, b, how="left", left_on=['retailer_product_id', 'store_product_id'], right_on=['retailerproductid', 'storeproductid'])
    df = df[(df['retailerproductid'].isnull()) & (df['storeproductid'].isnull())]
    df = df.drop(['retailerproductid', 'storeproductid'], axis=1)
    print(df)

    c['productid'] = c['productid'].astype(int).astype(str)
    c['storeid'] = c['storeid'].astype(int).astype(str)
    df['product_id'] = df['product_id'].astype(str)
    df['store_id'] = df['store_id'].astype(str)

    df = pd.merge(df,c, how="left",left_on=['product_id', 'store_id'], right_on=['productid', 'storeid'])
    df = df[(df['productid'].isnull()) & (df['storeid'].isnull())]
    df = df.drop(['productid', 'storeid'], axis=1)
    print(df)
    if not df.empty:

        df['country'] = df['country'].str.lower()

        df = df[['retailer_product_id', 'name','alcoholic','status','store_id', 'store_name','task_id','vertical_sub_group','city','country','starts_at',
         'ends_at','store_product_id','product_id']]

        text = '''
        *Alarm Ley Seca* :alert:
        Nuevos productos que no se apagaron en tiendas com Ley Seca (Tag Alcohol)'''

        print(text)
        home = expanduser("~")
        results_file = '{}/details.xlsx'.format(home)
        df_final = df[['country','city','store_id','store_name','starts_at', 'ends_at','store_product_id','retailer_product_id']]
        df_final_excel = df[['country','city','store_id','store_name','starts_at', 'ends_at','store_product_id','retailer_product_id','name']]
        df_final_excel.to_excel(results_file, index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

        df_final['sync_at'] = current_time
        snow.upload_df_occ(df_final,'product_ley_seca')

    else:
        print("null")

countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']
df1 = []
for country in countries:
    try:
        df2 = products_oa(country)
        df1.append(df2)
        c = pd.concat(df1, ignore_index=True)
    except Exception as e:
        print(e)
print(c)
try:
    a = products()
    b = logs()
    alarm(a,b,c)
except Exception as e:
    print(e)