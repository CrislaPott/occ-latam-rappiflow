# -*- coding: utf-8 -*-
import os, sys, json, pytz, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
import matplotlib.pyplot as plt
from os.path import expanduser
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from dotenv import load_dotenv
from functions import timezones

def chat(order_ids):
	query_chat = """
	--no_cache
	select order_id, data as chat_evidence 
	from notifications
	where 
	order_id in ({order_ids})
	group by 1,2
	""".format(order_ids=order_ids)

	if country == 'co':
		chat = redash.run_query(652, query_chat)
	elif country == 'ar':
		chat = redash.run_query(647, query_chat)
	elif country == 'cl':
		chat = redash.run_query(649, query_chat)
	elif country == 'mx':
		chat = redash.run_query(651, query_chat)
	elif country == 'uy':
		chat = redash.run_query(650, query_chat)
	elif country == 'ec':
		chat = redash.run_query(2184, query_chat)
	elif country == 'cr':
		chat = redash.run_query(2183, query_chat)
	elif country == 'pe':
		chat = redash.run_query(745, query_chat)
	elif country == 'br':
		chat = redash.run_query(648, query_chat)
	return chat

def risks(country):
	timezone, interval = timezones.country_timezones(country)

	query = """
	--no_cahce
	with a as (select * 
	from risks 
	where created_at >= now()::date
	)
	select risk_id, count(distinct order_id) as risks,
	string_agg(distinct order_id::text,',') as order_ids
	from a r
	where upper(r.risk_id) in 
	('NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_IN_STORE_V3',
'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP_V3',
'NOTIFICATION_RT_RELEASE_ORDER_PICK_ADDRESS_LONG',
'NOTIFICATION_RT_RELEASE_ORDER_DOES_NOT_WANT_CAN_NOT_CONTINUE',
'NOTIFICATION_RT_SHOPPER_NOT_ANSWER',
'NOTIFICATION_RT_CUSTOMER_REQUEST_CHANGE_ADDRESS_V3',
'NOTIFICATION_RT_REPORT_PRICE_CHANGE_V3',
'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_ORDER_DOES_NOT_APPEAR',
'NOTIFICATION_RT_REQUEST_HELP_FOR_PAY_ORDER',
'NOTIFICATION_RT_CANNOT_WITHDRAW_MONEY_PROBLEM_WITH_ATM',
'NOTIFICATION_RT_PROBLEM_WITH_ORDER_PAYMENT_CUSTOMER_NOT_APPROVE_PRODUCTS_PRICE',
'NOTIFICATION_RT_CUSTOMER_REFUSE_TO_PAY_V3',
'NOTIFICATION_RT_PROBLEM_WITH_CLIENT_INTERACTION',
'CHAT_SUPPORT_CLIENT',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_CASHLESS_T',
'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ARRIVE',
'NOTIFICATION_RT_PRICE_DIFFERENCE_NOT_AUTHORIZED',
'NOTIFICATION_RT_RELEASE_ORDER',
'NOTIFICATION_RT_CUSTOMER_NOT_COME_OUT_V3',
'NOTIFICATION_RT_CUSTOMER_INCOMPLETE_V3_ADDRESS',
'NOTIFICATION_RT_RELEASE_ORDER_V3_ACCIDENT',
'MANUAL_ORDER_CALLCENTER_PARTNER',
'NOTIFICATION_RT_UNPROCESSED_ORDER',
'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ON_THE_ROUTE_V3',
'NOTIFICATION_RT_CUSTOMER_INCOMPLETE_ADDRESS',
'NOTIFICATION_RT_RELEASE_ORDER_DELIVERY_ADDRESS_LONG',
'NOTIFICATION_RT_RELEASE_ORDER_WITHOUT_CARD',
'NOTIFICATION_RT_HELP_WITH_LARGE_ORDER',
'NOTIFICATION_RT_USER_REQUEST_CANCEL_V3',
'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_LATE',
'NOTIFICATION_RT_OTHER_RT_PICKED_UP_MY_ORDER_V3',
'NOTIFICATION_RT_DISPERSION_INCOMPLETE_V3',
'NOTIFICATION_RT_PROBLEM_WITH_INVOICE',
'NOTIFICATION_RT_DOES_NOT_HAVE_MONEY',
'NOTIFICATION_RT_PROBLEM_WITH_CLIENT',
'NOTIFICATION_RT_PROBLEM_WITH_CLIENT_NOT_HAVE_ID',
'NOTIFICATION_RT_DISPERSION',
'NOTIFICATION_PERSONALSHOPPER_ORDER_WITHOUT_RT',
'NOTIFICATION_RT_CUSTOMER_NOT_RESPOND_V2',
'NOTIFICATION_RT_DISPERSION_NOT_RECEIVED_V3',
'NOTIFICATION_RT_RELEASE_ORDER_ACCIDENT',
'NOTIFICATION_RT_RELEASE_ORDER_SHIPPING_LOW',
'NOTIFICATION_RT_RELEASE_ORDER_BY_CONSIGNMENT_OR_BY_IMPROPER',
'NOTIFICATION_RT_NEED_PAY_ORDER',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_V3',
'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_NOT_RELEASED_ORDER',
'NOTIFICATION_RT_FRAUDULENT_PRODUCT',
'NOTIFICATION_RT_CARD_DOES_NOT_WORK_V3',
'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE_V3',
'NOTIFICATION_RT_STORE_RECEIVES_ONLY_CASH_V3',
'NOTIFICATION_RT_CUSTOMER_REQUEST_HELP_VERIFICATION_CODE',
'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE_SHOPPER_V3',
'RT_REQUEST_HELP_VERIFICATION_CODE',
'NOTIFICATION_RT_ARRIVED_CUSTOMER_NOT_COME_OUT',
'NOTIFICATION_RT_ORDER_NOT_CLOSED',
'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ARRIVE_V3',
'CHAT_SUPPORT_STOREKEEPER',
'CHAT_SUPPORT_PERSONALSHOPPER',
'NOTIFICATION_FIND_RT',
'NOTIFICATION_RT_NOT_ANSWER',
'SK_DOES_NOT_RESPOND_TO_ALERTS',
'CHAT_TABLET_SUPPORT',
'NOTIFICATION_ORDER_WITHOUT_RT',
'ETA_RT_NOT_ON_ROUTE',
'NOTIFICATION_RT_ORDER_NOT_CLOSED_V3',
'NOTIFICATION_CUSTOMER_RT_NOT_ANSWER',
'NOTIFICATION_CUSTOMER_FIND_RT',
'NOTIFICATION_PARTNER_SK_DELAY',
'ETA_ORDER_WITHOUT_RT_10',
'ETA_RT_IN_STORE_AFTER_COOKING_TIME',
'RT_IN_STORE_AFTER_SHOPPER_REQUEST_WITH_DELAY',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE',
'NOTIFICATION_RT_REQUEST_TO_CANCEL_STORE_CLOSED',
'NOTIFICATION_RT_RECHARGE',
'NOTIFICATION_RT_DOUBTS_ABOUT_PAYMENTS',
'NOTIFICATION_RT_PAYMENT_METHOD',
'NOTIFICATION_RT_CANCEL',
'NOTIFICATION_RT_OTHER',
'NOTIFICATION_RT_CHAT_HISTORY',
'NOTIFICATION_RT_CUSTOMER_NOT_COME_OUT',
'NOTIFICATION_RT_CUSTOMER_NOT_RESPOND',
'NOTIFICATION_RT_SUSPECTED_FRAUD_ORDER',
'NOTIFICATION_RT_DISPERSION_NOT_RECEIVED',
'NOTIFICATION_PARTNER_ORDER_WITHOUT_SK',
'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP_T',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_NO_CASHLESS_T',
'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE',
'NOTIFICATION_RT_CUSTOMER_NOT_AVAILABLE_TO_RECEIVE_ORDER_T',
'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ON_THE_ROUTE',
'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_IN_STORE',
'NOTIFICATION_RT_CUSTOMER_REFUSE_TO_PAY',
'NOTIFICATION_RT_STORE_NOT_FOUND',
'RT_COMING_TO_USER',
'NOTIFICATION_RT_STORE_RECEIVES_ONLY_CASH',
'NOTIFICATION_RT_CANCEL_REASON_CHANGE_STORE',
'NOTIFICATION_RT_USER_REQUESTS_CANCEL_CHANGE_ADDRESS',
'NOTIFICATION_RT_USER_REQUESTS_CANCEL_CHANGE_PAYMENT_METHOD',
'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP',
'NOTIFICATION_RT_USER_REQUEST_CANCEL_PRODUCT_NOT_AVAILABLE',
'NOTIFICATION_RT_USER_REQUEST_CANCEL_OTHER',
'NOTIFICATION_RT_CUSTOMER_NOT_AVAILABLE_TO_RECEIVE_ORDER',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_NO_CASHLESS',
'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_CASHLESS',
'NOTIFICATION_RT_CARD_DOES_NOT_WORK',
'CALL_CENTER_PARTNER',
'NOTIFICATION_RT_DISPERSION_INCOMPLETE',
'THIRD_PARTY_ASSIGN',
'NOTIFICATION_RT_OTHER_RT_PICKED_UP_MY_ORDER',
'NOTIFICATION_CUSTOMER_CONTACT_SUPPORT',
'NOTIFICATION_PARTNER_PRODUCT_TOPPING_NOT_AVAILABLE',
'NOTIFICATION_RT_CUSTOMER_REQUEST_CHANGE_ADDRESS',
'NOTIFICATION_RT_ADDRESS_NOT_FOUND',
'NOTIFICATION_RT_CANCEL_REASON_CHANGE_ADDRESS',
'NOTIFICATION_RT_RECHARGE_STORE_NOT_FOUND',
'NOTIFICATION_RT_RECHARGE_DISTANCE',
'NOTIFICATION_RT_RECHARGE_COMPLEXITY',
'NOTIFICATION_RT_USER_REQUEST_CANCEL',
'NOTIFICATION_PARTNER_UPDATE_PRODUCT_PRICE_CONTENT',
'NOTIFICATION_PARTNER_ORDER_CHARGE_INQUIRY',
'NOTIFICATION_PARTNER_ORDER_WRONG_SK',
'NOTIFICATION_PARTNER_SK_NO_DELIVERY_KIT')
and created_at at time zone 'America/{timezone}' >= now() at time zone 'America/{timezone}' - interval '15 minutes'
group by 1
	""".format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(959, query)
	elif country == 'ar':
		metrics = redash.run_query(113, query)
	elif country == 'cl':
		metrics = redash.run_query(468, query)
	elif country == 'mx':
		metrics = redash.run_query(71, query)
	elif country == 'uy':
		metrics = redash.run_query(500, query)
	elif country == 'ec':
		metrics = redash.run_query(2259, query)
	elif country == 'cr':
		metrics = redash.run_query(2261, query)
	elif country == 'pe':
		metrics = redash.run_query(730, query)
	elif country == 'br':
		metrics = redash.run_query(33, query)

	return metrics

def historic(country):
	timezone, interval = timezones.country_timezones(country)
	interval = interval.replace('h', '')
	print(interval)

	query = f'''
	--no_cahce
	with historic as (select
	dateadd(hour,-{interval},created_at)::date as day,
	risk_id,
	count(distinct r.order_id) as risks
	from
	{country}_pg_ms_risk_calculator_public.risks r
	where
	upper(r.risk_id) in (
	'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_IN_STORE_V3'
	, 'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP_V3'
	, 'NOTIFICATION_RT_RELEASE_ORDER_PICK_ADDRESS_LONG'
	, 'NOTIFICATION_RT_RELEASE_ORDER_DOES_NOT_WANT_CAN_NOT_CONTINUE'
	, 'NOTIFICATION_RT_SHOPPER_NOT_ANSWER'
	, 'NOTIFICATION_RT_CUSTOMER_REQUEST_CHANGE_ADDRESS_V3'
	, 'NOTIFICATION_RT_REPORT_PRICE_CHANGE_V3'
	, 'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_ORDER_DOES_NOT_APPEAR'
	, 'NOTIFICATION_RT_REQUEST_HELP_FOR_PAY_ORDER'
	, 'NOTIFICATION_RT_CANNOT_WITHDRAW_MONEY_PROBLEM_WITH_ATM'
	, 'NOTIFICATION_RT_PROBLEM_WITH_ORDER_PAYMENT_CUSTOMER_NOT_APPROVE_PRODUCTS_PRICE'
	, 'NOTIFICATION_RT_CUSTOMER_REFUSE_TO_PAY_V3'
	, 'NOTIFICATION_RT_PROBLEM_WITH_CLIENT_INTERACTION'
	, 'CHAT_SUPPORT_CLIENT'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_CASHLESS_T'
	, 'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ARRIVE'
	, 'NOTIFICATION_RT_PRICE_DIFFERENCE_NOT_AUTHORIZED'
	, 'NOTIFICATION_RT_RELEASE_ORDER'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_COME_OUT_V3'
	, 'NOTIFICATION_RT_CUSTOMER_INCOMPLETE_V3_ADDRESS'
	, 'NOTIFICATION_RT_RELEASE_ORDER_V3_ACCIDENT'
	, 'MANUAL_ORDER_CALLCENTER_PARTNER'
	, 'NOTIFICATION_RT_UNPROCESSED_ORDER'
	, 'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ON_THE_ROUTE_V3'
	, 'NOTIFICATION_RT_CUSTOMER_INCOMPLETE_ADDRESS'
	, 'NOTIFICATION_RT_RELEASE_ORDER_DELIVERY_ADDRESS_LONG'
	, 'NOTIFICATION_RT_RELEASE_ORDER_WITHOUT_CARD'
	, 'NOTIFICATION_RT_HELP_WITH_LARGE_ORDER'
	, 'NOTIFICATION_RT_USER_REQUEST_CANCEL_V3'
	, 'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_LATE'
	, 'NOTIFICATION_RT_OTHER_RT_PICKED_UP_MY_ORDER_V3'
	, 'NOTIFICATION_RT_DISPERSION_INCOMPLETE_V3'
	, 'NOTIFICATION_RT_PROBLEM_WITH_INVOICE'
	, 'NOTIFICATION_RT_DOES_NOT_HAVE_MONEY'
	, 'NOTIFICATION_RT_PROBLEM_WITH_CLIENT'
	, 'NOTIFICATION_RT_PROBLEM_WITH_CLIENT_NOT_HAVE_ID'
	, 'NOTIFICATION_RT_DISPERSION'
	, 'NOTIFICATION_PERSONALSHOPPER_ORDER_WITHOUT_RT'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_RESPOND_V2'
	, 'NOTIFICATION_RT_DISPERSION_NOT_RECEIVED_V3'
	, 'NOTIFICATION_RT_RELEASE_ORDER_ACCIDENT'
	, 'NOTIFICATION_RT_RELEASE_ORDER_SHIPPING_LOW'
	, 'NOTIFICATION_RT_RELEASE_ORDER_BY_CONSIGNMENT_OR_BY_IMPROPER'
	, 'NOTIFICATION_RT_NEED_PAY_ORDER'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_V3'
	, 'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_NOT_RELEASED_ORDER'
	, 'NOTIFICATION_RT_FRAUDULENT_PRODUCT'
	, 'NOTIFICATION_RT_CARD_DOES_NOT_WORK_V3'
	, 'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE_V3'
	, 'NOTIFICATION_RT_STORE_RECEIVES_ONLY_CASH_V3'
	, 'NOTIFICATION_RT_CUSTOMER_REQUEST_HELP_VERIFICATION_CODE'
	, 'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE_SHOPPER_V3'
	, 'RT_REQUEST_HELP_VERIFICATION_CODE'
	, 'NOTIFICATION_RT_ARRIVED_CUSTOMER_NOT_COME_OUT'
	, 'NOTIFICATION_RT_ORDER_NOT_CLOSED'
	, 'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ARRIVE_V3'
	, 'CHAT_SUPPORT_STOREKEEPER'
	, 'CHAT_SUPPORT_PERSONALSHOPPER'
	, 'NOTIFICATION_FIND_RT'
	, 'NOTIFICATION_RT_NOT_ANSWER'
	, 'SK_DOES_NOT_RESPOND_TO_ALERTS'
	, 'CHAT_TABLET_SUPPORT'
	, 'NOTIFICATION_ORDER_WITHOUT_RT'
	, 'ETA_RT_NOT_ON_ROUTE'
	, 'NOTIFICATION_RT_ORDER_NOT_CLOSED_V3'
	, 'NOTIFICATION_CUSTOMER_RT_NOT_ANSWER'
	, 'NOTIFICATION_CUSTOMER_FIND_RT'
	, 'NOTIFICATION_PARTNER_SK_DELAY'
	, 'ETA_ORDER_WITHOUT_RT_10'
	, 'ETA_RT_IN_STORE_AFTER_COOKING_TIME'
	, 'RT_IN_STORE_AFTER_SHOPPER_REQUEST_WITH_DELAY'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE'
	, 'NOTIFICATION_RT_REQUEST_TO_CANCEL_STORE_CLOSED'
	, 'NOTIFICATION_RT_RECHARGE'
	, 'NOTIFICATION_RT_DOUBTS_ABOUT_PAYMENTS'
	, 'NOTIFICATION_RT_PAYMENT_METHOD'
	, 'NOTIFICATION_RT_CANCEL'
	, 'NOTIFICATION_RT_OTHER'
	, 'NOTIFICATION_RT_CHAT_HISTORY'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_COME_OUT'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_RESPOND'
	, 'NOTIFICATION_RT_SUSPECTED_FRAUD_ORDER'
	, 'NOTIFICATION_RT_DISPERSION_NOT_RECEIVED'
	, 'NOTIFICATION_PARTNER_ORDER_WITHOUT_SK'
	, 'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP_T'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_NO_CASHLESS_T'
	, 'NOTIFICATION_RT_REQUEST_HELP_VERIFICATION_CODE'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_AVAILABLE_TO_RECEIVE_ORDER_T'
	, 'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_ON_THE_ROUTE'
	, 'NOTIFICATION_RT_UNABLE_TO_CHANGE_STATE_IN_STORE'
	, 'NOTIFICATION_RT_CUSTOMER_REFUSE_TO_PAY'
	, 'NOTIFICATION_RT_STORE_NOT_FOUND'
	, 'RT_COMING_TO_USER'
	, 'NOTIFICATION_RT_STORE_RECEIVES_ONLY_CASH'
	, 'NOTIFICATION_RT_CANCEL_REASON_CHANGE_STORE'
	, 'NOTIFICATION_RT_USER_REQUESTS_CANCEL_CHANGE_ADDRESS'
	, 'NOTIFICATION_RT_USER_REQUESTS_CANCEL_CHANGE_PAYMENT_METHOD'
	, 'NOTIFICATION_RT_CUSTOMER_ADDRESS_NOT_MATCH_WITH_MAP'
	, 'NOTIFICATION_RT_USER_REQUEST_CANCEL_PRODUCT_NOT_AVAILABLE'
	, 'NOTIFICATION_RT_USER_REQUEST_CANCEL_OTHER'
	, 'NOTIFICATION_RT_CUSTOMER_NOT_AVAILABLE_TO_RECEIVE_ORDER'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_NO_CASHLESS'
	, 'NOTIFICATION_RT_PRODUCT_NOT_AVAILABLE_CASHLESS'
	, 'NOTIFICATION_RT_CARD_DOES_NOT_WORK'
	, 'CALL_CENTER_PARTNER'
	, 'NOTIFICATION_RT_DISPERSION_INCOMPLETE'
	, 'THIRD_PARTY_ASSIGN'
	, 'NOTIFICATION_RT_OTHER_RT_PICKED_UP_MY_ORDER'
	, 'NOTIFICATION_CUSTOMER_CONTACT_SUPPORT'
	, 'NOTIFICATION_PARTNER_PRODUCT_TOPPING_NOT_AVAILABLE'
	, 'NOTIFICATION_RT_CUSTOMER_REQUEST_CHANGE_ADDRESS'
	, 'NOTIFICATION_RT_ADDRESS_NOT_FOUND'
	, 'NOTIFICATION_RT_CANCEL_REASON_CHANGE_ADDRESS'
	, 'NOTIFICATION_RT_RECHARGE_STORE_NOT_FOUND'
	, 'NOTIFICATION_RT_RECHARGE_DISTANCE'
	, 'NOTIFICATION_RT_RECHARGE_COMPLEXITY'
	, 'NOTIFICATION_RT_USER_REQUEST_CANCEL'
	, 'NOTIFICATION_PARTNER_UPDATE_PRODUCT_PRICE_CONTENT'
	, 'NOTIFICATION_PARTNER_ORDER_CHARGE_INQUIRY'
	, 'NOTIFICATION_PARTNER_ORDER_WRONG_SK'
	, 'NOTIFICATION_PARTNER_SK_NO_DELIVERY_KIT')
	and date_trunc(week,(dateadd('hour', -{interval}, created_at))::timestamp_ntz)::date >= dateadd(week,-4,date_trunc(week,convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz))::date
	and extract(dow from dateadd('hour', -{interval}, created_at)) = extract(dow from convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz)
	and to_char(dateadd('hour', -{interval}, created_at),'HH24:MI') >=
	to_char(convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz - interval '15 minutes','HH24:MI')
	and to_char(dateadd('hour', -{interval}, created_at),'HH24:MI') <=
	to_char(convert_timezone('America/{timezone}',current_timestamp())::timestamp_ntz,'HH24:MI')
	group by 1,2
	)
	select risk_id, avg(risks) as avg, stddev(risks) as stddev, (avg+(4*stddev)) as "3stddev" from historic group by 1
	'''
	df = snow.run_query(query)
	return df

def orders(country):
	timezone, interval = timezones.country_timezones(country)

	query_orders = """
	  --no_cache
	  with base as
	  (
	  SELECT distinct order_id
	  FROM order_modifications om
	  WHERE om.created_at >= now() AT TIME ZONE 'America/{timezone}' - interval '120 minutes'
	  GROUP BY 1
	  )
	  select os.store_id::text as store_id,
	  base.order_id, 
	  max(os.name) as store_name, 
	  max(os.type) as store_type
	  from base
	  inner join orders o on o.id=base.order_id 
	  inner join order_stores os on os.order_id = o.id 
	  group by 1,2
	  """.format(countrt=country, timezone=timezone)

	if country == 'co':
		orders = redash.run_query(1904, query_orders)
	elif country == 'ar':
		orders = redash.run_query(1337, query_orders)
	elif country == 'cl':
		orders = redash.run_query(1155, query_orders)
	elif country == 'mx':
		orders = redash.run_query(1371, query_orders)
	elif country == 'uy':
		orders = redash.run_query(1156, query_orders)
	elif country == 'ec':
		orders = redash.run_query(1922, query_orders)
	elif country == 'cr':
		orders = redash.run_query(1921, query_orders)
	elif country == 'pe':
		orders = redash.run_query(1157, query_orders)
	elif country == 'br':
		orders = redash.run_query(1338, query_orders)

	return orders

def slack_alert(b,c):

	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('rt_operation')
	if not b.empty:
		a = historic(country)
		a['risk_id']=a['risk_id'].astype(str)
		b['risk_id'] = b['risk_id'].astype(str)
		df = pd.merge(a,b,how='inner',on=['risk_id'])
		df = df[(df['risk_id'].isin(['NOTIFICATION_RT_CARD_DOES_NOT_WORK_V3','NOTIFICATION_RT_DISPERSION_NOT_RECEIVED_V3',
				'NOTIFICATION_RT_FRAUDULENT_PRODUCT','NOTIFICATION_RT_PROBLEM_WITH_ORDER_PAYMENT_CUSTOMER_NOT_APPROVE_PRODUCTS_PRICE',
				'NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_NOT_RELEASED_ORDER','NOTIFICATION_RT_PROBLEMS_WITH_RESTAURANT_ORDER_DOES_NOT_APPEAR',
				'NOTIFICATION_RT_RELEASE_ORDER_V3_ACCIDENT','NOTIFICATION_RT_USER_REQUEST_CANCEL_V3']))]
		final_df = df[(df['risks'] > df['3stddev']) & (df['risks'] >= 12)]
		print(final_df)
		if not final_df.empty:
			def explode_str(df, col, sep):
				s = df[col]
				i = np.arange(len(s)).repeat(s.str.count(sep) + 1)
				return df.iloc[i].assign(**{col: sep.join(s).split(sep)})
			df_open = explode_str(final_df, 'order_ids', ',')
			df_open['order_ids'] = df_open['order_ids'].astype(str)
			c['order_id'] = c['order_id'].astype(str)
			df_open1 = pd.merge(df_open, c, how='left', left_on=df_open['order_ids'], right_on=c['order_id'])
			df_open1 = df_open1.drop(['order_id','store_name','store_type'], axis=1)
			df0 = df_open1.groupby(['risk_id', 'avg', 'stddev', '3stddev', 'risks'])['order_ids'].agg(
					[('order_ids', ', '.join)]).reset_index()
			df1 = df_open1.groupby(['risk_id', 'avg', 'stddev', '3stddev', 'risks'])['store_id'].agg(
					[('store_ids', ', '.join)]).reset_index()
			df_final = pd.merge(df0,df1, on=['risk_id', 'avg', 'stddev', '3stddev', 'risks'])


			to_upload = df_final
			to_upload['alarm_at'] = current_time
			to_upload['country'] = country
			to_upload = to_upload[['country','alarm_at','risk_id','risks','stddev','3stddev','order_ids','store_ids']]
			snow.upload_df_occ(to_upload,'risks_alarm')

			for index, row in df_final.iterrows():
				rows = dict(row)
				order_ids = row['order_ids']
				print(order_ids)
				chat1 = chat(order_ids)
				summary = chat1['chat_evidence']
				all_summary = " ".join(s for s in summary)
				stopwords = set(STOPWORDS)
				stopwords.update(["llego","RT","llegando","xfavor","amigo","me","hora","ayudas","ajudar","support","favor","amables","llevo","pena","El","debido","cerca","del","lugar","quedé","deme","rapy","rappi","compañero","llegue","compra","llegué","deja","disculpen","adentro","llegar","llevó","hice","ayuden","quiero","queria","via","vía","hacerme","hacer","colaboran","minutos","soporte","disculpe","pedí","colaboracion","acabo","irlo","mientras","por","favor","porfavor","perro","gracias","noche","noches","señores","quisiera","urgente","ayudas","porfa","favor","dicienda","continuar","r","s","solucionan","busca","ah","hola","pedido","ayuda","disculpa","necessito","necesito","orden","ordene","ola","ajuda","colaboren","local","aceite","noche","días","buenos","dias","buenas","buena","tardes","tarde","da", "meu", "em", "você", "de", "ao", "os","de","a","o","que","e","do","da","em","um","para","com","não","uma","os","no","se","na","por","mais","as","dos","como","mas","ao","ele","das","à","seu","sua","ou","quando","muito","nos","já","eu","também","só","pelo","pela","até","isso","ela","entre","depois","sem","mesmo","aos","seus","quem","nas","me","esse","eles","você","essa","num","nem","suas","meu","às","minha","numa","pelos","elas","qual","nós","lhe","deles","essas","esses","pelas","este","dele","tu","te","vocês","vos","lhes","meus","minhas","teu","tua","teus","tuas","nosso","nossa","nossos","nossas","dela","delas","esta","estes","estas","aquele","aquela","aqueles","aquelas","isto","aquilo","estou","está","estamos","estão","estive","esteve","estivemos","estiveram","estava","estávamos","estavam","estivera","estivéramos","esteja","estejamos","estejam","estivesse","estivéssemos","estivessem","estiver","estivermos","estiverem","hei","há","havemos","hão","houve","houvemos","houveram","houvera","houvéramos","haja","hajamos","hajam","houvesse","houvéssemos","houvessem","houver","houvermos","houverem","houverei","houverá","houveremos","houverão","houveria","houveríamos","houveriam","sou","somos","são","é","era","éramos","eram","fui","foi","fomos","foram","fora","fôramos","seja","sejamos","sejam","fosse","fôssemos","fossem","for","formos","forem","ser","serei","será","seremos","serão","seria","seríamos","seriam","tenho","tem","temos","tém","tinha","tínhamos","tinham","tive","teve","tivemos","tiveram","tivera","tivéramos","tenha","tenhamos","tenham","tivesse","tivéssemos","tivessem","tiver","tivermos","tiverem","terei","terá","teremos","terão","teria","teríamos","teriam","fazer","faço","faz","fazemos","fazem","fizemos","fizeram","faremos","farão","dizer","digo","diz","dizem","disse","dissemos","disseram","direi","diremos","dirão","ir","vou","vai","vamos","vêm","vinha","vinham","fui","foi","fomos","foram","irei","irá","iremos","irão","porquê","porque","sobre","apenas","ainda","exactamente","bem","mal","bom","mau","todo","toda","todos","todas","hoje","amanhã","ontem","aqui","onde","vez","quer","sr","srs","sra","srª","sras","senhor","senhora","senhores","senhoras","um","uma","dois","duas","três","quatro","cinco","neste","ter","têm","assim","peço","pode","antes","caso","nesta","meses","deve","cada","agora","portanto","quanto","menos","desta","quais","á","tudo","nada","sempre","dr","favor","exª","deste","pois","faça","então","vós","alguns","algumas","passamos","desde","gostaria","falar","exatamente","1","2","3","4","5","6","7","8","9","a","actualmente","acuerdo","adelante","ademas","además","adrede","afirmó","agregó","ahi","ahora","ahí","al","algo","alguna","algunas","alguno","algunos","algún","alli","allí","alrededor","ambos","ampleamos","antano","antaño","ante","anterior","antes","apenas","aproximadamente","aquel","aquella","aquellas","aquello","aquellos","aqui","aquél","aquélla","aquéllas","aquéllos","aquí","arriba","arribaabajo","aseguró","asi","así","atras","aun","aunque","ayer","añadió","aún","b","bajo","bastante","bien","breve","buen","buena","buenas","bueno","buenos","c","cada","casi","cerca","cierta","ciertas","cierto","ciertos","cinco","claro","comentó","como","con","conmigo","conocer","conseguimos","conseguir","considera","consideró","consigo","consigue","consiguen","consigues","contigo","contra","cosas","creo","cual","cuales","cualquier","cuando","cuanta","cuantas","cuanto","cuantos","cuatro","cuenta","cuál","cuáles","cuándo","cuánta","cuántas","cuánto","cuántos","cómo","d","da","dado","dan","dar","de","debajo","debe","deben","debido","decir","dejó","del","delante","demasiado","demás","dentro","deprisa","desde","despacio","despues","después","detras","detrás","dia","dias","dice","dicen","dicho","dieron","diferente","diferentes","dijeron","dijo","dio","donde","dos","durante","día","días","dónde","e","ejemplo","el","ella","ellas","ello","ellos","embargo","empleais","emplean","emplear","empleas","empleo","en","encima","encuentra","enfrente","enseguida","entonces","entre","era","erais","eramos","eran","eras","eres","es","esa","esas","ese","eso","esos","esta","estaba","estabais","estaban","estabas","estad","estada","estadas","estado","estados","estais","estamos","estan","estando","estar","estaremos","estará","estarán","estarás","estaré","estaréis","estaría","estaríais","estaríamos","estarían","estarías","estas","este","estemos","esto","estos","estoy","estuve","estuviera","estuvierais","estuvieran","estuvieras","estuvieron","estuviese","estuvieseis","estuviesen","estuvieses","estuvimos","estuviste","estuvisteis","estuviéramos","estuviésemos","estuvo","está","estábamos","estáis","están","estás","esté","estéis","estén","estés","ex","excepto","existe","existen","explicó","expresó","f","fin","final","fue","fuera","fuerais","fueran","fueras","fueron","fuese","fueseis","fuesen","fueses","fui","fuimos","fuiste","fuisteis","fuéramos","fuésemos","g","general","gran","grandes","gueno","h","ha","haber","habia","habida","habidas","habido","habidos","habiendo","habla","hablan","habremos","habrá","habrán","habrás","habré","habréis","habría","habríais","habríamos","habrían","habrías","habéis","había","habíais","habíamos","habían","habías","hace","haceis","hacemos","hacen","hacer","hacerlo","haces","hacia","haciendo","hago","han","has","hasta","hay","haya","hayamos","hayan","hayas","hayáis","he","hecho","hemos","hicieron","hizo","horas","hoy","hube","hubiera","hubierais","hubieran","hubieras","hubieron","hubiese","hubieseis","hubiesen","hubieses","hubimos","hubiste","hubisteis","hubiéramos","hubiésemos","hubo","i","igual","incluso","indicó","informo","informó","intenta","intentais","intentamos","intentan","intentar","intentas","intento","ir","j","junto","k","l","la","lado","largo","las","le","lejos","les","llegó","lleva","llevar","lo","los","luego","lugar","m","mal","manera","manifestó","mas","mayor","me","mediante","medio","mejor","mencionó","menos","menudo","mi","mia","mias","mientras","mio","mios","mis","misma","mismas","mismo","mismos","modo","momento","mucha","muchas","mucho","muchos","muy","más","mí","mía","mías","mío","míos","n","nada","nadie","ni","ninguna","ningunas","ninguno","ningunos","ningún","no","nos","nosotras","nosotros","nuestra","nuestras","nuestro","nuestros","nueva","nuevas","nuevo","nuevos","nunca","o","ocho","os","otra","otras","otro","otros","p","pais","para","parece","parte","partir","pasada","pasado","paìs","peor","pero","pesar","poca","pocas","poco","pocos","podeis","podemos","poder","podria","podriais","podriamos","podrian","podrias","podrá","podrán","podría","podrían","poner","por","por","qué","porque","posible","primer","primera","primero","primeros","principalmente","pronto","propia","propias","propio","propios","proximo","próximo","próximos","pudo","pueda","puede","pueden","puedo","pues","q","qeu","que","quedó","queremos","quien","quienes","quiere","quiza","quizas","quizá","quizás","quién","quiénes","qué","r","raras","realizado","realizar","realizó","repente","respecto","s","sabe","sabeis","sabemos","saben","saber","sabes","sal","salvo","se","sea","seamos","sean","seas","segun","segunda","segundo","según","seis","ser","sera","seremos","será","serán","serás","seré","seréis","sería","seríais","seríamos","serían","serías","seáis","señaló","si","sido","siempre","siendo","siete","sigue","siguiente","sin","sino","sobre","sois","sola","solamente","solas","solo","solos","somos","son","soy","soyos","su","supuesto","sus","suya","suyas","suyo","suyos","sé","sí","sólo","t","tal","tambien","también","tampoco","tan","tanto","tarde","te","temprano","tendremos","tendrá","tendrán","tendrás","tendré","tendréis","tendría","tendríais","tendríamos","tendrían","tendrías","tened","teneis","tenemos","tener","tenga","tengamos","tengan","tengas","tengo","tengáis","tenida","tenidas","tenido","tenidos","teniendo","tenéis","tenía","teníais","teníamos","tenían","tenías","tercera","ti","tiempo","tiene","tienen","tienes","toda","todas","todavia","todavía","todo","todos","total","trabaja","trabajais","trabajamos","trabajan","trabajar","trabajas","trabajo","tras","trata","través","tres","tu","tus","tuve","tuviera","tuvierais","tuvieran","tuvieras","tuvieron","tuviese","tuvieseis","tuviesen","tuvieses","tuvimos","tuviste","tuvisteis","tuviéramos","tuviésemos","tuvo","tuya","tuyas","tuyo","tuyos","tú","u","ultimo","un","una","unas","uno","unos","usa","usais","usamos","usan","usar","usas","uso","usted","ustedes","v","va","vais","valor","vamos","van","varias","varios","vaya","veces","ver","verdad","verdadera","verdadero","vez","vosotras","vosotros","voy","vuestra","vuestras","vuestro","vuestros","w","x","y","ya","yo","z","él","éramos","ésa","ésas","ése","ésos","ésta","éstas","éste","éstos","última","últimas","último","últimos"])
				wordcloud = WordCloud(stopwords=stopwords, background_color='white', width=800, height=800).generate(all_summary)
				fig, ax = plt.subplots(figsize=(80,40))
				ax.imshow(wordcloud, interpolation='bilinear')
				ax.set_axis_off()
				plt.imshow(wordcloud)
				home = expanduser("~")
				results_file2 = '{}/wordcloud.png'.format(home)
				wordcloud.to_file(results_file2)
				text2='''
				*Alarma riesgos :alert: :flag-{country}:*
				Pais: {country}
				El riesgo "{risk_id}" tiene {risks} ordenes
				Order IDs: {order_ids}
				Store IDs: {store_ids}
				Promédio ultimas semanas: {avg}
				Desviación típica: {stddev}
				'''.format(country=country,
						risk_id=row['risk_id'],
						risks=row['risks'],
						order_ids=row['order_ids'],
						store_ids=row['store_ids'],
						avg = round(row['avg'],2),
						stddev = round(row['stddev'],2)
							)
				print(text2)
				slack.file_upload_channel(channel_alert,text2,results_file2,'png')
				if os.path.exists(results_file2):
					os.remove(results_file2)
		else:
			print('final_df null')
	else:
		print('b null')

def run_alarm(country):

	b=risks(country)
	c = orders(country)
	slack_alert(b,c)

countries = ['mx','br','co','ar','cl','pe','uy','ec','cr']
for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)