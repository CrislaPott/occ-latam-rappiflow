import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

load_dotenv()

def limbo(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
    --no_cache
    BEGIN;
    SET LOCAL timezone = 'UTC';
    with release_to_shopper as (
    select order_id,
       min(case when name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') then (created_at) else null end) as not_shopper,
       min(case when name in ('order_to_sh','order_released','release_to_shopper','released_to_shopper') then (created_at) else null end) as try_to_shopper,
       count(distinct case when name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper') then created_at else null end) as qtd
       from order_events
    where
    created_at >= now() - interval  '6 hours'
    and name in ('not_assigned_personal_shopper_retry_out','not_assigned_personal_shopper','order_to_sh','order_released','release_to_shopper','released_to_shopper')
    group by 1
    )

    , base as (select rts.order_id, 
                      physical_store_id, 
                      not_shopper, try_to_shopper
                      state, try_to_shopper,qtd
               from release_to_shopper rts
               inner join order_summary os on os.order_id=rts.order_id
               where os.state not in ('canceled_for_payment_error','canceled_by_split_error', 'canceled_by_fraud','canceled_by_early_regret')
               and try_to_shopper is not null
               and os.store_type_store not in ('rappi_pay','soat_webview')
               )

    select physical_store_id,
    count(distinct case when not_shopper is not null and qtd >=5 then order_id else null end) as shopper_problens, 
    count(distinct order_id) as total_orders,
    string_agg(distinct case when not_shopper is not null and qtd >=5 then order_id::text else null end,',') as order_ids, 
    count(distinct case when not_shopper is not null and qtd >=5 then order_id else null end)::float/count(distinct order_id)::float as percentage

    from base
    where extract('hour' from try_to_shopper - interval '{interval}')::int = extract('hour' from now() at time zone 'America/{timezone}' - interval  '1 hours')::int
    group by 1
    having count(distinct case when not_shopper is not null and qtd >=5 then order_id else null end) >= 3
    and count(distinct case when not_shopper is not null and qtd >=5 then order_id else null end)::float/count(distinct order_id)::float >= 0.20
 
    """.format(country=country,timezone=timezone, interval=interval)

    if country == 'co':
        metrics = redash.run_query(2449, query)
    elif country == 'ar':
        metrics = redash.run_query(2430, query)
    elif country == 'cl':
        metrics = redash.run_query(2431, query)
    elif country == 'mx':
        metrics = redash.run_query(2450, query)
    elif country == 'uy':
        metrics = redash.run_query(2426, query)
    elif country == 'ec':
        metrics = redash.run_query(2559, query)
    elif country == 'cr':
        metrics = redash.run_query(2552, query)
    elif country == 'pe':
        metrics = redash.run_query(2448, query)
    elif country == 'br':
        metrics = redash.run_query(2447, query)

    return metrics

def alarm(country):
    channel_alert = timezones.slack_channels('trend_alarms')

    df = limbo(country)
    print(df)
    print(df.columns)
    if not df.empty:
        current_time = timezones.country_current_time(country)
        to_upload = df
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        to_upload = to_upload[['physical_store_id','total_orders','shopper_problens','order_ids','alarm_at','country']]
        snow.upload_df_occ(to_upload,'limbo_pickers')


        text = '''
        *Alarma de Limbo (shopper/Picker offline - problemas de asignación) - :flag-{country}:*
        Pais: {country}
        Nuevas tiendas con ordenes con problemas para asignar el shopper/Picker alcanzaron el threshold.
        '''.format(country=country)
        print(text)
        home = expanduser("~")
        results_file = '{}/details_limbo_pickers_{country}.xlsx'.format(home, country=country)
        df_final = df[['physical_store_id', 'shopper_problens', 'total_orders', 'order_ids',
       'percentage']]
        df_final.to_excel(results_file, sheet_name='orders', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")


    else:
        print('df empty')

countries = ['br','co','pe','ec','cr','mx','cl','uy','ar']
for country in countries:
    print(country)
    try:
        alarm(country)
        print('Alarme desativado')
    except Exception as e:
        print(e)

