import os, sys, json
import pandas as pd
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def reported_products(country):
	timezone, interval = timezones.country_timezones(country)

	query = f"""
	select distinct product_id::text as product_id, store_id::text as store_id, 'no' as flag, country 
	from ops_occ.product_price_errors
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	group by 1,2,3,4
	"""
	df = snow.run_query(query)
	return df

def data(country):
	timezone, interval = timezones.country_timezones(country)

	query_latam = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.store_id not in (900125904,900048603)
	and os.store_id != 0
	and os.type not in ('grin','rentbrella','sampling_card_rt_test','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview','mensajeria')
	and os.type not ilike '%flow%'
	and os.type not ilike 'soat%'
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price = 1)
	and op.product_id != 0
	and opd.name not ilike '%ackhams%'
and opd.name not ilike '%erejil%'
and opd.name not ilike '%lacitas%'
and opd.name not ilike '%runa%'
and opd.name not ilike '%ackus%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%arshmellow%'
and opd.name not ilike '%inimellows%'
	and opd.name not ilike '%091927422%'
	and opd.name not ilike '%enter%'
	and opd.name not ilike '%all%'
	and opd.name not ilike '%nual%'
	and opd.name not ilike '%et%'
	and opd.name not ilike '%hocolate%'
	and opd.name not ilike '%omate%'
	and opd.name not ilike '%gua%'
	and opd.name not ilike '%olden%'
	and opd.name not ilike '%light%'
	and opd.name not ilike '%ebada%'
	and opd.name not ilike '%imiento%'
	and opd.name not ilike '%lanco%'
	and opd.name not ilike '%olpe%'
	and opd.name not ilike '%hocolate%'
	and opd.name not ilike '%afer%'
	and opd.name not ilike '%arra%'
	and opd.name not ilike '%gua%'
	and opd.name not ilike '%anuto%'
	and opd.name not ilike '%alillo%'
	and opd.name not ilike '%oconut%'
	and opd.name not ilike '%altin%'
	and opd.name not ilike '%elatina%'
	and opd.name not ilike '%ndrews%'
	and opd.name not ilike '%entholatum%'
    and opd.name not ilike '%icante%'
    and opd.name not ilike '%omitas%'
    and opd.name not ilike '%uates%'
    and opd.name not ilike '%aseosa%'
    and opd.name not ilike '%anuto%'
    and opd.name not ilike '%eposito%'
    and opd.name not ilike '%icaras%'
    and opd.name not ilike '%repe%'
and opd.name not ilike '%apel%'
and opd.name not ilike '%reo%'
and opd.name not ilike '%alleta%'
and opd.name not ilike '%wist%'
and opd.name not ilike '%hizitos%'
and opd.name not ilike '%lasica%'
and opd.name not ilike '%alls%'
and opd.name not ilike '%hicles%'
and opd.name not ilike '%andwich%'
and opd.name not ilike '%ayonesa%'
and opd.name not ilike '%asless%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%loria%'
and opd.name not ilike '%ebida%'
and opd.name not ilike '%essfrill%'
and opd.name not ilike '%imbo%'
and opd.name not ilike '%latuzym%'
and opd.name not ilike '%ax%'
and opd.name not ilike '%enpirinox%'
and opd.name not ilike '%mbrosoli%'
and opd.name not ilike '%itz%'
and opd.name not ilike '%ueso%'
and opd.name not ilike '%antene%'
and opd.name not ilike '%hampoo%'
and opd.name not ilike '%oloacemifen%'
and opd.name not ilike '%sponja%'
and opd.name not ilike '%atorade%'
and opd.name not ilike '%enter%'
and opd.name not ilike '%all%'
and opd.name not ilike '%etergente%'
and opd.name not ilike '%te%'
and opd.name not ilike '%elado%'
and opd.name not ilike '%enutrit%'
and opd.name not ilike '%eche%'
and opd.name not ilike '%enter%'
and opd.name not ilike '%rutilla%'
and opd.name not ilike '%hocolate%'
and opd.name not ilike '%ondimesa%'
and opd.name not ilike '%ogurt%'
and opd.name not ilike '%ideo%'
and opd.name not ilike '%nstantaneo%'
and opd.name not ilike '%nacake%'
and opd.name not ilike '%ainilla%'
and opd.name not ilike '%alak%'
and opd.name not ilike '%elleno%'
and opd.name not ilike '%enteja%'
and opd.name not ilike '%oca%'
and opd.name not ilike '%anduche%'
and opd.name not ilike '%ermelada%'
and opd.name not ilike '%aquita%'
and opd.name not ilike '%rodicereal%'
and opd.name not ilike '%anguil%'
and opd.name not ilike '%ola%'
and opd.name not ilike '%riginal%'
and opd.name not ilike '%roma%'
and opd.name not ilike '%spinaca%'
and opd.name not ilike '%oni%'
and opd.name not ilike '%kittles%'
and opd.name not ilike '%andia%'
and opd.name not ilike '%hirstea%'
and opd.name not ilike '%nergizante%'
and opd.name not ilike '%arnecitas%'
and opd.name not ilike '%argarina%'
and opd.name not ilike '%olgate%'
and opd.name not ilike '%ierba%'
and opd.name not ilike '%angay%'
and opd.name not ilike '%inta?%'
and opd.name not ilike '%dhisiva%'
and opd.name not ilike '%apita%'
and opd.name not ilike '%lanca%'
and opd.name not ilike '%uquini%'
and opd.name not ilike '%erde%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%ornimans%'
and opd.name not ilike '%nny%'
and opd.name not ilike '%entos%'
and opd.name not ilike '%ollo%'
and opd.name not ilike '%enta%'
and opd.name not ilike '%oral%'
and opd.name not ilike '%rroz%'
and opd.name not ilike '%lumrose%'
and opd.name not ilike '%nergizante%'
and opd.name not ilike '%oni%'
and opd.name not ilike '%ogurt%'
and opd.name not ilike '%vena%'
and opd.name not ilike '%luconazol%'
and opd.name not ilike '%enta%'
and opd.name not ilike '%ascarilla%'
and opd.name not ilike '%anguil%'
and opd.name not ilike '%ervilleta%'
and opd.name not ilike '%izcocho%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%arquillo%'
and opd.name not ilike '%andwich%'
and opd.name not ilike '%osta%'
and opd.name not ilike '%hocman%'
and opd.name not ilike '%olillo%'
and opd.name not ilike '%ilantro%'
and opd.name not ilike '%pan%'
and opd.name not ilike '%an%'
and opd.name not ilike '%ortilla%'
and opd.name not ilike '%ulces%'
and opd.name not ilike '%iruela%'
and opd.name not ilike '%ebolla%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%oga%'
and opd.name not ilike '%ascar%'
and opd.name not ilike '%rident%'
and opd.name not ilike '%ajo%'
and opd.name not ilike '%anahoria%'
and opd.name not ilike '%aranja%' 
and opd.name not ilike '%anzana%'
and opd.name not ilike '%oghurt %'
and opd.name not ilike '%efresco%'
and opd.name not ilike '%ang%'
and opd.name not ilike '%oing%'
and opd.name not ilike '%hapata%'
and opd.name not ilike '%opa%'
and opd.name not ilike '%rano%'
and opd.name not ilike '%uerno%'
and opd.name not ilike '%rejas%'
and opd.name not ilike '%alsa%'
and opd.name not ilike '%al%'
and opd.name not ilike '%ugo%'
and opd.name not ilike '%ulce%'
and opd.name not ilike '%alapeÃ±o%'
and opd.name not ilike '%celga%'
and opd.name not ilike '%ncendedor%'
and opd.name not ilike '%elon%'
and opd.name not ilike '%aco%'
and opd.name not ilike '%ostitos%'
and opd.name not ilike '%alabaza%'
and opd.name not ilike '%arina%'
and opd.name not ilike '%icarbonato%'
and opd.name not ilike '%amaica%' 
and opd.name not ilike '%eine?%'
and opd.name not ilike '%asta%'
and opd.name not ilike '%olsa%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%andarina%'
and opd.name not ilike '%rijol%'

and opd.name not ilike '%ona%'
and opd.name not ilike '%ellogg%'
and opd.name not ilike '%lastico%'
and opd.name not ilike '%acapuntas%'
and opd.name not ilike '%emilla%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%aizena%'
and opd.name not ilike '%udu%'
and opd.name not ilike '%oplait%'
and opd.name not ilike '%amarindo%'
and opd.name not ilike '%amito%'
and opd.name not ilike '%tros%'
and opd.name not ilike '%erejil%'
and opd.name not ilike '%ectar%'
and opd.name not ilike '%enda%'
and opd.name not ilike '%uberculos%'
and opd.name not ilike '%ebollita%'
and opd.name not ilike '%olvoroncitos%'
and opd.name not ilike '%icama%'
and opd.name not ilike '%erenjena%'
and opd.name not ilike '%grio%'
and opd.name not ilike '%ecula%'
and opd.name not ilike '%ombricitas%'
and opd.name not ilike '%lorfenamina%'
and opd.name not ilike '%omatillo%'
and opd.name not ilike '%rispies%'
and opd.name not ilike '%ice%'
and opd.name not ilike '%ugo%'
and opd.name not ilike '%ulce%'
and opd.name not ilike '%oodles%'
and opd.name not ilike '%ubbaloo%'
and opd.name not ilike '%auce%'
and opd.name not ilike '%urazno%'
and opd.name not ilike '%roissant%'
and opd.name not ilike '%oncha%'
and opd.name not ilike '%ulparindots%'
and opd.name not ilike '%chweppes%'
and opd.name not ilike '%lamesi%'
and opd.name not ilike '%eslactosado%'
and opd.name not ilike '%hinos%'
and opd.name not ilike '%eringa%'
and opd.name not ilike '%ipodermica%'
and opd.name not ilike '%era%'
and opd.name not ilike '%aramelos%'
and opd.name not ilike '%aff%'
and opd.name not ilike '%erao%'
and opd.name not ilike '%ugus%'
and opd.name not ilike '%asticables%'
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	query_latam_3decimal = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.store_id not in (900125904,900048603)
	and os.store_id != 0
	and os.type not in ('grin','rentbrella','sampling_card_rt_test','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview','mensajeria')
	and os.type not ilike '%flow%'
	and os.type not ilike 'soat%'
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price <= 10 and op.unit_price > 0)
	and op.product_id != 0
	and opd.name not ilike '%ackhams%'
	and opd.name not ilike '%era%'
and opd.name not ilike '%aramelos%'
and opd.name not ilike '%aff%'
and opd.name not ilike '%erao%'
and opd.name not ilike '%ugus%'
and opd.name not ilike '%asticables%'
and opd.name not ilike '%erejil%'
and opd.name not ilike '%lacitas%'
and opd.name not ilike '%runa%'
and opd.name not ilike '%ackus%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%arshmellow%'
and opd.name not ilike '%inimellows%'
	and opd.name not ilike '%091927422%'
	and opd.name not ilike '%enter%'
	and opd.name not ilike '%all%'
	and opd.name not ilike '%nual%'
	and opd.name not ilike '%et%'
	and opd.name not ilike '%hocolate%'
	and opd.name not ilike '%omate%'
	and opd.name not ilike '%gua%'
	and opd.name not ilike '%olden%'
	and opd.name not ilike '%light%'
	and opd.name not ilike '%ebada%'
	and opd.name not ilike '%imiento%'
	and opd.name not ilike '%lanco%'
	and opd.name not ilike '%olpe%'
	and opd.name not ilike '%hocolate%'
	and opd.name not ilike '%afer%'
	and opd.name not ilike '%arra%'
	and opd.name not ilike '%gua%'
	and opd.name not ilike '%anuto%'
	and opd.name not ilike '%alillo%'
	and opd.name not ilike '%oconut%'
	and opd.name not ilike '%altin%'
	and opd.name not ilike '%elatina%'
	and opd.name not ilike '%ndrews%'
	and opd.name not ilike '%entholatum%'
    and opd.name not ilike '%icante%'
    and opd.name not ilike '%omitas%'
    and opd.name not ilike '%uates%'
    and opd.name not ilike '%aseosa%'
    and opd.name not ilike '%anuto%'
    and opd.name not ilike '%eposito%'
    and opd.name not ilike '%icaras%'
    and opd.name not ilike '%repe%'
and opd.name not ilike '%apel%'
and opd.name not ilike '%reo%'
and opd.name not ilike '%alleta%'
and opd.name not ilike '%wist%'
and opd.name not ilike '%hizitos%'
and opd.name not ilike '%lasica%'
and opd.name not ilike '%alls%'
and opd.name not ilike '%hicles%'
and opd.name not ilike '%andwich%'
and opd.name not ilike '%ayonesa%'
and opd.name not ilike '%asless%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%loria%'
and opd.name not ilike '%ebida%'
and opd.name not ilike '%essfrill%'
and opd.name not ilike '%imbo%'
and opd.name not ilike '%latuzym%'
and opd.name not ilike '%ax%'
and opd.name not ilike '%enpirinox%'
and opd.name not ilike '%mbrosoli%'
and opd.name not ilike '%itz%'
and opd.name not ilike '%ueso%'
and opd.name not ilike '%antene%'
and opd.name not ilike '%hampoo%'
and opd.name not ilike '%oloacemifen%'
and opd.name not ilike '%sponja%'
and opd.name not ilike '%atorade%'
and opd.name not ilike '%enter%'
and opd.name not ilike '%all%'
and opd.name not ilike '%etergente%'
and opd.name not ilike '%te%'
and opd.name not ilike '%elado%'
and opd.name not ilike '%enutrit%'
and opd.name not ilike '%eche%'
and opd.name not ilike '%enter%'
and opd.name not ilike '%rutilla%'
and opd.name not ilike '%hocolate%'
and opd.name not ilike '%ondimesa%'
and opd.name not ilike '%ogurt%'
and opd.name not ilike '%ideo%'
and opd.name not ilike '%nstantaneo%'
and opd.name not ilike '%nacake%'
and opd.name not ilike '%ainilla%'
and opd.name not ilike '%alak%'
and opd.name not ilike '%elleno%'
and opd.name not ilike '%enteja%'
and opd.name not ilike '%oca%'
and opd.name not ilike '%anduche%'
and opd.name not ilike '%ermelada%'
and opd.name not ilike '%aquita%'
and opd.name not ilike '%rodicereal%'
and opd.name not ilike '%anguil%'
and opd.name not ilike '%ola%'
and opd.name not ilike '%riginal%'
and opd.name not ilike '%roma%'
and opd.name not ilike '%spinaca%'
and opd.name not ilike '%oni%'
and opd.name not ilike '%kittles%'
and opd.name not ilike '%andia%'
and opd.name not ilike '%hirstea%'
and opd.name not ilike '%nergizante%'
and opd.name not ilike '%arnecitas%'
and opd.name not ilike '%argarina%'
and opd.name not ilike '%olgate%'
and opd.name not ilike '%ierba%'
and opd.name not ilike '%angay%'
and opd.name not ilike '%inta?%'
and opd.name not ilike '%dhisiva%'
and opd.name not ilike '%apita%'
and opd.name not ilike '%lanca%'
and opd.name not ilike '%uquini%'
and opd.name not ilike '%erde%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%ornimans%'
and opd.name not ilike '%nny%'
and opd.name not ilike '%entos%'
and opd.name not ilike '%ollo%'
and opd.name not ilike '%enta%'
and opd.name not ilike '%oral%'
and opd.name not ilike '%rroz%'
and opd.name not ilike '%lumrose%'
and opd.name not ilike '%nergizante%'
and opd.name not ilike '%oni%'
and opd.name not ilike '%ogurt%'
and opd.name not ilike '%vena%'
and opd.name not ilike '%luconazol%'
and opd.name not ilike '%enta%'
and opd.name not ilike '%ascarilla%'
and opd.name not ilike '%anguil%'
and opd.name not ilike '%ervilleta%'
and opd.name not ilike '%izcocho%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%arquillo%'
and opd.name not ilike '%andwich%'
and opd.name not ilike '%osta%'
and opd.name not ilike '%hocman%'
and opd.name not ilike '%olillo%'
and opd.name not ilike '%ilantro%'
and opd.name not ilike '%pan%'
and opd.name not ilike '%an%'
and opd.name not ilike '%ortilla%'
and opd.name not ilike '%ulces%'
and opd.name not ilike '%iruela%'
and opd.name not ilike '%ebolla%'
and opd.name not ilike '%latano%'
and opd.name not ilike '%oga%'
and opd.name not ilike '%ascar%'
and opd.name not ilike '%rident%'
and opd.name not ilike '%ajo%'
and opd.name not ilike '%anahoria%'
and opd.name not ilike '%aranja%' 
and opd.name not ilike '%anzana%'
and opd.name not ilike '%oghurt %'
and opd.name not ilike '%efresco%'
and opd.name not ilike '%ang%'
and opd.name not ilike '%oing%'
and opd.name not ilike '%hapata%'
and opd.name not ilike '%opa%'
and opd.name not ilike '%rano%'
and opd.name not ilike '%uerno%'
and opd.name not ilike '%rejas%'
and opd.name not ilike '%alsa%'
and opd.name not ilike '%al%'
and opd.name not ilike '%ugo%'
and opd.name not ilike '%ulce%'
and opd.name not ilike '%alapeÃ±o%'
and opd.name not ilike '%celga%'
and opd.name not ilike '%ncendedor%'
and opd.name not ilike '%elon%'
and opd.name not ilike '%aco%'
and opd.name not ilike '%ostitos%'
and opd.name not ilike '%alabaza%'
and opd.name not ilike '%arina%'
and opd.name not ilike '%icarbonato%'
and opd.name not ilike '%amaica%' 
and opd.name not ilike '%eine?%'
and opd.name not ilike '%asta%'
and opd.name not ilike '%olsa%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%andarina%'
and opd.name not ilike '%rijol%'

and opd.name not ilike '%ona%'
and opd.name not ilike '%ellogg%'
and opd.name not ilike '%lastico%'
and opd.name not ilike '%acapuntas%'
and opd.name not ilike '%emilla%'
and opd.name not ilike '%imon%'
and opd.name not ilike '%aizena%'
and opd.name not ilike '%udu%'
and opd.name not ilike '%oplait%'
and opd.name not ilike '%amarindo%'
and opd.name not ilike '%amito%'
and opd.name not ilike '%tros%'
and opd.name not ilike '%erejil%'
and opd.name not ilike '%ectar%'
and opd.name not ilike '%enda%'
and opd.name not ilike '%uberculos%'
and opd.name not ilike '%ebollita%'
and opd.name not ilike '%olvoroncitos%'
and opd.name not ilike '%icama%'
and opd.name not ilike '%erenjena%'
and opd.name not ilike '%grio%'
and opd.name not ilike '%ecula%'
and opd.name not ilike '%ombricitas%'
and opd.name not ilike '%lorfenamina%'
and opd.name not ilike '%omatillo%'
and opd.name not ilike '%rispies%'
and opd.name not ilike '%ice%'
and opd.name not ilike '%ugo%'
and opd.name not ilike '%ulce%'
and opd.name not ilike '%oodles%'
and opd.name not ilike '%ubbaloo%'
and opd.name not ilike '%auce%'
and opd.name not ilike '%urazno%'
and opd.name not ilike '%roissant%'
and opd.name not ilike '%oncha%'
and opd.name not ilike '%ulparindots%'
and opd.name not ilike '%chweppes%'
and opd.name not ilike '%lamesi%'
and opd.name not ilike '%eslactosado%'
and opd.name not ilike '%hinos%'
and opd.name not ilike '%eringa%'
and opd.name not ilike '%ipodermica%'
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	query_br = """
	--no_cache
	with a as (select order_id,store_id, name, type 
	from order_stores os
	join orders o on o.id=os.order_id
	where o.created_at >= date(now() at time zone 'America/{timezone}')
	and os.store_id not in (900125904,900048603)
	and os.type not in ('grin','rentbrella','rappi_prime','restaurant','courier_hours','courier_sampling','soat_webview')
	and os.type not ilike '%flow%'
	and os.store_id != 0
	and os.type not ilike '%rappi%pay%'
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error'))
	select
	os.store_id::text as store_id,
	os.name,
	os.type,
	op.PRODUCT_ID::text as product_id,
	opd.name as product_name, op.sale_type,
	op.unit_price as unit_price_in_order,
	count(distinct os.order_id) as orders,
	string_agg(distinct os.order_id::text,',') as order_ids
	from a os
	left join ORDER_PRODUCT op on op.order_id = os.order_id
	left join order_product_detail opd on opd.ORDER_PRODUCT_id=op.id
	where (op.unit_price = 1) and op.PRODUCT_ID not in (2089975713,2099967410)
	and op.product_id != 0
	and opd.name not ilike 'Sacola%' and opd.name not ilike '%Refresco%'and opd.name not ilike '%Cebol%' and opd.name not ilike '%Biscoito%' and opd.name not ilike '%Cenoura%' and opd.name not ilike '%Laranja%' and opd.name not ilike '%Banana%' and opd.name not ilike '%Batata%' and opd.name not ilike '%Beterraba%' and opd.name not ilike '%gua%' and opd.name not ilike '%lface%' and opd.name not ilike '%ang%' and opd.name not ilike '%oentro%' and opd.name not ilike '%efrigerante%' and opd.name not ilike 'Sal%' and opd.name not ilike '%hocolat%' and opd.name not ilike '%heiro%' and opd.name not ilike '%ouve%' and opd.name not ilike '%sponja%' and opd.name not ilike '%elatina%' and opd.name not ilike 'Color%' and opd.name not ilike '%acarr%' and opd.name not ilike '%olho%' and opd.name not ilike 'LimÃ£o%' and opd.name not ilike '%alito%' and opd.name not ilike 'Bombom%' and opd.name not ilike '%ebida%' and opd.name not ilike '%Sabonete%' and opd.name not ilike 'Vinagre%' and opd.name not ilike 'Pepino%' and opd.name not ilike 'Melancia%' and opd.name not ilike 'Hortel%' and opd.name not ilike '%Knorr%'and opd.name not ilike '%Abobrinha%'and opd.name not ilike '%amendoi%' and opd.name not ilike '%Bauducco%' and opd.name not ilike '%Suco%' and opd.name not ilike 'Aveia%' and opd.name not ilike 'Coco%' and opd.name not ilike '%urguer%' and opd.name not ilike '%PÃ£o%' and opd.name not ilike '%arinha%' and opd.name not ilike '%omate%' and opd.name not ilike '%Wafer%' and opd.name not ilike '%Polvilho%' and opd.name not ilike '%Pipoca%' and opd.name not ilike '%nÃ©ctar%' and opd.name not ilike '%eite%' and opd.name not ilike 'Bicabornato%' and opd.name not ilike 'Hamb%'
	and opd.name not ilike 'Burger%' and opd.name not ilike '%milho%' and opd.name not ilike '%irulito%' and opd.name not ilike '%huchu%' and opd.name not ilike '%urry%' and opd.name not ilike '%ermento%' and opd.name not ilike 'Limao%' and opd.name not ilike 'Aipim%' and opd.name not ilike 'MamÃ£o%' and opd.name not ilike 'FubÃ¡%' and opd.name not ilike 'MelÃ£o%' and opd.name not ilike '%Colorifico%' and opd.name not ilike 'Mostarda%' and opd.name not ilike '%Polpa%' and opd.name not ilike 'Polpa%' and opd.name not ilike '%Bombom%' and opd.name not ilike '%Colorifico' and opd.name not ilike 'Arisco%' and opd.name not ilike '%PaÃ§oca%' and opd.name not ilike '%Coco%' and opd.name not ilike 'Pimenta%' and opd.name not ilike '%Cerea%' and opd.name not ilike 'Envelope%' and opd.name not ilike '%Salgadinho%' and opd.name not ilike '%Caldo%' and opd.name not ilike '%Chicle%' and opd.name not ilike '%Agulha%' and opd.name not ilike '%ColorÃ­fico' and opd.name not ilike '%Macarr%' and opd.name not ilike '%PaÃ§oquita' and opd.name not ilike '%Detergente%' and opd.name not ilike '%Sal' and opd.name not ilike '%Maionese%' and opd.name not ilike '%ColorÃ­fico%' and opd.name not ilike 'RÃºcula' and opd.name not ilike '%Hortel%' and opd.name not ilike '%LimÃ£o' and opd.name not ilike '%Burger' and opd.name not ilike '%Assolan' and opd.name not ilike 'Pratos%' and opd.name not ilike '%Prato%' and opd.name not ilike '%Sal%' and opd.name not ilike '%Bombo%' and opd.name not ilike '%PÃ³%' and opd.name not ilike '%Assolan%' and opd.name not ilike 'AÃ§Ãºcar%' and opd.name not ilike 'PilÃ£o Yellow Finger' and opd.name not ilike '%Bolinho%' and opd.name not ilike '%InstantÃ¢neo%' and opd.name not ilike 'PicolÃ©%' and opd.name not ilike '%PicolÃ©%' and opd.name not ilike '%Bicarbonato%' and opd.name not ilike '%Desodor%' and opd.name not ilike '%Bolo%' and opd.name not ilike 'Valda%' and opd.name not ilike '%Gaze%' and opd.name not ilike '%Gr%o%' and opd.name not ilike '%erinjela'
	and opd.name not ilike '%SACO%PRESENTE%' and opd.name not ilike '%luva%' and opd.name not ilike '%lava%lou%' and opd.name not ilike '%Baterraba%'
	and opd.name not ilike '%Seringa%' and opd.name not ilike '%PinÃ§a%' and opd.name not ilike '%Berinjela%' 
	and opd.name not ilike '%po%refres%' and opd.name not ilike '%lixa%' and opd.name not ilike '%bala%' and opd.name not ilike '%Popcorn%' and opd.name not ilike '%cerv%lata%'
	and opd.name not ilike '%piment%o%' and opd.name not ilike '%Iogurte%' and opd.name not ilike '%Leve%Brisa%Naftalina%'
	and opd.name not ilike '%Soro%Fisiol%gico%5 ml%'
	group by
	1,2,3,4,5,6,7
	""".format(countrt=country,timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(1904, query_latam_3decimal)
	elif country == 'ar':
		metrics = redash.run_query(1337, query_latam_3decimal)
	elif country == 'cl':
		metrics = redash.run_query(1155, query_latam_3decimal)
	elif country == 'mx':
		metrics = redash.run_query(1371, query_latam_3decimal)
	elif country == 'uy':
		metrics = redash.run_query(1156, query_latam)
	elif country == 'ec':
		metrics = redash.run_query(1922, query_latam)
	elif country == 'cr':
		metrics = redash.run_query(1921, query_latam)
	elif country == 'pe':
		metrics = redash.run_query(1157, query_latam)
	elif country == 'br':
		metrics = redash.run_query(1338, query_br)

	return metrics

def offenders(df,rp):
	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('partner_operation')

	print(df)
	if not df.empty:
		merged = pd.merge(df, rp, how='left', on=['product_id','store_id'])
		news = merged[merged['flag'].isnull()]

		final = news[['product_id','product_name','store_id','name','type','orders','unit_price_in_order','order_ids']]
		to_upload = final[['product_id','store_id','unit_price_in_order','order_ids']]
		to_upload['alarm_at'] = current_time
		to_upload['country'] = country
		to_upload = to_upload[['product_id','store_id','unit_price_in_order','alarm_at','country','order_ids']]

		snow.upload_df_occ(to_upload, 'product_price_errors')

		if not final.empty:
			home = expanduser("~")
			results_file = '{}/products.xlsx'.format(home)
			final.to_excel(results_file,sheet_name='produts', index=False)
			if country in ['mx','cl','co','ar']:
				text='''
				*Producto con precio muy bajo entre $0.1-$10 :flag-{country}:*
				Pais: {country}
				:alert:
				Los productos en adjunto recibieron pedidos con precio muy bajo *entre $0.1-$10*'''.format(country=country)
			else:
				text = '''
				*Producto con precio muy bajo <= $1 :flag-{country}:*
				Pais: {country}
				:alert:
				Los productos en adjunto recibieron pedidos con un valor muy bajo *<= $1*'''.format(country=country)
			print(text)
			slack.file_upload_channel(channel_alert, text, results_file, 'xlsx')

			if os.path.exists(results_file):
				os.remove(results_file)
		else:
			print("final null")
	else:
		print('df null')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
	print(country)
	try:
		df = data(country)
		rp = reported_products(country)
		offenders(df,rp)
	except Exception as e:
		print(e)