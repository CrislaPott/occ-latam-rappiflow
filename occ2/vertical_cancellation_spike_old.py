import os, sys, json
import numpy as np
import pandas as pd
from time import sleep
from datetime import datetime, timedelta
from dotenv import load_dotenv
from os.path import expanduser
import gspread
from df2gspread import df2gspread as d2g
from oauth2client.service_account import ServiceAccountCredentials
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from openpyxl import load_workbook

load_dotenv()


def cancels(country):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	--no_cache
	with base as
	(SELECT order_id
	FROM order_modifications om
	join (select max(created_at) max_c from order_modifications) om2 on om.created_at >= max_c - interval '20 minutes' 
	WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}' - interval '45 minutes')
	and ((params ->> 'cancelation_reason')::text not ilike '%LIMBO%OCC%' or (params ->> 'cancelation_reason') is null)
	AND (TYPE IN ('close_order','arrive','finish_order','close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') OR TYPE ILIKE '%cancel%')
	GROUP BY 1
	)
	select coalesce(o2.store_id,0) as store_id, coalesce(os.name,'whim') as store_name, case when city_address_id is null then 0 else city_address_id end as city_address_id, 
	case when init_store_type in ('whim') then 'whim' else os.type end as store_type, 
	count(distinct base.order_id) as orders, 
	count(distinct case when o.state ilike '%cancel%' then base.order_id else null end) as canceladas,
	string_agg(distinct case when o.state ilike '%cancel%' then base.order_id::text else null end,';') as order_ids
	from base
	left join calculated_information.orders o2 on o2.order_id=base.order_id
	left join order_stores os on os.order_id=o2.order_id and os.store_id=o2.store_id and os.contact_email not like '%@synth.rappi.com%'
	join orders o on o.id=base.order_id and o.state not in ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error','canceled_by_split_error','canceled_by_early_regret')
	where (os.type not in ('rappi_pay','queue_order_flow','soat_webview') and os.type not ilike '%rappi%pay%' or os.type is null)
	and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
	and o.payment_method not ilike '%synthe%'
	and os.name not ilike '%Dummy Store%'
	group by 1,2,3,4'''.format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(1904, query)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
	elif country == 'mx':
		metrics = redash.run_query(1371, query)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
	elif country == 'br':
		metrics = redash.run_query(1338, query)

	return metrics

def verticals(country):
	query ='''
	--no_cache
	select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	 when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	 when vertical_group = 'WHIM' then 'Antojos'
	 when vertical_group = 'RAPPICASH' then 'RappiCash'
	 when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
	 when store_type in ('turbo') then 'Turbo'
	 when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
	 when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
	 when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
	 when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
	 when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
	 when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2'''.format(country=country)

	df = snow.run_query(query)
	return df

def cities(country):
	query='''
	--no_cahce
	select ca.id, ca.city
	from city_addresses ca
	join countries c on c.id=ca.country_id and code_iso_2='{country}'
	'''.format(country=country.upper())

	if country == 'co':
		metrics = redash.run_query(6164, query)
	elif country == 'ar':
		metrics = redash.run_query(6092, query)
	elif country == 'cl':
		metrics = redash.run_query(6093, query)
	elif country == 'mx':
		metrics = redash.run_query(6152, query)
	elif country == 'uy':
		metrics = redash.run_query(6436, query)
	elif country == 'ec':
		metrics = redash.run_query(6066, query)
	elif country == 'cr':
		metrics = redash.run_query(6094, query)
	elif country == 'pe':
		metrics = redash.run_query(6103, query)
	elif country == 'br':
		metrics = redash.run_query(6151, query)

	return metrics

def store_infos(country):
	if country == 'br':
		tag_id = 318574
	elif country == 'mx':
		tag_id = 65702
	elif country == 'co':
		tag_id = 325885
	elif country == 'ar':
		tag_id = 325778
	elif country == 'cl':
		tag_id = 151
	elif country == 'pe':
		tag_id = 123
	elif country == 'uy':
		tag_id = 325759
	elif country == 'ec':
		tag_id = 121
	elif country == 'cr':
		tag_id = 121

	query_store_infos = """ 
	select a.store_id as id_store, 
	max(a.type) as storetype,
	max(c.id) as brand_id, 
	max(c.name) as brand_name, 
	max(case when t.tag_id is not null then 'ccp' else null end) as tag
    
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c on c.id=a.store_brand_id
    left join {country}_PGLR_MS_STORES_PUBLIC.store_tag t on t.store_id=a.store_id and t.tag_id = {tag_id}
    group by 1
	""".format(country=country,tag_id=tag_id)

	store_infos = snow.run_query(query_store_infos)

	return store_infos

def category(country):
	query = f'''SELECT '{country}' as country1, si.store_id::text as storeid, c.category
	      from ops_occ.store_infos_latam si
	      left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=si.store_id
	      left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=si.brand_group_id and s.city_address_id=c.city_address_id and c.COUNTRY='{country.upper()}'
	      and c.vertical=(case when si.vertical in ('Mercados') then 'Super/Hiper' else si.vertical end)
	      where si.country='{country}'
	      union all
	      (select lower(country) as country1, store_id::text as storeid,
      	  case when brand_segmentation in ('1. CORPORATE BRAND','2. FINE DINING','2. LOCAL CHAIN','3. LOCAL CHAIN','4. LOCAL HERO','6. BRAND DEVELOPMENT')
          then 'Top'
          else 'LongTail' end as category
		  from RESTAURANTS_GLOBAL_POSTSALES.A_SCORECARD_REST4 a
		  where lower(country)='{country}'
		  group by 1,2,3)
	      '''
	df = snow.run_query(query)
	return df

def final(country):
	channel_alert = timezones.slack_channels('partner_operation')

	a = cancels(country)
	b = verticals(country)
	c = cities(country)
	d = store_infos(country)
	e = category(country)
	# merging a with b in order to get the verticals
	
	df = pd.merge(a,b, how='left',left_on=a['store_type'],right_on=b['storetype'])
	df = df.drop(['key_0','storetype'], axis=1)
	grouped_df = df.groupby(["store_id","store_name","vertical","city_address_id","order_ids"])[["canceladas","orders"]].sum().reset_index()
	grouped_df['Percentage'] = round((grouped_df['canceladas'] / grouped_df['orders']) * 100,2)

	# merging grouped_df with c and d in order to get the store cities and other store informations

	final_sheets_df = pd.merge(grouped_df,c, how='left',left_on=grouped_df['city_address_id'],right_on=c['id'])
	final_sheets_df = final_sheets_df.drop(['key_0','id'], axis=1)

	final_sheets_df = pd.merge(final_sheets_df,d, how='left',left_on=final_sheets_df['store_id'],right_on=d['id_store'])
	final_sheets_df = final_sheets_df.drop(['key_0','id_store'], axis=1)
	final_sheets_df  = final_sheets_df[['store_id','store_name',"tag",'storetype','vertical','brand_id','brand_name','city','order_ids','canceladas','orders','Percentage']]

	final_sheets_df['country'] = country
	e = e.drop_duplicates(subset=['storeid'], keep='last')
	e['storeid'] = e['storeid'].astype(str)
	final_sheets_df['store_id'] = final_sheets_df['store_id'].astype(str)
	final_sheets_df = final_sheets_df.merge(e, how='left', left_on=['country', 'store_id'], right_on=['country1', 'storeid'])
	final_sheets_df = final_sheets_df.drop(['country1', 'storeid', 'country'], axis=1)
	final_sheets_df.category = final_sheets_df.category.fillna('D')

	# data by verticals

	vertical_df = df.groupby(["vertical"])[["canceladas","orders"]].sum().reset_index()
	vertical_df['Percentage'] = (vertical_df['canceladas'] / vertical_df['orders']) * 100

	print(vertical_df)

	# threshold by countries

	if country =='br' or country == 'co' or country == 'mx':
		restaurantes_cancelados = 30
		restaurantes_percentage = 10
		licores_cancelados = 15
		licores_percentage = 12
		farmacia_cancelados = 15
		farmacia_percentage = 8.5
		mercados_cancelados = 15
		mercados_percentage = 12
		antojos_cancelados = 10
		antojos_percentage = 17
		cash_cancelados = 10
		cash_percentage = 15
		favor_cancelados = 10
		favor_percentage = 25
		express_cancelados = 15
		express_percentage = 12
		ecommerce_cancelados = 10
		ecommerce_percentage = 15
		cpgs_cancelados = 10
		cpgs_percentage = 15
		turbo_cancelados = 10
		turbo_percentage = 10
		others_cancelados = 10
		others_percentage = 25
	else:
		restaurantes_cancelados = 10
		restaurantes_percentage = 10
		licores_cancelados = 10
		licores_percentage = 17
		farmacia_cancelados = 10
		farmacia_percentage = 13
		mercados_cancelados = 10
		mercados_percentage = 17
		antojos_cancelados = 10
		antojos_percentage = 17
		cash_cancelados = 5
		cash_percentage = 15
		favor_cancelados = 10
		favor_percentage = 20
		express_cancelados = 10
		express_percentage = 17
		ecommerce_cancelados = 10
		ecommerce_percentage = 17
		cpgs_cancelados = 10
		cpgs_percentage = 15
		turbo_cancelados = 10
		turbo_percentage = 15
		others_cancelados = 10
		others_percentage = 25

	#restaurantes = vertical_df[(vertical_df['vertical'] == 'Restaurantes') & (vertical_df['canceladas'] >= restaurantes_cancelados) & (vertical_df['Percentage'] >= restaurantes_percentage)]
	licores = vertical_df[(vertical_df['vertical'] == 'Licores') & (vertical_df['canceladas'] >= licores_cancelados) & (vertical_df['Percentage'] >= licores_percentage)]
	farmacia = vertical_df[(vertical_df['vertical'] == 'Farmacia') & (vertical_df['canceladas'] >= farmacia_cancelados) & (vertical_df['Percentage'] >= farmacia_percentage)]
	mercados = vertical_df[(vertical_df['vertical'] == 'Mercados') & (vertical_df['canceladas'] >= mercados_cancelados) & (vertical_df['Percentage'] >= mercados_percentage)]
	whim = vertical_df[(vertical_df['vertical'] == 'Antojos') & (vertical_df['canceladas'] >= antojos_cancelados) & (vertical_df['Percentage'] >= antojos_percentage)]
	cash = vertical_df[(vertical_df['vertical'] == 'RappiCash') & (vertical_df['canceladas'] >= cash_cancelados) & (vertical_df['Percentage'] >= cash_percentage)]
	express = vertical_df[(vertical_df['vertical'] == 'Express') & (vertical_df['canceladas'] >= express_cancelados) & (vertical_df['Percentage'] >= express_percentage)]
	ecommerce = vertical_df[(vertical_df['vertical'] == 'Ecommerce') & (vertical_df['canceladas'] >= ecommerce_cancelados) & (vertical_df['Percentage'] >= ecommerce_percentage)]
	others = vertical_df[(vertical_df['vertical'] == 'Others') & (vertical_df['canceladas'] >= others_cancelados) & (vertical_df['Percentage'] >= others_percentage)]
	cpgs = vertical_df[(vertical_df['vertical'] == 'CPGs') & (vertical_df['canceladas'] >= cpgs_cancelados) & (vertical_df['Percentage'] >= cpgs_percentage)]
	turbo = vertical_df[(vertical_df['vertical'] == 'Turbo') & (vertical_df['canceladas'] >= turbo_cancelados) & (vertical_df['Percentage'] >= turbo_percentage)]

	df_verticals_list = [licores,farmacia,mercados,whim,cash,express,ecommerce,others,cpgs]

	for df in df_verticals_list:
		for index, row in df.iterrows():
			rows = dict(row)
			vertical = df['vertical'].to_list()
			vertical = ''.join(vertical)
			sheets = final_sheets_df[(final_sheets_df['vertical']== vertical)].sort_values(by='canceladas', ascending=False)
			home = expanduser("~")
			results_file = '{}/{}_details.xlsx'.format(home, vertical)
			sheets.to_excel(results_file,sheet_name='Stores', index=False)
			book = load_workbook(results_file)
			writer = pd.ExcelWriter(results_file, engine='openpyxl')
			writer.book = book
			writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

			sheets_brands = sheets.groupby(["brand_id","brand_name","vertical"])[["canceladas","orders"]].sum().reset_index()
			sheets_brands['Percentage'] = round((sheets_brands['canceladas'] / sheets_brands['orders']) * 100,2)
			sheets_brands = sheets_brands.sort_values(by='canceladas', ascending=False)
			
			sheets_cities = sheets.groupby(["city"])[["canceladas","orders"]].sum().reset_index()
			sheets_cities['Percentage'] = round((sheets_cities['canceladas'] / sheets_cities['orders']) * 100,2)
			sheets_cities = sheets_cities.sort_values(by='canceladas', ascending=False)

			sheets_brands.to_excel(writer, "Brands", index=False)
			sheets_cities.to_excel(writer, "Cities", index=False)

			writer.save()

			text = '''
			:warning: *Pico de Cancelación en Vertical* :warning: 
			:flag-{country}:
			Pais: {country}
			Vertical *{vertical}* alcanzó el *{Percentage}% de cancelación* en los últimos 20 minutos :chart_with_upwards_trend:
			*Cancelados:* {canceladas}
			*Ordenes:* {orders}
			Lista de tiendas infractoras  :arrow_down:
			'''.format(country=country,
				vertical = row['vertical'],
				Percentage = round(row['Percentage'],2),
				canceladas = row['canceladas'],
				orders = row['orders']
				)

			print("affected verticals")
			print(df)

			slack.file_upload_channel(channel_alert, text, results_file, 'xlsx')
			current_time = timezones.country_current_time(country)
			to_upload = df
			to_upload['alarm_at'] = current_time
			to_upload['country'] = country
			snow.upload_df_occ(to_upload, 'vertical_cancellation_spike')
			to_upload2 = sheets
			to_upload2['alarm_at'] = current_time
			to_upload2['country'] = country
			snow.upload_df_occ(to_upload2, 'vertical_cancellation_spike_details')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
	try:
		final(country)
	except Exception as e:
		print(e)