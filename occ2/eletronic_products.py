import pandas as pd
from lib import snowflake, slack, redash
from functions import timezones
from os.path import expanduser


def stockout_store_br(country):
	timezone, interval = timezones.country_timezones(country)

	query_br = f'''
with reported as (
    select distinct product_id::text as product_id, store_id, sum(orders) as orders_
	from ops_occ.ecommerce_low_prices
	where alarm_at::date = convert_timezone('America/Buenos_Aires',current_timestamp())::date
	and country = 'br'
	group by 1,2
    )

    ,
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id, s.id as cp_store_id
        from br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )

,products as (
select p.name as product_name,
	   p.id::text as product_id,
       ps.store_id,
	   ps.price,
       ws.product_name as product_name_externo,
       p.ean,
       ((ps.price-avg_amazon_price)/nullif(avg_amazon_price,0)) as variation,
       e.price as elastic_price
from br_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
join br_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id=p.id
left join (select distinct product_id, store_id, price from cpgs_datascience.cpgs_elastic_content where country_code = 'BR') e on e.product_id = p.id::text and e.store_id = ps.store_id  
left join (select product_name,
      w.ean,
      w.country,
      avg(product_price) as avg_amazon_price
      from ops_occ.web_scraping w
      where time::date < '2022-01-04'
      group by 1,2,3
     ) ws on ws.ean = p.ean
where (ws.ean not in ('0','7896523207704','7892840249847','7896051130291','7896023081538','7898476327102','7898506454747','7898506474738','7899718700516')
and variation <= -0.70 and (product_name_externo not ilike '%Dvd%' and product_name_externo not ilike '%Cabo%' and product_name_externo not ilike '%Fogo%'))
or
(variation is null and ((name ilike '%Galaxy S%'
or name ilike '%Iphone%'
or name ilike '%Ipad %'
or name ilike '%Earpods%'
or name ilike '%Motorola%'
or name ilike '%Samsung%'
or name ilike '%Xiaomi smart%'
or name ilike '%Xiaomi tele%'
or name ilike '%ASUS %'
or name ilike '%Alcatel%'
or name ilike '%Huawei%'
or name ilike '%Lenovo%'
or name ilike '%VIVO%'
or name ilike '%ZTE%'
or name ilike '%NOKIA%'
or name ilike '%SONY XPERIA%'
or name ilike '%notebook%'
or name like 'Tablet %')

and trademark not in ('Sony Music','Bacardi','Som Livre','Troppo','Toppo','Appleton','Pineapple','R & S','Jack Daniels','Solo Vivo','Fila','BISCOITO FINO','Payot','CATMAKE')

and name not ilike '%tablet in %'
and name not ilike '%Tablet Chocolate%'
and name not ilike '%Tablet Gold%'
and name not ilike '%Tablet Manteiga%'
and name not ilike '%Tablet de %'
and name not ilike '%0g%'
and name not ilike '%Caixa%de%Som%'
and name not ilike '%de notebook%'
and name not ilike '%para notebook%'
and name not ilike '%pen%drive%'
and name not ilike '%Capa%'
and name not ilike '%Tela%'
and name not ilike '%Cabo%'
and name not ilike '%Impressora%'
and name not ilike '%Barbeador%'
and name not ilike '%Cilindro%'
and name not ilike '%Balança%'
and name not ilike '%placa%'
and name not ilike '%Alcatel%'
and name not ilike '%cartucho%'
and name not ilike '%case%'
and name not ilike '%silicone%'
and name not ilike '%joystick%'
and name not ilike '%radio%'
and name not ilike '%espatula%'
and name not ilike '%repartidor%'
and name not ilike '%Bateria%'
and name not ilike '%oculos%'
and name not ilike '%microfone%'
and name not ilike '%pusleira%'
and name not ilike '%chopeira%'
and name not ilike '%cartão%memoria%'
and name not ilike '%Vivo%'
and name not ilike '%controle%'
and name not ilike '%teclado%'
and name not ilike '%l%mpada%'
and name not ilike '%capinha%'
and name not ilike '%Mouse%'
and name not ilike '%Copo Plstico%'
and name not ilike '%Copo Plstico%'
and name not ilike '%PEL%CULA%'
and name not ilike '%sidra%'
and name not ilike '%pilha%'
and name not ilike '%curso%'
and name not ilike '%pulseira%'
and name not ilike '%Framboesa%'
and name not ilike '%presente%'
and name not ilike '%adesivo%'
and name not ilike '%brincadeira%'
and name not ilike '%latika%'
and name not ilike '%led%'
and name not ilike '%Injetavel%'
and name not ilike '%Travesseiro%'
and name not ilike '%Óculos%'
and name not ilike '%Toalha%'
and name not ilike '%Roteador%'
and name not ilike '%Refil%'
and name not ilike '%Mochila%'
and name not ilike '%Adaptador%'
and name not ilike '%Carregador%'
and name not ilike '%Caneta%'
and name not ilike '%caneta%'
and name not ilike '%carregador%'
and name not ilike '%pelucia%'
and name not ilike '%bone%'
and name not ilike '%boné%'
and name not ilike '%luminaria%'
and name not ilike '%luminária%'
and name not ilike '%tequila%'
and name not ilike '%guardians%'
and name not ilike '%bracadeira%'
and name not ilike '%braçadeira%'
and name not ilike '%bumper%'
and name not ilike '%base%'
and name not ilike '%bolsa%'
and name not ilike '%carteira%'
and name not ilike '%camiseta%'
and name not ilike '%drive%'
and name not ilike '%pegasus%'
and name not ilike '%fonte%'
and name not ilike '%fume%'
and name not ilike '%acessories%'
and name not ilike '%acessorios%'
and name not ilike '%mesa%'
and name not ilike '%porta%'
and name not ilike '%pochete%'
and name not ilike '%micro%'
and name not ilike '%SD%'
and name not ilike '%cover%'
and name not ilike '%keyboard%'
and name not ilike '%suporte%'
and name not ilike '%toner%'
and name not ilike '%leigos%'
and name not ilike '%desconto%'
and name not ilike '%tofu%'
and name not ilike '%mesa%'
and name not ilike '%zTe%'
and name not ilike '%Lightning%'
))

and (ps.price <= 500 or e.price <= 500)
group by 1,2,3,4,5,6,7,8)

, final as  (
select op.product_id,
       product_name,
       case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else op.product_id end)::int as store_product_id,
       p.price,
       os.store_id, os.name as store_name,
       orders_,
       count(distinct o.id) as orders,
       listagg(distinct o.id::text,',') as order_ids,
       p.variation,
       p.elastic_price,
       pscp.cp_store_id

from BR_CORE_ORDERS_PUBLIC.orders o
left join BR_CORE_ORDERS_PUBLIC.order_stores os on os.order_id=o.id
left join BR_CORE_ORDERS_PUBLIC.order_product op on op.order_id=o.id
join products p on p.product_id=op.product_id and p.store_id=os.store_id
left join reported rp on rp.product_id=op.product_id and rp.store_id=os.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=os.store_id and lower(b.COUNTRY_CODE)='br'
left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = op.product_id::int
left join BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = OS.STORE_ID
where o.created_at::date >= convert_timezone('America/Buenos_Aires',current_timestamp())::date::date
and  os.type not in ('restaurant')

group by 1,2,3,4,5,6,7,8,12, p.variation, cp_store_id)

select * from final where orders > coalesce(orders_,0)
	'''

	query_co = f'''with reported as (
    select distinct product_id::text as product_id, store_id, sum(orders) as orders_
	from ops_occ.ecommerce_low_prices
	where alarm_at::date = convert_timezone('America/Bogota',current_timestamp())::date
	and country = 'co'
	group by 1,2
    )

    ,
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id, s.id as cp_store_id
        from co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )

,products as (
select p.name as product_name,
	   p.id::text as product_id,
       ps.store_id,
	   ps.price,
       e.price as elastic_price
from co_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
join co_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id=p.id
left join (select distinct product_id, store_id, price from cpgs_datascience.cpgs_elastic_content where country_code = 'CO') e on e.product_id = p.id::text and e.store_id = ps.store_id
where
(name ilike '%Galaxy S%'
or name ilike '%Iphone%'
or name ilike '%Ipad %'
or name ilike '%Earpods%'
or name ilike '%audifono ear%'
or name ilike '%Motorola%'
or name ilike '%Samsung%'
or name ilike '%Xiaomi smart%'
or name ilike '%Xiaomi tele%'
or name ilike '%ASUS %'
or name ilike '%Alcatel%'
or name ilike '%Huawei%'
or name ilike '%Lenovo%'
or name ilike '%VIVO%'
or name ilike '%ZTE%'
or name ilike '%NOKIA%'
or name ilike '%SONY XPERIA%'
or name ilike '%notebook%'
or name like 'Tablet %')


and trademark not in ('Sony Music','Bacardi','Som Livre','Troppo','Toppo','Appleton','Pineapple','R & S','Jack Daniels','Solo Vivo','Fila','BISCOITO FINO','Payot','CATMAKE')

and name not ilike '%tablet in %' 
and name not ilike '%Tablet Chocolate%'
and name not ilike '%Tablet Gold%'
and name not ilike '%Tablet Manteiga%'
and name not ilike '%Tablet de %'
and name not ilike '%0g%'
and name not ilike '%Caixa%de%Som%'
and name not ilike '%de notebook%'
and name not ilike '%para notebook%'
and name not ilike '%Equipo%de%Sonido%'
and name not ilike '%usb%'
and name not ilike '%Pantalla%'
and name not ilike '%Cable%'
and name not ilike '%Impresora%'
and name not ilike '%Afeitadora%'
and name not ilike '%Maquina%'
and name not ilike '%pesa%'
and name not ilike '%placa%'
and name not ilike '%Alcatel%'
and name not ilike '%cartucho%'
and name not ilike '%case%'
and name not ilike '%silicone%'
and name not ilike '%joystick%'
and name not ilike '%radio%'
and name not ilike '%espatula%'
and name not ilike '%repartidor%'
and name not ilike '%Bateria%'
and name not ilike '%lentes%'
and name not ilike '%microfono%'
and name not ilike '%pulso%'
and name not ilike '%memoria%'
and name not ilike '%Vivo%'
and name not ilike '%control%'
and name not ilike '%teclado%'
and name not ilike '%lampara%'
and name not ilike '%forro%'
and name not ilike '%Mouse%'
and name not ilike '%Vaso plastico%'
and name not ilike '%sidra%'
and name not ilike '%pila%'
and name not ilike '%curso%'
and name not ilike '%reloj%'
and name not ilike '%Frambuesa%'
and name not ilike '%regalo%'
and name not ilike '%adhesivo%'
and name not ilike '%chocolate%'
and name not ilike '%led%'
and name not ilike '%inyeccion%'
and name not ilike '%Cabecera%'
and name not ilike '%Toalla%'
and name not ilike '%repetidor%'
and name not ilike '%Refil%'
and name not ilike '%Mochila%'
and name not ilike '%Adaptador%'
and name not ilike '%Cargador%'
and name not ilike '%esfero%'
and name not ilike '%boligrafo%'
and name not ilike '%cargador%'
and name not ilike '%peluche%'
and name not ilike '%gorra%'
and name not ilike '%tequila%'
and name not ilike '%asador%'
and name not ilike '%base%'
and name not ilike '%bolsa%'
and name not ilike '%bolso%'
and name not ilike '%camiseta%'
and name not ilike '%fuente%'
and name not ilike '%accesorios%'
and name not ilike '%mesa%'
and name not ilike '%puerta%'
and name not ilike '%cinturon%'
and name not ilike '%micro%'
and name not ilike '%SD%'
and name not ilike '%cover%'
and name not ilike '%keyboard%'
and name not ilike '%soporte%'
and name not ilike '%toner%'
and name not ilike '%descuento%'
and name not ilike '%canguro%'
and name not ilike '%mesa%'
and name not ilike '%zTe%'
and name not ilike '%Lightning%'
and name not ilike '%vidrio%'
and name not ilike '%Auricular%'
and name not ilike '%Protector%'
and name not ilike '%Estuche%'
and name not ilike '%proteccion%'
and name not ilike '%funda%'
and name not ilike '%manos%'
and name not ilike '%Freebuds%'
and name not ilike '%carcasa%'
and name not ilike '%igthning%'
and name not ilike '%Inal%mbrico%'

and (ps.price <= 500000 or e.price <= 500000)
group by 1,2,3,4,5)

, final as  (
select op.product_id,
       product_name,
       case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else op.product_id end)::int as store_product_id,
       p.price,
       os.store_id, os.name as store_name,
       orders_,
       count(distinct o.id) as orders,
       listagg(distinct o.id::text,',') as order_ids,
       p.elastic_price,
       pscp.cp_store_id

from co_CORE_ORDERS_PUBLIC.orders_vw o
left join co_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
left join co_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=o.id
left join co_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = OS.STORE_ID
join products p on p.product_id=op.product_id and p.store_id=os.store_id
left join reported rp on rp.product_id=op.product_id and rp.store_id=os.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=os.store_id and lower(b.COUNTRY_CODE)='co'
left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = op.product_id::int
where o.created_at::date >= convert_timezone('America/Bogota',current_timestamp())::date::date
and os.type not in ('restaurant')

group by 1,2,3,4,5,6,7,8,11, cp_store_id)

select * from final where orders > coalesce(orders_,0)
	'''

	query_mx = f'''
	with reported as (
    select distinct product_id::text as product_id, store_id, sum(orders) as orders_
	from ops_occ.ecommerce_low_prices
	where alarm_at::date = convert_timezone('America/Mexico_City',current_timestamp())::date
	and country = 'mx'
	group by 1,2
    )

    ,
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id, s.id as cp_store_id
        from mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join mx_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )

,products as (
select p.name as product_name,
	   p.id::text as product_id,
       ps.store_id,
	   ps.price,
       e.price as elastic_price
from mx_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
join mx_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id=p.id
left join (select distinct product_id, store_id, price from cpgs_datascience.cpgs_elastic_content where country_code = 'MX') e on e.product_id = p.id::text and e.store_id = ps.store_id
where
(name ilike '%Galaxy S%'
or name ilike '%Iphone%'
or name ilike '%Ipad %'
or name ilike '%Earpods%'
or name ilike '%audifono ear%'
or name ilike '%Motorola%'
or name ilike '%Samsung%'
or name ilike '%Xiaomi smart%'
or name ilike '%Xiaomi tele%'
or name ilike '%ASUS %'
or name ilike '%Alcatel%'
or name ilike '%Huawei%'
or name ilike '%Lenovo%'
or name ilike '%VIVO%'
or name ilike '%ZTE%'
or name ilike '%NOKIA%'
or name ilike '%SONY XPERIA%'
or name ilike '%notebook%'
or name like 'Tablet %')


and trademark not in ('Sony Music','Bacardi','Som Livre','Troppo','Toppo','Appleton','Pineapple','R & S','Jack Daniels','Solo Vivo','Fila','BISCOITO FINO','Payot','CATMAKE')

and name not ilike '%tablet in %' 
and name not ilike '%Tablet Chocolate%'
and name not ilike '%Tablet Gold%'
and name not ilike '%Tablet Manteiga%'
and name not ilike '%Tablet de %'
and name not ilike '%0g%'
and name not ilike '%Caixa%de%Som%'
and name not ilike '%de notebook%'
and name not ilike '%para notebook%'
and name not ilike '%Equipo%de%Sonido%'
and name not ilike '%usb%'
and name not ilike '%Pantalla%'
and name not ilike '%Cable%'
and name not ilike '%Impresora%'
and name not ilike '%Afeitadora%'
and name not ilike '%Maquina%'
and name not ilike '%pesa%'
and name not ilike '%placa%'
and name not ilike '%Alcatel%'
and name not ilike '%cartucho%'
and name not ilike '%case%'
and name not ilike '%silicone%'
and name not ilike '%joystick%'
and name not ilike '%radio%'
and name not ilike '%espatula%'
and name not ilike '%repartidor%'
and name not ilike '%Bateria%'
and name not ilike '%lentes%'
and name not ilike '%microfono%'
and name not ilike '%pulso%'
and name not ilike '%memoria%'
and name not ilike '%Vivo%'
and name not ilike '%control%'
and name not ilike '%teclado%'
and name not ilike '%lampara%'
and name not ilike '%forro%'
and name not ilike '%Mouse%'
and name not ilike '%Vaso plastico%'
and name not ilike '%sidra%'
and name not ilike '%pila%'
and name not ilike '%curso%'
and name not ilike '%reloj%'
and name not ilike '%Frambuesa%'
and name not ilike '%regalo%'
and name not ilike '%adhesivo%'
and name not ilike '%chocolate%'
and name not ilike '%led%'
and name not ilike '%inyeccion%'
and name not ilike '%Cabecera%'
and name not ilike '%Toalla%'
and name not ilike '%repetidor%'
and name not ilike '%Refil%'
and name not ilike '%Mochila%'
and name not ilike '%Adaptador%'
and name not ilike '%Cargador%'
and name not ilike '%esfero%'
and name not ilike '%boligrafo%'
and name not ilike '%cargador%'
and name not ilike '%peluche%'
and name not ilike '%gorra%'
and name not ilike '%tequila%'
and name not ilike '%asador%'
and name not ilike '%base%'
and name not ilike '%bolsa%'
and name not ilike '%bolso%'
and name not ilike '%camiseta%'
and name not ilike '%fuente%'
and name not ilike '%accesorios%'
and name not ilike '%mesa%'
and name not ilike '%puerta%'
and name not ilike '%cinturon%'
and name not ilike '%micro%'
and name not ilike '%SD%'
and name not ilike '%cover%'
and name not ilike '%keyboard%'
and name not ilike '%soporte%'
and name not ilike '%toner%'
and name not ilike '%descuento%'
and name not ilike '%canguro%'
and name not ilike '%mesa%'
and name not ilike '%zTe%'
and name not ilike '%Lightning%'
and name not ilike '%vidrio%'
and name not ilike '%Auricular%'
and name not ilike '%Protector%'
and name not ilike '%Estuche%'
and name not ilike '%proteccion%'
and name not ilike '%funda%'
and name not ilike '%manos%'
and name not ilike '%Freebuds%'
and name not ilike '%carcasa%'
and name not ilike '%igthning%'
and name not ilike '%Inal%mbrico%'
and name not ilike '%miniso%'


and (ps.price <= 2000 or e.price <= 2000)
group by 1,2,3,4,5)

, final as  (
select op.product_id,
       product_name,
       case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else op.product_id end)::int as store_product_id,
       p.price,
       os.store_id, os.name as store_name,
       orders_,
       count(distinct o.id) as orders,
       listagg(distinct o.id::text,',') as order_ids,
       p.elastic_price,
       cp_store_id

from mx_CORE_ORDERS_PUBLIC.orders_vw o
left join mx_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
left join mx_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=o.id
left join mx_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = OS.STORE_ID
join products p on p.product_id=op.product_id and p.store_id=os.store_id
left join reported rp on rp.product_id=op.product_id and rp.store_id=os.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=os.store_id and lower(b.COUNTRY_CODE)='mx'
left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = op.product_id::int
where o.created_at::date >= convert_timezone('America/Mexico_City',current_timestamp())::date::date
and os.type not in ('restaurant')

group by 1,2,3,4,5,6,7,8,11, cp_store_id)

select * from final where orders > coalesce(orders_,0)
	'''

	query_cl = f'''with reported as (
    select distinct product_id::text as product_id, store_id, sum(orders) as orders_
	from ops_occ.ecommerce_low_prices
	where alarm_at::date = convert_timezone('America/Santiago',current_timestamp())::date
	and country = 'cl'
	group by 1,2
    )

    ,
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id, s.id as cp_store_id
        from cl_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join cl_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join cl_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id
        )

,products as (
select p.name as product_name,
	   p.id::text as product_id,
       ps.store_id,
	   ps.price,
       e.price as elastic_price
from cl_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
join cl_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id=p.id
left join (select distinct product_id, store_id, price from cpgs_datascience.cpgs_elastic_content where country_code = 'CL') e on e.product_id = p.id::text and e.store_id = ps.store_id
where (name ilike '%Galaxy S%'
or name ilike '%Iphone%'
or name ilike '%Ipad %'
or name ilike '%Earpods%'
or name ilike '%audifono ear%'
or name ilike '%Motorola%'
or name ilike '%Samsung%'
or name ilike '%Xiaomi smart%'
or name ilike '%Xiaomi tele%'
or name ilike '%ASUS %'
or name ilike '%Alcatel%'
or name ilike '%Huawei%'
or name ilike '%Lenovo%'
or name ilike '%VIVO%'
or name ilike '%ZTE%'
or name ilike '%NOKIA%'
or name ilike '%SONY XPERIA%'
or name ilike '%notebook%'
or name like 'Tablet %')


and trademark not in ('Sony Music','Bacardi','Som Livre','Troppo','Toppo','Appleton','Pineapple','R & S','Jack Daniels','Solo Vivo','Fila','BISCOITO FINO','Payot','CATMAKE')

and name not ilike '%tablet in %' 
and name not ilike '%Tablet Chocolate%'
and name not ilike '%Tablet Gold%'
and name not ilike '%Tablet Manteiga%'
and name not ilike '%Tablet de %'
and name not ilike '%0g%'
and name not ilike '%Caixa%de%Som%'
and name not ilike '%de notebook%'
and name not ilike '%para notebook%'
and name not ilike '%Equipo%de%Sonido%'
and name not ilike '%usb%'
and name not ilike '%Pantalla%'
and name not ilike '%Cable%'
and name not ilike '%Impresora%'
and name not ilike '%Afeitadora%'
and name not ilike '%Maquina%'
and name not ilike '%pesa%'
and name not ilike '%placa%'
and name not ilike '%Alcatel%'
and name not ilike '%cartucho%'
and name not ilike '%case%'
and name not ilike '%silicone%'
and name not ilike '%joystick%'
and name not ilike '%radio%'
and name not ilike '%espatula%'
and name not ilike '%repartidor%'
and name not ilike '%Bateria%'
and name not ilike '%lentes%'
and name not ilike '%microfono%'
and name not ilike '%pulso%'
and name not ilike '%memoria%'
and name not ilike '%Vivo%'
and name not ilike '%control%'
and name not ilike '%teclado%'
and name not ilike '%lampara%'
and name not ilike '%forro%'
and name not ilike '%Mouse%'
and name not ilike '%Vaso plastico%'
and name not ilike '%sidra%'
and name not ilike '%pila%'
and name not ilike '%curso%'
and name not ilike '%reloj%'
and name not ilike '%Frambuesa%'
and name not ilike '%regalo%'
and name not ilike '%adhesivo%'
and name not ilike '%chocolate%'
and name not ilike '%led%'
and name not ilike '%inyeccion%'
and name not ilike '%Cabecera%'
and name not ilike '%Toalla%'
and name not ilike '%repetidor%'
and name not ilike '%Refil%'
and name not ilike '%Mochila%'
and name not ilike '%Adaptador%'
and name not ilike '%Cargador%'
and name not ilike '%esfero%'
and name not ilike '%boligrafo%'
and name not ilike '%cargador%'
and name not ilike '%peluche%'
and name not ilike '%gorra%'
and name not ilike '%tequila%'
and name not ilike '%asador%'
and name not ilike '%base%'
and name not ilike '%bolsa%'
and name not ilike '%bolso%'
and name not ilike '%camiseta%'
and name not ilike '%fuente%'
and name not ilike '%accesorios%'
and name not ilike '%mesa%'
and name not ilike '%puerta%'
and name not ilike '%cinturon%'
and name not ilike '%micro%'
and name not ilike '%SD%'
and name not ilike '%cover%'
and name not ilike '%keyboard%'
and name not ilike '%soporte%'
and name not ilike '%toner%'
and name not ilike '%descuento%'
and name not ilike '%canguro%'
and name not ilike '%mesa%'
and name not ilike '%zTe%'
and name not ilike '%Lightnin%'
and name not ilike '%vidrio%'
and name not ilike '%Auricular%'
and name not ilike '%Protector%'
and name not ilike '%Estuche%'
and name not ilike '%proteccion%'
and name not ilike '%funda%'
and name not ilike '%manos%'
and name not ilike '%Freebuds%'
and name not ilike '%carcasa%'
and name not ilike '%igthning%'
and name not ilike '%Inal%mbrico%'
and name not ilike '%miniso%'
and name not ilike '%l%mina%'
and name not ilike '%mica%'
and name not ilike '%cerveza%'
and name not ilike '%micr%fono%'

and (ps.price <= 72694 or e.price <= 72694)
group by 1,2,3,4,5)

, final as  (
select op.product_id,
       product_name,
       case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else op.product_id end)::int as store_product_id,
       p.price,
       os.store_id, 
       os.name as store_name,
       orders_,
       count(distinct o.id) as orders,
       listagg(distinct o.id::text,',') as order_ids,
       p.elastic_price,
       cp_store_id

from cl_CORE_ORDERS_PUBLIC.orders_vw o
left join cl_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
left join cl_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=o.id
left join cl_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = OS.STORE_ID
join products p on p.product_id=op.product_id and p.store_id=os.store_id
left join reported rp on rp.product_id=op.product_id and rp.store_id=os.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=os.store_id and lower(b.COUNTRY_CODE)='cl'
left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = op.product_id::int
where o.created_at::date >= convert_timezone('America/Santiago',current_timestamp())::date::date
and os.type not in ('restaurant')

group by 1,2,3,4,5,6,7,8,11, cp_store_id)

select * from final where orders > coalesce(orders_,0)
	'''
	query_ar = f'''with reported as (
    select distinct product_id::text as product_id, store_id, sum(orders) as orders_
	from ops_occ.ecommerce_low_prices
	where alarm_at::date = convert_timezone('America/Buenos_Aires',current_timestamp())::date
	and country = 'ar'
	group by 1,2
    )

    ,
        pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id, s.id as cp_store_id
        from ar_amysql_cpgs_clg_im_cpgs_clg_inventory.product p
        join ar_amysql_cpgs_clg_im_cpgs_clg_inventory.store s on s.id = p.store_id
        join ar_amysql_cpgs_clg_im_cpgs_clg_inventory.virtual_store vs ON vs.store_id = s.id
        )

,products as (
select p.name as product_name,
	   p.id::text as product_id,
       ps.store_id,
	   ps.price,
       e.price as elastic_price
from ar_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
join ar_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id=p.id
left join (select distinct product_id, store_id, price from cpgs_datascience.cpgs_elastic_content where country_code = 'AR') e on e.product_id = p.id::text and e.store_id = ps.store_id
where (name ilike '%Galaxy S%'
or name ilike '%Iphone%'
or name ilike '%Ipad %'
or name ilike '%Earpods%'
or name ilike '%audifono ear%'
or name ilike '%Motorola%'
or name ilike '%Samsung%'
or name ilike '%Xiaomi smart%'
or name ilike '%Xiaomi tele%'
or name ilike '%ASUS %'
or name ilike '%Alcatel%'
or name ilike '%Huawei%'
or name ilike '%Lenovo%'
or name ilike '%VIVO%'
or name ilike '%ZTE%'
or name ilike '%NOKIA%'
or name ilike '%SONY XPERIA%'
or name ilike '%notebook%'
or name like 'Tablet %')


and trademark not in ('Sony Music','Bacardi','Som Livre','Troppo','Toppo','Appleton','Pineapple','R & S','Jack Daniels','Solo Vivo','Fila','BISCOITO FINO','Payot','CATMAKE')

and name not ilike '%tablet in %' 
and name not ilike '%Tablet Chocolate%'
and name not ilike '%Tablet Gold%'
and name not ilike '%Tablet Manteiga%'
and name not ilike '%Tablet de %'
and name not ilike '%0g%'
and name not ilike '%Caixa%de%Som%'
and name not ilike '%de notebook%'
and name not ilike '%para notebook%'
and name not ilike '%Equipo%de%Sonido%'
and name not ilike '%usb%'
and name not ilike '%Pantalla%'
and name not ilike '%Cable%'
and name not ilike '%Impresora%'
and name not ilike '%Afeitadora%'
and name not ilike '%Maquina%'
and name not ilike '%pesa%'
and name not ilike '%placa%'
and name not ilike '%Alcatel%'
and name not ilike '%cartucho%'
and name not ilike '%case%'
and name not ilike '%silicone%'
and name not ilike '%joystick%'
and name not ilike '%radio%'
and name not ilike '%espatula%'
and name not ilike '%repartidor%'
and name not ilike '%Bateria%'
and name not ilike '%lentes%'
and name not ilike '%microfono%'
and name not ilike '%pulso%'
and name not ilike '%memoria%'
and name not ilike '%Vivo%'
and name not ilike '%control%'
and name not ilike '%teclado%'
and name not ilike '%lampara%'
and name not ilike '%forro%'
and name not ilike '%Mouse%'
and name not ilike '%Vaso plastico%'
and name not ilike '%sidra%'
and name not ilike '%pila%'
and name not ilike '%curso%'
and name not ilike '%reloj%'
and name not ilike '%Frambuesa%'
and name not ilike '%regalo%'
and name not ilike '%adhesivo%'
and name not ilike '%chocolate%'
and name not ilike '%led%'
and name not ilike '%inyeccion%'
and name not ilike '%Cabecera%'
and name not ilike '%Toalla%'
and name not ilike '%repetidor%'
and name not ilike '%Refil%'
and name not ilike '%Mochila%'
and name not ilike '%Adaptador%'
and name not ilike '%Cargador%'
and name not ilike '%esfero%'
and name not ilike '%boligrafo%'
and name not ilike '%cargador%'
and name not ilike '%peluche%'
and name not ilike '%gorra%'
and name not ilike '%tequila%'
and name not ilike '%asador%'
and name not ilike '%base%'
and name not ilike '%bolsa%'
and name not ilike '%bolso%'
and name not ilike '%camiseta%'
and name not ilike '%fuente%'
and name not ilike '%accesorios%'
and name not ilike '%mesa%'
and name not ilike '%puerta%'
and name not ilike '%cinturon%'
and name not ilike '%micro%'
and name not ilike '%SD%'
and name not ilike '%cover%'
and name not ilike '%keyboard%'
and name not ilike '%soporte%'
and name not ilike '%toner%'
and name not ilike '%descuento%'
and name not ilike '%canguro%'
and name not ilike '%mesa%'
and name not ilike '%zTe%'
and name not ilike '%Lightnin%'
and name not ilike '%vidrio%'
and name not ilike '%Auricular%'
and name not ilike '%Protector%'
and name not ilike '%Estuche%'
and name not ilike '%proteccion%'
and name not ilike '%funda%'
and name not ilike '%manos%'
and name not ilike '%Freebuds%'
and name not ilike '%carcasa%'
and name not ilike '%igthning%'
and name not ilike '%Inal%mbrico%'
and name not ilike '%miniso%'
and name not ilike '%l%mina%'
and name not ilike '%mica%'
and name not ilike '%cerveza%'
and name not ilike '%micr%fono%'

and (ps.price <= 9000 or e.price <= 9000)
group by 1,2,3,4,5)

, final as  (
select op.product_id,
       product_name,
       case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else op.product_id end)::int as store_product_id,
       p.price,
       os.store_id, os.name as store_name,
       orders_,
       count(distinct o.id) as orders,
       listagg(distinct o.id::text,',') as order_ids,
       p.elastic_price,
       cp_store_id

from ar_CORE_ORDERS_PUBLIC.orders_vw o
left join ar_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
left join ar_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=o.id
left join ar_PG_MS_CPGOPS_STORES_MS_PUBLIC.STORES PH ON PH.STORE_ID = OS.STORE_ID
join products p on p.product_id=op.product_id and p.store_id=os.store_id
left join reported rp on rp.product_id=op.product_id and rp.store_id=os.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=os.store_id and lower(b.COUNTRY_CODE)='ar'
left join pscp on pscp.store_id::int = os.store_id::int and pscp.product_id::int = op.product_id::int
where o.created_at::date >= convert_timezone('America/Buenos_Aires',current_timestamp())::date::date
and os.type not in ('restaurant')

group by 1,2,3,4,5,6,7,8,11, cp_store_id)

select * from final where orders > coalesce(orders_,0)
	'''

	query = '''select store_id::int as storeid, product_id::int as productid from override_availability
		    where ends_at >= now() at time zone 'America/{timezone}'
		    and (in_stock = False or status = 'unpublished')
		    order by ends_at desc
		    '''.format(timezone=timezone)

	if country in ['br']:
		df = snowflake.run_query(query_br)
		df_oa = redash.run_query(6520, query)
	elif country in ['mx']:
		df = snowflake.run_query(query_mx)
		df_oa = redash.run_query(6526, query)
	elif country in ['co']:
		df = snowflake.run_query(query_co)
		df_oa = redash.run_query(6522, query)
	elif country in ['cl']:
		df = snowflake.run_query(query_cl)
		df_oa = redash.run_query(6521, query)
	elif country in ['ar']:
		df = snowflake.run_query(query_ar)
		df_oa = redash.run_query(6519, query)
	else:
		print("nothing")

	return df, df_oa

def run_alarm(df,df_oa):
	channel_alert = timezones.slack_channels('trend_alarms')
	current_time = timezones.country_current_time(country)
	if not df_oa.empty:
		df_oa['productid'] = df_oa['productid'].astype(int).astype(str)
		df_oa['storeid'] = df_oa['storeid'].astype(int).astype(str)
		df['product_id'] = df['product_id'].astype(str)
		df['cp_store_id'] = df['cp_store_id'].astype(str)

		df = pd.merge(df, df_oa, how="left", left_on=['product_id', 'cp_store_id'], right_on=['productid', 'storeid'])
		df = df[(df['productid'].isnull()) & (df['storeid'].isnull())]
		df = df.drop(['productid', 'storeid'], axis=1)

	df['country'] = country
	df['alarm_at'] = current_time
	to_upload = df[['product_id', 'product_name', 'price', 'store_id', 'store_name', 'orders', 'order_ids','alarm_at','country','elastic_price']]
	snowflake.upload_df_occ(to_upload,'ecommerce_low_prices')

	df=df[['product_id', 'product_name', 'product_plataform','store_product_id', 'price', 'store_id', 'store_name', 'orders', 'order_ids', 'country','elastic_price']]

	if not df.empty:
		for index, row in df.iterrows():
			rows = dict(row)
			text = '''
			Productos Electrónicos con Precio Muy Bajo :alert: 
			(<= R$ 500)
			(<= COP 500.000)
			(<= ARS 9.000)
			(<= CLP 72694) 
			(<= MXN 2.000)
			Country: {country} :flag-{country}:
			:productos: 
			Product ID: {product_id}
			Product Name: {product_name}
			Plataform (CP/CMS): {product_plataform}
			Store Product ID: {store_product_id}
			Price: {price}
            Elastic Price: {elastic_price}
			Store ID: {store_id}
			Store Name: {store_name}
			Orders: {orders}
			Order IDs: {order_ids}
			'''.format(country=country, 
				product_plataform = row['product_plataform'],
				store_product_id = row['store_product_id'],
                elastic_price = row['elastic_price'],
				product_id = row['product_id'], 
				product_name = row['product_name'], 
				store_id = row['store_id'],
				store_name = row['store_name'],
				order_ids = row['order_ids'],
				price = row['price'],
				orders = row['orders'])

			print(text)
			slack.bot_slack(text,channel_alert)
	else:
		print('df vazio')

countries = ['mx','co','br','ar','cl']
for country in countries:
	print(country)
	try:
		df,df_oa = stockout_store_br(country)
		run_alarm(df,df_oa)
	except Exception as e:
		print(e)