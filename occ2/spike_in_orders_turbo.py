import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash
from lib import slack
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones, snippets
import regex as re


def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select store_id::text as identification_id, last_growth
    from
    (select store_id, last_value(growth) over (partition by store_id order by alarm_at asc) as last_growth
     from ops_occ.spike_in_orders_turbo
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    )
    group by 1,2
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df


def store_history(country):
    timezone, interval = timezones.country_timezones(country)
    query = """
  --no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method,
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp())))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '20minute', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '20minute', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '20minute', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D14'
          END AS calendar_day

   FROM {country}_CORE_ORDERS_PUBLIC.orders_vw b
   WHERE (created_at >= date(convert_timezone('America/{timezone}',current_timestamp())) - interval '14 days')
     and created_at < date(convert_timezone('America/{timezone}',current_timestamp()))
   AND to_char(created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '20minute', 'HH24:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_20minute AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          opd.name as product_name,
          calendar_day

   FROM base b
   INNER JOIN {country}_CORE_ORDERS_PUBLIC.order_stores_vw os ON os.order_id = b.order_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=b.order_id and os.store_id=op.store_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )

SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name
FROM day_20minute
group by 1,2,3,4,5,6,7
    """.format(country=country, timezone=timezone)

    df = snow.run_query(query)
    return df

def country_query(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
--no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '20 minutes', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '20 minutes', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '20 minutes', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D14'
          END AS calendar_day
          
   FROM orders b
   WHERE (created_at >= date(now() AT TIME ZONE 'America/{timezone}'))
   AND to_char(created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '20 minutes', 'HH24:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_hour AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          opd.name as product_name, 
          calendar_day

   FROM base b
   INNER JOIN order_stores os ON os.order_id = b.order_id
   AND (contact_email NOT LIKE '%@synth.rappi.com'
        OR contact_email IS NULL)
   left join order_product op on op.order_id=b.order_id and os.store_id=op.store_id
   left join order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )
   
SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name

   FROM day_hour
 
   group by 1,2,3,4,5,6,7
   """.format(country=country,timezone=timezone)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics

def store_infos(country):
    query= """
    --no_cache
    select a.store_id::text as storeid, bg.brand_group_id::text as store_brand_id, bg.brand_group_name as brand_name, 
    case when v.vertical in ('Express','Licores','Farmacia','Turbo') then 'Now'
         when v.vertical in ('Mercados') then 'Mercados'
         when v.vertical in ('Ecommerce') then 'Ecommerce' else null end as vertical
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw b on b.id=a.store_brand_id
    inner join (
    select store_type as storetype,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
    when upper (store_type) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype=a.type
    
    left join (
       select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
       ) bg on bg.store_brand_id = a.store_brand_id

    where a.deleted_at is null
    and not coalesce (a._fivetran_deleted, false)
    and vertical in ('Now','Turbo')
    group by 1,2,3,4""".format(country=country)

    df = snow.run_query(query)
    return df

def alarm(a,b):

    channel_alert = timezones.slack_channels('tech_integration')
    a['store_id']=a['store_id'].astype(str)
    b['storeid'] = b['storeid'].astype(str)
    df0 = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
    df0 = df0.drop(['key_0','storeid'], axis=1)

    df1_d0 = df0[((df0['calendar_day'] == 'D0'))]
    df1_d0 = df1_d0.drop_duplicates(subset=["store_id",'store_name', "store_brand_id", "brand_name", "vertical", "order_id"])

    df_for_ids = df1_d0.drop_duplicates(subset=['store_id','store_name', 'store_brand_id', 'vertical', 'order_id'])
    order_ids = df_for_ids.groupby(["store_id"])["order_id"].apply(list).reset_index(name='order_ids_d0')

    df1_d0_orders = df1_d0.groupby(["store_id",'store_name', 'store_brand_id', "brand_name", "vertical"])['order_id'].agg(
        ['count', ('orders_d0', lambda x: ', '.join(map(str, x)))]).reset_index().rename(columns={'count':'count_d0'})

    df1_d7 = df0[((df0['calendar_day'] == 'D7'))]
    df1_d7 = df1_d7.drop_duplicates(subset=["store_id",'store_name', "store_brand_id", "brand_name", "vertical", "order_id"])

    df1_d7_orders = df1_d7.groupby(["store_id",'store_name', 'store_brand_id', "brand_name", "vertical"])['order_id'].agg(
        ['count', ('orders_d7', lambda x: ', '.join(map(str, x)))]).reset_index().rename(columns={'count':'count_d7'})

    df1_d14 = df0[((df0['calendar_day'] == 'D14'))]
    df1_d14 = df1_d14.drop_duplicates(subset=["store_id",'store_name', "store_brand_id", "brand_name", "vertical", "order_id"])
    df1_d14_orders = df1_d14.groupby(["store_id",'store_name', 'store_brand_id', "brand_name", "vertical"])['order_id'].agg(
        ['count', ('orders_d14', lambda x: ', '.join(map(str, x)))]).reset_index().reset_index().rename(columns={'count':'count_d14'})

    final_df = pd.concat([df1_d0_orders, df1_d7_orders, df1_d14_orders], axis=0, ignore_index=True)

    final_df['orders_d0'].fillna(0, inplace=True)
    final_df['orders_d7'].fillna(0, inplace=True)
    final_df['orders_d14'].fillna(0, inplace=True)
    final_df['count_d0'].fillna(0, inplace=True)
    final_df['count_d7'].fillna(0, inplace=True)
    final_df['count_d14'].fillna(0, inplace=True)

    df1 = final_df
    print(df1)
    df1['average_last_weeks'] = ((df1['count_d7'] + df1['count_d14']) / 2)
    df1['growth'] = round((((df1['count_d0'] - df1['average_last_weeks']) / df1['average_last_weeks'])*100),2)
    df1['growth'] = df1['growth'].replace(np.inf, np.nan)


    df1 = df1[(df1['count_d0'] >= 8) & (df1['average_last_weeks'] >= 2.5)]

    if not df1.empty:
        c = excluding_stores(country)
        df1['store_brand_id']=df1['store_brand_id'].astype(str)
        c['identification_id'] = c['identification_id'].astype(str)
        df1 = pd.merge(df1, c, how='left', left_on=df1['store_brand_id'], right_on=c['identification_id'])
        df1 = df1[(df1['identification_id'].isnull()) | (df1['growth'] > df1['last_growth'])]
        df1 = df1.drop(['key_0', 'identification_id', 'last_growth'], axis=1)

        print(df1)

        current_time = timezones.country_current_time(country)
        to_upload = df1
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        print(to_upload)
        to_upload = to_upload[['store_id', 'store_brand_id', 'brand_name', 'vertical', 'count_d0',
           'orders_d0', 'count_d7', 'orders_d7', 'count_d14', 'orders_d14',
           'average_last_weeks', 'growth','alarm_at','country']]
        to_upload['orders_d0']=to_upload['orders_d0'].astype(str)

        snow.upload_df_occ(to_upload, 'spike_in_orders_turbo')

        for index, row in df1.iterrows():
            rows = dict(row)
            df_row = df0[((df0['store_id'] == row['store_id']) & (df0['store_brand_id'] == row['store_brand_id']))]
            print(df_row)
            df_row_0 = df_row[((df_row['calendar_day'] == 'D0'))]
            df_row_0 = df_row_0.fillna(0)
            df_row_0 = df_row_0.drop_duplicates(
                subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"]).reset_index()
            df_row_0 = df_row_0.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d0")
            print(df_row_0)
            df_row_7 = df_row[((df_row['calendar_day'] == 'D7'))]
            df_row_7 = df_row_7.fillna(0)
            df_row_7 = df_row_7.drop_duplicates(subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"])
            df_row_7 = df_row_7.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d7")
            print(df_row_7)
            df_row_14 = df_row[((df_row['calendar_day'] == 'D14'))]
            df_row_14 = df_row_14.fillna(0)
            df_row_14 = df_row_14.drop_duplicates(subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"])
            df_row_14 = df_row_14.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d14")
            print(df_row_14)
            df_row_final = pd.concat([df_row_7,df_row_14,df_row_0], axis=0, ignore_index=True)
            print(df_row_final)

            df_row_final['orders_d0'].fillna(0, inplace=True)
            df_row_final['orders_d7'].fillna(0, inplace=True)
            df_row_final['orders_d14'].fillna(0, inplace=True)

            df2 = df_row_final.groupby(["store_id", "store_name"])[
                ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df2['orders_d0'])
            df2['share_d0'] = round(((df2['orders_d0'] / soma) * 100), 2)
            df2['average_last_weeks'] = ((df2['orders_d7'] + df2['orders_d14']) / 2)
            df2['growth'] = round((((df2['orders_d0'] - df2['average_last_weeks']) / df2['average_last_weeks']) * 100), 2)
            df2['store_id']=df2['store_id'].astype(str)
            order_ids['store_id'] = order_ids['store_id'].astype(str)
            df2 = pd.merge(df2, order_ids, on=['store_id'], how='left')
            df2 = df2[['store_id', "store_name", 'orders_d0', 'share_d0', 'orders_d7', 'orders_d14', 'average_last_weeks',
                       'growth', 'order_ids_d0']]
            df2 = df2.sort_values(by='orders_d0', ascending=False)
            home = expanduser("~")
            results_file = '{}/spike_in_orders_turbo.xlsx'.format(home)
            df2.to_excel(results_file, sheet_name='stores', index=False)
            book = load_workbook(results_file)
            writer = pd.ExcelWriter(results_file, engine='openpyxl')
            writer.book = book
            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

            df3 = df_row_final.groupby(["coupon_code"])[["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df3['orders_d0'])
            df3['share_d0'] = round(((df3['orders_d0'] / soma) * 100), 2)
            df3['average_last_weeks'] = ((df3['orders_d7'] + df3['orders_d14']) / 2)
            df3['growth'] = round((((df3['orders_d0'] - df3['average_last_weeks']) / df3['average_last_weeks']) * 100), 2)
            df3 = df3[['coupon_code', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks", "growth"]]
            df3 = df3.sort_values(by='orders_d0', ascending=False)
            df3.to_excel(writer, "coupon_code", index=False)

            df4 = df_row_final.groupby(["payment_method"])[["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df4['orders_d0'])
            df4['share_d0'] = round(((df4['orders_d0'] / soma) * 100), 2)
            df4['average_last_weeks'] = ((df4['orders_d7'] + df4['orders_d14']) / 2)
            df4['growth'] = round((((df4['orders_d0'] - df4['average_last_weeks']) / df4['average_last_weeks']) * 100), 2)
            df4 = df4[
                ['payment_method', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks", "growth"]]
            df4 = df4.sort_values(by='orders_d0', ascending=False)
            df4.to_excel(writer, "payment_method", index=False)

            df5 = df_row_final.groupby(["application_user_id"])[
                ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
            soma = sum(df4['orders_d0'])
            df5['share_d0'] = round(((df5['orders_d0'] / soma) * 100), 2)
            df5['average_last_weeks'] = ((df5['orders_d7'] + df5['orders_d14']) / 2)
            df5['growth'] = round((((df5['orders_d0'] - df5['average_last_weeks']) / df4['average_last_weeks']) * 100), 2)
            df5 = df5[
                ['application_user_id', "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks", "growth"]]
            df5 = df5.sort_values(by='orders_d0', ascending=False)
            df5.to_excel(writer, "application_user_id", index=False)

            try:
                prod0 = df_row
                prod0 = prod0[((prod0['calendar_day'] == 'D0'))]
                prod0 = prod0.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
                prod0 = prod0.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d0")

                prod7 = df_row
                prod7 = prod7[((prod7['calendar_day'] == 'D7'))]
                prod7 = prod7.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
                prod7 = prod7.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d7")

                prod14 = df_row
                prod14 = prod14[((prod14['calendar_day'] == 'D14'))]
                prod14 = prod14.drop_duplicates(subset=['product_id', 'product_name', 'order_id'])
                prod14 = prod14.groupby(["product_id", "product_name"])['order_id'].count().reset_index(name="orders_d14")

                prod_final = pd.concat([prod0, prod7, prod14], axis=0, ignore_index=True)

                prod_final['orders_d0'].fillna(0, inplace=True)
                prod_final['orders_d7'].fillna(0, inplace=True)
                prod_final['orders_d14'].fillna(0, inplace=True)

                prod_final = prod_final.groupby(["product_id", "product_name"])[
                    ["orders_d0", "orders_d7", "orders_d14"]].sum().reset_index()
                soma = sum(prod_final['orders_d0'])
                prod_final['share_d0'] = round(((prod_final['orders_d0'] / soma) * 100), 2)
                prod_final['average_last_weeks'] = ((prod_final['orders_d7'] + prod_final['orders_d14']) / 2)
                prod_final['growth'] = round((((prod_final['orders_d0'] - prod_final['average_last_weeks']) / prod_final[
                    'average_last_weeks']) * 100), 2)
                prod_final = prod_final[
                    ['product_id', "product_name", "orders_d0", "orders_d7", "orders_d14", "share_d0", "average_last_weeks",
                     "growth"]]
                prod_final = prod_final.sort_values(by='orders_d0', ascending=False)
                prod_final.to_excel(writer, "product_id", index=False)

            except:
                print("deu merda product_id")

            writer.save()

            text = '''*Alarma de pico de ordenes creadas Turbo:alert: :arrow_up:* 
        :flag-{country}:
        Pais: {country}
        *Store ID:* {store_id}
        *Store Name* {store_name}
        *Threshold:* Alcanzó *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* mismos 20 minutos/día semanas pasadas
        *Porcentaje de crecimiento:* {growth}%
        Adjunto más detalles por tiendas, cupones, métodos de pagos, top products y users'''.format(
                store_id=row['store_id'],
                store_name=row['store_name'],
                vertical=row['vertical'],
                d0=row['count_d0'],
                d7=row['orders_d7'],
                average_last_weeks=row['average_last_weeks'],
                growth=round(row['growth'], 2),
                country=country)

            print(text)
            slack.file_upload_channel(channel_alert,text,results_file,'xlsx')

            if os.path.exists(results_file):
                os.remove(results_file)
    else:
        print('df1 null')

countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        a = country_query(country)
        d = store_history(country)
        a = pd.concat([a, d])
        b = store_infos(country)
        alarm(a,b)
    except Exception as e:
        print(e)
