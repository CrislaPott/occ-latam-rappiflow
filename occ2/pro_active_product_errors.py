import os, sys, json
import numpy as np
import pandas as pd
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

def get_logs(country):
    query = f'''
    select product_id, max(sync_at) as sync_at
    from ops_occ.wrong_product_price
    where sync_at >= current_date() - interval '6h'
    and country = '{country}'
    group by 1
    '''
    df = snow.run_query(query)
    return df

def balance_products(country, store_id):
    query = '''
    --no_cache
    with b as (
    SELECT ps.store_id::text as store_id,ps.product_id as product_id,ps.is_available,ps.is_discontinued,ps.price,ps.balance_price
          FROM product_store ps
          WHERE ps.store_id in ({store_id})
          )
    SELECT b.*,sale_type,p.name
    FROM b
    JOIN products p ON p.id=b.product_id
    WHERE sale_type IN ('WW','WU','WB','WP')
    and is_available = TRUE AND is_discontinued = FALSE AND (balance_price = 0 OR balance_price IS NULL)
    '''.format(country=country, store_id=store_id)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics

def product_price(country, store_id):
    query = """
    with b as (SELECT ps.store_id::text as store_id,ps.product_id as product_id,ps.is_available,ps.is_discontinued,ps.price,ps.balance_price
    FROM product_store ps 
    WHERE store_id in ({store_id})
    )
    SELECT b.*,sale_type,p.name
    FROM b
    JOIN products p ON p.id=b.product_id
    where
    is_available = TRUE AND is_discontinued = FALSE
    AND (price = 0 OR price IS NULL)
    """.format(countrt=country, store_id=store_id)

    if country == 'co':
        metrics = redash.run_query(1, query)
    elif country == 'ar':
        metrics = redash.run_query(206, query)
    elif country == 'cl':
        metrics = redash.run_query(248, query)
    elif country == 'mx':
        metrics = redash.run_query(3, query)
    elif country == 'uy':
        metrics = redash.run_query(310, query)
    elif country == 'ec':
        metrics = redash.run_query(1920, query)
    elif country == 'cr':
        metrics = redash.run_query(1919, query)
    elif country == 'pe':
        metrics = redash.run_query(673, query)
    elif country == 'br':
        metrics = redash.run_query(4, query)

    return metrics

def enabled_stores(country,vertical, express):
    if express == 'yes':
        query = """
    --no_cache
    SELECT distinct store_id::text as store_id
    FROM stores s 
    join verticals v on v.store_type=s.type
    WHERE is_enabled=TRUE
    and s.deleted_at is null
    AND TYPE NOT IN ('grin',
                   'rentbrella',
                   'rappi_prime',
                   'restaurant',
                   'courier_hours',
                   'courier_sampling')
    AND name NOT ILIKE '%teste%'
    AND name NOT ILIKE '%prueba%'
    and v.sub_group in ('{vertical}')
    """.format(countrt=country,vertical=vertical)
    else:
        query = """
        --no_cache
    SELECT distinct store_id::text as store_id
    FROM stores s 
    join verticals v on v.store_type=s.type
    WHERE is_enabled=TRUE
    and s.deleted_at is null
    AND TYPE NOT IN ('grin',
                   'rentbrella',
                   'rappi_prime',
                   'restaurant',
                   'courier_hours',
                   'courier_sampling')
    AND name NOT ILIKE '%teste%'
    AND name NOT ILIKE '%prueba%'
    and v.sub_group not in ('{vertical}')
    """.format(countrt=country,vertical=vertical)

    print(query)
    if country == 'co':
        metrics = redash.run_query(1, query)
    elif country == 'ar':
        metrics = redash.run_query(206, query)
    elif country == 'cl':
        metrics = redash.run_query(248, query)
    elif country == 'mx':
        metrics = redash.run_query(3, query)
    elif country == 'uy':
        metrics = redash.run_query(310, query)
    elif country == 'ec':
        metrics = redash.run_query(1920, query)
    elif country == 'cr':
        metrics = redash.run_query(1919, query)
    elif country == 'pe':
        metrics = redash.run_query(673, query)
    elif country == 'br':
        metrics = redash.run_query(4, query)

    return metrics

def offenders(c,d,e,f,l):
    if country == 'co':
        channel_alert = 'C01C1LSQREW'
    elif country == 'ar':
        channel_alert = 'C01EEPH2ECU'
    elif country == 'cl':
        channel_alert = 'C01EEPH2ECU'
    elif country == 'mx':
        channel_alert = 'C01GMHJRDM2'
    elif country == 'uy':
        channel_alert = 'C01E5G49L1K'
    elif country == 'ec':
        channel_alert = 'C01E5G49L1K'
    elif country == 'cr':
        channel_alert = 'C01E5G49L1K'
    elif country == 'pe':
        channel_alert = 'C01E5G49L1K'
    elif country == 'br':
        channel_alert = 'C01672B528Y'

    current_time = timezones.country_current_time(country)
    df = pd.concat([c,d,e,f])
    df = pd.merge(df,l,how='left',on=['product_id'])
    df = df[(df['sync_at'].isnull()) & (~df['product_id'].isnull())]

    if not df.empty:
        text = f'''
        Alarma proactivo de productos con precio incorrecto :dollar:
        Pais: {country} :flag-{country}:
        '''
        home = expanduser("~")
        results_file = '{}/wrong_product_price_{country}.xlsx'.format(home,country=country)
        df.to_excel(results_file,sheet_name='wrong_price', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
    
        df['sync_at'] = current_time
        df['country'] = country
        snow.upload_df_occ(df, 'wrong_product_price')

def run_alarm(country):
    l = get_logs(country)
    a = enabled_stores(country,'express','yes')
    b = enabled_stores(country,'express','no')
    try:
        express_list = a['store_id'].to_list()
        my_express_string = ','.join(map(str, express_list))
    except:
        print("fail")
    try:
        no_express_list = b['store_id'].to_list()
        my_no_express_string = ','.join(map(str, express_list))
    except:
        print("fail")

    print("pesaveis")
    try:
        print("express")
        c = balance_products(country, my_express_string)
    except:
        column_names = ['store_id','product_id','is_available','is_discontinued','price','balance_price','sale_type','name']
        c = pd.DataFrame(columns = column_names, index=range(1))
    try:
        print("no_express_list")
        d = balance_products(country,my_no_express_string)
    except:
        column_names = ['store_id','product_id','is_available','is_discontinued','price','balance_price','sale_type','name']
        d = pd.DataFrame(columns = column_names, index=range(1))
    
    print("nao pesaveis")
    try:
        print("express")
        e = product_price(country, my_express_string)
    except:
        column_names = ['store_id','product_id','is_available','is_discontinued','price','balance_price','sale_type','name']
        e = pd.DataFrame(columns = column_names, index=range(1))
    try:
        print("no express")
        f = product_price(country,my_no_express_string)
    except:
        column_names = ['store_id','product_id','is_available','is_discontinued','price','balance_price','sale_type','name']
        f = pd.DataFrame(columns = column_names, index=range(1))
    
    print("starting")
    print(c)
    print(d)
    print(e)
    print(f)
    offenders(c,d,e,f,l)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)