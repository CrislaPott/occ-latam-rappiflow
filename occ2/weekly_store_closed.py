import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from os.path import expanduser
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from functions import timezones

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''
    select distinct store_id::text as identification_id
    from ops_occ.weekly_store_closed
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def offenders(country):
  timezone, interval = timezones.country_timezones(country)

  query_pt = '''
  select * from (
  with chats as (
  select distinct
      order_id
    from
      {country}_PG_MS_REALTIME_INTERFACE_PUBLIC.CHATS
    where
      dateadd(hour, -6, created_at)::date >= dateadd(day,-8,convert_timezone('America/{timezone}', current_timestamp))::date
and (trim(data) ilike '%fechada%'
        or trim(data) ilike '%fechado%'
        or trim(data) ilike '%fechando%'
        or trim(data) ilike '%foto%faixada%'
        or trim(data) ilike '%foto%fachada%'
        or trim(data) ilike '%foto%do%estabelecimento%'
        or trim(data) ilike '%foto%da%loja%'
        or trim(data) ilike '%fechou%loja%'
        or trim(data) ilike '%estabelecimento%fechou%'
        or trim(data) ilike '%loja%fechou%'
        or trim(data) ilike '%restaurante%fechou'
        or trim(data) ilike '%local%fechou%'
        or trim(data) ilike '%local%fechado%'
        or trim(data) ilike '%posto%fechado%'
        or trim(data) ilike '%farm%cia%fechada%'
        or trim(data) ilike '%restaurante%fechado%'
        or trim(data) ilike '%posto%fechou%'
        or trim(data) ilike '%mercado%fechado%'
        or trim(data) ilike '%mercado%fechou%'
        or trim(data) ilike '%fechado%reforma%'
        or trim(data) ilike '%encerramento%atividades%estabelecimento%'
      )
      and sender_type ilike '%storekeeper%'
      )

, reasons as (
select distinct order_id
from {country}_CORE_ORDERS_PUBLIC.order_modifications
where ((type in ('cancel_by_support')
  and (params:cancelation_reason)::text in ('crsan_closed_store'))
 or (type in ('canceled_store_closed')))
  and created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))::date
)

, store_closed as (
select * from reasons
union all
select * from chats
)

, cancel as (select order_id, max(created_at) as canceled from {country}_core_orders_public.order_modifications 
where created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))
and type ilike '%cancel%' 
group by 1)

, store_users as (
  select DISTINCT STORE_ID,FIRST_VALUE(USER_ID) OVER (PARTITION BY STORE_ID ORDER BY ID DESC ) AS USER_ID
 FROM {country}_GRABILITY_PUBLIC.STORE_COMMISSIONS SC
  where _fivetran_deleted=false
  )
, kam as (
  select store_id, USER_ID, u.email
from store_users sc
  join {country}_GRABILITY_PUBLIC.USERS u on u.id = sc.user_id
where _fivetran_deleted=false
  group by 1,2,3)

select '{country}' as country
, os.store_id::text as store_id
,os.name as store_name
,v.sub_group as vertical
,coalesce(kam.email,'Não há KAM no Sistema') as kam
,round(t0.perc_cancellation,2) as perc_cancellation
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=0 then o.id else null end) as Domingo
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=1 then o.id else null end) as Segunda
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=2 then o.id else null end) as Terca
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=3 then o.id else null end) as Quarta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=4 then o.id else null end) as Quinta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=5 then o.id else null end) as Sexta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=6 then o.id else null end) as Sabado
,count(distinct o.id) as cancels
,listagg(distinct to_char(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz,'HH:MI'),', ') as cancel_times
,listagg(distinct coalesce(cancel.canceled,o.updated_at)::date,', ') as cancel_dates
,listagg(distinct o.id, '; ') as order_ids
,max(coalesce(cancel.canceled,o.updated_at)) as last_cancel
from store_closed sc
join cancel on cancel.order_id=sc.order_id
inner join {country}_core_orders_public.orders o on o.id=sc.order_id and coalesce(o._fivetran_deleted, false)=false
inner join {country}_core_orders_public.order_stores os on os.order_id=o.id and coalesce(os._fivetran_deleted, false)=false
left join kam on kam.store_id=os.store_id
left join 
( select *
    from (select distinct
    store_type,
    last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from
    {country}_PGLR_MS_STORES_PUBLIC.verticals
    ) group by 1,2
) v on v.store_type=os.type

 left join (select store_id, (count(distinct case when o.state ilike '%cancel%' then o.id else null end)/count(distinct o.id))*100 as perc_cancellation
        from {country}_CORE_ORDERS_PUBLIC.orders_vw o
        join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
        where o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
        and coalesce(o._fivetran_deleted,'false')=false
        and coalesce(o.closed_at, o.updated_at)::date >= dateadd(day,-14,convert_timezone('America/{timezone}', current_timestamp))::date
        group by 1
        ) t0 on t0.store_id=os.store_id

where os.type not in ('courier_hours','courier_sampling','queue_order_flow') and os.type not ilike '%rappi%pay%'
and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
and v.sub_group not in ('Rappicash','Rappi')
and os.name not ilike '%Dummy%Store%Rappi%Connect%'
and perc_cancellation >= 30
and o.state ilike '%cancel%' and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
and os.store_id not in (900148653)
group by 1,2,3,4,5,6

having count(distinct coalesce(cancel.canceled,o.updated_at)::date) > 1
and max(coalesce(cancel.canceled,o.updated_at))::date = convert_timezone('America/{timezone}', current_timestamp)::date
and cancels >= 4 
) br
'''.format(country=country,timezone=timezone)
  
  query_ss = '''
  select * from (
with chats as (
    select distinct
      order_id
    from
      informatica.{country}_PG_MS_REALTIME_INTERFACE_PUBLIC.CHATS
    where
      dateadd(hour, -8, created_at)::date >= dateadd(day,-8,convert_timezone('America/{timezone}', current_timestamp))::date
and (trim(data) ilike '%está cerrad%'
        or trim(data) ilike '%esta%cerrad%'
        or trim(data) ilike '%tienda%cerrad%'
        or trim(data) ilike '%tienda%cerrada%'
                or trim(data) ilike '%encuentra%cerrad%'
        or trim(data) ilike '%están%cerrada%'
        or trim(data) ilike '%estan%cerrada%'
        or trim(data) ilike '%estan%cerrado%'
        or trim(data) ilike '%están%cerrando%'
        or trim(data) ilike '%estan%cerrando%'
        or trim(data) ilike '%está%serrada%'
        or trim(data) ilike '%esta%serrada%'
        or trim(data) ilike '%ya%cerro%'
      )
      and sender_type ilike '%storekeeper%'
      )

, reasons as (
select distinct order_id
from {country}_CORE_ORDERS_PUBLIC.order_modifications_vw
where ((type in ('cancel_by_support')
  and (params:cancelation_reason)::text in ('crsan_closed_store'))
 or (type in ('canceled_store_closed')))
  and created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))::date
)

, store_closed as (
select * from reasons
union all
select * from chats
)

, cancel as (select order_id, max(created_at) as canceled from {country}_core_orders_public.order_modifications 
where created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))
and type ilike '%cancel%' 
group by 1)

, store_users as (
  select DISTINCT STORE_ID,FIRST_VALUE(USER_ID) OVER (PARTITION BY STORE_ID ORDER BY ID DESC ) AS USER_ID
 FROM {country}_GRABILITY_PUBLIC.STORE_COMMISSIONS_vw SC
  where _fivetran_deleted=false
  )
, kam as (
  select store_id, USER_ID, u.email
from store_users sc
  join {country}_GRABILITY_PUBLIC.USERS_vw u on u.id = sc.user_id
where _fivetran_deleted=false
  group by 1,2,3)

select '{country}' as country
, os.store_id::text as store_id
,os.name as store_name
,v.sub_group as vertical
,coalesce(kam.email,'Não há KAM no Sistema') as kam
,round(t0.perc_cancellation,2) as perc_cancellation
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=0 then o.id else null end) as Domingo
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=1 then o.id else null end) as Segunda
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=2 then o.id else null end) as Terca
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=3 then o.id else null end) as Quarta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=4 then o.id else null end) as Quinta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=5 then o.id else null end) as Sexta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=6 then o.id else null end) as Sabado
,count(distinct o.id) as cancels
,listagg(distinct to_char(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz,'HH:MI'),', ') as cancel_times
,listagg(distinct coalesce(cancel.canceled,o.updated_at)::timestamp_ntz::date,', ') as cancel_dates
,listagg(distinct o.id, '; ') as order_ids
,max(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz) as last_cancel
from store_closed sc
join cancel on cancel.order_id=sc.order_id
inner join {country}_core_orders_public.orders_vw o on o.id=sc.order_id and coalesce(o._fivetran_deleted, false)=false
inner join {country}_core_orders_public.order_stores_vw os on os.order_id=o.id and coalesce(os._fivetran_deleted, false)=false
left join kam on kam.store_id=os.store_id
left join 
( select *
    from (select distinct
    store_type,
    last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from
    {country}_PGLR_MS_STORES_PUBLIC.verticals
    ) group by 1,2
) v on v.store_type=os.type

 left join (select store_id, (count(distinct case when o.state ilike '%cancel%' then o.id else null end)/count(distinct o.id))*100 as perc_cancellation
        from {country}_CORE_ORDERS_PUBLIC.orders_vw o
        join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
        where o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
        and coalesce(o._fivetran_deleted,'false')=false
        and coalesce(o.closed_at, o.updated_at)::date >= dateadd(day,-14,convert_timezone('America/{timezone}', current_timestamp))::date
        group by 1
        ) t0 on t0.store_id=os.store_id

where os.type not in ('courier_hours','courier_sampling','queue_order_flow','soat_webview') and os.type not ilike '%rappi%pay%'
and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
and v.sub_group not in ('Rappicash','Rappi')
and os.name not ilike '%Dummy%Store%Rappi%Connect%'
and perc_cancellation >= 30
and o.state ilike '%cancel%' and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
group by 1,2,3,4,5,6

having count(distinct coalesce(cancel.canceled,o.updated_at)::timestamp_ntz::date) > 1
and max(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)::date = convert_timezone('America/{timezone}', current_timestamp)::date
and cancels >= 4 
 ) 
 '''.format(country=country, timezone=timezone)

  query_ss2 = '''
  select * from (
with chats as (
    select distinct
      order_id
    from
      {country}_PG_MS_REALTIME_INTERFACE_PUBLIC.CHATS
    where
      dateadd(hour, -8, created_at)::date >= dateadd(day,-8,convert_timezone('America/{timezone}', current_timestamp))::date
and (trim(data) ilike '%está cerrad%'
        or trim(data) ilike '%esta%cerrad%'
        or trim(data) ilike '%tienda%cerrad%'
        or trim(data) ilike '%tienda%cerrada%'
                or trim(data) ilike '%encuentra%cerrad%'
        or trim(data) ilike '%están%cerrada%'
        or trim(data) ilike '%estan%cerrada%'
        or trim(data) ilike '%estan%cerrado%'
        or trim(data) ilike '%están%cerrando%'
        or trim(data) ilike '%estan%cerrando%'
        or trim(data) ilike '%está%serrada%'
        or trim(data) ilike '%esta%serrada%'
        or trim(data) ilike '%ya%cerro%'
      )
      and sender_type ilike '%storekeeper%'
      )

, reasons as (
select distinct order_id
from {country}_CORE_ORDERS_PUBLIC.order_modifications_vw
where ((type in ('cancel_by_support')
  and (params:cancelation_reason)::text in ('crsan_closed_store'))
 or (type in ('canceled_store_closed')))
  and created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))::date
)

, store_closed as (
select * from reasons
union all
select * from chats
)

, cancel as (select order_id, max(created_at) as canceled from {country}_core_orders_public.order_modifications 
where created_at::date >= dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))
and type ilike '%cancel%' 
group by 1)

, store_users as (
  select DISTINCT STORE_ID,FIRST_VALUE(USER_ID) OVER (PARTITION BY STORE_ID ORDER BY ID DESC ) AS USER_ID
 FROM {country}_GRABILITY_PUBLIC.STORE_COMMISSIONS_vw SC
  where _fivetran_deleted=false
  )
, kam as (
  select store_id, USER_ID, u.email
from store_users sc
  join {country}_GRABILITY_PUBLIC.USERS_vw u on u.id = sc.user_id
where _fivetran_deleted=false
  group by 1,2,3)

select '{country}' as country
, os.store_id::text as store_id
,os.name as store_name
,v.sub_group as vertical
,coalesce(kam.email,'Não há KAM no Sistema') as kam
,round(t0.perc_cancellation,2) as perc_cancellation
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=0 then o.id else null end) as Domingo
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=1 then o.id else null end) as Segunda
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=2 then o.id else null end) as Terca
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=3 then o.id else null end) as Quarta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=4 then o.id else null end) as Quinta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=5 then o.id else null end) as Sexta
,count(distinct case when extract(dow from coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)=6 then o.id else null end) as Sabado
,count(distinct o.id) as cancels
,listagg(distinct to_char(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz,'HH:MI'),', ') as cancel_times
,listagg(distinct coalesce(cancel.canceled,o.updated_at)::timestamp_ntz::date,', ') as cancel_dates
,listagg(distinct o.id, '; ') as order_ids
,max(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz) as last_cancel
from store_closed sc
join cancel on cancel.order_id=sc.order_id
inner join {country}_core_orders_public.orders_vw o on o.id=sc.order_id and coalesce(o._fivetran_deleted, false)=false
inner join {country}_core_orders_public.order_stores_vw os on os.order_id=o.id and coalesce(os._fivetran_deleted, false)=false
left join kam on kam.store_id=os.store_id
left join 
( select *
    from (select distinct
    store_type,
    last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
    from
    {country}_PGLR_MS_STORES_PUBLIC.verticals
    ) group by 1,2
) v on v.store_type=os.type

 left join (select store_id, (count(distinct case when o.state ilike '%cancel%' then o.id else null end)/count(distinct o.id))*100 as perc_cancellation
        from {country}_CORE_ORDERS_PUBLIC.orders_vw o
        join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
        where o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
        and coalesce(o._fivetran_deleted,'false')=false
        and coalesce(o.closed_at, o.updated_at)::date >= dateadd(day,-14,convert_timezone('America/{timezone}', current_timestamp))::date
        group by 1
        ) t0 on t0.store_id=os.store_id

where os.type not in ('courier_hours','courier_sampling','queue_order_flow') and os.type not ilike '%rappi%pay%'
and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
and v.sub_group not in ('Rappicash','Rappi')
and os.name not ilike '%Dummy%Store%Rappi%Connect%'
and perc_cancellation >= 30
and o.state ilike '%cancel%' and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
group by 1,2,3,4,5,6

having count(distinct coalesce(cancel.canceled,o.updated_at)::timestamp_ntz::date) > 1
and max(coalesce(cancel.canceled,o.updated_at)::timestamp_ntz)::date = convert_timezone('America/{timezone}', current_timestamp)::date
and cancels >= 4 
 ) 
 '''.format(country=country, timezone=timezone)

  if country == 'br':
    query = query_pt
  elif country == 'co':
    query = query_ss
  else:
    query = query_ss2

  df = snow.run_query(query)
  print(df)
  return df

def new_offenders(a,b):
    if country == 'co':
        channel_alert = 'C01C1LSQREW'
        channel = 'C010GBS4BT7'
    elif country == 'ar':
        channel_alert = 'C01EEPH2ECU'
        channel = 'C010GB6FJ1W'
    elif country == 'cl':
        channel_alert = 'C01EEPH2ECU'
        channel = 'C0104SW3HS7'
    elif country == 'mx':
        channel_alert = 'C01GMHJRDM2'
        channel = 'C0104SVEZEX'
    elif country == 'uy':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0103HJS1B4'
    elif country == 'ec':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0104SXCA4B'
    elif country == 'cr':
        channel_alert = 'C01E5G49L1K'
        channel = 'G01GQ1F4XFU'
    elif country == 'pe':
        channel_alert = 'C01E5G49L1K'
        channel = 'C0103HK55CJ'
    elif country == 'br':
        channel_alert = 'C01672B528Y'
        channel = 'CU3E4KNTY'

    current_time = timezones.country_current_time(country)

    df = pd.merge(b,a,how='left',left_on=b['store_id'],right_on=a['identification_id'])
    news = df[((df['identification_id'].isnull()))]
    news = news.drop(['key_0','identification_id'], axis=1)

    to_upload = news
    to_upload['alarm_at'] = current_time
    to_upload['country'] = country
    to_upload = to_upload.rename(columns={'order_ids_day': 'order_ids'})
    to_upload = to_upload[['store_id','alarm_at','country','order_ids','segunda','terca','quarta','quinta','sexta','sabado','domingo']]
    snow.upload_df_occ(to_upload,'weekly_store_closed')

    for index, row in news.iterrows():
        rows = dict(row)
        text = '''
        :flag-{country}:
        Pais: {country}
        Store ID: {store_id}
        Store Name: {name}
        kam: {kam}
        Vertical: {vertical}
        Problema: La Tienda ha tenido cancelaciones debido a la tienda cerrada en la semana <!subteam^S018ASNLN5T> :alert:
        Cancelamiento lunes: {segunda}
        Cancelamiento martes: {terca}
        Cancelamiento miercoles: {quarta}
        Cancelamiento jueves: {quinta}
        Cancelamiento viernes: {sexta}
        Cancelamiento Sabado: {sabado}
        Cancelamiento Domingo: {domingo}
        Fechas com cancelaciones: {datas}
        Momientos en que ocurrio las cancelaciones: {horarios}
        Order Ids: {order_ids}
        Porcentage Cancelacion General (ultimos 7 dias): {perc_cancellation}%
        '''.format(
            country=country,
            store_id=row['store_id'],
            name=row['store_name'],
            kam=row['kam'],
            vertical=row['vertical'],
            segunda=row['segunda'],
            terca=row['terca'],
            quarta=row['quarta'],
            quinta=row['quinta'],
            sexta=row['sexta'],
            sabado=row['sabado'],
            domingo=row['domingo'],
            datas=row['cancel_dates'],
            horarios=row['cancel_times'],
            order_ids=row['order_ids'],
            perc_cancellation=row['perc_cancellation']
            )
        slack.bot_slack(text,channel_alert)

def run_alarm(country):
    a = excluding_stores(country)
    b = offenders(country)
    new_offenders(a,b)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)

