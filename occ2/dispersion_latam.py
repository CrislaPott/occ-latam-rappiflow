import os, sys, json
import numpy as np
import pandas as pd
from time import sleep
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from slack import WebClient
from functions import timezones
from dotenv import load_dotenv
from os.path import expanduser
from airflow.models import Variable

load_dotenv()

def point_of_contact(country):
	new_google_sheet_url = "https://docs.google.com/spreadsheets/d/13p7QYmkwyqNVEg1RER4Y5HMC8jC0L91W3y-Y3Myy6Jw/export?format=csv&gid=0"
	pocs_list = pd.read_csv(new_google_sheet_url, error_bad_lines=False)
	pocs_list = pocs_list[(pocs_list["Country"] == f'{country}')]
	pocs_list = pocs_list['POCs'].to_list()
	pocs_list = ''.join(pocs_list)
	print(pocs_list)
	return pocs_list


def get_channel_history(channel, messages_per_page, max_messages, oldest, latest):
	client = WebClient(token=Variable.get('SLACK_TOKEN'))
	page = 1
	print("Retrieving page {}".format(page))
	response = client.conversations_history(
		channel=channel,
		limit=messages_per_page,
		oldest=str(oldest.timestamp()),
		latest=str(latest.timestamp())
		)
	assert response["ok"]
	messages_all = response['messages']
	while len(messages_all) + messages_per_page <= max_messages and response['has_more']:
		page += 1
		print("Retrieving page {}".format(page))
		sleep(1)   # need to wait 1 sec before next call due to rate limits
		response = client.conversations_history(
			channel=channel,
			limit=messages_per_page,
			oldest=str(oldest.timestamp()),
			latest=str(latest.timestamp()),
			cursor=response['response_metadata']['next_cursor']
			)
		assert response["ok"]
		messages = response['messages']
		messages_all = messages_all + messages

	print(
		"Fetched a total of {} messages from channel {}".format(
			len(messages_all),
			channel
			))

	return pd.DataFrame(messages_all)

def get_last_alarms():
	try:
		latest = datetime.now()
		oldest = latest - timedelta(minutes=60)
		channel_name = 'CS7UQAMLG'

		channel_history = get_channel_history(
			channel = channel_name,
			messages_per_page = 500,
			max_messages = 20000,
			oldest = oldest,
			latest = latest
		)

		last_alarms = channel_history[
			channel_history['text'].str.contains('Alarma de Dispersion')
		]

		last_alarms = last_alarms[['ts', 'text']]
		last_alarms['country'] = last_alarms['text'].str.extract(r"(?<=Country\:)(.*)")
		last_alarms['country'] = last_alarms['country'].str[:3]
		last_alarms['country'] = last_alarms['country'].str.strip()

		return last_alarms['country'].unique().tolist()

	except:
		return []

def country_query(country):
	timezone, interval = timezones.country_timezones(country)

	if country == 'br':
		query_br =  ''' --no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='hubfintech' then id else null end) as hubfintech, 
		count(distinct case when meta ->> 'gateway'='bpp' then id else null end) as bpp, 
		count(distinct case when meta ->> 'gateway'='hubfintech' and status_id in (2) then id else null end) as hubfintech_errors, 
		(count(distinct case when meta ->> 'gateway'='hubfintech' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='hubfintech' then id else null end)::float,0) *100) as percentage_hubfintech,
		count(distinct case when meta ->> 'gateway'='bpp' and status_id in (2) then id else null end) as bpp_errors,
		(count(distinct case when meta ->> 'gateway'='bpp' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='bpp' then id else null end)::float,0) *100) as percentage_bpp
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		where meta ->> 'action' = 'disperse'
		AND created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		'''.format(timezone=timezone)

	elif country == 'ar':
		query_ar = '''--no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='edenredrest' then id else null end) as edenred, 
		count(distinct case when meta ->> 'gateway'='novopayment' then id else null end) as novopayment, 
		count(distinct case when meta ->> 'gateway'='edenredrest' and status_id in (2) then id else null end) as edenred_errors, 
		(count(distinct case when meta ->> 'gateway'='edenredrest' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='edenredrest' then id else null end)::float,0) *100) as percentage_edenred,
		count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end) as novopayment_errors,
		(count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='novopayment' then id else null end)::float,0) *100) percentage_novopayment
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		where meta ->> 'action' = 'disperse'
		AND created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		'''.format(timezone=timezone)

	elif country == 'mx':
		query_mx = '''
		--no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='edenred' then t.id else null end) as edenred, 
		count(distinct case when meta ->> 'gateway'='novopayment' then t.id else null end) as novopayment, 
		count(distinct case when meta ->> 'gateway'='rappipay-products' then t.id else null end) as rappipay, 
		count(distinct case when meta ->> 'gateway'='edenred' and t.status_id in (2) then t.id else null end) as edenred_errors, 
		count(distinct case when meta ->> 'gateway'='rappipay-products' and t.status_id in (2) then t.id else null end) as rappipay_errors, 
		(count(distinct case when meta ->> 'gateway'='edenred' and t.status_id in (2) then t.id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='edenred' then t.id else null end)::float,0) *100) as percentage_edenred,
		count(distinct case when meta ->> 'gateway'='novopayment' and t.status_id in (2) then t.id else null end) as novopayment_errors,
		(count(distinct case when meta ->> 'gateway'='novopayment' and t.status_id in (2) then t.id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='novopayment' then t.id else null end)::float,0) *100) as percentage_novopayment,
		(count(distinct case when meta ->> 'gateway'='rappipay-products' and t.status_id in (2) then t.id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='rappipay-products' then t.id else null end)::float,0) *100) as percentage_rappipay
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		left join transaction_requests tr on tr.transaction_id=t.id
		where meta ->> 'action' = 'disperse'
		AND t.created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		--and ((tr.response::text not ilike '%429%' and tr.response::text not ilike '%444%') or tr.response is null)
		'''.format(timezone=timezone)
	elif country in ['cl']:
		query_cl = '''
		--no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='edenred' then id else null end) as edenred, 
		count(distinct case when meta ->> 'gateway'='novopayment' then id else null end) as novopayment, 
		count(distinct case when meta ->> 'gateway'='heroes' then id else null end) as heroes, 
		count(distinct case when meta ->> 'gateway'='edenred' and status_id in (2) then id else null end) as edenred_errors, 
		count(distinct case when meta ->> 'gateway'='heroes' and status_id in (2) then id else null end) as heroes_errors, 
		(count(distinct case when meta ->> 'gateway'='edenred' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='edenred' then id else null end)::float,0) *100) as percentage_edenred,
		count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end) as novopayment_errors,
		(count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='novopayment' then id else null end)::float,0) *100) as percentage_novopayment,
		(count(distinct case when meta ->> 'gateway'='heroes' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='heroes' then id else null end)::float,0) *100) as percentage_heroes
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		where meta ->> 'action' = 'disperse'
		AND created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		'''.format(timezone=timezone)

	elif country in ['pe']:
		query = '''--no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='edenred' then t.id else null end) as edenred, 
		count(distinct case when meta ->> 'gateway'='novopayment' then t.id else null end) as novopayment, 
		count(distinct case when meta ->> 'gateway'='edenred' and t.status_id in (2) then t.id else null end) as edenred_errors, 
		(count(distinct case when meta ->> 'gateway'='edenred' and t.status_id in (2) then t.id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='edenred' then t.id else null end)::float,0) *100) as percentage_edenred,
		count(distinct case when meta ->> 'gateway'='novopayment' and t.status_id in (2) then t.id else null end) as novopayment_errors,
		(count(distinct case when meta ->> 'gateway'='novopayment' and t.status_id in (2) then t.id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='novopayment' then t.id else null end)::float,0) *100) as percentage_novopayment
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		left join transaction_requests tr on tr.transaction_id=t.id
		where meta ->> 'action' = 'disperse'
		AND t.created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		and ((tr.response::text not ilike '%404%') or tr.response is null)
		'''.format(timezone=timezone)

	else:
		query = '''--no_cache
		with a as (
select max(id) as id_maximo from transactions)
		SELECT 
		count(distinct case when meta ->> 'gateway'='edenred' then id else null end) as edenred, 
		count(distinct case when meta ->> 'gateway'='novopayment' then id else null end) as novopayment, 
		count(distinct case when meta ->> 'gateway'='edenred' and status_id in (2) then id else null end) as edenred_errors, 
		(count(distinct case when meta ->> 'gateway'='edenred' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='edenred' then id else null end)::float,0) *100) as percentage_edenred,
		count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end) as novopayment_errors,
		(count(distinct case when meta ->> 'gateway'='novopayment' and status_id in (2) then id else null end)::float/nullif(count(distinct case when meta ->> 'gateway'='novopayment' then id else null end)::float,0) *100) as percentage_novopayment
		FROM a 
		inner join transactions t on t.id >= (a.id_maximo - 50000)
		where meta ->> 'action' = 'disperse'
		AND created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes'
		'''.format(timezone=timezone)

	query_sheets = '''--no_cache
with a as (
select max(id) as id_maximo from transactions)


, t as (select id, meta 
			   from a
			   inner join transactions t on t.id >= (a.id_maximo - 50000)
			   where created_at >= now() at time zone 'America/{timezone}' - interval '10 minutes' 
			   and status_id not in (1,3) and meta ->> 'action' = 'disperse')
			   
	SELECT t.id as transaction_id, (meta ->> 'gateway')::text as gateway, response, request
	FROM t 
	left join transaction_requests tr on tr.transaction_id=t.id
	'''.format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(305, query)
	elif country == 'ar':
		metrics = redash.run_query(352, query_ar)
	elif country == 'cl':
		metrics = redash.run_query(354, query_cl)
	elif country == 'uy':
		metrics = redash.run_query(353, query)
	elif country == 'mx':
		metrics = redash.run_query(325, query_mx)
	elif country == 'ec':
		metrics = redash.run_query(2233, query)
	elif country == 'cr':
		metrics = redash.run_query(2229, query)
	elif country == 'pe':
		metrics = redash.run_query(704, query)
	elif country == 'br':
		metrics = redash.run_query(16, query_br)

	if country == 'co':
		sheets = redash.run_query(305, query_sheets)
	elif country == 'ar':
		sheets = redash.run_query(352, query_sheets)
	elif country == 'cl':
		sheets = redash.run_query(354, query_sheets)
	elif country == 'uy':
		sheets = redash.run_query(353, query_sheets)
	elif country == 'mx':
		sheets = redash.run_query(325, query_sheets)
	elif country == 'ec':
		sheets = redash.run_query(2233, query_sheets)
	elif country == 'cr':
		sheets = redash.run_query(2229, query_sheets)
	elif country == 'pe':
		sheets = redash.run_query(704, query_sheets)
	elif country == 'br':
		sheets = redash.run_query(16, query_sheets)
	
	return metrics, sheets

def slack_notification(a,sheets):
	channel_alert = timezones.slack_channels('tech_integration')
	current_time = timezones.country_current_time(country)

	print(a)
	a = a.fillna(0)

	if country == 'br':
		a = a[((a['percentage_hubfintech'] >= 10) & (a['hubfintech_errors'] >= 6)) | ((a['percentage_bpp'] >= 10) & (a['bpp_errors'] >= 6))]
	elif country not in ['mx','br','cl']:
		a = a[((a['percentage_novopayment'] >= 10) & (a['novopayment_errors'] >= 6)) | ((a['percentage_edenred'] >= 10) & (a['edenred_errors'] >= 6))]
	elif country in ['mx']:
		a = a[((a['percentage_novopayment'] >= 10) & (a['novopayment_errors'] >= 6)) | ((a['percentage_edenred'] >= 10) & (a['edenred_errors'] >= 6))
		| ((a['percentage_rappipay'] >= 6) & (a['rappipay_errors'] >= 4))]
	elif country in ['cl']:
		a = a[((a['percentage_novopayment'] >= 10) & (a['novopayment_errors'] >= 6)) | ((a['percentage_edenred'] >= 10) & (a['edenred_errors'] >= 6)) 
		| ((a['percentage_heroes'] >= 6) & (a['heroes_errors'] >= 4))]	

	try:
		sheets = sheets[['transaction_id','gateway','response','request']]
		home = expanduser("~")
		results_file = '{}/dispersion_{}.xlsx'.format(home, country)
		sheets.to_excel(results_file)
	except Exception as e:
		print(e)

	to_upload = a
	to_upload['alarm_at'] = current_time
	to_upload['country'] = country

	if country in ['br']:
		to_upload['card_errors'] = to_upload['hubfintech_errors'] + to_upload['bpp_errors']
	elif country in ['mx']:
	    to_upload['card_errors'] = to_upload['rappipay_errors'] + to_upload['novopayment_errors'] + to_upload['edenred_errors']
	elif country in ['cl']:
		to_upload['card_errors'] = to_upload['heroes_errors'] + to_upload['novopayment_errors'] + to_upload['edenred_errors']
	else:
		to_upload['card_errors'] = to_upload['novopayment_errors'] + to_upload['edenred_errors']

	to_upload = to_upload[['country','alarm_at','card_errors']]
	snow.upload_df_occ(to_upload,'dispersion_alarm')

	for index, row in a.iterrows():
		rows = dict(row)
		if country == 'br':
			text = '''*Alarma de Dispersion :alert:*
		Inestabilidad en las dispersiones en los últimos 10 minutos
		Country: {country} :flag-{country}: 
		Pais: {country}
		Hubfintech transactions: {hubfintech}
		*Hubfintech errors:* {hubfintech_errors}
		*Percentage hubfintech errors:* {Percentage_hubfintech}%
		BPP transactions: {bpp}
		*BPP errors:* {bpp_errors}
		*Percentage BPP errors:* {Percentage_bpp}%
			'''.format(country=country,
				hubfintech = round(row['hubfintech'],2),
				hubfintech_errors = round(row['hubfintech_errors'],2),
				Percentage_hubfintech = round(row['percentage_hubfintech'],2),
				bpp = round(row['bpp'],2),
				bpp_errors = round(row['bpp_errors'],2),
				Percentage_bpp = round(row['percentage_bpp'],2)
				)
		elif country in ['mx']:
			text = '''*Alarma de Dispersion :alert:*
		Inestabilidad en las dispersiones en los últimos 10 minutos
		Country: {country} :flag-{country}: 
		Pais: {country}
		Rappipay transactions: {rappipay}
		*Rappipay errors:* {rappipay_errors}
		*Percentage Rappipay errors:* {Percentage_rappipay}%
		Novopayment transactions: {novopayment}
		Novopayment errors: {novopayment_errors}
		Percentage_novopayment: {Percentage_novopayment}%
		Edenred transactions: {edenred}
		Edenred errors: {edenred_errors}
		Percentage endenred: {Percentage_edenred}%
			'''.format(country=country,
				rappipay = round(row['rappipay'],2),
				rappipay_errors = round(row['rappipay_errors'],2),
				Percentage_rappipay = round(row['percentage_rappipay'],2),
				novopayment = round(row['novopayment'],2),
				novopayment_errors = round(row['novopayment_errors'],2),
				Percentage_novopayment = round(row['percentage_novopayment'],2),
				edenred = round(row['edenred'],2),
				edenred_errors = round(row['edenred_errors'],2),
				Percentage_edenred = round(row['percentage_edenred'],2)
				)
		elif country in ['cl']:
			text = '''*Alarma de Dispersion :alert:*
		Inestabilidad en las dispersiones en los últimos 10 minutos
		Country: {country} :flag-{country}: 
		Pais: {country}
		Heroes transactions: {heroes}
		*Heroes errors:* {heroes_errors}
		*Percentage heroes errors:* {Percentage_heroes}%
		Novopayment transactions: {novopayment}
		Novopayment errors: {novopayment_errors}
		Percentage_novopayment: {Percentage_novopayment}%
		Edenred transactions: {edenred}
		Edenred errors: {edenred_errors}
		Percentage endenred: {Percentage_edenred}%
			'''.format(country=country,
				heroes = round(row['heroes'],2),
				heroes_errors = round(row['heroes_errors'],2),
				Percentage_heroes = round(row['percentage_heroes'],2),
				novopayment = round(row['novopayment'],2),
				novopayment_errors = round(row['novopayment_errors'],2),
				Percentage_novopayment = round(row['percentage_novopayment'],2),
				edenred = round(row['edenred'],2),
				edenred_errors = round(row['edenred_errors'],2),
				Percentage_edenred = round(row['percentage_edenred'],2)
				)
		else:
			text = '''*Alarma de Dispersion :alert:*
		Inestabilidad en las dispersiones en los últimos 10 minutos
		Country: {country} :flag-{country}: 
		Pais: {country}
		Novopayment transactions: {novopayment}
		Novopayment errors: {novopayment_errors}
		Percentage_novopayment: {Percentage_novopayment}%
		Edenred transactions: {edenred}
		Edenred errors: {edenred_errors}
		Percentage endenred: {Percentage_edenred}%
			'''.format(country=country,
				novopayment = round(row['novopayment'],2),
				novopayment_errors = round(row['novopayment_errors'],2),
				Percentage_novopayment = round(row['percentage_novopayment'],2),
				edenred = round(row['edenred'],2),
				edenred_errors = round(row['edenred_errors'],2),
				Percentage_edenred = round(row['percentage_edenred'],2)
				)

		if not sheets.empty:
			print(text)
			slack.file_upload_channel(channel_alert, text, results_file, 'xlsx')
			last_countries = get_last_alarms()
			print(last_countries)
			if (country in ['br'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['cl'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['co'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['mx'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['pe'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['uy'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['ec'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['cr'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			elif (country in ['ar'] and country not in last_countries):
				pocs = point_of_contact(country.upper())
				slack.file_upload_channel('CS7UQAMLG', text+pocs, results_file, 'xlsx')
			else:
				print("out of countries")
		else:
			print(text)
			slack.bot_slack(text +"\n No se pudo obtener información sobre el error",channel_alert)

		if os.path.exists(results_file):
			os.remove(results_file)

def run_alarm(country):
	a, sheets = country_query(country)
	try:
		slack_notification(a, sheets)
	except Exception as e:
		print(e)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']

for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)

