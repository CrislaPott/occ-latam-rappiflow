import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from slack import WebClient
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def reported_stores(country):
	timezone, interval = timezones.country_timezones(country)
	query = '''
	select physical_store_id::text as physical_store_id, 'reported' as flag
	from ops_occ.limbo_pickers
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and alarm_at::timestamp_ntz >= dateadd(hour,-2,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
	and country='{country}'
	'''.format(country=country, timezone=timezone)
	df = snow.run_query(query)

	return df
	
def limbo(country):
	timezone, interval = timezones.country_timezones(country)

	delay = f"""
	with last as (
select max(created_at) at time zone 'america/{timezone}' as created_at from order_events)
select EXTRACT(EPOCH FROM (now() at time zone 'America/{timezone}' - base.created_at at time zone 'America/{timezone}' )) / 60 as diff
from last base
where EXTRACT(EPOCH FROM (now() at time zone 'America/{timezone}' - base.created_at at time zone 'America/{timezone}' )) / 60 >= 10
	"""

	query = """
	 --no_cache  
	 with base as (SELECT
	 ORDER_ID, NAME, CREATED_AT, rank() OVER (PARTITION BY ORDER_ID ORDER BY ID DESC)
	 FROM ORDER_EVENTS
	 WHERE created_at at time zone 'America/{timezone}' >= now() at time zone 'America/{timezone}' - interval  '2 hours'),
	 a as (
	 select distinct base.order_id::text, base.name, os.physical_store_id, base.created_at at time zone 'America/{timezone}',
	 EXTRACT(EPOCH FROM (now() at time zone 'America/{timezone}' - base.created_at at time zone 'America/{timezone}' )) / 60 as dif,
	 count(base.order_id) OVER (PARTITION BY PHYSICAL_STORE_ID) as delayed_orders
	 from base
	 join order_summary os on os.order_id = base.order_id
	 where rank = 1 and base.name = 'order_assigned'
	 and EXTRACT(EPOCH FROM (now() at time zone 'America/{timezone}' - base.created_at at time zone 'America/{timezone}' )) / 60 >= 30
	 and os.store_type_store not in ('rappi_pay','soat_webview')),
	 b as (
	 select count(distinct order_id) as live, physical_store_id
	 from order_summary 
	 where created_at::date at time zone 'America/{timezone}' = now()::date at time zone 'America/{timezone}'
	 and state not in ('canceled', 'finished', 'scheduled') and physical_store_id in (select distinct physical_store_id from a)
	 group by 2) 
	 select b.physical_store_id::text as physical_store_id, b.live as total_orders, a.delayed_orders as shopper_problens, a.delayed_orders::float / b.live::float as percentage,
	 string_agg(a.order_id::text, ',') as order_ids
	 from a 
	 join b on b.physical_store_id = a.physical_store_id
	 where a.delayed_orders >= 3
	 group by 1,2,3,4
	 """.format(country=country,timezone=timezone, interval=interval)

	if country == 'co':
		metrics = redash.run_query(2453, query)
		delayed = redash.run_query(2453, delay)
	elif country == 'ar':
		metrics = redash.run_query(2427, query)
		delayed = redash.run_query(2427, delay)
	elif country == 'cl':
		metrics = redash.run_query(3685, query)
		delayed = redash.run_query(3685, delay)
	elif country == 'mx':
		metrics = redash.run_query(2454, query)
		delayed = redash.run_query(2454, delay)
	elif country == 'uy':
		metrics = redash.run_query(2429, query)
		delayed = redash.run_query(2429, delay)
	elif country == 'ec':
		metrics = redash.run_query(2555, query)
		delayed = redash.run_query(2555, delay)
	elif country == 'cr':
		metrics = redash.run_query(2551, query)
		delayed = redash.run_query(2551, delay)
	elif country == 'pe':
		metrics = redash.run_query(2452, query)
		delayed = redash.run_query(2452, delay)
	elif country == 'br':
		metrics = redash.run_query(2451, query)
		delayed = redash.run_query(2451, delay)

	return metrics, delayed

def alarm(country):
	channel_alert = timezones.slack_channels('trend_alarms')

	df, delayed = limbo(country)
	rp = reported_stores(country)
		
	if df.shape[0] == 0:
		print("nada")

	df = pd.merge(df, rp, how='left', on=['physical_store_id'])
	df = df[(df['flag'].isnull())]
	print(df.columns)
	if delayed.shape[0] == 0:
		print('no delay')

		current_time = timezones.country_current_time(country)
		to_upload = df
		to_upload['alarm_at'] = current_time
		to_upload['country'] = country
		to_upload = to_upload[['physical_store_id','total_orders','shopper_problens','order_ids','alarm_at','country']]
		snow.upload_df_occ(to_upload,'limbo_pickers')

		if not df.empty:
			text = '''
			*Alarma de Limbo (Picker Stores - Tienda sin actualización en las ordenes live) - :flag-{country}:*
			Pais: {country}
			Nuevas tiendas con ordenes con problemas para asignar el shopper/Picker alcanzaron el threshold. '''.format(country=country)
			print(text)
			home = expanduser("~")
			results_file = '{}/details_limbo_pickers_live_{country}.xlsx'.format(home, country=country)
			df_final = df[['physical_store_id', 'total_orders', 'shopper_problens', 'percentage','order_ids']]
			df_final.to_excel(results_file, sheet_name='orders', index=False)
			slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
		else:
			print('df empty')

countries = ['br','co','pe','ec','cr','mx','cl','uy','ar']
for country in countries:
	print(country)
	try:
		alarm(country)
	except Exception as e:
		print(e)

