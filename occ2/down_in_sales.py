import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack, avenue, gmail
from bot_telegram import send_telegram
from update_telegram import update_telegram
from lib import snowflake as snow
from os.path import expanduser
from openpyxl import load_workbook
from functions import timezones
from email.mime.base import MIMEBase
from email import encoders

def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select groupbrandid::text as identification_id, last_growth
    from
    (select groupbrandid, last_value(growth) over (partition by groupbrandid order by alarm_at asc) as last_growth
    from ops_occ.down_in_sales
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    )
    group by 1,2
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def pocs(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select id::text as groupbrandid, 
       concat('+',kam_phone_number) kam_phone_number,
       concat('+',leader_phone_number) leader_phone_number,
       concat('+',global_team_number) global_team_number,
       ltrim(kam) as kam_email
    from ops_occ.group_brand_pocs pocs
    where lower(country) = '{country}'
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    print("pocs")
    return df

def store_history(country):
    timezone, interval = timezones.country_timezones(country)
    query = """
    --no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp())))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (convert_timezone('America/{timezone}',current_timestamp()) - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(convert_timezone('America/{timezone}',current_timestamp()), 'HH24:MI') THEN 'D14'
          END AS calendar_day

   FROM {country}_CORE_ORDERS_PUBLIC.orders_vw b
   WHERE (created_at >= date(convert_timezone('America/{timezone}',current_timestamp())) - interval '14 days')
     and created_at < date(convert_timezone('America/{timezone}',current_timestamp()))
   AND to_char(created_at, 'HH24:MI') >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '1h', 'HH24:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_hour AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          opd.name as product_name, 
          calendar_day

   FROM base b
   INNER JOIN {country}_CORE_ORDERS_PUBLIC.order_stores_vw os ON os.order_id = b.order_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=b.order_id and os.store_id=op.store_id
   left join {country}_CORE_ORDERS_PUBLIC.order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','restaurant','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )
   
SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name

   FROM day_hour
 
   group by 1,2,3,4,5,6,7
    """.format(country=country, timezone=timezone)

    df = snow.run_query(query)
    return df

def country_query(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
--no_cache
 WITH base AS
  (SELECT id AS order_id,
          created_at,
          payment_method, 
          (case when coupon_code is null then 'null' else coupon_code end)::text as coupon_code,
          application_user_id,
          CASE
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D0'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '7day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D7'
              WHEN b.created_at::date = date_trunc('day', (now() AT TIME ZONE 'America/{timezone}' - interval '14day'))::date
                   AND to_char(b.created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
                   AND to_char(b.created_at, 'HH24:MI') <= to_char(now() AT TIME ZONE 'America/{timezone}', 'HH24:MI') THEN 'D14'
          END AS calendar_day
          
   FROM orders b
   WHERE (created_at >= date(now() AT TIME ZONE 'America/{timezone}'))
   AND to_char(created_at, 'HH24:MI') >= to_char((now() AT TIME ZONE 'America/{timezone}') - interval '1h', 'HH24:MI')
   and state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')),
      day_hour AS
  (SELECT os.store_id,
          os.name,
          b.order_id,
          os.type,
          b.created_at,
          b.coupon_code::text as coupon_code,
          payment_method,
          b.application_user_id,
          op.product_id,
          opd.name as product_name, 
          calendar_day

   FROM base b
   INNER JOIN order_stores os ON os.order_id = b.order_id
   AND (contact_email NOT LIKE '%@synth.rappi.com'
        OR contact_email IS NULL)
   left join order_product op on op.order_id=b.order_id and os.store_id=op.store_id
   left join order_product_detail opd on opd.order_product_id=op.id and op.product_id=opd.product_id
   where calendar_day in ('D0','D7','D14')
   and os.type NOT IN ('courier_hours',
                         'courier_sampling','queue_order_flow','soat_webview')
                         and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
     AND os.type not ilike '%rappi%pay%'
     and os.name not ilike '%Dummy%Store%Rappi%Connect%'
     AND payment_method NOT IN ('synthetic')
     )
   
SELECT store_id::text as store_id, coupon_code, payment_method, application_user_id, calendar_day, product_id, order_id, max(product_name) as product_name, max(name) as store_name

   FROM day_hour
 
   group by 1,2,3,4,5,6,7
   """.format(country=country,timezone=timezone)

    delay = f'''
    with a as (select max(created_at) as b from orders)
    select
    extract(epoch from (now() at time zone 'America/{timezone}') - b) / 60 as time_in_minutes
    from a
    where (extract(epoch from (now() at time zone 'America/{timezone}') - b) / 60)::float >= 5
    '''

    if country == 'co':
        metrics = redash.run_query(1904, query)
        delayed = redash.run_query(1904, delay)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
        delayed = redash.run_query(1337, delay)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
        delayed = redash.run_query(1155, delay)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
        delayed = redash.run_query(1371, delay)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
        delayed = redash.run_query(1156, delay)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
        delayed = redash.run_query(1922, delay)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
        delayed = redash.run_query(1921, delay)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
        delayed = redash.run_query(1157, delay)
    elif country == 'br':
        metrics = redash.run_query(1338, query)
        delayed = redash.run_query(1338, delay)

    return metrics, delayed

def no_enabled_stores(country):
    query = f'''
    select store_id::text as store_id, case when is_enabled=true then 'true' else 'false' end as is_enabled from stores
    where deleted_at is null
    '''
    if country == 'co':
        metrics = redash.run_query(6312, query)
    elif country == 'ar':
        metrics = redash.run_query(6309, query)
    elif country == 'cl':
        metrics = redash.run_query(6311, query)
    elif country == 'mx':
        metrics = redash.run_query(6324, query)
    elif country == 'uy':
        metrics = redash.run_query(6322, query)
    elif country == 'ec':
        metrics = redash.run_query(6447, query)
    elif country == 'cr':
        metrics = redash.run_query(6313, query)
    elif country == 'pe':
        metrics = redash.run_query(6323, query)
    elif country == 'br':
        metrics = redash.run_query(6310, query)

    return metrics

def store_infos(country):
    query= """
    with base as (select country, coalesce(brand_group,brand_name) as brand_group, coalesce(brand_group_id, brand_id) as brand_group_id,
                     vertical_group,
       sum(gmv) as gmv
    from GLOBAL_FINANCES.global_orders
    where created_at::date >= dateadd(day,-30,current_date())::date
    and vertical_group in ('CPGS','ECOMMERCE','RESTAURANTS')
    and lower(country) = '{country}'
    group by 1,2,3,4
    )
, filter as (
    select base.*, round(gmv::float,2)::float, round((PERCENT_RANK() over (partition by country, vertical_group order by gmv)*100)::float,2) as percentile
    from base
)
, final as (
select * from filter where percentile >= 70)

    --no_cache
    select a.store_id::text as storeid, 
    coalesce(bg.brand_group_id,b.id)::text as store_brand_id, 
    coalesce(bg.brand_group_name,b.name) as brand_name, 
    case when v.vertical in ('Express','Licores','Farmacia','Mercados') then 'CPGs'
         when v.vertical in ('Restaurantes') then 'Restaurantes'
         when v.vertical in ('Ecommerce') then 'Ecommerce' else v.vertical end as vertical
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw b on b.id=a.store_brand_id
    inner join (
    select store_type as storetype,
    case
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
    when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype=a.type
    
    left join (
       select * from
(select store_brand_id::text as store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
       ) bg on bg.store_brand_id = a.store_brand_id

    join final on final.brand_group_id::text=coalesce(bg.brand_group_id,b.id)::text

    where a.deleted_at is null
    and not coalesce (a._fivetran_deleted, false)
    and vertical in ('Now','Mercados','Ecommerce','CPGs','Restaurantes')
    group by 1,2,3,4""".format(country=country)

    df = snow.run_query(query)
    return df

def alarm(a,b):
    df0 = pd.merge(a,b,how='inner',left_on=a['store_id'],right_on=b['storeid'])
    df0 = df0.drop(['key_0'], axis=1)

    df1_d0 = df0[((df0['calendar_day'] == 'D0'))]
    df_for_ids = df1_d0.drop_duplicates(subset=['store_id','store_brand_id','vertical','order_id'])
    order_ids = df_for_ids.groupby(["store_id"])["order_id"].apply(list).reset_index(name='order_ids_d0')
    df1_d0 = df1_d0.drop_duplicates(subset=["store_brand_id","brand_name","vertical","order_id"])
    df1_d0 = df1_d0.groupby(["store_brand_id","brand_name","vertical"])['order_id'].count().reset_index(name="orders_d0")

    df1_d7 = df0[((df0['calendar_day'] == 'D7'))]
    df1_d7 = df1_d7.drop_duplicates(subset=["store_brand_id","brand_name","vertical","order_id"])
    df1_d7 = df1_d7.groupby(["store_brand_id","brand_name","vertical"])['order_id'].count().reset_index(name="orders_d7")

    df1_d14 = df0[((df0['calendar_day'] == 'D14'))]
    df1_d14 = df1_d14.drop_duplicates(subset=["store_brand_id","brand_name","vertical","order_id"])
    df1_d14 = df1_d14.groupby(["store_brand_id","brand_name","vertical"])['order_id'].count().reset_index(name="orders_d14")


    final_df = pd.concat([df1_d7,df1_d14,df1_d0], axis=0, ignore_index=True)

    final_df['orders_d0'].fillna(0, inplace = True)
    final_df['orders_d7'].fillna(0, inplace = True)
    final_df['orders_d14'].fillna(0, inplace = True)

    df1 = final_df
    df1 = df1.groupby(["store_brand_id","brand_name","vertical"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
    df1['average_last_weeks'] = ((df1['orders_d7'] + df1['orders_d14']) / 2)
    df1['growth'] = round((((df1['orders_d0'] - df1['average_last_weeks']) / df1['average_last_weeks']) *100),2)
    df1['growth'] = df1['growth'].replace(np.inf, np.nan)
    df1 = df1[
    ((df1['growth'] <= -30) & (df1['average_last_weeks'] >= 35) & (df1['average_last_weeks'] - df1['orders_d0']>= 30))
    | ((df1['average_last_weeks'] >= 35) & (df1['orders_d0'] == 0))
    | ((df1['vertical'].isin(['Ecommerce'])) & (df1['average_last_weeks'] >= 15) & (df1['orders_d0'] == 0))
    | ((df1['vertical'].isin(['Ecommerce'])) & (df1['growth'] <= -40) & (df1['average_last_weeks'] >= 15) & (df1['average_last_weeks'] - df1['orders_d0']>= 15))
    ]
    if not df1.empty:
        c = excluding_stores(country)
        not_df = c
        not_df['last_growth'] = not_df['last_growth'] -15 # more than 15% of variation

        df1 = pd.merge(df1,not_df,how='left',left_on=df1['store_brand_id'],right_on=not_df['identification_id'])
        df1 = df1[(df1['identification_id'].isnull()) | (df1['growth'] < df1['last_growth'])]
        df1 = df1.drop(['key_0','identification_id','last_growth'], axis=1)

        df_pocs = pocs(country)

        df1 = pd.merge(df1,df_pocs,how='inner',left_on=df1['store_brand_id'],right_on=df_pocs['groupbrandid'])

        df1 = df1[~df1['vertical'].isin(['Restaurantes'])]
        df1 = df1.drop(['key_0'], axis=1)

        current_time = timezones.country_current_time(country)

        to_upload = df1.rename(columns={"store_brand_id": "groupbrandid"})
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        to_upload = to_upload[['groupbrandid','vertical','growth','alarm_at','country','orders_d0','orders_d7','orders_d14','brand_name']]
        snow.upload_df_occ(to_upload,'down_in_sales')

        for index, row in df1.iterrows():
          rows = dict(row)
          df_row = df0[((df0['store_brand_id'] == row['store_brand_id']) & (df0['vertical'] == row['vertical']))]
          df_row_0 = df_row[((df_row['calendar_day'] == 'D0'))]
          df_row_0 = df_row_0.fillna(0)
          df_row_0 = df_row_0.drop_duplicates(subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"]).reset_index()
          df_row_0 = df_row_0.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d0")

          df_row_7 = df_row[((df_row['calendar_day'] == 'D7'))]
          df_row_7 = df_row_7.fillna(0)
          df_row_7 = df_row_7.drop_duplicates(subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"])
          df_row_7 = df_row_7.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d7")

          df_row_14 = df_row[((df_row['calendar_day'] == 'D14'))]
          df_row_14 = df_row_14.fillna(0)
          df_row_14 = df_row_14.drop_duplicates(subset=["store_id","store_name","coupon_code","payment_method","application_user_id","order_id"])
          df_row_14 = df_row_14.groupby(["store_id","store_name","coupon_code","payment_method","application_user_id"])['order_id'].count().reset_index(name="orders_d14")

          df_row_final = pd.concat([df_row_7,df_row_14,df_row_0], axis=0, ignore_index=True)


          df_row_final['orders_d0'].fillna(0, inplace = True)
          df_row_final['orders_d7'].fillna(0, inplace = True)
          df_row_final['orders_d14'].fillna(0, inplace = True)

          df2 = df_row_final.groupby(["store_id","store_name"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
          soma = sum(df2['orders_d0'])
          df2['share_d0'] = round(((df2['orders_d0'] / soma)*100),2)
          df2['average_last_weeks'] = ((df2['orders_d7'] + df2['orders_d14']) / 2)
          df2['growth'] = round((((df2['orders_d0'] - df2['average_last_weeks']) / df2['average_last_weeks']) *100),2)
          df2 = pd.merge(df2, order_ids, on=['store_id'], how='left')
          df2 = df2[['store_id',"store_name",'orders_d0','share_d0','orders_d7','orders_d14','average_last_weeks','growth','order_ids_d0']]
          non_enabled_stores = no_enabled_stores(country)
          df2 = pd.merge(df2,non_enabled_stores, on=['store_id'], how='left')
          df2 = df2.sort_values(by='average_last_weeks', ascending=False)
          email_stores = df2[['store_id',"store_name",'orders_d0','average_last_weeks','growth','is_enabled']]
          email_stores['orders_d0'] = email_stores['orders_d0'].astype(int)
          email_stores = email_stores.rename(columns={"orders_d0": "Pedidos Hoy", "average_last_weeks": "Promedio L2W", "growth": "Variacion"})
          home = expanduser("~")
          complete_report = '{}/relatorio.xlsx'.format(home)
          email_stores.to_excel(complete_report,sheet_name='stores', index=False)
          book2 = load_workbook(complete_report)
          writer2 = pd.ExcelWriter(complete_report, engine='openpyxl')
          writer2.book = book2
          writer2.sheets = dict((ws.title, ws) for ws in book2.worksheets)
          email_stores = email_stores.head(10)
          home = expanduser("~")
          results_file = '{}/details.xlsx'.format(home)

          df2.to_excel(results_file,sheet_name='stores', index=False)
          book = load_workbook(results_file)
          writer = pd.ExcelWriter(results_file, engine='openpyxl')
          writer.book = book
          writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

          df3 = df_row_final.groupby(["coupon_code"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
          soma = sum(df3['orders_d0'])
          df3['share_d0'] = round(((df3['orders_d0'] / soma)*100),2)
          df3['average_last_weeks'] = ((df3['orders_d7'] + df3['orders_d14']) / 2)
          df3['growth'] = round((((df3['orders_d0'] - df3['average_last_weeks']) / df3['average_last_weeks']) *100),2)
          df3 = df3[['coupon_code',"orders_d0","orders_d7","orders_d14","share_d0","average_last_weeks","growth"]]
          df3 = df3.sort_values(by='orders_d7', ascending=False)
          df3.to_excel(writer, "coupon_code", index=False)

          df4 = df_row_final.groupby(["payment_method"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
          soma = sum(df4['orders_d0'])
          df4['share_d0'] = round(((df4['orders_d0'] / soma)*100),2)
          df4['average_last_weeks'] = ((df4['orders_d7'] + df4['orders_d14']) / 2)
          df4['growth'] = round((((df4['orders_d0'] - df4['average_last_weeks']) / df4['average_last_weeks']) *100),2)
          df4 = df4[['payment_method',"orders_d0","orders_d7","orders_d14","share_d0","average_last_weeks","growth"]]
          df4 = df4.sort_values(by='orders_d7', ascending=False)
          df4.to_excel(writer, "payment_method", index=False)

          df5 = df_row_final.groupby(["application_user_id"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
          soma = sum(df4['orders_d0'])
          df5['share_d0'] = round(((df5['orders_d0'] / soma)*100),2)
          df5['average_last_weeks'] = ((df5['orders_d7'] + df5['orders_d14']) / 2)
          df5['growth'] = round((((df5['orders_d0'] - df5['average_last_weeks']) / df4['average_last_weeks']) *100),2)
          df5 = df5[['application_user_id',"orders_d0","orders_d7","orders_d14","share_d0","average_last_weeks","growth"]]
          df5 = df5.sort_values(by='orders_d7', ascending=False)
          df5.to_excel(writer, "application_user_id", index=False)

          try:
            prod0 = df_row
            prod0 = prod0[((prod0['calendar_day'] == 'D0'))]
            prod0 = prod0.drop_duplicates(subset=['product_id','product_name','order_id'])
            prod0 = prod0.groupby(["product_id","product_name"])['order_id'].count().reset_index(name="orders_d0")

            prod7 = df_row
            prod7 = prod7[((prod7['calendar_day'] == 'D7'))]
            prod7 = prod7.drop_duplicates(subset=['product_id','product_name','order_id'])
            prod7 = prod7.groupby(["product_id","product_name"])['order_id'].count().reset_index(name="orders_d7")

            prod14 = df_row
            prod14 = prod14[((prod14['calendar_day'] == 'D14'))]
            prod14 = prod14.drop_duplicates(subset=['product_id','product_name','order_id'])
            prod14 = prod14.groupby(["product_id","product_name"])['order_id'].count().reset_index(name="orders_d14")

            prod_final = pd.concat([prod0,prod7,prod14], axis=0, ignore_index=True)

            prod_final['orders_d0'].fillna(0, inplace = True)
            prod_final['orders_d7'].fillna(0, inplace = True)
            prod_final['orders_d14'].fillna(0, inplace = True)

            prod_final = prod_final.groupby(["product_id","product_name"])[["orders_d0","orders_d7","orders_d14"]].sum().reset_index()
            soma = sum(prod_final['orders_d0'])
            prod_final['share_d0'] = round(((prod_final['orders_d0'] / soma)*100),2)
            prod_final['average_last_weeks'] = ((prod_final['orders_d7'] + prod_final['orders_d14']) / 2)
            prod_final['growth'] = round((((prod_final['orders_d0'] - prod_final['average_last_weeks']) / prod_final['average_last_weeks']) *100),2)
            prod_final = prod_final[['product_id',"product_name","orders_d0","orders_d7","orders_d14","share_d0","average_last_weeks","growth"]]
            email_prod = prod_final[['product_id','product_name','orders_d0','average_last_weeks','growth']]
            email_prod['orders_d0'] = email_prod['orders_d0'].astype(int)
            email_prod['product_id'] = email_prod['product_id'].astype(int)
            email_prod = email_prod.sort_values(by='average_last_weeks', ascending=False)
            email_prod = email_prod.rename(columns={"orders_d0": "Pedidos Hoy", "average_last_weeks": "Promedio L2W", "growth": "Variacion"})
            email_prod.to_excel(writer2, "products", index=False)
            email_prod = email_prod.head(20)
            print(email_prod)
            prod_final = prod_final.sort_values(by='orders_d0', ascending=False)
            prod_final.to_excel(writer, "product_id", index=False)

          except:
            print("deu merda product_id")

          writer.save()
          writer2.save()

          text = '''*:warning: Down Selling :arrow_down:*
          :flag-{country}:  
           Pais: {country}
          *Brand Group* {brand_name}
          *Brand Group ID:* {store_brand_id}
          *Vertical:* {vertical}
          *Threshold: *{d0}* ventas hoy VS un promedio de *{average_last_weeks}* misma hora/día semanas pasadas
          *Porcentaje de variacion:* {growth}%
          Adjunto más detalles por tiendas, cupones, métodos de pagos y top products'''.format(
            store_brand_id=row['store_brand_id'],
            brand_name=row['brand_name'],
            vertical=row['vertical'],
            d0 = row['orders_d0'],
            d7 = row['orders_d7'],
            average_last_weeks=row['average_last_weeks'],
            growth = round(row['growth'],2),
            country = country)

          print(text)
          slack.file_upload_channel('C029MJCAMS6',text,results_file,'xlsx')

          ## email
          d0 = int(row['orders_d0']),
          d7 = row['orders_d7'],
          d14 = row['orders_d14'],
          global_team = row['global_team_number'],
          kam = row['kam_phone_number'],
          leader = row['leader_phone_number'],
          vertical=row['vertical'],
          brand = row['brand_name'],
          average = int(row['average_last_weeks']),
          growth = int(row['growth']),
          brand_name = row['brand_name'],
          email = row['kam_email']
          print(email_stores)
          part = MIMEBase('application', "octet-stream")
          part.set_payload(open(complete_report, "rb").read())
          encoders.encode_base64(part)
          part.add_header('Content-Disposition', f'attachment; filename={complete_report}')
          gmail.send_email(email_stores,email_prod, growth, d0, average, email,brand_name, part,vertical)

          try:
            avenue.drop_salles(brand, d0, d7, d14, average, growth, kam, leader, global_team)
          except:
            "fail"

        #   if os.path.exists(results_file):
        #   	os.remove(results_file)
        #   if os.path.exists(complete_report):
        #   	os.remove(complete_report)
    else:
        print('df1 null')
    return email_stores, email_prod, growth, d0, average, email, brand_name, complete_report, vertical

import telegram
def error_bot():
    telegram_token = '1903200994:AAH_HycPXkAZOQCzGHtEQuoP7g-DJUGrLNw'
    chat_id = 1530031754
    bot = telegram.Bot(token=telegram_token)
    bot.send_message(chat_id=chat_id, text = 'guarda, algo está fallando... :(')


from os.path import expanduser
home = expanduser("~")
print(home)

countries = ['ar','cl','pe','br','uy','mx','ec','cr','co']
countries_complete = ['\U0001F1E6\U0001F1F7','\U0001F1E8\U0001F1F1','\U0001F1F5\U0001F1EA','\U0001F1E7\U0001F1F7','\U0001F1FA\U0001F1FE','\U0001F1F2\U0001F1FD','\U0001F1EA\U0001F1E8','\U0001F1E8\U0001F1F7','\U0001F1E8\U0001F1F4']

for index, country in enumerate(countries):
    try:
        update_telegram()
    except Exception as e:
        print(e)
        print('error updating')
        error_bot()

    print(countries_complete[index])
    try:

        a,e = country_query(country)
        b = store_infos(country)
        d = store_history(country)
        a = pd.concat([a, d])
        print('Shape of e is: ', e.shape[0])
        if e.shape[0] == 0:
            print("no delay")
            email_stores, email_prod, growth, d0, average, email, brand_name, complete_report, vertical = alarm(a,b)
            try:
                print('Bot telegram ')
                send_telegram(email_stores, email_prod, growth, d0, average, email, brand_name, complete_report, vertical, countries_complete[index])
            except:
                print('Error in Bot')
                error_bot()
        else:
            print('No bot also')
            print("delay:")
            print(d)
    except Exception as e:
        print(e)
        print('Not country')