import pandas as pd
import numpy as np
from lib import redash, slack
from os.path import expanduser
from lib import snowflake as snow
from functions import timezones, snippets

def get_data(country):
    timezone, interval = timezones.country_timezones(country)
    if country in ['br','ar','uy']:
    	interval = '6h'
    elif country in ['mx']:
        interval = '0h'
    elif country in ['co','pe','ec']:
    	interval = '8h'
    elif country in ['cl']:
    	interval = '7h'
    elif country in ['cr']:
    	interval = '9h'

    query_base_orders = f'''
    --no_cache
    select om.order_id, os.store_id::text as store_id
    from order_modifications om
    join order_stores os on os.order_id=om.order_id
    where om.created_at >= now() AT TIME ZONE 'America/{timezone}' - interval '3h'
    group by 1,2'''

    query_chat = f"""
    --no_cache
    with a as (
    select max(id) as id_maximo from chats)
    select c.order_id as orderid, max(c.data) as chat_evidence
    from a
    join chats c on c.id >= (a.id_maximo - 100000)
    where 
    created_at - interval '{interval}' >= now() at time zone 'america/{timezone}' - interval '3h'
    and (data ilike '%qr%code%' or data ilike '%qrcode%' or data ilike '%el pago%' or data ilike '%pagar código%' or data ilike '%pagar codigo%'
    or data ilike '%pagar%codigo%' or data ilike '%no%me%deja%pagar%' 
    or data ilike '%rechasa%codigo%' 
    or data ilike '%rechasa%código%'
    or data ilike '% QR %'
    or data ilike '%c%digo%de%barras%'
    or data ilike '%cancel%el%pago%'
    or data ilike '%tarjeta%'
    ) 
    and data not ilike '%produto%codigo%de%barras%'
    and sender_type = 'storekeeper'
    and receiver_type = 'support'
    group by 1
    """

    query_stores = f"""
    --no_cache
    select distinct physical_store_id::text as physical_storeid
    from app_configs_physical_stores
    where payment_flow ='payless_integration'
    """

    price_difference = f"""
     --no_cache
    select distinct order_id as id_order
    from 
    order_events 
    where name in ('payless_transaction_declined','price_difference_updated')
    and created_at >= now() - interval '6h'
    """

    query_payless = f"""
    --no_cache
with invalidated as (
  select
    distinct order_id
  from
    order_barcodes_history po
  where
    status = 'invalidated'
    and (created_at AT TIME ZONE 'America/{timezone}') >= date(now() at time zone 'America/{timezone}')
)
select
  po.order_id as payless_order,
  case
    when (
      (
        i.order_id is not null
        and po.status in (
          'payment_authorization_request_succeeded',
          'created'
        )
      )
      or po.status in ('paid_by_dispersion')
    ) then po.order_id
    else null
  end as kpi
from
  orders po
  left join invalidated i on i.order_id = po.order_id
where
  (created_at AT TIME ZONE 'America/{timezone}') >= (
    now() at time zone 'America/{timezone}' - interval '3h'
  )
  
  and (case
    when (
      (
        i.order_id is not null
        and po.status in (
          'payment_authorization_request_succeeded',
          'created'
        )
      )
      or po.status in ('paid_by_dispersion')
    ) then po.order_id
    else null
  end) is not null
    """
    
    if country == 'co':
        chat = redash.run_query(652, query_chat)
        orders = redash.run_query(1904, query_base_orders)
        stores = redash.run_query(1971, query_stores)
        price = redash.run_query(2449, price_difference)
        payless = redash.run_query(4167, query_payless)
    elif country == 'ar':
        chat = redash.run_query(647, query_chat)
        orders = redash.run_query(1337, query_base_orders)
        stores = redash.run_query(1968, query_stores)
        price = redash.run_query(2430, price_difference)
        payless = redash.run_query(4165, query_payless)
    elif country == 'cl':
        chat = redash.run_query(649, query_chat)
        orders = redash.run_query(1155, query_base_orders)
        stores = redash.run_query(1970, query_stores)
        price = redash.run_query(2431, price_difference)
        payless = redash.run_query(4573, query_payless)
    elif country == 'mx':
        chat = redash.run_query(651, query_chat)
        orders = redash.run_query(7977, query_base_orders)
        stores = redash.run_query(1972, query_stores)
        price = redash.run_query(2450, price_difference)
        payless = redash.run_query(3191, query_payless)
    elif country == 'uy':
        chat = redash.run_query(650, query_chat)
        orders = redash.run_query(1156, query_base_orders)
        stores = redash.run_query(1974, query_stores)
        price = redash.run_query(2426, price_difference)
        payless = redash.run_query(4571, query_payless)
    elif country == 'ec':
        chat = redash.run_query(2184, query_chat)
        orders = redash.run_query(1922, query_base_orders)
        stores = redash.run_query(2556, query_stores)
        price = redash.run_query(2559, price_difference)
        payless = redash.run_query(4575, query_payless)
    elif country == 'cr':
        chat = redash.run_query(2183, query_chat)
        orders = redash.run_query(1921, query_base_orders)
        stores = redash.run_query(2558, query_stores)
        price = redash.run_query(2552, price_difference)
        payless = redash.run_query(4574, query_payless)
    elif country == 'pe':
        chat = redash.run_query(745, query_chat)
        orders = redash.run_query(1157, query_base_orders)
        stores = redash.run_query(1973, query_stores)
        price = redash.run_query(2448, price_difference)
        payless = redash.run_query(4572, query_payless)
    elif country == 'br':
        chat = redash.run_query(648, query_chat)
        orders = redash.run_query(1338, query_base_orders)
        stores = redash.run_query(1969, query_stores)
        price = redash.run_query(2447, price_difference)
        payless = redash.run_query(4166, query_payless)

    return orders, chat, stores, price, payless

def reported_store(country):
    timezone, interval = timezones.country_timezones(country)
    logs = f"""
    select distinct store_id::int as id_store
    from ops_occ.payless_alarm
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz > dateadd(hour,-4,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and country = '{country}'
    """
    log = snow.run_query(logs)
    return log

def reported_group_brand(country):
    timezone, interval = timezones.country_timezones(country)
    logs_group_brand = f"""
    select distinct brand_group_id::int as brandgroupid
    from ops_occ.payless_alarm_group_brand
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz > dateadd(hour,-4,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and country = '{country}'
    """
    log_group_brand = snow.run_query(logs_group_brand)
    return log_group_brand

def reported_brand(country):
    timezone, interval = timezones.country_timezones(country)
    logs_brand = f"""
    select distinct brand_id::int as brandid
    from ops_occ.payless_alarm_brands
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz > dateadd(hour,-4,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and country = '{country}'
    """
    log_brand = snow.run_query(logs_brand)
    return log_brand

def offenders(base_orders,chats,store_payless,price_diff,payless_error,store_infos):
    channel_alert = timezones.slack_channels('tech_integration')

#chats
    try:
        df = pd.merge(base_orders,chats,how='left',left_on=base_orders['order_id'],right_on=chats['orderid']).drop(['key_0'],axis=1)
        print(df)
        df = df.fillna(0)
    except:
        df = base_orders
        df['orderid'] = 0


#fallback invalid attempts
    try:
        df = pd.merge(df,payless_error,how='left',left_on=df['order_id'],right_on=payless_error['payless_order']).drop(['key_0'],axis=1)
    except:
        df = df
        df['payless_order'] = 0

#select df with chat evidence or payless error

    df['payless_order'] = df['payless_order'].fillna(0)
    df['orderid'] = df['orderid'].fillna(0)
    df = df[(df['payless_order'] != 0) | (df['orderid'] != 0)]
    df['order_id_error'] = np.where(df['payless_order']!= 0, df['payless_order'],df['orderid'])
    df = df.drop(["order_id","orderid","payless_order"],axis=1)


#store_infos: take the physical store id, brand and group brand
    brand_df = pd.merge(df,store_infos,how='inner',left_on=df['store_id'],right_on=store_infos['storeid']).drop(['key_0','storeid'],axis=1)

##only stores with payless method
    brand_df['physical_store_id']=brand_df['physical_store_id'].astype(float)
    store_payless['physical_storeid']=store_payless['physical_storeid'].astype(float)
    brand_df = pd.merge(brand_df,store_payless,how='inner',left_on=brand_df['physical_store_id'],right_on=store_payless['physical_storeid']).drop(['key_0','physical_storeid'],axis=1)

#without price difference
    if price_diff.empty:
        price_diff = pd.DataFrame(columns=['id_order'])
        price_diff['id_order'] = price_diff['id_order'].fillna(0)
        price_diff['id_order']=price_diff['id_order'].astype(float)
        print(price_diff)
    brand_df['order_id_error']=brand_df['order_id_error'].astype(float)
    price_diff['id_order']=price_diff['id_order'].astype(float)
    brand_df = pd.merge(brand_df,price_diff, how="left", left_on=brand_df['order_id_error'], right_on=price_diff['id_order'])
    brand_df = brand_df[(brand_df['id_order'].isnull())]
    brand_df = brand_df.drop(['key_0','id_order'],axis=1)

#building final store id df
    df1 = brand_df.groupby(["store_id","store_name","brand_group_name"])["order_id_error"].count().reset_index(name="total_orders")
    df2 = brand_df.groupby(["store_id"])["order_id_error"].apply(list).reset_index(name='order_ids')
    df1['store_id']=df1['store_id'].astype(float)
    df2['store_id']=df2['store_id'].astype(float)
    final_df = pd.merge(df1,df2[['store_id','order_ids']], how='inner',left_on=df1["store_id"],right_on=df2["store_id"]).drop(['key_0','store_id_y'],axis=1)
    final_df = final_df.rename(columns={'store_id_x': 'store_id'})


#building final brand group df
    df3 = brand_df.groupby(["brand_group_id","brand_group_name"])["order_id_error"].count().reset_index(name="total_orders")
    df4 = brand_df.groupby(["brand_group_id"])["order_id_error"].apply(list).reset_index(name='order_ids')

    df_ = brand_df.groupby(['brand_group_id'])['store_id'].agg(['count', ('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()


    df_['brand_group_id'] = df_['brand_group_id'].astype(float)
    df3['brand_group_id']=df3['brand_group_id'].astype(float)
    df4['brand_group_id']=df4['brand_group_id'].astype(float)
    final_group_brand_df = pd.merge(df3,df4[['brand_group_id','order_ids']], how='inner',left_on=df3['brand_group_id'],right_on=df4['brand_group_id']).drop(['key_0','brand_group_id_y'],axis=1)
    final_group_brand_df = final_group_brand_df.rename(columns={'brand_group_id_x': 'brand_group_id'})

    final_group_brand_df = pd.merge(final_group_brand_df, df_[['brand_group_id', 'store_id']], how='left', left_on=final_group_brand_df['brand_group_id'],right_on=df_['brand_group_id']).drop(['key_0','brand_group_id_y'],axis=1)
    final_group_brand_df = final_group_brand_df.rename(columns={'brand_group_id_x': 'brand_group_id','store_id':'store_ids'})

#building final brand df
    df5 = brand_df.groupby(["brand_id","brand_name"])["order_id_error"].count().reset_index(name="total_orders")
    df6 = brand_df.groupby(["brand_id"])["order_id_error"].apply(list).reset_index(name='order_ids')

    df7 = brand_df.groupby(['brand_id'])['store_id'].agg(['count', ('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()
    df5['brand_id']=df5['brand_id'].astype(float)
    df6['brand_id']=df6['brand_id'].astype(float)
    df7['brand_id'] = df7['brand_id'].astype(float)
    final_brand_df = pd.merge(df5,df6[['brand_id','order_ids']], how='inner',left_on=df5['brand_id'],right_on=df6['brand_id']).drop(['key_0','brand_id_y'],axis=1)
    final_brand_df = final_brand_df.rename(columns={'brand_id_x': 'brand_id'})
    final_brand_df = pd.merge(final_brand_df, df7[['brand_id', 'store_id']], how='left', left_on=final_brand_df['brand_id'],
                              right_on=df7['brand_id']).drop(['key_0','brand_id_y'],axis=1)
    final_brand_df = final_brand_df.rename(columns={'brand_id_x': 'brand_id','store_id':'store_ids'})
    final_brand_df['store_ids']= final_brand_df['store_ids'].astype(str)


    if not final_df.empty:
        log_store_id=reported_store(country)
        final_df1 = pd.merge(final_df, log_store_id, how='left', left_on=final_df['store_id'],right_on=log_store_id['id_store'])
        final_df1 = final_df1[((final_df1['id_store'].isnull()))]
        final_df1 = final_df1.drop(['key_0'], axis=1)
        final_df1 = final_df1[(final_df1['total_orders'] >= 2)]

        if not final_df1.empty:
            for index, row in final_df1.iterrows():
                text = '''
                *Payless Alert* :alert:
                Pais: {country} :flag-{country}:
                Tienda {store_name}
                Store id: {physical_store_id}
                Group Brand {brand_group_name}
                Ordenes com errores: {orders}
                Order IDs: {order_ids}
                '''.format(
                country=country,
                physical_store_id=row['store_id'],
                orders=row['total_orders'],
                store_name=row['store_name'],
                brand_group_name=row['brand_group_name'],
                order_ids=row['order_ids']
                )

                print(text)
                home = expanduser("~")
                results_file = '{}/payless_store_{country}.xlsx'.format(home, country=country)
                final_df1.to_excel(results_file, sheet_name='store', index=False)
                slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

            current_time = timezones.country_current_time(country)
            final_df1['alarm_at'] = current_time
            final_df1['country'] = country

            to_upload = final_df1[['store_id','store_name','brand_group_name','total_orders','alarm_at','country']]
            snow.upload_df_occ(to_upload,'payless_alarm')
    else:
        print('final_df null')

    if not final_group_brand_df.empty:
        log_group_brand = reported_group_brand(country)
        final_group_brand_df1 = pd.merge(final_group_brand_df, log_group_brand, how='left', left_on=final_group_brand_df['brand_group_id'],right_on=log_group_brand['brandgroupid'])
        final_group_brand_df1 = final_group_brand_df1[((final_group_brand_df1['brandgroupid'].isnull()))]
        final_group_brand_df1 = final_group_brand_df1.drop(['key_0'], axis=1)
        final_group_brand_df1 = final_group_brand_df1[(final_group_brand_df1['total_orders'] >= 15)]

        if not final_group_brand_df1.empty:
            for index, row in final_group_brand_df1.iterrows():
                text = '''
                *Payless Alert - Spike en Group Brand* :alert:
                Pais: {country} :flag-{country}:
                Group Brand {brand_group_name}
                Ordenes com errores: {orders}
                Order IDs: {order_ids}
                Store IDs: {store_id}
                '''.format(
                country=country,
                orders=row['total_orders'],
                brand_group_name=row['brand_group_name'],
                order_ids=row['order_ids'],
                store_id=row['store_ids'],
                )
                print(text)
                home = expanduser("~")
                results_file = '{}/payless_groupbrand_{country}.xlsx'.format(home, country=country)
                final_group_brand_df1.to_excel(results_file, sheet_name='group_brand', index=False)
                slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
            current_time = timezones.country_current_time(country)
            final_group_brand_df1['alarm_at'] = current_time
            final_group_brand_df1['country'] = country
            to_upload = final_group_brand_df1[['brand_group_id','brand_group_name','total_orders','alarm_at','country','store_ids']]
            snow.upload_df_occ(to_upload,'payless_alarm_group_brand')
    else:
        print('final_group_brand_df null')

    if not final_brand_df.empty:
        log_brands=reported_brand(country)
        final_brand_df1 = pd.merge(final_brand_df, log_brands, how='left', left_on=final_brand_df['brand_id'], right_on=log_brands['brandid'])
        final_brand_df1 = final_brand_df1[((final_brand_df1['brandid'].isnull()))]
        final_brand_df1 = final_brand_df1.drop(['key_0'], axis=1)
        final_brand_df1 = final_brand_df1[(final_brand_df1['total_orders'] >= 4)]


        if not final_brand_df1.empty:
            for index, row in final_brand_df1.iterrows():
                text = '''
                *Payless Alert - Spike en Brand* :alert:
                Pais: {country} :flag-{country}:
                Brand {brand_group_name}
                Ordenes com errores: {orders}
                Order IDs: {order_ids}
                Store IDs: {store_id}
                '''.format(
                country=country,
                orders=row['total_orders'],
                brand_group_name=row['brand_name'],
                order_ids=row['order_ids'],
                store_id=row['store_ids'],
                )
                print(text)
                home = expanduser("~")
                results_file = '{}/payless_brand_{country}.xlsx'.format(home, country=country)
                final_brand_df1.to_excel(results_file, sheet_name='brands', index=False)
                slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
            current_time = timezones.country_current_time(country)
            final_brand_df1['alarm_at'] = current_time
            final_brand_df1['country'] = country
            to_upload = final_brand_df1[['brand_id','brand_name','total_orders','alarm_at','country','store_ids']]
            snow.upload_df_occ(to_upload,'payless_alarm_brands')
    else:
        print('final_brand_df null')

def run_alarm(country):
    base_orders, chats, store_payless, price_diff, payless_error = get_data(country)
    store_infos = snippets.store_infos(country)

    offenders(base_orders, chats,store_payless,price_diff,payless_error,store_infos)

countries = snippets.country_list()
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)