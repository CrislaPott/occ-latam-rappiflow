import os, sys, json
import pandas as pd
from dotenv import load_dotenv
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones


def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
--no_cache
 select j.store_id,
        j.name,
        j.type,
        j.store_type_group,
        count(distinct case when created_at::date = current_date::date then order_id else null end) as orders_d0,
        count(distinct case when created_at::date = current_date::date - interval '7d' then order_id else null end) as orders_d7,
        listagg(distinct case when created_at::date = current_date::date then order_id::text else null end, ',') as order_ids,
        sum(distinct case when created_at::date = current_date::date then gmv else 0 end) as gmv_lost
 from
 (select o.id as order_id,
 o.state ,
 o.created_at ,
 o.closed_at ,
 os.vertical_sub_group ,
 os.type ,
 os.name,
 os.store_id,
 os.store_type_group,
 go.GMV_USD AS gmv
from {country}_CORE_ORDERS_PUBLIC.orders_vw o
join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on o.id=os.order_id
join GLOBAL_FINANCES.GLOBAL_ORDERS go on go.ORDER_ID=os.order_id
where
o.created_at >= current_date::date - interval '7 d'
 and os.vertical_sub_group in ('Express','Farmacia','Licores','Super','Hiper')
 and (os.vertical_sub_group not in ('Turbo') or os.vertical_sub_group is null)
 and datediff(minutes,o.created_at, o.closed_at) <5
 and os.name not ilike '%covid%' and os.type not ilike '%membresias%'
 and os.name not ilike '%turbo%'
 and os.name not ilike '%bebidas j%'
 and (os.type not in ('rappi_pay','queue_order_flow') or os.type is null)
 and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
) j
group by 1,2,3,4
having
count(distinct case when created_at::date = current_date::date - interval '7d' then order_id else null end) >= 3
and
count(distinct case when created_at::date = current_date::date then order_id else null end) >= 5
    """.format(country=country,timezone=timezone)

    logs = '''select store_id as storeid from ops_occ.early_order_closed
  where alarm_at::date = current_date
  and country = '{country}'
  '''.format(country=country,timezone=timezone)

    df_logs = snow.run_query(logs)
    metrics = snow.run_query(query)

    return metrics, df_logs


def notification_slack(a,logs):
    channel_alert = timezones.slack_channels('rt_operation')
    current_time = timezones.country_current_time(country)

    print(a)
    df = pd.merge(a, logs, how='left', left_on=a['store_id'], right_on=logs['storeid'])
    df = df[df['storeid'].isnull()]
    df = df.drop(['key_0', 'storeid'], axis=1)
    if not df.empty:

        home = expanduser("~")
        results_file = '{}/early_order_closed_{}.xlsx'.format(home,country)
        df.to_excel(results_file,sheet_name='stores', index=False)
        text = """
        *Alarma de indicación de posibles errores* :alert: :flag-{country}:
        Pais: {country}""".format(country=country)
        slack.file_upload_channel(channel_alert,text,results_file,'xlsx')
        df['country'] = country
        df['alarm_at'] = current_time
        df = df[['type','name','store_id','store_type_group','country','alarm_at','order_ids']]
        snow.upload_df_occ(df,'early_order_closed')
        print(text)
    else:
        print('df empty')

def run_alarm(country):
    a, logs = offenders(country)
    notification_slack(a,logs)

countries = ['mx','br','co','cl','uy','cr','ec','pe','ar']
for country in countries:
    print(country)
    run_alarm(country)