import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from functions import timezones as tm
from os.path import expanduser
from openpyxl import load_workbook
from lib import snowflake as snow

def cbpi_cbpr(country):
	timezone, interval = tm.country_timezones(country)
	query_cbpi = f"""
	--no_cache
	with o as (select id, created_at, payment_method
	from orders
	where 
	(
	(created_at >= date(now() at time zone 'America/{timezone}') - interval '7d'
	and created_at < date(now() at time zone 'America/{timezone}') - interval '6d') 
	or 
	(created_at >= date(now() at time zone 'America/{timezone}'))
	)
	and state in ('canceled_by_partner_inactivity')
	and extract('hour' from created_at) = extract('hour' from now() AT TIME ZONE 'America/{timezone}')
	)
	,cbpi as (
	SELECT extract(hour
			   FROM o.created_at) AS hour,
			   count(distinct o.id),
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int as cbpi_d0,
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date - interval '7d' then o.id else null end)::int cbpi_d7
		   FROM o

	join order_stores os on os.order_id=o.id and os.name not ilike '%burger%king%' and os.name not ilike '%popeyes%'
	WHERE extract('hour' from o.created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
	and payment_method not ilike '%synthetic%'
	group by 1

	)
	select *
	from cbpi 
	where 
	(cbpi_d0 >= ((cbpi_d7*0.3)+cbpi_d7) 
	and cbpi_d0 >= 8
	and (cbpi_d0-cbpi_d7) >= 5 
	)
	"""

	query_cbpi_sheets = f"""
	--no_cache
	with o as (select id, created_at
	from orders
	where 
	(
	(created_at >= date(now() at time zone 'America/{timezone}') - interval '7d'
	and created_at < date(now() at time zone 'America/{timezone}') - interval '6d'
	) 
	or 
	(
	created_at >= date(now() at time zone 'America/{timezone}')
	)
	)
	and state in ('canceled_by_partner_inactivity')
	and extract('hour' from created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
	)

	SELECT extract('hour' from now()AT TIME ZONE 'America/{timezone}') as hour, os.store_id, os.name, case when os.is_marketplace=true then 'sim' else 'no' end as is_marketplace,
			   count(distinct o.id),
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int as cbpi_d0,
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date - interval '7d' then o.id else null end)::int cbpi_d7,
			   string_agg(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id::text else null end,',') as order_ids

	FROM o
	left join order_stores os on os.order_id=o.id and (os.contact_email not like '%@synth.rappi.com' or os.contact_email is null)
	WHERE extract('hour' from o.created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
	group by 1,2,3,4
	having count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int >=1
	"""	

	query_cbpr = f"""
			--no_cache
with o as (select id, created_at, payment_method
from orders
where 
(
  (created_at >= date(now() at time zone 'America/{timezone}') - interval '7d'
and created_at < date(now() at time zone 'America/{timezone}') - interval '6d'
  ) 
 or 
  (
  created_at >= date(now() at time zone 'America/{timezone}')
  )
)
and state in ('canceled_partner_order_refused','canceled_partner_after_take')

and extract('hour' from created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
	)
,cbpi as (
SELECT extract(hour
			   FROM o.created_at) AS hour,
			   count(distinct o.id),
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int as cbpr_d0,
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date - interval '7d' then o.id else null end)::int cbpr_d7
FROM o
join order_stores os on os.order_id=o.id --and os.name not ilike '%burger%king%' and os.name not ilike '%popeyes%'
join order_modifications om on om.order_id=o.id and om.type in ('refuse_order_by_partner','refuse_partner_after_take')
WHERE extract('hour' from o.created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
and payment_method not ilike '%synthetic%'
and not (((params->'secondary_reason')::text ilike '%CBPR%BAD_RELATED%' or (params->'secondary_reason')::text ilike '%MAPPING_UNKNOWN%') and (os.name ilike '%burger%king%' or os.name ilike '%burguer%king%'))
group by 1
 )
 select *
 from cbpi 
  where 
 (cbpr_d0 >= ((cbpr_d7*0.3)+cbpr_d7) 
 and cbpr_d0 >= 8
 and (cbpr_d0-cbpr_d7) >= 5 
 )
"""

	query_cbpr_sheets = f"""
	--no_cache
	with o as (select id, created_at
	from orders
	where 
	(
	  (created_at >= date(now() at time zone 'America/{timezone}') - interval '7d'
	and created_at < date(now() at time zone 'America/{timezone}') - interval '6d'
	  ) 
	or 
	  (
	   created_at >= date(now() at time zone 'America/{timezone}')
	  )
	)
	and state in ('canceled_partner_order_refused','canceled_partner_after_take')
and extract('hour' from created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
	)

SELECT extract('hour' from now()AT TIME ZONE 'America/{timezone}') as hour, os.store_id, os.name, case when os.is_marketplace=true then 'sim' else 'no' end as is_marketplace,
			   count(distinct o.id),
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int as cbpr_d0,
			   count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date - interval '7d' then o.id else null end)::int cbpr_d7,
			   string_agg(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id::text else null end,',') as order_ids

FROM o
left join order_stores os on os.order_id=o.id and (os.contact_email not like '%@synth.rappi.com' or os.contact_email is null)
WHERE extract('hour' from o.created_at) = extract('hour' from now()AT TIME ZONE 'America/{timezone}')
group by 1,2,3,4
having count(distinct case when o.created_at::date = (now() at time zone 'America/{timezone}')::date then o.id else null end)::int >=1
	"""


	if country == 'co':
		cbpi = redash.run_query(1904, query_cbpi)
		cbpr = redash.run_query(1904, query_cbpr)
		cbpi_sheets = redash.run_query(1904, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1904, query_cbpr_sheets)
	elif country == 'ar':
		cbpi = redash.run_query(1337, query_cbpi)
		cbpr = redash.run_query(1337, query_cbpr)
		cbpi_sheets = redash.run_query(1337, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1337, query_cbpr_sheets)
	elif country == 'cl':
		cbpi = redash.run_query(1155, query_cbpi)
		cbpr = redash.run_query(1155, query_cbpr)
		cbpi_sheets = redash.run_query(1155, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1155, query_cbpr_sheets)
	elif country == 'mx':
		cbpi = redash.run_query(1371, query_cbpi)
		cbpr = redash.run_query(1371, query_cbpr)
		cbpi_sheets = redash.run_query(1371, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1371, query_cbpr_sheets)
	elif country == 'uy':
		cbpi = redash.run_query(1156, query_cbpi)
		cbpr = redash.run_query(1156, query_cbpr)
		cbpi_sheets = redash.run_query(1156, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1156, query_cbpr_sheets)
	elif country == 'ec':
		cbpi = redash.run_query(1922, query_cbpi)
		cbpr = redash.run_query(1922, query_cbpr)
		cbpi_sheets = redash.run_query(1922, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1922, query_cbpr_sheets)
	elif country == 'cr':
		cbpi = redash.run_query(1921, query_cbpi)
		cbpr = redash.run_query(1921, query_cbpr)
		cbpi_sheets = redash.run_query(1921, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1921, query_cbpr_sheets)
	elif country == 'pe':
		cbpi = redash.run_query(1157, query_cbpi)
		cbpr = redash.run_query(1157, query_cbpr)
		cbpi_sheets = redash.run_query(1157, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1157, query_cbpr_sheets)
	elif country == 'br':
		cbpi = redash.run_query(1338, query_cbpi)
		cbpr = redash.run_query(1338, query_cbpr)
		cbpi_sheets = redash.run_query(1338, query_cbpi_sheets)
		cbpr_sheets = redash.run_query(1338, query_cbpr_sheets)

	return cbpi, cbpr, cbpi_sheets, cbpr_sheets

def cbpr_details(country,order_ids):
	query_details = f'''
	WITH cbpr AS
  ( SELECT CASE
			   WHEN params->>'refuse_reason_enum'='7' THEN 'ran_out_products'
			   ELSE params->>'refuse_reason_enum'
		   END AS refuse_reason,
		   order_id,
		   created_at,
		   (params->'secondary_reason')::text AS secondary_reason,
		   CASE
			   WHEN TYPE IN ('refuse_order_by_partner',
							 'refuse_partner_after_take') THEN 'cbpr'
			   ELSE NULL
		   END AS has_cbpr
   FROM order_modifications
   WHERE order_id in ({order_ids})
   GROUP BY 1,
			2,
			3,
			4,
			5 ) 

SELECT 
		  CASE
			  WHEN coalesce(cbpr.refuse_reason,b.refuse_reason_enum,c.refuse_reason_enum)='7' THEN 'ran_out_products'
			  ELSE coalesce(cbpr.refuse_reason,b.refuse_reason_enum,c.refuse_reason_enum)
		  END AS refuse_reason,
		  count(DISTINCT cbpr.order_id) as cbpr_orders
   FROM cbpr
   LEFT JOIN ORDER_REFUSE_REASONS_ORDER b ON b.order_id=cbpr.order_id
   LEFT JOIN ORDER_REFUSE_REASONS c ON c.id=b.order_refuse_reason_id
   JOIN orders o ON o.id=cbpr.order_id AND o.payment_method NOT ILIKE '%synthetic%'
   
   group by 1
   '''

	if country == 'co':
		cbpr_details = redash.run_query(1904, query_details)
	elif country == 'ar':
		cbpr_details = redash.run_query(1337, query_details)
	elif country == 'cl':
		cbpr_details = redash.run_query(1155, query_details)
	elif country == 'mx':
		cbpr_details = redash.run_query(1371, query_details)
	elif country == 'uy':
		cbpr_details = redash.run_query(1156, query_details)
	elif country == 'ec':
		cbpr_details = redash.run_query(1922, query_details)
	elif country == 'cr':
		cbpr_details = redash.run_query(1921, query_details)
	elif country == 'pe':
		cbpr_details = redash.run_query(1157, query_details)
	elif country == 'br':
		cbpr_details = redash.run_query(1338, query_details)

	return cbpr_details

def send_notification(cbpi,cbpr,cbpi_sheets,cbpr_sheets):
	channel_alert = tm.slack_channels('trend_alarms')
	current_time = tm.country_current_time(country)

	try:
		cbpi_sheets = cbpi_sheets[['hour', 'store_id', 'name', 'is_marketplace', 'cbpi_d0', 'cbpi_d7', 'order_ids']]
		print(cbpi_sheets)

	except Exception as e:
		print(e)
	try:
		cbpr_sheets = cbpr_sheets[['hour', 'store_id', 'name', 'is_marketplace', 'cbpr_d0', 'cbpr_d7', 'order_ids']]
		cbpr_sheets['country'] = country
		cbpr_sheets['alarm_at'] = current_time
		print(cbpr_sheets)

		order_ids = cbpr_sheets['order_ids'].unique().tolist()
		order_ids = [f'{order_ids}' for order_ids in order_ids]
		order_ids = ','.join(order_ids)
		print(order_ids)

	except Exception as e:
		print(e)

	print("cbpi: "+country)
	for index, row in cbpi.iterrows():
		rows = dict(row)
		text = '''Alarma de Pico en CBPI :alert:
		:flag-{country}:
		Pais: {country}
		Threshold: Al menos 30% de aumento sobre D-7
		En la hora actual hay {cbpi_d0} ordenes cancelados por CBPI contra {cbpi_d7} en D-7 hasta el momento
		'''.format(country=country,
			cbpi_d0 = int(row['cbpi_d0']),
			cbpi_d7 = int(row['cbpi_d7']))
		cbpi_sheets = cbpi_sheets[['hour', 'store_id', 'name', 'is_marketplace', 'cbpi_d0', 'cbpi_d7', 'order_ids']]

		home = expanduser("~")
		results_file = '{}/cbpi_{}.xlsx'.format(home,country)
		cbpi_sheets.to_excel(results_file,sheet_name='cbpi_stores', index=False)

		slack.file_upload_channel(channel_alert,text,results_file,'xlsx')
		cbpi_sheets['country'] = country
		cbpi_sheets['alarm_at'] = current_time
		snow.upload_df_occ(cbpi_sheets, 'cbpi_peak_sheets')

		if os.path.exists(results_file):
			os.remove(results_file)

	print("cbpr: "+country)
	for index, row in cbpr.iterrows():
		rows = dict(row)
		reasons = cbpr_details(country, order_ids)
		reasons = reasons.sort_values(by='cbpr_orders', ascending=False)
		soma = sum(reasons['cbpr_orders'])
		reasons['share'] = round(((reasons['cbpr_orders'] / soma)*100),2)
		text_2 = '''Alarma de Pico en CBPR :alert:
		:flag-{country}:
		Pais: {country}
		Threshold: Al menos 30% de aumento sobre D-7
		En la hora actual hay {cbpr_d0} ordenes cancelados por CBPR contra {cbpr_d7} en D-7 hasta el momento
		'''.format(country=country,
			cbpr_d0 = int(row['cbpr_d0']),
			cbpr_d7 = int(row['cbpr_d7']))

		cbpr_sheets = cbpr_sheets[['hour', 'store_id', 'name', 'is_marketplace', 'cbpi_d0', 'cbpi_d7', 'order_ids']]
		home = expanduser("~")
		results_file2 = '{}/cbpr_{}.xlsx'.format(home,country)
		cbpr_sheets.to_excel(results_file2,sheet_name='cbpr_stores', index=False)
		book = load_workbook(results_file2)
		writer = pd.ExcelWriter(results_file2, engine='openpyxl')
		writer.book = book
		writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
		reasons.to_excel(writer, "share_reasons", index=False)

		writer.save()

		slack.file_upload_channel(channel_alert,text_2,results_file2,'xlsx')
		cbpr_sheets['country'] = country
		cbpr_sheets['alarm_at'] = current_time
		snow.upload_df_occ(cbpr_sheets, 'cbpr_peak_sheets')

		if os.path.exists(results_file2):
			os.remove(results_file2)

countries = ['br','ar','cl','pe','uy','ec','cr','mx','co']

for country in countries:
	print(country)
	try:
		cbpi, cbpr, cbpi_sheets, cbpr_sheets = cbpi_cbpr(country)
		send_notification(cbpi, cbpr, cbpi_sheets, cbpr_sheets)
	except Exception as e:
		print(e)