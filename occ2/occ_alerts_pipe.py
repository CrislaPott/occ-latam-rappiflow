import os, json, requests
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from unidecode import unidecode
from lib import slack
from lib import thread_listener as tl
from lib import snowflake as snow
from functions import timezones
import time

def get_country(text_to_parse):
    values = text_to_parse.split('\n')
    for value in values:
        if 'pais' in value:
            return value
    return None 

def threads(channel):
	current_time = timezones.country_current_time('br')
	latest = current_time.replace(minute=0, hour=0, second=0, microsecond=0)
	oldest = latest - timedelta(days=1)
	print(current_time)
	print(latest)
	print(oldest)

	channel_history = tl.get_channel_history(
		channel = channel,
		messages_per_page = 5000,
		max_messages = 2000000,
		oldest = oldest,
		latest = latest
		)

	df = channel_history
	df = df[["ts","text", "user"]]
	df['text'] = df['text'].str.lower()
	df['country'] = df['text'].str.extract(r"(?<=pais:)(.*)")

	df['thread_ts'] = df['ts']
	df['channel_id'] = channel
	df = df[['channel_id','thread_ts', 'user', 'text','country']]
	print(df)

	snow.upload_df_occ(df, 'occ_alerts')

channels = ['C01C1LSQREW','C01EEPH2ECU','C01GMHJRDM2','C01E5G49L1K','C01672B528Y']
for channel in channels:
	current_time = timezones.country_current_time('br')
	print(channel)
	print(current_time)
	time.sleep(60)
	print(current_time)
	threads(channel)