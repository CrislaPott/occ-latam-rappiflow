import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from functions import timezones


def chat(country):
    timezone, interval = timezones.country_timezones(country)
    if country in ['br']:
        query_chat = '''with a as (
select (max(created_at) - interval '6h') as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/{timezone}' - last_data)) / 60)) as delay_minutes

from a
'''.format(timezone=timezone)
    elif country in ['ar']:
        query_chat = '''with a as (
select (max(created_at) + interval '2h') as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/{timezone}' - last_data)) / 60)) as delay_minutes

from a
'''.format(timezone=timezone)
    elif country in ['mx']:
        query_chat = '''with a as (
select (max(created_at)) as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/Mexico_City' - last_data)) / 60)) as delay_minutes

from a
'''
    elif country in ['cl']:
        query_chat = '''with a as (
select (max(created_at) + interval '1h') as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/Santiago' - last_data)) / 60)) as delay_minutes

from a

    '''
    elif country in ['uy']:
        query_chat = '''with a as (
select (max(created_at) + interval '2h') as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/Buenos_Aires' - last_data)) / 60)) as delay_minutes

from a
        '''
    elif country in ['cr']:
        query_chat = '''with a as (
select (max(created_at) - interval '9h') as last_data
from chats)

select 

(extract(epoch from ((now() at time zone 'America/Costa_Rica' - last_data)) / 60)) as delay_minutes

from a
            '''

    else:
        query_chat = '''with a as (
select (max(created_at) - interval '8h') as last_data
from chats)
select 
(extract(epoch from ((now() at time zone 'America/{timezone}' - last_data)) / 60)) as delay_minutes

from a'''.format(timezone=timezone)

    if country == 'co':
        chat = redash.run_query(652, query_chat)
    elif country == 'ar':
        chat = redash.run_query(647, query_chat)
    elif country == 'cl':
        chat = redash.run_query(649, query_chat)
    elif country == 'mx':
        chat = redash.run_query(651, query_chat)
    elif country == 'uy':
        chat = redash.run_query(650, query_chat)
    elif country == 'ec':
        chat = redash.run_query(2184, query_chat)
    elif country == 'cr':
        chat = redash.run_query(2183, query_chat)
    elif country == 'pe':
        chat = redash.run_query(745, query_chat)
    elif country == 'br':
        chat = redash.run_query(648, query_chat)

    return chat

def alarm(a):
    print(a)
    hour1 = (datetime(2021, 4, 24, 10, 0, 0, 0)).time()

    current_time = timezones.country_current_time(country).time()
    print(current_time)
    if current_time >= hour1:
        if float(a.delay_minutes) >= 30:
            print(a)
            text = f'''
        *Alarma - Problemas de Delay en los Microservicios*
        :flag-{country}:
        Pais: {country}
        MS Chat Monit - Delay: *{round(float(a.delay_minutes),2)}* min'''
            print(text)
            slack.bot_slack(text=text, channel='C02SA9D3TQC')


countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']

for country in countries:
  try:
    print(country)
    a = chat(country)
    alarm(a)
  except Exception as e:
    print(e)