import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import slack, redash
from lib import snowflake as snow
from functions import timezones
import pytz
from os.path import expanduser

def cbpi_cbpr():

	query = f'''
	
select    'br' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from br_CORE_ORDERS_PUBLIC.orders_vw o
    left join br_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)
union all


select    'co' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from co_CORE_ORDERS_PUBLIC.orders_vw o
    left join co_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'mx' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from mx_CORE_ORDERS_PUBLIC.orders_vw o
    left join mx_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'ar' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from ar_CORE_ORDERS_PUBLIC.orders_vw o
    left join ar_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all

select    'cl' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from cl_CORE_ORDERS_PUBLIC.orders_vw o
    left join cl_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'pe' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from pe_CORE_ORDERS_PUBLIC.orders_vw o
    left join pe_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'ec' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from ec_CORE_ORDERS_PUBLIC.orders_vw o
    left join ec_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'cr' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from cr_CORE_ORDERS_PUBLIC.orders_vw o
    left join cr_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)

union all


select    'uy' as country, os.store_id::text as store_id, os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null
                          end) as total_cbpi,
           count(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id else null
                          end) as total_cbpr,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage_cbpi,
           total_cbpr/total_orders as percentage_cbpr,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id::text else null
                          end,',') as order_ids_cbpi,
           listagg(distinct case when o.state in ('canceled_partner_after_take','canceled_partner_order_refused')
                                    then o.id::text else null
                          end,',') as order_ids_cbpr,
      case when (total_cbpi >= 3 and percentage_cbpi >= 0.60) then 'CBPI' else 'CBPR' end as alarm_type
    from uy_CORE_ORDERS_PUBLIC.orders_vw o
    left join uy_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
    where o.created_at::date >= current_date::date - 7
    group by 1,2,3
    having (total_cbpi >= 3 and percentage_cbpi >= 0.60)
or (total_cbpr >= 3 and percentage_cbpr >= 0.60)
	'''

	metrics = snow.run_query(query)
	return metrics

def logs(country):
	timezone, interval = timezones.country_timezones(country)
	query = f'''
	--no_cache
	select store_id::text as storeid, max(total_cbpi_cbpr) as total_cbpi_cbpr
	from ops_occ.cbpi_cbpr_latam
	where alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date - 8
	and country ='{country}'
	group by 1

union all

    select storeid::text as storeid, max(total_cbpr) as total_cbpi_cbpr
	from ops_occ.cbpr_latam_alarm
	where alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date - 8
	and country ='{country}'
	group by 1

union all

	select storeid::text as storeid, max(total_cbpi) as total_cbpi_cbpr
	from ops_occ.cbpi_latam_alarm
	where alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date - 8
	and country ='{country}'
	group by 1
	'''
	reported = snow.run_query(query)
	return reported

def news(df):
	current_time = timezones.country_current_time(country)
	channel_alert = timezones.slack_channels('trend_alarms')
	if not df.empty:
		reported_stores = logs(country)
		news = pd.merge(df,reported_stores, how='left',left_on=df['store_id'], right_on=reported_stores['storeid'])
		news = news[(news['storeid'].isnull())]
		news = news.drop(['key_0','total_cbpi_cbpr','storeid'], axis=1)
		print(news)

		###CBPI

		news_cbpi = news[news['alarm_type'] == 'CBPI']

		if not news_cbpi.empty:
			print('CBPI')
			stores_cbpi = news_cbpi[['store_id', 'name', 'total_cbpi', 'total_orders', 'percentage_cbpi', 'order_ids_cbpi']]
			stores_cbpi = stores_cbpi.rename(columns={'store_id':'storeid','percentage_cbpi':'percentage','order_ids_cbpi':'order_ids'})
			to_upload = stores_cbpi
			to_upload['country'] = country
			to_upload['alarm_at'] = current_time
			to_upload = to_upload[['country','storeid','alarm_at','total_cbpi','total_orders','percentage','order_ids']]

			print(to_upload)

			snow.upload_df_occ(to_upload,'cbpi_latam_alarm')

			for index, row in stores_cbpi.iterrows():
				store_id = row['storeid']
				store_name = row['name']
				total_cbpi = row['total_cbpi']
				total_orders = row['total_orders']
				percentage = round(row['percentage'],2)*100
				order_ids = row['order_ids']

				text = f'''
						*Alarma de CBPI por tiendas :alert:* 
						Tienda con mayor a 60% cancelaciones CBPI en los ultimos 7 dias
						:flag-{country}:
						Pais: {country}
						Store ID: {store_id}
						Tienda: {store_name}
						Total CBPI last 7 days: {total_cbpi}
						Total Orders last 7 days: {total_orders}
						Percentage: {percentage}%
						Orders IDs: {order_ids}
						'''
				print(text)
				slack.bot_slack(text,channel_alert)

		else:
			print('cbpi null')

		###CBPR

		news_cbpr = news[news['alarm_type'] == 'CBPR']

		if not news_cbpr.empty:
			print('CBPR')
			stores_cbpr = news_cbpr[
				['store_id', 'name', 'total_cbpr', 'total_orders', 'percentage_cbpr', 'order_ids_cbpr']]
			stores_cbpr = stores_cbpr.rename(
				columns={'store_id': 'storeid', 'percentage_cbpr': 'percentage', 'order_ids_cbpr': 'order_ids'})
			to_upload = stores_cbpr
			to_upload['country'] = country
			to_upload['alarm_at'] = current_time
			to_upload = to_upload[
				['country', 'storeid', 'alarm_at', 'total_cbpr', 'total_orders', 'percentage', 'order_ids']]
			print(to_upload)

			snow.upload_df_occ(to_upload,'cbpr_latam_alarm')

			for index, row in stores_cbpr.iterrows():
				store_id = row['storeid']
				store_name = row['name']
				total_cbpr = row['total_cbpr']
				total_orders = row['total_orders']
				percentage = round(row['percentage'], 2) * 100
				order_ids = row['order_ids']

				text = f'''
					*Alarma de CBPR por tiendas :alert:* 
					Tienda con mayor a 60% cancelaciones CBPR en los ultimos 7 dias
					:flag-{country}:
					Pais: {country}
					Store ID: {store_id}
					Tienda: {store_name}
					Total CBPR last 7 days: {total_cbpr}
					Total Orders last 7 days: {total_orders}
					Percentage: {percentage}%
					Orders IDs: {order_ids}
					'''
				print(text)
				slack.bot_slack(text, channel_alert)
		else:
			print('cbpr null')

	else:
		print('news null')

countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']

a = cbpi_cbpr()

for country in countries:
	print(country)
	try:
		df = a[a['country']==country]
		news(df)
	except Exception as e:
		print(e)