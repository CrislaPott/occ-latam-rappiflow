import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from datetime import date, datetime, timedelta
import pytz

tz = pytz.timezone('America/Buenos_Aires')
current_time = datetime.now(tz)

def get_lt_wo_stores():
    long_tail_wo_query = """with alarms as(with stores
AS
  (
            SELECT    distinct a.country,
                      a.brand_group as brand,
                      a.major_subvertical as subvertical,
                      b.store_id
            FROM      CPGS_SALESCAPABILITY.TBL_SNP_STORES_SOT a
            LEFT JOIN
                      (
                             SELECT *
                             FROM   cpgs_salescapability.tbl_core_stores_link
                             WHERE  vertical_group = 'CPGS') b
            ON        a.country = b.country_code
            AND       a.brand_group = b.brand_group_name
            AND       a.major_subvertical = b.treated_subvertical
            INNER JOIN (SELECT DISTINCT COUNTRY, STORE_ID FROM CPGS_SALESCAPABILITY.TBL_MASTER_ORDERS) c
            on b.country_code = c.country
            and b.store_id = c.store_id
            WHERE     A.OWNERSHIP = 'LT'
            and IS_LATEST)
         
           
           
 , base_stores as (
                  select distinct country, store_id from stores
                  ),

base as (
select
o.country,
o.order_id,
FLOOR(
      DATEDIFF('day',
               o.closed_at::date,
               TO_TIMESTAMP(CONVERT_TIMEZONE('America/Bogota',current_timestamp)
              )::date)/7
    ) AS wk, 
o.store_id,
o.store_name,
o.vertical,
o.store_type,
cr.level_1 IS NOT NULL AS cancelada,
cr.level_1 = 'partner_related_errors' AS partner_cancelada,  
o.application_user_id,
o.state_type,
cr.level_1
from Ops_global.global_orders o
 join (select distinct * from base_stores) st
  on st.store_id = o.store_id
  and st.country = o.country
left join ops_global.cancellation_reasons cr on cr.order_id = o.order_id and cr.country = o.country
where o.vertical in ('Super/Hiper','Express','Licores','Farmacia','Specialized','Pets')
//and state_type not in ('canceled_by_fraud') 
AND o.closed_at::date >= current_date - interval '30 days'
and (not(state_type = 'canceled_by_user' and DATEDIFF(min,o.created_at,o.closed_at) < 5))
)

            
,check_cancel_by_user as (select 
 country,
store_id,
store_name,
store_type,
vertical,

  count(case when wk = 0 AND cancelada = TRUE and state_type = 'canceled_by_user' then order_id else null end) as total_cancels_by_user_w_1,
count(distinct case when wk = 0 AND cancelada = TRUE and state_type = 'canceled_by_user' then application_user_id else null end) as total_user_cancels_w_1,
case when total_cancels_by_user_w_1 <= 1.5*total_user_cancels_w_1  then 'TRUE' ELSE 'FALSE' end as valid_user_cancels_w_1,
  count(case when wk = 1 AND cancelada = TRUE and state_type = 'canceled_by_user' then order_id else null end) as total_cancels_by_user_w_2,
count(distinct case when wk = 1 AND cancelada = TRUE and state_type = 'canceled_by_user' then application_user_id else null end) as total_user_cancels_w_2,  
case when total_cancels_by_user_w_2 <= 1.5*total_user_cancels_w_2  then 'TRUE' ELSE 'FALSE' end as valid_user_cancels_w_2,
  count(case when wk = 2 AND cancelada = TRUE and state_type = 'canceled_by_user' then order_id else null end) as total_cancels_by_user_w_3,
count(distinct case when wk = 2 AND cancelada = TRUE and state_type = 'canceled_by_user' then application_user_id else null end) as total_user_cancels_w_3, 
case when total_cancels_by_user_w_3 <= 1.5* total_user_cancels_w_3 then 'TRUE' ELSE 'FALSE' end as valid_user_cancels_w_3,
  count(case when wk = 3 AND cancelada = TRUE and state_type = 'canceled_by_user' then order_id else null end) as total_cancels_by_user_w_4,
count(distinct case when wk = 3 AND cancelada = TRUE and state_type = 'canceled_by_user' then application_user_id else null end) as total_user_cancels_w_4,
case when total_cancels_by_user_w_4 <= 1.5* total_user_cancels_w_4 then 'TRUE' ELSE 'FALSE' end as valid_user_cancels_w_4
  from base
  group by 1,2,3,4,5
)               
               
select
a.country,
a.store_id,
a.store_name,
a.store_type,
a.vertical,
max(valid_user_cancels_w_1) as user_cancels_w_1,
max(valid_user_cancels_w_2) as user_cancels_w_2,
max(valid_user_cancels_w_3) as user_cancels_w_3,
max(valid_user_cancels_w_4) as user_cancels_w_4, 


count(case when wk = 0 then order_id end) as total_orders_w_1,
count(case when valid_user_cancels_w_1 = 'TRUE' and  wk = 0 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud' then order_id
      when valid_user_cancels_w_1 = 'FALSE' and wk = 0 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud' and state_type <> 'canceled_by_user' then order_id end) as total_canceladas_w_1,
case when total_orders_w_1 = 0 then 0 else round(total_canceladas_w_1 / total_orders_w_1,4) end as cancel_rate_w_1 ,
-- //case when total_orders_w_1 = 0 then 0 else round(count(case when wk = 0 and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_1,4) end as partner_cr_w_1,


count(case when wk = 1 then order_id end) as total_orders_w_2,
count(case when total_cancels_by_user_w_2 <= 1.5*total_user_cancels_w_2 and wk = 1 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud' then order_id 
      when total_cancels_by_user_w_2 > 1.5*total_user_cancels_w_2 and wk = 1 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud'  and state_type <> 'canceled_by_user' then order_id end) as total_canceladas_w_2,
case when total_orders_w_2 = 0 then 0 else  round(total_canceladas_w_2 / total_orders_w_2,4) end as cancel_rate_w_2,
-- //case when total_orders_w_2 = 0 then 0 else round(count(case when wk = 1 and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_2,4) end as partner_cr_w_2,

 
count(case when wk = 2 then order_id end) as total_orders_w_3,
count(case when valid_user_cancels_w_3 = 'TRUE' and wk = 2 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud' then order_id
      when valid_user_cancels_w_3 = 'FALSE' and wk = 2 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud'  and state_type <> 'canceled_by_user' then order_id end) as total_canceladas_w_3,
case when total_orders_w_3 = 0 then 0 else round(total_canceladas_w_3 / total_orders_w_3,4) end as cancel_rate_w_3 ,
-- //case when total_orders_w_3 = 0 then 0 else round(count(case when wk = 2 and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_3,4) end as partner_cr_w_3,

count(case when wk = 3 then order_id end) as total_orders_w_4,
count(case when valid_user_cancels_w_4 = 'TRUE' and wk = 3 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud' then order_id
      when valid_user_cancels_w_4 = 'FALSE' and wk = 3 AND cancelada = TRUE and level_1 is not null and level_1 <> 'fraud'  and state_type <> 'canceled_by_user' then order_id end) as total_canceladas_w_4,
case when total_orders_w_4 = 0 then 0 else round(total_canceladas_w_4 / total_orders_w_4,4) end as cancel_rate_w_4 ,
-- //case when total_orders_w_4 = 0 then 0 else round(count(case when wk = 3 and level_1 = 'partner_related_errors' then order_id end) / total_orders_w_4,4) end as partner_cr_w_4,

case when (total_canceladas_w_1 >= 2 and total_orders_w_1 >= 2) and cancel_rate_w_1 > 0.1  then 1 else 0 end as triggered_w_1,

case when (total_canceladas_w_2 >= 2 and total_orders_w_2 >= 2) and cancel_rate_w_2 > 0.1  then 1 else 0 end as triggered_w_2,

case when (total_canceladas_w_3 >= 2 and total_orders_w_3 >= 2) and cancel_rate_w_3 > 0.1  then 1 else 0 end as triggered_w_3,

case when (total_canceladas_w_4 >= 2 and total_orders_w_4 >= 2) and cancel_rate_w_4 > 0.1  then 1 else 0 end as triggered_w_4,

case when triggered_w_1 = 1 and triggered_w_2 = 0 then '1. Alerta OCC'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 0 then '2. Apagado 24 hrs'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 0  then '3. Apagado 48 hrs'
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 1  then '4. Apagado 96 hrs'
end as status,

case when triggered_w_1 = 1 and triggered_w_2 = 0 then 0
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 0 then 1
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 0  then 2
when triggered_w_1 = 1 and triggered_w_2 = 1 and triggered_w_3 = 1 and triggered_w_4 = 1  then 4
end as closed_time,            
total_canceladas_w_1 + total_canceladas_w_2 + total_canceladas_w_3 + total_canceladas_w_4 as total_cancels,
               total_orders_w_1 + total_orders_w_2 + total_orders_w_3 + total_orders_w_4  as total_created
               
from base a
left join  check_cancel_by_user b
on a.country = b.country
and a.store_id = b.store_id
and a.store_type = b.store_type
group by 1,2,3,4,5
having total_orders_w_1 + total_orders_w_2 + total_orders_w_3 + total_orders_w_4 <= 25

order by total_created desc)


,reasons as (select a.country, a.store_id, a.status,o.order_id, cr.level_1, cr.level_2,cr.level_3, state_type,FLOOR(
      DATEDIFF('day',
               o.closed_at::date,
               TO_TIMESTAMP(CONVERT_TIMEZONE('America/Bogota',current_timestamp)
              )::date)/7
    ) AS wk, 
             user_cancels_w_1, 
      user_cancels_w_2,
       user_cancels_w_3,
      user_cancels_w_4,
             case when wk = 0 and USER_CANCELS_W_1 = FALSE and state_type = 'canceled_by_user' then 'NO'
                  when wk = 1 and USER_CANCELS_W_2 = FALSE and state_type = 'canceled_by_user' then 'NO'
                  when wk = 2 and USER_CANCELS_W_3 = FALSE and state_type = 'canceled_by_user' then 'NO'
                  when wk = 3 and USER_CANCELS_W_4 = FALSE and state_type = 'canceled_by_user' then 'NO'
                else 'YES'
             end as should_count
from (select distinct country, 
      store_id, 
      status,
             user_cancels_w_1,
      user_cancels_w_2,
       user_cancels_w_3,
      user_cancels_w_4 from alarms)  a
join Ops_global.global_orders o
  on a.store_id = o.store_id
  and a.country = o.country
join ops_global.cancellation_reasons cr on cr.order_id = o.order_id and cr.country = o.country
where a.status is not null
and o.vertical in ('Super/Hiper','Express','Licores','Farmacia','Specialized','Pets')
AND o.closed_at::date >= current_date - interval '30 days'
AND cr.level_1 IS NOT NULL
and (not(state_type = 'canceled_by_user' and DATEDIFF(min,o.created_at,o.closed_at) < 5))    
and should_count = 'YES'
order by 1,2,3,4 asc)



select a.country, 
a.store_id, 
c.store_name,
a.status,
c.contact_email,
count(distinct CASE WHEN a.level_2 = 'stockout' then a.order_id else null end) as stockout,
count(distinct CASE WHEN a.level_2 = 'user_black_box' then a.order_id else null end) as user_black_box,
count(distinct CASE WHEN a.level_2 = 'shopper_delay' then a.order_id else null end) as shopper_delay,
count(distinct CASE WHEN a.level_2 = 'retail_failure' then a.order_id else null end) as retail_failure,
count(distinct CASE WHEN a.level_2 not in ('stockout','user_black_box', 'shopper_delay','retail_failure') then a.order_id else null end) as others,
cast(array_agg(distinct a.order_id) as varchar) as order_id,
ZEROIFNULL(max(b.GMV_TOTAL)) AS GMV_TOTAL_L4W  from reasons a
LEFT join OPS_OCC.RISK_MANAGEMENT b
on a.country = b.country
and a.store_id = b.store_id
LEFT JOIN (select 'BR' as country, store_id, contact_email, name as store_name  from br_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'AR' as country, store_id, contact_email, name as store_name   from ar_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'CR' as country, store_id, contact_email, name as store_name   from cr_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'CL' as country, store_id, contact_email, name as store_name   from cl_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'CO' as country, store_id, contact_email, name as store_name   from co_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'MX' as country, store_id, contact_email, name as store_name   from mx_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'PE' as country, store_id, contact_email, name as store_name   from pe_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'UY' as country, store_id, contact_email, name as store_name   from uy_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false
union all
select 'EC' as country, store_id, contact_email, name as store_name   from ec_PGLR_MS_STORES_PUBLIC.STORES_VW
where COALESCE(_fivetran_deleted,false)=false) c
on a.country = c.country
and a.store_id = c.store_id
group by 1,2,3,4,5
order by 1,2,3 desc"""

    wo_stores = snow.run_query(long_tail_wo_query)
    return wo_stores


def get_main_reason(wo_stores):
    wo_stores['total_cancels'] = wo_stores[['stockout','user_black_box','shopper_delay','retail_failure','others']].sum(axis =1)
    wo_stores['main_cancel_reason'] = wo_stores[['stockout','user_black_box','shopper_delay','retail_failure','others']].idxmax(axis=1)
    return wo_stores

def send_data(wo_list):
    tz = pytz.timezone('America/Buenos_Aires')
    current_time = datetime.now(tz)
    
    if not wo_list.empty:
        wo_list['checked_at'] = current_time
        to_upload = wo_list[['country','store_id','store_name','status','stockout','user_black_box','shopper_delay','retail_failure','others','order_id','gmv_total_l4w','total_cancels','main_cancel_reason','checked_at']]

        print("uploading df")
        snow.upload_df_occ(to_upload, "nl_worst_offenders_long_tail")


        wo_list['alarm_at'] = current_time
        wo_list['alarm_at'] = wo_list['alarm_at'].dt.tz_localize(None) 
        text = '''

        *NONLIVE - ALARMA WORST OFFENDERS LONG TAIL*

        :no_entry: Se consideran las tiendas que tuvieron >=2 ordenes creadas y >=2 ordenes canceladas y > 10% cancel rate

        :warning: Estructura del proceso:

        Tiendas Worst Offender durante 1 semana: Identificamos las tiendas ofensoras de acuerdo con los criterios anteriores, pero no suspendemos ninguna tienda. Este reporte es solo para que comprueben y corrijan el problema
        Tiendas Worst Offenders durante 2, 3 y 4 Semanas: Con base en el desempeño de las tiendas, que permanezcan ofensoras en los criterios anteriores accionamos las siguientes suspensiones:
        2 semanas → Suspensión durante 24 horas
        3 semanas → Suspensión durante 48 horas
        4 semanas → Suspensión durante 96 horas
            
        :alert: Las tiendas que han sido suspendidas por este proceso no serán encendidas hasta que termine el plazo de suspensión :alert:
        '''
        print(text)
        home = expanduser("~")
        results_file = '{}/details_nl_worst_offenders_long_tail.csv'.format(home)
        df_final = wo_list[['country','store_id','store_name','status','contact_email','stockout','user_black_box','shopper_delay','retail_failure','others','order_id','gmv_total_l4w','total_cancels','main_cancel_reason','alarm_at']]
        df_final.to_csv(results_file, index=False)
        slack.file_upload_channel('C02MJQ1Q5H6', text, results_file, "csv")
    else:
        print('df null')



#run base query to get offenders
stores = get_lt_wo_stores()


#add total cancels column and main cancel reason for each store
stores = get_main_reason(stores)

#upload to log and send to slack
send_data(stores)