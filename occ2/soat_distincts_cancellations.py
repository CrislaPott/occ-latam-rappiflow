import pandas as pd
import sys
from lib import slack, redash
from lib import snowflake as snow
from functions import timezones

def logs(country):
	timezone, interval = timezones.country_timezones(country)
	query = f"""
	select distinct store_type as storetype
	from ops_occ.soat_distincts_cancellations 
	where alarm_at::timestamp_ntz >= convert_timezone('America/{timezone}',current_timestamp()) - interval '2h' 
	and country = '{country}'
	"""
	df = snow.run_query(query)
	return df


def soat_cancellations(country):
	query = f"""
select distinct os.type as store_type,
                count(distinct o.id) as total_orders,
                count(distinct case when o.state ilike '%cancel%' then o.id else null end) as total_canceled,
                count(distinct case when o.state ilike '%cancel%' then o.id else null end)/count(distinct o.id) as percentage_canceled,
                string_agg(distinct case when o.state ilike '%cancel%' then o.id else null end::text, ',') as order_ids_canceled
from orders o
inner join order_stores os on os.order_id = o.id
where o.created_at >= now() at time zone 'America/Bogota' - interval '30 minutes'
and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
and type like '%soat_webview%'
group by 1
having count(distinct case when o.state ilike '%cancel%' then o.id else null end) >= 3 and count(distinct case when o.state ilike '%cancel%' then o.id else null end)/count(distinct o.id) >= 0.1
order by 1
"""
	df = redash.run_query(1904, query)
	return df


def run_alarm(df1,df2):
	channel_alert, channel = timezones.slack_channels(country)
	current_time = timezones.country_current_time(country)
	print(df1)
	print(df2)

	df = pd.merge(df1, df2, how='left', left_on=df1['store_type'], right_on=df2['storetype'])
	df = df[(df['storetype'].isnull())]
	df = df.drop(['key_0', 'storetype'], axis=1)
	print(df)

	df['country'] = country
	df['alarm_at'] = current_time
	snow.upload_df_occ(df, 'soat_distincts_cancellations')

	for index, row in df.iterrows():
		rows = dict(row)
		text = '''
		*Alarma - Volumen de cancelación de la tienda SOAT - Colombia: alert:*
		Country :flag-{country}:
		Pais: {country}
		Las tiendas SOAT alcanzaron {total_canceled} cancelaciones de un total de {total_orders} pedidos en los últimos 15 minutos 
		Porcentaje de cancelaciones: {percentage_canceled}%
		Order IDs cancelado: {order_ids_canceled}
		'''.format(
			total_canceled = row['total_canceled'],
			total_orders = row['total_orders'],
			percentage_canceled=round(row['percentage_canceled']*100,2),
			order_ids_canceled = row['order_ids_canceled'],
			country = country
			)
		print(text)
		slack.bot_slack(text,channel_alert)

country = 'co'
df2 = logs(country)
df1 = soat_cancellations(country)
if df1.empty:
	sys.exit(0)
run_alarm(df1,df2)