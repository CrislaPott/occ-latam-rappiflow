import pandas as pd
import os
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from os.path import expanduser

def stockout(country):
    timezone, interval = timezones.country_timezones(country)
    current_time = timezones.country_current_time(country)
    channel_alert = timezones.slack_channels('trend_alarms')

    query_so = f"""
    --no_cache
with base_stockout as (
    select o.order_id
         , o.store_id as store_id
         , op.product_id
         , p.name
         , (case
                when
                    op.status in ('removed', 'replaced')
                    then 1
                else 0 end) as stock_out

    from co_pg_ms_cpgops_orders_ms_public.order_summary o
    join co_pg_ms_cpgops_orders_ms_public.order_products op on op.order_id = o.order_id
    join co_pglr_ms_grability_products_public.products_vw p on p.id = op.product_id

    where coalesce(o.place_at, op.updated_at, o.updated_at)::date >= convert_timezone('America/Bogota', current_timestamp())::date
    group by 1,2,3,4,5
)
, final as (
select sb.id brand_id, sb.name as brand_name,
         bs.product_id, max(p.name) as product_name, max(balance_price) as balance_price, max(price) as price, v.vertical,
         count(distinct case when stock_out >=1 then order_id else null end) as orders_so,
         count(distinct order_id) as all_orders,
         orders_so/all_orders as percentage_so,
         listagg(distinct bs.store_id::text,',') as store_ids
from base_stockout bs
join co_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
join co_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
join (select store_type as storetype,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type = 'turbo' then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.co_VERTICALS_V2 where store_type not in ('soat_webview','turbo','ECOMMERCE')) v on v.storetype = s.type
join co_pglr_ms_grability_products_public.products_vw p on p.id = bs.product_id and sale_type not in ('WW')
join co_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=bs.store_id
left join co_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
left join co_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag

where
 p.name not ilike '%almeirão %' and p.name not ilike '%almeirao %'
and p.name not ilike '%alface %' and p.name not ilike '%repolho %'
and p.name not ilike '%couve-flor %' and p.name not ilike '%coentro %'
and p.name not ilike '%salsa %' and p.name not ilike '%coentro %'
and p.name not ilike '%cebolinha %' and p.name not ilike '%agrião %'
-- spanish
and p.name not ilike '%lechuga %' and p.name not ilike '%achicoria %'
and p.name not ilike '%coliflor %' and p.name not ilike '%cilantro %'
and p.name not ilike '%repollo %' and p.name not ilike '%berro %'
and (pg.name not in ('Verduras') or pg.name is null)
and v.vertical not in ('Ecommerce')
group by 1,2,3,7
having orders_so >= 4 and percentage_so >= 0.10)

,
pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM co_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
)
,

wh as (select retailer_product_id::text as productid
       from CPGS_SALESCAPABILITY.TBL_CORE_PRODUCTS_IDEAL_BASKET_BY_STORE_CP
       where lower(country_code)= 'co'
       and is_covered=true
       )

,
reported_products as (
    select distinct product_id as productid
    from ops_occ.product_brand_stockout
    where checked_at::date = convert_timezone('America/Bogota',current_timestamp())::date
    and country = 'co'
)
,
 pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join co_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        )

, final2 as (
select brand_id, brand_name, f.product_id, product_name, f.balance_price, f.price, f.vertical, orders_so, all_orders, percentage_so, b.SUPER_STORE_ID, ps.store_id as store_id, b.in_content
from final f
join co_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=f.product_id
join co_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=ps.store_id and lower(b.COUNTRY_CODE)= 'co'
where s.deleted_at is null and s.suggested_state=true
)

select brand_id,
       brand_name,
       case when f.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
       f.product_id,
       case when f.in_content=true and pscp.product_id is not null then pscp.store_product_id else f.product_id end as store_product_id,
       product_name,
       f.balance_price,
       f.price,
       vertical,
       orders_so,
       all_orders,
       percentage_so,
       CASE WHEN (SUPER_STORE_ID=-1 or pscp.product_id is null) then f.store_id else SUPER_STORE_ID end as store_ids
       from final2 f
       left join pscms on pscms.store_id::int = f.store_id::int and pscms.product_id::int = f.product_id::int
       left join pscp on pscp.store_id::int = f.store_id::int and pscp.product_id::int = f.product_id::int
       left join reported_products rp on rp.productid::int = f.product_id::int
       left join wh on wh.productid::int = f.product_id::int
       where rp.productid is null and wh.productid is null and vertical not in ('Ecommerce')
       """


    df = snow.run_query(query_so)
    df = df[~df["vertical"].isin(["Turbo"])]
    df['checked_at'] = current_time
    df['country'] = country
    df['store_id'] = 0
    to_upload = df
    to_upload = to_upload[["brand_id","brand_name","product_id","product_name","orders_so","all_orders","percentage_so","checked_at","country", "store_id", "store_ids"]]

    print("uploading df")
    snow.upload_df_occ(to_upload, "product_brand_stockout")

    df_cpgs = df[df['vertical'].isin(['Express','Farmacia','Licores','Mercados'])]

    print(df_cpgs)

    if not df_cpgs.empty:
        df_cpgs = df_cpgs[["brand_id","brand_name","vertical","store_ids","product_plataform","product_id","store_product_id","product_name","balance_price","price","orders_so","all_orders","percentage_so"]]
        home = expanduser("~")
        results_file1 = '{}/details_cpgs{}.xlsx'.format(home, country)
        df_cpgs.to_excel(results_file1)
        text = f'''
        *Alarma Product/Brand - Stockout :alert:*
        *Vertical Group CPGs*
        :flag-{country}:
        Pais: {country}
        Nuevos productos alcanzaron el threshold de stockout
        '''
        print(text)
        slack.file_upload_channel(channel_alert, text, results_file1, 'xlsx')

    else:
        pass

countries = ['co']
for country in countries:
    print(country)
    try:

        stockout(country)
    except Exception as e:
        print(e)
