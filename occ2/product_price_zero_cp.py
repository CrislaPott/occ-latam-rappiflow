from lib import snowflake as snow
from lib import slack
from functions import timezones
import pandas as pd
from os.path import expanduser

def logs(country):
  timezone, interval = timezones.country_timezones(country)
  query = '''
  select distinct  case when product_id is not null then product_id::int else 0 end as productid
, case when store_id is not null then store_id::int else 0 end as storeid
, case when PRODUCT_ID_ELASTIC is not null then product_id_elastic::int else 0 end as productidelastic
, case when store_id_elastic is not null then store_id_elastic::int else 0 end as storeidelastic 
from ops_occ.product_price_zero_cp cp
  where country_code = '{country}'
  and alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  '''.format(country=country.upper(), timezone=timezone)
  df = snow.run_query(query)
  return df

def products_precio(country):
    if ((country.upper() == 'CO') | (country.upper() == 'MX') | (country.upper() == 'BR')):

        cp = f'''with CP as (
SELECT  	'{country.upper()}' AS country_code,
			ps.id as product_store_id,
            vp.id as virtual_product_id,
            vp.product_id as product_id,
            vs.id as virtual_store_id,
			s.id as store_id,
			CASE WHEN DC.STORE_ID IS NULL THEN 'NO_CASHLESS' ELSE 'CASHLESS' END AS delivery_condition,
            ps.price as price,
            ps.price as balance_price,
            ps.RETAILER_MARKDOWN,
            V."GROUP" as vertical_group,
            CA.CITY AS CITY,
            r.name AS brand_name,
            vs.store_type,
            p.sell_type:type as sale_type,
            p.name,
			CASE WHEN ps.status = 'published' THEN TRUE ELSE FALSE END AS is_available,
            case when ps.price <=0 or ps.price is null or  ps.RETAILER_MARKDOWN<-100 then 'Price 0 CP'
            end as Alerta,
           op.order_id as order_id,
           o.CREATED_AT as created_at

FROM {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.product ps
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.store s on s.id = ps.store_id
                                       AND NOT COALESCE(s._fivetran_deleted, FALSE)
                                       AND s.status='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.retailer r ON r.id = s.retailer_id
                                       AND NOT COALESCE(r._fivetran_deleted, FALSE)
                                       AND r.status='published'
JOIN {country.upper()}_pglr_ms_cpgs_clg_pm_public.RETAILER_PRODUCT rp ON rp.id = ps.retailer_product_id
                                       AND NOT COALESCE(rp._fivetran_deleted, FALSE)
                                       AND rp.status ='published'
JOIN {country.upper()}_pglr_ms_cpgs_clg_pm_public.product p ON p.id = rp.product_id
                                       AND NOT COALESCE(p._fivetran_deleted, FALSE)
                                       AND p.status ='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_store vs on vs.store_id = s.id
                                       AND NOT COALESCE(vs._fivetran_deleted, FALSE)
                                      -- AND vs.status='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.virtual_product vp ON vp.product_id = ps.id
                                       AND vp.virtual_store_id = vs.id
                                       AND NOT COALESCE(vp._fivetran_deleted, FALSE)
                                      -- AND vp.status='published'
JOIN {country.upper()}_PGLR_MS_STORES_PUBLIC.stores_VW ST ON ST.STORE_ID=VS.ID AND NOT ST._FIVETRAN_DELETED AND ST.IS_ENABLED
LEFT JOIN
      (
        select distinct store_id
          from (
            select distinct conditionable_id as store_id
            from {country.upper()}_PGLR_MS_STORES_PUBLIC.delivery_conditions dc
            where type = 'cashless_required' and params:cashless = 'true' and not _fivetran_deleted
            union all
            select distinct conditionable_id as store_id
            from {country.upper()}_PGLR_MS_STORES_PUBLIC.delivery_conditions dc
            where type = 'no_pay' and params:no_pay = 'true' and not _fivetran_deleted
          )
      ) DC ON DC.STORE_ID=ST.STORE_ID

JOIN {country.upper()}_PGLR_MS_STORES_PUBLIC.VERTICALS V ON V.STORE_TYPE=ST.TYPE AND NOT V._FIVETRAN_DELETED
LEFT JOIN {country.upper()}_grability_public.city_addresses_VW CA ON CA.ID=ST.CITY_ADDRESS_ID AND NOT CA._fivetran_deleted
JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.store_id = vs.id
                             AND LS.country_code = '{country.upper()}'
  left join {country.upper()}_core_orders_public.order_product_vw op on op.product_id = ps.id  and op.store_id = ps.store_id
    left join {country.upper()}_CORE_ORDERS_PUBLIC.orders_vw o on o.id = op.order_id


WHERE NOT COALESCE(ps._fivetran_deleted, FALSE)
      AND V."GROUP" ilike any ('%CPGs%','%ecommerce%')
      AND ps.status ='published'
      and LS.IN_CONTENT=TRUE
      and not (p.name ilike any ('%rappi%','%favor%','%soat%'))
	  and Alerta is not null
     and op.order_id is not null
    and o.created_at::date >= current_date),'''

    else:
        cp = f'''with CP as (SELECT  	'{country.upper()}' AS country_code,
			ps.id as product_store_id,
            vp.id as virtual_product_id,
            vp.product_id as product_id,
            vs.id as virtual_store_id,
			s.id as store_id,
			CASE WHEN DC.STORE_ID IS NULL THEN 'NO_CASHLESS' ELSE 'CASHLESS' END AS delivery_condition,
            ps.price as price,
            ps.price as balance_price,
            ps.RETAILER_MARKDOWN,
            V."GROUP" as vertical_group,
            CA.CITY AS CITY,
            r.name AS brand_name,
            vs.store_type,
            p.sell_type:type as sale_type,
            p.name,
			CASE WHEN ps.status = 'published' THEN TRUE ELSE FALSE END AS is_available,
            case when ps.price <=0 or ps.price is null or  ps.RETAILER_MARKDOWN<-100 then 'Price 0 CP'
            end as Alerta,
           op.order_id as order_id,
           o.CREATED_AT as created_at

FROM {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.product ps
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.store s on s.id = ps.store_id
                                       AND NOT COALESCE(s._fivetran_deleted, FALSE)
                                       AND s.status='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.retailer r ON r.id = s.retailer_id
                                       AND NOT COALESCE(r._fivetran_deleted, FALSE)
                                       AND r.status='published'
JOIN {country.upper()}_pglr_ms_cpgs_clg_pm_public.RETAILER_PRODUCT rp ON rp.id = ps.retailer_product_id
                                       AND NOT COALESCE(rp._fivetran_deleted, FALSE)
                                       AND rp.status ='published'
JOIN {country.upper()}_pglr_ms_cpgs_clg_pm_public.product p ON p.id = rp.product_id
                                       AND NOT COALESCE(p._fivetran_deleted, FALSE)
                                       AND p.status ='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_store vs on vs.store_id = s.id
                                       AND NOT COALESCE(vs._fivetran_deleted, FALSE)
                                      -- AND vs.status='published'
JOIN {country.upper()}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY.virtual_product vp ON vp.product_id = ps.id
                                       AND vp.virtual_store_id = vs.id
                                       AND NOT COALESCE(vp._fivetran_deleted, FALSE)
                                      -- AND vp.status='published'
JOIN {country.upper()}_PGLR_MS_STORES_PUBLIC.stores_VW ST ON ST.STORE_ID=VS.ID AND NOT ST._FIVETRAN_DELETED AND ST.IS_ENABLED
LEFT JOIN
      (
        select distinct store_id
          from (
            select distinct conditionable_id as store_id
            from {country.upper()}_PGLR_MS_STORES_PUBLIC.delivery_conditions dc
            where type = 'cashless_required' and params:cashless = 'true' and not _fivetran_deleted
            union all
            select distinct conditionable_id as store_id
            from {country.upper()}_PGLR_MS_STORES_PUBLIC.delivery_conditions dc
            where type = 'no_pay' and params:no_pay = 'true' and not _fivetran_deleted
          )
      ) DC ON DC.STORE_ID=ST.STORE_ID

JOIN {country.upper()}_PGLR_MS_STORES_PUBLIC.VERTICALS V ON V.STORE_TYPE=ST.TYPE AND NOT V._FIVETRAN_DELETED
LEFT JOIN {country.upper()}_grability_public.city_addresses_VW CA ON CA.ID=ST.CITY_ADDRESS_ID AND NOT CA._fivetran_deleted
JOIN CPGS_DATASCIENCE.LOOKUP_STORE LS ON LS.store_id = vs.id
                             AND LS.country_code = '{country.upper()}'
left join {country.upper()}_core_orders_public.order_product_vw op on op.product_id = ps.id  and op.store_id = ps.store_id
left join {country.upper()}_CORE_ORDERS_PUBLIC.orders_vw o on o.id = op.order_id


WHERE NOT COALESCE(ps._fivetran_deleted, FALSE)
      AND V."GROUP" ilike any ('%CPGs%','%ecommerce%')
      AND ps.status ='published'
      and LS.IN_CONTENT=TRUE
      and not (p.name ilike any ('%rappi%','%favor%','%soat%'))
	  and Alerta is not null
     and op.order_id is not null
    and o.created_at::date >= current_date),'''

    query = cp + f'''
elastic as (SELECT '{country.upper()}' AS country_code,
           elc.STORE_ID AS STORE_ID_ELASTIC,
           elc.PRODUCT_ID AS PRODUCT_ID_ELASTIC,
           CASE WHEN ELC.DISCOUNT=1 THEN 0 ELSE ROUND(ELC.PRICE/(1-ELC.DISCOUNT),2) END AS FULL_PRICE_ELASTIC,
           ELC.PRICE as ELASTIC_PRICE,
           ELC.DISCOUNT AS DISCOUNT_ELASTIC,
           ELC.STORE_TYPE AS STORE_TYPE_ELASTIC,
           ELC.NAME AS PRODUCT_NAME_ELASTIC,
           ELC.TIMESTAMP AS TIMESTAMP_ELASTIC,
           ELC.METADATA,
           'Price 0 ELASTIC'as Alerta,
           op.order_id as order_id,
           o.CREATED_AT as created_at
    FROM CPGS_DATASCIENCE.CPGS_ELASTIC_CONTENT ELC
    left join {country.upper()}_core_orders_public.order_product_vw op on op.product_id = elc.product_id and op.store_id = elc.store_id
    left join {country.upper()}_CORE_ORDERS_PUBLIC.orders_vw o on o.id = op.order_id
    WHERE ELC.COUNTRY_CODE='{country.upper()}' AND ELC.IS_AVAILABLE AND NOT ELC.IS_DISCONTINUED
    AND (ELC.PRICE<=0 or ELC.PRICE is null or ELC.DISCOUNT>1)
    and (ELC.STORE_TYPE not in ('courier_hours'))
    AND ELC.METADATA='newCatalog'
    and op.order_id is not null
    and o.created_at::date >= current_date)

select * from elastic
left join CP on CP.country_code = elastic.country_code
where product_name_elastic not ilike '%Tarjeta RappiCard%' '''

    df = snow.run_query(query)
    return df

def alarm(df):
    channel_alert = timezones.slack_channels('trend_alarms')
    current_time = timezones.country_current_time(country)

    if not df.empty:
        a = logs(country)
        a['productidelastic']=a['productidelastic'].astype(str)
        a['storeidelastic'] = a['storeidelastic'].astype(str)
        df['product_id_elastic'] =df['product_id_elastic'].astype(str)
        df['store_id_elastic'] = df['store_id_elastic'].astype(str)
        df = df.merge(a, left_on=['product_id_elastic', 'store_id_elastic'], right_on=['productidelastic', 'storeidelastic'], how='left')
        df = df[(df['productidelastic'].isnull())]
        df = df.drop(['productidelastic','storeidelastic','productid', 'storeid'], axis=1)

        a['productid'] = a['productid'].astype(str)
        a['storeid'] = a['storeid'].astype(str)
        df['product_id'] = df['product_id'].astype(str)
        df['store_id'] = df['store_id'].astype(str)
        df = df.merge(a, left_on=['product_id', 'store_id'],right_on=['productid', 'storeid'], how='left')
        df = df[(df['productid'].isnull())]
        df = df.drop(['productid', 'storeid','productidelastic','storeidelastic'], axis=1)

        df1= df.iloc[:, :13]
        df2 = df.iloc[:, 13:]

        df = df1.append([df1, df2],ignore_index=True)
        df = df[(df['country_code'].notnull())]

        df['timestamp_elastic'] = pd.to_datetime(df['timestamp_elastic'], errors='coerce').dt.tz_localize(None)
        df['created_at'] = pd.to_datetime(df['created_at'], errors='coerce').dt.tz_localize(None)
        df = df.drop_duplicates()
        print(df)
        if not df.empty:
            to_upload = df
            to_upload['alarm_at'] = current_time

            snow.upload_df_occ(to_upload, 'product_price_zero_cp')
            to_upload['alarm_at'] = to_upload['alarm_at'].dt.tz_localize(None)

            text = '''*Pedidos con products con precio cero en CP :rotating_light:*
    Country: :flag-{country}:
    Los pedidos en adjunto tuvieran productos con precio :zero:
    '''.format(country=country)
            print(text)
            home = expanduser("~")
            results_file = '{}/details_precio_cero_CP_{country}.xlsx'.format(home, country=country)

            df.to_excel(results_file, sheet_name='products', index=False)
            slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
    else:
        print('null')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
  try:
    print(country)
    df = products_precio(country)
    alarm(df)
  except Exception as e:
    print(e)

