from lib import redash, slack
from lib import snowflake as snow
import pandas as pd
from functions import timezones
import snowflake.connector
from dotenv import load_dotenv
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from airflow.models import Variable

load_dotenv()

def run_query(query):
    """Run a select query on Snowflake."""
    conn = snowflake.connector.connect(
        host=Variable.get('SNOWFLAKE_HOST'),
        user=Variable.get('SNOWFLAKE_USER'),
        password=Variable.get('SNOWFLAKE_PASSWORD'),
        account=Variable.get('SNOWFLAKE_ACCOUNT'),
        database='FIVETRAN',
        role='RESTAURANTES_INTEGRATED_STORES_write_role',
        warehouse='RESTAURANTS',
        timezone='America/Recife'

    )
    cur = conn.cursor()
    cur.execute(query)
    df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
    conn.close()
    df.columns = [x.lower() for x in df.keys()]
    return df


def integrations(country):
    timezone, interval = timezones.country_timezones(country)
    query_orders = f'''
    --no_cache
    with base as (
        SELECT DISTINCT ORDER_ID, created_at
        FROM ORDER_MODIFICATIONS om
        WHERE om.created_AT >= now() AT TIME ZONE 'America/{timezone}' - interval '4h'
        and om.type in ('refuse_order_by_partner','remove_by_partner_inactivity','refuse_partner_after_take','cancel_by_restaurant_integration')
        )
    SELECT '{country}' COUNTRY
        ,O.ID
        ,O.STATE
        ,O.CLOSED_AT
        , O.ID as order_id_o
        , S.STORE_ID 
        , b.ORDER_ID
        ,CASE 
            WHEN NOT b.ORDER_ID IS NULL
                THEN 1
            ELSE 0
            END INTEGRATION_ERROR
    FROM ORDERS O
    LEFT JOIN ORDER_STORES S ON O.ID = S.ORDER_ID
    LEFT join base b on b.order_id = O.ID
    where O.created_AT >= now() AT TIME ZONE 'America/{timezone}' - interval '4h'
    and s.type = 'restaurant'
    
    '''
    query_integrations = f'''with integration as (
    select * from integrations si
    where si.last_scan >= now() AT TIME ZONE 'America/{timezone}' - interval '4h'
    and si.country = '{country}'
    and SI.IS_ACTIVE
    ),
    brand as (
    select * from brands
    where last_scan >= now() AT TIME ZONE 'America/{timezone}' - interval '4h'
    and country = '{country}'
    ),
    store as (
    select * from stores sd
    where country = '{country}'
    and SD.IS_ACTIVE
    )
    select * from store sd
    join integration si on si.country = sd.country and si.id = sd.integration_id
    join brand b on b.country = sd.country and B.ID = SD.BRAND_ID'''

    if country == 'co':
        ordersl15 = redash.run_query(1904, query_orders)
        df_integrations = redash.run_slow_query(5561,query_integrations)
    elif country == 'ar':
        ordersl15 = redash.run_query(1337, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'cl':
        ordersl15 = redash.run_query(1155, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'mx':
        ordersl15 = redash.run_query(1371, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'uy':
        ordersl15 = redash.run_query(1156, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'ec':
        ordersl15 = redash.run_query(1922, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'cr':
        ordersl15 = redash.run_query(1921, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'pe':
        ordersl15 = redash.run_query(1157, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)
    elif country == 'br':
        ordersl15 = redash.run_query(1338, query_orders)
        df_integrations = redash.run_slow_query(5561, query_integrations)

    return ordersl15, df_integrations

def last_weeks(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''WITH BASE
    AS (
        SELECT O.COUNTRY
            ,O.CLOSED_AT
            ,S.BRAND_ID
            ,S.BRAND_NAME
            ,COUNT(*) TOTAL_ORDERS
            ,COUNT(CASE 
                    WHEN NOT CANCELLATION_SUBTYPES IS NULL
                        THEN ORDER_ID
                    ELSE NULL
                    END) CANCELED_ORDERS
        FROM RESTAURANTES_INTEGRATED_STORES.ORDERS O
        LEFT JOIN RESTAURANTES_INTEGRATED_STORES.INTEGRATED_STORES S ON O.COUNTRY = S.COUNTRY
            AND O.STORE_ID = S.STORE_ID
            and  O.COUNTRY = '{country.upper()}'
            and O.CLOSED_AT::date >= dateadd(week,-8,current_date)
            and o.CLOSED_TIME >= to_char((convert_timezone('America/{timezone}',current_timestamp())) - interval '4h', 'HH24:MI')
            and o.CLOSED_TIME <= to_char((convert_timezone('America/{timezone}',current_timestamp())), 'HH24:MI')
        GROUP BY 1
            ,2
            ,3,4
        )
        ,BASE1
    AS (
        SELECT COUNTRY
            ,CLOSED_AT
            ,BRAND_ID
            ,BRAND_NAME
            ,(CANCELED_ORDERS / TOTAL_ORDERS) CANCEL_RATE
        FROM BASE
        )
    SELECT COUNTRY
        ,BRAND_ID
        ,BRAND_NAME
        ,(STDDEV(CANCEL_RATE)) SD
        ,AVG(CANCEL_RATE) AVG1
    FROM BASE1
    GROUP BY 1,2, 3'''

    df_l8w = run_query(query)

    return df_l8w

def alarm(ordersl15,df_integrations,df_l8w):

    df_l8w['country'] = df_l8w['country'].str.lower()

    df = pd.merge(ordersl15,df_integrations, how='left',left_on=ordersl15['store_id'],right_on=df_integrations['id'])
    df = df[['country_x', 'state', 'closed_at', 'store_id','order_id_o','order_id','integration_error', 'brand_id', 'integration_id','name', 'integration_type', 'integrated_at', 'allied_integration_type']].rename(columns={'country_x':'country'})
    stores = df[['order_id','store_id']].dropna().astype(str).set_index('order_id').to_dict()['store_id']
    print(stores)

    df1 = df.groupby(['country','brand_id'])['order_id_o'].agg(['count']).reset_index().rename(columns={'count':'total_orders'})
    df2 = df.groupby(['country','brand_id'])['order_id'].agg(['count']).reset_index().rename(columns={'count':'integration_error'})
    df2['integration_error']=df2['integration_error'].fillna(0)

    df3 = df.groupby(['country', 'brand_id'])['order_id'].apply(list).reset_index(name='cancels')
    df3['cancels'] = df3['cancels'].apply(lambda x: [i for i in x if str(i) != "nan"]).astype(str)
    print(df3)

    df_final = pd.merge(df1,df2, how='left',on=['brand_id'])
    df_final = pd.merge(df_final, df3, how='left', on=['brand_id'])

    df_final = df_final[['country_x', 'brand_id', 'total_orders', 'integration_error', 'cancels']].rename(columns={'country_x':'country'})

    df_final['integration_error'] = df_final['integration_error'].fillna(0)
    df_final['integration_cancel'] = df_final['integration_error']/df_final['total_orders']
    print(df_final.sort_values(by='integration_cancel'))

    df_final['brand_id'] = df_final['brand_id'].astype(int)
    df = pd.merge(df_final,df_l8w,how='left', left_on=['country','brand_id'], right_on=['country','brand_id'])
    print(df)
    df['sd'] = df['sd'].astype(float)
    df['avg1'] = df['avg1'].astype(float)
    df['delta'] = (df['integration_cancel'] - df['avg1'])
    df['sigma'] = df['delta']/df['sd']


    df = df[(df['sigma'] > 1) & (df['integration_error'] > 6) & (df['integration_cancel'] > 0.015)]
    if not df.empty:

        df['store_ids'] = pd.Series()

        upload = df[['country', 'total_orders', 'integration_error', 'cancels','store_ids', 'integration_cancel', 'brand_id', 'brand_name','sd', 'avg1', 'sigma']]
        upload['alarm_at'] = timezones.country_current_time(country)
        upload['sigma'] = upload['sigma'].astype(str)
        snow.upload_df_occ(upload, 'integrations_restaurants_brand')

        for index, row in df.iterrows():
            orders_ids = row['cancels'].replace('[','').replace(']','')
            orders_ids_list = orders_ids.split(', ')
            print(orders_ids_list)
            store_ids = [x if x not in stores else stores[x] for x in orders_ids_list]
            print(store_ids)
            store_ids = [x for i, x in enumerate(store_ids) if i == store_ids.index(x)]
            print(store_ids)
            text = '''
            :alert: *Alarma Restaurantes Integrations/Brands* :flag-{country}:
            Brand: {brand_name} - {brand_id}
            Order IDs: {cancels}
            Store Ids: {store_ids}
            CR D0: {integration_cancel}% (Total Orders: {total_orders}, Integration Error: {integration_error})
            AVG CR L8W: {avg1}
            SD CR L8W: {sd}
            Delta (CR D0 - AVG CR L8W): {delta}
            Sigma (Delta/SD CR L8W): {sigma}
             '''.format(country= country,
                        brand_id= row['brand_id'],
                        brand_name= row['brand_name'],
                        integration_cancel=round(row['integration_cancel'],2) * 100,
                        total_orders=row['total_orders'],
                        store_ids=store_ids,
                        integration_error=row['integration_error'],
                        cancels=row['cancels'],
                        delta=round(row['delta'],2),
                        avg1=round(row['avg1'],2),
                        sd=round(row['sd'],2),
                        sigma= round(row['sigma'],2)
                        )

            print(text)
            slack.bot_slack(text,'C037P6DNG2H')
    else:
        text = '''
        :alert: *Alarma Restaurantes Integrations/Brands* :flag-{country}:
        Sin nuevos Restaurantes para actuar
        '''.format(country= country)
        print(text)
        slack.bot_slack(text,'C037P6DNG2H')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']

for country in countries:
  try:
      print(country)
      df_l8w = last_weeks(country)
      ordersl15, df_integrations = integrations(country)
      alarm(ordersl15,df_integrations,df_l8w)
  except Exception as e:
      print(e)
