import os, sys, json
import pandas as pd
import gspread
from df2gspread import df2gspread as d2g
from oauth2client.service_account import ServiceAccountCredentials
from lib import slack, redash
from lib import snowflake as snow
from datetime import datetime, timedelta
from dotenv import load_dotenv

load_dotenv()

def orders(country):
	if country == 'co' or country == 'pe' or country == 'ec':
		timezone = 'Bogota'
	elif country == 'cr' or country == 'mx':
		timezone = 'Costa_Rica'
	else:
		timezone = 'Buenos_Aires'

	if country in ['co']:
		lista = '1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,19,20,21,22,23,24,25,26,33,38,40,43,51,52,55,62,68,72,106,109,110,152,153,199,200,238,243,254,275,311,337,392,445,7125,7414,7876,7877,7957,12965'
	elif country in ['br']:
		lista = '19,38,40,46,50,264,265,276,278,281,284,286,302,304,310,311,313,335,340,345,349,360,369,384,392,407,433,462,467,468,503,508,514,520,524,525,532,537,538,576,582,626,642,649,696,700,720,726,7571,7577,7578,7582,7583,7592,7593,7597,7600,7601,7602,7605,7610,7612,7842,7876,7907,7923,7924,7925,7947,7948,7952,7956,7963,8001,8047,8157,8162,8188,8396,8397,8706,8717,8718,8723,8724,8725,8726,8727,8730,8762,8764,8778,8780,8781,8784,8785,8786,8787,8789,8791,8793,8813,8816,8819,8820,8821,8822,8823,8833,8868,8874,9130,9131,9132,9133,9134,9135,9137,9139,9140,9160,9177,9178,9180,9181,9205,9215,9218,9221,9222,9223,9224,9225,9266,9300,9301,9417,9420,9426,9454,9504,9514,9536,9552,9594,9597,9641,9649,9658,9710,9741,10076,10332,10464,10542,11087,11088,11260,11274,11277,11278,11374,11679,11889,12047,12106,12147,12246,12823,12824,13179,13199,13240,13313,13317,13720,13721,13722,13788,13789,13820,13981,14224,14244,14246,14257,14258,14259,14260,14261,14263,14264,14265,14266,14267,14269,14271,14363,14394,14395,14409,14498,14507,14515,14518,14519,14750,14768,14768,14769,14770,14849,14903,14909,15112,15152,15154,15205,15243,15272,15274,15736,15772,15794,15990,16012,16014,16020,16025,32043,32139,32140,32141,32251,32252,32470,32471,32803,32804,32805,32806,32807,32831,32832,32833,32898,33174,33175,33176,33177,33178,33179,33180,33181,33182,33183,33184,33185,33186,33187,33188,33189,33190,33191,33192,33356,33357,33358,33360,33361,33364,33365,33366,33367,33368,33409,33410,33411,33412,33413,33414,33415,33416,33417,33418,33419,33420'
	elif country in ['mx']:
		lista = '2,3,4,6,7,8,10,11,12,13,15,17,18,19,22,23,25,26,27,28,33,35,48,49,53,62,76,77,78,79,158,159,167,168,169,170,171,172,173,174,175,176,200,201,202,236,237,242,442,443,444,445,446,447,448,449,450,452,456,457,461,462,500,502,606,608,627,628,630,1102,1107,1112,1123,1135,1140,1478,1544,1545,1546,1547,1563,1568,1572,1573,1712,1714,1715,1716,17191,17196,17320,17438,17445,17447,17449,17474,17658,17668,17737,18240,18682,20180,20382,20611,20970,21334,22454,22822,23898'
	elif country in ['pe']:
		lista = '9,44,61,66,68,138,180,183,193,205,216,222,224,239,242,244,263,265,2736,2804,3014,3016,3405,3409,3410,3419,3462,3463,3524,3525'
	elif country in ['cl']:
		lista = '12,14,15,18,28,38,39,45,56,81,115,207,241,265,290,292,312,317,346,353,354,355,361,3674,3709,3873,3884,3904,3999,4003,4017,4031,4046,4053,4298,4345,4473,4525,4771,5027,5028,5128,5129,5130,5131,5132,5133,5134,5135,5136,5469,54704'
	elif country in ['ar']:
		lista = '2,3,7,8,9,10,35,36,37,38,39,40,41,42,43,44,45,48,50,51,52,53,55,62,63,64,641,642,644,743,753,756,784,790,878,920,921,953,954,955,957,960,961,982,983,1002,1003,1004,1005,1057,1117,1350,1351,1526,1527,1531,1872,1998,2060,2061,2091,2098'
	elif country in ['ec']:
		lista = '22,23,24,25,26,28,29,30,31,32,34,62,76,101,119,127,152,153,166,167,230,231'
	elif country in ['cr']:
		lista = '14,1,28,65,12,82,71,74,148,149,146,144,145,147'
	elif country in ['uy']:
		lista = '13193'

	query = '''
	--no_cache
	select
	physical_store_id,
	shopper_id,
	os.order_id
	from order_summary os
	where state in ('assigned', 'in_picking')
	and (place_at AT TIME ZONE 'America/{timezone}')::date = date(now() AT TIME ZONE 'America/{timezone}')
	and physical_store_id in ({lista})
	group by 1,2,3
	'''.format(timezone=timezone,lista=lista)

	if country == 'co':
		metrics = redash.run_query(2453, query)
	elif country == 'ar':
		metrics = redash.run_query(2427, query)
	elif country == 'cl':
		metrics = redash.run_query(2428, query)
	elif country == 'mx':
		metrics = redash.run_query(2454, query)
	elif country == 'uy':
		metrics = redash.run_query(2429, query)
	elif country == 'ec':
		metrics = redash.run_query(2555, query)
	elif country == 'cr':
		metrics = redash.run_query(2551, query)
	elif country == 'pe':
		metrics = redash.run_query(2452, query)
	elif country == 'br':
		metrics = redash.run_query(2451, query)

	return metrics

def slots(country):
	if country == 'co' or country == 'pe' or country == 'ec':
		timezone = 'Bogota'
	elif country == 'cr' or country == 'mx':
		timezone = 'Costa_Rica'
	else:
		timezone = 'Buenos_Aires'
	query = '''
--no_cache
WITH a AS
  (SELECT order_id,
		  max(created_at) AS created_at
   FROM slots_orders so
   WHERE place_at >= now() - interval '1h'
   and place_at <= now() + interval '3h'
   and deleted_at is null
  and type in ('released_to_shopper','order_to_sh','aggregate')
   GROUP BY 1),
	 b AS
  (SELECT physical_store_id,
		  last_value(id) over (partition by physical_store_id order by (datetime_utc_iso AT TIME ZONE 'America/{timezone}') asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_slot,
		  last_value((datetime_utc_iso AT TIME ZONE 'America/{timezone}')) over (partition by physical_store_id order by (datetime_utc_iso AT TIME ZONE 'America/{timezone}') asc ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as hour
   FROM slots
   WHERE datetime_utc_iso >= now() - interval '1h'
   and datetime_utc_iso <= now() + interval '3h'
   and deleted_at is null
   )

SELECT
physical_store_id, hour, so.order_id
FROM slots_orders so
INNER JOIN a ON a.created_at=so.created_at AND a.order_id=so.order_id
INNER JOIN b s ON s.last_slot=so.slots_id
where deleted_at is null
group by 1,2,3
   '''.format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(1971, query)
	elif country == 'ar':
		metrics = redash.run_query(1968, query)
	elif country == 'cl':
		metrics = redash.run_query(1970, query)
	elif country == 'mx':
		metrics = redash.run_query(1972, query)
	elif country == 'uy':
		metrics = redash.run_query(1974, query)
	elif country == 'ec':
		metrics = redash.run_query(2556, query)
	elif country == 'cr':
		metrics = redash.run_query(2558, query)
	elif country == 'pe':
		metrics = redash.run_query(1973, query)
	elif country == 'br':
		metrics = redash.run_query(1969, query)

	return metrics

def slot_offset(country):
	if country == 'co' or country == 'pe' or country == 'ec':
		timezone = 'Bogota'
	elif country == 'cr' or country == 'mx':
		timezone = 'Costa_Rica'
	else:
		timezone = 'Buenos_Aires'
	query = '''
	select physical_stores_id, slot_offset 
from slot_configs_physical_stores
   '''.format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(1971, query)
	elif country == 'ar':
		metrics = redash.run_query(1968, query)
	elif country == 'cl':
		metrics = redash.run_query(1970, query)
	elif country == 'mx':
		metrics = redash.run_query(1972, query)
	elif country == 'uy':
		metrics = redash.run_query(1974, query)
	elif country == 'ec':
		metrics = redash.run_query(2556, query)
	elif country == 'cr':
		metrics = redash.run_query(2558, query)
	elif country == 'pe':
		metrics = redash.run_query(1973, query)
	elif country == 'br':
		metrics = redash.run_query(1969, query)

	return metrics

def empty_df_to_gsheets(df,country):
	scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
	credentials = ServiceAccountCredentials.from_json_keyfile_name('jsonFileFromGoogle.json', scope)
	gc = gspread.authorize(credentials)
	spreadsheet_key = '1Nt1zR-I0PRmI4_XvmJnkvQvAIV8SKYXZEfF261TL3WE'
	wks_name = country.upper()
	d2g.upload(df,spreadsheet_key, wks_name, credentials=credentials,row_names=False)
	current_time_co = datetime.utcnow() - timedelta(hours=5)
	current_time_co = current_time_co.strftime("%Y-%m-%d %H:%M")
	spreadsheet = gc.open_by_key(spreadsheet_key)
	panel = spreadsheet.get_worksheet(0)
	if country =='ar':
		cell = 'B17'
	elif country =='br':
		cell = 'B18'
	elif country =='cl':
		cell = 'B19'
	elif country =='co':
		cell = 'B20'
	elif country =='cr':
		cell = 'B21'
	elif country =='ec':
		cell = 'B22'
	elif country =='mx':
		cell = 'B23'
	elif country =='pe':
		cell = 'B24'
	elif country =='uy':
		cell = 'B25'    
	panel.update(cell, current_time_co)

def to_gsheets(dff,country):
	scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
	credentials = ServiceAccountCredentials.from_json_keyfile_name('jsonFileFromGoogle.json', scope)
	gc = gspread.authorize(credentials)
	spreadsheet_key = '1Nt1zR-I0PRmI4_XvmJnkvQvAIV8SKYXZEfF261TL3WE'
	wks_name = country.upper()
	dff.columns = [x.lower() for x in dff.keys()]
	spreadsheet = gc.open_by_key(spreadsheet_key)
	values = [dff.columns.values.tolist()]
	values.extend(dff.values.tolist())
	spreadsheet.values_update(wks_name, params={'valueInputOption': 'USER_ENTERED'}, body={'values': values})
	current_time_co = datetime.utcnow() - timedelta(hours=5)
	current_time_co = current_time_co.strftime("%Y-%m-%d %H:%M")
	panel = spreadsheet.get_worksheet(0)
	if country =='ar':
		cell = 'B17'
	elif country =='br':
		cell = 'B18'
	elif country =='cl':
		cell = 'B19'
	elif country =='co':
		cell = 'B20'
	elif country =='cr':
		cell = 'B21'
	elif country =='ec':
		cell = 'B22'
	elif country =='mx':
		cell = 'B23'
	elif country =='pe':
		cell = 'B24'
	elif country =='uy':
		cell = 'B25'    
	panel.update(cell, current_time_co)

def merge(a,b,c):
	try:
		df = pd.merge(a,b,how='inner',left_on=a['order_id'],right_on=b['order_id'],suffixes=('', '_DROP')).filter(regex='^(?!.*_DROP)')
		df = df.drop(['key_0'], axis=1)
		df = pd.merge(df,c,how='left',left_on=df['physical_store_id'],right_on=c['physical_stores_id'])
		df = df.drop(['key_0','physical_stores_id'], axis=1)
		df = df[['physical_store_id','hour','shopper_id','order_id','slot_offset']]
		
		if not df.empty:
			to_gsheets(df,country)
		else:
			column_names = ['physical_store_id','hour','shopper_id','order_id','slot_offset']
			df = pd.DataFrame(columns = column_names, index=range(1))
			print(df)
			empty_df_to_gsheets(df,country)
	except:
		column_names = ['physical_store_id','hour','shopper_id','order_id','slot_offset']
		df = pd.DataFrame(columns = column_names, index=range(1))
		print(df)
		empty_df_to_gsheets(df,country)

countries = ['uy','br','co','mx','ar','cl','cr','ec','pe']
for country in countries:
	print(country)
	try:
		a = orders(country)
		b = slots(country)
		c = slot_offset(country)
		merge(a,b,c)
	except Exception as e:
		print(e)