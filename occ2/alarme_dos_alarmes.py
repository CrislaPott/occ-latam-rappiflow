from lib import snowflake as snow
import numpy as np
import pandas as pd
from lib import redash, slack
from datetime import datetime, timedelta
from dotenv import load_dotenv
from os.path import expanduser

query = ''' with store_infos as (
        select 'br'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from br_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join br_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join br_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join br_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join br_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.br_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from br_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join br_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'co'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from co_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join co_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join co_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join co_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join co_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.co_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from co_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join co_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'mx'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from mx_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join mx_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join mx_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join mx_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join mx_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.mx_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from mx_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join mx_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all

        select 'cl'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from cl_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join cl_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join cl_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join cl_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join cl_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.cl_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from cl_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join cl_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'pe'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from pe_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join pe_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join pe_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join pe_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join pe_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.pe_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from pe_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join pe_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'ar'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from ar_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join ar_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join ar_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join ar_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join ar_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.ar_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from ar_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join ar_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all

        select 'cr'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from cr_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join cr_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join cr_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join cr_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join cr_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.cr_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from cr_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join cr_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'ec'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from ec_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join ec_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join ec_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join ec_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join ec_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.ec_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from ec_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join ec_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)

        union all
        select 'uy'                     as country,
               a.store_id               as store_id,
               a.name                   as store_name,
               a.type                   as store_type,
               c.id                     as brand_id,
               c.name                   as brand_name,
               a.city_address_id        as city_id,
               msc.city                 as city_name,
               v.vertical,
               brand_group_id           as brand_group_id,
               brand_group_name,
               cpgops.physical_store_id as physical_store_id,
               pss.name                 as physical_store_name
        from uy_PGLR_MS_STORES_PUBLIC.stores_vw a
                 left join uy_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                           on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                 left join uy_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                 left join uy_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                 left join uy_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                           on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                 inner join (select store_type                      as storetype,
                                    case
                                        when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                        when vertical_group = 'WHIM' then 'Antojos'
                                        when vertical_group = 'RAPPICASH' then 'RappiCash'
                                        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                        when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                        when upper(store_type) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                        when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                        when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                        when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                        when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                        when upper(vertical_group) in ('CPGS') then 'CPGs'
                                        else vertical_sub_group end as vertical
                             from VERTICALS_LATAM.uy_VERTICALS_V2
        ) v on v.storetype = a.type

                 left join (
            select *
            from (select store_brand_id,
                         last_value(brand_group_id)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
                         last_value(bg.name)
                                    over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
                  from uy_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                           join uy_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
            group by 1, 2, 3
        ) bg on bg.store_brand_id = a.store_brand_id

        where not coalesce(a._fivetran_deleted, false)
    )
    select wod.country,
           'Top Offenders Tier AB'     as alarm_type,
           null                        as product_id,
           null                        as store_id,
           null,
           wod.STORE_ID                as physical_store_id,
           psi.physical_store_name,
           null                        as city_id,
           null                        as city_name,
           null                        as brand_id,
           null                        as brand_name,
           null                        as vertical,
           null                        as brand_group_id,
           null                        as brand_group_name,
           SUSPENDED_AT::timestamp_ntz as alarm_at

    from ops_occ.worst_offenders_alarm wod
             left join (select physical_store_id, physical_store_name, country from store_infos group by 1, 2, 3) psi
                       on psi.physical_store_id::int = wod.STORE_ID::int and wod.country = psi.country

    where category in ('A', 'B')

    union all
    select wod.country,
           'Top Offenders Tier CD'     as alarm_type,
           null                        as product_id,
           null                        as store_id,
           null,
           wod.STORE_ID                as physical_store_id,
           psi.physical_store_name,
           null                        as city_id,
           null                        as city_name,
           null                        as brand_id,
           null                        as brand_name,
           null                        as vertical,
           null                        as brand_group_id,
           null                        as brand_group_name,
           SUSPENDED_AT::timestamp_ntz as alarm_at

    from ops_occ.worst_offenders_alarm wod
             left join (select physical_store_id, physical_store_name, country from store_infos group by 1, 2, 3) psi
                       on psi.physical_store_id::int = wod.STORE_ID::int and wod.country = psi.country

    where category in ('C', 'D')


    union all
    select wod.country,
           'High Discounts Physical Stores' as alarm_type,
           null                             as product_id,
           null                             as store_id,
           null,
           wod.physical_store_id            as physical_store_id,
           psi.physical_store_name,
           null                             as city_id,
           null                             as city_name,
           null                             as brand_id,
           null                             as brand_name,
           null                             as vertical,
           null                             as brand_group_id,
           null                             as brand_group_name,
           alarm_at::timestamp_ntz          as alarm_at

    from ops_occ.high_discounts_physical wod
             left join (select physical_store_id, physical_store_name, country from store_infos group by 1, 2, 3) psi
                       on psi.physical_store_id::int = wod.physical_store_id::int and wod.country = psi.country


    union all

    select wod.country,
           'Payless - Stores'        as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.payless_alarm wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    union all

    select wod.country,
           'Rappi Connect Stores'    as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.rappi_connect wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    union all

    select wod.country,
           'Product Price Error $0'  as alarm_type,
           product_id                as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.product_price_errors wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    where unit_price_in_order = 0

    union all

    select wod.country,
           'Product Price Error $0-$10' as alarm_type,
           product_id                   as product_id,
           wod.store_id::int            as store_id,
           si.store_name,
           si.physical_store_id::int    as physical_store_id,
           si.physical_store_name,
           si.city_id::int              as city_id,
           city_name,
           si.brand_id::int             as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int       as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz      as alarm_at

    from ops_occ.product_price_errors wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    where unit_price_in_order > 0

    union all

    select wod.country,
           'Product/Brand Stockout'  as alarm_type,
           product_id                as product_id,
           store_id                  as store_id,
           null                      as store_name,
           null                      as physical_store_id,
           null                      as physical_store_name,
           null                      as city_id,
           null                      as city_name,
           wod.brand_id::int         as brand_id,
           brand_name,
           null                      as vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           checked_at::timestamp_ntz as alarm_at

    from ops_occ.product_brand_stockout wod
             left join (select brand_id, brand_group_id, brand_group_name, vertical, country
                        from store_infos
                        group by 1, 2, 3, 4, 5) si on si.brand_id::int = wod.brand_id::int and wod.country = si.country

    union all

    select wod.country,
           'High Discounts Brand'  as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null                    as store_name,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           wod.STORE_BRAND_ID::int as brand_id,
           si.brand_name,
           null                    as vertical,
           si.brand_group_id::int  as brand_group_id,
           si.brand_group_name,
           ALARM_AT::timestamp_ntz as alarm_at

    from ops_occ.high_discounts_brand wod
             left join (select brand_id, brand_name, brand_group_id, brand_group_name, vertical, country
                        from store_infos
                        group by 1, 2, 3, 4, 5, 6) si
                       on si.brand_id::int = wod.STORE_BRAND_ID::int and wod.country = si.country


    union all

    select wod.country,
           'Limbo Pickers'         as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           wod.physical_store_id   as physical_store_id,
           psi.physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           null                    as brand_group_id,
           null                    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.limbo_pickers wod
             left join (select physical_store_id, physical_store_name, country from store_infos group by 1, 2, 3) psi
                       on psi.physical_store_id::int = wod.physical_store_id::int and wod.country = psi.country

    union all
    select wod.country,
           'Vertical Cancellation Spike' as alarm_type,
           null                          as product_id,
           null                          as store_id,
           null,
           null                          as physical_store_id,
           null                          as physical_store_name,
           null                          as city_id,
           null                          as city_name,
           null                          as brand_id,
           null                          as brand_name,
           wod.vertical,
           null                          as brand_group_id,
           null                          as brand_group_name,
           alarm_at::timestamp_ntz       as alarm_at

    from ops_occ.vertical_cancellation_spike wod

    union all
    select wod.country,
           'Dispersions'           as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           null                    as brand_group_id,
           null                    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.dispersion_alarm wod

    union all
    select wod.country,
           'Risk Latam'            as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           null                    as brand_group_id,
           null                    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.risks_alarm wod


    union all
    select wod.country,
           'Global Offers'         as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           null                    as brand_group_id,
           null                    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.global_offers wod

    union all
    select wod.country,
           'Spike in Orders'       as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           wod.groupbrandid::int   as brand_group_id,
           wod.brand_name          as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.spike_in_orders wod

    union all
    select wod.country,
           'Payless Group Brand'   as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           wod.brand_group_id::int as brand_group_id,
           wod.brand_group_name    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.payless_alarm_group_brand wod

    union all
    select wod.country,
           'Rappi Connect Brands'  as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null                    as store_name,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           wod.brand_id::int       as brand_id,
           wod.brand_name,
           null                    as vertical,
           si.brand_group_id::int  as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.rappi_connect_brands wod
             left join (select brand_id, brand_group_id, brand_group_name, vertical, country
                        from store_infos
                        group by 1, 2, 3, 4, 5) si on si.brand_id::int = wod.brand_id::int and wod.country = si.country

    union all

    select wod.country,
           'Payless Group Brand'   as alarm_type,
           null                    as product_id,
           null                    as store_id,
           null,
           null                    as physical_store_id,
           null                    as physical_store_name,
           null                    as city_id,
           null                    as city_name,
           null                    as brand_id,
           null                    as brand_name,
           null                    as vertical,
           wod.brand_group_id::int as brand_group_id,
           wod.brand_group_name    as brand_group_name,
           alarm_at::timestamp_ntz as alarm_at

    from ops_occ.payless_alarm_group_brand wod

    union all

    select wod.country,
           'Bug Content Portal'      as alarm_type,
           PRODUCT_ID                as PRODUCT_ID,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.bug_content_portal wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country


    union all
    select wod.country,
           'Early Order Closed'      as alarm_type,
           null                      as PRODUCT_ID,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.early_order_closed wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    union all
    select wod.country,
           'Limitador de Unidades'   as alarm_type,
           product_id                as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.orders_products_unit wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

    union all
    select wod.country,
           'Productos Eletronicos'   as alarm_type,
           product_id                as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           alarm_at::timestamp_ntz   as alarm_at

    from ops_occ.ecommerce_low_prices wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
    union all
    select wod.country,
           'Turbo Fake Orders'       as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SYNC_AT::timestamp_ntz    as alarm_at

    from ops_occ.turbo_fake_orders_alarms wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
    union all
    select wod.country,
           'Falta de RTs'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           ALARM_AT::timestamp_ntz   as alarm_at

    from ops_occ.lack_of_rt_alarm wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
    union all

    select wod.country,
           'Rappi Connect Brands Chat' as alarm_type,
           null                        as product_id,
           null                        as store_id,
           null,
           null                        as physical_store_id,
           null,
           si.city_id::int             as city_id,
           city_name,
           wod.brand_id::int           as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int      as brand_group_id,
           si.brand_group_name,
           ALARM_AT::timestamp_ntz     as alarm_at

    from ops_occ.connect_brands_chat wod
             left join store_infos si on si.brand_id::int = wod.brand_id::int and wod.country = si.country
    union all

    select wod.country,
           'Store Closed Bot'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.store_closed_bot wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
    union all
    select si.country,
           'Predictive Store Closed Bot'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           wod.SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.predictive_groupbrand_details wod
            join ops_occ.predictive_groupbrand pg on wod.BRAND_GROUP_ID = pg.BRAND_GROUP_ID
             left join store_infos si on si.store_id::int = wod.store_id::int and pg.country = si.country
    union all
    select wod.country,
           'Slot Store Closed Bot'     as alarm_type,
           null                        as product_id,
           null                        as store_id,
           null,
           wod.physical_store_id                as physical_store_id,
           psi.physical_store_name,
           null                        as city_id,
           null                        as city_name,
           null                        as brand_id,
           null                        as brand_name,
           null                        as vertical,
           null                        as brand_group_id,
           null                        as brand_group_name,
           SUSPENDED_AT::timestamp_ntz as alarm_at

    from ops_occ.store_closed_super wod
             left join (select physical_store_id, physical_store_name, country from store_infos group by 1, 2, 3) psi
                       on psi.physical_store_id::int = wod.physical_store_id::int and wod.country = psi.country
    union all

    select wod.country,
           'Bleeding Bot'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.worst_offenders_bleeding wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
union all

    select wod.country,
           'Turbo Cancellation Bot'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.turbo_cancellation wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
union all

    select wod.country,
           'Bot de Garantia'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.nl_worst_offenders_guarantee wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
union all

    select wod.country,
           'Bot Store Closed Ecomm Weekly'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.ecommerce_weekly wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
union all

    select wod.country,
           'Bot Store Closed Ecomm Monthly'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.ecommerce_monthly wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country
union all

    select wod.country,
           'Bot Store Closed Ecomm Mi Tienda'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.ecomm_mi_tienda_stock wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

union all

    select wod.country,
           'Bot Store Closed Recorrente'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.store_closed_recurrently wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country

union all

    select wod.country,
           'Bot Peak Orders in Progress'            as alarm_type,
           null                      as product_id,
           wod.store_id::int         as store_id,
           si.store_name,
           si.physical_store_id::int as physical_store_id,
           si.physical_store_name,
           si.city_id::int           as city_id,
           city_name,
           si.brand_id::int          as brand_id,
           si.brand_name,
           si.vertical,
           si.brand_group_id::int    as brand_group_id,
           si.brand_group_name,
           SUSPENDED_AT::timestamp_ntz   as alarm_at

    from ops_occ.waiting_time_cpgsnow wod
             left join store_infos si on si.store_id::int = wod.store_id::int and wod.country = si.country'''


df = snow.run_query(query)

print(df)
df['date'] = pd.to_datetime(df.alarm_at.dt.date)
last_weeks = df.loc[df.date <= datetime.today() - timedelta(days=2)]
last_weeks = last_weeks.groupby(["alarm_type",'date'])["country"].count().reset_index(name="total_alarms")
last_weeks = last_weeks.sort_values(by='date')
last_weeks['week']=last_weeks.date.dt.weekday
last_weeks1 = last_weeks.groupby(["alarm_type",'week'])["total_alarms"].mean().reset_index(name="mean_L8W")
weekday = (datetime.today() - timedelta(days=1)).weekday()

for index, row in last_weeks1.iterrows():
  total_alarm_week = last_weeks1[last_weeks1.week == weekday]

ontem = df.loc[(df.date >= datetime.today() - timedelta(days=2)) & (df.date <= datetime.today() - timedelta(days=1)) ]
ontem = ontem.groupby(["alarm_type",'date'])["country"].count().reset_index(name="total_alarms")
df_final = pd.merge(ontem, total_alarm_week,how='left', on = ['alarm_type'])
df_final['variation'] = (df_final['total_alarms']-df_final['mean_L8W'])/df_final['mean_L8W']
df_final = df_final[df_final.variation < - 0.4]
print(df_final)

if not df_final.empty:
  for index, row in df_final.iterrows():

    text ='''
    :alert: *{alarm_type}*
    Total alarms ayer: {total_alarms}
    {variation}% en relacion a mediana {mean_L8W} das ultimas 8 semanas para jueves.
    '''.format(alarm_type = row['alarm_type'],total_alarms = row['total_alarms'],variation = round(row['variation'],2)*100, mean_L8W = round(row['mean_L8W'],2))

    print(text)
    slack.bot_slack(text=text, channel='C02SA9D3TQC')



