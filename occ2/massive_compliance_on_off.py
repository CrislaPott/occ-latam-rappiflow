from lib import snowflake as snow
from functions import timezones, snippets
import pandas as pd
from lib import slack

query = '''select CONCAT(SLACK_USER,'@rappi.com') as user from ops_occ.slack_users
where TITLE ilike '%OCC%'
and DEACTIVATED in ('false')'''
df_team = snow.run_query(query)
team_occ = df_team.user.to_list()
print(team_occ)

def logs():
	query = '''select day, user, alarm_at::date as date
	from ops_occ.massivecompliance_on_off
	 where alarm_at::date = convert_timezone('America/Buenos_Aires',current_timestamp())::date'''

	df = snow.run_query(query)
	return df

def person_open_close():

	query = '''select created_at::date as day, user, action,count(distinct store_id) as number_stores, listagg(distinct country,',') as countries, listagg(distinct store_id,',') as store_ids
	from ops_occ.global_cms_logger
	where day::date = convert_timezone('America/Buenos_Aires',current_timestamp())::date
	and action in ('turn_store_off - massive','turn_store_off','massive_suspension')
	group by 1,2,3
	having count(distinct store_id) >= 15000
	order by 1 desc, 4 desc
    '''
	df = snow.run_query(query)
	return df

def run_alarm(df1,df2):

	current_time = timezones.country_current_time('br')

	df = pd.merge(df1,df2, how='left', on=['day','user'])
	df = df[(df['date'].isnull())]
	df = df.drop(['date'], axis=1)

	df['alarm_at'] = current_time
	print(df)
	print(df.columns)
	snow.upload_df_occ(df, 'massivecompliance_on_off')

	if not df.empty:
		for index, row in df.iterrows():
			user = row['user'].split('@')[0]
			if row['user'] in team_occ and row['number_stores'] >= 30000:
				print('occ',row['user'])

				text = '''Alarma Tiendas on/off - CMS Logger :alert:
				<@{user}> (team_occ) ha tenido muchos apagados e encendidos :arrow_up_down: por arriba del promedio de las últimas semanas.
				Día: {day}
				Accíon: {action}
				Numero Stores: {number_stores}
				Paises: {countries}	
				<!subteam^S0191S07TND> <!subteam^S018ASNLN5T>				
				'''.format(user=user, day=row['day'], action=row['action'],
										   number_stores=row['number_stores'],
										   countries=row['countries'])
				print(text)
				slack.bot_slack(text,'C029MJCAMS6')

			elif row['user'] not in team_occ and row['number_stores'] >= 15000:
				print('no occ',row['user'])

				text = '''Alarma Tiendas on/off - CMS Logger :alert:
				<@{user}> (no team_occ) ha tenido muchos apagados e encendidos :arrow_up_down: por arriba del promedio de las últimas semanas.
				Día: {day}
				Accíon: {action}
				Numero Stores: {number_stores}
				Paises: {countries}		
				<!subteam^S0191S07TND> <@andrea.gonzalez>		
				'''.format(user=user, day=row['day'], action=row['action'],
										   number_stores=row['number_stores'],
										   countries=row['countries'])
				print(text)
				slack.bot_slack(text, 'CS7UQAMLG')

	else:
		print('df is null')


try:
	df2 = logs()
	df1 = person_open_close()
	run_alarm(df1,df2)
except Exception as e:
	print(e)