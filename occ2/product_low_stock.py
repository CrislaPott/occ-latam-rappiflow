from lib import snowflake as snow
from lib import slack
from functions import timezones
import pandas as pd
from os.path import expanduser

def logs(country):
  timezone, interval = timezones.country_timezones(country)

  query = '''
  select distinct rappi_store_id as rappi_storeid, rappi_product_id as rappi_productid  from ops_occ.products_low_stock
  where country = '{country}'
  and alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  '''.format(country=country, timezone=timezone)

  df = snow.run_query(query)
  return df


def products(country):
    query = '''with pscp as (
        select distinct p.country, p.retailer_product_id as product_id, p.status, vs.id as store_id,p.id as store_product_id
        from br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.product p
        join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.store s on s.id = p.store_id
        join br_amysql_cpgs_clg_im_cpgs_clg_inventory_manager.virtual_store vs ON vs.store_id = s.id
        ),
pscms as
(
SELECT PRODUCT_ID, STORE_ID
FROM br_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.PRODUCT_STORE_VW
),

stores as (
select
            distinct(o.store_id),
            o.store_name,
            o.store_type,
            case when o.store_type ilike 'grupo%big%' then 'Grupo Big'
                when o.store_type ilike 'sams_club%' then 'Sams Club'
                when o.store_type ilike 'extra_%' then 'Extra'
                when o.store_type ilike 'pao%de%azucar%' or o.brand_name ilike '%pao de a%' then 'Pão de Açúcar'
                when o.store_type ilike '%carrefour%' then 'Carrefour'
                when o.store_type ilike '%dalben%' then 'Dalben'
                when o.brand_name ilike '%turbo%' then 'Turbo'
            else coalesce(o.BRAND_GROUP, o.BRAND_NAME) end as brand,
           case when brand ilike '%turbo%' then 'Turbo'
                when o.vertical_sub_group = 'PETS' then 'Pets'
                else o.VERTICAL end as vertical,
            o.city,
            count(distinct o.order_id) as orders_to_month
        from ops_global.global_orders o
        where closed_at::date >= date_trunc('month',current_date)::date
        and closed_at::date < current_date::date -1
        and country = 'BR'
        and o.vertical_group = 'CPGS'
        group by 1,2,3,4,5,6
)
,t1 as (
select
    i.rappi_store_id,
    --s.store_type,
    s.store_name,
    i.rappi_product_id,
    gap.name as product_name,
    pg.name as corredor,
    i.partner_stock,
    i.price,
    i.percentage_price_variation,
    row_number() over (partition by i.rappi_store_id, i.rappi_product_id order by stocker_execution_date desc) as roww,
    (CASE WHEN (b.SUPER_STORE_ID=-1 or pscp.product_id is null) then i.rappi_product_id else b.SUPER_STORE_ID end)::int as super_store_id,
    case when b.in_content=true and pscp.product_id is not null then 'CP' else 'CMS' end as product_plataform,
    (case when b.in_content=true and pscp.product_id is not null then pscp.store_product_id else i.rappi_product_id end)::int as store_product_id
    
    from CPG_INTEGRATIONS.BR_INTEGRATION_METRICS_METADATA i
    left join (select * from AI_AUTOMATION.GLOBAL_ANALYTICS_PRODUCT where country = 'BR') gap
    on gap.product_id = i.rappi_product_id
    left join BR_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc
    on pc.product_id = i.rappi_product_id
    inner join BR_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg
    on pg.tag = pc.category_tag
    left join stores s
    on s.store_id = i.rappi_store_id --lojas

    left join CPGS_DATASCIENCE.LOOKUP_STORE b on b.store_id=i.rappi_store_id and lower(b.COUNTRY_CODE)= 'br'
    left join pscms on pscms.store_id::int = i.rappi_store_id::int and pscms.product_id::int = i.rappi_product_id::int
    left join pscp on pscp.store_id::int = i.rappi_store_id::int and pscp.product_id::int = i.rappi_product_id::int

    where stocker_execution_date::date >= current_date - 1
    and i.partner_stock = 0   -- limite de estoque de segurança
    and (pg.name ilike '%black friday%')
    and s.store_type is not null
    order by 1,2,6 desc
)
select * from t1 where roww = 1'''

    df = snow.run_query(query)
    return df

def alarm(a,b):
    channel_alert = timezones.slack_channels('trend_alarms')
    current_time = timezones.country_current_time(country)

    df = a.merge(b, left_on=['rappi_store_id', 'rappi_product_id'], right_on=['rappi_storeid', 'rappi_productid'], how='left')
    df = df[(df['rappi_productid'].isnull())]
    df = df.drop(['rappi_storeid','rappi_productid'], axis=1)

    print(df)
    df['alarm_at'] = current_time
    df['country'] = country

    if not df.empty:
        print('snowflake')
        snow.upload_df_occ(df, 'products_low_stock')

        text = '''
                Alarma - Productos con stock bajo *CPGs* - Black Friday  :alert:
                :flag-{country}:
                Pais: {country}
                Nuevos productos alcanzarón el threshold de stock bajo!
                '''.format(country=country)
        print(text)
        home = expanduser("~")
        results_file = '{}/details_products_low_stock_{country}.xlsx'.format(home, country=country)
        df_final = df[['rappi_store_id', 'store_name', 'rappi_product_id', 'product_name','corredor', 'partner_stock', 'price', 'percentage_price_variation',
           'roww', 'super_store_id', 'product_plataform', 'store_product_id']]
        df_final.to_excel(results_file, sheet_name='product_id', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")


countries = ['br']
for country in countries:
  try:
    print(country)
    b = logs(country)
    a = products(country)
    alarm(a,b)

  except Exception as e:
    print(e)