import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from os.path import expanduser
from functions import timezones, snippets

def reported_store(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select store_id::text as storeid
    from ops_occ.rappi_connect_chat
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz > dateadd(hour,-2,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and country='{country}'
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def reported_brand(country):
    timezone, interval = timezones.country_timezones(country)

    query2 = '''
    select brand_id::text as brandid
    from ops_occ.connect_brands_chat
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and alarm_at::timestamp_ntz > dateadd(hour,-2,convert_timezone('America/{timezone}',current_timestamp()))::timestamp_ntz
    and country='{country}'
    '''.format(country=country, timezone=timezone)
    df2 = snow.run_query(query2)

    return df2


def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    rappi_connect_query = """
    select
    s.store_id::text as store_id
    from stores s
    join owners o on o.id=s.owner_id
    where rappi_connect=true
    """

    query_orders_l40 = """
    --no_cache
    with base as
    (
    SELECT distinct order_id
    FROM order_modifications om
    WHERE om.created_at >= now() AT TIME ZONE 'America/Buenos_Aires' - interval '40 minutes'
    GROUP BY 1
    )
    select os.store_id::text as store_id,
    base.order_id, 
    max(os.name) as store_name, 
    max(os.type) as store_type
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
    inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
    where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_picker')
    and os.name not ilike '%Dummy%Store%'
    and os.type NOT IN ('queue_order_flow','soat_webview')
    and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
    group by 1,2
    """.format(countrt=country, timezone=timezone)

    if country in ['br', 'ar', 'uy']:
        interval = '6h'
    elif country in ['mx']:
        interval = '0h'
    elif country in ['co', 'pe', 'ec']:
        interval = '8h'
    elif country in ['cl']:
        interval = '7h'
    elif country in ['cr']:
        interval = '9h'

    query_chat = """
    --no_cache
    with a as (
    select max(id) as id_maximo from chats
    )
    select c.order_id as orderid
    from a
    join chats c on c.id >= (a.id_maximo - 100000)
    where 
    created_at - interval '{interval}' >= now() at time zone 'america/{timezone}' - interval '1h'
    and (data ilike '%sem sistema%' or data ilike '%sin sistema%')
    group by 1
    """.format(interval=interval, timezone=timezone)

    query_store_infos = """
    select 'br'                     as country,
           a.store_id::text         as store_id,
           a.name                   as store_name,
           a.type                   as store_type,
           c.id::text               as brand_id,
           c.name                   as brand_name,
           a.city_address_id        as city_id,
           msc.city                 as city_name,
           case when tp.store_type is not null then 'Turbo' else v.vertical end as vertical,
           brand_group_id::text     as brand_group_id,
           brand_group_name,
           cpgops.physical_store_id as physical_store_id,
           pss.name                 as physical_store_name
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
             left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                       on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
             left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
             left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
             left join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                       on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
             inner join (select store_type                      as storetype,
                                case
                                    when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                    when vertical_group = 'WHIM' then 'Antojos'
                                    when vertical_group = 'RAPPICASH' then 'RappiCash'
                                    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                    when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                    when upper(store_type) in ('TURBO') then 'Turbo'
                                    when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                    when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                    when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                    when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                    when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                    when upper(vertical_group) in ('CPGS') then 'CPGs'
                                    else vertical_sub_group end as vertical
                         from VERTICALS_LATAM.{country}_VERTICALS_V2
    ) v on v.storetype = a.type

             left join (
        select *
        from (select store_brand_id,
                     last_value(brand_group_id)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_id,
                     last_value(bg.name)
                                over (partition by store_brand_id order by tb.created_at asc)          as brand_group_name
              from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                       join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
        group by 1, 2, 3
    ) bg on bg.store_brand_id = a.store_brand_id

    left join OPS_OCC.TURBO_STORE_TYPE tp on tp.store_type=a.type and lower(tp.country)='{country}'

    where not coalesce(a._fivetran_deleted, false)
    --and v.vertical not in ('Turbo')
    """.format(timezone=timezone, country=country)

    if country == 'co':
        ordersl40 = redash.run_query(1904, query_orders_l40)
        chat = redash.run_query(652, query_chat)
        rappi_connect_stores = redash.run_query(4486, rappi_connect_query)
    elif country == 'ar':
        ordersl40 = redash.run_query(1337, query_orders_l40)
        chat = redash.run_query(647, query_chat)
        rappi_connect_stores = redash.run_query(4487, rappi_connect_query)
    elif country == 'cl':
        ordersl40 = redash.run_query(1155, query_orders_l40)
        chat = redash.run_query(649, query_chat)
        rappi_connect_stores = redash.run_query(4489, rappi_connect_query)
    elif country == 'mx':
        ordersl40 = redash.run_query(7977, query_orders_l40)
        chat = redash.run_query(651, query_chat)
        rappi_connect_stores = redash.run_query(4291, rappi_connect_query)
    elif country == 'uy':
        ordersl40 = redash.run_query(1156, query_orders_l40)
        chat = redash.run_query(650, query_chat)
        rappi_connect_stores = redash.run_query(4492, rappi_connect_query)
    elif country == 'ec':
        ordersl40 = redash.run_query(1922, query_orders_l40)
        chat = redash.run_query(2184, query_chat)
        rappi_connect_stores = redash.run_query(4491, rappi_connect_query)
    elif country == 'cr':
        ordersl40 = redash.run_query(1921, query_orders_l40)
        chat = redash.run_query(2183, query_chat)
        rappi_connect_stores = redash.run_query(4490, rappi_connect_query)
    elif country == 'pe':
        ordersl40 = redash.run_query(1157, query_orders_l40)
        chat = redash.run_query(745, query_chat)
        rappi_connect_stores = redash.run_query(4493, rappi_connect_query)
    elif country == 'br':
        ordersl40 = redash.run_query(1338, query_orders_l40)
        chat = redash.run_query(648, query_chat)
        rappi_connect_stores = redash.run_query(4488, rappi_connect_query)

    store_infos = snow.run_query(query_store_infos)

    return ordersl40, chat, rappi_connect_stores, store_infos


def alarm(country):

    current_time = timezones.country_current_time(country)
    channel_alert = timezones.slack_channels('tech_integration')

    orders_l40, chat, rappi_connect_stores, store_infos = offenders(country)

    if not chat.empty:
        rappi_connect_stores['store_id']=rappi_connect_stores['store_id'].astype(str)
        store_infos['store_id']=store_infos['store_id'].astype(str)
        df_ = pd.merge(rappi_connect_stores, store_infos, how='inner', left_on=rappi_connect_stores['store_id'],
                      right_on=store_infos['store_id'])
        df_ = df_[['store_id_y', 'brand_name', 'brand_id']]
        orders_l40['store_id']=orders_l40['store_id'].astype(str)
        df_orders = pd.merge(df_, orders_l40, how='inner', left_on=df_['store_id_y'], right_on=orders_l40['store_id'])
        df_orders = df_orders.drop(['key_0', 'store_id', 'store_name', 'store_type'], axis=1)
        df_total = df_orders.groupby(['store_id_y', 'brand_name', 'brand_id']).count().reset_index()
        df_total = df_total.rename(columns={'order_id': 'total_orders', 'store_id_y': 'store_id'})
        chat['orderid']=chat['orderid'].astype(str)
        df_orders['order_id']=df_orders['order_id'].astype(str)
        df_chat = pd.merge(df_orders, chat, how='inner', left_on=df_orders['order_id'], right_on=chat['orderid'])
        df1 = df_chat.groupby(['store_id_y', 'brand_name', 'brand_id'])['order_id'].agg(
            ['count', ('order_id', lambda x: ', '.join(map(str, x)))])
        df1 = df1.sort_values('count', ascending=False).reset_index()
        df1 = df1.rename(columns={'count': 'total_cancels', 'store_id_y': 'store_id'})
        df1 = pd.merge(df_total, df1, how='left', left_on=df_total['store_id'], right_on=df1['store_id'])
        df1['percentage'] = (df1['total_cancels'] / df1['total_orders']).round(3)
        df1 = df1[['store_id_x', 'brand_name_x', 'brand_id_x', 'total_orders', 'total_cancels', 'percentage',
                   'order_id']].rename(
            columns={'store_id_x': 'store_id', 'brand_name_x': 'brand_name', 'brand_id_x': 'brand_id'})

        df1 = df1[df1['total_cancels'] >= 2]
        print(df1)
        if not df1.empty:
            rp = reported_store(country)
            rp['storeid']=rp['storeid'].astype(str)
            df_1 = pd.merge(df1, rp, how='left', left_on=df1['store_id'], right_on=rp['storeid'])
            df_1 = df_1[(df_1['storeid'].isnull())]
            df_1 = df_1.drop(['key_0', 'storeid'], axis=1)
            print(df_1)

            df_1['country'] = country
            df_1['alarm_at'] = current_time

            snow.upload_df_occ(df_1, 'rappi_connect_chat')

            df_1['alarm_at'] = current_time.replace(tzinfo=None)

            try:
                for index, row in df_1.iterrows():
                    row = dict(row)
                    text = '''*Alarma Rappi Connect - Tiendas con informes de ningún sistema pelo chat * :alert:
                        Country: {country} :flag-{country}:
                        La Tienda de Store ID {store_id} de la brand {brand_name} ancalzó {percentage}% de cancelacion en los ultimos 40 minutos
                        Total Ordenes: {total_orders}
                        Total Pedidos com problemas: {total_cancels}
                        Order_IDs: {order_id}
                        '''.format(
                        store_id=row['store_id'],
                        total_orders=row['total_orders'],
                        total_cancels=row['total_cancels'],
                        percentage=round(row['percentage'] * 100, 2),
                        order_id=row['order_id'],
                        brand_name=row['brand_name'],
                        country=country
                    )
                    print(text)
                    home = expanduser("~")
                    results_file = '{}/rappi_connect_chat_store_{country}.xlsx'.format(home, country=country)
                    df_1.to_excel(results_file, sheet_name='store', index=False)
                    slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

            except Exception as e:
                print(e)
        else:
            print('df1 null')
    else:
        print('no se aplica')

    if not chat.empty:
        rappi_connect_stores['store_id'] = rappi_connect_stores['store_id'].astype(str)
        store_infos['store_id'] = store_infos['store_id'].astype(str)
        store_infos = store_infos[~store_infos['vertical'].isin(['Turbo'])]
        df = pd.merge(rappi_connect_stores, store_infos, how='inner', left_on=rappi_connect_stores['store_id'],
                      right_on=store_infos['store_id'])
        df = df[['store_id_y', 'brand_name', 'brand_id']]
        orders_l40['store_id'] = orders_l40['store_id'].astype(str)
        df_orders = pd.merge(df, orders_l40, how='inner', left_on=df['store_id_y'], right_on=orders_l40['store_id'])
        df_total = df_orders.groupby(['brand_name', 'brand_id']).count().reset_index()
        df_total = df_total[['brand_name', 'brand_id', 'order_id']].rename(columns=dict(order_id='total_orders'))
        df_chat_brand = df_chat.groupby(['brand_name', 'brand_id']).count().reset_index()
        df_chat_brand = df_chat_brand[['brand_name', 'brand_id', 'order_id']].rename(
            columns={'order_id': 'total_cancels'})
        df_total['brand_id']=df_total['brand_id'].astype(str)
        df_chat_brand['brand_id']=df_chat_brand['brand_id'].astype(str)
        df2 = pd.merge(df_chat_brand, df_total, how='inner', left_on=df_chat_brand['brand_id'],
                       right_on=df_total['brand_id'])
        df2 = df2[['brand_name_x', 'brand_id_x', 'total_orders', 'total_cancels']].rename(
            columns={'brand_name_x': 'brand_name', 'brand_id_x': 'brand_id'})
        df2['percentage'] = (df2['total_cancels'] / df2['total_orders']).round(3)

        ordercancel = df_chat.groupby(['brand_name', 'brand_id'])['orderid'].agg(
            ['count', ('orderid', lambda x: ', '.join(map(str, x)))]).reset_index()
        storecancel = df_chat.groupby(['brand_name', 'brand_id'])['store_id_y'].agg(
            ['count', ('store_id_y', lambda x: ', '.join(map(str, x)))]).reset_index()
        order_store = pd.merge(ordercancel, storecancel)
        df2 = pd.merge(df2, order_store).drop(['count'], axis=1).rename(columns={'store_id_y': 'store_id'})

        df2 = df2[df2['total_cancels'] >= 3]

        print(df2)

        if not df2.empty:
            rp2 = reported_brand(country)
            rp2['brandid'] = rp2['brandid'].astype(str)
            df2['brand_id']=df2['brand_id'].astype(str)
            df_2 = pd.merge(df2, rp2, how='left', left_on=df2['brand_id'], right_on=rp2['brandid'])
            df_2 = df_2[(df_2['brandid'].isnull())]
            df_2 = df_2.drop(['key_0', 'brandid'], axis=1)
            print(df_2)

            df_2['country'] = country
            df_2['alarm_at'] = current_time

            snow.upload_df_occ(df_2, 'connect_brands_chat')
            df_2['alarm_at'] = current_time.replace(tzinfo=None)
            try:
                for index, row in df_2.iterrows():
                    row = dict(row)
                    text = '''*Alarma Rappi Connect - Brand con informes de ningún sistema pelo chat* :alert:
                        Country: {country} :flag-{country}:
                        La Brand {brand_name} ancalzó {percentage}% de cancelacion en los ultimos 40 minutos
                        Total Ordenes: {total_orders}
                        Total Pedidos com problemas: {total_cancels}
                        Order_IDs: {orderid}
                        Store_IDs: {store_id}
                        '''.format(
                        total_orders=row['total_orders'],
                        total_cancels=row['total_cancels'],
                        percentage=round(row['percentage'] * 100, 2),
                        orderid=row['orderid'],
                        store_id=row['store_id'],
                        brand_name=row['brand_name'],
                        country=country
                    )
                    print(text)
                    home = expanduser("~")
                    results_file = '{}/rappi_connect_chat_brand_{country}.xlsx'.format(home, country=country)
                    df_2.to_excel(results_file, sheet_name='brand', index=False)
                    slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

            except Exception as e:
                print(e)
        else:
            print('df2 null')

    else:
        print('no se aplica')


def run_alarm(country):
    alarm(country)

countries = snippets.country_list()
for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)