import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from functions import timezones


def orders(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''with a as (
select (max(updated_at)) as last_data
from orders)
select 
(extract(epoch from ((now() at time zone 'America/{timezone}' - last_data)) / 60)) as delay_minutes
from a'''
    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)
    return metrics

def alarm(a):
    hour1 = (datetime(2021, 4, 24, 10, 0, 0, 0)).time()

    current_time = timezones.country_current_time(country).time()
    print(current_time)
    if current_time >= hour1:
        if float(a.delay_minutes) >= 10:
            print(a)
            text = f'''
        *Alarma - Problemas de Delay en los Microservicios*
        :flag-{country}:
        Pais: {country}
        MS Rappi Core Orders Live - Delay: *{round(float(a.delay_minutes),2)}* min'''
            print(text)
            slack.bot_slack(text=text, channel='C02SA9D3TQC')



countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']

for country in countries:
  try:
    print(country)
    a = orders(country)
    alarm(a)
  except Exception as e:
    print(e)
