import pandas as pd


def query_eletronic_products(country):
	if country in ['br']:
		query = f"""
		(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
    join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id

    where name like '% Garmin %' or name like '%Garmin %'
       or name like '%Amazfit%'  or name like '%smartwatch%'
       or name like '%haylou%'   or name ilike '%projetor led%'
       or name ilike '%receiver%' or name ilike '%yamaha%'
       or name ilike '%telescopio%' or name ilike '%telescópio%'
       or name ilike '%telescopio%' or name ilike '%telescópio%'
       or name ilike '% Go Pro %' or name ilike 'Go Pro %'
       or name ilike 'Drone %' or name ilike 'echo %'
       or name ilike '% echo %' or name ilike '%homepod %'
       or name ilike 'multifuncional%' or name ilike '%epson%'
       or name ilike '%impressora%' or name ilike '%deskjet%'
       or name ilike '%notebook%' or name like 'Tablet%'
     --and ps.price <= 500
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
		"""
	elif country in ['co']:
		query = f"""
				(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
    join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id

    where name like '% Garmin%'
or name like '%Amazfit%'
or name like '%smartwatch%'
or name like '%haylou%'
or name ilike '% televisor led%'
or name ilike '%barra de sonido%'
or name ilike '%laser%'
or name ilike '% Go Pro %'
or name ilike 'Iphone %'
or name ilike 'Drone %'
or name ilike '% echo%'
or name ilike '%homepod %'
or name ilike '%multifuncional%'
or name ilike '%epson%'
or name ilike '%impressora%'
or name ilike '%deskjet%'
or name ilike '%notebook%'
or name like '%Tablet%'
or name ilike '%Ipad%'
or name ilike '%pioneer%'
or name ilike '%beats%'
or name ilike '%Apple watch%'
or name ilike '%HUAWEI%'
or name ilike '%airpods%'
or name ilike '%JBL%'
or name ilike '%Bose%'
or name like '%MacBook%'
or name ilike '%Sony%'
or name ilike '%Xiaomi%'
or name ilike '%Audio-Technica%'
or name ilike '%Hudson%'
or name ilike '%Samsung%'
or name ilike '%galaxy%'
or name ilike '%hp Book%'
or name ilike '%yamaha%'
or name ilike '%economodo%'
or name ilike '%Ge Force%'
or name ilike '%Procesador%'
or name ilike '%nintendo%'
or name ilike '%consola%'
or name ilike '%homepod%'
or name ilike '%deskjet%'
or name ilike '%deco mesh%'
or name ilike '%lenovo%'
or name ilike '%alexa%'
or name ilike '%disco duro%'
or name ilike '%hytab%'
or name ilike '%nikon%'
or name ilike '%canon%'
or name ilike '%monitor%' 
or name ilike '%kaley%'
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
		"""
	elif country in ['mx','ec','cr']:
		query = f"""
				(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
    join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id

    where name like '% Garmin %'
   or name like '%Garmin %'
   or name like '%Amazfit%'
   or name like '%smartwatch%'
   or name like '%haylou%'
   or name ilike '%projetor led%'
   or name ilike '%receiver%'
   or name ilike '%yamaha%'
   or name ilike '%telescopio%' or name ilike '%telescópio%'
   or name ilike '% Go Pro %'
   or name ilike 'Go Pro %'
   or name ilike 'Drone %'
   or name ilike 'echo %'
   or name ilike '% echo %'
   or name ilike '%homepod %'
   or name ilike 'multifuncional%'
   or name ilike '%epson%'
   or name ilike '%impressora%'
   or name ilike '%deskjet%'
   or name ilike '%notebook%'
   or name like 'Tablet%'
   or name ilike 'Tablet%'
   or name ilike '%pioneer%'
   or name ilike '%beats%'
   or name ilike '%SKULLCANDY%'
   or name ilike '%HUAWEI%'
   or name ilike '%Sennheiser%'
   or name ilike '%Onkyo%'
   or name ilike '%Bose%'
   or name like '%KEF%'
   or name ilike '%Harman Kardon%'
   or name ilike '%denon%'
   or name ilike '%Audio-Technica%'
   or name ilike '%Hudson%'
   or name ilike '%Edifier%'
   or name ilike '%projetor led%'
   or name ilike '%receiver%'
   or name ilike '%yamaha%'
   or name ilike '%telescopio%'
   or name ilike '%Ge Force%'
   or name ilike '%Procesador%'
   or name ilike '% Go Pro %'
   or name ilike '%nintendo%'
   or name ilike '%Drone%'
   or name ilike '%consola%'
   or name ilike '% echo %'
   or name ilike '%homepod %'
   or name ilike '%multifuncional%'
   or name ilike '%epson%'
   or name ilike '%impresora%'
   or name ilike '%deskjet%'
   or name ilike '%deco mesh%'
   or name ilike '%lenovo%'
   or name ilike '%alexa%'
   or name ilike '%disco duro%'
   or name ilike '%hytab%'
   or name ilike '%nikon%'
   or name ilike '%canon%'
   or name ilike '%monitor%'
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
		"""
	elif country in ['cl','pe']:
		query = f"""
				(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
    join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id

    where name like '%Garmin%'
or name like '%Amazfit%'
or name like '%smartwatch%'
or name like '%haylou%'
or name ilike '% televisor led%'
or name ilike '%barra de sonido%'
or name ilike '%laser%'
or name ilike '% Go Pro %'
or name ilike 'Iphone %'
or name ilike 'Drone %'
or name ilike '%camara de video%'
or name ilike '%homepod %'
or name ilike '%multifuncional%'
or name ilike '%epson%'
or name ilike '%impressora%'
or name ilike '%deskjet%'
or name ilike '%notebook%'
or name like '%Tablet%'
or name ilike '%Ipad%'
or name ilike '%pioneer%'
or name ilike '%beats%'
or name ilike '%Apple watch%'
or name ilike '%HUAWEI%'
or name ilike '%airpods%'
or name ilike '%JBL%'
or name ilike '%Bose%'
or name like '%MacBook%'
or name ilike '%Sony%'
or name ilike '%Xiaomi%'
or name ilike '%FDR%'
or name ilike '%Hudson%'
or name ilike '%Samsung%'
or name ilike '%galaxy%'
or name ilike '%hp Book%'
or name ilike '%yamaha%'
or name ilike '%economodo%'
or name ilike '%Ge Force%'
or name ilike '%Procesador%'
or name ilike '%nintendo%'
or name ilike '%consola%'
or name ilike '%homepod%'
or name ilike '%deskjet%'
or name ilike '%deco mesh%'
or name ilike '%lenovo%'
or name ilike '%alexa%'
or name ilike '%disco duro%'
or name ilike '%hytab%'
or name ilike '%nikon%'
or name ilike '%canon%'
or name ilike '%monitor%' 
or name ilike '%kaley%'
or name ilike '%tornamesa%'
or name ilike '%microcomponente%'
or name ilike '%wireless%'
or name ilike '%extrabass%'
or name ilike '%lente montura%'
or name ilike 'LG%'

or name ilike '%LG'
or name ilike '%switch%'
or name ilike '%acer%'
or name ilike '%motorola%'
or name ilike '%televisor%'
or name ilike 'Led%'

or name ilike '%Led'
or name ilike '%watch active%'
or name ilike '%google wifi%'
or name ilike '%inissia%'
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
		"""
	elif country in ['ar','uy']:
		query = f"""
				(
    select p.id as product_id, 'eletronic_products' as product_flag
    from {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.products_vw p
    join {country}_PGLR_MS_GRABILITY_PRODUCTS_PUBLIC.product_store_vw ps on ps.product_id = p.id

   where name like '%Garmin%'
or name like '%Amazfit%'
or name like '%smartwatch%'
or name like '%haylou%'
or name ilike '% televisor led%'
or name ilike '%barra de sonido%'
or name ilike '%laser%'
or name ilike '% Go Pro %'
or name ilike 'Iphone %'
or name ilike 'Drone %'
or name ilike '%camara de video%'
or name ilike '%homepod %'
or name ilike '%multifuncional%'
or name ilike '%epson%'
or name ilike '%impresora%'
or name ilike '%deskjet%'
or name ilike '%notebook%'
or name like '%Tablet%'
or name ilike '%Ipad%'
or name ilike '%pioneer%'
or name ilike '%beats%'
or name ilike '%Apple watch%'
or name ilike '%HUAWEI%'
or name ilike '%airpods%'
or name ilike '%JBL%'
or name ilike '%Bose%'
or name like '%MacBook%'
or name ilike '%Sony%'
or name ilike '%Xiaomi%'
or name ilike '%FDR%'
or name ilike '%Hudson%'
or name ilike '%Samsung%'
or name ilike '%galaxy%'
or name ilike '%hp Book%'
or name ilike '%yamaha%'
or name ilike '%dell%'
or name ilike '%Ge Force%'
or name ilike '%Procesador%'
or name ilike '%nintendo%'
or name ilike '%consola%'
or name ilike '%homepod%'
or name ilike '%deskjet%'
or name ilike '%deco mesh%'
or name ilike '%lenovo%'
or name ilike '%alexa%'
or name ilike '%disco duro%'
or name ilike '%hytab%'
or name ilike '%nikon%'
or name ilike '%canon%'
or name ilike '%monitor%'
or name ilike '%google%'
or name ilike '%asus%'
or name ilike '%microcomponente%'
or name ilike '%wireless%'
or name ilike '%extrabass%'
or name ilike '%lente montura%'
or name ilike 'LG%'

or name ilike '%Led'
or name ilike '%switch%'
or name ilike '%acer%'
or name ilike '%motorola%'
or name ilike '%televisor%'
or name ilike 'Led%'
or name ilike '%watch active%'
or name ilike '%google wifi%'
or name ilike '%inissia%'
or name ilike '%intel%'
or name ilike '%fujifilm%'
or name ilike '%panasonic%'
or name ilike '%razer%'
or name ilike 'hp%'
or name ilike '%msi%'
or name ilike '%home teatre%'
or name ilike '%m-audio%'
or name ilike '%numark%'
or name ilike '%akai%'
or name ilike '%alesis%'
or name ilike '%marshall%'
or name ilike '%klipsch%'
    group by 1
)
union all
(
-- saca a média de preço de cada ean/quantidade
with a as (
    select ean, quantity, avg(price) as ean_avg_price
    from (
             select ean, quantity, max_price, min_price, store_ids, price
             from (
                      select ean,
                             quantity,
                             price,
                             max(price) over (partition by ean, quantity)               as max_price,
                             min(price) over (partition by ean, quantity)               as min_price,
                             count(distinct store_id) over (partition by ean, quantity) as store_ids

                      from {country}_pglr_ms_grability_products_public.product_store_vw ps
                               inner join {country}_pglr_ms_grability_products_public.products_vw p on p.id = ps.product_id

                      where is_available = true
                        and in_stock = true
                        and is_discontinued = false
                        and len(p.ean) >= 13
                        and ps.price <> 9999
                        and ps.price <= 10000
                        and ps.price <> 0
                      group by 1, 2, 3, ps.store_id)
            ---where (case when store_ids >= 10 then price <> max_price and price <> min_price else price end)
             group by 1, 2, 3, 4, 5, 6
         ) group by 1,2
)
select product_id, 'high_ean_variation' as product_flag
from (
-- pega os produtos nas lojas que tem o preço <= - 70% em relação a média do mesmo ean na Rappi
select ps.store_id, s.name as store_name, ps.product_id, p.name, p.ean, ps.price, ean_avg_price, (ps.price-ean_avg_price)/nullif(ean_avg_price, 0) as var_perc, p.quantity

from {country}_pglr_ms_grability_products_public.product_store_vw ps
left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=ps.store_id
left join {country}_pglr_ms_grability_products_public.products_vw p on p.id=ps.product_id
left join a on a.ean=p.ean and a.quantity=p.quantity

where is_available=true
and in_stock = true
and is_discontinued = false
and len(p.ean) >= 13
and var_perc <= - 0.70)
group by 1,2)
		"""
	return query

