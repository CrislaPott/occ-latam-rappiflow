import os, sys, json
import pandas as pd
import gspread
from df2gspread import df2gspread as d2g
from oauth2client.service_account import ServiceAccountCredentials
import snowflake.connector
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from dotenv import load_dotenv
from airflow.models import Variable

load_dotenv()

def to_gsheets(df):
	scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
	credentials = ServiceAccountCredentials.from_json_keyfile_name('jsonFileFromGoogle.json', scope)
	gc = gspread.authorize(credentials)
	spreadsheet_key = '17Ip7vKo0SJ7OHaZrqKBaA0Xc6stUMdsHGFaHcjgWdjs'
	if country == 'br':
		df1 = df[(df['vertical'] == 'Ecommerce') | (df['vertical'] == 'CPGs')]
		wks_name1 = 'Ecommerce-CPGs BR'
		d2g.upload(df1, spreadsheet_key, wks_name1, credentials=credentials, row_names=True)
		df2 = df[(df['vertical'] == 'Restaurant' )]
		wks_name2 = 'Restaurant BR'
		d2g.upload(df2, spreadsheet_key, wks_name2, credentials=credentials, row_names=True)
	else:
		wks_name3 = 'Country_'+country.upper()
		d2g.upload(df, spreadsheet_key, wks_name3, credentials=credentials, row_names=True)


def suspended_stores(country):
	conn = snowflake.connector.connect(
        host=Variable.get('SNOWFLAKE_HOST'),
        user=Variable.get('SNOWFLAKE_USER'),
        password=Variable.get('SNOWFLAKE_PASSWORD'),
        account=Variable.get('SNOWFLAKE_ACCOUNT'),
        database=Variable.get('SNOWFLAKE_DATABASE'),
        warehouse=Variable.get('SNOWFLAKE_WAREHOUSE')
		)
	cur = conn.cursor()
	query = '''with

	rest_cancel_shutdowns as (
		select
			store_id,
      name,
			'Restaurant' as vertical,
			'Ops Bot' as responsible,
      'Cancellations' as reason,
      'Suspension' as action, category,
			dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp) as shutdown_moment,
			dateadd ('hour', shutdown_time_amount,dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp))::varchar as scheduled_revival
		from br_writable.{country}_rest_cancellations_shutdowns
		where dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp)::date between dateadd ('days', -7, convert_timezone ('America/Buenos_Aires', current_timestamp))::date and convert_timezone ('America/Buenos_Aires', current_timestamp)::date
	),

	cpgs_cancel_shutdowns as (
		select
			store_id,
      name,
			'CPGs' as vertical,
			'Ops Bot' as responsible,
      'Cancellations' as reason,
      'Suspension' as action,category,
			dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp) as shutdown_moment,
			dateadd ('hour', shutdown_time_amount,dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp))::varchar as scheduled_revival
		from br_writable.{country}_cpgs_cancellations_shutdowns
		where dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp)::date between dateadd ('days', -7, convert_timezone ('America/Buenos_Aires', current_timestamp))::date and convert_timezone ('America/Buenos_Aires', current_timestamp)::date
	),

	ecomm_cancel_shutdowns as (
		select
			store_id,
      name,
			'Ecommerce' as vertical,
			'Ops Bot' as responsible,
      'Cancellations' as reason,
      'Suspension' as action,category,
			dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp) as shutdown_moment,
			dateadd ('hour', shutdown_time_amount,dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp))::varchar as scheduled_revival
		from br_writable.{country}_ecomm_cancellations_shutdowns
		where dateadd (s, coalesce (shutdown_moment,epoch) - 3600*3, '1970-01-01'::timestamp)::date between dateadd ('days', -7, convert_timezone ('America/Buenos_Aires', current_timestamp))::date and convert_timezone ('America/Buenos_Aires', current_timestamp)::date
	),

	occ_slack_bot_shutdowns as (
		select
			occ.store_id,
      st.name,
      case
        when type = 'restaurant' then 'Restaurant'
        when "GROUP" = 'ecommerce' and store_type not in ('petz_loja', 'petzdark_loja') then 'Ecommerce'
        else 'CPGs'
			end as vertical,
			'OCC' as responsible,
      initcap(suspended_reason) as reason,
      'Suspension' as action,'null' as category,
			dateadd (s, shutdown_moment - 3600*3, '1970-01-01'::timestamp) as shutdown_moment,
			case
				when timeframe::varchar = 'indefinido' then timeframe::varchar
				when length(timeframe) = 16 then to_timestamp_ntz(timeframe, 'dd/mm/yyyy hh24:MI')::varchar
				else to_timestamp_ntz(timeframe, 'dd/mm/yyyy')::varchar
			end as scheduled_revival
		from br_writable.occ_suspensions occ
    left join {country}_PGLR_MS_STORES_PUBLIC.stores st on (st.store_id = occ.store_id)
    left join
      (select distinct
          "GROUP",
          store_type
      from {country}_PGLR_MS_STORES_PUBLIC.verticals
      ) vert on (vert.store_type = st.type)
		where lower(country) = '{country}'
    and dateadd (s, shutdown_moment - 3600*3, '1970-01-01'::timestamp)::date between dateadd ('days', -7, convert_timezone ('America/Buenos_Aires', current_timestamp))::date and convert_timezone ('America/Buenos_Aires', current_timestamp)::date
	),

	stores_shutdowns as (
		select * from rest_cancel_shutdowns
		union all
		select * from cpgs_cancel_shutdowns
		union all
		select * from ecomm_cancel_shutdowns
		union all
		select * from occ_slack_bot_shutdowns
		order by shutdown_moment desc
	),

	suspended_stores as (
    select distinct
        store_id
    from ops_global.suspended_stores
    where
        lower(country) = '{country}'
	)

, a as (
select
	shut.store_id,
  shut.name as store_name,
	shut.vertical,
	shut.responsible as responsavel,
  --shut.reason,
  --shut.action,
  --susp.store_id is null as is_active,
	split_part (date_trunc ('seconds', shut.shutdown_moment) ,'.', 1) as inicio_suspensao,
  case
    when shut.scheduled_revival::varchar = 'indefinido' then shut.scheduled_revival::varchar
    else split_part (date_trunc ('seconds', shut.scheduled_revival::timestamp_ntz) ,'.', 1)::varchar
  end as fim_suspensao,
  datediff('hours', inicio_suspensao::timestamp_ntz, fim_suspensao::timestamp_ntz) as duracao_suspensao,
  category
from stores_shutdowns shut
left join suspended_stores susp on (susp.store_id = shut.store_id)
-- where shutdown_moment is not null
where fim_suspensao::timestamp_ntz >= convert_timezone('America/Buenos_Aires',current_timestamp)::timestamp_ntz
order by shutdown_moment desc
  )

, last as (select store_id, max(inicio_suspensao) as last_start from a group by 1)

select a.store_id, a.store_name, a.vertical, a.responsavel, a.inicio_suspensao, a.fim_suspensao,
       category,
    case
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Top' and duracao_suspensao > 1 then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Top' and duracao_suspensao <= 1 then 'strike 1 ou 2'

       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Mid' and duracao_suspensao > 3  then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Mid' and duracao_suspensao <= 3 then 'strike 1 ou 2'

       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Mid' and duracao_suspensao > 6 then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Low' and duracao_suspensao <= 6 then 'strike 1 ou 2'

       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Top' and duracao_suspensao > 1 then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Top' and duracao_suspensao <= 1 then 'strike 1 ou 2'

       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Mid' and duracao_suspensao > 3  then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs','Restaurant') and category ='Mid' and duracao_suspensao <= 3 then 'strike 1 ou 2'

       when a.vertical in ('Ecommerce','CPGs') and category ='Low' and duracao_suspensao > 6 then 'strike 3'
       when a.vertical in ('Ecommerce','CPGs') and category ='Low' and duracao_suspensao <= 6 then 'strike 1 ou 2'

       when a.vertical in ('Restaurant') and category ='Low' and duracao_suspensao > 5 then 'strike 3'
       when a.vertical in ('Restaurant') and category ='Low' and duracao_suspensao <= 5 then 'strike 1 ou 2'
       when responsavel = 'OCC' then 'nao se aplica'
  end as strike, upper('{country}') as country
 from a
inner join last on last.store_id=a.store_id and last_start=inicio_suspensao
order by inicio_suspensao desc'''.format(country=country)
	cur.execute(query)
	df = pd.DataFrame.from_records(iter(cur), columns=[x[0] for x in cur.description])
	conn.close()
	df.columns = [x.lower() for x in df.keys()]
	df.set_index('store_id', inplace=True)
	return df


countries = ['ar','br','cl','uy','co','pe','ec','mx']
for country in countries:
	try:
		df = suspended_stores(country)
		print(df)
		to_gsheets(df)
	except Exception as e:
		print(e)
