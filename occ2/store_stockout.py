import pandas as pd
from lib import snowflake, slack
from functions import timezones

def logs(country):
	timezone, interval = timezones.country_timezones(country)
	query = f"""
	select distinct store_id::text as storeid 
	from ops_occ.special_days_so_stores 
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country = '{country}'
	"""
	df = snowflake.run_query(query)
	return df


def stockout_store_br(country):
	query = f'''--no_cache
	with om as (select order_id,coalesce(om.params:removed_product_id, om.params:product_id) as product_id
	from br_core_orders_public.order_modifications_vw om
	where (coalesce(om._fivetran_deleted, 'false') = false
	and created_at::date >= convert_timezone('America/Buenos_Aires',current_timestamp())::date
	)
	and om.type in ('shopper_replacement_product','remove_product','shopper_remove_product',
	'shopper_remove_whim','support_remove_product','support_remove_whim')
	),
	base_stockout as (
	select o.id             as order_id
	, max(op.store_id) as store_id
	, op.product_id
	, p.name
	, (case when om.order_id is not null then 1 else 0 end) as stock_out
	, case when o.state ilike '%cancel%' then 'canceled' else 'no canceled' end as order_state
	, max(balance_price) as balance_price
	, max(price) as price

	from br_core_orders_public.orders_vw o
	join br_core_orders_public.order_product_vw op on op.order_id = o.id
	left join br_grability_public.products_vw p on p.id = op.product_id
	left join br_grability_public.product_store_vw ps on ps.product_id=p.id and ps.store_id=op.store_id
	left join BR_PGLR_MS_PRODUCT_GROUPS_PUBLIC.product_categories pc on pc.product_id = p.id
	inner join BR_PGLR_MS_PRODUCT_GROUPS_PUBLIC.PRODUCT_GROUP pg on pg.tag = pc.category_tag
	left join om on om.order_id = op.order_id and om.product_id = op.product_id

	where coalesce(o.closed_at, o.updated_at)::date >= convert_timezone('America/Buenos_Aires',current_timestamp())::date
	and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and (lower (pg.name) ilike 'dia dos pais' or lower (pg.name) ilike 'dia do pai')
	group by 1,3,4,5,6
	)
	select sb.id brand_id,
	sb.name as brand_name,
	bs.store_id::text as store_id,
    s.name,
    s.type,
	v.vertical,
	count(distinct case when stock_out >=1 and order_state in ('canceled') then order_id else null end) as orders_so,
	count(distinct order_id) as all_orders, orders_so/all_orders as percentage_so,
	listagg(distinct case when stock_out >=1 and order_state in ('canceled') then order_id::text else null end, ',') as order_ids
	from base_stockout bs
	join br_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=bs.store_id
	join br_PGLR_MS_STORES_PUBLIC.store_brands_vw sb on sb.id=s.store_brand_id
	join (select store_type as storetype,
	case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
	when vertical_group = 'RESTAURANTS' then 'Restaurantes'
	when vertical_group = 'WHIM' then 'Antojos'
	when vertical_group = 'RAPPICASH' then 'RappiCash'
	when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
	when upper (store_type) in ('TURBO') then 'Turbo'
	when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
	when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
	when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
	when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
	when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
	when upper(vertical_group) in ('CPGS') then 'CPGs'
	else 'Others' end as vertical
	from VERTICALS_LATAM.br_VERTICALS_V2) v on v.storetype = s.type
	where v.vertical not in ('Turbo')
	group by 1,2,3,4,5,6
	having orders_so >= 2 and percentage_so >= 0.02
	'''
	df = snowflake.run_query(query)
	return df

def run_alarm(df1,df2):
	channel_alert, channel = timezones.slack_channels(country)
	current_time = timezones.country_current_time(country)

	df = pd.merge(df1, df2, how='left', left_on=df1['store_id'], right_on=df2['storeid'])
	df = df[(df['storeid'].isnull())]
	df = df.drop(['key_0','storeid'],axis=1)
	print(df)

	df['country'] = country
	df['alarm_at'] = current_time
	snowflake.upload_df_occ(df,'special_days_so_stores')

	for index, row in df.iterrows():
		rows = dict(row)
		text = '''
		*Alarma - Tiendas ofensoras Stockout - Dia dos Pais Brasil :alert:*
		Country :flag-{country}:
		Pais: {country}
		La tienda {store_name} (Store ID: {store_id}) tiene {orders_so} pedidos con stockout de un total de {all_orders} pedidos
		Porcentaje de pedidos: {percentage_so}%
		Order IDs: {order_ids}
		'''.format(
			store_name = row['name'],
			store_id = row['store_id'],
			orders_so = row['orders_so'],
			all_orders=row['all_orders'],
			percentage_so=round(row['percentage_so']*100,2),
			order_ids = row['order_ids'],
			country = country
			)
		print(text)
		slack.bot_slack(text,channel_alert)

country = 'br'
df2 = logs(country)
df1 = stockout_store_br(country)
run_alarm(df1,df2)