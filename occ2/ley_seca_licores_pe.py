import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from random import randrange
import pytz
import time
import datetime as dt


def get_orders(country):
	timezone, interval = timezones.country_timezones(country)

	query = """
	SET TIMEZONE TO 'America/{timezone}';
	select os.store_id, os.name, os.type, count(distinct o.id) as orders, string_agg(distinct o.id::text,',') as order_ids
	from orders o
	left join order_stores os on os.order_id=o.id
	where o.created_at >= now()::timestamp - interval '30 minutes'
	and type not in ('queue_order_flow')
	and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
	group by 1,2,3
	""".format(timezone=timezone)

	if country == 'co':
		metrics = redash.run_query(7973, query)
	elif country == 'ar':
		metrics = redash.run_query(7970, query)
	elif country == 'cl':
		metrics = redash.run_query(7972, query)
	elif country == 'mx':
		metrics = redash.run_query(7977, query)
	elif country == 'uy':
		metrics = redash.run_query(7978, query)
	elif country == 'ec':
		metrics = redash.run_query(7975, query)
	elif country == 'cr':
		metrics = redash.run_query(7974, query)
	elif country == 'pe':
		metrics = redash.run_query(7976, query)
	elif country == 'br':
		metrics = redash.run_query(7971, query)

	return metrics

def store_infos(country):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	SELECT
	  DISTINCT s.store_id,
	  s.store_brand_id as brand_id,
	  b.name as brand_name,
	  s.name AS store_name,
	  v.group AS vertical,
	  v.sub_group,
	  stg.store_type,
	  s.created_at,
	  c.city
	FROM
	  store_types st
	  LEFT JOIN verticals v ON st.id = v.store_type
	  LEFT JOIN stores s ON v.store_type = s.type
	  LEFT JOIN store_type_groups stg ON st.id = stg.store_type
	  left join store_brands b on s.store_brand_id = b.id
	  LEft join city_addresses c on s.city_address_id = c.id
	WHERE
	  v.group = 'CPGs' ---OR v.group = 'ecommerce'
	   AND s.deleted_at IS NULL and s.store_id is not null 
	   AND v.sub_group = 'Licores'
	ORDER BY
	  s.name asc'''
	df = redash.run_query(673, query)
	return df


def metrics(df,stores):
	current_time = timezones.country_current_time(country)

	df = pd.merge(df, stores, how='inner',on=['store_id'])
	print(df)

	df0 = df.groupby(["sub_group"])[["orders"]].sum().reset_index()
	df1 = df.groupby(["sub_group"])["order_ids"].apply(list).reset_index(name='order_ids')
	df1['order_ids'] = [','.join(map(str, l)) for l in df1['order_ids']]
	df2 = df.groupby(["sub_group"])["store_id"].apply(list).reset_index(name='store_ids')
	df2['store_ids'] = [','.join(map(str, l)) for l in df2['store_ids']]
	df = pd.merge(df0, df1, how='inner',on=['sub_group'])
	df = pd.merge(df, df2, how='inner',on=['sub_group'])

	print(df)

	df = df[df['orders'] >= 1]
	df = df.rename(columns={'sub_group':'vertical'})
	print(df)

	for index, row in df.iterrows():
		text = '''
	*Tiendas Prendidas Despues Horario Limite*  - Ley Seca :clock930:
	:alert:

	*Country* :flag-{country}:

	*Vertical*: {vertical}

	*Orders:* {orders}

	:page_facing_up: *Orders IDs:* {order_ids}

	:page_with_curl: *Store IDs:* {store_ids}

		'''.format(
		vertical=row['vertical'],
		order_ids=row['order_ids'],
		store_ids=row['store_ids'],
		orders=row['orders'],
		country=country
		)
		print(text)
		slack.bot_slack(text, 'C015QR8KT62')

	df['sync_at'] = current_time
	df['country'] = country

	snow.upload_df_occ(df, 'ley_seca_licores_pe')


countries = ['pe']
for country in countries:
	try:
		orders = get_orders(country)
		stores = store_infos(country)
		metrics(orders,stores)
	except Exception as e:
		print(e)
