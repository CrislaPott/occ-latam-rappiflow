#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# !jupyter nbconvert --to script cancelaciones_col_cl.ipynb


# In[126]:


import os, sys, json
import numpy as np
import pandas as pd
from time import sleep
from datetime import datetime, timedelta
from dotenv import load_dotenv
from os.path import expanduser
import gspread
from df2gspread import df2gspread as d2g
from oauth2client.service_account import ServiceAccountCredentials
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
from openpyxl import load_workbook
import pytz

# In[106]:


def process(country, canal):

    if country == "CO":
        country = "CO"
        country_min = "co"
        time_zones = "Bogota"
        BD = 1904
        BD1 = 6164
        tag_id = 325885
        flag = "\U0001F1E8\U0001F1F4"

    elif country == "CL":
        country = "CL"
        country_min = "cl"
        time_zones = "Santiago"
        BD = 1155
        BD1 = 6093
        tag_id = 151
        flag = "\U0001F1E8\U0001F1F1"

    # # Cancels

    # In[107]:

    print("init_", country)
    cancels = redash.run_query(
        BD,
        """
        --no_cache
        with base as
        (SELECT order_id
        FROM order_modifications om
        join (select max(created_at) max_c from order_modifications) om2 on om.created_at >= max_c - interval '30 minutes' 
        WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}' - interval '55 minutes')
        AND (TYPE IN ('close_order','arrive','finish_order','close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take') OR TYPE ILIKE '%cancel%')
        GROUP BY 1
        )
        select coalesce(o2.store_id,0) as store_id, coalesce(os.name,'whim') as store_name, case when city_address_id is null then 0 else city_address_id end as city_address_id, 
        case when init_store_type in ('whim') then 'whim' else os.type end as store_type, 
        count(distinct base.order_id) as orders, 
        count(distinct case when o.state ilike '%cancel%' then base.order_id else null end) as canceladas,
        string_agg(distinct case when o.state ilike '%cancel%' then base.order_id::text else null end,';') as order_ids
        from base
        left join calculated_information.orders o2 on o2.order_id=base.order_id
        left join order_stores os on os.order_id=o2.order_id and os.store_id=o2.store_id and os.contact_email not like '%@synth.rappi.com%'
        join orders o on o.id=base.order_id and o.state not in ('canceled_for_payment_error', 'canceled_by_fraud', 'canceled_by_split_error','canceled_by_split_error','canceled_by_early_regret')
        where (os.type not in ('rappi_pay','queue_order_flow','soat_webview') and os.type not ilike '%rappi%pay%' or os.type is null)
        and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
        and o.payment_method not ilike '%synthe%'
        and os.name not ilike '%Dummy Store%'
        group by 1,2,3,4""".format(
            timezone=time_zones
        ),
    )

    print(cancels.shape)
    cancels.head()

    # # Verticals

    # In[108]:

    verticals = snow.run_query(
        """select store_type as storetype,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
        when vertical_group = 'WHIM' then 'Antojos'
        when vertical_group = 'RAPPICASH' then 'RappiCash'
        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
        when store_type in ('turbo') then 'Turbo'
        when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
        when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
        when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
        when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
        when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
        when upper(vertical_group) in ('CPGS') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country_min}_VERTICALS_V2""".format(
            country_min=country_min
        )
    )

    print(verticals.shape)
    verticals.head()

    # # Cities

    # In[109]:

    cities = redash.run_query(
        BD1,
        """
        select ca.id, ca.city
        from city_addresses ca
        join countries c on c.id=ca.country_id and code_iso_2='{country}' """.format(
            country=country
        ),
    )

    print(cities.shape)
    cities.head()

    # # store_infos

    # In[110]:

    store_infos = snow.run_query(
        """ 
    select a.store_id as id_store, 
    max(a.type) as storetype,
    max(c.id) as brand_id, 
    max(c.name) as brand_name, 
    max(case when t.tag_id is not null then 'ccp' else null end) as tag

    from {country_min}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country_min}_PGLR_MS_STORES_PUBLIC.store_brands_vw c on c.id=a.store_brand_id
    left join {country_min}_PGLR_MS_STORES_PUBLIC.store_tag t on t.store_id=a.store_id and t.tag_id = {tag_id}
    group by 1
    """.format(
            country_min=country_min, tag_id=tag_id
        )
    )

    print(store_infos.shape)
    store_infos.head()

    # # Cancels real time

    # In[111]:

    if len(cancels) > 0:

        # merging a with b in order to get the verticals
        df = pd.merge(
            cancels,
            verticals,
            how="left",
            left_on=["store_type"],
            right_on=["storetype"],
        ).drop(columns=["storetype"])

        print(df.shape)
        df.head()

        # data by verticals
        vertical_df = (
            df.groupby(["vertical"])[["canceladas", "orders"]].sum().reset_index()
        )
        vertical_df["Percentage"] = (
            vertical_df["canceladas"] / vertical_df["orders"]
        ) * 100
        vertical_df = vertical_df.loc[~((vertical_df['canceladas'] == 1) & (vertical_df['orders'] == 1))]

        print(vertical_df.shape)
        print('change')
        vertical_df.head()

        # # Cancels history

        # In[114]:

        cancels_history = snow.run_query(
            """
        with verticals as
        (select store_type as storetype,
        case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
        when vertical_group = 'RESTAURANTS' then 'Restaurantes'
        when vertical_group = 'WHIM' then 'Antojos'
        when vertical_group = 'RAPPICASH' then 'RappiCash'
        when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
        when store_type in ('turbo') then 'Turbo'
        when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
        when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
        when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
        when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
        when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
        when upper(vertical_group) in ('CPGS') then 'CPGs'
        else 'Others' end as vertical
        from VERTICALS_LATAM.{country}_VERTICALS_V2)

        select coalesce(closed_at,created_at)::date as fecha, 
        v.vertical, 
        count(distinct o.order_id) as total_orders,
        count(distinct case when o.order_state ilike '%cancel%' then o.order_id else null end) as cancels,
        cancels/total_orders as percentage
        from OPS_GLOBAL.global_orders o
        left join verticals v
        on v.storetype=o.store_type
        where o.country='{country}'
        and to_char(coalesce(o.closed_at,o.created_at),'HH:MI') >= to_char(dateadd(minutes,-30,convert_timezone('America/{timezone}',current_timestamp())),'HH:MI')
        and to_char(coalesce(o.closed_at,o.created_at),'HH:MI') < to_char(dateadd(minutes,0,convert_timezone('America/{timezone}',current_timestamp())),'HH:MI')
        and extract(dow from fecha) = extract(dow from (convert_timezone('America/{timezone}',current_timestamp())))
        and fecha >= dateadd(day,-28, current_date())::date
        and fecha < convert_timezone('America/{timezone}',current_timestamp())::date
        group by 1,2
        order by 2 asc, 1 desc""".format(
                timezone=time_zones, country=country
            )
        )

        cancels_history["percentage"] = cancels_history["percentage"].astype(float)

        print(cancels_history.shape)
        cancels_history.head()

        # In[115]:

        o_cancels = cancels_history.groupby(["vertical"])[
            ["cancels", "percentage"]
        ].agg(["sum", "median", np.std])
        o_cancels = o_cancels.replace(np.nan, 0)
        o_cancels.columns = ["_".join(col).strip() for col in o_cancels.columns.values]
        o_cancels.reset_index(inplace=True)

        print(o_cancels.shape)
        o_cancels.head()

        # In[ ]:

        o_cancels.loc[o_cancels["cancels_median"] < 3, "cancels_median"] = 3
        o_cancels["cancels_proportion"] = (
            o_cancels["cancels_std"] / o_cancels["cancels_median"]
        )
        o_cancels.loc[
            ~np.isfinite(o_cancels["cancels_proportion"]), "cancels_proportion"
        ] = 0

        o_cancels["percentage_proportion"] = (
            o_cancels["percentage_std"] / o_cancels["percentage_median"]
        )
        o_cancels.loc[
            ~np.isfinite(o_cancels["percentage_proportion"]), "percentage_proportion"
        ] = 0

        print(o_cancels.shape)
        o_cancels.head(11)

        # # Operations

        # In[116]:

        # Opertation cancelations

        o_cancels["threshold"] = 0

        for index, row in o_cancels.iterrows():

            if row["cancels_proportion"] < 0.3:
                o_cancels.loc[index, "threshold"] = row["cancels_median"] + (
                    2 * row["cancels_std"]
                )

            elif (row["cancels_proportion"] >= 0.3) & (row["cancels_proportion"] < 0.5):
                o_cancels.loc[index, "threshold"] = row["cancels_median"] + (
                    1 * row["cancels_std"]
                )

            elif (row["cancels_proportion"] >= 0.5) & (
                row["cancels_proportion"] < 0.95
            ):
                o_cancels.loc[index, "threshold"] = row["cancels_median"] + (
                    0.5 * row["cancels_std"]
                )

            elif row["cancels_proportion"] >= 0.95:
                o_cancels.loc[index, "threshold"] = row["cancels_median"] + (
                    0.25 * row["cancels_std"]
                )

            elif row["cancels_proportion"] == 0:
                o_cancels.loc[index, "threshold"] = 0

        print(o_cancels.shape)
        o_cancels.head()

        # In[ ]:

        # Opertation cancelations rate

        o_cancels["rate_threshold"] = 0

        for index, row in o_cancels.iterrows():

            if row["percentage_proportion"] < 0.15:
                o_cancels.loc[index, "rate_threshold"] = 100 * (
                    row["percentage_median"] + (2 * row["percentage_std"])
                )

            elif (row["percentage_proportion"] >= 0.15) & (
                row["percentage_proportion"] < 0.35
            ):
                o_cancels.loc[index, "rate_threshold"] = 100 * (
                    row["percentage_median"] + (1.5 * row["percentage_std"])
                )

            elif (row["percentage_proportion"] >= 0.35) & (
                row["percentage_proportion"] < 0.5
            ):
                o_cancels.loc[index, "rate_threshold"] = 100 * (
                    row["percentage_median"] + (1.25 * row["percentage_std"])
                )

            elif row["percentage_proportion"] >= 0.5:
                o_cancels.loc[index, "rate_threshold"] = 100 * (
                    row["percentage_median"] + (0.5 * row["percentage_std"])
                )

            elif row["percentage_proportion"] == 0:

                o_cancels.loc[index, "rate_threshold"] = 0

        print(o_cancels.shape)
        o_cancels.head(11)

        # # Final table

        # In[118]:

        final_df = pd.merge(
            vertical_df,
            o_cancels[["threshold", "rate_threshold", "vertical"]],
            how="left",
            left_on=["vertical"],
            right_on=["vertical"],
        )

        final_df["threshold"] = final_df["threshold"].replace([np.nan, np.inf], 0)
        final_df["rate_threshold"] = final_df["rate_threshold"].replace([np.nan, np.inf], 0)

        final_df["threshold"] = final_df["threshold"].astype(int)

        print(final_df.shape)
        final_df.head(10)

        # In[119]:

        print(
            final_df.shape,
            "\n Con Alarma: ",
            final_df[
                (final_df["canceladas"] > final_df["threshold"])
                & (final_df["Percentage"] > final_df["rate_threshold"])
            ].shape,
            "\n Sin Alarma: ",
            final_df[
                ~(
                    (final_df["canceladas"] > final_df["threshold"])
                    & (final_df["Percentage"] > final_df["rate_threshold"])
                )
            ].shape,
        )

        # In[120]:

        favor = final_df.loc[
            (final_df["vertical"].isin(["RappiFavor"]))
            & (final_df["canceladas"] > final_df["threshold"])
            & (final_df["Percentage"] > final_df["rate_threshold"])
        ]
        favor = favor[["vertical", "canceladas", "orders", "Percentage"]]
        favor.head()

        # In[121]:

        df_verticals_list = [favor]

        # # Excel

        # In[122]:

        grouped_df = (
            df.groupby(
                ["store_id", "store_name", "vertical", "city_address_id", "order_ids"]
            )[["canceladas", "orders"]]
            .sum()
            .reset_index()
        )
        grouped_df["Percentage"] = round(
            (grouped_df["canceladas"] / grouped_df["orders"]) * 100, 2
        )
        print(grouped_df.shape)
        grouped_df.head()

        # In[123]:

        # merging grouped_df with c and d in order to get the store cities and other store informations

        final_sheets_df = pd.merge(
            grouped_df, cities, how="left", left_on=["city_address_id"], right_on=["id"]
        ).drop(columns=["id"])
        final_sheets_df = pd.merge(
            final_sheets_df,
            store_infos,
            how="left",
            left_on=["store_id"],
            right_on=["id_store"],
        ).drop(columns=["id_store"])
        final_sheets_df = final_sheets_df[
            [
                "store_id",
                "store_name",
                "tag",
                "storetype",
                "vertical",
                "brand_id",
                "brand_name",
                "city",
                "order_ids",
                "canceladas",
                "orders",
                "Percentage",
            ]
        ]

        print(final_sheets_df.shape)
        final_sheets_df.head()

        # # Mensaje

        # In[131]:

        channel_alert = canal

        for df in df_verticals_list:

            for index, row in df.iterrows():

                rows = dict(row)
                vertical = df["vertical"].to_list()
                vertical = "".join(vertical)
                sheets = final_sheets_df[
                    (final_sheets_df["vertical"] == vertical)
                ].sort_values(by="canceladas", ascending=False)
                home = expanduser("~")
                results_file = "{}/{}_details.xlsx".format(home, vertical)
                sheets.to_excel(results_file, sheet_name="Stores", index=False)
                book = load_workbook(results_file)
                writer = pd.ExcelWriter(results_file, engine="openpyxl")
                writer.book = book
                writer.sheets = dict((ws.title, ws) for ws in book.worksheets)

                sheets_brands = (
                    sheets.groupby(["brand_id", "brand_name", "vertical"])[
                        ["canceladas", "orders"]
                    ]
                    .sum()
                    .reset_index()
                )
                sheets_brands["Percentage"] = round(
                    (sheets_brands["canceladas"] / sheets_brands["orders"]) * 100, 2
                )
                sheets_brands = sheets_brands.sort_values(
                    by="canceladas", ascending=False
                )

                sheets_cities = (
                    sheets.groupby(["city"])[["canceladas", "orders"]]
                    .sum()
                    .reset_index()
                )
                sheets_cities["Percentage"] = round(
                    (sheets_cities["canceladas"] / sheets_cities["orders"]) * 100, 2
                )
                sheets_cities = sheets_cities.sort_values(
                    by="canceladas", ascending=False
                )

                sheets_brands.to_excel(writer, "Brands", index=False)
                sheets_cities.to_excel(writer, "Cities", index=False)

                writer.save()

                text = """
                \U000026A0 *Pico de Cancelación en Vertical* \U000026A0
                {flag}:
                Pais: {country}
                Vertical *{vertical}* alcanzó el *{Percentage:.2f}% de cancelación* en los últimos 30 minutos \U0001F4C8
                *Cancelados:* {canceladas}
                *Ordenes:* {orders}
                Lista de tiendas infractoras \U00002B07
                """.format(
                    country=country,
                    flag=flag,
                    vertical=row["vertical"],
                    Percentage=row["Percentage"],
                    canceladas=row["canceladas"],
                    orders=row["orders"],
                )

                print(text)

                slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
                to_upload = df
                to_upload["alarm_at"] = datetime.now(
                    pytz.timezone("America/{timezone}".format(timezone=time_zones))
                )
                to_upload["country"] = country_min
                snow.upload_df_occ(to_upload, "vertical_cancellation_spike")
                print(to_upload)

                to_upload2 = sheets
                to_upload2["alarm_at"] = datetime.now(
                    pytz.timezone("America/{timezone}".format(timezone=time_zones))
                )
                to_upload2["country"] = country_min
                snow.upload_df_occ(to_upload2, "vertical_cancellation_spike_details")
                print(to_upload2)
        print("end_", country)

    else:

        print("no cancels end_", country)
