import os, sys, json
import numpy as np
import pandas as pd
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
     --no_cache
WITH base AS
  ( SELECT o.id,
           CASE
               WHEN EXTRACT (SECOND
                             FROM o.place_at) = 0
                    AND EXTRACT (MINUTE
                                 FROM o.place_at) = 0
                    AND o.place_at IS NOT NULL
                    AND o.place_at > o.taked_at THEN o.taked_at
               WHEN EXTRACT (SECOND
                             FROM o.place_at) = 0
                    AND EXTRACT (MINUTE
                                 FROM o.place_at) = 0
                    AND o.place_at IS NOT NULL
                    AND o.place_at < o.taked_at THEN o.place_at
               ELSE o.created_at
           END AS begins_at,
           state,
           min(om.created_at) AS finished_at
   FROM orders o
   JOIN order_modifications om ON om.order_id=o.id
   AND om.type IN ('closed_order',
                   'arrive')
   WHERE o.created_at >= (now() AT TIME ZONE 'america/{timezone}') - interval '2h'
   GROUP BY 1,
            2,
            3 ) ,
     dts AS
  ( SELECT os.store_id,
           os.name AS name,
           os.type AS type,
           b.id AS order_id,
           begins_at,
           (extract(epoch
                    FROM finished_at - b.begins_at) / 60) AS dt
   FROM base b
   JOIN order_stores os ON os.order_id=b.id)
   
   , final as (
SELECT store_id,
       name,
        type,
       count(DISTINCT CASE
                          WHEN dt >= 20 THEN order_id
                          ELSE NULL
                      END) AS orders_above_20,
                      
       count(DISTINCT order_id) AS orders,
       
       count(DISTINCT CASE
                          WHEN dt >= 20 THEN order_id
                          ELSE NULL
                      END)::float/count(DISTINCT order_id)::float as percentage_above_20,
                      string_agg(CASE
                          WHEN dt >= 20 THEN order_id::text
                          ELSE NULL
                      END,',') as order_ids_above20
       
FROM dts
WHERE extract('hour'
              FROM begins_at) = extract('hour'
                                        FROM now() AT TIME ZONE 'america/{timezone}') - 1
GROUP BY 1,
         2,
         3)
         
         select * from final where orders_above_20 >= 3
         and percentage_above_20 >= 0.20
    """.format(countrt=country,timezone=timezone)

    query_verticals ='''
    --no_cache
    select store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2'''.format(country=country)

    verticals = snow.run_query(query_verticals)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics, verticals

def run_alarm(a,b):
    channel_alert = timezones.slack_channels('tech_integration')

    current_time = timezones.country_current_time(country)
    a = pd.merge(a,b,how='left',left_on=a['type'],right_on=b['store_type'])
    a = a.drop(['key_0'], axis=1)
    df = a[(a['vertical'].isin(['Turbo']))]
    print(df)
    df['alarm_at'] = current_time
    df['country'] = country

    snow.upload_df_occ(df,'occ_turbo_alarm_dt')

    for index, row in df.iterrows():
            rows = dict(row)
            text = '''
            :alert: Turbo Store Delays :turbo-10-min: 
            :flag-{country}:
            Pais: {country}
            En la última hora, la tienda {name} (store id: {store_id}) obtuvo el {percentage_above20}% de los pedidos con más de 20 minutos de tiempo de entrega
            Orders above 20 min: {orders_above20}
            Total Orders: {orders}
            Orders IDs avove 20 min: {order_ids_above20}
            '''.format(country=country,
                name = row['name'],
                store_id = row['store_id'],
                percentage_above20 = round(row['percentage_above_20'],2)*100,
                orders_above20 = row['orders_above_20'],
                order_ids_above20 = row['order_ids_above20'],
                orders = row['orders'])
            print(text)
            slack.bot_slack(text,channel_alert)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
  try:
    print(country)
    a,b = offenders(country)
    run_alarm(a,b)
  except Exception as e:
    print(e)