import os, sys, json
import numpy as np
import pandas as pd
from os.path import expanduser
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones


def excluding_stores(country):
    timezone, interval = timezones.country_timezones(country)

    query = '''
    select storeid, max(metric) as metric 
    from
    (select storeid::text as storeid,
    last_value(metric) over (partition by storeid order by alarm_at asc) as metric
    from ops_occ.occ_turbo_alarm
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
    and country='{country}'
    )
    group by 1
    '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def offenders(country):
    timezone, interval = timezones.country_timezones(country)

    query = """
    --no_cache
    with base as
    (
    SELECT case 
    when type in ('cancel_by_support') then 'cancel_by_support'
    when type in ('cancel_by_restaurant_integration') then 'cancel_by_restaurant_integration'
    when type in ('refuse_order_by_partner','refuse_partner_after_take') then 'cbpr'
    when type in ('cancel_by_user') then 'cancel_by_user'
    when type in ('canceled_by_automation') then 'canceled_by_automation'
    when type in ('canceled_by_partner_phase') then 'canceled_by_partner_phase'
    when type in ('canceled_store_closed') then 'canceled_store_closed'
    when type in ('remove_by_partner_inactivity') then 'cbpi'
    else 'finished_order' end as big_group_reasons, 
    (params->>'cancelation_reason') sub_reasons, 
    order_id, 
    max(created_at) as terminated
    FROM order_modifications om
    WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}')::date - interval '2days'
    AND (TYPE IN ('close_order','arrive','finish_order',
    'close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take','remove_by_partner_inactivity') 
    OR TYPE ILIKE '%cancel%')
    and (params->>'cancelation_reason')::text not in ('crsan_inner_test')
    group by 1,2,3
    )

  , inter2 as (
    select os.store_id::text as store_id, sub_reasons, 
    count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end) as contd0
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
    inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
    where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
    and o.state ilike '%cancel%'
    group by grouping sets ((os.store_id), (os.store_id, sub_reasons)))
    
  , inter3 as (
    select os.store_id::text as store_id, big_group_reasons, 
    count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end) as contd0_2
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
    inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
    where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
    and o.state ilike '%cancel%'
    and big_group_reasons not in ('finished_order')
    group by grouping sets ((os.store_id), (os.store_id, big_group_reasons)))

  , inter as (  
    select os.store_id::text as store_id, max(os.name) as name, max(os.type) as type,
    count(distinct case when o.state ilike '%cancel%' and terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end) cancels_day_0,
    count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end) as orders_day_0,
    count(distinct case when o.state ilike '%cancel%' and terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end)::float/nullif(count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date then o.id else null end)::float,0) as percentage_day_0,
    string_agg(distinct case when o.state ilike '%cancel%' and terminated::date = (now() at time zone 'america/{timezone}')::date then o.id::text else null end,',') as order_ids_d0,
    count(distinct case when o.state ilike '%cancel%' and terminated::date = (now() at time zone 'america/{timezone}')::date - interval '1d' and to_char(terminated,'HH:MI') <= to_char(now() at time zone 'america/{timezone}', 'HH:MI') then o.id else null end) cancels_day_1,
    count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date - interval '1d' and to_char(terminated,'HH:MI') <= to_char(now() at time zone 'america/{timezone}', 'HH:MI') then o.id else null end) as orders_day_1,
    count(distinct case when o.state ilike '%cancel%' and terminated::date = (now() at time zone 'america/{timezone}')::date - interval '1d' and to_char(terminated,'HH:MI') <= to_char(now() at time zone 'america/{timezone}', 'HH:MI') then o.id else null end)::float/nullif(count(distinct case when terminated::date = (now() at time zone 'america/{timezone}')::date and to_char(terminated,'HH:MI') <= to_char(now() at time zone 'america/{timezone}', 'HH:MI') then o.id else null end)::float,0) as percentage_day_d1
    
    from base
    inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
    inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
    where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
    and (os.type not in ('rappi_pay','queue_order_flow') or os.type is null)
    and (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
    group by 1
    )
    , select_stores as (
    select * from inter
    where (percentage_day_0 >= (percentage_day_d1 + 0.20))
    )
    select ss.store_id,
           ss.name, 
           ss.type,
           ss.cancels_day_0,
           ss.orders_day_0,
           ss.percentage_day_0,
           ss.order_ids_d0,
           ss.percentage_day_d1
    from select_stores ss
    left join inter2 i2 on i2.store_id::text=ss.store_id::text
    left join inter3 i3 on i3.store_id::text=ss.store_id::text
    where ss.cancels_day_0 >= 3
    group by 1,2,3,4,5,6,7,8
    """.format(countrt=country,timezone=timezone)

    query_verticals ='''
    --no_cache
    select store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as vertical
from VERTICALS_LATAM.{country}_VERTICALS_V2'''.format(country=country)

    verticals = snow.run_query(query_verticals)

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics, verticals

def new_offenders(a,b,d):
    channel_alert = timezones.slack_channels('tech_integration')

    current_time = timezones.country_current_time(country)

    reported_stores = d

    a = pd.merge(a,b,how='left',left_on=a['type'],right_on=b['store_type'])
    a = a.drop(['key_0'], axis=1)
    print(a)
    df = a[(a['vertical'].isin(['Turbo']))]
    print(df)
    
    df = df[["store_id","name","type","vertical","orders_day_0","cancels_day_0","percentage_day_0","percentage_day_d1","order_ids_d0"]]
    news = pd.merge(df,reported_stores,how='left',left_on=df['store_id'],right_on=reported_stores['storeid'])
    news = news.rename(columns={"metric": "last_report"})
    news['last_report'].fillna(0, inplace = True)
    news = news[((news['storeid'].isnull()) | (news['percentage_day_0'] >= (news['last_report'] + (news['last_report'] + 0.20))))]
    news = news.drop(['key_0','storeid'], axis=1)
    if not news.empty:
        text = '''
        *Turbo Cancellation vs D-1 - :flag-{country}:*
        Pais: {country}
        :alert:
        Las tiendas en anexo alcanzaron 20% o más de cancelación en el díade hoy vs D-1
        Estas tiendas ya no serán notificadas a menos que alcancen 20% de aumento de cancelación en las próximas horas'''.format(
            data=current_time,
            country=country
            )
        home = expanduser("~")
        results_file = '{}/turbo_cancellation_{country}.xlsx'.format(home,country=country)
        news.to_excel(results_file,sheet_name='stores', index=False)
        slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
        
        news = news.rename(columns={"store_id": "storeid", "percentage_day_0": "metric"})
        to_upload = news
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        print(to_upload)
        to_upload = to_upload[['storeid','metric','alarm_at','country']]
        snow.upload_df_occ(to_upload,'occ_turbo_alarm')

def run_alarm(country):
    d = excluding_stores(country)
    a,b = offenders(country)
    new_offenders(a,b,d)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
    print(country)
    try:
        run_alarm(country)
    except Exception as e:
        print(e)
