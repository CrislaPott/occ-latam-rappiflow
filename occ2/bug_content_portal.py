import os, sys, json
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones, snippets
import numpy as np
from fuzzywuzzy import fuzz

def logs():

    query = '''
    select product_id::text as productid,country,alarm_at
    from ops_occ.bug_content_portal
    where alarm_at::date = current_date
    '''
    df = snow.run_query(query)

    return df


def get_dataframe(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''
    --no_cache
with orders as (
    select distinct op.product_id as product_id, max(op.store_id) as store_id, count(distinct o.id) as ordenes,
                    listagg(distinct o.id::text,',') as order_ids
    from {country}_CORE_ORDERS_PUBLIC.orders_vw o
    join {country}_CORE_ORDERS_PUBLIC.order_product_vw op on op.order_id=o.id
    where o.created_at::date = current_date::date
    group by 1
)
select p.id as product_id, p.name as product_name,
       p.ean as ean,
       ps.price,
       p.description,
       ps.store_id,
       ps.is_discontinued,
       lm.product_id as master_product,
       p2.name as master_name,
       p2.ean as master_ean,
       p2.long_description, ordenes, order_ids
from {country}_pglr_ms_grability_products_public.products_vw p
left join {country}_pglr_ms_grability_products_public.product_store_vw ps on ps.product_id=p.id
join {country}_pglr_ms_cpgs_clg_pm_public.legacy_mapping lm on lm.legacy_product_id=p.id and lm._fivetran_deleted=false
join {country}_PGLR_MS_CPGS_CLG_PM_PUBLIC.PRODUCT p2 on p2.id=lm.product_id
join {country}_AMYSQL_CPGS_CLG_IM_CPGS_CLG_INVENTORY_MANAGER.VIRTUAL_STORE vs on vs.id=ps.store_id and vs.status='published'
join orders o on o.product_id=p.id and ps.store_id=o.store_id
where p2.ean <> p.ean
and len(p.ean) = 13
and p.deleted_at is null
and is_discontinued=false
and p2.status='published'
and in_stock=true
and is_available=true
    '''.format(timezone=timezone, country=country)
    df = snow.run_query(query)
    return df


def partial_match(x, y):
    return (fuzz.ratio(x, y))

partial_match_vector = np.vectorize(partial_match)

def alarm_price_different(df,df_logs):
    current_time = timezones.country_current_time(country)
    channel_alert = timezones.slack_channels('tech_integration')
    print(df)
    if not df.empty:

        df['fuzzy_name'] = partial_match_vector(df['product_name'], df['master_name'])
        df['fuzzy_desctiption'] = partial_match_vector(df['description'], df['long_description'])
        df = df[(df['fuzzy_name'] <= 10)]

        df = df[['product_id', 'product_name', 'price', 'ean', 'description', 'store_id','order_ids',
                 'master_name', 'master_product', 'master_ean', 'long_description']]
        df = df.rename(columns={'master_product': 'id_master_product'})

        df['product_id'] = df['product_id'].astype(str)

        df = pd.merge(df, df_logs, how='left', left_on=df['product_id'], right_on=df_logs['productid'])
        df = df[(df['productid'].isnull())]
        df = df.drop(['key_0', 'productid'], axis=1)
        print(df)

        df['country'] = country
        df['alarm_at'] = current_time

        snow.upload_df_occ(df, 'bug_content_portal')

        try:
            for index, row in df.iterrows():
                row = dict(row)
                text = '''*Alarma - Diferencia de precio producto maestro incorrecto * :alert:
                Country: {country} :flag-{country}:
                Productos con el nombre {product_name} e Product ID ({product_id}) tienen una mala asociación con el producto maestro.
                Precio: {price}
                Ean: {ean}
                Descripción: {description}
                Store ID: {store_id}
                Order IDs: {order_ids}

                Parámetros del producto maestro
                Nombre: {master_name}
                ID del producto maestro: {id_master_product}
                Ean: {master_ean}
                Descripción: {long_description}
                '''.format(
                    product_name=row['product_name'],
                    product_id=row['product_id'],
                    price=row['price'],
                    ean=row['ean'],
                    description=row['description'],
                    store_id=row['store_id'],
                    order_ids=row['order_ids'],
                    master_name=row['master_name'],
                    id_master_product=row['id_master_product'],
                    master_ean=row['master_ean'],
                    long_description=row['long_description'],
                    country=country
                )
                print(text)
                slack.bot_slack(text, channel_alert)

        except Exception as e:
            print(e)

    else:
        print('no se aplica')


countries = ['mx','br','co']
logs1 = logs()
print(logs1)
for country in countries:
    print(country)
    try:
        df_logs=logs1[logs1['country']==country]
        df = get_dataframe(country)
        alarm_price_different(df,df_logs)
    except Exception as e:
        print(e)