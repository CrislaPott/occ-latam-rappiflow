from lib import snowflake as snow
from functions import timezones, snippets
import pandas as pd
from lib import slack
import numpy as np
from datetime import datetime,timedelta
from os.path import expanduser

def store_close_bot():

    query = '''
      select date_trunc('hour', SUSPENDED_AT::timestamp_ntz)::timestamp_ntz as suspended_at,
        b.COUNTRY as country,
        b.STORE_ID as store_id,
        s.name as physical_store_name, b.cancels,  b.order_ids, b.vertical, b.category
        from ops_occ.worst_offenders_bleeding b
LEFT JOIN (
SELECT 'BR' AS COUNTRY, ID, NAME FROM BR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'CO' AS COUNTRY, ID, NAME FROM CO_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'MX' AS COUNTRY, ID, NAME FROM MX_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'AR' AS COUNTRY, ID, NAME FROM AR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'CL' AS COUNTRY, ID, NAME FROM CL_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'PE' AS COUNTRY, ID, NAME FROM PE_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'EC' AS COUNTRY, ID, NAME FROM EC_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'CR' AS COUNTRY, ID, NAME FROM CR_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores
UNION ALL
SELECT 'UY' AS COUNTRY, ID, NAME FROM UY_PG_MS_CPGOPS_STORES_MS_PUBLIC.physical_stores)
S ON S.COUNTRY = UPPER(B.COUNTRY) AND S.ID = B.STORE_ID
where SUSPENDED_AT::date = current_date
order by 1 asc
    '''
    df = snow.run_query(query)
    return df


def alarm(df):
    if not df.empty:

        for i in np.arange(0, len(df.country), 1):
            if df.country[i] == 'ar' or df.country[i] == 'br' or df.country[i] == 'uy':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Buenos_Aires', nonexistent='shift_forward')
            elif df.country[i] == 'cl':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Santiago', nonexistent='shift_forward')
            elif df.country[i] == 'co':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Bogota', nonexistent='shift_forward')
            elif df.country[i] == 'cr':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Costa_Rica', nonexistent='shift_forward')
            elif df.country[i] == 'ec':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Guayaquil', nonexistent='shift_forward')
            elif df.country[i] == 'mx':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Mexico_City', nonexistent='shift_forward')
            elif df.country[i] == 'pe':
                df.suspended_at[i] = df.suspended_at[i].tz_localize('America/Lima', nonexistent='shift_forward')


        df.suspended_at = pd.to_datetime(df.suspended_at, utc=True).dt.tz_convert('America/Bogota')
        current_hour = int(timezones.country_current_time('co').hour) - 1
        print(current_hour)
        df['hour'] = df.suspended_at.dt.hour.astype(int)
        df = df[df.hour == current_hour]



        if not df.empty:
            df = df.reset_index()
            total_alarms = df.store_id.count()
            print(total_alarms)

            df_store = df.groupby(['country'])['store_id'].agg(['count', ('store_id', lambda x: ', '.join(map(str, x)))]).reset_index()

            print(df_store)

            if total_alarms>=30:
                df_total_alarms = df[['store_id','physical_store_name','cancels','order_ids','vertical', 'country','category']]
                df_total_alarms = df_total_alarms.rename(columns={'store_id': 'physical_store_id'})

                text = '''
                *Alarma Worst offenders - Tiendas Cerradas* :close:
                El bot cerró {total_alarms} tiendas en la última hora.
                <@pedro.ayres>, <@crisla.pott>, <@leandro.benasse>, <!subteam^S0191S07TND>
                '''.format(total_alarms=total_alarms)

                print(text)

                home = expanduser("~")
                results_file = '{}/worst_offenders_bot.xlsx'.format(home)
                df_total_alarms.to_excel(results_file, sheet_name='store_id', index=False)
                slack.file_upload_channel('CS7UQAMLG', text, results_file, "xlsx")
            else:
                print('null')

            if not df_store.empty:
                for index, row in df_store.iterrows():
                    if row['count'] >= 20:
                        df_country = df[df.country == row['country']]
                        df_country=df_country[['store_id','physical_store_name', 'cancels', 'order_ids', 'vertical', 'country', 'category']]
                        df_country = df_country.rename(columns={'store_id': 'physical_store_id'})

                        text = '''
                        *Alarma Worst offenders - Tiendas Cerradas* :close:
                        Country: {country} :flag-{country}:
                        El bot cerró {count} tiendas en la última hora.
                        <@pedro.ayres>, <@crisla.pott>, <@leandro.benasse>, <!subteam^S0191S07TND>
                        '''.format(country=row['country'],count=row['count'])

                        print(text)

                        home = expanduser("~")
                        results_file = '{}/worst_offenders_bot_{country}.xlsx'.format(home,country=row['country'])
                        df_country.to_excel(results_file, sheet_name='store_id', index=False)
                        slack.file_upload_channel('CS7UQAMLG', text, results_file, "xlsx")

            else:
                print('df_store null')

try:
    df = store_close_bot()
    alarm(df)
except Exception as e:
    print(e)

