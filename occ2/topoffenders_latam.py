import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from os.path import expanduser
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
import pytz
from functions import timezones

def store_metrics(country):
	timezone, interval = timezones.country_timezones(country)

	historic = '''
	select store_id::text as idstore,
       count(distinct case when o.state ilike '%cancel%'
                                    and coalesce(o.closed_at,o.updated_at)::date = dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))::date
                                    and to_char(coalesce(o.closed_at,o.updated_at),'HH:MI') <= to_char(convert_timezone('America/{timezone}', current_timestamp)::timestamp_ntz, 'HH:MI')
                           then o.id else null end) as cancelsd7,
       count(distinct case when coalesce(o.closed_at,o.updated_at)::date = dateadd(day,-7,convert_timezone('America/{timezone}', current_timestamp))::date
                                and to_char(coalesce(o.closed_at,o.updated_at),'HH:MI') <= to_char(convert_timezone('America/{timezone}', current_timestamp)::timestamp_ntz, 'HH:MI')
                      then o.id else null end) as ordersd7,
       ((cancelsd7/nullif(ordersd7,0)) *100) as percentage_d7,

       count(distinct case when o.state ilike '%cancel%'
                                    and coalesce(o.closed_at,o.updated_at)::date = dateadd(day,-14,convert_timezone('America/{timezone}', current_timestamp))::date
                                    and to_char(coalesce(o.closed_at,o.updated_at),'HH:MI') <= to_char(convert_timezone('America/{timezone}', current_timestamp)::timestamp_ntz, 'HH:MI')
                           then o.id else null end) as cancelsd14,
       count(distinct case when coalesce(o.closed_at,o.updated_at)::date = dateadd(day,-14,convert_timezone('America/{timezone}', current_timestamp))::date
                                and to_char(coalesce(o.closed_at,o.updated_at),'HH:MI') <= to_char(convert_timezone('America/{timezone}', current_timestamp)::timestamp_ntz, 'HH:MI')
                      then o.id else null end) as ordersd14,
       ((cancelsd14/nullif(ordersd14,0)) *100) as percentage_d14

from {country}_CORE_ORDERS_PUBLIC.orders_vw o
left join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id
where o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
and (coalesce(o.closed_at, o.updated_at)::date =
     dateadd(day, -14, convert_timezone('America/{timezone}', current_timestamp))::date
    or coalesce(o.closed_at, o.updated_at)::date =
       dateadd(day, -7, convert_timezone('America/{timezone}', current_timestamp))::date
    )
and (os.type not in ('rappi_pay','queue_order_flow','soat_webview') and os.type not ilike '%rappi%pay%' or os.type is null)
	and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
group by 1
	'''.format(country=country, timezone=timezone)
	df = snow.run_query(historic)
	return df

def excluding_stores(country):
	timezone, interval = timezones.country_timezones(country)

	query = '''
	select identification_id, metric 
	from
	(select store_id::text as identification_id,
	last_value(percentage_day) over (partition by identification_id order by alarm_at asc) as metric
	from ops_occ.worst_offenders_day
	where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
	and country='{country}'
	)
	group by 1,2
	'''.format(country=country, timezone=timezone)
	df = snow.run_query(query)
	return df

def offenders(country):
	timezone, interval = timezones.country_timezones(country)

	if country == 'br':
		tag_id = 318574
	elif country == 'mx':
		tag_id = 65702
	elif country == 'co':
		tag_id = 325885
	elif country == 'ar':
		tag_id = 325778
	elif country == 'cl':
		tag_id = 151
	elif country == 'pe':
		tag_id = 123
	elif country == 'uy':
		tag_id = 325759
	elif country == 'ec':
		tag_id = 121
	elif country == 'cr':
		tag_id = 121

	query = """
	--no_cache
	with base as
	(
	SELECT case 
	when type in ('cancel_by_support') then 'cancel_by_support'
	when type in ('cancel_by_restaurant_integration') then 'cancel_by_restaurant_integration'
	when type in ('refuse_order_by_partner','refuse_partner_after_take') then 'cbpr'
	when type in ('cancel_by_user') then 'cancel_by_user'
	when type in ('canceled_by_automation') then 'canceled_by_automation'
	when type in ('canceled_by_partner_phase') then 'canceled_by_partner_phase'
	when type in ('canceled_store_closed') then 'canceled_store_closed'
	when type in ('remove_by_partner_inactivity') then 'cbpi'
	else 'finished_order' end as big_group_reasons, 
	(params->>'cancelation_reason') sub_reasons, 
	order_id, 
	max(created_at) as terminated
	FROM order_modifications om
	WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}')::date
	AND (TYPE IN ('close_order','arrive','finish_order',
	'close_by_restaurant_blind','close_order_by_partner_pickup','refuse_order_by_partner','refuse_partner_after_take','remove_by_partner_inactivity') 
	OR TYPE ILIKE '%cancel%')
	group by 1,2,3
	)
	
		, inter2 as (
	select os.store_id::text as store_id, sub_reasons, count(distinct o.id) as cont,	count(distinct case when terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end) cont2
	
	from base
	inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
	inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
	where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and os.store_id not in (900148653)
	group by grouping sets ((os.store_id), (os.store_id, sub_reasons)))
	
	, inter3 as (
	select os.store_id::text as store_id, big_group_reasons, count(distinct o.id) as cont3,	count(distinct case when terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end) cont4
	
	from base
	inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
	inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
	where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and os.store_id not in (900148653)
	and big_group_reasons not in ('finished_order')
	group by grouping sets ((os.store_id), (os.store_id, big_group_reasons)))
	
	, inter as (
	select os.store_id::text as store_id, max(os.name) as name, max(os.type) as type,
	count(distinct case when o.state ilike '%cancel%' then o.id else null end) cancels_day,
	count(distinct o.id) as orders_day,
	count(distinct case when o.state ilike '%cancel%' then o.id else null end)::float/count(distinct o.id)::float as percentage_day,
	
	string_agg(distinct case when o.state ilike '%cancel%' then o.id::text else null end,',') as order_ids_day,
	count(distinct case when o.state ilike '%cancel%' and terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end) cancels_last_hour,
	count(distinct case when terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end) orders_last_hour,
	count(distinct case when o.state ilike '%cancel%' and terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end)::float/nullif(count(distinct case when terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id else null end)::float,0) as percentage_last_hour,
	string_agg(distinct case when o.state ilike '%cancel%' and terminated >= now() at time zone 'america/{timezone}' - interval '60 minutes' then o.id::text else null end,',') as order_ids_last_hour
	
	from base
	inner join orders o on o.id=base.order_id and o.payment_method not ilike '%synthe%'
	inner join order_stores os on os.order_id = o.id and os.contact_email not like '%@synth.rappi.com%'
	where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret')
	and os.store_id not in (900148653)
	and (os.type not in ('rappi_pay','queue_order_flow') and os.type not ilike '%rappi%pay%' or os.type is null)
	and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
	and os.name not ilike '%Dummy%Store%'
	group by 1)

,select_stores as (
	select * from inter
	where ((percentage_day >= 0.10 and cancels_day >= 4)
	or (percentage_last_hour >= 0.10 and cancels_last_hour >=2)))
	
	select ss.*, 
	string_agg(distinct big_group_reasons || ': ' || cont3, ', ') as big_group_reasons_reasons_day, string_agg(distinct big_group_reasons || ': ' || cont4, ', ') as big_group_reasons_last_hour,
	string_agg(distinct sub_reasons || ': ' || cont, ', ') as sub_reasons_day, string_agg(distinct sub_reasons || ': ' || cont2, ', ') as sub_reasons_last_hour
	from select_stores ss
	left join inter2 i2 on i2.store_id::text=ss.store_id::text
	left join inter3 i3 on i3.store_id::text=ss.store_id::text
	group by 1,2,3,4,5,6,7,8,9,10,11
	""".format(countrt=country,timezone=timezone)

	query_verticals = """ 
	--no_cache
	select store_type,
	case when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
	when upper (sub_group) in ('TURBO') then 'Turbo'
	when upper (store_type) = 'GRIN' then 'Grin'
	when upper ("group") = 'CPGS' and upper (sub_group) IN ('LICORES') THEN 'Licores'
	when upper (sub_group) in ('SUPER','HIPER') then 'Mercados'
	when upper (sub_group) in ('EXPRESS') then 'Express'
	when upper ("group") = 'CPGS' then 'CPGs'
	when upper (sub_group) = 'FARMACIA' then 'Farmacia'
	when upper ("group") in ('RESTAURANTES') then 'Restaurantes'
	when upper ("group") in ('ANTOJOS','RAPPICASH','RAPPIFAVOR') then "group"
	when upper ("group") in ('ECOMMERCE') then 'Ecommerce'
	when upper ("group") = 'MARCAS' and upper (sub_group) in ('TECNOLOGIA','MASCOTAS','SEX SHOP','MAQUILLAJE','MODA','SERVICIOS','ROPA','GAMERS','FLORISTERIA','HOGAR','BELLEZA','FLORES') THEN 'Ecommerce'
	else 'Others' end as vertical 
	from verticals
	""".format()

	query_ccp = """ 
	--no_cache
	select store_id::text as id_store, 'ccp' as tag
	from store_tag
	where tag_id in ({tag_id})
	group by 1,2
	""".format(tag_id=tag_id)

	if country == 'co':
		metrics = redash.run_query(1904, query)
		verticals = redash.run_query(6312, query_verticals)
		ccp = redash.run_query(6312,query_ccp)
	elif country == 'ar':
		metrics = redash.run_query(1337, query)
		verticals = redash.run_query(6309, query_verticals)
		ccp = redash.run_query(6309,query_ccp)
	elif country == 'cl':
		metrics = redash.run_query(1155, query)
		verticals = redash.run_query(6311, query_verticals)
		ccp = redash.run_query(6311,query_ccp)
	elif country == 'mx':
		metrics = redash.run_query(7977, query)
		verticals = redash.run_query(6324, query_verticals)
		ccp = redash.run_query(6324,query_ccp)
	elif country == 'uy':
		metrics = redash.run_query(1156, query)
		verticals = redash.run_query(6322, query_verticals)
		ccp = redash.run_query(6322,query_ccp)
	elif country == 'ec':
		metrics = redash.run_query(1922, query)
		verticals = redash.run_query(6447, query_verticals)
		ccp = redash.run_query(6447,query_ccp)
	elif country == 'cr':
		metrics = redash.run_query(1921, query)
		verticals = redash.run_query(6313, query_verticals)
		ccp = redash.run_query(6313,query_ccp)
	elif country == 'pe':
		metrics = redash.run_query(1157, query)
		verticals = redash.run_query(6323, query_verticals)
		ccp = redash.run_query(6323,query_ccp)
	elif country == 'br':
		metrics = redash.run_query(1338, query)
		verticals = redash.run_query(6310, query_verticals)
		ccp = redash.run_query(6310,query_ccp)

	return metrics, verticals, ccp

def category(country):
	query = f'''SELECT '{country}' as country1, si.store_id::text as storeid, c.category
	      from ops_occ.store_infos_latam si
	      left join {country}_PGLR_MS_STORES_PUBLIC.stores_vw s on s.store_id=si.store_id
	      left join ops_occ.group_brand_category c on c.BRAND_GROUP_ID=si.brand_group_id and s.city_address_id=c.city_address_id and c.COUNTRY='{country.upper()}'
	      and c.vertical=(case when si.vertical in ('Mercados') then 'Super/Hiper' else si.vertical end)
	      where si.country='{country}'
	      '''
	df = snow.run_query(query)
	return df

def new_offenders(a,b,c,d,e,f):
	channel_alert = timezones.slack_channels('partner_operation')
	current_time = timezones.country_current_time(country)

	reported_stores = d

	a = pd.merge(a,b,how='left',left_on=a['type'],right_on=b['store_type'])
	a = a.drop(['key_0'], axis=1)
	a = pd.merge(a,c,how='left',left_on=a['store_id'],right_on=c['id_store'])
	a = a.query("vertical not in 'Restaurantes'")

# Regra 1) 17% (min 5) cancelamento no dia OU mais que 20% que o ultimo report
	if country =='ar' or country =='cl' or country=='uy' or country=='pe' or country=='cr' or country=='ec':
		df = a[((a['percentage_day'] >= 0.15) & (a['cancels_day'] >= 4) & (a['vertical'].isin(['Ecommerce','Farmacia','Express','Licores','Super','Hiper','CPGs','Mercados'])))]
	else:
		df = a[((a['percentage_day'] >= 0.17) & (a['cancels_day'] >= 5) & (a['vertical'].isin(['Ecommerce','Farmacia','Express','Licores','Super','Hiper','CPGs','Mercados'])))]

	df = df[["store_id","name","type","tag","vertical","orders_day","cancels_day","percentage_day","big_group_reasons_reasons_day","sub_reasons_day","order_ids_day"]]
	news = pd.merge(df,reported_stores,how='left',left_on=df['store_id'],right_on=reported_stores['identification_id'])
	news = news.rename(columns={"metric": "last_report"})
	news['last_report'].fillna(0, inplace = True)
	news = news[((news['identification_id'].isnull()) | (news['percentage_day'] > (news['last_report'] + (news['last_report'] + 20))))]
	news = news.drop(['key_0','identification_id'], axis=1)

	news = pd.merge(news,e,how='left',left_on=news['store_id'],right_on=e['idstore'])
	news = news.drop(['key_0','idstore'], axis=1)

	news['country'] = country
	f=f.drop_duplicates(subset=['storeid'],keep='last')
	f['storeid'] =f['storeid'].astype(str)
	news['store_id'] = news['store_id'].astype(str)
	news = news.merge(f, how='left', left_on=['country', 'store_id'], right_on=['country1', 'storeid'])

	news.category = news.category.fillna('D')
	news = news.drop(['country1', 'storeid', 'country'], axis=1)
	print(news)
	if not news.empty:
		text = '''
		*Worst Offender Stores Day - :flag-{country}:*
		Pais: {country}
		:alert:
		Las tiendas en anexo alcanzaron 15% o más de cancelación en el día
		Estas tiendas ya no serán notificadas a menos que alcancen 20% de aumento de cancelación en las próximas horas'''.format(
			data=current_time,
			country=country
			)
		print(text)
		home = expanduser("~")
		results_file = '{}/wo_day_{country}.xlsx'.format(home,country=country)
		news.to_excel(results_file,sheet_name='WorstOffendersDay', index=False)
		slack.file_upload_channel(channel_alert, text, results_file, "xlsx")
		
		to_upload = news
		to_upload['alarm_at'] = current_time
		to_upload['country'] = country
		to_upload = to_upload.rename(columns={'order_ids_day': 'order_ids'})
		to_upload = to_upload[['store_id','alarm_at','country','order_ids','cancels_day','percentage_day']]
		print(to_upload)
		snow.upload_df_occ(to_upload,'worst_offenders_day')

# Regra 2) 10% (min3) cancelamento na ultima hora
	if country =='ar' or country =='cl' or country=='uy' or country=='pe' or country=='cr' or country=='ec':
		df2 = a[((a['percentage_last_hour'] >= 0.10) & (a['cancels_last_hour'] >= 2) & (a['vertical'].isin(['Ecommerce','Farmacia','Express','Licores','Super','Hiper','Mercados','CPGs'])))]
	else:
		df2 = a[((a['percentage_last_hour'] >= 0.10) & (a['cancels_last_hour'] >= 3) & (a['vertical'].isin(['Ecommerce','Farmacia','Express','Licores','Super','Hiper','Mercados','CPGs'])))]
	df2 = df2[["store_id","name","type","tag","vertical","orders_last_hour","cancels_last_hour","percentage_last_hour","big_group_reasons_last_hour","sub_reasons_last_hour","order_ids_last_hour"]]
	if not news.empty:
		news2 = news[["store_id"]]
		df2 = pd.merge(df2,news['store_id'],how='left',left_on=df2['store_id'],right_on=news['store_id'])
		df2 = df2[df2['store_id_y'].isnull()]
		df2 = df2.drop(['key_0','store_id_y'], axis=1)
		df2 = df2.rename(columns={'store_id_x': 'store_id'})
	else:
		df2 = df2

	text2 = '''
	*Worst Offender Stores Last Hour - :flag-{country}:*
	Pais: {country}
	:alert:
	Las tiendas en anexo alcanzaron 10% o más de cancelación en la última hora'''.format(
		data=current_time,
		country=country
		)
	print(text2)
	df2 = pd.merge(df2,e,how='left',left_on=df2['store_id'],right_on=e['idstore'])
	df2 = df2.drop(['key_0','idstore'], axis=1)
	df2['country'] = country
	f['storeid'] = f['storeid'].astype(str)
	df2['store_id'] = df2['store_id'].astype(str)
	df2 = df2.merge(f, how='left', left_on=['country', 'store_id'], right_on=['country1', 'storeid'])
	df2 = df2.drop(['country1', 'storeid', 'country'], axis=1)
	df2.category = df2.category.fillna('D')


	if not df2.empty:
		home = expanduser("~")
		results_file2 = '{}/wo_hour_{country}.xlsx'.format(home,country=country)
		df2.to_excel(results_file2,sheet_name='WorstOffendersHour', index=False)
		slack.file_upload_channel(channel_alert, text2, results_file2, "xlsx")
		df2['country'] = country
		df2['alarm_at'] = current_time
		df2 = df2.rename(columns={'order_ids_last_hour': 'order_ids', 'vertical_x': 'vertical'})
		df2 = df2[['store_id','vertical','country','alarm_at','order_ids','cancels_last_hour','percentage_last_hour']]
		to_upload2 = df2
		to_upload2['alarm_at'] = current_time
		to_upload2['country'] = country
		to_upload2 = to_upload2[['store_id','country','alarm_at','order_ids','cancels_last_hour','percentage_last_hour']]
		snow.upload_df_occ(to_upload2,'worst_offenders_hour')

def run_alarm(country):
	d = excluding_stores(country)
	a,b,c = offenders(country)
	e = store_metrics(country)
	f = category(country)
	new_offenders(a,b,c,d,e,f)

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
	print(country)
	try:
		run_alarm(country)
	except Exception as e:
		print(e)
