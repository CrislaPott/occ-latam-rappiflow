import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones
import pytz


def cancels(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    --no_cahce
    SELECT distinct om.order_id
    FROM order_modifications om
    join orders o on o.id=om.order_id 
    left join order_stores os on os.order_id=o.id
    WHERE om.created_at >= (now() AT TIME ZONE 'America/{timezone}') - interval '30 minutes'
    AND (om.TYPE ILIKE '%cancel%') and o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error')
    and (os.store_type_group not in ('restaurant','restaurant_cargo') or os.store_type_group is null)
    and (os.type not in ('rappi_pay','queue_order_flow','soat_webview') or os.type is null)
    and o.payment_method not ilike '%synthe%'
    '''

    if country == 'co':
        metrics = redash.run_query(1904, query)
    elif country == 'ar':
        metrics = redash.run_query(1337, query)
    elif country == 'cl':
        metrics = redash.run_query(1155, query)
    elif country == 'mx':
        metrics = redash.run_query(1371, query)
    elif country == 'uy':
        metrics = redash.run_query(1156, query)
    elif country == 'ec':
        metrics = redash.run_query(1922, query)
    elif country == 'cr':
        metrics = redash.run_query(1921, query)
    elif country == 'pe':
        metrics = redash.run_query(1157, query)
    elif country == 'br':
        metrics = redash.run_query(1338, query)

    return metrics

def its(country):
    timezone, interval = timezones.country_timezones(country)
    query = f'''
    --no_cache
    WITH a AS
    (SELECT cast((payload->'store'->>'id') AS int) AS store_id,
    cast((payload->'store'->>'name') AS text) as name,
    order_iterations.order_id,
    id,
    m.order_id as rt_allocated
    FROM courier_dispatcher.order_iterations
    left join (select distinct order_id 
              from courier_dispatcher.courier_order_matches 
              where taken_at is not null
              and created_at >= now() at time zone 'America/{timezone}' - interval '60 minutes'
              ) m on m.order_id=order_iterations.order_id
    WHERE created_at >= now() at time zone 'America/{timezone}' - interval '60 minutes'
    ) 
    , b as (SELECT store_id,
            name,
            order_id, 
            rt_allocated,
            count(DISTINCT id) AS order_iterations
   FROM a
   WHERE store_id !=0
   GROUP BY 1,
            2,
            3,
            4)
    select b.*, case when order_iterations >=50 and rt_allocated is null then 'yes' else 'no' end as flag
    from b
    '''
    if country == 'co':
        metrics = redash.run_query(13, query)
    elif country == 'ar':
        metrics = redash.run_query(232, query)
    elif country == 'cl':
        metrics = redash.run_query(309, query)
    elif country == 'mx':
        metrics = redash.run_query(205, query)
    elif country == 'uy':
        metrics = redash.run_query(306, query)
    elif country == 'ec':
        metrics = redash.run_query(2177, query)
    elif country == 'cr':
        metrics = redash.run_query(2179, query)
    elif country == 'pe':
        metrics = redash.run_query(684, query)
    elif country == 'br':
        metrics = redash.run_query(231, query)

    return metrics

def run_alarm(its, cancels):
    channel_alert = timezones.slack_channels('rt_operation')
    current_time = timezones.country_current_time(country)

    new_df = pd.merge(its,cancels, how='left',left_on=its['order_id'],right_on=cancels['order_id'])
    orders_list = new_df[(new_df['flag'] == 'yes')]
    orders_list = orders_list[orders_list['order_id_y'].notnull()]
    orders_list["order_id_y"]=orders_list["order_id_y"].astype(int)
    orders_list = orders_list.groupby(["store_id"])["order_id_y"].agg([('order_ids', lambda x: ', '.join(map(str, x)))]).reset_index()
    new_df = new_df.groupby(["store_id","name","flag"])[["order_id_x","order_id_y"]].nunique().reset_index()
    new_df2 = new_df.groupby(["store_id","name"])[["order_id_x"]].sum().reset_index()
    new_df2 = new_df2.rename(columns={'order_id_x': 'total_iterated_orders'}) 
    
    cancelation = new_df[(new_df['flag'] == 'yes')]
    cancelation = cancelation.rename(columns={'order_id_y': 'total_canceled_iterated_orders', 'order_id_x': 'total_iterated_order_10'}) 
    
    final_df = pd.merge(new_df2,cancelation, how='left',left_on=new_df2['store_id'],right_on=cancelation['store_id'])
    final_df = final_df.drop(['key_0','store_id_y','name_y','flag'], axis=1)
    final_df['percentage'] = (final_df['total_canceled_iterated_orders'] / final_df['total_iterated_orders']) * 100
    final_df = final_df[(final_df['total_canceled_iterated_orders'] >= 1)]
    final_df = pd.merge(final_df,orders_list, how='left',left_on=final_df['store_id_x'],right_on=orders_list['store_id'])
    final_df = final_df.rename(columns={'name_x': 'name'})
    final_df['country'] = country
    final_df['alarm_at'] = current_time
    final_df = final_df[['store_id','name','total_iterated_orders','total_canceled_iterated_orders','percentage','order_ids','country','alarm_at']]
    final_df['order_ids']=final_df['order_ids'].astype(str)

    snow.upload_df_occ(final_df,'lack_of_rt_alarm')

    for index, row in final_df.iterrows():
        rows = dict(row)

        text = '''
        *Alarma de Falta de RTs :rt: en Tiendas* :alert:
        Country :flag-{country}:
        Pais: {country}
        La tienda {name} (Store ID: {store_id}) alcanzó {total_canceled_iterated_orders} pedidos con más de 50 iteraciones en los ultimos 30 minutos
        Percentage: {percentage}%
        Store ID: {store_id}
        Order IDs: {order_ids}
        <@claudio.leivas> <@ext-c.amigo> <@ext-ea.morales> <@ext-a.cornejo> 
        '''.format(
            name = row['name'],
            store_id = row['store_id'],
            total_canceled_iterated_orders = row['total_canceled_iterated_orders'],
            percentage = round(row['percentage'],2),
            order_ids = row['order_ids'],
            country = country
            )

        print(text)
        slack.bot_slack(text,'C02LF1U8W07')

countries = ['cl']
for country in countries:
    print(country)
    try:
        a = cancels(country)
        b = its(country)
        run_alarm(a,b)
    except Exception as e:
        print(e)