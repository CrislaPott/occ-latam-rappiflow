from lib import snowflake as snow
from lib import redash, slack
from functions import timezones
import pandas as pd
from os.path import expanduser

def excluding_orders(country):
    timezone, interval = timezones.country_timezones(country)
    query = '''select distinct order_id as orderid 
    from ops_occ.orders_arrived_4h
    where alarm_at::date = convert_timezone('America/{timezone}',current_timestamp())::date
     and country='{country}'
     '''.format(country=country, timezone=timezone)
    df = snow.run_query(query)
    return df

def country_query(country):
    timezone, interval = timezones.country_timezones(country)
    query= '''
 --no_cache 
SET TIMEZONE TO 'America/Buenos_Aires';

with a as 
(
select 
      om.order_id,
      now()::timestamp as local_time,
      max(case when om.type = 'arrive' then om.created_at end) as arrive_at
from 
      order_modifications om 
where 
      om.type in ('arrive')
and   
      om.created_at >= now()::date
group by 1,2
)

select 
      os.name as store_name,
      os.store_id::text as store_id,
      os.type,
      o.id as order_id,
      o.state,
      (extract(epoch from (local_time - arrive_at)) / 60) as arrive_time
from 
      a
join 
      order_stores os on os.order_id=a.order_id 
join 
      orders o on o.id=a.order_id
where
      (os.store_type_group not in ('restaurant_cargo') or os.store_type_group is null)
and not 
      os.is_marketplace
and  
      o.state not in ('canceled_by_fraud','canceled_for_payment_error','canceled_by_split_error','canceled_by_early_regret','finished','pending_review') 
and  
      o.state not ilike '%cancel%'
and 
      (extract(epoch from (local_time - arrive_at)) / 60) >= 240
    '''.format(country=country, timezone=timezone)
    if country == 'co':
        metrics = redash.run_query(7973, query)
    elif country == 'ar':
        metrics = redash.run_query(7970, query)
    elif country == 'cl':
        metrics = redash.run_query(7972, query)
    elif country == 'mx':
        metrics = redash.run_query(7977, query)
    elif country == 'uy':
        metrics = redash.run_query(7978, query)
    elif country == 'ec':
        metrics = redash.run_query(7975, query)
    elif country == 'cr':
        metrics = redash.run_query(7974, query)
    elif country == 'pe':
        metrics = redash.run_query(7976, query)
    elif country == 'br':
        metrics = redash.run_query(7971, query)
    return metrics


def store_infos(country):
    query = """
    --no_cache
    select a.store_id::text as storeid, bg.brand_group_id::text as store_brand_id, bg.brand_group_name as brand_name, 
    case when v.vertical in ('Express','Licores','Farmacia','Turbo','Specialized') then 'Now'
         when v.vertical in ('Mercados') then 'Mercados'
         when v.vertical in ('Ecommerce') then 'Ecommerce' else null end as vertical
    from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
    left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw b on b.id=a.store_brand_id
    inner join (
    select store_type as storetype,
    case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
    when vertical_group = 'RESTAURANTS' then 'Restaurantes'
    when vertical_group = 'WHIM' then 'Antojos'
    when vertical_group = 'RAPPICASH' then 'RappiCash'
    when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
    when lower(store_type) in ('turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja') then 'Turbo'
    when upper (store_type) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
    when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
    when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
    when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
    when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
    when upper (vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
    when upper(vertical_group) in ('CPGS') then 'CPGs'
    else 'Others' end as vertical
    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype=a.type

    left join (
       select * from
(select store_brand_id, last_value(brand_group_id) over (partition by store_brand_id order by tb.created_at asc) as brand_group_id,
last_value(bg.name) over (partition by store_brand_id order by tb.created_at asc) as brand_group_name
from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
group by 1,2,3
       ) bg on bg.store_brand_id = a.store_brand_id

    where a.deleted_at is null
    and not coalesce (a._fivetran_deleted, false)
    and vertical in ('Ecommerce')
    group by 1,2,3,4""".format(country=country)

    df = snow.run_query(query)
    return df

def alarm(a, b, c):

    channel_alert = 'C02LF1U8W07'
    df = pd.merge(a, b, how='inner', left_on=a['store_id'], right_on=b['storeid'])
    df = df.drop(['key_0'], axis=1)
    
    df1 = pd.merge(df, c, how='left', left_on=df['order_id'], right_on=c['orderid'])
    df1 = df1[df1['orderid'].isnull()]
    df1 = df1.drop(['key_0', 'orderid'], axis=1)
    print(df1)
    if not df1.empty:
        current_time = timezones.country_current_time(country)
        for index, row in df1.iterrows():
            text = '''
            *Ecommerce Orders with elapsed time of 4 hours since arrival* :alert:
            :flag-{country}:
            *Country*: {country}
            *Brand Group*: {brand_name} {store_brand_id}
            *Store ID*: {store_id}
            *Store Name*: {store_name}
            *Order ID*: {order_id}
            <@angie.paez> <@bpo-s.paniagua> <@daniel.mosquera>
            '''.format(country= country,
                       brand_name = row['brand_name'],
                       store_brand_id = row['store_brand_id'],
                       store_id=row['store_id'],
                       store_name=row['store_name'],
                       order_id=row['order_id']
                       )

            print(text)
            slack.bot_slack(text,channel_alert)
            
        to_upload = df1.copy()
        to_upload['alarm_at'] = current_time
        to_upload['country'] = country
        to_upload = to_upload[['store_id', 'store_name','order_id', 'alarm_at', 'country']]
        print(to_upload)
        print('snowflake')

        snow.upload_df_occ(to_upload, 'orders_arrived_4h')
    else:
        print('null')


countries = ['mx','co','cl','uy','cr','ec','pe','ar']
for country in countries:
    try:
        print(country)
        a = country_query(country)
        b = store_infos(country)
        c = excluding_orders(country)
        alarm(a, b, c)
    except Exception as e:
        print(e)