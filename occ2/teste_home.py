import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from os.path import expanduser
from dotenv import load_dotenv
from lib import redash, slack
from lib import snowflake as snow
import pytz

home = expanduser("~")
print(home)