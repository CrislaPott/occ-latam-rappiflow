import os, sys, json
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from dotenv import load_dotenv
from lib import slack, redash
from lib import snowflake as snow
from functions import timezones as tm
import pytz
from os.path import expanduser

def cbpi(country):
	timezone,interval = tm.country_timezones(country)
	query = f'''
	select os.store_id::text as store_id,
	       os.name,
           count(distinct case when o.state in ('canceled_by_partner_inactivity')
                                    then o.id else null 
                          end) as total_cbpi,
           count(distinct o.id) as total_orders,
           total_cbpi/total_orders as percentage,
           listagg(distinct case when o.state in ('canceled_by_partner_inactivity') 
                                    then o.id::text else null 
                          end,',') as order_ids

    from {country}_CORE_ORDERS_PUBLIC.orders_vw o
    left join {country}_CORE_ORDERS_PUBLIC.order_stores_vw os on os.order_id=o.id

    where o.created_at::date >= current_date::date - 7
    group by 1,2
    having total_cbpi >= 3 and percentage >= 0.60
	'''

	metrics = snow.run_query(query)
	print(metrics)

	query_reported_stores = f'''
	--no_cache
	select storeid::text as store_id, max(total_cbpi) as total_cbpi
	from ops_occ.cbpi_latam_alarm
	where alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date - 8
	and country ='{country}'
	group by 1
	'''
	reported_stores = snow.run_query(query_reported_stores)

	return metrics, reported_stores

def news(a,reported_stores):
	current_time = tm.country_current_time(country)
	channel_alert, channel = tm.slack_channels(country)

	news = pd.merge(a,reported_stores, how='left',left_on=a['store_id'], right_on=reported_stores['store_id'])
	news = news[(news['store_id_y'].isnull())]
	news = news.drop(['key_0','total_cbpi_y'], axis=1)
	news = news.rename(columns={'store_id_x': 'storeid', 'total_cbpi_x': 'total_cbpi'})
	stores = news[['storeid', 'name','total_cbpi','total_orders','percentage','order_ids']]

	to_upload = stores
	to_upload['country'] = country
	to_upload['alarm_at'] = current_time
	to_upload = to_upload[['country','storeid','alarm_at','total_cbpi','total_orders','percentage','order_ids']]
	stores = news[['storeid', 'name','total_cbpi','total_orders','percentage','order_ids']]

	if not to_upload.empty:
		snow.upload_df_occ(to_upload,'cbpi_latam_alarm')

	for index, row in stores.iterrows():
		store_id = row['storeid']
		store_name = row['name']
		total_cbpi = row['total_cbpi']
		total_orders = row['total_orders']
		percentage = row['percentage']
		order_ids = row['order_ids']

		text = f'''
		*Alarma de CBPI por tiendas:alert:* 
		Tienda con mayor a 60% cancelaciones CBPI en los ultimos 7 dias
		:flag-{country}:
		Pais: {country}
		Store ID: {store_id}
		Tienda: {store_name}
		Total CBPI last 7 days: {total_cbpi}
		Total Orders last 7 days: {total_orders}
		Percentage: {percentage}
		Orders IDs: {order_ids}
		<!subteam^S018ASNLN5T>
		'''
		print(text)
		slack.bot_slack(text,channel_alert)

countries = ['ar','cl','pe','br','uy','mx','co','ec','cr']

for country in countries:
	try:
		a, reported_stores = cbpi(country)
		news(a,reported_stores)
	except Exception as e:
		print(e)