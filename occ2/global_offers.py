from lib import snowflake as snow
from lib import slack
from functions import timezones
import pandas as pd
from os.path import expanduser


def logs(country):
  timezone, interval = timezones.country_timezones(country)
  query = '''
  select distinct date, CAMPAIGN_ID as campaignid from ops_occ.global_offers
  where country = '{country}'
  and alarm_at::date >= convert_timezone('America/{timezone}',current_timestamp())::date
  '''.format(country=country, timezone=timezone)
  df = snow.run_query(query)

  return df

def global_offer(country):

  timezone, interval = timezones.country_timezones(country)
  query = '''WITH  RANGE_ORDERS AS (
    SELECT A.ID AS ORDER_ID ,
    ARRAY_AGG(product_id) as order_products
    FROM FIVETRAN.{country}_CORE_ORDERS_PUBLIC.ORDERS_VW A
    LEFT join {country}_core_orders_public.order_product_vw B
        on a.id = b.order_id
    WHERE NOT NVL(A._FIVETRAN_DELETED,FALSE)
    AND A.STATE NOT IN('canceled_by_early_regret',
                    'canceled_for_payment_error',
                    'canceled')
//    and COALESCE(b._fivetran_deleted,false)=false
    AND A.CREATED_AT::DATE >= dateadd(hour,-2,convert_timezone('America/{timezone}',current_timestamp()))::date
    GROUP BY 1
  ),

VERTICAL as (select store_type as store_type,
case when vertical_group = 'ECOMMERCE' then 'Ecommerce'
     when vertical_group = 'RESTAURANTS' then 'Restaurantes'
     when vertical_group = 'WHIM' then 'Antojos'
     when vertical_group = 'RAPPICASH' then 'RappiCash'
     when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
     when store_type in ('turbo') then 'Turbo'
     when upper (vertical_sub_group) in ('TURBO') then 'Turbo'
     when upper (vertical_sub_group) in ('SUPER','HIPER') then 'Mercados'
     when upper (vertical_sub_group) in ('PHARMACY') then 'Farmacia'
     when upper (vertical_sub_group) in ('LIQUOR') then 'Licores'
     when upper (vertical_sub_group) in ('EXPRESS') then 'Express'
     when upper(vertical_group) in ('CPGS') then 'CPGs'
else 'Others' end as sub_group
from VERTICALS_LATAM.{country}_VERTICALS_V2
),

BASE_BRANDS AS (SELECT DISTINCT S.STORE_ID,
                                S.NAME,
                                S.TYPE,
                                B.ID as STORE_BRAND_ID,
                                B.NAME AS BRAND_NAME,
                                ST.BRAND_GROUP_NAME,
                                ST.BRAND_GROUP_ID
                FROM            {country}_PGLR_MS_STORES_PUBLIC.STORES_vw S
                LEFT JOIN       {country}_PGLR_MS_STORES_PUBLIC.STORE_BRANDS_vw B
                ON              B.ID = S.STORE_BRAND_ID
                LEFT JOIN
                                (
                                         SELECT   *
                                         FROM     (
                                                           SELECT   STORE_BRAND_ID,
                                                                    STORE_TYPE,
                                                                    LAST_VALUE(BRAND_GROUP_ID) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC) AS BRAND_GROUP_ID,
                                                                    LAST_VALUE(BG.NAME) OVER (PARTITION BY STORE_BRAND_ID ORDER BY TB.CREATED_AT ASC)        AS BRAND_GROUP_NAME
                                                           FROM     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS TB
                                                           JOIN     {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG
                                                           ON       BRAND_GROUP_ID = BG.ID)
                                         GROUP BY 1,
                                                  2,
                                                  3,
                                                  4) ST
                ON              ST.STORE_TYPE = S.TYPE
                AND             ST.STORE_BRAND_ID =  S.STORE_BRAND_ID
                WHERE             S.DELETED_AT IS NULL
                AND             S.CITY_ADDRESS_ID != 104
                AND             COALESCE(S._FIVETRAN_DELETED,'FALSE') = 'FALSE') ,


ORDER_DISCOUNTS_DETAILS AS (
    WITH ORDER_DISCOUNTS AS (
      SELECT
      OD.ID AS OD_ID
      , OD.ORDER_ID
      , OD.TOTAL_ORDER_DISCOUNT AS TOTAL_ORDER_DISCOUNT
      , OD.TOTAL_CHARGE_DISCOUNT AS TOTAL_CHARGE_DISCOUNT
      , OD.TOTAL_ORDER_DISCOUNT + OD.TOTAL_CHARGE_DISCOUNT AS TOTAL_DISCOUNT
      FROM FIVETRAN.{country}_core_orders_public.order_discounts_VW OD
      INNER JOIN (
        SELECT
        MIN(OD.ID) AS OD_ID
        , OD.ORDER_ID
        FROM FIVETRAN.{country}_core_orders_public.order_discounts_VW OD
        WHERE TOTAL_CHARGE_DISCOUNT + TOTAL_ORDER_DISCOUNT != 0
        AND OD.ORDER_ID IN (SELECT ORDER_ID FROM RANGE_ORDERS)
        GROUP BY OD.ORDER_ID
      ) OD1 ON
      OD1.OD_ID = OD.ID
      WHERE OD.ORDER_ID IN (SELECT ORDER_ID FROM RANGE_ORDERS)
    )

    SELECT
    OD.OD_ID
    , OD.ORDER_ID
    , OD.TOTAL_ORDER_DISCOUNT
    , OD.TOTAL_CHARGE_DISCOUNT
    , OD.TOTAL_DISCOUNT
    , ODD.ID AS ODD_ID
    , ODD.OFFERTABLE_ID
    , ODD.ORDER_DISCOUNT_ID
    , ODD.PRODUCT_ID
    , ODD.APPLIED_UNITS
    , ODD.TYPE AS ODD_TYPE
    , ODD.CREATED_AT
    , ODD.VALUE
    , ODD.OFFERTABLE_TYPE
    , ODD.TYPE_DESCRIPTION
    FROM ORDER_DISCOUNTS OD
    LEFT JOIN FIVETRAN.{country}_CORE_ORDERS_PUBLIC.ORDER_DISCOUNT_DETAILS_VW ODD ON
    ODD.ORDER_DISCOUNT_ID = OD.OD_ID

  ),

SQUAD AS (
    SELECT
      SQUAD
    , PERCENTAGE_ASSUMED
    , CAMPAIGN_ID
    , CAMPAIGN_SOURCE
    , CHANNEL
    , PLATFORM
    , CAMPAIGN_NAME
    , MESSAGE
    , EMAIL
    , TEAM
    , SUM_PERCENTAGE_ALLIES
    , NUM_ALLIES
    , LIST_ALLIANCE_TYPE
    , LIST_PERCENTAGE_ALLIES
    , LIST_ALLIES
    , LIST_ALLIES_ID
    FROM FIVETRAN.GLOBAL_FINANCES.VW_{country}_CAMPAIGNS_TERRICOS),

OFFERS AS (
    SELECT
    OFF.ID AS OFF_ID
    , OFF.TYPE AS OFF_TYPE
    , OFF.DESCRIPTION
    , REPLACE(REPLACE(OFF.VALUE,',','.'),'%','') AS OFF_VALUE
    , OFF.COUPON_ID
    , OFF.COUPON_CODE_ID
    , OFF.GLOBAL_OFFER_ID
    FROM FIVETRAN.{country}_PGLR_MS_COUPONS_PUBLIC.OFFERS OFF
    WHERE NOT COALESCE(_FIVETRAN_DELETED,FALSE)),


GLOBAL_OFFERS AS (SELECT GO.ID,
                  SQ.SQUAD,
                  GO.TYPE AS GO_TYPE,
                  GO.DESCRIPTION,
                  COALESCE(GGO.ASSUMED_BY,CASE WHEN GP.PARTNERSHIP IS NOT NULL
                                        OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'PARTNERSHIP')
                                      THEN 'Partnerships'
                                  WHEN GR.DEAL_TYPE = 'REVENUE'
                                        OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'REVENUE')
                                      THEN 'Revenue'
                                  WHEN GR.DEAL_TYPE = 'ALLY'
                                        OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'ALLY')
                                      THEN 'Ally'
                                      WHEN LIST_ALLIANCE_TYPE = 'INTEGRATIONS'
                                      THEN 'Integrations'
                                  WHEN SQ.TEAM = 'RAPPI_ADS'
                                      THEN 'Rappi Ads'
                              END) AS ASSUMED_BY

    FROM FIVETRAN.{country}_PG_MS_GLOBAL_OFFERS_PUBLIC.GLOBAL_OFFERS GO
    LEFT JOIN FIVETRAN.GLOBAL_FINANCES.GLOBAL_GLOBAL_OFFERS_TYPES GGO
        ON GGO.DISCOUNT_DESCRIPTION = GO.DESCRIPTION
    LEFT JOIN FIVETRAN.GLOBAL_FINANCES.GLOBAL_PARTNERSHIPS GP
        ON GP.GLOBAL_OFFER_ID = GO.ID
        AND GP.COUNTRY_CODE = '{country}'
        AND GP.PAY_WAY = '1'
    LEFT JOIN FIVETRAN.GLOBAL_FINANCES.GLOBAL_REVENUE GR
        ON GR.GLOBAL_OFFER_ID = GO.ID
        AND GR.COUNTRY_CODE = '{country}'
        AND GR.PAY_WAY = '1'
    LEFT JOIN SQUAD SQ
        ON SQ.CAMPAIGN_ID = GO.ID
        AND SQ.CHANNEL = 'GLOBAL_OFFERS'
    WHERE NOT COALESCE(GO._FIVETRAN_DELETED,FALSE)
    AND GO.TYPE <> 'add_rappi_credit'),


COUPONS AS(
      SELECT C.ID,
             C.TITLE,
             C.DISCOUNT_PERCENTAGE_BY_PARTNERS,
             SQ.SQUAD,
      CASE WHEN CC.COUPON_ID IS NULL
                OR NOT COALESCE(CC.PARAMS:"is_prime"::BOOLEAN,FALSE) THEN FALSE
            WHEN COALESCE(CC.PARAMS:"is_prime"::BOOLEAN,FALSE) THEN TRUE
      END AS IS_PRIME
    , CASE WHEN GP.PARTNERSHIP IS NOT NULL--drive partnerships
                OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'PARTNERSHIP')--terricos
              THEN 'Partnerships'
          WHEN GR.DEAL_TYPE = 'REVENUE'--drive revenue & ally
                OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'REVENUE')--terricos
              THEN 'Revenue'
          WHEN GR.DEAL_TYPE = 'ALLY'--drive revenue & ally
                OR (SQ.LIST_PERCENTAGE_ALLIES = '100.00' AND LIST_ALLIANCE_TYPE = 'ALLY')--terricos
              THEN 'Ally'
              WHEN LIST_ALLIANCE_TYPE = 'INTEGRATIONS'
                  THEN 'Integrations'
          WHEN SQ.TEAM = 'RAPPI_ADS'
              THEN 'Rappi Ads'
          WHEN (SQ.SQUAD ='PRICING'
                OR SQ.TEAM = 'PRICING')--Cuando se retagueen bien, este or debe morir
                AND NOT IS_PRIME
              THEN 'Pricing'
      END AS ASSUMED_BY
      FROM FIVETRAN.{country}_PGLR_MS_COUPONS_PUBLIC.COUPONS C
      LEFT JOIN FIVETRAN.GLOBAL_FINANCES.GLOBAL_PARTNERSHIPS GP ON
          GP.COUPON_ID = C.ID
          AND GP.COUNTRY_CODE = '{country}'
          AND GP.PAY_WAY = '1'
      LEFT JOIN FIVETRAN.GLOBAL_FINANCES.GLOBAL_REVENUE GR ON
          GR.COUPON_ID = C.ID
          AND GR.COUNTRY_CODE = '{country}'
          AND GR.PAY_WAY = '1'
      LEFT JOIN FIVETRAN.{country}_PGLR_MS_COUPONS_PUBLIC.COUPON_CONDITIONS CC
          ON C.ID = CC.COUPON_ID
          AND TYPE = 'is_prime'
          AND NOT COALESCE(CC._FIVETRAN_DELETED,FALSE)
      LEFT JOIN SQUAD SQ
          ON SQ.CAMPAIGN_ID = C.ID
          AND SQ.CHANNEL = 'COUPONS'
      WHERE NOT COALESCE(C._FIVETRAN_DELETED,FALSE)),


ORDER_PRODUCT_DETAILS AS(
           SELECT OP.ORDER_ID,
                  OP.PRODUCT_ID,
           FIRST_VALUE(OPPV.DISCOUNT_PERCENTAGE_BY_REVENUE) OVER (PARTITION BY OP.ORDER_ID, OP.PRODUCT_ID ORDER BY OPPV._FIVETRAN_SYNCED DESC) AS DISCOUNT_PERCENTAGE_BY_REVENUE,
	       CASE WHEN CA.ORDER_ID IS NOT NULL
              THEN 'Rappi Pay'
           END AS ASSUMED_BY
           FROM FIVETRAN.{country}_CORE_ORDERS_PUBLIC.ORDER_PRODUCT_VW OP
           INNER JOIN FIVETRAN.{country}_CORE_ORDERS_PUBLIC.ORDER_PRODUCT_PRICE_VARIATIONS OPPV
                ON OP.ID = OPPV.ORDER_PRODUCT_ID
                AND HAS_PRICE_CUT
                AND NOT COALESCE(OPPV._FIVETRAN_DELETED,FALSE)
           LEFT JOIN (
                      SELECT
                      ORDER_ID
                      , STORE_TYPE_STORE
                      FROM FIVETRAN.{country}_core_orders_calculated_information.orders_VW
                      WHERE ORDER_ID IN (SELECT ORDER_ID FROM RANGE_ORDERS)
                      AND STORE_TYPE_STORE in ('rappi_pay','rappi_pay_credit')
                      ) CA
                ON CA.ORDER_ID = OP.ORDER_ID
           WHERE NOT COALESCE(OP._FIVETRAN_DELETED,FALSE)
           AND OP.ORDER_ID IN (SELECT ORDER_ID FROM RANGE_ORDERS))

, FINAL AS (SELECT
    STORE_ID
    , NAME
    , stores.TYPE
    , O.APPLICATION_USER_ID
    , O.ID AS  DISCOUNT_ORDER_ID
    , O.STATE AS ORDER_STATE
    , O.CREATED_AT::TIMESTAMP_TZ(9) AS DISCOUNT_AT
    , ODD.VALUE
    , ODD.PRODUCT_ID
    , CASE WHEN ODD.OFFERTABLE_TYPE ilike '%Global%' and ODD.ODD_type = 7 then 'GO' else null end as OFFERTABLE_TYPE
    , CASE WHEN ODD.OFFERTABLE_TYPE LIKE 'offer-base-price%'
            THEN ODD.PRODUCT_ID
          WHEN ODD.ODD_TYPE = 7
            THEN GO.ID
          WHEN ODD.OFFERTABLE_TYPE = 'Offer'
            THEN C.ID
      END AS CAMPAIGN_ID
    , row_number () OVER(PARTITION BY O.ID, "VALUE" ORDER BY "VALUE") AS REPEATED_GO
    , row_number () OVER(PARTITION BY O.ID, CAMPAIGN_ID ORDER BY CAMPAIGN_ID) AS REPEATED_GO_CP_ID
    , CASE WHEN C.DISCOUNT_PERCENTAGE_BY_PARTNERS = 100
              THEN 'Ally'
            WHEN (GO.ASSUMED_BY='Integrations' or C.ASSUMED_BY='Integrations') AND O.CREATED_AT::DATE<'2021-04-26'
              THEN 'Rappi'
            WHEN (GO.ASSUMED_BY='Integrations' or C.ASSUMED_BY='Integrations')  AND O.CREATED_AT::DATE>='2021-04-26'
              THEN 'Ally'
            ELSE COALESCE(IFF(COALESCE(GO.SQUAD,C.SQUAD)='RAPPI_CORP','R4B',NULL)
                  , GO.ASSUMED_BY
                  , C.ASSUMED_BY
                  , OP.ASSUMED_BY
                  , CASE WHEN UPPER(ODD.OFFERTABLE_TYPE) = 'OFFER-BASE-PRICE'
                            OR (GO.GO_TYPE = 'free_shipping'
                                AND GO.description = 'Automatic restaurants-tech offer'
                                AND ODD.ODD_TYPE = '7')
                            OR (UPPER(ODD.OFFERTABLE_TYPE) = 'OFFER-BASE-PRICE-RAPPI' AND DISCOUNT_AT::DATE>='2020-10-01')--Ajuste por mal tagueo de markdowns de aliado
                          THEN 'Ally'
                        WHEN GO.GO_TYPE = 'order_percentage_cc_visa'
                          THEN 'Partnerships'
                        WHEN (UPPER(ODD.OFFERTABLE_TYPE) = 'OFFER-BASE-PRICE-RAPPI' AND OP.DISCOUNT_PERCENTAGE_BY_REVENUE = 100 AND DISCOUNT_AT::DATE<'2020-10-01')--Ajuste por mal tagueo de markdowns de aliado
                          THEN 'Revenue'
                      ELSE 'Rappi'
                    END) END AS ASSUMED_BY
                  , CASE WHEN ODD.OFFERTABLE_TYPE LIKE 'Prime%'
                          THEN 'Usuario Rappi Prime'
                        WHEN GO.id IS NOT NULL
                          THEN GO.DESCRIPTION
                        WHEN OFF.OFF_ID IS NOT NULL
                          THEN C.TITLE
                      ELSE 'Nuevo?'
                    END AS DISCOUNT_DESCRIPTION
            FROM FIVETRAN.{country}_CORE_ORDERS_PUBLIC.ORDERS_VW O
    INNER JOIN ORDER_DISCOUNTS_DETAILS ODD
        ON ODD.ORDER_ID = O.ID
        AND odd.value != 0
    LEFT JOIN OFFERS OFF
        ON ODD.OFFERTABLE_ID = OFF.OFF_ID
        AND UPPER(ODD.OFFERTABLE_TYPE) = 'OFFER'
        AND ODD.ODD_TYPE<>7
    LEFT JOIN COUPONS C
        ON OFF.COUPON_ID = C.ID
    LEFT JOIN GLOBAL_OFFERS GO
        ON ODD.OFFERTABLE_ID = GO.ID
        AND ODD.ODD_TYPE = 7
    LEFT JOIN ORDER_PRODUCT_DETAILS OP
        ON ODD.ORDER_ID = OP.ORDER_ID
        AND ODD.PRODUCT_ID = OP.PRODUCT_ID
        AND UPPER(ODD.OFFERTABLE_TYPE) LIKE 'OFFER-BASE-PRICE%'
    LEFT JOIN {country}_core_orders_public.order_stores_vw stores
        ON STORES.ORDER_ID = O.ID
        )


,FINAL_AGG AS  (
           select e.SUB_GROUP AS VERTICAL,
           COALESCE(BRAND_GROUP_ID, STORE_BRAND_ID) AS BRAND_GROUP_ID,
           COALESCE(BRAND_GROUP_NAME, BRAND_NAME) AS BRAND_GROUP_NAME,
           final.STORE_ID,
           FINAL.NAME,
           APPLICATION_USER_ID,
//           COUNT( DISTINCT DISCOUNT_ORDER_ID) AS ORDERS,
//           LISTAGG(DISTINCT DISCOUNT_ORDER_ID, ', ') AS ORDER_IDS,
           DISCOUNT_ORDER_ID,
           CAMPAIGN_ID,
           "VALUE",
           ORDER_PRODUCTS,
           DISCOUNT_AT,
           row_number () OVER(PARTITION BY APPLICATION_USER_ID, FINAL.STORE_ID, CAMPAIGN_ID ORDER BY CAMPAIGN_ID) AS REPEATED_GO
           from final
           LEFT JOIN VERTICAL E
                ON E.store_type = FINAL.type
           JOIN BASE_BRANDS C
                ON C.STORE_ID = FINAL.STORE_ID
           LEFT JOIN RANGE_ORDERS B
             ON B.ORDER_ID = DISCOUNT_ORDER_ID
        WHERE E.SUB_GROUP NOT IN ('Restaurantes', 'Turbo')
          AND ASSUMED_BY = 'Revenue')


SELECT DATEADD('MINUTE', FLOOR(DATE_PART('MINUTE', DISCOUNT_AT::TIMESTAMP_NTZ)/(20)::FLOAT)*     (20)::FLOAT, DATE_TRUNC('HOUR', DISCOUNT_AT::TIMESTAMP_NTZ))::TIMESTAMP_NTZ AS DATE,
       CAMPAIGN_ID,
       VERTICAL,
       COUNT(DISTINCT DISCOUNT_ORDER_ID) AS ORDERS_CREATED,
       COUNT(DISTINCT APPLICATION_USER_ID) AS USERS,
       COUNT(DISTINCT STORE_ID) AS STORES_AFFECTED,
       LISTAGG(DISTINCT DISCOUNT_ORDER_ID, ', ') WITHIN GROUP (ORDER BY DISCOUNT_ORDER_ID ASC) AS ORDER_IDS,
       LISTAGG(DISTINCT STORE_ID, ', ') AS STORES_IDS,
       COUNT(CAMPAIGN_ID) as REPEATED_GO,
       ARRAY_AGG(ORDER_PRODUCTS) WITHIN GROUP (ORDER BY DISCOUNT_ORDER_ID ASC) AS ORDER_PRODUCTS
       FROM FINAL_AGG

GROUP BY 1,2,3
HAVING COUNT(DISTINCT DISCOUNT_ORDER_ID) >= 4
AND COUNT(DISTINCT DISCOUNT_ORDER_ID) >= 1.5 * COUNT(DISTINCT APPLICATION_USER_ID)
ORDER BY 1
'''.format(country=country.upper(), timezone=timezone)
  df = snow.run_query(query)
  return df

def alarm(a,b):

  channel_alert = timezones.slack_channels('commercial_alarms')
  current_time = timezones.country_current_time(country)

  if not a.empty:
    a.order_products= a.order_products.str.replace("\n",'',regex=True).str.replace("[",'',regex=True).str.replace("  ",'',regex=True).str.replace("]",'',regex=True)

    df = a.merge(b, left_on=['date', 'campaign_id'], right_on=['date', 'campaignid'], how='left')
    df = df[(df['campaignid'].isnull())]
    df = df.drop(['campaignid'], axis=1)
    df['alarm_at'] = current_time
    df['country'] = country
    df['alarm_at'] = df['alarm_at'].dt.tz_localize(None)
    print(df)

    if not df.empty:
      print('snowflake')
      snow.upload_df_occ(df, 'global_offers')

      text = '''
        *Alarma - Global Offers :alert:*
        :flag-{country}:
        Pais: {country}
        Nuevos GO IDs alcanzaron el threshold.
        '''.format(country=country)
      home = expanduser("~")
      results_file = '{}/details_global_offers_{country}.xlsx'.format(home, country=country)
      df_final = df[['date','campaign_id','vertical','orders_created','users','stores_affected','order_ids',
                     'stores_ids','repeated_go','order_products','alarm_at','country']]
      df_final.to_excel(results_file, sheet_name='global_offers', index=False)
      slack.file_upload_channel(channel_alert, text, results_file, "xlsx")

    else:
      print('df empty')


  else:
    print('a null')

countries = ['br','co','mx','ar','cl','pe','ec','cr','uy']
for country in countries:
  try:
    print(country)
    b = logs(country)
    a = global_offer(country)
    alarm(a,b)

  except Exception as e:
    print(e)