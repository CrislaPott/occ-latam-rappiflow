import pytz
import pandas as pd
from datetime import datetime, timedelta
from lib import redash, slack
from lib import snowflake as snow
from functions import timezones, snippets

def orders_brand(country):
    timezone, interval = timezones.country_timezones(country)
    query_store_infos = """
               select 'br'                     as country,
                      a.store_id::text         as store_id,
                      a.name                   as store_name,
                      a.type                   as store_type,
                      c.id::text               as brand_id,
                      c.name                   as brand_name,
                      a.city_address_id        as city_id,
                      msc.city                 as city_name,
                      v.vertical,
                      brand_group_id::text     as brand_group_id,
                      brand_group_name,
                      cpgops.physical_store_id as physical_store_id,
                      pss.name                 as physical_store_name
               from {country}_PGLR_MS_STORES_PUBLIC.stores_vw a
                        left join {country}_PGLR_MS_STORES_PUBLIC.store_brands_vw c
                                  on c.id = a.store_brand_id and not coalesce(c._fivetran_deleted, false)
                        left join {country}_pg_ms_cpgops_stores_ms_public.stores cpgops on cpgops.store_id = a.store_id
                        left join {country}_pg_ms_cpgops_stores_ms_public.physical_stores pss on pss.id = cpgops.physical_store_id
                        left join {country}_PGLR_MS_COUNTRY_DATA_PUBLIC.city_addresses msc
                                  on msc.id = a.city_address_id and not coalesce(msc._fivetran_deleted, false)
                        inner join (select store_type                      as storetype,
                                           case
                                               when vertical_group = 'ECOMMERCE' then 'Ecommerce'
                                               when vertical_group = 'RESTAURANTS' then 'Restaurantes'
                                               when vertical_group = 'WHIM' then 'Antojos'
                                               when vertical_group = 'RAPPICASH' then 'RappiCash'
                                               when vertical_group = 'RAPPIFAVOR' then 'RappiFavor'
                                               when upper(vertical_sub_group) in ('SUPER', 'HIPER') then 'Mercados'
                                               when upper(store_type) in ('TURBO') then 'Turbo'
                                               when upper(vertical_sub_group) in ('TURBO') then 'Turbo'
                                               when upper(vertical_sub_group) in ('PHARMACY') then 'Farmacia'
                                               when upper(vertical_sub_group) in ('LIQUOR') then 'Licores'
                                               when upper(vertical_sub_group) in ('EXPRESS') then 'Express'
                                               when upper(vertical_sub_group) in ('SPECIALIZED') then 'Specialized'
                                               when upper(vertical_group) in ('CPGS') then 'CPGs'
                                               else vertical_sub_group end as vertical
                                    from VERTICALS_LATAM.{country}_VERTICALS_V2
               ) v on v.storetype = a.type

                        left join (
                   select *
                   from (select store_brand_id,
                                last_value(brand_group_id)
                                           over (partition by store_brand_id order by tb.created_at asc)          as brand_group_id,
                                last_value(bg.name)
                                           over (partition by store_brand_id order by tb.created_at asc)          as brand_group_name
                         from {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.STORE_TYPE_BRANDS tb
                                  join {country}_PGLR_MS_CMS_STORES_CONFIG_PUBLIC.BRAND_GROUPS BG on brand_group_id = BG.ID)
                   group by 1, 2, 3
               ) bg on bg.store_brand_id = a.store_brand_id

               where not coalesce(a._fivetran_deleted, false)
               and v.vertical not in ('Turbo')
               """.format(timezone=timezone, country=country)

    query = '''with PAIS as (select DISTINCT ORDER_ID, DATEADD('MINUTE', FLOOR(DATE_PART('MINUTE', created_at::TIMESTAMP_NTZ)/(60)::FLOAT)*     (60)::FLOAT, DATE_TRUNC('HOUR', created_at::TIMESTAMP_NTZ))::TIMESTAMP_NTZ as DATE from {country}_PG_MS_REALTIME_INTERFACE_PUBLIC.CHATS 
where created_at::date >= DATEADD(DD, -15,current_date())
and (data ilike '%receita%' or data ilike '%receta%' or data ilike '%formula medica%' or data ilike '%prescripcion%')),


TIME AS (SELECT DATEADD('MINUTE', FLOOR(DATE_PART('MINUTE', created_at::TIMESTAMP_NTZ)/(60)::FLOAT)*     (60)::FLOAT, DATE_TRUNC('HOUR', created_at::TIMESTAMP_NTZ))::TIMESTAMP_NTZ as DATE
 
      FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW
          WHERE DATE::date >= DATEADD(DD, -15,current_date())
             GROUP BY DATE),
             
VERTICAL as (select distinct store_type,
             last_value(sub_group) over (partition by store_type order by _fivetran_synced) as sub_group
            from {country}_grability_public.verticals
),

ORDERS AS (SELECT PAIS.ORDER_ID, 
           PAIS.DATE AS DAY,
           TOTAL_VALUE
          FROM PAIS
          JOIN ops_global.CANCELLATION_REASONS AS A
            ON PAIS.ORDER_ID = A.ORDER_ID
          JOIN {country}_CORE_ORDERS_PUBLIC.ORDERS_VW AS B
            ON PAIS.ORDER_ID = B.ID
          LEFT JOIN {country}_core_orders_public.order_stores_vw  C 
            ON C.order_id = B.id
          LEFT JOIN {country}_pglr_ms_stores_public.stores_vw D 
            ON D.store_id = C.store_id
          LEFT JOIN VERTICAL E
            ON E.store_type = D.type
          WHERE E.SUB_GROUP = 'Farmacia'),




CANCELS AS (SELECT DAY, 
       NAME AS BRAND_NAME,
       ID AS BRAND_ID,
       COUNT(*) AS CANCELS,
       SUM(TOTAL_VALUE) AS GMV_LOST
       
FROM ORDERS 
    LEFT JOIN (SELECT DISTINCT order_id, 
                               NAME as name_t, 
                               store_id,
                               created_at 
               FROM {country}_core_orders_public.order_stores_vw
               WHERE COALESCE(_fivetran_deleted,false)=false) as A
                    ON ORDERS.ORDER_ID = A.order_id 
 
               LEFT JOIN (SELECT store_id, 
                                 name as name_y, 
                                 store_brand_id 
                          FROM {country}_pglr_ms_stores_public.stores_vw 
                          WHERE city_address_id != 104) as B 
                                 ON  B.store_id = A.store_id 
               LEFT JOIN {country}_pglr_ms_stores_public.store_brands_vw as C 
                    ON C.id = B.store_brand_id
group by day, name, id
order by CANCELS asc),


BASE_ORDERS_D1 AS (SELECT CREATED_AT::DATE as data_2_d1,
ID
 
      FROM {country}_CORE_ORDERS_PUBLIC.ORDERS_VW
          WHERE data_2_d1::date >= DATEADD(DD, -15,current_date())
            AND state NOT IN ('canceled_by_fraud',
                              'canceled_for_payment_error',
                              'canceled_by_split_error')),



BRAND_ORDERS AS (SELECT count(distinct BASE_ORDERS_D1.ID) as ORDERS_D1, 
 C.NAME as B_NAME,
 DATEADD('MINUTE', FLOOR(DATE_PART('MINUTE', created_order_brand::TIMESTAMP_NTZ)/(60)::FLOAT)*     (60)::FLOAT, DATE_TRUNC('HOUR', created_order_brand::TIMESTAMP_NTZ))::TIMESTAMP_NTZ AS DIA,
 C.id as BRAND_ID_D1
 FROM BASE_ORDERS_D1 
    LEFT JOIN ( SELECT DISTINCT order_id, 
                             NAME, 
                             store_id,
                             DATEADD('MINUTE', FLOOR(DATE_PART('MINUTE', created_at::TIMESTAMP_NTZ)/(60)::FLOAT)*     (60)::FLOAT, DATE_TRUNC('HOUR', created_at::TIMESTAMP_NTZ))::TIMESTAMP_NTZ as CREATED_ORDER_BRAND
                             
              FROM {country}_core_orders_public.order_stores_vw
                 WHERE COALESCE(_fivetran_deleted,false)=false) as A
                    ON BASE_ORDERS_D1.id = A.order_id 
 
              LEFT JOIN (SELECT store_id, 
                                name, 
                                store_brand_id 
                                   FROM {country}_pglr_ms_stores_public.stores_vw 
                                        WHERE city_address_id != 104) B 
              ON  B.store_id = A.store_id 

              LEFT JOIN {country}_pglr_ms_stores_public.store_brands_vw as C 
              ON C.id = B.store_brand_id
              WHERE BRAND_ID_D1 in (SELECT distinct BRAND_ID
                                    FROM CANCELS)
group by DIA,
         B_NAME,
         BRAND_ID_D1/*,
         BRAND_GP_ID*/
order by ORDERS_D1 desc)

SELECT C.DATE AS DAY,
       D.BRAND_NAME AS BRAND_NAME,
       D.BRAND_ID AS BRAND_ID,
       D.CANCELS AS CANCELS,
       ZEROIFNULL(D.ORDERS) AS ORDERS,
       '{country}' AS COUNTRY
       FROM TIME AS C
       LEFT JOIN (SELECT COALESCE(A.DAY, B.DIA) AS DIA,
       COALESCE(A.BRAND_NAME, B.B_NAME) AS BRAND_NAME,
       COALESCE(A.BRAND_ID, B.BRAND_ID_D1) AS BRAND_ID,
       A.CANCELS AS CANCELS,
       ZEROIFNULL(B.ORDERS_D1) AS ORDERS
       FROM CANCELS AS A       
       FULL JOIN BRAND_ORDERS AS B
       ON B.BRAND_ID_D1 = A.BRAND_ID
       AND B.DIA =  A.DAY) D
       ON C.DATE =  D.DIA'''.format(timezone=timezone, country=country.upper())
    store_infos = snow.run_query(query_store_infos)
    orders_14d = snow.run_query(query)

    return store_infos, orders_14d


def get_dataframe(country):
    timezone, interval = timezones.country_timezones(country)
    if country in ['br', 'ar', 'uy']:
        interval = '6h'
    elif country in ['mx']:
        interval = '0h'
    elif country in ['co', 'pe', 'ec']:
        interval = '8h'
    elif country in ['cl']:
        interval = '7h'
    elif country in ['cr']:
        interval = '9h'

    query_chat = """
    --no_cache
    with a as (
    select max(id) as id_maximo from chats
    )
    select c.order_id as orderid
    from a
    join chats c on c.id >= (a.id_maximo - 100000)
    where 
    created_at - interval '{interval}' >= now() at time zone 'america/{timezone}' - interval '1h'
    and (data ilike '%receita%' or data ilike '%receta%' or data ilike '%formula medica%' or data ilike '%prescripcion%')
    group by 1
    """.format(interval=interval, timezone=timezone)

    query_orders = ''' 
    --no_cache
    with base as
    (
    SELECT distinct order_id
    FROM order_modifications om
    WHERE om.created_at >= now() AT TIME ZONE 'America/{timezone}' - interval '60 minutes'
    GROUP BY 1
    )
    select os.store_id::text as store_id,
    base.order_id,
    max(os.name) as store_name,
    max(os.type) as store_type
    from base
    inner join orders o on o.id=base.order_id
    inner join order_stores os on os.order_id = o.id
    where o.state not in ('canceled_by_fraud', 'canceled_for_payment_error','canceled_by_split_error','canceled_by_picker','finished','pending_review','arrive','on_the_route','in_store','in_store','created')
    and os.type NOT IN ('queue_order_flow','soat_webview','turbo', 'turbo_bebidas','turbo_market','turbo_express','loja_ja','loja_ja_market','bebidas_ja','express_st_marche')
    and os.store_type_group not in ('restaurant_cargo', 'restaurant','whim','travel','market')
    group by 1,2'''.format(interval=interval, timezone=timezone)

    if country == 'co':
        orders = redash.run_query(1904, query_orders)
        chat = redash.run_query(652, query_chat)
    elif country == 'ar':
        orders = redash.run_query(1337, query_orders)
        chat = redash.run_query(647, query_chat)
    elif country == 'cl':
        orders = redash.run_query(1155, query_orders)
        chat = redash.run_query(649, query_chat)
    elif country == 'mx':
        orders = redash.run_query(1371, query_orders)
        chat = redash.run_query(651, query_chat)
    elif country == 'uy':
        orders = redash.run_query(1156, query_orders)
        chat = redash.run_query(650, query_chat)
    elif country == 'ec':
        orders = redash.run_query(1922, query_orders)
        chat = redash.run_query(2184, query_chat)
    elif country == 'cr':
        orders = redash.run_query(1921, query_orders)
        chat = redash.run_query(2183, query_chat)
    elif country == 'pe':
        orders = redash.run_query(1157, query_orders)
        chat = redash.run_query(745, query_chat)
    elif country == 'br':
        orders = redash.run_query(1338, query_orders)
        chat = redash.run_query(648, query_chat)

    return chat,orders

def receita(chat,orders,store_infos,orders_14d):

    channel_alert = timezones.slack_channels('partner_operation')
    current_time = timezones.country_current_time(country)

    if not chat.empty:
        orders_cancel = pd.merge(chat, orders, how='inner', left_on=chat['orderid'], right_on=orders['order_id'])
        orders_cancel = orders_cancel.drop(['key_0', 'orderid'], axis=1)
        orders_cancel = pd.merge(orders_cancel, store_infos, how='left', left_on=orders_cancel['store_id'],right_on=store_infos['store_id'])
        orders_cancel = orders_cancel[['brand_id', 'brand_name', 'store_id_x', 'store_name_x', 'order_id', 'vertical']].rename(
            columns={'store_id_x': 'store_id', 'store_name_x': 'store_name'})
        orders_cancel = orders_cancel[orders_cancel['vertical'] == 'Farmacia'].drop(['vertical'], axis=1)
        total_orders_cancel_brand = orders_cancel.groupby(['brand_id', 'brand_name']).count().reset_index()
        total_orders_cancel_brand = total_orders_cancel_brand[['brand_id', 'brand_name', 'order_id']].rename(columns={'order_id': 'cancels'})
        orders_total = pd.merge(orders, store_infos, how='left', left_on=orders['store_id'],right_on=store_infos['store_id'])
        orders_total = orders_total[['brand_id', 'order_id']]
        orders_total = orders_total.groupby('brand_id').count().reset_index()
        orders_total = pd.merge(total_orders_cancel_brand, orders_total, on=['brand_id']).rename(
            columns={'order_id': 'orders'})
        orders_total['day']=current_time.date()
        orders_total = orders_total[['day','brand_id', 'brand_name', 'orders', 'cancels']]

        ### avg 14 days
        orders_14d_ = orders_14d.loc[orders_14d['day'].dt.date < current_time.date()]
        orders_14d_['cancels'] = orders_14d_['cancels'].notnull()
        avg_orders_14d = orders_14d_.groupby(['brand_id']).sum().reset_index()
        avg_orders_14d=avg_orders_14d.rename(columns={'cancels':'cancels_d14','orders':'orders_d14'})

        ### D0
        orders_d0 = orders_14d.loc[orders_14d['day'].dt.date >= current_time.date()]
        orders_d0['day']=orders_d0['day'].dt.date
        orders_d0['cancels'] = orders_d0['cancels'].fillna(0)
        orders_d0 = orders_d0.groupby(['day','brand_id', 'brand_name']).sum().reset_index()

        orders_total['cancels']=orders_total['cancels'].astype(float)
        orders_total['orders'] = orders_total['orders'].astype(float)
        orders_d0['cancels'] = orders_d0['cancels'].astype(float)
        orders_d0['orders'] = orders_d0['orders'].astype(float)

        orders_total_d0 = pd.concat([orders_d0,orders_total],axis=0)
        orders_total_d0 = orders_total_d0.groupby(['day','brand_id', 'brand_name']).sum().reset_index()
        orders_total_d0=orders_total_d0.rename(columns={'cancels':'cancels_d0','orders':'orders_d0'})
        final_orders = pd.merge(orders_total_d0,avg_orders_14d,how='inner',left_on=orders_total_d0['brand_id'],right_on=avg_orders_14d['brand_id']).rename(columns={'brand_id_x':'brand_id'})
        final_orders=final_orders.drop(['key_0','brand_id_y'],axis=1)
        final_orders['avg_d0'] = round(final_orders['cancels_d0'] / final_orders['orders_d0'], 2)
        final_orders['avg_14d']= round(final_orders['cancels_d14']/final_orders['orders_d14'],2)
        final_orders['rate_avg'] = (round(final_orders['avg_d0'] / final_orders['avg_14d'], 2)-1)*100
        final_orders['rate_cancel'] = (round(final_orders['cancels_d0']/final_orders['cancels_d14'],2)-1)* 100
        print(final_orders)

        final_orders = final_orders[((final_orders['rate_avg'] >= 50) & (final_orders['rate_cancel'] >= 50) & (final_orders['cancels_d0'] >= 5))]

        final_orders=final_orders[['brand_id', 'brand_name','cancels_d0', 'orders_d0','cancels_d14', 'orders_d14', 'avg_d0', 'avg_14d', 'rate_avg','rate_cancel']]
        final_orders['country'] = country
        final_orders['alarm_at'] = current_time
        print(final_orders)

        if not final_orders.empty:
            snow.upload_df_occ(final_orders,'prescription_drugs')
        else:
            print('final_orders null')


        if not final_orders.empty:
            try:
                for index, row in final_orders.iterrows():
                    text = '''
                    Alarma - Cancelación de ordenes por falta de prescripción de medicamentos :alert:
                    País: {country} :flag-{country}:
                    La Brand {brand_name} de brand_id {brand_id} aumentó {rate_cancel}% en número de cancelaciones y el % en {rate_avg} promedio de cancelaciones en comparación con los últimos 14 días.
                    Cancelaciones D0: {cancels_d0} 
                    Ordenes totales D0: {orders_d0}
                    Cancelaciones D14: {cancels_d14} 
                    Ordenes totales D14: {orders_d14}
                            '''.format(
                brand_name=row['brand_name'],
                brand_id=row['brand_id'],
                country=country,
                rate_cancel=row['rate_cancel'],
                cancels_d0=row['cancels_d0'],
                orders_d0=row['orders_d0'],
                cancels_d14=row['cancels_d14'],
                orders_d14=row['orders_d14'],
                rate_avg=row['rate_avg']
                    )

                    print(text)
                    slack.bot_slack(text, channel_alert)

            except Exception as e:
                print(e)

        else:
            print('final_orders null')

    else:
        print('chat empty')

countries = ['br','cl','co','ar','uy','cr','ec','pe','mx']
for country in countries:
    print(country)
    try:
        chat, orders = get_dataframe(country)
        store_infos, orders_14d = orders_brand(country)
        receita(chat,orders,store_infos,orders_14d)

    except Exception as e:
        print(e)


