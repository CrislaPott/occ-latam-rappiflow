from smtplib import SMTP
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import pandas as pd
from pretty_html_table import build_table
import os, sys, json
import pandas as pd
from os.path import expanduser

def send_email(df, df2, growth, orders0, promedio, email, brand_name, attach,vertical):
	body = build_table(df, 'blue_dark', text_align='center')
	body2 = build_table(df2, 'blue_dark', text_align='center')

	message = MIMEMultipart()
	message['Subject'] = f'Alarma 🔔 Caída de ventas {"".join(map(str, growth))}% {"".join(brand_name)} {"".join(vertical)}'
	message['From'] = 'occ.rappi@gmail.com'
	message['To'] =  email

	body_content = body
	body_content2 = body2
	text = f'''<html>
	<head>
	   <title>HTML text bold</title>
	</head>
	<body>
	   <h2 style="font-size:30px;"> 📣 Equipo OCC te Informa 📣</h2>
	   <p style="font-size:20px;">Hay una caída de ventas de <strong>{"".join(map(str, growth))}%</strong> en el Group Brand {"".join(brand_name)}</p>
	   <p style="font-size:20px;">Pedidos Hoy (ultima hora): <span style="color:red">{"".join(map(str, orders0))}</span> 😫  | Promedio mismo dia L2W (mismo periodo) : <span style="color:blue">{"".join(map(str, promedio))}</span> </p>
	   <p style="font-size:20px;"> <i>Top 10 tiendas con pedidos en las ultimas 2 semanas y su status en CMS: </i></p>
	</body>
	</html>
	'''

	text2 = f'''<html>
   <body>
	  <p style="font-size:20px;"> <i>Top 15 itens con mayor variación de ventas Vs. últimas semanas : </i></p>
   </body>
   </html>
    '''
	text3 = f'''<html>
   <body>
      <p style="font-size:20px;">Para obtener más información y ayuda, envie una msg en slack <a href="https://app.slack.com/client/T2QSQ3L48/CS7UQAMLG">#occ_atendimento</a> </p>
   <p style="font-size:20px;"> </p>
   </body>
   </html>
    '''

	message.attach(MIMEText(text, "html"))
	message.attach(MIMEText(body_content, "html"))
	message.attach(MIMEText(text2, "html"))
	message.attach(MIMEText(body_content2, "html"))
	message.attach(MIMEText(text3, "html"))


	message.attach(attach)

	msg_body = message.as_string()

	server = SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(message['From'], 'mvlteqnokrxenvuf')
	server.sendmail(message['From'], message['To'], msg_body)
	server.quit()
	return "Mail sent successfully."