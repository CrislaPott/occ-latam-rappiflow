#!/usr/bin/env python
# coding: utf-8

# In[1]:


# !jupyter nbconvert --to script bot_telegram.ipynb


# In[1]:

def send_telegram():

	import matplotlib.pylab as plt
	from pandas.plotting import table
	from os.path import expanduser

	telegram_token = '1903200994:AAH_HycPXkAZOQCzGHtEQuoP7g-DJUGrLNw'
	URL = "https://api.telegram.org/bot{}/".format(telegram_token)
	import requests
	import json
	import os
	def get_url(url):
		response = requests.get(url,verify=True)
		content = response.content.decode("utf8")
		return content


	def get_json_from_url(url):
		content = get_url(url)
		js = json.loads(content)
		return js


	def get_updates():
		url = URL + "getUpdates"
		js = get_json_from_url(url)
		return js


	def get_last_chat_id_and_text(updates):
		num_updates = len(updates["result"])
		last_update = num_updates - 1
		text = updates["result"][last_update]["message"]["text"]
		chat_id = updates["result"][last_update]["message"]["chat"]["id"]
		return (text, chat_id)

	def send_message(text, chat_id):
		url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
		get_url(url)
		
	def f7(seq):
		seen = set()
		seen_add = seen.add
		return [x for x in seq if not (x in seen or seen_add(x))]


	# In[2]:


	file = 'chats_id.csv'
	import pandas as pd
	import csv


	last_updates = get_updates()
	print(last_updates)
	print(len(last_updates))
	num_updates = len(last_updates["result"])
	for i in range(0, num_updates):
		print(i)
	#     text = last_updates["result"][i]["message"]["text"]

		chat_id = last_updates["result"][i]["message"]["chat"]["id"]
		first_name = last_updates["result"][i]["message"]["chat"]["first_name"]
		try:
			last_name = last_updates["result"][i]["message"]["chat"]["last_name"]
		except:
			last_name =''
		fields=[chat_id, first_name, last_name]
		with open(file, 'a', newline='') as f:
			writer = csv.writer(f)
			writer.writerow(fields)
			
		df = pd.read_csv(file)
		df.columns = ['chats_id','first_name','last_name']
		df.drop_duplicates( inplace=True)
		df.to_csv(file, index= False)
		print (chat_id, first_name, last_name)


	# In[3]:


	df = pd.read_csv(file)
	df = df.head(2)
	df


	# In[4]:


	ids_tels = list(df['chats_id'].values)
	name_tels = list(df['first_name'].values)

	def launch_bot(ids_tels, name_tels):

		ids_tels = f7(ids_tels)
		name_tels = f7(name_tels)

		print(ids_tels)
		print(name_tels)

		for idx, id_tel in enumerate(ids_tels):
			msg = "%s, reset  \n  \n \U0001F601 \n  By: Aleja \U000026A1" % (
				name_tels[idx])
			test = send_message(msg, chat_id=id_tel)
			print(msg)
			
	print(name_tels)


	# In[5]:


	# # Bot
	# ARG, COL

	URL = "https://api.telegram.org/bot{}/".format(telegram_token)

	print(telegram_token)
	launch_bot(ids_tels, name_tels)

	# # Unir todos los procesos


	# In[6]:


	import string
	import numpy as np
	import pandas as pd


	import matplotlib.pyplot as plt
	import pandas as pd
	from pandas.plotting import table
	import dataframe_image as dfi


	data = {'store_id': ['126192', '128303', '126758', '118276', '128002'],
	'store_name': ['Carrefour, La Plata', 'Carrefour, Villa Devoto', 'Carrefour, Cabildo', 'Carrefour, Vicente Lopez',
	'Carrefour, Recoleta'],
	'Pedidos Hoy': ['2', '2', '5', '3', '6'],
	'Promedio': ['7.5', '7.0', '7.0', '5.5', '5.5'],
	'Variación': ['-73.33', '-71.43', '-28.57', '-45.45', '9.09'],
	'is_enabled': ['true', 'true', 'true', 'true', 'true'],
	}

	df = pd.DataFrame (data, columns = ['store_id','store_name', 'Pedidos Hoy', 'Promedio', 'Variación', 'is_enabled'])


	# df
	# set fig size
	fig, ax = plt.subplots(figsize=(12, 3)) 
	# no axes
	ax.xaxis.set_visible(False)  
	ax.yaxis.set_visible(False)  
	# no frame
	ax.set_frame_on(False)  
	# plot table
	tab = table(ax, df, loc='upper right')  
	# set font manually
	tab.auto_set_font_size(False)
	tab.set_fontsize(8) 
	# save the result
	home = expanduser("~")
	results_file = '{}/dataframe.png'.format(home)

	plt.savefig(results_file)


	# In[7]:


	import telegram

	PHOTO_PATH = 'dataframe.png'
	FILE_PATH = '_home_astro_relatorio.xlsx'

	for idx, chat_id in  enumerate(list(ids_tels)):
		print(name_tels[idx])
		chat_id = int(chat_id)
		bot = telegram.Bot(token=telegram_token)
		
		
		bot.send_message(chat_id=chat_id, 
					text=f''' 
		<b>📣 Equipo OCC te Informa 📣</b>
		

		Hay una caída de ventas de <b><strong>{"".join(map(str, '50'))}%</strong></b> en el Group Brand <u>{"".join('ACC')}</u>.
		
		Pedidos Hoy (última hora): <b>{"".join(map(str, '40'))}</b> 😫 
		Promedio mismo día L2W (mismo periodo): <b>{"".join(map(str, '80'))}</b> 
		
		<i>Top 10 tiendas con pedidos en las últimas 2 semanas y su status en CMS: </i>
		''', parse_mode=telegram.ParseMode.HTML)
		
		bot.send_photo(chat_id=chat_id, photo=open(results_file, 'rb'))
		
		
		bot.send_message(chat_id=chat_id, 
					text=f'''
		<i>Top 15 items con mayor variación de ventas hoy Vs. últimas semanas :</i>
		''', parse_mode=telegram.ParseMode.HTML)    
		
		bot.send_photo(chat_id=chat_id, photo=open(results_file, 'rb'))

		
		bot.send_message(chat_id=chat_id, 
					text=f'''
		Para obtener más información y ayuda, envie una msg en slack <a href="https://rappiplus.slack.com/archives/CS7UQAMLG"> occ_atendimento</a>
		''', parse_mode=telegram.ParseMode.HTML)    

		bot.send_document(chat_id=chat_id, document=open(FILE_PATH, 'rb'))


		# bot.send_message(chat_id=chat_id, 
		#              text=f'''
		# Para escalar: <a href="https://app.useavenue.com/api/escalations/58a7aa27-645b-422e-b03d-a1ffe402e254/escalate"> clic aquí</a>
		# ''', parse_mode=telegram.ParseMode.HTML)  
		

